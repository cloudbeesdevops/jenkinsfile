#!/bin/bash

username=f5081650

echo -e "\nThis script will check the available free memroy and if crossed the threshold, it will send the alert mailn"

while true; do
   echo -e "Enter the environment that needs to check the available free memroy: L1, L2, L3, L4A, L4B, P\n"

   read LINE

   if [ "${LINE}" == "L1" ]; then
      hosts="urh00600.ute.fedex.com urh00601.ute.fedex.com urh00602.ute.fedex.com"
      break

   elif [ "${LINE}" == "L2" ]; then
      hosts="irh00600.ute.fedex.com irh00601.ute.fedex.com irh00602.ute.fedex.com"
      break

   elif [ "${LINE}" == "L3" ]; then
      hosts="srh00600.ute.fedex.com srh00601.ute.fedex.com srh00602.ute.fedex.com"
      break

   elif [ "${LINE}" == "L4A" ]; then
      hosts="vrh00906.ute.fedex.com vrh00907.ute.fedex.com"
      break

   elif [ "${LINE}" == "L4B" ]; then
      hosts="vrh00916.ute.fedex.com vrh00917.ute.fedex.com vrh00918.ute.fedex.com \
             vrh00919.ute.fedex.com vrh00920.ute.fedex.com"
      break

   elif [ "${LINE}" == "P" ]; then
  hosts="prh01911.prod.fedex.co"
break

   else
      echo -e "INVALID input!!!\n"
      continue
   fi
done

for host in $hosts; do
   scp -q CheckMemory.sh $username@$host:/tmp

   echo -e "\nChecking the available free memroy in $host\n"
   ssh -t -o LogLevel=QUIET $username@$host "cd /tmp;./CheckMemory.sh"
done
