pipeline {

    agent {
        label 'BuildNode-10278'
    }
    
    parameters {
        booleanParam(name: 'nexus_fortify_only', defaultValue: false, description: '')
    }
    
    triggers {
    	parameterizedCron(env.BRANCH_NAME == 'master' ? '''
        H H * * 6 %nexus_fortify_only=true
        ''' : '')
        
        gitlab(
        triggerOnPush: true,
        triggerOnMergeRequest: true, triggerOpenMergeRequestOnPush: "never",
        triggerOnNoteRequest: true,      
        noteRegex: "please build",      
        skipWorkInProgressMergeRequest: true,
        secretToken: "093e050bda2483fe9cf92a2a5103814a",
        ciSkip: false,
        setBuildDescription: true,
        addNoteOnMergeRequest: true,
        addCiMessage: true,
        addVoteOnMergeRequest: true,
        acceptMergeRequestOnSuccess: false,        
        cancelPendingBuildsOnUpdate: false)            
    }
    
    tools {
        jdk 'JAVA_8'
    }
    
    environment {
        HTTP_PROXY='http://east.network.fedex.com:3128'
        HTTPS_PROXY='http://east.network.fedex.com:3128'
        EMAIL_LIST="RatingTrain-ConvergentTeam@corp.ds.fedex.com"
    }

    stages {
        stage ('Checkout') {
            steps {
            	gitlabCommitStatus('Checkout') {
            		checkout scm
                	echo "Space left on node:"
                	sh "df -h"
                	echo "hostname"
                	sh "hostname"
            	}                
            }
        }
        
        stage ('commonRepo') {
            steps {
                 dir('common-repo') {
                    git branch:'master',
                    credentialsId:'rebs',
                    url:'git@gitlab.prod.fedex.com:APP7238/rebscommon.git'
                }
            }   
        }
        
        stage('clean') {
            steps {
            	gitlabCommitStatus('clean') {
            		sh "chmod 755 ./gradlew"
                	sh "./gradlew :ui-client:npmClean clean --no-daemon"
            	}            	
            }
        }
        
        stage ("Fortify Scan") {
			/*when {
				branch 'master'				
			}*/
  			steps {
  				script {
  					gitlabCommitStatus('Fortify Scan') {
  						if ("${BRANCH_NAME}" == 'master') {
  							if (params.nexus_fortify_only) {
  								node ('master') {
     								dir (env.JENKINS_HOME + '/userContent/fortify') {
     									stash name:'stash-fortify', includes:'fortifyFunctions.sh, translate.sh'
     								}     			
     							}
     							unstash 'stash-fortify'
     							sh "${WORKSPACE}/translate.sh 3530806_Enterprise-Rating-Data RUN"
  							} else {  					      
  								print "Fortify Scan will run on every Saturday"
  							}
  						} else {
  							print "Fortify Scan will run on every Saturday for master branch only"
  						}
  					}    
  				}     			
  			}
  		}
    
		stage('assemble') {
            steps {
            	gitlabCommitStatus('assemble') {
            		sh "./gradlew :ui-client:npmBuild assemble --no-daemon"
                	sh "./gradlew createTar --no-daemon"
            	}				
            }
        }
        
		stage('generate metrics') {
        	steps {
        		gitlabCommitStatus('generate metrics') {
					sh "./gradlew javadoc jacocoTestReport --no-daemon"
				}
			}
        }
        
		stage('test') {
			steps {
				gitlabCommitStatus('test') {
					sh "./gradlew test -x integrationTest --no-daemon"
					junit '**/build/test-results/**/*.xml'
				}				
            }
        }
        
        stage('Integration Test') {
            steps {
            	gitlabCommitStatus('Integration Test') {                
					sh "./gradlew integrationTest --no-daemon"
				}
			}
        }
        
		stage('compile metrics and archive artifacts') {
            steps {
                parallel(
                	archive:{
						archiveArtifacts artifacts: 'build/distributions/*.tar.gz'
		            },
	                openTasks: {
						step([$class: 'TasksPublisher', canComputeNew: false, defaultEncoding: '', excludePattern: '', healthy: '', high: 'FIXME', low: 'EXTRA', normal: 'TODO', pattern: '**/*.java', unHealthy: ''])
	                },
	                javadoc: {
						step([$class: 'JavadocArchiver', javadocDir: 'build/javadoc', keepAll: false])
	                },
	                jacoco: {
						step([$class: 'JacocoPublisher', classPattern: '**/build/classes/main', exclusionPattern: '**/com/fedex/rating/**/*.class,**/com/fedex/util/*.class', execPattern: '**/build/jacoco/*.exec'])
	                },
	                sonarqube: {
	                	withSonarQubeEnv('SonarQube') {
	                		sh "./gradlew --info sonarqube -Dsonar.branch.name=${BRANCH_NAME} --no-daemon"
	                	}
	                }
                )
            }
        }
        
    	stage ("Quality Control") {
     		steps {
     			timeout(5) {     			    
	    	    	script {
	    	    		gitlabCommitStatus('Quality Control') {
	    	    			//STAGE_NAME = "Quality Control"	
	           				def qualitygate = waitForQualityGate()
	              			if (qualitygate.status != "OK") {
	              				abortPipeline: true
	                 			error "Pipeline aborted due to quality gate coverage failure: ${qualitygate.status}"
	              			}
	    	    		}	           			
	        		}
     			}
     		}
  		}
  		
  		stage ("Nexus IQ") {
  			/*when {  			    
  			    branch 'master'
  			}*/
  			steps {
  				script {
  					gitlabCommitStatus('Nexus IQ') {
  						if ("${BRANCH_NAME}" == 'master') {
  							if (params.nexus_fortify_only) {
  								def policyEvaluationResult = nexusPolicyEvaluation failBuildOnNetworkError: false, iqApplication: selectedApplication('Enterprise-Rate-Data-3530806'), iqStage: 'build', jobCredentialsId: 'nexusIQ'       
  					   		} else { 
  								print "Nexus IQ Scan will run every Saturday"
  							}
  						} else {
  							print "Nexus IQ Scan will run every Saturday and for master branch only"  
  						}
  					} 					
  				}
  			}
  		}
    
        stage('deploy to test') {
			/*when {
				branch 'master'
			}*/
            steps {                
                script {
                	gitlabCommitStatus('deploy to test') {
						if ("${BRANCH_NAME}" == 'master') {
                			sh "./gradlew deployPatch --no-daemon"
                			sh "./gradlew restartAllDev --no-daemon"
                			EMAIL_LIST = 'RAISESCRUMTeams@corp.ds.fedex.com'
                		} else {
                			print "This is not master branch"    
                		}
                	}
            	}
            }
        }
        
        stage('deploy to DI1') {
			when {
				tag 'tag-di-*'
			}
            steps {
            	script {  
            		sh "./gradlew deployPatchDI1 --no-daemon"
                	sh "./gradlew restartDI1 --no-daemon"
                	EMAIL_LIST = 'RAISESCRUMTeams@corp.ds.fedex.com'
                }
            }
        }
        
        stage('deploy to SI1') {
			/*when {
				branch 'preprod'
			}*/
            steps {                
                script {
                	gitlabCommitStatus('deploy to SI1') {
						if ("${BRANCH_NAME}" == 'preprod') {
							sh "./gradlew deployPatchSI1 --no-daemon"
                			sh "./gradlew restartSI1 --no-daemon"
                			EMAIL_LIST = 'RAISESCRUMTeams@corp.ds.fedex.com'
                		} else {
                			print "This is not a preprod branch"
                		}
                	}
            	}
            }
        }
        
        stage('deploy to SI2') {
			/*when {
				branch 'prod'
			}*/
            steps {                
                script {
                	gitlabCommitStatus('deploy to SI2') {
				    	if ("${BRANCH_NAME}" == 'prod') {
				    		sh "./gradlew deployPatchSI2 --no-daemon"
                			sh "./gradlew restartSI2 --no-daemon"
                			EMAIL_LIST = 'RAISESCRUMTeams@corp.ds.fedex.com'
                		} else {
                			print "This is not a prod branch"	      
                		}
                	}
            	}
            }
        }
        
        stage('deploy to DI2') {
			/*when {
				branch 'prodfix'
			}*/
            steps {
            	script {
            		gitlabCommitStatus('deploy to DI2') {
				    	if ("${BRANCH_NAME}" =~ '^prodfix') { 
                			sh "./gradlew deployPatchDI2 --no-daemon"
                			sh "./gradlew restartDI2 --no-daemon"
                		} else {
							print "This is not a prodfix branch"                		      
						}
					}
				}
            }
        }
        
        stage('deploy feature') {
			/*when {
				not {			      
				    branch 'master'			      
				}
				not {
				    branch 'preprod'
				}
				not {
				    branch 'prod'
				}
				not {
				    branch 'prodfix*'
				}
			}*/
            steps {
            	script {
				    gitlabCommitStatus('deploy feature') {
				    	if ("${BRANCH_NAME}" != '^prodfix' && "${BRANCH_NAME}" != 'prod' && "${BRANCH_NAME}" != 'master' && "${BRANCH_NAME}" != 'preprod') {
                			sh "./gradlew deployPatchDev3 --no-daemon"
                			sh "./gradlew restartDev3 --no-daemon"
                		} else {
							print "This is not a feature branch"			                		      
						}
					}
				}
            }
        }
    
    }
    
    post { 
    	always {
    	    junit '**/build/test-results/**/*.xml,**/regression/**/*.xml'
    	}

        success {        
            emailext body: '''${SCRIPT, template="groovy-html.template"}''',
                mimeType: 'text/html',
                subject: "[Jenkins] ${currentBuild.fullDisplayName}",
                to: "",
                replyTo: "",
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'CulpritsRecipientProvider'], [$class: 'RequesterRecipientProvider']] 
        }

        failure {
        	echo "BRANCH: ${env.BRANCH_NAME}"
        	echo "AUTHOR: ${env.CHANGE_AUTHOR}"
	     
            emailext body: '''${SCRIPT, template="groovy-html.template"}''',
                mimeType: 'text/html',
                subject: "[Jenkins] ${currentBuild.fullDisplayName}",
                to: "${env.EMAIL_LIST}",
                replyTo: "${env.EMAIL_LIST}",
                recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'CulpritsRecipientProvider'], [$class: 'RequesterRecipientProvider']] 
        }
    }

    options {
    
		disableConcurrentBuilds()
		
		//In order to make connection between GitLab repo(only EAI 7238) and Jenkins.
    	gitLabConnection('Gitlab-3530806(Gitlab-3530806)')
    	
    	//Provide stage names in gitlab builds to send the status of the stage to gitlab
    	gitlabBuilds(builds: ['Checkout', 'clean', 'assemble', 'test', 'generate metrics',
    		'Quality Control', 'Nexus IQ', 'Fortify Scan', 'deploy to test', 'deploy to SI1', 'deploy to SI2',
    		'deploy to DI2', 'deploy feature', 'Integration Test'
    	]) 
    
        // For example, we'd like to make sure we only keep 20 days at a time, so
        // we don't fill up our storage!
        buildDiscarder(logRotator(artifactDaysToKeepStr:'5', numToKeepStr: env.BRANCH_NAME == 'master' ? '5' : '2'))
    
        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 60, unit: 'MINUTES')
    }
}
