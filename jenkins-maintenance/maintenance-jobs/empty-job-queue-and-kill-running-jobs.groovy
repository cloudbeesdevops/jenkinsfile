import java.util.ArrayList
import jenkins.model.Jenkins;
import org.jenkinsci.plugins.workflow.support.steps.StageStepExecution;

def props = []
search_folder="abcdefXYZ"
try { search_folder = "${env.SEARCH}" } catch(Exception e) {}
   props.add(
         [
          $class: 'ParametersDefinitionProperty', parameterDefinitions: [
            [$class: 'StringParameterDefinition', defaultValue: '', description: 'Search Job Phrase', name: 'SEARCH']
        ]]
    )
    properties(props)

// change the search criteria for focus area of the process.
// Remove everything which is currently queued
try {
    echo "Remove everything queued up"
    def q = Jenkins.instance.queue
    echo "${q}"
    for (queued in Jenkins.instance.queue.items) {
      echo "--> removing ${queued.task.name} from queue"
      try {
          echo "  --> ${queued}"
          q.cancel(queued.task)
      } catch (error) {
      }
    }
    echo "Searching for ${search_folder}"
    Jenkins.instance.getAllItems(org.jenkinsci.plugins.workflow.job.WorkflowJob.class).each {job ->
          if (!"${job}".contains("CleanUpJobs")) {
              //echo "--> terminating ${job.fullName}"
              stopJobs(job,"terminate")
          }
    }
    Jenkins.instance.getAllItems(org.jenkinsci.plugins.workflow.job.WorkflowJob.class).each {job ->
          if (!"${job}".contains("CleanUpJobs")) {
              //echo "--> killing ${job.fullName}"
              stopJobs(job,null)
          }
    }
} catch (err) {
    echo "error: ${err}"
}

def stopJobs(job, terminate ) {
    if (!job.fullName.contains(search_folder) || !job instanceof org.jenkinsci.plugins.workflow.job.WorkflowJob) {
        return
    }
    if (job.isBuilding() || job.isBuildBlocked()) {
      echo "cleanup ${job} : building ${job.isBuilding()} | ${job.isBuildBlocked()} | ${job.isConcurrentBuild() } | ${job.getWhyBlocked() }"
      echo "<a href=\"${job.url}\">${job.url}</a>"
      
      for (build in job.builds) {
          //echo "${build}"
          try {
              if (terminate) {
                  //echo "--> terminating job=${job.fullName} build=${build.displayName}"
                  build.doTerm()
                  StageStepExecution.exit(build)
              } else {
                  //echo "--> killing     job=${job.fullName} build=${build.displayName}"
                  build.doKill()
                  StageStepExecution.exit(build)
              }
          } catch (error) {
              echo "dokill error: ${error}"
          }
      }
    }

}
