import java.util.ArrayList
import jenkins.model.Jenkins;

// Remove everything which is currently queued
try {
    echo "Remove everything queued up, job count: ${Jenkins.instance.queue.items.size()}"
    def q = Jenkins.instance.queue
    for (queued in Jenkins.instance.queue.items) {
      echo "--> removing ${queued.task.name} from queue"
      try {
          echo "  --> ${queued}"
          q.cancel(queued.task)
      } catch (error) {
      }
    }

    // stop all the currently running jobs

} catch (err) {
    echo "error: ${err}"
}

def stopJobs(job, terminate ) {
  if (job instanceof com.cloudbees.hudson.plugins.folder.AbstractFolder) {
    for (child in job.items) {
      stopJobs(child,terminate)
    }
  } else if (job instanceof hudson.model.Job) {
    //echo "${job} : building ${job.isBuilding()}"
    if (job.isBuilding()) {

      for (build in job.builds) {
          //echo "${build}"
          try {
              if (terminate) {
                  echo "--> terminating job=${job.displayName} build=${build.displayName}"
              build.doTerm()
              } else {
                  echo "--> killing     job=${job.displayName} build=${build.displayName}"
                  build.doKill()
              }
          } catch (error) {
              echo "dokill error: ${error}"
          }
      }
    }
  }

}
