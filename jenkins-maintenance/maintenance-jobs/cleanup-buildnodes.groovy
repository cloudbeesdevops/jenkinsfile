#!/usr/bin/groovy

@Library('libz') _


//Loop thru nodes and clean up tmp spaces and localRepositories
String[] nodeNames = ['3511','1603','1602','2143','994','1599']
nodeNames = ['uwb00078','urh00614', 'urh00613']
def stepsForParallel = [:]
for (i=0;i<nodeNames.length;i++) {
    stepsForParallel["${i} - Node ${nodeNames[i]}"] = cleanup("${nodeNames[i]}")
}
parallel stepsForParallel
// END



def cleanup(nodeName) {
    return {
      stage(nodeName) {
        node (nodeName) {
            echo "cleanup on ${nodeName}"
            sh '''#!/bin/bash
df -kh
## declare an array variable
declare -a arr=("/tmp" "/var/tmp" "/opt/jenkins" "/opt/fedex/jenkins" "/opt/fedex/tibco/tibco" "/opt/fedex/tibco/jenkins")
## now loop through the above array
for i in "${arr[@]}"
do
   echo "scanning $i"
	[[ -e "${i}/workspace/" ]] && find "${i}/workspace/" -user jenkins 	-mtime +7 -type d -exec rm -fr {} \\; -ls
	[[ -e "${i}/.m2/repository/" ]] && find "${i}/.m2/repository/" -user jenkins 	-mtime +7 -type d -exec rm -fr {} \\; -ls
done

echo
echo
echo End Result.
df -kh

'''
        }
      }
    }
}
