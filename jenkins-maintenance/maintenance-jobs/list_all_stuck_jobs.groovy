import java.util.ArrayList
import jenkins.model.Jenkins;
import org.jenkinsci.plugins.workflow.support.steps.StageStepExecution;

def props = []
search_folder="abcdefXYZ"
try { search_folder = "${env.SEARCH}" } catch(Exception e) {}
props.add(
     [
      $class: 'ParametersDefinitionProperty', parameterDefinitions: [
        [$class: 'StringParameterDefinition', defaultValue: '', description: 'Search Job Phrase', name: 'SEARCH'],
        [$class: 'BooleanParameterDefinition', description: 'Kill stuck jobs', name : 'KILL_STUCK_JOBS'],

        
    ]]
)
properties(props)

// change the search criteria for focus area of the process.
// Remove everything which is currently queued
try {
    def printer = []
    echo "Searching for ${search_folder}"
    Jenkins.instance.getAllItems(org.jenkinsci.plugins.workflow.job.WorkflowJob.class).each {job ->
          if (!"${job}".contains("CleanUpJobs")) {
              list(job,"${KILL_STUCK_JOBS}}",printer,false)
              
          }
    }
    Jenkins.instance.getAllItems(org.jenkinsci.plugins.workflow.job.WorkflowJob.class).each {job ->
          if (!"${job}".contains("CleanUpJobs")) {
              list(job,"${KILL_STUCK_JOBS}}",printer,true)
              
          }
    }    
    def printer_str = ""
    printer.each{ it->
        printer_str = printer_str + it
    }
    echo "Stuck Jobs\n" + printer_str
} catch (err) {
    echo "error: ${err}"
}


def list(job,kill_flag,printer,term_flag) {
    if (!search_folder.equals("")) {
        if (!job.fullName.contains(search_folder) || !job instanceof org.jenkinsci.plugins.workflow.job.WorkflowJob) {
            return
        }
    }
    if (job.isBuilding() || job.isBuildBlocked()) {
        echo "."
      //echo "job: ${job.fullName} - \n\t\tflag_kill=${kill_flag}"      
      for (build in job.builds) {
            echo "${kill_flag} " + kill_flag.getClass() + " " + "true".equals(kill_flag) + " job:" + job.fullName
          if ("true".equals(kill_flag) || kill_flag ) {
              if (term_flag) {
                printer <<  "\nterminate stuck: ${job.fullName}:build# ${build.displayName} : kill_flag=${kill_flag} \n\tbuilding flags ${job.isBuilding()} | ${job.isBuildBlocked()} | ${job.isConcurrentBuild() } | ${job.getWhyBlocked() }"
                build.doTerm()
                StageStepExecution.exit(build)
              } else {                  
                printer <<  "\nkill stuck: ${job.fullName}:build# ${build.displayName} : kill_flag=${kill_flag} \n\tbuilding flags ${job.isBuilding()} | ${job.isBuildBlocked()} | ${job.isConcurrentBuild() } | ${job.getWhyBlocked() }"
                build.doKill()
                StageStepExecution.exit(build)
              }
          }
          else {
              printer <<  "\nbuild stuck: ${job.fullName}:build# ${build.displayName} : kill_flag=${kill_flag} \n\tbuilding flags ${job.isBuilding()} | ${job.isBuildBlocked()} | ${job.isConcurrentBuild() } | ${job.getWhyBlocked() }"
          }
      }
    }
}

