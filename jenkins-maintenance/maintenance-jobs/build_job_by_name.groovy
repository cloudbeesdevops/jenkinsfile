import java.util.ArrayList
import jenkins.model.Jenkins;
import org.jenkinsci.plugins.workflow.support.steps.StageStepExecution;

def props = []
search_folder="${SEARCH}"


echo "Searching for ${search_folder}"
Jenkins.instance.getAllItems(org.jenkinsci.plugins.workflow.job.WorkflowJob.class).each {job ->
    search_folder.split(',').each{ it ->
        if (job.fullName.contains(it)) {
            if (!"${job}".contains("CleanUpJobs")) {
                echo "--> rebuilding ${job.fullName}"
                def cause = new hudson.model.Cause.UserIdCause()
                def causeAction = new hudson.model.CauseAction(cause) 
                try {
                    job.doReload()
                    //job.onCreatedFromScratch()
                    job.save()
                    Jenkins.instance.queue.schedule(job,10,causeAction)
                } catch (err) {}
            }
        }
    }
}
