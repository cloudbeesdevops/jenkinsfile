#!/usr/bin/groovy

pipeline {
    agent any
  options {
    timeout(time: 10, unit: 'MINUTES')
    timestamps()
    retry(1)
    disableConcurrentBuilds()

  }

tools {
    maven 'CF_CLIENT_INSTALL'
  }

  environment {
    https_proxy = 'http://internet.proxy.fedex.com:3128'
    http_proxy = 'http://internet.proxy.fedex.com:3128'
  }

  stages {
    stage('Setup') {
        steps {
          script {
              sshagent(['gitlab-user-for-master']) {
    withEnv([
        "GIT_SSH_COMMAND=ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no", "SEFS_VERIFICATION_BRANCH=${env.SEFS_VERIFICATION_BRANCH}"
        ]) {
            sh script: '''#!/bin/bash
bash <(curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh) > /dev/null
#find . -maxdepth 3 -type d
rm -fr sefs-verification
git clone git@gitlab.prod.fedex.com:5120346/sefs-verification.git -b "${SEFS_VERIFICATION_BRANCH}"
cd sefs-verification
cp -arf * ..
cd ..
pwd
mkdir -p tmp
'''
    stash name: 'verification', includes: '**/*.py,**/*.sh,config*/**'
          }
        }
          }
    }
    }
    stage('App Health') {
        steps {
            script {
            doQueueJobs(this,"${env.LDD_PUBLISH_TYPE}", "${env.LDD_HOST}","${env.SEFS_VERIFICATION_BRANCH}")
            }
        }
    }
  } // stages
    post {
    success {
      step([$class: 'WsCleanup', cleanWhenFailure: true, cleanWhenNotBuilt: true, cleanWhenUnstable: true, notFailBuild: true])
    }
    }

} // pipeline

def doVerification(l,o,t,branch) {
    sh 'pwd'
    if (branch.indexOf('refactor') < 0) {
        def types = ['BE','BW','JAVA']
        for (app in types) {
            // -i -l L1 -d config.PCF/pcf-pcf_info.json  -r app_health -w -p -vv
            sh returnStatus: true, script: "python envreadjson.py -d config.${o} -t ${app} -l ${l} -r ${t}"
        }
    }
    else {
      sh returnStatus: true, script: "python envreadjson.py -d config.${o}/*.json -l ${l} -r ${t}"
    }
}


def doQueueJobs(script, report_type, host, branch) {

  if (host == null || host == "null" || host == "") {
      host = "c0009869.test.cloud.fedex.com"
  }
  runs = [:]
  def levels = ['L1','L2','L3','L4','L5']
  def levelstring = levels.join(",")
  if (env.LDD_LEVEL != null && env.LDD_LEVEL != "") {
      levelstring = "${env.LDD_LEVEL}"
  }
  def u = levelstring.indexOf(env.LDD_LEVEL)
  def x = levelstring.indexOf('L1')
  def y = levelstring.indexOf('L2')
  def z = levelstring.indexOf('L3')
  def w = levelstring.indexOf('L4')
  script.echo("runninig for levels ${levels} and ${env.LDD_LEVEL}? L1 = ${x}")
  script.echo("runninig for levels ${levels} and ${env.LDD_LEVEL}? L2 = ${y}")
  script.echo("runninig for levels ${levels} and ${env.LDD_LEVEL}? L3 = ${z}")
  script.echo("runninig for levels ${levels} and ${env.LDD_LEVEL}? L4 = ${w}")
  def opcos = ['FXG','FXF','FXE']

    if (levelstring.indexOf('L1') >= 0 ) {
        runs["L1"] = {
        node {
            try {
                sh 'rm -rf `pwd`/*'
            unstash name: 'verification'
            for (o in opcos) {
                doVerification('L1',o, report_type,branch)
            }
            sh "curl -s http://${host}:8090/sefs-dashboard/api/public/health/harvest/L1"
        } catch (exception) {
            print exception
        } } }
    }
    if (levelstring.indexOf('L2') >= 0) {
      runs["L2"] = {
        node {
            try {
                sh 'rm -rf `pwd`/*'
            unstash name: 'verification'
            for (o in opcos) {
                doVerification('L2',o, report_type,branch)
            }
            sh "curl -s http://${host}:8090/sefs-dashboard/api/public/health/harvest/L2"
        } catch (exception) {
            print exception
        }
    }  } }

    if (levelstring.indexOf('L3') >=0 ) {
    runs["L3"] = {
    node {
        try {
            sh 'rm -rf `pwd`/*'
        unstash name: 'verification'
        for (o in opcos) {
            doVerification('L3',o, report_type,branch)
        }

        sh "curl -s http://${host}:8090/sefs-dashboard/api/public/health/harvest/L3"
    } catch (exception) {
        print exception
    }
    }
    }
    }

    if (levelstring.indexOf('L4') >=0 ) {
    runs["L4"] = {
    node {
        try {
        sh 'rm -rf `pwd`/*'
        unstash name: 'verification'
        for (o in opcos) {
            doVerification('L4',o, report_type,branch)
        }

        sh "curl -s http://${host}:8090/sefs-dashboard/api/public/health/harvest/L4"
    } catch (exception) {
        print exception
    }
    }  }  }

    if (levelstring.indexOf('L5') >=0 ) {
    runs["L5"] = {
    node {
        try {
            sh 'rm -rf `pwd`/*'
        unstash name: 'verification'
        for (o in opcos) {
            doVerification('L5',o, report_type,branch)
        }
        sh "curl -s http://${host}:8090/sefs-dashboard/api/public/health/harvest/L5"
    } catch (exception) {
        print exception
    }
    }    } }

  parallel runs
  for (f in levels) {
        for (o in opcos) {
            sh "curl -s http://${host}:8090/sefs-dashboard/api/public/health/${o}/${f} > /dev/null"
        }
  }
}
