import hudson.plugins.git.*
import jenkins.*
import jenkins.plugins.git.*
import jenkins.model.*
import java.lang.reflect.*
import org.jenkinsci.plugins.workflow.cps.*
import org.jenkinsci.plugins.workflow.libs.*

// concept taken from https://stackoverflow.com/questions/45266075/can-this-multibranch-pipeline-groovy-snippet-for-the-jenkins-script-console-beop?rq=1
// https://github.com/Praqma/JenkinsAsCodeReference/blob/master/dockerizeit/master/globalPipelineLibraries.groovy
shared_library_version = "4.1.0"
shared_library_name = "libz"
shared_library_remote = "git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git"
shared_library_creds = "gitlab-user-for-master"
shared_jenkinsfile = "./Jenkinsfile"
//job_name_search = "FXG_CORE/JAVA"
job_name_search = "${SEARCH}"


DRY_RUN = "nulls" // make null to activate and commit changes.


println "FOLDER ASSIGNMENTS\n"
output = ""
Jenkins.instance.getAllItems(com.cloudbees.hudson.plugins.folder.AbstractFolder).each{ it->
  
  if (it.fullName.contains(job_name_search) && !it.fullName.contains("CICD")) {
    folder_libraries = it.properties.findAll{ item -> item instanceof  org.jenkinsci.plugins.workflow.libs.FolderLibraries };
      if (folder_libraries.size() > 0) {
      //println "------ BEGIN ------"
        
        //https://github.com/jenkinsci/workflow-cps-global-lib-plugin/blob/master/src/main/java/org/jenkinsci/plugins/workflow/libs/LibraryConfiguration.java
        //https://github.com/jenkinsci/workflow-cps-global-lib-plugin/blob/master/src/main/java/org/jenkinsci/plugins/workflow/libs/SCMSourceRetriever.java
        //https://github.com/jenkinsci/git-plugin/blob/master/src/main/java/jenkins/plugins/git/GitSCMSource.java
        
        //println it.fullName
        
        def libraryconfig = folder_libraries[0].libraries
        def scm = libraryconfig.retriever[0].scm
        
        //println "SCMSource: "+  scm + " " + libraryconfig.defaultVersion
        if (scm instanceof hudson.plugins.git.GitSCM) {
          url = scm.remoteRepositories[0].uris[0].host
          output += "GitSCM : [" + url + "]: "+ libraryconfig.defaultVersion[0] + "\t" + it.fullName + "\n"

          alter_folder_library(it, libraryconfig, scm, url)
          
        }else
          if (scm instanceof jenkins.plugins.git.GitSCMSource) {
            output += "GitSCMSource: [" +  scm.id + "]:" +  libraryconfig.defaultVersion + "\t" + it.fullName + "\n"
          }
        else {
          println "Non-GitScmSource: " + scm.userRemoteConfigs
        }
    }
  } 
}

println output
printed = false

println "\nJOB ASSIGNMENTS\n"
output = ""
alter_jenkinsfile_job_assignment(job_name_search, DRY_RUN)

def alter_jenkinsfile_job_assignment(a_job_name_search, dry_run) {
    // change the existing job definitions
    //print "dry_run=${dry_run}"
    Jenkins.instance.getAllItems(jenkins.branch.MultiBranchProject.class).each{ it->
        it.getItems().each{ item ->
            if (item.toString().contains(a_job_name_search)) {
                //println "\tFound job " + item.fullName
                if (item.getDefinition() instanceof org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition) {
                    result = item.definition.scm.branches[0]
                        output += " [" + result + "]\tBuild Configuration definition is: [" + result + "]\t  " + it.fullName + "\n"
                }
            }
        }
    }
    // change the factory flow definitoin which made the jobs
    Jenkins.instance.getAllItems(org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject).each{ it->
        //println "${it.projectFactory} ${dry_run}"
        if (it.fullName.contains(a_job_name_search) 
                && it.projectFactory instanceof com.cloudbees.workflow.multibranch.CustomBranchProjectFactory ) {
                    if (it.projectFactory.definition instanceof org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition) {
                        result = it.projectFactory.definition.scm.branches[0]
                        output += " [" + result + "]\tBuild Configuration definition is: [" + result + "]\t  " + it.fullName + "\n"
            }
        } //if
    } //each
}

print output 


return null
