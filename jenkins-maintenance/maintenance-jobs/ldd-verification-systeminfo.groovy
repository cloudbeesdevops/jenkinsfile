#!/usr/bin/groovy

pipeline {
  agent any

  options {
    timeout(time: 10, unit: 'MINUTES')
    timestamps()
    retry(1)
    disableConcurrentBuilds()
    buildDiscarder(logRotator(numToKeepStr: '5')) 
}

  tools {
    maven 'CF_CLIENT_INSTALL'
  }

 triggers{ cron('30 */4 * * *') }

  parameters {
    booleanParam(name: 'LDD_POSTING', defaultValue: true, description: 'Sending results to LDD')
    choice(name: 'LDD_HOST', choices: 'c0009869.test.cloud.fedex.com' , description: '')
  }

  environment {
    https_proxy = 'http://internet.proxy.fedex.com:3128'
    http_proxy = 'http://internet.proxy.fedex.com:3128'
  }

  stages {
    stage('Setup') {
        steps {
          script {
              sshagent(['gitlab-user-for-master']) {
    withEnv([
        "GIT_SSH_COMMAND=ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"
        ]){
			withCredentials([
				usernamePassword(credentialsId: '9e863997-c0d9-4acb-8d54-99bd6c0b6ae1',
				passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME'),
			])
        {
            sh script: '''#!/bin/bash
#bash <(curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh) > /dev/null
#find . -maxdepth 3 -type d
rm -rf fdeploy-install-git*.sh
wget http://sefsmvn.ute.fedex.com/fdeploy-install-git.sh
sh fdeploy-install-git.sh "3534681:git@gitlab.prod.fedex.com:APP3534681/fxe-bw-deploy.git"
rm -fr sefs-verification
git clone git@gitlab.prod.fedex.com:5120346/sefs-verification.git
cd sefs-verification
cp -arf * ..
cd ..
pwd
mkdir -p tmp
set | grep LDD_
'''
    stash name: 'verification', includes: '**/*.py,**/*.sh,config*/**'
          }
        }
        }
          }
    }
    }
    stage('App Health') {
        steps {
            script {
            doQueueJobs(this,'system_info', "${env.LDD_HOST}")
            }
        }
    }
  } // stages
    post {
    success {
      step([$class: 'WsCleanup', cleanWhenFailure: true, cleanWhenNotBuilt: true, cleanWhenUnstable: true, notFailBuild: true])
    }
    }

} // pipeline

def doVerification(l,o,app, t) {
  sh 'pwd'
  sh returnStatus: true, script: "python envreadjson.py -d config.${o} -t ${app} -l ${l} -r ${t} -p -w -i -vvv"
}

def doQueueJobs(script, report_type, host) {

  if (host == null || host == "null" || host == "") {
      host = "c0009869.test.cloud.fedex.com"
  }
  runs = [:]
  def levels = ['L1','L2','L3','L4']
  def levelstring = levels.join(",")
  if (env.LDD_LEVEL != null && env.LDD_LEVEL != "") {
      levelstring = "${env.LDD_LEVEL}"
  } 
  def x = levelstring.indexOf('L1')
  def y = levelstring.indexOf('L2')
  def z = levelstring.indexOf('L3')
  def w = levelstring.indexOf('L4')
  script.echo("runninig for levels ${levels} and  L1 = ${x}")
  script.echo("runninig for levels ${levels} and  L2 = ${y}")
  script.echo("runninig for levels ${levels} and  L3 = ${z}")
  script.echo("runninig for levels ${levels} and  L4 = ${w}")
  def opcos = ['FXG','FXF','FXE','3534681']

    if (levelstring.indexOf('L1') >= 0) {
  runs["L1"] = {
    node {
        try {
            sh 'rm -rf `pwd`/*'
        unstash name: 'verification'
        for (o in opcos) {
            def types = ['BE','BW','JAVA']
                for (t in types) {
                    doVerification('L1',o, t, report_type)
                }
            }        
    } catch (exception) {
        print exception
    }
    } } }
    if (levelstring.indexOf('L2') >= 0) {
  runs["L2"] = {
    node {
        try {
            sh 'rm -rf `pwd`/*'
        unstash name: 'verification'
        for (o in opcos) {
            def types = ['BE','BW','JAVA']
                for (t in types) {
                    doVerification('L2',o, t, report_type)
                }
            }       
    } catch (exception) {
        print exception
    }
    }  } }

    if (levelstring.indexOf('L3') >=0) {
    runs["L3"] = {
    node {
        try {
            sh 'rm -rf `pwd`/*'
        unstash name: 'verification'
        for (o in opcos) {
            def types = ['BE','BW','JAVA']
                for (t in types) {
                    doVerification('L3',o, t, report_type)
                }
            }

        
    } catch (exception) {
        print exception
    }
    }
    }
    }

    if (levelstring.indexOf('L4') ) {
    runs["L4"] = {
    node {
        try {
            sh 'rm -rf `pwd`/*'
        unstash name: 'verification'
        for (o in opcos) {
            def types = ['BE','BW','JAVA']
                for (t in types) {
                    doVerification('L4',o, t, report_type)
                }
            }       
    } catch (exception) {
        print exception
    }
    }  }  }

    if (levelstring.indexOf('L5') >=0) {
    runs["L5"] = {
    node {
        try {
            sh 'rm -rf `pwd`/*'
        unstash name: 'verification'
        for (o in opcos) {
            def types = ['BE','BW','JAVA']
                for (t in types) {
                    doVerification('L5',o, t, report_type)
                }
            }
    } catch (exception) {
        print exception
    }
    }    } }

  parallel runs
  for (f in levels) {
        for (o in opcos) {
            sh "curl -s http://${host}:8090/sefs-dashboard/api/public/health/${o}/${f} > /dev/null"
        }
  }
}