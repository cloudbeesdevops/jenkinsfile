node ('uwb00078') {
sh returnStatus: false, script : '''#!/bin/bash 
whoami
df -khP
ssh -T -i /opt/fedex/jenkins/.ssh/id_dsa -o StrictHostKeyChecking=no jenkins@uwb00078.ute.fedex.com 'set -xv && find /opt/fedex/jenkins/cloudbees/workspace/* -mtime +2  | xargs -t -i% rm -fr % '
ssh -T -i /opt/fedex/jenkins/.ssh/id_dsa -o StrictHostKeyChecking=no jenkins@uwb00078.ute.fedex.com 'set -xv && find /opt/fedex/tibco/node/.npm/ -type d -maxdepth 0 | xargs -t -i% rm -fr % '
ssh -T -i /opt/fedex/jenkins/.ssh/id_dsa -o StrictHostKeyChecking=no jenkins@uwb00078.ute.fedex.com 'set -xv && find /opt/fedex/jenkins/.m2/repository/* -mtime +2 |  xargs -t -i% rm -fr % '
ssh -T -i /opt/fedex/jenkins/.ssh/id_dsa -o StrictHostKeyChecking=no jenkins@uwb00078.ute.fedex.com 'set -xv && find /opt/fedex/jenkins/cloudbees/workspace/*\\@* -type d -maxdepth 0 |  xargs -t -i% rm -fr % '
ssh -T -i /opt/fedex/jenkins/.ssh/id_dsa -o StrictHostKeyChecking=no jenkins@uwb00078.ute.fedex.com 'set -xv && find /opt/fedex/tibco/jenkins/cloudbees/workspace/*\\@* -type d -maxdepth 0 |  xargs -t -i% rm -fr % '
df -khP
'''
}
