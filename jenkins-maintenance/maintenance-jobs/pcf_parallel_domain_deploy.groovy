#!/usr/bin/groovy

pipeline {
  agent { node { label 'urh00614'} }
  

  options {
    timeout(time: 10, unit: 'MINUTES')
    timestamps()
    retry(1)
    disableConcurrentBuilds()
  }

  parameters {
    booleanParam(name: 'ITINERARY', defaultValue: false, description: 'please check if want to deploy domain')
    booleanParam(name: 'HANDLING', defaultValue: false, description: 'please check if want to deploy domain')
    booleanParam(name: 'TRIP', defaultValue: false, description: 'please check if want to deploy domain')
    booleanParam(name: 'SHIPMENT', defaultValue: false, description: 'please check if want to deploy domain')
    booleanParam(name: 'ORCHESTRATION', defaultValue: false, description: 'please check if want to deploy domain')
    booleanParam(name: 'WAYPOINT', defaultValue: false, description: 'please check if want to deploy domain')
    booleanParam(name: 'TASK', defaultValue: false, description: 'please check if want to deploy domain')
    choice ( name: 'LEVEL', choices: ['L1', 'L2','L2B','L3','L3-FXG','L4','L4-FXG','L5','L6','PROD','PROD-FXG','PROD-WTC-FXG'], description: 'LEVEL selction')
    choice ( name: 'RELEASE_BRANCH', choices: ['master', 'development'], description: 'Branch selction for RELEASE_MANIFEST')
  }
  

  stages {
    stage('Setup') {
        steps {
            script {
                list_of_jobs (this)
            }
          
        }
  }//stages
  }
} // pipeline

def list_of_jobs(script){
    runs = [:]
    def DOAMINS = ['ITINERARY','HANDLING','TRIP','SHIPMENT','ORCHESTRATION','WAYPOINT']
    def a = "${env.ITINERARY}"
    def b = "${env.HANDLING}"
    def c = "${env.TRIP}"
    def d = "${env.SHIPMENT}"
    def e = "${env.ORCHESTRATION}"
    def f = "${env.WAYPOINT}"
	script.echo("runninig for DOAMINS ${DOAMINS} and  ITINERARY = ${a}")
	script.echo("runninig for DOAMINS ${DOAMINS} and  HANDLING = ${b}")
	script.echo("runninig for DOAMINS ${DOAMINS} and  TRIP = ${c}")
	script.echo("runninig for DOAMINS ${DOAMINS} and  SHIPMENT = ${d}")
	script.echo("runninig for DOAMINS ${DOAMINS} and  ORCHESTRATION = ${e}")
	script.echo("runninig for DOAMINS ${DOAMINS} and  WAYPOINT = ${f}")
    
    if (a == "true") {
			  runs["ITINERARY"] = {
			    node {
			        try {
			            println "build a job of ITINERARY"
			            build job: 'domain_deploy_Itinerary/'+"{env.RELEASE_BRANCH}", parameters: [booleanParam(name: 'PERFORM_RELEASE_CUT', value: false), booleanParam(name: 'FORTIFY_SCAN', value: false), booleanParam(name: 'BY_PASS_AUTO_DEPLOY_GATE', value: false), booleanParam(name: 'ENABLE_GITLAB_INTEGRATION', value: false), string(name: 'JOB_TYPE', value: 'CD'), string(name: 'LEVEL', value: "${env.LEVEL}")]
			    } 
			        catch (exception) {
			        	print exception
			        }
			    
			    } 
			  } 
			}
    if (b == "true" ) {
		  runs["HANDLING"] = {
		      
		    node {
		        try {
		            println "build a job of HANDLING"
		            build job: 'domain_deploy_handling/'+"${env.RELEASE_BRANCH}", parameters: [booleanParam(name: 'PERFORM_RELEASE_CUT', value: false), booleanParam(name: 'FORTIFY_SCAN', value: false), booleanParam(name: 'noADG', value: false), booleanParam(name: 'ENABLE_GITLAB_INTEGRATION', value: false), string(name: 'JOB_TYPE', value: 'CD'), string(name: 'LEVEL', value: "${env.LEVEL}")]
		    } 
		        catch (exception) {
		        	print exception
		        }
		    
		    } 
		  } 
		}
    if (c == "true" ) {
		  runs["TRIP"] = {
		    node {
		        try {
		            println "build a job of TRIP"
		            build job: 'domain_deploy_trip/'+"${env.RELEASE_BRANCH}", parameters: [booleanParam(name: 'PERFORM_RELEASE_CUT', value: false), booleanParam(name: 'FORTIFY_SCAN', value: false), booleanParam(name: 'BY_PASS_AUTO_DEPLOY_GATE', value: false), booleanParam(name: 'ENABLE_GITLAB_INTEGRATION', value: false), string(name: 'JOB_TYPE', value: 'CD'), string(name: 'LEVEL', value: "${env.LEVEL}")]
		    } 
		        catch (exception) {
		        	print exception
		        }
		    
		    } 
		  } 
		}
    if (d == "true" ) {
		  runs["SHIPMENT"] = {
		    node {
		        try {
		            println "build a job of SHIPMENT"
		            build job: 'domain_deploy_shipment/'+"${env.RELEASE_BRANCH}", parameters: [booleanParam(name: 'PERFORM_RELEASE_CUT', value: false), booleanParam(name: 'FORTIFY_SCAN', value: false), booleanParam(name: 'noADG', value: false), booleanParam(name: 'ENABLE_GITLAB_INTEGRATION', value: false), string(name: 'JOB_TYPE', value: 'CD'), string(name: 'LEVEL', value: "${env.LEVEL}")]
		    } 
		        catch (exception) {
		        	print exception
		        }
		    
		    } 
		  } 
		}
    if (e == "true" ) {
		  runs["ORCHESTRATION"] = {
		    node {
		        try {
		            println "build a job of ORCHESTRATION"
		            build job: 'domain_deploy_orchestration/'+"${env.RELEASE_BRANCH}", parameters: [booleanParam(name: 'PERFORM_RELEASE_CUT', value: false), booleanParam(name: 'FORTIFY_SCAN', value: false), booleanParam(name: 'BY_PASS_AUTO_DEPLOY_GATE', value: false), booleanParam(name: 'ENABLE_GITLAB_INTEGRATION', value: false), string(name: 'JOB_TYPE', value: 'CD'), string(name: 'LEVEL', value: "${env.LEVEL}")]
		    } 
		        catch (exception) {
		        	print exception
		        }
		    
		    } 
		  } 
		}
    if (f == "true" ) {
			  runs["WAYPOINT"] = {
			    node {
			        try {
			            println "build a job of WAYPOINT"
			            build job: 'domain_deploy_waypoint/'+"${env.RELEASE_BRANCH}", parameters: [booleanParam(name: 'PERFORM_RELEASE_CUT', value: false), booleanParam(name: 'FORTIFY_SCAN', value: false), booleanParam(name: 'BY_PASS_AUTO_DEPLOY_GATE', value: false), booleanParam(name: 'ENABLE_GITLAB_INTEGRATION', value: false), string(name: 'JOB_TYPE', value: 'CD'), string(name: 'LEVEL', value: "${env.LEVEL}")]
			    } 
			        catch (exception) {
			        	print exception
			        }
			    
			    } 
			  } 
			}
    
}
parallel runs