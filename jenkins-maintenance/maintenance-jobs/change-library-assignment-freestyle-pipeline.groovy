import hudson.plugins.git.*

import jenkins.*
import jenkins.plugins.git.*
import jenkins.model.*
import java.lang.reflect.*
import org.jenkinsci.plugins.workflow.cps.*
import org.jenkinsci.plugins.workflow.libs.*
import org.jenkinsci.plugins.workflow.multibranch.*
import com.cloudbees.workflow.multibranch.*
//https://github.com/jenkinsci/workflow-cps-global-lib-plugin/blob/master/src/main/java/org/jenkinsci/plugins/workflow/libs/LibraryConfiguration.java
//https://github.com/jenkinsci/workflow-cps-global-lib-plugin/blob/master/src/main/java/org/jenkinsci/plugins/workflow/libs/SCMSourceRetriever.java
//https://github.com/jenkinsci/git-plugin/blob/master/src/main/java/jenkins/plugins/git/GitSCMSource.java


class SharedLibChange implements Serializable {

	def shared_library_version
	def shared_library_from
	def shared_library_name
	def shared_library_remote
	def shared_library_creds
	def shared_jenkinsfile
	def xtra_filter = ''
	//job_name_search = "FXG_CORE/JAVA"
	def job_name_search = ''
	def DRY_RUN = true
	def verbose = false

	public SharedLibChange(env) {
		shared_library_version = "${env.TO_VERSION}"
		shared_library_from = "${env.FROM_VERSION}"
		shared_library_name = "libz"
		shared_library_remote = "git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git"
		shared_library_creds = "gitlab-user-for-master"
		shared_jenkinsfile = "${env.JenkinsFileName}"
		//job_name_search = "6873"
		job_name_search = "${env.NAME_FILTER}"
		xtra_filter = "${env.XTRA_FILTER}"
		if (xtra_filter == 'null') {
			xtra_filter = ''
		}
		DRY_RUN = "${env.MAKE_CHANGES}" == 'true' ? false : true
	}
	def boolean skip(String name, String version) {
		def keep = !skipByNameAndXtraFilter(name) && !toVersionEquals(version)
		if (hasFromVersion()) {
			keep = keep && fromVersionEquals(version)
		}
		return !keep
	}
	def boolean skip_by_all(String name, String version, String jenkinsFile) {
		def _skip = this.skip(name, version)
		if (!_skip) {
			_skip = !fromJenkinsFileNotEquals(jenkinsFile)
		}
		if (verbose) {
			if (_skip) {
				println "🚫 skipping ${name} ${version} ${jenkinsFile}"
			}
		}
		return _skip
	}
	def boolean hasNameFilter() {
		return job_name_search != null && !job_name_search.isEmpty()
	}
	def boolean hasFromVersion() {
		return shared_library_from != null && !shared_library_from.isEmpty()
	}
	def boolean nameContains(String name) {
		return job_name_search != "" && name.contains(job_name_search)
	}
	def boolean toVersionEquals(String version) {
		return shared_library_version != "" && version == shared_library_version
	}
	def boolean fromVersionEquals(String version) {
		return shared_library_from != "" && version == shared_library_from
	}
	def boolean fromJenkinsFileNotEquals(String jenkinsFile) {
		return shared_jenkinsfile != "" && shared_jenkinsfile == jenkinsFile
	}
	def boolean hasExtraFilter() {
		return xtra_filter != null && !xtra_filter.isEmpty()
	}
	def boolean xtraFilterContains(String name) {
		if (hasExtraFilter()) {
			return xtra_filter != "" && name.contains(xtra_filter)
		}
		return true
	}
	def boolean auxFilterEquals(String name) {
		return name.contains('AUXILARY_TEAM')
	}
	def boolean isDryRun() {
		return DRY_RUN
	}
	def setVerboseOn() {
		verbose = true
	}
	def isVerbose() {
		return verbose
	}
	def String getLibraryRefSpec() {
		if (shared_library_version ==~ /^[0-9].*/) {
			// assume tags are starting with numbers
			//println "assuming numbers ${shared_libary_version}"
			return "refs/tags/" + shared_library_version
		} else {
			println "assuming branches ${shared_library_version}"
			return shared_library_version
		}
	}

	def print_skip(name) {
		println "-> ${skipByNameAndXtraFilter(name)} ${name} name=${nameContains(name)}, xtra=${xtraFilterContains(name)}, aux=${auxFilterEquals(name)}"

	}

	def boolean skipByNameAndXtraFilter(name) {
		def keep = nameContains(name)
		if (hasExtraFilter()) {
			keep = keep && xtraFilterContains(name)
		}
		keep = keep && !auxFilterEquals(name)
		if (verbose) {
			println (!keep ? "🚫 skipping" : "\u2705 processing") + " ${name} with name filter '${this.job_name_search}', xtra filter '${this.xtra_filter}'"
		}
		return !keep
	}
	
	def String toString() {
		return  "shared_library [name=${shared_library_name}, extrafilter=${xtra_filter} from=${shared_library_from}, to=${shared_library_version}, shared_jenkinsfile=${shared_jenkinsfile}], dry_run=${DRY_RUN}"

	}
}  //class


public void printAllMethods( obj ){
	if( !obj ){
		println( "Object is null\r\n" );
		return;
	}
	if( !obj.metaClass && obj.getClass() ){
		printAllMethods( obj.getClass() );
		return;
	}
	def str = "class ${obj.getClass().name} functions:\r\n";
	obj.metaClass.methods.name.unique().each{  str += it+"(); ";  }
	println "${str}\r\n";
}

class CpsFlowData implements Serializable {
	def version
	def library_name
	def projectFactory
	def scm
	def name
	def current_branch
	def definition
	def scriptpath
	
	
	public CpsFlowData(org.jenkinsci.plugins.workflow.job.WorkflowJob d, String _name) {
		projectFactory = d
		definition = d.definition
		name = _name
		scm = d.SCMs[0]
		if (scm != null && scm.branches != null) {
			current_branch = scm.branches[0]
		}
	}
	public CpsFlowData(org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject d, String _name) {
		this.projectFactory = d.projectFactory
		definition = this.projectFactory.definition
		name = _name
		scriptpath = (definition instanceof org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition) ? definition.getScript() : definition.getScriptPath()
		println definition.getClass()
	    if (definition instanceof org.jenkinsci.plugins.workflow.cps.CpsFlowDefinition) {
//	    	printAllMethods(this.projectFactory)
//	    	printAllMethods(definition)
	    } 
					scm = this.definition.scm
		
		if (scm.branches != null) {
			current_branch = scm.branches[0]
		}
	}

	public branches(SharedLibChange sl) {
		def branches = [new BranchSpec(sl.getLibraryRefSpec())]
		if (scm != null && scm.branches != null) {
			branches = scm.branches.collect{ branch ->
				new BranchSpec(sl.getLibraryRefSpec() )
			}
		}
		return branches
	}

	public remoteConfigs(sl) {
		def remoteConfigs = []
		if (scm == null) {
			println "NO Scm Defined"
		} else if (scm != null && scm.userRemoteConfigs == null) {
			remoteConfigs << new UserRemoteConfig("git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git", 'libz', sl.getLibraryRefSpec(), "gitlab-user-for-master")
		} else if (scm != null && scm.userRemoteConfigs.size() == 0) {
			remoteConfigs << new UserRemoteConfig("git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git",null,null, "gitlab-user-for-master")
		} else {
			scm.userRemoteConfigs.each{ urc ->
				if (urc.getUrl().contains("conexus")) {
					println "${urc.getUrl()} ref=${urc.getRefspec()} creds=${urc.getCredentialsId()}"
					remoteConfigs << new UserRemoteConfig("git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git", urc.name, urc.refspec, "gitlab-user-for-master")
				} else {
					remoteConfigs << urc
				}
			}
		}
		return remoteConfigs
	}



	public String toString() {
		return "CpsFlowData [${version}, ${library_name} ${scriptpath}]"
	}
}

/**
 * ------------------------------------------------------------------------------------------------------------
 */

def isEmpty(value) {
	return "" == value || 'null' == value
}

/**
 *  ----------- ----------- ----------- ----------- ----------- ----------- ----------- ----------- -----------
 */
def set_cps_flow_definition(cps, sl) {
	//def scm = item.definition.getScm()
	def library_version_spec = sl.getLibraryRefSpec()
	def newDef = null
	def remoteConfigs = cps.remoteConfigs(sl)
	def branches = cps.branches(sl)

	if (remoteConfigs && !sl.isDryRun()) {
		def newScm = new GitSCM(
			remoteConfigs,
			branches,
			cps.scm.doGenerateSubmoduleConfigurations,
			cps.scm.submoduleCfg,
			cps.scm.browser,
			cps.scm.gitTool,
			cps.scm.extensions
			)
		newDef = new CpsScmFlowDefinition(newScm,sl.shared_jenkinsfile)
		newDef.lightweight = true
	}
	return [cps.current_branch, sl.isDryRun() ? library_version_spec : newDef]
} // set_cps_flow_definition

/**
 *
 */
def alter_folder_library(it, libraryconfig, scm, url,sl) {
	if (sl.fromVersionEquals(libraryconfig.defaultVersion[0])) {
		println "\u2705 version is ${sl.shared_library_from} is a match"
	}
	if (!sl.toVersionEquals(libraryconfig.defaultVersion[0])) {
		if (!sl.isDryRun()) {
			oldVersion = libraryconfig.defaultVersion[0]
			LibraryConfiguration newLibraryConfig = new LibraryConfiguration(sl.shared_library_name, new SCMSourceRetriever(scm))
			newLibraryConfig.setDefaultVersion(sl.shared_library_version)
			newLibraryConfig.setImplicit(true)
			newLibraryConfig.setAllowVersionOverride(libraryconfig.allowVersionOverride)
			newLibraryConfig.setIncludeInChangesets(libraryconfig.includeInChangesets)
			List<LibraryConfiguration> newLibraryConfigs = new ArrayList<LibraryConfiguration>()
			newLibraryConfigs.add(newLibraryConfig)
			FolderLibraries fl = new FolderLibraries(newLibraryConfigs)
			if (!sl.isDryRun()) {
				it.properties.replace(fl)
				it.save()
			}
		}
		println "\u2705 [${oldVersion}] to [${sl.shared_library_version}] \tAdjusted version of ${it.fullName}"
	}
} // alter_folder_library


def alter_cps_flow_definition(item, sl) {
	def result = [null, null]
	CpsFlowData data = new CpsFlowData(item, item.fullName)
	result = set_cps_flow_definition(data, sl)
	if (sl.shared_library_from != '' && !sl.fromVersionEquals("${result[0]}")) {
		println "🚫 detected version [${sl.shared_library_from}]\t does not match [${result[0]}] skipping"
		return
	}
	if (sl.skip(item.toString(), "${result[0]}")) {
		println "🚫 skipping ${item.fullName} with ${result[0]} change job asssignment"
		return
	}

	if (result[1] != null) {
		if (!sl.toVersionEquals("${result[0]}")) {
			if (!sl.isDryRun()) {
				println item.getClass()
				if (item instanceof org.jenkinsci.plugins.workflow.job.WorkflowJob) {
					item.definition = result[1]
				} else {
					item.projectFactory =  new com.cloudbees.workflow.multibranch.CustomBranchProjectFactory(".build",result[1])
				}
				println "\u2705 [${result[0]}] \t to  [${sl.shared_library_version}]\tModified Build Configuration definition ${item.fullName}["
			}
			else {
				println "\u2705 [${result[0]}] \t to  [${sl.shared_library_version}]\twould be changed definition ${item.fullName}["

			}
		} else {
			println "🚫 [" + result[0] + "]\tskipping build Configuration definition is up-to-date: " + item.fullName + " as [" + result[0] + "]"
		}


	}
	else {
		println "\u2642 \2654 abort result is null"
	}
}

/**
 * ------------------------------------------------------------------------------------------------------------
 */
def alter_jenkinsfile_job_assignment(sl) {
	println "JOB ASSIGNMENTS"
	// change the existing job definitions
	Jenkins.instance.getAllItems(jenkins.branch.MultiBranchProject.class).each{ it->
		it.getItems().each{ item ->
			if (sl.skipByNameAndXtraFilter(item.fullName)) {
				return // skip
			}
			println item.fullName
			if (item.getDefinition() instanceof CpsScmFlowDefinition) {
				alter_cps_flow_definition(item, sl)
			}
		} // each
	} // each

	// change the factory flow definitoin which made the jobs
	Jenkins.instance.getAllItems(WorkflowMultiBranchProject).each{ it->
		//println "${it.projectFactory} ${dry_run}"
		if (it.projectFactory instanceof CustomBranchProjectFactory ) {
			if (sl.skipByNameAndXtraFilter(it.fullName)) {
				return // skip
			}
			if (it.projectFactory.definition instanceof CpsScmFlowDefinition) {
				alter_cps_flow_definition(it, sl,)
			}
			else {
//				def result = [null, null]
//				println it.fullName
//				printAllMethods(it)
//				printAllMethods(it.getSCMSource())
//				CpsFlowData data = new CpsFlowData(it, it.fullName)
//				result = set_cps_flow_definition(data, sl)
//				println it.fullName
//				// no definition found
//				// https://groups.google.com/forum/#!topic/job-dsl-plugin/tUe8pp0Gy44
//				if (!sl.isDryRun()) {
//					GitSCM newscm = new GitSCM([], [], false, [], null, null, [])
//					result = set_cps_flow_definition(it.projectFactory, it.fullName, newscm, sl)
//					it.projectFactory = new com.cloudbees.workflow.multibranch.CustomBranchProjectFactory(".build",result[1])
//				}
//				println "\u2705 \tCreated Job definition ${it.fullName} from ["+ result[0]+"] to  " + sl.shared_library_version
			}
		} //if
	} //each
} // alter_jenkinsfile_job_assignment


node('JAVA') {
	parameters {
		string(name: 'NAME_FILTER', defaultValue: '', description: 'Additional filter to trim the name filter.', trim: true)
		string(name: 'XTRA_FILTER', defaultValue: '', description: 'Additional filter to trim the name filter.', trim: true)
		string(name: 'FROM_VERSION', defaultValue: '', description: 'The version the job needs to have to upgrade to <b>TO_VERSION</b>.', trim: true)
		string(name: 'TO_VERSION', defaultValue: 'master', description: 'The version of the pipeline that jobs matching the filter get set to.', trim: true)
		choice(name: 'JenkinsFileName', choices: 'Jenkinsfile\nJenkinsfileTibco\nJenkinsfileSpringConfigSync' , description: 'Type of pipeline (Tibco is for BE/BW)')
		booleanParam name: 'MAKE_CHANGES', defaultValue: false, description: 'make changes if selected, default is dry run.'
		booleanParam name: 'VERBOSE', defaultValue: false, description: ''
	}

		stage('Setup') {
					// concept taken from https://stackoverflow.com/questions/45266075/can-this-multibranch-pipeline-groovy-snippet-for-the-jenkins-script-console-beop?rq=1
					// https://github.com/Praqma/JenkinsAsCodeReference/blob/master/dockerizeit/master/globalPipelineLibraries.groovy
					println params
					def sl = new SharedLibChange(params)
					if (isEmpty(sl.shared_library_version)) {
						throw new Exception("Must define valid TO_VERSION")
					}
					if (isEmpty(sl.job_name_search) && isEmpty(sl.shared_library_from)) {
						throw new Exception("Must define valid NAME_FILTER or FROM_VERSION")
					}
					if (params.VERBOSE == true) {
						sl.setVerboseOn()
					}

					println sl.toString()
					println "FOLDER ASSIGNMENTS"
					Jenkins.instance.getAllItems(com.cloudbees.hudson.plugins.folder.AbstractFolder).each{ iit->
						if (sl.skipByNameAndXtraFilter(iit.fullName)) {
							return
						}
						folder_libraries = iit.properties.findAll{ item -> item instanceof  org.jenkinsci.plugins.workflow.libs.FolderLibraries };
						if (folder_libraries.size() <= 0) {
							return
						}
						def libraryconfig = folder_libraries[0].libraries
						if (libraryconfig.retriever[0] != null) {
							def scm = libraryconfig.retriever[0].scm
							//println "\n\n\t\t lib config ${libraryconfig.defaultVersion} " + libraryconfig.name + " \n\n"
							if (sl.skip(iit.fullName, "${libraryconfig.defaultVersion[0]}")) {
								if (sl.verbose)
									println "🚫 skipping [${libraryconfig.defaultVersion[0]}]\t for ${iit.fullName} and search query '*${sl.job_name_search}*'"
								return
							}
							//sl.print_skip(iit.fullName)
							if (scm instanceof hudson.plugins.git.GitSCM) {
								url = scm.remoteRepositories[0].uris[0].host
								if (sl.verbose)
									println "GitSCM : [" + url + "]: "+ libraryconfig.defaultVersion[0] + "\t" + iit.fullName + "\n"
								if  (!sl.isDryRun()) {
									GitSCMSource newscm = new GitSCMSource(null, sl.shared_library_remote, sl.shared_library_creds, "*", "", false)
									alter_folder_library(iit, libraryconfig, newscm, url, sl)
								}

							} else
							if (scm instanceof jenkins.plugins.git.GitSCMSource) {
								if (sl.verbose)
									"GitSCMSource: [" +  scm.id + "]:" +  libraryconfig.defaultVersion + "\t" + iit.fullName + "\n"
								if  (!sl.isDryRun()) {
									alter_folder_library(iit, libraryconfig, scm, scm.remote, sl)
								}
							}
							else {
								if (sl.verbose)
									println "Non-GitScmSource: " + scm.userRemoteConfigs
							}
						} else {
							if (sl.verbose)
								println "Null-GitScmSource: for " + iit.fullName
						}
					}

					printed = false

					
					alter_jenkinsfile_job_assignment(sl)

					println "\n" + sl.DRY_RUN + "is dryrun ? " + sl.isDryRun( )+ "\n"
		} //stage

} //pipeline


