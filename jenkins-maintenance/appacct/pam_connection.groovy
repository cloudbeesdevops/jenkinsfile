package com.fedex.jenkins;

import hudson.model.*
import java.io.*;
import java.net.*;
import sun.misc.BASE64Encoder;
import groovy.json.JsonSlurper;
import java.util.regex.*;


def userName
def password
def cfPass
def pamURL
def inputLine
def PAM_ID
    
String  pcfDeploy(String pamID) {
    def gitURL =  env.GIT_URL
    def userName = 'jenkins_api_user-1459'
    def secrets = [[$class: 'VaultSecret', path: 'secret/APP6143/selfservice', secretValues: [
                [$class: 'VaultSecretValue', envVar: 'PAM_PASSWORD', vaultKey: 'pam']]]
        ]
    
    def configuration = [$class: 'VaultConfiguration',
                             vaultUrl: 'https://vault-l4-edcw.ute.fedex.com:8200',
                             vaultCredentialId: 'JENKINS_VAULT']
    wrap([$class: 'VaultBuildWrapper', configuration: configuration, vaultSecrets: secrets]) {
        password = PAM_PASSWORD
        }
    PAM_ID = pamID
  def url = new URL("https://pam-wtc-vip.prod.fedex.com/api.php/v1/passwords.json/"+PAM_ID+"?reason=Severity+3%3A+Pre-production+application+testing");

    HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod( "GET" );
    con.setRequestProperty("Content-Type", "application/json");
    byte[] encodedPassword = (userName + ":" + password).getBytes();
    BASE64Encoder encoder = new BASE64Encoder();
    con.setRequestProperty( "Authorization","Basic " + encoder.encode( encodedPassword ) );
    def getRC = con.getResponseCode();
    if(getRC.equals(200)) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
    inputLine = reader.readLine();
    reader.close();
    }
    def inputJSON = new JsonSlurper().parseText(inputLine);
    inputJSON.each { k, v ->
      if(k.equals('password')) {
          cfPass = v
      }
    }
          return cfPass
} 
