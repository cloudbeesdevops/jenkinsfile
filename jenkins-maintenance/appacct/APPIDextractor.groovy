package com.fedex.jenkins;

import java.util.regex.*;

class APPIDextractor {
    
    String appIDParser(def gitURL) {
        def app
        def regex = "APP([0-9]{1,})"
        def url =   gitURL
        print "url is:  ${url}"
        def compiledRegex = Pattern.compile(regex) 
        def regexMatches  = compiledRegex.matcher(url)
        if(regexMatches.find()) {
            app = regexMatches.group(0);
	    }  
        return app	
    }
       
}