import hudson.plugins.git.*
import jenkins.*
import jenkins.plugins.git.*
import jenkins.model.*
import java.lang.reflect.*
import org.jenkinsci.plugins.workflow.cps.*
import org.jenkinsci.plugins.workflow.libs.*

// concept taken from https://stackoverflow.com/questions/45266075/can-this-multibranch-pipeline-groovy-snippet-for-the-jenkins-script-console-beop?rq=1
// https://github.com/Praqma/JenkinsAsCodeReference/blob/master/dockerizeit/master/globalPipelineLibraries.groovy
shared_library_version = "5.0.5"
shared_library_name = "libz"
shared_library_remote = "git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git"
shared_library_creds = "gitlab-user-for-master"
shared_jenkinsfile = "./Jenkinsfile"
//job_name_search = "FXG_CORE/JAVA"
job_name_search = "3534"


DRY_RUN = null //"nulls" // make null to activate and commit changes.


Jenkins.instance.getAllItems(com.cloudbees.hudson.plugins.folder.AbstractFolder).each{ it->
  
  if (it.fullName.contains(job_name_search) && !it.fullName.contains("CICD")) {
    folder_libraries = it.properties.findAll{ item -> item instanceof  org.jenkinsci.plugins.workflow.libs.FolderLibraries };
      if (folder_libraries.size > 0) {
      //println "------ BEGIN ------"
        
        //https://github.com/jenkinsci/workflow-cps-global-lib-plugin/blob/master/src/main/java/org/jenkinsci/plugins/workflow/libs/LibraryConfiguration.java
        //https://github.com/jenkinsci/workflow-cps-global-lib-plugin/blob/master/src/main/java/org/jenkinsci/plugins/workflow/libs/SCMSourceRetriever.java
        //https://github.com/jenkinsci/git-plugin/blob/master/src/main/java/jenkins/plugins/git/GitSCMSource.java
        
        println it.fullName
        
        def libraryconfig = folder_libraries[0].libraries
        if (libraryconfig.retriever[0] != null) {
            
            def scm = libraryconfig.retriever[0].scm
            
            //println "SCMSource: "+  scm + " " + libraryconfig.defaultVersion
            if (scm instanceof hudson.plugins.git.GitSCM) {
              url = scm.remoteRepositories[0].uris[0].host
              println "GitSCM : " + url + " ? " + url.getClass()
              GitSCMSource newscm = new GitSCMSource(null, shared_library_remote, shared_library_creds, "*", "", false)
              alter_folder_library(it, libraryconfig, newscm, url)
              
            }else
              if (scm instanceof jenkins.plugins.git.GitSCMSource) {
                println "GitSCMSource: ["+  scm.id + "]:" + scm.remote + " " + libraryconfig.defaultVersion
                alter_folder_library(it, libraryconfig, scm, scm.remote)
              }
            else {
              println "Non-GitScmSource: " + scm.userRemoteConfigs
            }
            // println "------ END ------"
    	    println ""
        } else {
            println "Null-GitScmSource: for " + it.fullName 
            
        }
    }
  } 
}

printed = false


alter_jenkinsfile_job_assignment(job_name_search, DRY_RUN)

def alter_folder_library(it, libraryconfig, scm, url) {

 if (libraryconfig.defaultVersion[0] != shared_library_version) {
   	oldVersion = libraryconfig.defaultVersion[0]
     LibraryConfiguration newLibraryConfig = new LibraryConfiguration(shared_library_name, new SCMSourceRetriever(scm))
     newLibraryConfig.setDefaultVersion(shared_library_version)
     newLibraryConfig.setImplicit(true)
     newLibraryConfig.setAllowVersionOverride(libraryconfig.allowVersionOverride)
     newLibraryConfig.setIncludeInChangesets(libraryconfig.includeInChangesets)
     List<LibraryConfiguration> newLibraryConfigs = new ArrayList<LibraryConfiguration>()
     println "[${oldVersion}] to [${shared_library_version}] \tAdjusted version of ${it.fullName}"
   if (DRY_RUN == null) {
     newLibraryConfigs.add(newLibraryConfig)
     FolderLibraries fl = new FolderLibraries(newLibraryConfigs)
     it.properties.replace(fl)
     it.save()
   }
  } 
}



def alter_jenkinsfile_job_assignment(a_job_name_search, dry_run) {
    // change the existing job definitions
    //print "dry_run=${dry_run}"
    Jenkins.instance.getAllItems(jenkins.branch.MultiBranchProject.class).each{ it->
        it.getItems().each{ item ->
            if (item.toString().contains(a_job_name_search)) {
                //println "\tFound job " + item.fullName
                if (item.getDefinition() instanceof org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition) {
                    result = set_cps_flow_definition(item, item.fullName, item.definition.scm, dry_run)
                    if (result[1] != null){
                        item.definition = result[1]
                        println "["+ result[0]+"] to  [" + shared_library_version + "]\tModified Build Configuration definition ${item.fullName}[" 
                    }
                    else {
                        println " [" + result[0] + "]\tBuild Configuration definition is up-to-date: " + item.fullName + " as [" + result[0] + "]"
                    }
                }
            }
        }
    }
    // change the factory flow definitoin which made the jobs
    Jenkins.instance.getAllItems(org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject).each{ it->
        //println "${it.projectFactory} ${dry_run}"
        if (it.fullName.contains(a_job_name_search) 
                && it.projectFactory instanceof com.cloudbees.workflow.multibranch.CustomBranchProjectFactory ) {
                    if (it.projectFactory.definition instanceof org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition) {
                        result = set_cps_flow_definition(it.projectFactory, it.fullName, it.projectFactory.definition.scm, dry_run)
                        println "->" + result[1]
                        if (result[1] != null) {
                            it.projectFactory = new com.cloudbees.workflow.multibranch.CustomBranchProjectFactory(".build",result[1])
                            println "\tModified Job definition ${it.fullName} from ["+ result[0]+"] to  " + shared_library_version
                        }
                        else {
                            if (result[0] != shared_library_version) {
            					it.projectFactory = new com.cloudbees.workflow.multibranch.CustomBranchProjectFactory(".build",result[1])
                              println "\tJob definition need modification: " + it.fullName + " as [" + result[0] + "]"
                            } else {
                                println "\tJob definition is up-to-date: " + it.fullName + " as [" + result[0] + "]"
                            }
                        
                        }
                    }
                    else {
                        // no definition found
                        // https://groups.google.com/forum/#!topic/job-dsl-plugin/tUe8pp0Gy44
                        GitSCM newscm = new GitSCM(
                          [],
                          [],
                          false,
                          [],
                          null,
                          null,
                          [] )
                        result = set_cps_flow_definition(it.projectFactory, it.fullName, newscm, dry_run)
                        it.projectFactory = new com.cloudbees.workflow.multibranch.CustomBranchProjectFactory(".build",result[1])
                        println "\tCreated Job definition ${it.fullName} from ["+ result[0]+"] to  " + shared_library_version
                    }
        } //if
    } //each
}

def set_cps_flow_definition(item, fullName,scm, dry_run) {
    //def scm = item.definition.getScm()
    def remoteConfigs = []
    if (scm.userRemoteConfigs .size() == 0) {
        remoteConfigs << new UserRemoteConfig("git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git",null,null, "gitlab-user-for-master")
    }
  else {
  
    scm.userRemoteConfigs.each{
        urc ->
          if (urc.getUrl().contains("conexus")) {
            println "${urc.getUrl()} ref=${urc.getRefspec()} creds=${urc.getCredentialsId()}"
        remoteConfigs << new UserRemoteConfig("git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git", urc.name, urc.refspec, "gitlab-user-for-master")
          } else {
            remoteConfigs << urc
          }
    }
  }
    oldBranch = scm.branches[0]
    if (shared_library_version ==~ /^[0-9].*/) {
        // assume tags are starting with numbers
        //println "assuming numbers ${shared_libary_version}"
        library_version_spec = "refs/tags/" + shared_library_version
    } else {
        println "assuming branches ${shared_library_version}"
        library_version_spec = shared_library_version
    } 
    def branches = scm.branches.collect{
    branch ->
        new BranchSpec(library_version_spec )
    }
    if (remoteConfigs) { // && (!oldBranch.toString().endsWith(shared_library_version) || !item.definition.scriptPath.equals(shared_jenkinsfile))) {
        //println scm.doGenerateSubmoduleConfigurations + "  " + scm.submoduleCfg + ",  " + scm.browser + ", " + scm.gitTool + "," + scm.extensions
        def newScm = new GitSCM(
          remoteConfigs,
          branches,
          scm.doGenerateSubmoduleConfigurations,
          scm.submoduleCfg,
          scm.browser,
          scm.gitTool,
          scm.extensions
        )
        def newDef = new CpsScmFlowDefinition(newScm,shared_jenkinsfile)
        newDef.lightweight = true
        return [oldBranch,newDef]
    }
    return [oldBranch, null]
}

return null
