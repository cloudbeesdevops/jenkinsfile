import hudson.plugins.git.*
import jenkins.*
import jenkins.plugins.git.*
import jenkins.model.*
import java.lang.reflect.*
import org.jenkinsci.plugins.workflow.cps.*
import org.jenkinsci.plugins.workflow.libs.*

// concept taken from https://stackoverflow.com/questions/45266075/can-this-multibranch-pipeline-groovy-snippet-for-the-jenkins-script-console-beop?rq=1
// https://github.com/Praqma/JenkinsAsCodeReference/blob/master/dockerizeit/master/globalPipelineLibraries.groovy
shared_library_version = "2.0.2"
shared_library_name = "libz"
shared_library_remote = "git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git"
shared_library_creds = "gitlab-user-for-master"
shared_jenkinsfile = "./JenkinsfileCICD"
//job_name_search = "FXG_CORE/JAVA"
job_name_search = "SEFS_FXE-6269/FXE_DOMAIN"


DRY_RUN = "nulls" // make null to activate and commit changes.


printed = false


alter_jenkinsfile_job_assignment(job_name_search)



def alter_jenkinsfile_job_assignment(a_job_name_search) {

     // change the existing job definitions
    Jenkins.instance.getAllItems(jenkins.branch.MultiBranchProject.class).each{ it->
        it.getItems().each{ item ->
            if (item.toString().contains(a_job_name_search)) {
              println item.fullName
              hudson.model.Cause.UserIdCause cause = new hudson.model.Cause.UserIdCause()
              def causeAction = new hudson.model.CauseAction(cause) 
                 Jenkins.instance.queue.schedule(item,0,causeAction) 
            }
        }
    }
 
}




return null
