import hudson.plugins.git.*
import jenkins.*
import jenkins.plugins.git.*
import jenkins.model.*
import java.lang.reflect.*
import org.jenkinsci.plugins.workflow.cps.*
import org.jenkinsci.plugins.workflow.libs.*



DRY_RUN = "nulls" // make null to activate and commit changes.


array=[]

Jenkins.instance.getAllItems(org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject).each{ it->

  base = null
  if (it.sources[0].source instanceof jenkins.scm.impl.subversion.SubversionSCMSource) {
    base = it.sources[0].source.remoteBase
  } else if (it.sources[0].source instanceof jenkins.plugins.git.GitSCMSource) {
    base = it.sources[0].source.remote
  }
  if (base != null && !array.contains(base)) {
    array << base
  }
}

printed = false





return array
