import hudson.plugins.git.*
import jenkins.*
import jenkins.plugins.git.*
import jenkins.model.*
import java.lang.reflect.*
import org.jenkinsci.plugins.workflow.cps.*
import org.jenkinsci.plugins.workflow.libs.*

// concept taken from https://stackoverflow.com/questions/45266075/can-this-multibranch-pipeline-groovy-snippet-for-the-jenkins-script-console-beop?rq=1
// https://github.com/Praqma/JenkinsAsCodeReference/blob/master/dockerizeit/master/globalPipelineLibraries.groovy
shared_library_version = "3.0.0"
shared_library_name = "libz"
shared_library_remote = "git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git"
shared_library_creds = "gitlab-user-for-master"
Jenkins.instance.getAllItems(com.cloudbees.hudson.plugins.folder.AbstractFolder).each{ it->

    if (it.fullName.contains("SEFS_COMMON-6270") && !it.fullName.contains("CICD")) {
      folder_libraries = it.properties.findAll{ item -> item instanceof  org.jenkinsci.plugins.workflow.libs.FolderLibraries };
        if (folder_libraries.size > 0) {

          //https://github.com/jenkinsci/workflow-cps-global-lib-plugin/blob/master/src/main/java/org/jenkinsci/plugins/workflow/libs/LibraryConfiguration.java
          //https://github.com/jenkinsci/workflow-cps-global-lib-plugin/blob/master/src/main/java/org/jenkinsci/plugins/workflow/libs/SCMSourceRetriever.java
          //https://github.com/jenkinsci/git-plugin/blob/master/src/main/java/jenkins/plugins/git/GitSCMSource.java

          println it.fullName
          def libraryconfig = folder_libraries[0].libraries
          def scm = libraryconfig.retriever[0].scm
          
          println "SCMSource: "+  scm + " " + libraryconfig.defaultVersion
          if (scm instanceof hudson.plugins.git.GitSCM) {
            url = scm.remoteRepositories[0].uris[0].host
            println "GitSCM : " + url + " ? " + url.getClass()
            GitSCMSource newscm = new GitSCMSource(null, shared_library_remote, shared_library_creds, "*", "", false)
            alter_folder_library(it, libraryconfig, newscm, url)
            
          } else if (scm instanceof jenkins.plugins.git.GitSCMSource) {
               println "GitSCMSource: ["+  scm.id + "]:" + scm.remote + " " + libraryconfig.defaultVersion
               alter_folder_library(it, libraryconfig, scm, scm.remote)
          }
          else {
                println "Non-GitScmSource: " + scm.userRemoteConfigs
          }
        }
    }
}

def alter_folder_library(it, libraryconfig, scm, url) {
 if (!url.contains("conexus") && libraryconfig.defaultVersion[0] != shared_library_version) {
     oldVersion = libraryconfig.defaultVersion[0]
     LibraryConfiguration newLibraryConfig = new LibraryConfiguration(shared_library_name, new SCMSourceRetriever(scm))
     newLibraryConfig.setDefaultVersion(shared_library_version)
     newLibraryConfig.setImplicit(libraryconfig.implicit)
     newLibraryConfig.setAllowVersionOverride(libraryconfig.allowVersionOverride)
     newLibraryConfig.setIncludeInChangesets(libraryconfig.includeInChangesets)
     List<LibraryConfiguration> newLibraryConfigs = new ArrayList<LibraryConfiguration>()
     newLibraryConfigs.add(newLibraryConfig)
     //FolderLibraries fl = new FolderLibraries(newLibraryConfigs)
     //it.properties.replace(fl)
     println "Adjusted version of ${it.fullName} from [${oldVersion}] to [${shared_library_version}]"
     //it.save()
   }  
  
}

return null
