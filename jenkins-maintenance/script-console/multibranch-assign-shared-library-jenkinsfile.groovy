import hudson.plugins.git.*
  import jenkins.*
  import jenkins.model.*
  import java.lang.reflect.*
  import org.jenkinsci.plugins.workflow.cps.*

  Jenkins.instance.getAllItems(jenkins.branch.MultiBranchProject.class).each{ it->

    it.getItems().each{ item ->

      if (item.toString().contains("FXS_SEFS_DASHBOARD_PROBE_CICD")) {

        if (item.getDefinition() instanceof org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition) {
          def scm = item.getDefinition().getScm()
          def remoteConfigs = []
          scm.userRemoteConfigs.each{
            urc ->

              if (urc.getUrl().contains("conexus")) {
                println "${urc.getUrl()} ref=${urc.getRefspec()} creds=${urc.getCredentialsId()}"
          remoteConfigs << new UserRemoteConfig("git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git", urc.name, urc.refspec, "gitlab-user-for-master")
              }


          }
          def branches = scm.branches.collect{
            branch ->
              new BranchSpec("refs/tags/3.0.0")
          }
          if (remoteConfigs) {
            print remoteConfigs
            def newScm = new GitSCM(
              remoteConfigs,
              branches,
              scm.doGenerateSubmoduleConfigurations,
              scm.submoduleCfg,
              scm.browser,
              scm.gitTool,
              scm.extensions
            )
            def newDef = new CpsScmFlowDefinition(newScm,"./JenkinsfieCICD")
            newDef.lightweight = true
            item.definition = newDef
            //item.save()
            println "\nUpdated job ${item.fullName}"
          }

        }
      }
    }
  }




    return null
