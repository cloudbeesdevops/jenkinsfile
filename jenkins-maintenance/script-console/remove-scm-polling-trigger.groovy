import jenkins.*
import jenkins.model.*
import jenkins.triggers.*
import hudson.*
import husdon.triggers.*
import hudson.model.*
import java.time.*
import hudson.triggers.Trigger
import org.jenkinsci.plugins.workflow.job.properties.*

def now = ZonedDateTime.now();
def oneDayAgo = now.plusDays(-1);
def dryRun = true
def folder_name_pattern =  /.*ORCHEST.*/ //Folder name pattern, determines which folders to scan
def jenkinsFolders = Jenkins.instance.getAllItems()
jenkinsFolders.each{ item ->
  trigger(item,folder_name_pattern,dryRun)
}

def trigger(item, folder_name_pattern,dryRun) {
if (item instanceof com.cloudbees.hudson.plugins.folder.Folder) {
    for (i in item.items) {
      trigger(i,folder_name_pattern,dryRun)
    }
  }
  else {
    def matcher = (item.name ==~ folder_name_pattern)
    if (matcher) {
      if (item instanceof jenkins.branch.MultiBranchProject) {
        if (dryRun) {
         	jobs = item.getItems().each{ job ->
            printjobdetails(job)
          }
        }
        else {
          // do somthing
        }
      }
    }

  }
}


def printjobdetails(job){
  if (job instanceof org.jenkinsci.plugins.workflow.job.WorkflowJob) {
    println "${job} ${job.getSCMTrigger()} ${job.name}"
    if (job.getSCMTrigger() instanceof hudson.triggers.SCMTrigger) {
      removeTrigger(job)
    }
  }
}

def removeTrigger(job) {
  Trigger trigger = job.getSCMTrigger()
  PipelineTriggersJobProperty originalProp = job.getTriggersJobProperty();
  println originalProp
  Trigger old = originalProp.getTriggerForDescriptor(trigger.getDescriptor());
  if (old != null) {
    originalProp.removeTrigger(old)
    old.stop()
  }
  job.removeProperty(PipelineTriggersJobProperty.class);
}


return null // mute output
