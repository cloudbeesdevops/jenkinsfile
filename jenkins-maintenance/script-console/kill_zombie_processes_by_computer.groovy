server_name_pattern = /urh/
jenkins.model.Jenkins.instance.getComputers().each { computer ->
  if (computer.getName().find(server_name_pattern)) {
    println computer.getName()
    execList = computer.getExecutors()      
    for( exec in execList ) {
      busyState = exec.isBusy() ? ' busy' : ' idle'
      println '--' + exec.getDisplayName() + busyState + ' elapsed: ' + exec.getElapsedTime()
      // kill jobs that are running more than 1 hour
      if (exec.isBusy() && exec.getElapsedTime() > 3600000 ) {
        println '--    killed ' + exec.getDisplayName()
        exec.interrupt()
      }
    }
  }
}
