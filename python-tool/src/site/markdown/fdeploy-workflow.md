﻿

##fdeploy Workflow 

| Step | Description | 
|---|---|
| 1 | cli command issued  `fdeploy control -f run.json -l L1 -a stop` |
| 2 | loading deployment descriptor `run.json` | 
| 3 | download the `content` from nexus, in this case `share-cache=plugin.zip | 
| 4 | loading the target platform, in this case `bash_generator` | 
| 5 | generate the scripts for level `L1` for type `java` of the `bash_generator` platform |
| 6 | create the scripts from embedded code snippets based on the `stop` action. In this case `*.txt` files are the remote invocations, `master.sh` is the execution script for the deployer. The `sefs-fxe-java-cache.sh` will generate the `*.txt` files for each target. | 
| 7 | invoke the deployer `javaDeployer`|
| 8 | execute the `master.sh` which will invoke remotely with generated script contents for each host | 


```json
[
	{
		"id"          : "sefs-fxe-java-cache-service",
		"name_format" : "JAVA_CACHE_SERVICE_%0d",
		"platform"    : "bash_generator",
		"type"   : "java",
		"rtv"
		: [
			{ "value" : "8096", "name" : "HTTP_PORT" },
			{ "value" : "L1",   "name" : "SPRING_PROFILE" }
		],
		"levels" : [
			{
				"rtv"
				: [
					{ "value" : "8100", "name" : "HTTP_PORT" }
				],
				"level" : "L1",
				"targets"
				: ["sefscm@c0003476.test.cloud.fedex.com:/var/tmp/apps"]
			}
		],
		"content"
		: [
			{
				"gav" : "com.fedex.sefs.cache.plugins:share-cache-plugin:zip",
				"relativePath" : "apparchives",
				"saveArchiveName" : "share-plugin.zip"
			}
		]
	}
]

```

![Workflow fdeploy execution for bash_generator](./images/fdeploy-structure.jpg "Workflow fdeploy execution for bash_generator")


##fdeploy - base_generator - authorization model 

![fdeploy authorization model for bash_generator ](./images/fdeploy-authorization-model.jpg "fdeploy authorization model")