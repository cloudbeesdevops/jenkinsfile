Publishing the application details consist of levels of information:

| JSON Key | LDD Table.Field | Usage |
|-------|--------|----|
| artifactId | `FX_APPLICATION.ARTIFACT_ID` | The unique application identifier |
| version | `FX_PROCESS_QUERY.DEPLOYED_VERSION` | The deployed version in this level |
| opco | `FX_PROCESS_QUERY->OPCO->OPCO_ABBREV` `FX_APPLICATION->OPCO->OPCO_ABBREV` | The opco the process belongs to. |
| deployId | `FX_PROCESS_QUERY.PROCESS_NM` | The unique name of the process shown on the health views. | 
| type | `FX_PROCESS_QUERY.PROCESS_TYPE` | The type of process (FX_PROCESS_TYPE), used for the percentage graphs to show app distribution. |
| level | `FX_PROCESS_QUERY->LEVEL->FULL_NAME` | The level where the process runs. |
| jmxport | `FX_PROCESS_QUERY.JMX_PORT` | The port for connecting the performance monitor |
| queryCmd | `FX_PROCESS_QUERY.QUERY_CMD` | The search pattern for the ps footprint |
| queryHost | `FX_PROCESS_QUERY.QUERY_HOST` | The search pattern for the host | 
| host | `FX_PROCESS_QUERY->SF_HOST_ID->HOST_NM` | Linking FX_AGENT FK associated with the process, and used to auto register new machines like CloudVM's or physicals. |


This information is gathered from various sources:


```
{
    deployId: "fxg-pd-stop-service_1",
    version: "4.5.0-SNAPSHOT",
    queryCmd: "fxg-pd-stop-service",
    level: "L1",
    host: "fg2da02.test.cloud.fedex.com",
    opco: "FXG",
    type: "Java",
    queryHost: "c0005093",
    jmxport : "15351",
    artifactId: "fxg-pd-stop-service"
}
```

POSTING REST interface where the host is the LDD dashboard-web server.
```
http://irh00601.ute.fedex.com:8090/sefs-dashboard/api/public/query/register
```

The code for the registration / publish and deregistration / unpublish 
```
/dashboard-web/src/main/java/com/fedex/sefs/dashboard/business/QueryServiceImpl.java
/dashboard-web/src/main/java/com/fedex/sefs/dashboard/rest/QueryRestController.java
```

## fdeploy

The version field is retrieved from the `level/RELEASE_MANIFEST.MF`

The opco field is retrieved from the `config.[FXG]` directory where the JSON files are located.

```
[
	{
		"rtv"
		: [
// JMX_PORT field
			{ "name" : "JMX_PORT",    "value" : "15558" }
		],
		"levels"               : [
			{
// level field
				"level" : "L1",
				"targets"
				: [
// host field = fg1da02.test.cloud.fedex.com
"sefscm@fg1da02.test.cloud.fedex.com:/opt/fedex/sefs/fxg-gps-adapter"
				]
			},
		],
		"content"
		: [
			{
				  "gav"
// artifactId = fxg-gps-adapter
				: "com.fedex.ground.sefs.pd.adapter:fxg-gps-adapter:zip",
			}
		],
// deployId = will be artifactId + _1
		"id"          : "sefs-fxg-gps-adapter",
	}
]

```

## silvercontrol

For the silver control properties we have to load all the property files both application props and common props in the config parser.
After that we analyse the contents per application group in individualized extractor scripts:

``` 
/sefs_common/silvercontrol/src/main/python/ldd/tibcoASextractor.py
/sefs_common/silvercontrol/src/main/python/ldd/tibcoBEextractor.py
/sefs_common/silvercontrol/src/main/python/ldd/tibcoBWextractor.py
/sefs_common/silvercontrol/src/main/python/ldd/tibcoJAVAextractor.py
/sefs_common/silvercontrol/src/main/python/ldd/tibcoMiscextractor.py
```

### Generic Step

Find the `contentFiles` section in the config parser, get the `nexusArtifact` value as the `artifactId` as the application identifier.

### Base Steps BE & Java

Analyze the properties sections of in the config parser, associate the component names with stacks, and retrieve the assigned host. First find all the component names out of the `[component.'section'.'name']` sections. Then find the components key that associates the component with the stack. It is found by looking in the stacks in `[stacks.'section'.'name']` sections.
Get the association `COMPONENT:1:1:1:4` and find the allocation rule for `[....allocationrules.ResourcePreferenceHostName.properties]` section and get the `propertyValue` which has the hostname.

Here an example which should resolve into 

```
[component.StopCorePU1.common.contentfiles.StopPU1]
nexusGroup=com.fedex.ground.sefs
nexusArtifact=StopCoreFXG
nexusSaveArchiveName=StopCoreFXG.zip
relativePath=apparchives

[component.StopCorePU1.Cache1]
name=BE_StopCoreFXGPU_1

[stack.StopCorePU1.StopPU11]
name=BE_StopCoreFXGPUStack_1

[stack.StopCorePU1.StopPU11.schedules.Default1]
components=BE_StopCoreFXGPU_1;1;1;4

[stack.StopCorePU1.Default1.allocationrules.ResourcePreferenceHostname.properties]
propertyValue=urh00607

```

```
{
'host': 'srh00607.ute.fedex.com', 
'version': '3.0.7', 
'queryCmd': 'fxg_sefs_core_stopcorefxg_01', 
'level': 'L3', 
'opco': 'FXG', 
'queryHost': 'srh00607', 
'type': 'BE', 
'deployId': 'StopCorePU1.Cache1', 
'jmxport': '15611', 
'artifactId': 'StopCoreFXG'
}
```
 
### BE Specifics

Gather the `jmxport` and `queryCmd` values from the Runtime Variables.

```
[component.StopCorePU1.common.runtimevariables.StopCoreFXG_NAME]
value=fxg_StopFXGPU01
[component.StopCorePU1.common.runtimevariables.StopCoreFXG_JMX_PORT]
value=15611

```


### Java

#### ASMM

Hard code the artifactId to 'asmm'

####


### BW


