
###Numbering Strategy

The context of a release there are three elements for most SEFS.

 - The Binary; compiled code packaged in an archive;
 - The Configuration; items to configure the binary so we create a runtime;
 - The Deployment; items how and where to put the runtime (binary + configuration);

These elements can all be on their own release cycle.  A release cycle is development iteration of a piece of a release. The release is group of binaries with accompanied configurations. 

How do we indicate what is part of a release? We are applying [semantic versioning](http://semver.org/) principles.

A release generally has the goal of putting something into production (MVP). It contains one or more "applications". The application binary is being developed and tested from its incubation into a lower test level to higher levels of testing (functional, performance). This is the release cycle for the binary. The configuration of the binary also moves thru the levels, however from incubation level to actual production has specific values for some of these properties. This is the release cycle of the configuration.
Where do you register these applications (binary and configuration) as part of a release? The deployment configuration which contains a list of items to deploy and the deployment configuration itself is also on its own release cycle, since composition can undergo change from incubation to production. 

How does this tie with semantic versioning and MVP releases? 
A MVP has a list of applications (binaries and configurations) that implement functionality for the MVP. 

#####Example: RELEASE "SUPERHERO" for MVP August 2015

The release `SUPERHERO` is the `1.0 release` in semantic versioning, that is the starting point. And has changes in ClearanceFeed, SDS Activespaces and ShipmentCoreEngine. 
Here the state after the MVP 

|Level Test |  | QATest |  | PROD  | Application |
|---|----|---|---|---|--|--|
| L1/2/3 | `1.0.0-SNAPSHOT` | L3/4 | `1.0.23` | `1.0.23` | ClearanceFeed | binary |
| L1/2/3 | `1.0.0-SNAPSHOT` | L3/4 | `1.0.43` | `1.0.45` | ClearanceFeedConfig | configuation |
| L1/2/3 | `1.0.0-SNAPSHOT` | L3/4 | `1.0.3` | `1.0.3` | SDS | binary |
| L1/2/3 | `1.0.0-SNAPSHOT` | L3/4 | `1.0.5` | `1.0.6` | SDSConfig | configuation |
| L1/2/3 | `1.0.0-SNAPSHOT` | L3/4 | `1.0.13` | `1.0.13` | ShipmentCoreEngine | binary |
| L1/2/3 | `1.0.0-SNAPSHOT` | L3/4 | `1.0.16` | `1.0.17` | ShipmentCoreEngineConfig | configuation |
| L1/2/3 | `1.0.0.SUPERHERO-SNAPSHOT` | L3/4 | `1.0.9` | `1.0.10` | DeploymentConfig | configuration |

How to read this? `ClearanceFeed` had 23 iterations (release cuts) to get to the final product in the MVP for the **binaries**. And parallel with that 45 iterations to go to production for the **configuration**, and only 43 to go thru L3/. This means we had 2 sets of changes, adjustments to **configuration** items to go from L4 to PROD, HOWEVER the binary never changed and stated at 23 iterations.
In order to get the product thru the levels for `SUPERHERO` release we did 9 iterations of the deployment properties and added one for the production install.

The `SUPERHERO` release is defined by the DeploymentConfiguation and that area contains the RELEASE_MANIFEST hence that is where the grouping takes place and really is named release.

Hence the RELEASE Manifest is the recipe for the MVP, it defines what is in the release, what versions and how it gets prepared for deployment. 

What for the next release "CIDERMAN"?

#####Example: RELEASE "CIDERMAN" for MVP December 2015

The next MVP, `CIDERMAN` has changes for ShipmentCoreEngine only and it's a +1 minor version increment for any component that changes... ShipmentCoreEngine... 

|Level Test |  | QATest |  | PROD  | Application |
|---|----|---|---|---|--|--|
| L1/2/3 | `1.0.23` | L3/4 | `1.0.23` | `1.0.23` | ClearanceFeed | binary |
| L1/2/3 | `1.0.45` | L3/4 | `1.0.45` | `1.0.45` | ClearanceFeedConfig | configuation |
| L1/2/3 | `1.0.3` | L3/4 | `1.0.3` | `1.0.3` | SDS | binary |
| L1/2/3 | `1.0.6` | L3/4 | `1.0.6` | `1.0.6` | SDSConfig | configuation |
| L1/2/3 | `1.1.0-SNAPSHOT` | L3/4 | `1.1.3` | `1.1.3` | ShipmentCoreEngine | binary |
| L1/2/3 | `1.1.0-SNAPSHOT` | L3/4 | `1.1.3` | `1.1.6` | ShipmentCoreEngineConfig | configuation |
| L1/2/3 | `1.1.0.CIDERMAN-SNAPSHOT` | L3/4 | `1.1.2.CIDERMAN` | `1.13.CIDERMAN` | DeploymentConfig | configuration |


Conclusion:

Every MVP / RELEASE will have a deployment configuration that contains the contents of that release and that configuration will be named CIDERMAN and versioned minor + 1. 
The binaries and related configurations that change will also have an increment of +1 on the minor version.  Binaries and configurations that DO NOT change will become the latest version that is in PROD.

The idea is that major versions of deployment configs would change if there are breaking changes or architectural changes. The same applies for the binary+configuration pairs however they do not have to change at the same time. 




----------


   



###RELEASE MANAGEMENT

As a release train begins to produce more components on different release cycles there is a need to come together at deployment time from a release management perspective.

For example with FXG we now have 2.3.0 elements but also new domain elements that are on 1.0.0 and published from git. However together they form a release. To keep track of what is in a release and what version we list the items by artifactId in the RELEASE_MANIFEST.MF for each level.

    $ find . | grep REL
    ./L1/RELEASE_MANIFEST.MF
    ./L2/RELEASE_MANIFEST.MF
    ./L3/RELEASE_MANIFEST.MF

Lets take the release "SUPERHERO" mentioned earlier. 

In **L1** we have mostly if not all snapshots so our list would look like this 

    [RELEASE_MANIFEST]
    ClearanceFeed=1.0.0-SNAPSHOT
    ClearanceFeedConfig=1.0.0-SNAPSHOT
    SDS=1.0.0-SNAPSHOT
    SDSConfig=1.0.0-SNAPSHOT
    ShipmentCoreEngine=1.0.0-SNAPSHOT
    ShipmentCoreEngineConfig=1.0.0-SNAPSHOT

Lets say in **L4** we are publishing release cuts (no snapshots) the manifest would look like this, a mixture of component versions

    [RELEASE_MANIFEST]
    ClearanceFeed=1.0.23
    ClearanceFeedConfig=1.0.43 <-----
    SDS=1.0.3
    SDSConfig=1.0.5 <-----
    ShipmentCoreEngine=1.0.16 <----
    ShipmentCoreEngineConfig=1.0.9 <----

The **PROD** manifest has this mixture of component versions

    [RELEASE_MANIFEST]
    ClearanceFeed=1.0.23
    ClearanceFeedConfig=1.0.45 <-----
    SDS=1.0.3
    SDSConfig=1.0.6 <-----
    ShipmentCoreEngine=1.0.17 <----
    ShipmentCoreEngineConfig=1.0.10 <---


Now during the life of a release we might have multiple release cuts… silver control will automatically resolve the version to the latest release that starts with the version number that is in the Manifest… 

For example BizmaticsAdapter has released versions listed in [the maven-metadata.xml for the component (click to see the list)](http://sefsmvn.ute.fedex.com:9999/nexus/content/repositories/releases/com/fedex/ground/sefs/BizmaticsAdapter/) Next if we set the version number for BizmaticsAdapter to `2.3` for the L2 `RELEASE_MANIFEST.MF` silvercontrol it will find the last released version of `2.3.*` … and reports 

`Redefined version for BizmaticsAdapter > 2.3.4`

If we want to be more specific and put down `2.3.1` it would be similar but then with three digits...

So in the silver control logs you will find this stating that it resolve to `2.3.1` 

    NEXUS - Metadata: http://sefsmvn.ute.fedex.com:9999/nexus/content/repositories/releases/com/fedex/ground/sefs/BizmaticsAdapter/maven-metadata.xml
    Redefined version for BizmaticsAdapter > 2.3.1
    NEXUS - Pulling: http://sefsmvn.ute.fedex.com:9999/nexus/service/local/artifact/maven/redirect?r=releases&g=com.fedex.ground.sefs&a=BizmaticsAdapter&v=2.3.1&p=zip
    NEXUS - Discovered archive: http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repositories/releases/content/com/fedex/ground/sefs/BizmaticsAdapter/2.3.1/BizmaticsAdapter-2.3.1.4.zip

And as a summary 

    =========================
    NEXUS: ReferenceDataTransformer-2.3.1.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/ReferenceDataTransformer.zip
    
    NEXUS: fxg-bizmatics-adapter-1.0.0-20170331.141737-25.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/fxg-bizmatics-adapter.zip
    
    NEXUS: ShipmentGatewayFXG-2.3.1.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/ShipmentGatewayFXG.zip
    
    NEXUS: fxg-pd-discovery-1.0.0-20170329.191544-72.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/fxg-pd-discovery.zip
    
    NEXUS: RouteStopGatewayFXG-2.3.1.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/RouteStopGatewayFXG.zip
    
    NEXUS: fxg-barcode-service-1.0.0-20170329.194321-11.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/fxg-barcode-service.zip
    
    NEXUS: fxg-pd-proxy-1.0.0-20170329.191838-52.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/fxg-pd-proxy.zip
    
    NEXUS: ShipmentCoreFXG-2.3.1.4.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/ShipmentCoreFXG.zip
      extracted file: /home/ak751818/silvercontrol/tmp/tmptsjPSz/ShipmentCoreFXG.ear
    
    NEXUS: sefs_asProxy-2.3.1.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/sefs_asProxy.zip
    
    NEXUS: fxg-dashboard-adapter-1.0.0-20170331.141621-25.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/fxg-dashboard-adapter.zip
    
    NEXUS: PDDomainViewFXG-2.3.1.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/PDDomainViewFXG.zip
      extracted file: /home/ak751818/silvercontrol/tmp/tmptsjPSz/PDDomainViewFXG.ear
    
    NEXUS: fxg-pd-hystrixdashboard-1.0.0-20170329.191744-49.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/fxg-pd-hystrixdashboard.zip
    
    NEXUS: fxg-nfs-adapter-1.0.0-20170331.141319-25.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/fxg-nfs-adapter.zip
    
    NEXUS: SDSActiveSpace-2.3.1.4.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/SDSActiveSpace.zip
    
    NEXUS: StopCoreFXG-2.3.1.4.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/StopCoreFXG.zip
      extracted file: /home/ak751818/silvercontrol/tmp/tmptsjPSz/StopCoreFXG.ear
    
    NEXUS: fxg-gps-adapter-1.0.0-20170331.141451-25.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/fxg-gps-adapter.zip
    
    NEXUS: ReferenceDataEventGateway-2.3.1.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/ReferenceDataEventGateway.zip
    
    NEXUS: BizmaticsAdapter-2.3.1.zip
      saved as /home/ak751818/silvercontrol/tmp/tmptsjPSz/BizmaticsAdapter.zip
    
    =========================
    
    
    Finish = 2017-03-31T19:25:40.859888
    
    Total runtime = 505 seconds
    
    EXIT 0
    
    [me@uwb00078 silvercontrol]$

