﻿[Java](fdeploy-deployment-descriptor.html) | [TIBCO BW](fdeploy-bw-descriptor.html) | 


##Deployment Descriptor - JSON

|  Label | Type | Description | 
|--|--|--|
| `id` | string | unique identified of the component |
| `platform` | string | indicator for dynamically modeling the fdeploy output. For example platform `bash_generator` will dynamically generate bash scripts for each of the focus area's like test, deploy and control. Currently `bash_generator` is the most versed implementation, however we also have a `silvercontrol` generator that would generate silver control properties. | 
| `type` | string | the generated type of component. The scripts for java might be different from AppManage scripts with this identifier you can switch targeted type in the platform of choice. | 
| `rtv` | list | the runtime variables as a list of name value pairs. These `rtv`'s will get exported as part of the script generation. There are global `rtv` and they can be on each level, both will be merged. Global `rtv`'s will be overwriten when the same `name` is declared in the levels `rtv` section. |
| `levels` | list | the list of levels with `rtv`, `level` and `targets` section.
| `targets` | url | the url notation of a target with format `user@host:path?param=value&param2=value2`|
| `content` | list | the list of artifacts being deployed to the targets. |
| `gav` | string | the notation for specifying the group, artifact, packaging and version to be looked up in the binary repository | 
| `saveArchiveName` | string | the name of the archive after being downloaded | 

###Target Specification

`user@host:path?param2=value2`

| Entity | Type | Description |
|--|--|--|
| `user` | the `user` of the installing user on the remote target |
| `host` | the `host` name of the remote target |
| `path` | the installation `path` where the root of this application resides. The fdeploy implementation for `bash_generator` will use this path to create the `[root]/[version]/[archive contents]` | 
| `uri` | additional `name`, `value` pairs to steer specific execution (not implemented yet) |


Example of `path` being `/opt/fedex/sefs/share-cache-plugin` the application layout underneath would look like this:


```
$ ls -R /opt/fedex/sefs/share-cache-plugin
   |-----share-cache-plugin
   |-------0.0.1-SNAPSHOT
   |---------lib
   |---------scripts
   |-------current->0.0.1-SNAPSHOT
```   

> NOTE: `user` can be overwritten with the `--user|-u` commandline
> option enabling user switching. `path` can be overwritten with the
> `--path|-p` commandline option enabling diverting the installation to
> a different area
> Hence if I wanted to install under `/var/tmp/apps` I would just use `-p /var/tmp/apps`

###GAV Specification

The GAV are the Maven coordinates of an artifact. These are Group (groupId), Artifact (artifactId), Version (version), Packaging (packaging), and Classifier (classifier)
For fdeploy you have two syntax options. 

The hardcoded version, where `2.4.5` indicates the artifact version. Note: fdeploy will warn you in the execution.
```
com.fedex.ground.sefs:pd-domain:2.4.5:zip
```

The `no version` specified will trigger the version resolving from the `RELEASE_MANIFEST.MF`. When the `pd-domain=2.4.5` fdeploy will download the 2.4.5 version of the pd-domain
```
com.fedex.ground.sefs:pd-domain:zip
```


###JSON Example
```json
[
	{
		"id"          : "sefs-fxe-java-cache-service",
		"name_format" : "JAVA_CACHE_SERVICE_%0d",
		"platform"    : "bash_generator",
		"type"   : "java",
		"rtv"
		: [
			{ "value" : "8096", "name" : "HTTP_PORT" },
			{ "value" : "L1",   "name" : "SPRING_PROFILE" }
		],
		"levels" : [
			{
				"rtv"
				: [
					{ "value" : "8100", "name" : "HTTP_PORT" }
				],
				"level" : "L1",
				"targets"
				: ["sefscm@c0003476.test.cloud.fedex.com:/var/tmp/apps"]
			}
		],
		"content"
		: [
			{
				"gav" : "com.fedex.sefs.cache.plugins:share-cache-plugin:zip",
				"relativePath" : "apparchives",
				"saveArchiveName" : "share-plugin.zip"
			}
		]
	}
]

```


> Written with [StackEdit](https://stackedit.io/).