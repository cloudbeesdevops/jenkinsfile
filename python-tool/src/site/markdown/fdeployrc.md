The `$HOME/.fdeployrc` file contains information used by the automatic feature of the fdeploy commands and it controls how a **fdeploy** runs. . It is a hidden file in a user's home directory and must be owned either by the user executing the command or by the root user.

The `$HOME/.fdeployrc` is the default location for the run control file for auto-discovery. The user can also supply any rc file with the `-rc` option on fdeploy.

```
./fdeploy.py -rc mydeploy.rc test -a ssh -l L1 -f my.json
``` 

### Contents

The contents of the fdeployrc must start with the section header `[DEFAULTS]` followed by key value pairs. Normally alternate identities, repositories can be specified.

Example:

```
[DEFAULTS]
test_identity=~/.ssh/fxg_dev_fdeployer_dsa
dev_identity=~/.ssh/fxg_dev_fdeployer_dsa
nexusURL=https://nexus.prod.cloud.fedex.com:8443
snapshotsRepo=fxg-snapshots
releasesRepo=fxg-releases
cache=True
logLevel=INFO
ldd_url=http://srh00601.ute.fedex.com:8090/sefs-dashboard
```

