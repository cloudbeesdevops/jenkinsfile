##Linux##

###Fdeploy
 
| [CLI Synyax  ](fdeploy-index.html) |
| [Deployment Descriptor ](fdeploy-deployment-descriptor.html) |
| [Workflow step by step ](fdeploy-workflow.html) |
| [Nano tutorial how to use in 2 minutes](fdeploy-installation.html) |

 
####Installation 
 
Requirements: python2.6+, Linux, for example uwb00078.ute.fedex.com (not tested on Windows yet) | |
 

```    
source <(curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh)
```

###SilverControl

 Requirements: python2.6+, Linux, for example uwb00078.ute.fedex.com (not tested on Windows yet) | |
 

```    
source <(curl -s http://sefsmvn.ute.fedex.com/silvercontrol-install.sh)
```


For authentication setup do the following: put the broker passwords in a file called `.sc` in your home directory and populate the broker sections with passwords.

```
$ vi $HOME/.sc

[broker.urh00218]
password=NoPass#01
    
[broker.urh00172]
password=test123

$ chmod 600 $HOME/.sc
```

----------

###RELEASE_MANIFEST

[The release manifest rationel](releaseManifest.html) mini overview.
