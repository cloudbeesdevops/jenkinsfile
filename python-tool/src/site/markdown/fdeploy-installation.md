﻿##Installation##

Requirements: 

 - python2.6+, Linux, for example uwb00078.ute.fedex.com (not tested on Windows yet)
 - installed private key for performing the right operations for levels 
 
 
| Level_______ | Keyname  | Users / Availability |
|---|---|---|
| `L1 to L2` | `dev_deploy_user_dsa` | developer access, which is provided with any fdeploy installation | 
| `L1 to L6` | `test_deploy_user_dsa` | access to all test levels, which has to be requested from the SEFS System Team | 
| `PROD` | `prod_deploy_user_dsa` | access to PROD level ONLY, which is reserved for RELY and selected members of System Team ONLY |  


```    
source <(curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh)
```

When running `pull_fdeploy.sh` you will be prompted to enter versions, generally you just enter thru them for the lower levels. 
Users are also able to enter a version number as an argument, for example I would like to use `5.10.13.UGG` which translated into the `5.10.13` version of the `U-Go-Girl` release.

`./pull_fdeploy.sh 5.10.13.UGG`

 Generally the latter will be used in combination with a support ticket OR a PSDM for production installs. 

Example of just entering thru the versions giving you the latest and greatest snapshot wise... .

```
[ak751818@uwb00078 ~]$ ./pull_fdeploy.sh
NOTE: [if ends with SNAPSHOT the snapshots repository will be used]

 Pull F-Deploy code version:

Pulling F-Deploy Code from snapshots with version LATEST

 Pull FXE F-Deploy properties version:

Pulling F-Deploy FXE Properties from snapshots with version LATEST

 Pull FXG F-Deploy properties version:

Pulling F-Deploy FXG Properties from snapshots with version LATEST

 Pull FXF F-Deploy properties version:

Pulling F-Deploy FXF Properties from snapshots with version LATEST

 Pull FXS F-Deploy properties version:

Pulling F-Deploy FXS Properties from snapshots with version LATEST

total 768
drwxr-xr-x 10 ak751818 fxusers    560 Sep 28 14:43 ./
drwxr-xr-x  4 ak751818 fxusers     80 Sep 28 14:43 ../
drwxr-xr-x 10 ak751818 fxusers    220 Sep 28 14:43 config.FXE/
drwxr-xr-x  9 ak751818 fxusers    280 Sep 28 14:43 config.FXS/
-rw-r--r--  1 ak751818 fxusers    668 Sep 28 14:35 dev_deploy_user_dsa
drwxr-xr-x  5 ak751818 fxusers    200 Sep 28 14:43 fdeploy/
-rw-r--r--  1 ak751818 fxusers  16706 Sep 27 17:37 fdeploy_FXE_CORE-5.10.0.UGG-20170927.173753-63.zip
-rw-r--r--  1 ak751818 fxusers  12623 Sep 27 23:11 fdeploy_FXS-1.7.0-20170927.231151-11.zip
-rwxr-xr-x  1 ak751818 fxusers  12363 Sep 28 14:35 fdeploy.py*
drwxr-xr-x  2 ak751818 fxusers    260 Sep 28 14:43 fedex/
-rwxr-xr-x  1 ak751818 fxusers   3051 Sep 28 14:35 pull_fdeploy.sh*
-rwxr-xr-x  1 ak751818 fxusers   4089 Sep 28 14:35 pull_silvercontrol.sh*
-rw-r--r--  1 ak751818 fxusers 551809 Sep 28 14:36 sefs_silverControl-6.0.2-20170928.143600-22.zip
-rwxr-xr-x  1 ak751818 fxusers   9648 Sep 28 14:35 silverControlFXE.sh*
-rwxr-xr-x  1 ak751818 fxusers   9537 Sep 28 14:35 silverControlFXF.sh*
-rwxr-xr-x  1 ak751818 fxusers   8921 Sep 28 14:35 silverControlFXG.sh*
[ak751818@uwb00078 ~]$
```


###Action Matrix

| Command | Action | Description |
|----|----|----|
| deploy | stage | Uploading and Extracting Application Archive |
| deploy | relink | Link the current application version to deployed version |
| deploy | install | `deploy:stage` followed by `deploy:relink` |
| deploy | deploy | `control:stop`, `deploy:install`, `control:start` |



###Basic Usage Examples

Possible usage for an application in L1.

```

  982  cd fdeploy
  
  # checking ssh trust between the build host and 
  984  python fdeploy.py -vvv deploy -a install   -l L1 -f config.FXS/fxs-probe-client-pddv.json
  
 # check current status 
  985  python fdeploy.py control -a status  -l L1 -f config.FXS/fxs-probe-client-pddv.json
  
  # install : upload , unpack and relink the application
  951  python fdeploy.py  deploy -a install   -l L1 -f config.FXS/fxs-probe-client-pddv.json
  
  # start the process
  952  python fdeploy.py control -a start  -l L1 -f config.FXS/fxs-probe-client-pddv.json
    
  # check status
  988  python fdeploy.py control -a status  -l L1 -f config.FXS/fxs-probe-client-pddv.json
  
```





