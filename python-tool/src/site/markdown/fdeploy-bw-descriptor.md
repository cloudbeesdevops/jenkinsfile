
[Java](fdeploy-deployment-descriptor.html) | [TIBCO BW](fdeploy-bw-descriptor.html) | 


#Deployment Descriptor TIBCO BW - JSON


The anatomy of the TIBCO BusinessWorks deployment descriptor for fdeploy.

## Required RTV's 

| RTV Name | Description | Example | 
|----------|-------------|--------|
| JMX_PORT | The jmx port for external management | `15034` |
| TRA_HOME | The TIBCO tra installation on the admin machine | `/opt/tibco/tra/5.9/bin` |
| DOMAIN_NAME | The full domain name to where we publish applications | `FXG_${level}_BW_ADMIN` |
| PROCESS_NAME | The name of the process for tracking purposes | `COEFeed-SOEFeed`  | 
| ADMIN_TARGET_URL | The url of the host that the admin is running and path where our archive gets extracted | `tibco@drh57000.ute.fedex.com: /var/fedex/tibco/apps/my-app` | 

## Target URL Syntax

The target url syntax is different from the normal `bash_generator` url notation. 

`[TLM_NAME]@[tlm_host_name]`

Where the `TLM_NAME` is the logical name where the application is running and `tlm_host_name` is the host where the logical machine with `TLM_NAME` runs.

Example:
```
FX_VNDR_BROKER_TLM_1@urh00110.ground.fedex.com
```

## Sample Descriptor 

The descriptor will:
* upload the `COEFeedGatewayFXG-3.0.0-SNAPSHOT.zip` to the `ADMIN_TARGET_URL` location `/var/tmp/tibcosilver` as `tibcosilver` on host `urh00600.ute.fedex.com` and saved as `COEFeedGatewayFXG.zip`. 
* extract the `COEFeedGatewayFXG.zip` at `/opt/tibco/silver/persistentdata/FXG/apps/COEFeedGatewayFXG`
* when invoking the `install` the application will be deployed to `FXG_L1_FXG_BW_TLM_2` that runs on `urh00603.ute.fedex.com`
* the status script will validate that `COEFeed-SOEFeed` is running on jmx port `15552` on host `urh00603.ute.fedex.com`.



```
[{
   "type": "tibco_bw",
   "rtv": [{
         "name": "JMX_PORT",
         "value": "15552"
      },
      {
         "name": "TRA_HOME",
         "value": " /opt/tibco/silver/persistentdata/FXG_${level}_BW_ADMIN/tibco/tra/5.9"
      },
      {
         "name": "DOMAIN_NAME",
         "value": "FXG_${level}_BW_ADMIN"
      },
      {
         "name": "PROCESS_NAME",
         "value": "COEFeed-SOEFeed"
      },
      {
         "name": "DOMAIN_NAME",
         "value": "FXG_${level}_BW_ADMIN"
      }
   ],
   "includeBaseDirectory": "true",
   "relink": "true",
   "overwriteInstall": "false",
   "levels": [{
         "level": "L1",
         "rtv" : [
            {
               "name": "ADMIN_TARGET_URL",
               "value": "tibcosilver@urh00600.ute.fedex.com:/opt/tibco/silver/persistentdata/FXG/apps/COEFeedGatewayFXG"
            }
         ],
         "targets": [
            "FXG_L1_FXG_BW_TLM_2@urh00603.ute.fedex.com"
         ]
      }
   ],
   "content": [{
      "gav": "com.fedex.ground.sefs:COEFeedGatewayFXG:3.0.0-SNAPSHOT:zip",
      "relativePath": "apparchives",
      "saveArchiveName": "COEFeedGatewayFXG.zip"
   }],
   "id": "sefs-COEFeedGatewayFXG",
   "name_format": "JAVA_FXG_COEFeedGatewayFXG_%0d",
   "platform": "bash_generator"
}]
```