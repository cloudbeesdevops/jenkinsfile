
##fdeploy 

----------

[CLI Synyax  ](fdeploy-index.html)

----------

[Java - Deployment Descriptor ](fdeploy-deployment-descriptor.html)
[TIBCO BW - Deployment Descriptor ](fdeploy-bw-descriptor.html)

----------

[Workflow step by step ](fdeploy-workflow.html)

----------

[Nano tutorial how to use in 2 minutes](fdeploy-installation.html)

[fdeploy Run Control (rc-file)](fdeployrc.html)

----------

[Publishing to LDD](publish-to-ldd.html)
