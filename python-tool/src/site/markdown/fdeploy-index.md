
##fdeploy - executions

The executions are split into several groups. #1 is to make sure commands are not accidentally executed. #2 the ability to give certain groups access to certain command groups.

```
control group:
	control-status
	control-stop
	control-start
	control-restart

test group:
	test-ssh
	test-inspect
	test-ps
	test-cloudenv
	test-ml
	test-machine_list

deploy group:
	deploy-deploy
	deploy-install
	deploy-clean
	deploy-relink
	deploy-stage
	deploy-wipeout
menu group:
	menu
```


##fdeploy - global options

| option | args | description | default | 
|-|-|-|-|
| `-c|--cache` | | cache the downloaded archives | `false` |
| `-X`  |  | verbose remote commands | `false` |
| `-k|--keep`  | | preserve workarea | `false` |
| `-i|--identity`  | `[file]` | selects a file from which the identity (private key) for RSA or DSA authentication is read. There is a built-in resolving. If identity is not supplied then for L1 and L2 we try `./ssh/dev_deploy_user_dsa` , for L3/4/5/6 we try `~/.ssh/test_deploy_user_dsa` and finally for PROD we try `~/.ssh/prod_deploy_user_dsa`.    |  |
| `-t|--dry-run`  | | generate all the scripts for remote invocation without executing them | | 
| `--log-level` | WARN\|ERROR\|INFO\|DEBUG\|TRACE | set the log level | None | 
| `-v[vv]` | | set the log level v=info, vv=debug, vvv=trace | None |
| `--version`  | | show version of fdeploy | | 
| `-h|--help`  | | show this text ||

##fdeploy - deploy commands

`fdeploy.py [global-options] deploy -f [file] [options]`

| option | args | description | default |
|--|--|--|--|
| `-l|--level`  | L1/2/3/4/5/PROD | level name of target environment |  
| `-f|--file`  | `[file]` | the deployment descriptor in json for a component | 
| `-a|--action` | (see table) | action |
| `-u|--user` | `[user]` | overwriting the username in the targets (optional)|  
| `-p|--path` | `[path]` | overwriting the path in the targets (optional)|  
  
All actions for deployment take place under the `cm` user (if the last two characters are `cm` of the deploying user)

| action | description |
|---|---|
| `deploy` | uploads the contents, stops, unpacks, relinks and then starts the application |
| `install` |uploads the contents, unpacks, and relinks the application |
| `stage` | uploads the contents, unpacks the application |
| `relink` | link the specified version of the application as current |
| `clean` | will delete application installations that are oldest, leaving three of them on disk |
| `wipeout` | will delete the whole remote installation area |


##fdeploy - control commands
`fdeploy.py [global-options] control -f [file] [options]`

| option | args | description | default |
|--|--|--|--|
| `-l|--level`  | L1/2/3/4/5/PROD | level name of target environment |  |
| `-f|--file`  | `[file]` | the deployment descriptor in json for a component | | 
| `-a|--action` | (see table) | action ||
| `-u|--user` | `[user]` | overwriting the username in the targets |  |
| `-p|--path` | `[path]` | overwriting the path in the targets |  |

All actions for control take place under the `app` user. If specified user is cm it will be the truncated username example: `sefscm` (cm user) => `sefs` (app user)

| action | description |
|---|---|
| `start` | starts the application invoking `current/scripts/start.sh` as app user |
| `stop` | stops the application invoking `current/scripts/stop.sh` |
| `restart` | stops and starts the application |
| `status` | returns the application status (running or not running) |


###fdeploy - test commands
`fdeploy.py [global-options] test -f [file] -l [level] [options]`

| option | args | description | default |
|--|--|--|--|
| `-l|--level`  | L1/2/3/4/5/PROD | level name of target environment |  |
| `-f|--file`  | `[file]` | the deployment descriptor in json for a component | | 
| `-a|--action` | (see table) | action ||
| `-u|--user` | `[user]` | overwriting the username in the targets |  |
| `-p|--path` | `[path]` | overwriting the path in the targets |  |

| action | description |
|---|---|
| `ssh` | tests if ssh is setup correctly |
| `inspect` | list the contents of the remote installation area of the application |
| `ps` | lists the running processes for this application |
| `cloudenv` | returns information about the cloud instance (if applicable) |
| `ml|machine_list` | returns list of machine names the deployment descriptor reference |




####Examples  

+ testing the ssh connectivity to all the defined targets with an alternate identity

```bash
$ python fdeploy.py --log-level ERROR -i ~/.ssh/test_deployer_identity -d test -f ../../test/resources/java-cache.json -a ssh -l L1
stdout-c0003476.test.cloud.fedex.com.log : Last login: Tue Sep  5 03:02:19 2017 from 10.233.1.200
stdout-c0003476.test.cloud.fedex.com.log :
stdout-c0003476.test.cloud.fedex.com.log : WARNING: Unauthorized access to this system is forbidden. By accessing this
stdout-c0003476.test.cloud.fedex.com.log : system, you agree that your actions will be monitored.
stdout-c0003476.test.cloud.fedex.com.log :
stdout-c0003476.test.cloud.fedex.com.log : Server provisioned by the FedEx Cloud Engine
stdout-c0003476.test.cloud.fedex.com.log : [sefscm@c0003476 ~]$
```

+ inspecting the installation area of the target in level L1

```bash
$ python fdeploy.py  test -f ../../test/resources/java-cache.json -a inspect -l L1

[INFO ] :  < init bash-generator stage_directory = tmp
[INFO ] :  > generating level L1 for action inspect from /Users/akaan/workspace-4.6/SilverRuntimeEnvironment-6.0.0/src/test/resources
[INFO ] :  < wrote 'sefs-fxs-java-cache-service.sh'
deploy-sefs-fxs-java-cache-service.sh.log : [INFO] :  < processing : machine=sefscm@c0003476.test.cloud.fedex.com:/var/tmp/apps | host=c0003476.test.cloud.fedex.com | url_path=/var/tmp/apps
deploy-sefs-fxs-java-cache-service.sh.log :  > inspecting ssh sefscm@c0003476.test.cloud.fedex.com:/var/tmp/apps
deploy-sefs-fxs-java-cache-service.sh.log : 322641   20 -rw-r--r--   1 sefscm   sefs        16839 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jackson-jaxrs-json-provider-2.7.0.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322642   44 -rw-r--r--   1 sefscm   sefs        45024 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/hamcrest-core-1.3.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322643  380 -rw-r--r--   1 sefscm   sefs       385091 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/commons-lang3-3.2.1.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322644 1172 -rw-r--r--   1 sefscm   sefs      1198501 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jackson-databind-2.7.0.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322645  232 -rw-r--r--   1 sefscm   sefs       234063 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jetty-client-9.2.14.v20151106.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322646   52 -rw-r--r--   1 sefscm   sefs        50894 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jackson-annotations-2.7.0.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322647   76 -rw-r--r--   1 sefscm   sefs        76045 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jackson-dataformat-smile-2.7.0.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322648  356 -rw-r--r--   1 sefscm   sefs       361119 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jetty-util-9.2.14.v20151106.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322649   36 -rw-r--r--   1 sefscm   sefs        34471 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jackson-module-jaxb-annotations-2.7.0.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322650  308 -rw-r--r--   1 sefscm   sefs       314932 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/junit-4.12.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322651  480 -rw-r--r--   1 sefscm   sefs       489884 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/log4j-1.2.17.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322652  108 -rw-r--r--   1 sefscm   sefs       108324 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jetty-io-9.2.14.v20151106.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322653   44 -rw-r--r--   1 sefscm   sefs        41123 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/commons-cli-1.2.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322654  104 -rw-r--r--   1 sefscm   sefs       105860 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jetty-http-9.2.14.v20151106.jar
deploy-sefs-fxs-java-cache-service.sh.log : 322655   80 -rw-r--r--   1 sefscm   sefs        80091 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/probe-client-1.3.0-SNAPSHOT.jar
deploy-sefs-fxs-java-cache-service.sh.log : 201286   32 -rw-r--r--   1 sefscm   sefs        29995 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jackson-jaxrs-base-2.7.0.jar
deploy-sefs-fxs-java-cache-service.sh.log : 201287  248 -rw-r--r--   1 sefscm   sefs       252084 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/lib/jackson-core-2.7.0.jar
deploy-sefs-fxs-java-cache-service.sh.log : 8519560    4 -rwxr-xr-x   1 sefscm   sefs          854 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/scripts/start.sh
deploy-sefs-fxs-java-cache-service.sh.log : 8519565    4 -rwxr-xr-x   1 sefscm   sefs          117 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/scripts/stop.sh
deploy-sefs-fxs-java-cache-service.sh.log : 8519566    4 -rwxr-xr-x   1 sefscm   sefs          133 Aug 29 16:48 /var/tmp/apps/1.3.0-SNAPSHOT/scripts/status.sh
deploy-sefs-fxs-java-cache-service.sh.log : 25197743 79188 -rw-rw-r--   1 sefscm   sefs     81086948 Aug 31 21:57 /var/tmp/apps/0.0.1-SNAPSHOT/lib/share-cache-plugin-0.0.1-SNAPSHOT.jar
deploy-sefs-fxs-java-cache-service.sh.log : 169283    4 -rwxr-xr-x   1 sefscm   sefs         1565 Aug 31 21:56 /var/tmp/apps/0.0.1-SNAPSHOT/scripts/start.sh
deploy-sefs-fxs-java-cache-service.sh.log : 169284    4 -rwxr-xr-x   1 sefscm   sefs          150 Aug 31 21:56 /var/tmp/apps/0.0.1-SNAPSHOT/scripts/stop.sh
deploy-sefs-fxs-java-cache-service.sh.log : 169285    4 -rwxr-xr-x   1 sefscm   sefs          155 Aug 31 21:56 /var/tmp/apps/0.0.1-SNAPSHOT/scripts/status.sh
deploy-sefs-fxs-java-cache-service.sh.log :  > inspection succeeded
stdout-c0003476.test.cloud.fedex.com.log : Last login: Tue Sep  5 03:02:19 2017 from 10.233.1.200
stdout-c0003476.test.cloud.fedex.com.log :
stdout-c0003476.test.cloud.fedex.com.log : WARNING: Unauthorized access to this system is forbidden. By accessing this
stdout-c0003476.test.cloud.fedex.com.log : system, you agree that your actions will be monitored.
stdout-c0003476.test.cloud.fedex.com.log :
stdout-c0003476.test.cloud.fedex.com.log : Server provisioned by the FedEx Cloud Engine
stdout-c0003476.test.cloud.fedex.com.log : [sefscm@c0003476 ~]$
```