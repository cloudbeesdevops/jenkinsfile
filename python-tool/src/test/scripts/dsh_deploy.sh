#!/binbash


## declare an array variable
declare -a arr=("sefs@c000437.cloud.fedex.com")

## now loop through the above array
for i in "${arr[@]}"
do
   echo "$i"
   # or do whatever with individual element of the array
done
