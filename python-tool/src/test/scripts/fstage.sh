#!/bin/bash

function usage() {
  echo "USAGE: $0 APP.[zip|tar.gz|tgz|tar] [-v]"
  exit 1
}

function warnDirExists() {
  if [[ -d ${1} ]]; then echo; echo "  WARNING: ${1} already exists!"; echo; fi
}

APPARCHIVE=${1}
if [[ ! -r ${APPARCHIVE} ]]; then usage; fi

echo "=========================================="
echo "Staging ${APPARCHIVE}"
echo "=========================================="

if [[ ${APPARCHIVE} =~ \.tar\.gz$ || ${APPARCHIVE} =~ \.tgz || ${APPARCHIVE} =~ \.tar ]]; then
  gunzip -q ${APPARCHIVE}
  APPARCHIVE=${APPARCHIVE%.gz}
  APPARCHIVE=${APPARCHIVE%.tgz}
  APPARCHIVE=${APPARCHIVE%.tar}
  warnDirExists ${APPARCHIVE}
  mkdir -p ${APPARCHIVE}
  if [[ "${2}" =~ "-v" ]] ; then VERBOSE=-vv; fi
  tar ${VERBOSE} --overwrite --directory=${APPARCHIVE} -x -f ${APPARCHIVE}.tar
  rm -f ${APPARCHIVE}.tar
elif [[ ${APPARCHIVE} =~ \.zip ]]; then
  warnDirExists ${APPARCHIVE%.zip}
  if [[ "${2}" =~ "-v" ]] ; then VERBOSE=""; else VERBOSE=-q; fi
  unzip ${VERBOSE} -od ${APPARCHIVE%.zip} ${APPARCHIVE}
  rm -f ${APPARCHIVE}
else
  echo "ERROR: Unsupported archive file format"
  usage
fi