#!/bin/bash

# Full path to script dir
pushd `dirname $0` > /dev/null
SCRIPTDIR=`pwd`
popd  > /dev/null

function usage() {
  echo "USAGE: $0 [AppDirToLink]"
  exit 1
}

function checkDirExists() {
  if [[ ! -d ${1} ]]; then echo; echo "ERROR: ${1} does not exist!"; usage; fi
}

NEWLINKDIR=${1:-""}
if [[ -z "${NEWLINKDIR}" ]]; then
  NEWLINKDIR=`find ${SCRIPTDIR}/* -type d -prune | xargs ls -1rtd | tail -1`
fi
NEWLINKDIR=${NEWLINKDIR%/} # trim trailing slash if exists
NEWLINKDIR=${NEWLINKDIR##*/} # trim to just the basename
checkDirExists ${NEWLINKDIR}

rm -f current
ln -s ${NEWLINKDIR} current