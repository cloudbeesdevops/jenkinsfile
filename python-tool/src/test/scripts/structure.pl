#!/usr/bin/perl

use warnings;
use strict;

use JSON;

my @comp_common_rtv = (
	{
		name  => 'HTTP_PORT',
		value => '8096'
	},
	{
		name => 'UNIX_PRODUCT_DIR',
		value =>
'${CONTAINER_WORK_DIR}/apparchives/sefs-cache-service.zip_Files/scripts',
	},
	{
		name  => 'UNIX_START_COMMAND',
		value => 'start.sh',
	},
	{
		name => 'UNIX_START_COMMAND_ARGS',
		value =>
'EUREKA_PORT=8093 APP_ID=fxg-gps-adapter APPD_ACCOUNT_KEY=1dce5fc52c17 JMX_PORT=15558 JMX_AUTHENTICATE=false JMX_SSL=false',
	},
	{
		name  => 'UNIX_SHUTDOWN_COMMAND',
		value => 'stop.sh',
	},
	{
		name  => 'UNIX_SHUTDOWN_COMMAND_ARGS',
		value => '%%%%BROKERLEVELNAME%%%%',
	},
	{
		name  => 'UNIX_COPY_FILES',
		value => 'false',
	}
);
my @comp_env_l1rtv = (
	{
		name  => 'SPRING_PROFILE',
		value => 'L1'
	},
	{
		name  => 'EUREKA_HOST',
		value => 'urh00600.ute.fedex.com'
	},
	{
		name  => 'APPD_APPNAME',
		value => 'SU-SEFS-RELY-L1',
	},
	{

		name  => 'APPD_CONTROLLER_HOST',
		value => 'fedex1-test.saas.appdynamics.com'
	},
	{ name => 'APPD_ACCOUNT', value => 'fedex1-test' }
);
my $comp_deploy_platform = 'SilverFabric';
my @comp_env_l2rtv       = (
	{
		name  => 'SPRING_PROFILE',
		value => 'L1,wsso'
	}
);

my @l1 = ( @comp_common_rtv, @comp_env_l1rtv );
my @l2 = ( @comp_common_rtv, @comp_env_l2rtv );

# COMPONENT DEF
my $comp_config = [
	{
		id          => 'sefs-fxs-java-cache-service',
		name_format => 'JAVA_CACHE_SERVICE_%0d',
		platform    => $comp_deploy_platform,
		type        => 'java',
		content     => [
			{
				gav => 'com.fedex.ground.sefs.pd.adapter:fxg-gps-adapter:zip',
				saveArchiveName => 'com.fedex.ground.sefs.pd.adapter:fxg-gps-adapter:zip',
				relativePath => 'apparchives'
			}
		],

		levels => [
			{
				'level'        => 'L1',
				'rtv'          => \@l1,
				'dependencies' => qw/JAVA_PD_DISCOVERY/,
				'targets'      => [
					'tibcosilver@urh00606.ute.fedex.com:/var/tmp/cache',

					#'sefs@c0003476.test.cloud.com'
				]
			},
			{
				'level'   => 'L2',
				'rtv'     => \@l2,
				'targets' => [
					'tibcosilver@irh00606.ute.fedex.com:/opt/fedex/tibco',

					#'sefs@c0003476.test.cloud.com:/opt/fedex/sefs'
				  ]

			}

		]
	}
];

my $json = JSON->new->allow_nonref;

print $json->pretty->encode($comp_config);
