#!/usr/bin/perl

# usage: ps -ef | grep ======= | perl -n rm_notty.pl
chomp;
@x=split/\s+/;
my $CMD=sprintf "/usr/bin/ssh -i ~/.ssh/test_deploy_user_dsa %s ls -l ",$x[8];
$CMD=sprintf "/usr/bin/ssh -i ~/.ssh/test_deploy_user_dsa %s 'ps -ef | grep notty'",$x[8];
printf " analyzing %s\n", $x[8];
printf " $CMD";
open( PS, "-|",$CMD) or die "$!";
my $item;
while (defined($item = <PS>)) {
	print $item, "\n";
	@line = split/\s+/, $item;
	$cmd=sprintf "ssh -i ~/.ssh/test_deploy_user_dsa %s kill -9 %s\n", $x[8],$line[1];
	#print $cmd;
	open(__PS, "-|", $cmd) or die "$!";
	my @_XX=<__PS>;
	close(__PS);
 } # process the output of ps command one line at a time
close(PS);
1;