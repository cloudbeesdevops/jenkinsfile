#!/bin/perl
use IO::Socket::INET;

$host = $ARGV[0]?$ARGV[0] : 'localhost' ;
# auto-flush on socket
$| = 1;

# create a connecting socket
RECVACK:
my $socket = new IO::Socket::INET (
    PeerHost => $host,
    PeerPort => '7777',
    Proto => 'tcp',
);
die "cannot connect to the server $host $!\n" unless $socket;
print "connected to the server $host\n";

# data to send to a server
while (1) {
        $send_data = <STDIN>;
        print "send: $send_data";
        last if $send_data =~ /quit/;
        $socket->send($send_data);
        while ($line = <$socket>) {
                $socket->recv($recv_data,1024);
                print "recv: $recv_data.\n";
        }
        goto RECVACK;
}

# notify server that request has been sent
shutdown($socket, 1);

# receive a response of up to 1024 characters from server
my $response = "";
$socket->recv($response, 1024);
print "received response: $response\n";

$socket->close();

1;