#!/bin/bash

# Full path to script dir
pushd `dirname $0` > /dev/null
SCRIPTDIR=`pwd`
popd  > /dev/null

function usage() {
  echo "USAGE: $0 "
  exit 1
}

CURRENTLINK=`readlink current`
CANDIDATES="`find ${SCRIPTDIR}/*  -type d -prune | xargs ls -1rtd | head -n -3`"
for CANDIDATE in ${CANDIDATES} ; do
  if [[ "${CANDIDATE##*/}" == "${CURRENTLINK}" ]] ; then
    echo "Skipping ${CANDIDATE##*/} as it is the currently linked version"
  elif [[ ! -d "${CANDIDATE}" ]] ; then
    echo "Skipping ${CANDIDATE} as it does not appear to be a directory?"
  else
    echo "Removing old install: ${CANDIDATE}"
    rm -rf ${CANDIDATE}
  fi
done
