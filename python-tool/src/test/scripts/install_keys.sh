export dshlist="~/.dsh/machine.list"
export target="/opt/fedex/sefs"
export user=sefscm
if [[ "${1}" != "" ]]; then
	export dshlist="${1}"
fi
echo <<EOF > inject.sh
#!/bin/bash
U_DIR=/opt/fedex/tibcosilver
ls \$U_DIR
[[ -e \${U_DIR}/.ssh/ ]] && mkdir -p \${U_DIR}/.ssh
whoami && ls -lart ${U_DIR}/.ssh
echo "ssh-dss AAAAB3NzaC1kc3MAAACBAMF/NOOV3bBoku/pcBqC7hRKafCsTL0e6RLj4lrqK6uNxGkH9osHy5Xn8bTdBdSlBVppFc7CREZKz8+I/gNVG3v4XXVCi/k0MI6Ls9hE+bgNA3Crn7lxHLLGfLQSjE2BxBtzAhckXCkm+2u+mjjSCuyo53gqvKmH0AI8aA+zWtqxAAAAFQCJmbDEnTrot8eZM0qHpq6epASNsQAAAIEAsEbpsb/YfvzdBePCXVaArYLN1KpBrZUlzijrIZL9Gv86xPlcpfVuul2M2Uja9bgyKm2e5QmY8ID2SeutZ5Mbw0KYdsBB/bq7N/1mk7yvQh8lSu5efWRPWRgnLCuMWpKJuAnZVhw3xmQPmWJfkEc+UuQQkj80yMDqBq6UGbCtefAAAACAbObh6BbDcsdpM7TlHkywTdslkThD5yjDlHncf45pt692VhHhXbsbTx8UbrXn0F/sIIev6ZduX9bn9+DupegHtiKBMM/mRZv4Bd0uefdvupquGOho+fY15OFvMTMjZTZkWrbeDi+i4uWliLJtvUueNXVZCUMkh+leS7QF9jgmpwI= jenkins@uwb00078.ute.fedex.com" >> \${U_DIR}/.ssh/authorized_keys
chmod -R 700 \${U_DIR}/.ssh/authorized_keys
ls -lart \${U_DIR}/.ssh/
EOF
chmod 755 inject.sh
cat ~/.dsh/machines.list | xargs -i% -t scp -q inject.sh %:/tmp/
#dsh ls /etc/bashrc | perl -ne '{ $_=~s/://g;@x=split /\s+/;printf "ssh -t %s \" echo \${1} | sudo -S -u %s cp /tmp/inject.sh %s && %s/inject.sh \"\n",$x[0], $x[2], $ENV[{target},$ENV{dshlist}' | tee out
dsh ls /etc/bashrc | perl -ne '{ $_=~s/://g;@x=split /\s+/;printf "ssh -t %s \" echo \${1} | sudo -S -u %s cp /tmp/inject.sh %s && %s/inject.sh \"\n",$x[0], $ENV{user},$ENV{target},$ENV{target};}'
perl -i -pe "s/#/'/g" out
echo "Please run: . out [Password]"