<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.fedex.sefs.common</groupId>
	<artifactId>spring-boot-bom-parent</artifactId>
	<version>1.5.10.0-SNAPSHOT</version>
	<packaging>pom</packaging>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.10.RELEASE</version>
		<relativePath />
		<!-- lookup parent from repository -->
	</parent>
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<env.BUILD_NUMBER>unknown</env.BUILD_NUMBER>
		<spring-cloud.version>Edgware.SR2</spring-cloud.version>
		<spring-cloud-services.version>1.6.1.RELEASE</spring-cloud-services.version>
		<dev-framework.version>7.1.1</dev-framework.version>
		<shipmentdom.bom.version>1.0.0</shipmentdom.bom.version>
	</properties>

	<build>
		<pluginManagement>
			<plugins>
				<plugin>
					<artifactId>maven-compiler-plugin</artifactId>
					<version>3.7.0</version>
					<configuration>
						<source>1.8</source>
						<target>1.8</target>
					</configuration>
				</plugin>
				<plugin>
					<artifactId>maven-deploy-plugin</artifactId>
					<version>2.8.2</version>
				</plugin>
				<plugin>
					<artifactId>maven-dependency-plugin</artifactId>
					<version>3.1.0</version>
				</plugin>
				<plugin>
					<artifactId>maven-surefire-plugin</artifactId>
					<version>2.21.0</version>
				</plugin>
				<plugin>
					<groupId>org.apache.maven.plugins</groupId>
					<artifactId>maven-enforcer-plugin</artifactId>
					<version>3.0.0-M2</version>
					<executions>
						<execution>
							<id>enforce-no-snapshots</id>
							<goals>
								<goal>enforce</goal>
							</goals>
							<configuration>
								<rules>
									<banDuplicatePomDependencyVersions />
									<banDistributionManagement />
									<requireReleaseDeps>
										<message>No Snapshots Allowed!</message>
										<failWhenParentIsSnapshot>false</failWhenParentIsSnapshot>
									</requireReleaseDeps>
									<requireProperty>
										<property>shipmentdom.bom.version</property>
										<message>You must not change the shipmentdom bom
											(xopco-common-tools-shipment-dom) version property!</message>
										<regex>${shipmentdom.bom.version}</regex>
									</requireProperty>
								</rules>
								<fail>true</fail>
							</configuration>
						</execution>
					</executions>
				</plugin>
				<plugin>
					<groupId>org.springframework.boot</groupId>
					<artifactId>spring-boot-maven-plugin</artifactId>
					<configuration>
						<attach>true</attach>
					</configuration>
					<executions>
						<execution>
							<goals>
								<goal>build-info</goal>
							</goals>
							<configuration>
								<additionalProperties>
									<cloudbees.build.number>${env.BUILD_NUMBER}</cloudbees.build.number>
								</additionalProperties>
							</configuration>
						</execution>
					</executions>
				</plugin>

			</plugins>
		</pluginManagement>
	</build>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
			<dependency>
				<groupId>sefs.xopco.core.common-tools</groupId>
				<artifactId>xopco-common-tools-shipment-dom</artifactId>
				<version>${shipmentdom.bom.version}</version>
			</dependency>
			<dependency>
				<groupId>io.pivotal.spring.cloud</groupId>
				<artifactId>spring-cloud-services-dependencies</artifactId>
				<version>${spring-cloud-services.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<profiles>
		<profile>
			<id>default</id>
			<activation>
				<property>
					<name>env.BUILD_NUMBER</name>
				</property>
			</activation>
			<properties>
				<cloudbees.build.number>${env.BUILD_NUMBER}</cloudbees.build.number>
			</properties>
		</profile>
		<!-- enable this profile in externalized file, so enforcer does not run 
			on builds -->
		<profile>
			<id>externalized</id>
			<activation>
				<file>
					<missing>${basedir}/profiles.xml</missing>
				</file>
			</activation>
			<build>
				<plugins>
					<plugin>
						<groupId>org.apache.maven.plugins</groupId>
						<artifactId>maven-enforcer-plugin</artifactId>
						<configuration>
							<skip>${enforce.skip}</skip>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>

	</profiles>

	<distributionManagement>
		<snapshotRepository>
			<id>snapshots</id>
			<url>http://sefsmvn.ute.fedex.com:9999/nexus/content/repositories/snapshots</url>
		</snapshotRepository>
		<repository>
			<id>releases</id>
			<url>http://sefsmvn.ute.fedex.com:9999/nexus/content/repositories/releases</url>
		</repository>
		<site>
			<id>mvnown</id>
			<url>scpexe://maven.ground.fedex.com/var/fedex/scm/mvn/httproot/site/${project.artifactId}/${project.artifactId}-${project.version}</url>
		</site>
	</distributionManagement>

</project>
