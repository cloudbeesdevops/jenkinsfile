#!/bin/bash
#set -e

export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=control:action=install, on level
level=L1
APP_NM=L1-waypoint-config-server
# ---- begin - data_wrapper_header.sh - source
#echo "[ERROR]: $@"
#echo "[INFO] : $@"
#set -xv
#echo "[WARN] : $@"

set -m # Enable Job Control
_uuid="common-config-service"
_workdir="/Users/ak751818/git/xopco-common-tools-fdeploy/src/test/resources/domainloader"
_stagedir="/Users/ak751818/git/xopco-common-tools-fdeploy/src/main/python/tmp"
_timeout="20"
_archive_version="1.0.0"
_archive_filename="config-server.jar"
_artifactId="config-server"
_target_path="${_stagedir}/archives"

if [[ -z "${MANIFEST_FILE}" ]]; then
  if [[ -f "${_workdir}/apps/${_uuid}/manifest-${level}.yml" ]]; then
    export MANIFEST_FILE="${_workdir}/apps/${_uuid}/manifest-${level}.yml"
  else
    export MANIFEST_FILE="${_workdir}/apps/${_uuid}/manifest.yml"
  fi
else
  echo "OVERWRITING MANIFEST FILE LOGIC with hardcoded manifest @ ${MANIFEST_FILE}"
fi

# Generating the current properties for this version
cat <<EOF > "${_stagedir}/${_archive_version}-${APP_NM}${_uuid}.properties"
#!/bin/bash
# Generated property file for application.
export API_URL="api.sys.wtcdev2.paas.fedex.com"
export APPD_NM="waypoint-proxy-server"
export APP_HOST="L1-waypoint"
export APP_NM="L1-waypoint-config-server"
export ARTIFACTID="config-server"
export CF_HOME="/var/folders/46/g1j7jmk54kz44ll0kx_xwfsh0000gn/T/tmpr44noR"
export DISCOVERY_SERVICE="waypoint-discovery-service"
export DOMAIN="app.wtcdev2.paas.fedex.com"
export EAI_NUMBER="3534645"
export GROUPID="com.fedex.cloud"
export LEVEL="L1"
export LOG_LEVEL="1"
export NUM_INSTANCES="1"
export PAM_ID="18409"
export PCF_PASSWORD="U2go007P2TCPtcgt"
export PCF_PUSH_OPTIONS="--no-start"
export PCF_USER="FXS_app3534642"
export SPACE="development"
export SPRING_PROFILES_ACTIVE="L1"
export VERSION="1.0.0"

EOF

# load the RTV's
source ${_stagedir}/${_archive_version}-${APP_NM}${_uuid}.properties

# expanding variables in YML
while read
do
    eval "echo \"${REPLY}\""
done < "${MANIFEST_FILE}" > ${_stagedir}/manifest-${_uuid}.yml

echo "------generated manifest------"
cat ${_stagedir}/manifest-${_uuid}.yml
echo "------generated manifest------"


hostname
which cf
cf_exists=${?}
if [[ "${cf_exists}" != 0 ]]; then
  echo "CloudFoundry_CLI was not found. Abort! (code=${cf_exists})"
  echo "Please export PATH with location of the cf binary (f.e: /opt/fedex/tibco/cf-cli/bin)"
  exit ${cf_exists}
else
  echo "CloudFoundry_CLI found at $(which cf)"
fi

echo "Login to ${API_URL}"
cf api https://${API_URL}
cf auth ${PCF_USER} ${PCF_PASSWORD}

echo "Set target space '${SPACE}'"
cf target -o ${EAI_NUMBER} -s ${SPACE}
if [[ "${?}" != 0 ]]; then
  echo "Error setting space ${SPACE} in org ${EAI_NUMBER}"
  exit 2
fi

# --- status ---
APP_EXISTS=$(cf get-health-check ${APP_NM})
if [[ "${APP_EXISTS}" != *"FAILED"* ]]; then
  export app_exists=1
  echo "Application ${APP_NM} Already Exists, refreshing (code=${app_exists})"
else
  export app_exists=0
  echo "Application ${APP_NM} Does not Exist, initializing (code=${app_exists})"
fi
# --- status end ---

# --- upload ---
echo "Pushing ${_target_path}/${_archive_filename} in ${SPACE}"
cf app ${APP_NM}-green > /dev/null 2>&1
green_exists=${?}
if [[ "${green_exists}" -eq 0 ]]; then
	cf delete -f ${APP_NM}-green
fi
cf p ${APP_NM} -f ${_stagedir}/manifest-${_uuid}.yml -p ${_target_path}/${_archive_filename} --hostname ${APP_HOST} ${PCF_PUSH_OPTIONS} --vars-file <(echo EUREKA_NAME: ${APP_NM})
# --- end upload ---

# ---- begin - pcf_wrapper_footer.sh - source

# ---- end - pcf_wrapper_footer.sh - source
