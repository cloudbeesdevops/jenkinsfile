#!/bin/bash
#set -e
#export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=deploy:action=stop, on level
_level=L1
level=${_level}
# ---- begin - data_wrapper_header.sh - source
function log_error {
   set > /dev/null
   echo "[ERROR]: $@"
}
function log_info {
   set > /dev/null
   echo "[INFO] : $@"
}
function log_debug {
   set > /dev/null
   echo "[DEBUG]: $@"
}
function log_warn {
   set > /dev/null
   echo "[WARN] : $@"
}
function join_by {
   local IFS="$1"; shift; echo "\$*";
}

set -m # Enable Job Control
_action="stop"
_identity_="-i /home/ak751818/.ssh/test_deploy_user_dsa"
_ssh_options="-oConnectTimeout=20 -oStrictHostKeyChecking=no -oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null"
_verbose_flag="set +xv"
_echo_envvars="set +xv"
_switch="tibcosilver"
_uuid="sefs-fxs-java-dashboard-web"
_no_wait="#SKIP#"
_timeout="20"

declare -a MACHINE_LIST=(
   "tibcosilver@irh00601.ute.fedex.com:/opt/tibco/silver/persistentdata/FXS/apps/fxs-dashboard-web"
 );
declare -a UPLOAD_CONTENTS=(
"dashboard-web.zip:1.2.3:::" );
_upload_elements=""dashboard-web.zip:1.2.3:::""

log_debug "elements ${_upload_elements}"

# set working base on the local host
local_base_dir="/home/ak751818/fdeploy/tmp"
[ ! -e "${local_base_dir}" ] && mkdir -p "${local_base_dir}"
[ ! -e "${local_base_dir}" ] && log_error " ! aborting unable creating : ${local_base_dir}" && exit 1
cd "${local_base_dir}"
#
for MACHINE in ${MACHINE_LIST[@]}; do
   IFS=':' read -ra URL <<< "$MACHINE"
   log_debug " > machine=${URL}"
   _uri_path="${URL[1]}"
   _url="${URL[0]}"
   _archive_version="1.2.3"
   _archive_filename="dashboard-web.zip"
   _artifactId="dashboard-web"
   IFS='@' read -ra HOST <<< "$_url"
   _user="${HOST[0]}"
   _host="${HOST[1]}"
   _ssh="${_user}@${_host}"
   if [[ "${_switch}" == "" ]]; then
      if [[ "${_user}" == *cm ]]; then
         b=${_user%cm}
         _ssh="${b}@${_host}";
         _user="${b}"
      fi
   fi
   _landing_area="/var/tmp/${_user}"
   IFS='?' read -ra URI <<< "$_uri_path"
   _target_path="${URI[0]}"
   _uri="${URI[1]}"
   _script_name="${_host}${_uuid}-${_action}"
   log_info  " < processing : target=${MACHINE} sudo=${_switch} "
   log_debug " < host=${_host} | url_path=${_uri_path} | ssh=${_ssh}"
# ---- end - data_wrapper_header.sh - source

# ---- begin - remote_script_begin.sh - source

# generate remote install
log_info " > writing remote script : ${local_base_dir}/${_script_name}"
cat <<EOT > "${local_base_dir}/${_script_name}.txt"
#!/bin/bash
# --- begin ${local_base_dir}/${_script_name}.txt
log_error() {
  set > /dev/null
  echo "[ERROR]: \$@"
}
log_info()  {
  set > /dev/null
  echo "[INFO] : \$@"
}
log_debug() {
  set > /dev/null
  echo "[DEBUG]: \$@"
}
log_warn()  {
  set > /dev/null
  echo "[WARN] : \$@"
}
timestamp() {
    while IFS= read -r line; do
        echo "$(date) - $line"
    done
}

echo "====================================="
echo "DATE: `date` / USER: `whoami` / HOST: `hostname`" - PID: $$
echo "====================================="

# ENVIRONMENT SETTINGS
export APP_ID="8090"
export ARTIFACTID="dashboard-web"
export EAI_NUMBER="8090"
export GROUPID="com.fedex.sefs.dashboard"
export HTTP_PORT="8090"
export JAVA_HOME="8090"
export LEVEL="L1"
export SPRING_PROFILE="oracle,L0,wsso"
export VERSION="1.2.3"

#
#
set +xv

# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[2019-07-30 04:05:19.743088] action=stop
EOF
# ---- begin - remote_readme_snippet.sh - source #
cat "${_target_path}/${_archive_version}/README" >> "${_target_path}/fdeploy.log"
# ---- end - remote_readme_snippet.sh - source

/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source

# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[2019-07-30 04:05:19.743088] action=stop - version-info=# ---- begin - remote_readme_snippet.sh - source
EOF
# ---- begin - remote_readme_snippet.sh - source #
cat "${_target_path}/${_archive_version}/README" >> "${_target_path}/fdeploy.log"
# ---- end - remote_readme_snippet.sh - source

/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source

# ---- begin - remote_stop_snippet.sh - source
if [[ -e "${_target_path}/current/scripts" ]]; then
   cd ${_target_path}/current/scripts
   echo -n "STATUS: ${_host}:${_archive_filename}" && ./status.sh
   if [ \$? -eq 0 ]; then
      echo "found process, stopping... ${_archive_version}"
      ./stop.sh
      sleep 5
      echo -n "STATUS: ${_host}:${_archive_filename}" && ./status.sh
      # now we should get >0 back since not running.
      if [[ \$? -ne 0 || "\$?" != "" ]]; then
         echo "╔═╗╔╦╗╔═╗╔═╗╔═╗╔═╗╔╦╗";
	      echo "╚═╗ ║ ║ ║╠═╝╠═╝║╣  ║║${_artifactId}-${_archive_version}";
	      echo "╚═╝ ╩ ╚═╝╩  ╩  ╚═╝═╩╝(${_user}@${_host})";
	   else
         echo "┌─┐┌┬┐┌─┐┌─┐  ┌─┐┌─┐┬┬  ┬ ┬┬─┐┌─┐";
         echo "└─┐ │ │ │├─┘  ├┤ ├─┤││  │ │├┬┘├┤ ${_artifactId}-${_archive_version}";
         echo "└─┘ ┴ └─┘┴    └  ┴ ┴┴┴─┘└─┘┴└─└─┘(${_user}@${_host})";
      fi
   else
      echo "╔═╗╔╦╗╔═╗╔═╗╔═╗╔═╗╔╦╗";
      echo "╚═╗ ║ ║ ║╠═╝╠═╝║╣  ║║${_artifactId}-${_archive_version}";
      echo "╚═╝ ╩ ╚═╝╩  ╩  ╚═╝═╩╝(${_user}@${_host})";
   fi
else
   echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
   echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}-${_archive_version}";
   echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
   echo " ! aborting no current symlink available ${_target_path}/current."  && exit 1
fi
# ---- end - remote_stop_snippet.sh - source

# ---- begin - remote_script_end.sh - source
log_debug "Leaving remote execution, looking for ${_user}@notty :"
# find orphaned ssh session due to the remote invocation
NOTTY_PRCS=\$(ps -u ${_user} -f | grep '@notty' | grep -v grep | grep -v APPD)
#
log_debug "Echoing pids: \${NOTTY_PRCS}"
#
[[ "\${NOTTY_PRCS}" != "" ]] && log_debug "found orphaned processes ${NOTTY}"
if [[ "\${NOTTY_PRCS}" != "" ]]; then
   echo "\${NOTTY_PRCS}" | perl -ne '@x=split/\s+/;printf " %s", \$x[1];' | xargs -i% kill %
fi
# ---- end - remote_script_end.sh - source

EOT

# ---- begin - data_wrapper_footer.sh - source
   log_debug " > target : ${MACHINE} "
   log_info " > executing : ${_ssh} | ${_script_name}"
   #${verboseFlag}
   if [[ "${local_base_dir}" != "." ]]; then
      [ ! -e ${local_base_dir} ] && mkdir -p "${local_base_dir}"
   fi
   if [[ ! -e "${local_base_dir}/master-${_uuid}.sh" ]]; then
      cat <<EOX > "${local_base_dir}/master-${_uuid}.sh"
#!/bin/bash -s
timestamp() {
  while IFS= read -r line; do
      echo "\$(date) - \$line"
  done
}
EOX
   fi
   timescript=""
   script=""
   if [[  `which timeout` != '' ]]; then
      timescript="timeout -s SIGKILL ${_timeout}s"
   fi
   cat <<EOXX >> "${local_base_dir}/master-${_uuid}.sh"
echo " __ \ /  __  __     ___  __ (${_timeout})"
echo "|_   X  |_  /   | |  |  |_ (${_ssh})"
echo "|__ / \ |__ \__ |_|  |  |__ [${local_base_dir}/${_script_name}]"
EOXX
   printf -v script '%s /usr/bin/ssh -n %s %s %s "$(<%s.txt)" 2>&1  >> "%s/stdout-%s-%s.log" &' \
   "${timescript}" "${_ssh}" "${_identity_}" "${_ssh_options}" "${local_base_dir}/${_script_name}" \
   "${local_base_dir}" "${_uuid}" "${_host}"
   echo -E "$script" >> "${local_base_dir}/master-${_uuid}.sh"
done # ---

if [[ "${_no_wait}" == "" ]]; then
   echo "echo starting" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "wait" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "echo completed" >> "${local_base_dir}/master-${_uuid}.sh"
fi


log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - data_wrapper_footer.sh - source

#!/bin/bash
#set -e
#export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=deploy:action=install, on level
_level=L1
level=${_level}
# ---- begin - data_wrapper_header.sh - source
function log_error {
   set > /dev/null
   echo "[ERROR]: $@"
}
function log_info {
   set > /dev/null
   echo "[INFO] : $@"
}
function log_debug {
   set > /dev/null
   echo "[DEBUG]: $@"
}
function log_warn {
   set > /dev/null
   echo "[WARN] : $@"
}
function join_by {
   local IFS="$1"; shift; echo "\$*";
}

set -m # Enable Job Control
_action="install"
_identity_="-i /home/ak751818/.ssh/test_deploy_user_dsa"
_ssh_options="-oConnectTimeout=20 -oStrictHostKeyChecking=no -oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null"
_verbose_flag="set +xv"
_echo_envvars="set +xv"
_switch="tibcosilver"
_uuid="sefs-fxs-java-dashboard-web"
_no_wait="#SKIP#"
_timeout="20"

declare -a MACHINE_LIST=(
   "tibcosilver@irh00601.ute.fedex.com:/opt/tibco/silver/persistentdata/FXS/apps/fxs-dashboard-web"
 );
declare -a UPLOAD_CONTENTS=(
"dashboard-web.zip:1.2.3:::" );
_upload_elements=""dashboard-web.zip:1.2.3:::""

log_debug "elements ${_upload_elements}"

# set working base on the local host
local_base_dir="/home/ak751818/fdeploy/tmp"
[ ! -e "${local_base_dir}" ] && mkdir -p "${local_base_dir}"
[ ! -e "${local_base_dir}" ] && log_error " ! aborting unable creating : ${local_base_dir}" && exit 1
cd "${local_base_dir}"
#
for MACHINE in ${MACHINE_LIST[@]}; do
   IFS=':' read -ra URL <<< "$MACHINE"
   log_debug " > machine=${URL}"
   _uri_path="${URL[1]}"
   _url="${URL[0]}"
   _archive_version="1.2.3"
   _archive_filename="dashboard-web.zip"
   _artifactId="dashboard-web"
   IFS='@' read -ra HOST <<< "$_url"
   _user="${HOST[0]}"
   _host="${HOST[1]}"
   _ssh="${_user}@${_host}"
   if [[ "${_switch}" == "" ]]; then
      if [[ "${_user}" == *cm ]]; then
         b=${_user%cm}
         _ssh="${b}@${_host}";
         _user="${b}"
      fi
   fi
   _landing_area="/var/tmp/${_user}"
   IFS='?' read -ra URI <<< "$_uri_path"
   _target_path="${URI[0]}"
   _uri="${URI[1]}"
   _script_name="${_host}${_uuid}-${_action}"
   log_info  " < processing : target=${MACHINE} sudo=${_switch} "
   log_debug " < host=${_host} | url_path=${_uri_path} | ssh=${_ssh}"
# ---- end - data_wrapper_header.sh - source

# ---- begin - upload_snippet.sh - source
   #printf -v _local_archive_product "%s " "${UPLOAD_CONTENTS[@]}"
   for UPLOAD in ${UPLOAD_CONTENTS[@]}; do
      IFS=':' read -r -a contents <<< "${UPLOAD}"
      _local_archive="${contents[0]}"
      _path="${contents[1]}"
      _local_archive_product="${local_base_dir}/archives/${_local_archive}"
      log_debug " > upload  : (${_user}@${_host})";
      log_info " < uploading : _local_archive_products=${_local_archive_product} | _landing_area=${_landing_area}"
      log_debug " < processing : _target_path=${_target_path} | uri=${_uri} "
      #echo -n "PWD= " && pwd
      echo ${_local_archive_product}
      if [[ -e "${_local_archive_product}" ]]; then
         log_info " > uploading archive to ${_user}@${_host}:${_landing_area}, landing area: ${_landing_area}"
         /usr/bin/ssh ${_identity_} ${_ssh_options} "${_ssh}" "[[ ! -e ${_landing_area} ]] && mkdir -p ${_landing_area}" 2>&1 | tee upload_ssh.log
         log_debug " > ${MACHINE} --> ${_user}@${_host}:${_landing_area}"
         /usr/bin/scp ${_identity_} ${_ssh_options} ${_extract_contents} ${_local_archive_product} ${_user}@${_host}:${_landing_area} </dev/null 2>&1 | tee upload_ssh.log
         if [ $? -eq 0 ]; then
            echo "╦ ╦╔═╗╦  ╔═╗╔═╗╔╦╗╔═╗╔╦╗";
            echo "║ ║╠═╝║  ║ ║╠═╣ ║║║╣  ║║upload_area=${_landing_area}";
            echo "╚═╝╩  ╩═╝╚═╝╩ ╩═╩╝╚═╝═╩╝${_user}@${_host})"
            echo "target_path=${_target_path}; local_archive=${_local_archive_product}";
         else
            echo "╦ ╦╔═╗╦  ╔═╗╔═╗╔╦╗  ╔═╗╔═╗┬╦  ╔═╗╔╦╗";
            echo "║ ║╠═╝║  ║ ║╠═╣ ║║  ╠╣ ╠═╣│║  ║╣  ║║${_local_archive_product}";
            echo "╚═╝╩  ╩═╝╚═╝╩ ╩═╩╝  ╚  ╩ ╩┴╩═╝╚═╝═╩╝${_user}@${_host})";
            exit 1
         fi

      else
         echo "╦ ╦╔═╗╦  ╔═╗╔═╗╔╦╗  ╔═╗╦═╗╔╗ ╔═╗╦═╗╔╦╗╔═╗╔╦╗";
         echo "║ ║╠═╝║  ║ ║╠═╣ ║║  ╠═╣╠╦╝╠╩╗║ ║╠╦╝ ║ ║╣  ║║${_local_archive_product}";
         echo "╚═╝╩  ╩═╝╚═╝╩ ╩═╩╝  ╩ ╩╩╚═╚═╝╚═╝╩╚═ ╩ ╚═╝═╩╝${_user}@${_host})";
         echo " ! aborting no source material ${_local_archive_product} found for upload."
         exit 1
      fi
      if [[ ! -e "${_local_archive_product}" ]]; then
         echo " ! failed to write source material ${_local_archive_product} for upload."
         exit 1
      fi
   done
# ---- end - upload_snippet.sh - source

# ---- begin - remote_script_begin.sh - source

# generate remote install
log_info " > writing remote script : ${local_base_dir}/${_script_name}"
cat <<EOT > "${local_base_dir}/${_script_name}.txt"
#!/bin/bash
# --- begin ${local_base_dir}/${_script_name}.txt
log_error() {
  set > /dev/null
  echo "[ERROR]: \$@"
}
log_info()  {
  set > /dev/null
  echo "[INFO] : \$@"
}
log_debug() {
  set > /dev/null
  echo "[DEBUG]: \$@"
}
log_warn()  {
  set > /dev/null
  echo "[WARN] : \$@"
}
timestamp() {
    while IFS= read -r line; do
        echo "$(date) - $line"
    done
}

echo "====================================="
echo "DATE: `date` / USER: `whoami` / HOST: `hostname`" - PID: $$
echo "====================================="

# ENVIRONMENT SETTINGS
export APP_ID="8090"
export ARTIFACTID="dashboard-web"
export EAI_NUMBER="8090"
export GROUPID="com.fedex.sefs.dashboard"
export HTTP_PORT="8090"
export JAVA_HOME="8090"
export LEVEL="L1"
export SPRING_PROFILE="oracle,L0,wsso"
export VERSION="1.2.3"

#
#
set +xv

# ---- begin - remote_overwrite_snippet.sh - source
# overwrite previous install of the same version
echo "BEGIN ---OVERWRITE SECTION"
PREVIOUSLINK=\$(readlink current)
if [[ "$_archive_version" == "\${PREVIOUSLINK}" ]]; then
  if [[ -e "${_archive_version}" ]]; then
     log_warn " ! removing previous version ${_archive_version}"
     rm -fr "${_archive_version}"
  fi
fi
# remove the link to properties
[ -e  "${_target_path}/${_archive_version}.properties" ] && rm "${_target_path}/${_archive_version}.properties"
# ---- end - remote_overwrite_snippet.sh - source
echo "END -- -OVERWRITE SECTION"

# ---- begin - remote_stage_start_snippet.sh - source
echo "╔═╗╔╦╗╔═╗╔═╗╦╔╗╔╔═╗";
echo "╚═╗ ║ ╠═╣║ ╦║║║║║ ╦${_artifactId}";
echo "╚═╝ ╩ ╩ ╩╚═╝╩╝╚╝╚═╝(${_user}@${_host})";
log_info " > landing area ${_landing_area} "
# unpack the archive in the versioned directory
# if already started, stop the current stack first
if [[ -e "${_landing_area}/${_archive_filename}" ]]; then
    # install path
    if [[ ! -e "${_target_path}" ]]; then
      mkdir -p "${_target_path}"
    fi
    # unpacking all uploaded archives

    log_debug "staging elements '${_upload_elements}'"
    IFS=' ' read -r -a elements <<< "${_upload_elements}"
    for index in "\${!elements[@]}"
    do

      IFS=':' read -r -a contents <<< "\${elements[index]}"
      _file="\${contents[0]}"
      _path="\${contents[1]}"
      _dirs="\${contents[2]}"
      _perms="\${contents[3]}"
      _owner="\${contents[4]}"

      cd "${_target_path}"
      if [[ "\${_file}" == *zip ]]; then
         log_info "unpacking \${elements[index]} on path \${_path}"
         unzip -q -o -d "\${_path}" "${_landing_area}/\${_file}"
      else
         log_info "copying \${elements[index]} to \${_path}"
         [[ ! -e  "\${_path}" ]] && mkdir -p "\${_path}"
         cp -v "${_landing_area}/\${_file}"  "\${_path}"
      fi
      cd "\${_path}"
      if [[ -e "scripts" ]]; then
         chmod -R 755 scripts/*
      fi
      if [[ "\${_perms}" != "" ]] && [[ "\${_dirs}" != "" ]]; then
         log_info "changing permissions to \${_perms} for "
         chmod -vR "\${_perms}" ${_target_path}/\${_dirs}
      fi
      if [[ "\${_owner}" != "" ]] && [[ "\${_dirs}" != "" ]]; then
         log_info "changing ownership to \${_owner}"
         chown -vR "\${_owner}" ${_target_path}/\${_dirs}
      fi
	 done
# ---- end - remote_stage_snippet_begin.sh - source

# ---- begin - remote_readme_snippet.sh - source # ---- begin - remote_readme_snippet.sh - source #
cat "${_target_path}/${_archive_version}/README" >> "${_target_path}/fdeploy.log"
# ---- end - remote_readme_snippet.sh - source
#
cat "${_target_path}/${_archive_version}/README" >> "${_target_path}/fdeploy.log"
# ---- end - remote_readme_snippet.sh - source

# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[2019-07-30 18:34:06.232925] action=install
EOF
# ---- begin - remote_readme_snippet.sh - source #
cat "${_target_path}/${_archive_version}/README" >> "${_target_path}/fdeploy.log"
# ---- end - remote_readme_snippet.sh - source

/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source

# ---- begin - remote_properties_snippet.sh - source
   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
export APP_ID="8090"
export ARTIFACTID="dashboard-web"
export EAI_NUMBER="8090"
export GROUPID="com.fedex.sefs.dashboard"
export HTTP_PORT="8090"
export JAVA_HOME="8090"
export LEVEL="L1"
export SPRING_PROFILE="oracle,L0,wsso"
export VERSION="1.2.3"

EOF
# ---- end - remote_properties_snippet.sh - source


# ---- begin - remote_relink_snippet.sh - source
   # remove symlink to current install
   echo "--------------[relink]---------"
   cd ${_target_path}
set -xv
   PREVIOUSLINK=\$(readlink current)
   echo "linking ${_archive_version} as current, previous is '\${PREVIOUSLINK}'"
   if [[ "$_archive_version" != "\${PREVIOUSLINK}" ]]; then
     [[ -e current ]] && rm current
     ln -snf "${_archive_version}" current
     if [[ "\${PREVIOUSLINK}" != "" ]]; then
       [[ -e previous ]] && rm previous
       ln -snf "\${PREVIOUSLINK}" previous
     fi
   fi
   #  symlink to current properties
   ln -snf "${_archive_version}.properties" "./properties"
# ---- end - remote_relink_snippet.sh - source
set +xv

# ---- begin - remote_stage_snippet_end.sh - source
else
   echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
   echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}";
   echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
   echo " !!! packaging file ${_landing_area}/${_archive_filename} not found, aborting"
   exit 1
fi
# ---- end - remote_stage_snippet_end.sh - source

# ---- begin - remote_script_end.sh - source
log_debug "Leaving remote execution, looking for ${_user}@notty :"
# find orphaned ssh session due to the remote invocation
NOTTY_PRCS=\$(ps -u ${_user} -f | grep '@notty' | grep -v grep | grep -v APPD)
#
log_debug "Echoing pids: \${NOTTY_PRCS}"
#
[[ "\${NOTTY_PRCS}" != "" ]] && log_debug "found orphaned processes ${NOTTY}"
if [[ "\${NOTTY_PRCS}" != "" ]]; then
   echo "\${NOTTY_PRCS}" | perl -ne '@x=split/\s+/;printf " %s", \$x[1];' | xargs -i% kill %
fi
# ---- end - remote_script_end.sh - source

EOT

# ---- begin - data_wrapper_footer.sh - source
   log_debug " > target : ${MACHINE} "
   log_info " > executing : ${_ssh} | ${_script_name}"
   #${verboseFlag}
   if [[ "${local_base_dir}" != "." ]]; then
      [ ! -e ${local_base_dir} ] && mkdir -p "${local_base_dir}"
   fi
   if [[ ! -e "${local_base_dir}/master-${_uuid}.sh" ]]; then
      cat <<EOX > "${local_base_dir}/master-${_uuid}.sh"
#!/bin/bash -s
timestamp() {
  while IFS= read -r line; do
      echo "\$(date) - \$line"
  done
}
EOX
   fi
   timescript=""
   script=""
   if [[  `which timeout` != '' ]]; then
      timescript="timeout -s SIGKILL ${_timeout}s"
   fi
   cat <<EOXX >> "${local_base_dir}/master-${_uuid}.sh"
echo " __ \ /  __  __     ___  __ (${_timeout})"
echo "|_   X  |_  /   | |  |  |_ (${_ssh})"
echo "|__ / \ |__ \__ |_|  |  |__ [${local_base_dir}/${_script_name}]"
EOXX
   printf -v script '%s /usr/bin/ssh -n %s %s %s "$(<%s.txt)" 2>&1  >> "%s/stdout-%s-%s.log" &' \
   "${timescript}" "${_ssh}" "${_identity_}" "${_ssh_options}" "${local_base_dir}/${_script_name}" \
   "${local_base_dir}" "${_uuid}" "${_host}"
   echo -E "$script" >> "${local_base_dir}/master-${_uuid}.sh"
done # ---

if [[ "${_no_wait}" == "" ]]; then
   echo "echo starting" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "wait" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "echo completed" >> "${local_base_dir}/master-${_uuid}.sh"
fi


log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - data_wrapper_footer.sh - source

#!/bin/bash
#set -e
#export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=deploy:action=restart, on level
_level=L1
level=${_level}
# ---- begin - data_wrapper_header.sh - source
function log_error {
   set > /dev/null
   echo "[ERROR]: $@"
}
function log_info {
   set > /dev/null
   echo "[INFO] : $@"
}
function log_debug {
   set > /dev/null
   echo "[DEBUG]: $@"
}
function log_warn {
   set > /dev/null
   echo "[WARN] : $@"
}
function join_by {
   local IFS="$1"; shift; echo "\$*";
}

set -m # Enable Job Control
_action="restart"
_identity_="-i /home/ak751818/.ssh/test_deploy_user_dsa"
_ssh_options="-oConnectTimeout=20 -oStrictHostKeyChecking=no -oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null"
_verbose_flag="set +xv"
_echo_envvars="set +xv"
_switch="tibcosilver"
_uuid="sefs-fxs-java-dashboard-web"
_no_wait="#SKIP#"
_timeout="20"

declare -a MACHINE_LIST=(
   "tibcosilver@irh00601.ute.fedex.com:/opt/tibco/silver/persistentdata/FXS/apps/fxs-dashboard-web"
 );
declare -a UPLOAD_CONTENTS=(
"dashboard-web.zip:1.2.3:::" );
_upload_elements=""dashboard-web.zip:1.2.3:::""

log_debug "elements ${_upload_elements}"

# set working base on the local host
local_base_dir="/home/ak751818/fdeploy/tmp"
[ ! -e "${local_base_dir}" ] && mkdir -p "${local_base_dir}"
[ ! -e "${local_base_dir}" ] && log_error " ! aborting unable creating : ${local_base_dir}" && exit 1
cd "${local_base_dir}"
#
for MACHINE in ${MACHINE_LIST[@]}; do
   IFS=':' read -ra URL <<< "$MACHINE"
   log_debug " > machine=${URL}"
   _uri_path="${URL[1]}"
   _url="${URL[0]}"
   _archive_version="1.2.3"
   _archive_filename="dashboard-web.zip"
   _artifactId="dashboard-web"
   IFS='@' read -ra HOST <<< "$_url"
   _user="${HOST[0]}"
   _host="${HOST[1]}"
   _ssh="${_user}@${_host}"
   if [[ "${_switch}" == "" ]]; then
      if [[ "${_user}" == *cm ]]; then
         b=${_user%cm}
         _ssh="${b}@${_host}";
         _user="${b}"
      fi
   fi
   _landing_area="/var/tmp/${_user}"
   IFS='?' read -ra URI <<< "$_uri_path"
   _target_path="${URI[0]}"
   _uri="${URI[1]}"
   _script_name="${_host}${_uuid}-${_action}"
   log_info  " < processing : target=${MACHINE} sudo=${_switch} "
   log_debug " < host=${_host} | url_path=${_uri_path} | ssh=${_ssh}"
# ---- end - data_wrapper_header.sh - source

# ---- begin - remote_script_begin.sh - source

# generate remote install
log_info " > writing remote script : ${local_base_dir}/${_script_name}"
cat <<EOT > "${local_base_dir}/${_script_name}.txt"
#!/bin/bash
# --- begin ${local_base_dir}/${_script_name}.txt
log_error() {
  set > /dev/null
  echo "[ERROR]: \$@"
}
log_info()  {
  set > /dev/null
  echo "[INFO] : \$@"
}
log_debug() {
  set > /dev/null
  echo "[DEBUG]: \$@"
}
log_warn()  {
  set > /dev/null
  echo "[WARN] : \$@"
}
timestamp() {
    while IFS= read -r line; do
        echo "$(date) - $line"
    done
}

echo "====================================="
echo "DATE: `date` / USER: `whoami` / HOST: `hostname`" - PID: $$
echo "====================================="

# ENVIRONMENT SETTINGS
export APP_ID="8090"
export ARTIFACTID="dashboard-web"
export EAI_NUMBER="8090"
export GROUPID="com.fedex.sefs.dashboard"
export HTTP_PORT="8090"
export JAVA_HOME="8090"
export LEVEL="L1"
export SPRING_PROFILE="oracle,L0,wsso"
export VERSION="1.2.3"

#
#
set +xv

# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[2019-07-30 04:05:19.743334] action=restart - version-info=# ---- begin - remote_readme_snippet.sh - source
EOF
# ---- begin - remote_readme_snippet.sh - source #
cat "${_target_path}/${_archive_version}/README" >> "${_target_path}/fdeploy.log"
# ---- end - remote_readme_snippet.sh - source

/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source

# ---- begin - remote_restart_snippet.sh - source
   # restart with new process
   #stty -ixon
   echo "╦═╗╔═╗╔═╗╔╦╗╔═╗╦═╗╔╦╗╦╔╗╔╔═╗";
   echo "╠╦╝║╣ ╚═╗ ║ ╠═╣╠╦╝ ║ ║║║║║ ╦${_artifactId}-${_archive_version}";
   echo "╩╚═╚═╝╚═╝ ╩ ╩ ╩╩╚═ ╩ ╩╝╚╝╚═╝(${_user}@${_host})";
   if [[ -e "${_target_path}/current/scripts" ]]; then
      cd ${_target_path}/current/scripts
      echo -n "STATUS: ${_host}:${_archive_filename} " && ./status.sh
      if [ \$? -eq 0 ]; then
         echo "found process, stopping..."
         ./stop.sh
         sleep 5
      fi
      ./start.sh
      sleep 5
      echo -n "STATUS: ${_host}:${_archive_filename} " && ./status.sh
      echo
      if [ \$? -eq 0 ]; then
         echo "╔═╗╔╦╗╔═╗╦═╗╔╦╗╔═╗╔╦╗";
         echo "╚═╗ ║ ╠═╣╠╦╝ ║ ║╣  ║║${_artifactId}-${_archive_version}";
         echo "╚═╝ ╩ ╩ ╩╩╚═ ╩ ╚═╝═╩╝(${_user}@${_host})";
      else
         echo "┌─┐┌┬┐┌─┐┬─┐┌┬┐  ┌─┐┌─┐┬┬  ┬ ┬┬─┐┌─┐";
         echo "└─┐ │ ├─┤├┬┘ │   ├┤ ├─┤││  │ │├┬┘├┤ ${_artifactId}-${_archive_version}";
         echo "└─┘ ┴ ┴ ┴┴└─ ┴   └  ┴ ┴┴┴─┘└─┘┴└─└─┘(${_user}@${_host})";
      fi
   else
      echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
      echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}-${_archive_version}";
      echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
      echo "aborting restart ${_target_path}/current/scripts not found!"
      exit 1
   fi
# ---- end - remote_restart_snippet.sh - source

# ---- begin - remote_script_end.sh - source
log_debug "Leaving remote execution, looking for ${_user}@notty :"
# find orphaned ssh session due to the remote invocation
NOTTY_PRCS=\$(ps -u ${_user} -f | grep '@notty' | grep -v grep | grep -v APPD)
#
log_debug "Echoing pids: \${NOTTY_PRCS}"
#
[[ "\${NOTTY_PRCS}" != "" ]] && log_debug "found orphaned processes ${NOTTY}"
if [[ "\${NOTTY_PRCS}" != "" ]]; then
   echo "\${NOTTY_PRCS}" | perl -ne '@x=split/\s+/;printf " %s", \$x[1];' | xargs -i% kill %
fi
# ---- end - remote_script_end.sh - source

EOT

# ---- begin - data_wrapper_footer.sh - source
   log_debug " > target : ${MACHINE} "
   log_info " > executing : ${_ssh} | ${_script_name}"
   #${verboseFlag}
   if [[ "${local_base_dir}" != "." ]]; then
      [ ! -e ${local_base_dir} ] && mkdir -p "${local_base_dir}"
   fi
   if [[ ! -e "${local_base_dir}/master-${_uuid}.sh" ]]; then
      cat <<EOX > "${local_base_dir}/master-${_uuid}.sh"
#!/bin/bash -s
timestamp() {
  while IFS= read -r line; do
      echo "\$(date) - \$line"
  done
}
EOX
   fi
   timescript=""
   script=""
   if [[  `which timeout` != '' ]]; then
      timescript="timeout -s SIGKILL ${_timeout}s"
   fi
   cat <<EOXX >> "${local_base_dir}/master-${_uuid}.sh"
echo " __ \ /  __  __     ___  __ (${_timeout})"
echo "|_   X  |_  /   | |  |  |_ (${_ssh})"
echo "|__ / \ |__ \__ |_|  |  |__ [${local_base_dir}/${_script_name}]"
EOXX
   printf -v script '%s /usr/bin/ssh -n %s %s %s "$(<%s.txt)" 2>&1  >> "%s/stdout-%s-%s.log" &' \
   "${timescript}" "${_ssh}" "${_identity_}" "${_ssh_options}" "${local_base_dir}/${_script_name}" \
   "${local_base_dir}" "${_uuid}" "${_host}"
   echo -E "$script" >> "${local_base_dir}/master-${_uuid}.sh"
done # ---

if [[ "${_no_wait}" == "" ]]; then
   echo "echo starting" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "wait" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "echo completed" >> "${local_base_dir}/master-${_uuid}.sh"
fi


log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - data_wrapper_footer.sh - source

log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - master_execute.sh - source
