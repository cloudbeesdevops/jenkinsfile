#!/bin/bash
#set -e

export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=deploy:action=deploy, on level
level=L1
APP_NM=L1-performance-entity-identifier-service
# ---- begin - data_wrapper_header.sh - source
#echo "[ERROR]: $@"
#echo "[INFO] : $@"
#set -xv
#echo "[WARN] : $@"

set -m # Enable Job Control
_uuid="performance-entity-identifier-service"
_workdir="/home/ak751818/fdeploy/config.orchestration-deploy"
_stagedir="/home/ak751818/fdeploy/tmp"
_timeout="20"
_archive_version="1.0.1"
_archive_filename="entity-identifier-service.jar"
_artifactId="fxs-sefs-common-java-entity-identifier-service"
_target_path="${_stagedir}/archives"

if [[ -z "${MANIFEST_FILE}" ]]; then
  if [[ -f "${_workdir}/apps/${_uuid}/manifest-${level}.yml" ]]; then
    export MANIFEST_FILE="${_workdir}/apps/${_uuid}/manifest-${level}.yml"
  else
    export MANIFEST_FILE="${_workdir}/apps/${_uuid}/manifest.yml"
  fi
else
  echo "OVERWRITING MANIFEST FILE LOGIC with hardcoded manifest @ ${MANIFEST_FILE}"
fi

# Generating the current properties for this version
cat <<EOF > "${_stagedir}/${_archive_version}-${APP_NM}${_uuid}.properties"
#!/bin/bash
# Generated property file for application.
export API_URL="api.sys.wtcdev2.paas.fedex.com"
export APPD_NM="performance-entity-identifier-service"
export APP_HOST="L1-performance-entity-identifier-service"
export APP_NM="L1-performance-entity-identifier-service"
export ARTIFACTID="fxs-sefs-common-java-entity-identifier-service"
export CF_HOME="/tmp/tmp99Q53s"
export CONFIG_BRANCH_NM="master"
export DOMAIN="app.wtcdev2.paas.fedex.com"
export EAI_NUMBER="3534641"
export GROUPID="com.fedex.fxs.sefs.common.java.rest"
export LEVEL="L1"
export LOG_LEVEL="1"
export NFR_TEST="nfr_test_into_L1"
export NUM_INSTANCES="1"
export PAM_ID="18410"
export PCF_PASSWORD="gM8lh4zP468HcDgL"
export PCF_USER="FXS_app3534545"
export SERVICE_INSTANCES="1"
export SPACE="development"
export SPRING_PROFILES_ACTIVE="L1"
export UUID_DOMAIN_LOWERCASE="orchestration"
export UUID_DOMAIN_TITLECASE="Orchestration"
export UUID_EAI_NUMBER="3534681"
export VERSION="1.0.1"

EOF

# load the RTV's
source ${_stagedir}/${_archive_version}-${APP_NM}${_uuid}.properties

# expanding variables in YML
while read
do
    eval "echo \"${REPLY}\""
done < "${MANIFEST_FILE}" > ${_stagedir}/manifest-${_uuid}.yml

echo "------generated manifest------"
cat ${_stagedir}/manifest-${_uuid}.yml
echo "------generated manifest------"


hostname
which cf
cf_exists=${?}
if [[ "${cf_exists}" != 0 ]]; then
  echo "CloudFoundry_CLI was not found. Abort! (code=${cf_exists})"
  echo "Please export PATH with location of the cf binary (f.e: /opt/fedex/tibco/cf-cli/bin)"
  exit ${cf_exists}
else
  echo "CloudFoundry_CLI found at $(which cf)"
fi

echo "Login to ${API_URL}"
cf api https://${API_URL}
cf auth ${PCF_USER} ${PCF_PASSWORD}

echo "Set target space '${SPACE}'"
cf target -o ${EAI_NUMBER} -s ${SPACE}
if [[ "${?}" != 0 ]]; then
  echo "Error setting space ${SPACE} in org ${EAI_NUMBER}"
  exit 2
fi

#!/bin/bash
export API_URL="api.sys.wtcdev2.paas.fedex.com"
export APPD_NM="performance-entity-identifier-service"
export APP_HOST="L1-performance-entity-identifier-service"
export APP_NM="L1-performance-entity-identifier-service"
export ARTIFACTID="fxs-sefs-common-java-entity-identifier-service"
export CF_HOME="/tmp/tmp99Q53s"
export CONFIG_BRANCH_NM="master"
export DOMAIN="app.wtcdev2.paas.fedex.com"
export EAI_NUMBER="3534641"
export GROUPID="com.fedex.fxs.sefs.common.java.rest"
export LEVEL="L1"
export LOG_LEVEL="1"
export NFR_TEST="nfr_test_into_L1"
export NUM_INSTANCES="1"
export PAM_ID="18410"
export PCF_PASSWORD="gM8lh4zP468HcDgL"
export PCF_USER="FXS_app3534545"
export SERVICE_INSTANCES="1"
export SPACE="development"
export SPRING_PROFILES_ACTIVE="L1"
export UUID_DOMAIN_LOWERCASE="orchestration"
export UUID_DOMAIN_TITLECASE="Orchestration"
export UUID_EAI_NUMBER="3534681"
export VERSION="1.0.1"

cf set-env ${APP_NM} "some.new.setting" "NFR"
# --- status ---
APP_EXISTS=$(cf get-health-check ${APP_NM})
if [[ "${APP_EXISTS}" != *"FAILED"* ]]; then
  export app_exists=1
  echo "Application ${APP_NM} Already Exists, refreshing (code=${app_exists})"
else
  export app_exists=0
  echo "Application ${APP_NM} Does not Exist, initializing (code=${app_exists})"
fi
# --- status end ---

# --- upload ---
echo "Pushing ${_target_path}/${_archive_filename} in ${SPACE}"
cf app ${APP_NM}-green > /dev/null 2>&1
green_exists=${?}
if [[ "${green_exists}" -eq 0 ]]; then
	cf delete -f ${APP_NM}-green
fi
cf p ${APP_NM} -f ${_stagedir}/manifest-${_uuid}.yml -p ${_target_path}/${_archive_filename} --hostname ${APP_HOST} ${PCF_PUSH_OPTIONS} --vars-file <(echo EUREKA_NAME: ${APP_NM})
# --- end upload ---

cf start ${APP_NM}

# ---- begin - pcf_wrapper_footer.sh - source

# ---- end - pcf_wrapper_footer.sh - source
