#!/bin/bash
#set -e
#export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=control:action=clean_with_manifest, on level
_level=L1
level=${_level}
# ---- begin - data_wrapper_header.sh - source
function log_error {
   set > /dev/null
   echo "[ERROR]: $@"
}
function log_info {
   set > /dev/null
   echo "[INFO] : $@"
}
function log_debug {
   set > /dev/null
   echo "[DEBUG]: $@"
}
function log_warn {
   set > /dev/null
   echo "[WARN] : $@"
}
function join_by {
   local IFS="$1"; shift; echo "\$*";
}

set -m # Enable Job Control
_action="clean_with_manifest"
_identity_="-i ."
_ssh_options="-oConnectTimeout=20 -oStrictHostKeyChecking=no -oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null"
_verbose_flag="set +xv"
_echo_envvars="set +xv"
_switch="sefscm"
_uuid="fxe-share-cache-service"
_no_wait="#SKIP#"
_timeout="20"

declare -a MACHINE_LIST=(
   "sefscm@adrda01.test.cloud.fedex.com:/opt/fedex/sefs/share-cache-plugin/"
   "sefscm@adrda02.test.cloud.fedex.com:/opt/fedex/sefs/share-cache-plugin/"
 );
declare -a UPLOAD_CONTENTS=(
"share-cache-plugin.zip:1.0.3:::" "share_cache_plugin_resources.zip:1.0.3:config::" "share_cache_plugin_security.zip:.:/local:775:" );
_upload_elements=""share-cache-plugin.zip:1.0.3:::" "share_cache_plugin_resources.zip:1.0.3:config::" "share_cache_plugin_security.zip:.:/local:775:""

log_debug "elements ${_upload_elements}"

# set working base on the local host
local_base_dir="/Users/ak751818/git/xopco-common-tools-fdeploy/src/main/python/tmp"
[ ! -e "${local_base_dir}" ] && mkdir -p "${local_base_dir}"
[ ! -e "${local_base_dir}" ] && log_error " ! aborting unable creating : ${local_base_dir}" && exit 1
cd "${local_base_dir}"
#
for MACHINE in ${MACHINE_LIST[@]}; do
   IFS=':' read -ra URL <<< "$MACHINE"
   log_debug " > machine=${URL}"
   _uri_path="${URL[1]}"
   _url="${URL[0]}"
   _archive_version="1.0.3"
   _archive_filename="share-cache-plugin.zip"
   _artifactId="share-cache-plugin"
   IFS='@' read -ra HOST <<< "$_url"
   _user="${HOST[0]}"
   _host="${HOST[1]}"
   _ssh="${_user}@${_host}"
   if [[ "${_switch}" == "" ]]; then
      if [[ "${_user}" == *cm ]]; then
         b=${_user%cm}
         _ssh="${b}@${_host}";
         _user="${b}"
      fi
   fi
   _landing_area="/var/tmp/${_user}"
   IFS='?' read -ra URI <<< "$_uri_path"
   _target_path="${URI[0]}"
   _uri="${URI[1]}"
   _script_name="${_host}${_uuid}-${_action}"
   log_info  " < processing : target=${MACHINE} sudo=${_switch} "
   log_debug " < host=${_host} | url_path=${_uri_path} | ssh=${_ssh}"
# ---- end - data_wrapper_header.sh - source

# ---- begin - remote_script_begin.sh - source

# generate remote install
log_info " > writing remote script : ${local_base_dir}/${_script_name}"
cat <<EOT > "${local_base_dir}/${_script_name}.txt"
#!/bin/bash
# --- begin ${local_base_dir}/${_script_name}.txt
log_error() {
  set > /dev/null
  echo "[ERROR]: \$@"
}
log_info()  {
  set > /dev/null
  echo "[INFO] : \$@"
}
log_debug() {
  set > /dev/null
  echo "[DEBUG]: \$@"
}
log_warn()  {
  set > /dev/null
  echo "[WARN] : \$@"
}
timestamp() {
    while IFS= read -r line; do
        echo "$(date) - $line"
    done
}

echo "====================================="
echo "DATE: `date` / USER: `whoami` / HOST: `hostname`" - PID: $$
echo "====================================="

# ENVIRONMENT SETTINGS
export APPD_ACCOUNT="fedex1-test"
export APPD_APPNAME="SU-SEFS-RELY-L1"
export APPD_CONTROLLER_HOST="fedex1-test.saas.appdynamics.com"
export ARTIFACTID="share-cache-plugin"
export CANDIDATES="1.0.3,0.0.1,0.1.0"
export EUREKA_HOST="urh00600.ute.fedex.com"
export GROUPID="com.fedex.sefs.cache.plugins"
export HTTP_PORT="8096"
export LEVEL="L1"
export SPRING_PROFILE="L1"
export VERSION="1.0.3"

#
#
set +xv

# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[2019-07-29 20:24:55.954634] action=clean_with_manifest - version-info=
EOF
# ---- begin - remote_readme_snippet.sh - source #
cat "${_target_path}/${_archive_version}/README" >> "${_target_path}/fdeploy.log"
# ---- end - remote_readme_snippet.sh - source

/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source

#
# ---- begin - remote_cleanup_with_manifest_snippet.sh - source
cd "${_target_path}"
CURRENTLINK=\$(readlink current)
log_info "current link version \${CURRENTLINK}"
PREVIOUSLINK=\$(readlink previous)
log_info "previous link version \${PREVIOUSLINK}"
CURRENT_VERSIONS=\$(find *  -type d -prune | xargs ls -1rtd )
if [[ "\${CANDIDATES}" != "" ]]; then
  IFS=',' read -r -a array <<< "\${CANDIDATES}"
  log_debug "Search candidates \${CANDIDATES}"
  LIST_OF_VERSIONS=""
  for CANDIDATE in \${CURRENT_VERSIONS} ; do
    if [[ "\${CANDIDATE##*/}" == "\${CURRENTLINK}" ]] ; then
      log_warn "Skipping \${CANDIDATE##*/} as it is the currently linked version"
    elif [[ "\${CANDIDATE##*/}" == "\${PREVIOUSLINK}" ]] ; then
      log_warn "Skipping \${CANDIDATE##*/} as it is the previously linked version"
    elif [[ ! -d "\${CANDIDATE}" ]] ; then
      log_warn "Skipping \${CANDIDATE} as it does not appear to be a directory?"
    else
      MATCH=""
      for candidate in \${array[@]} ; do
        if [[ "\${candidate}" == "\${CANDIDATE}" ]] ; then
          if [[ -d "\${candidate}" ]]; then
            MATCH="\${CANDIDATE}"
          fi
        fi
      done
      if [[ "\${MATCH}" ==  "" ]]; then
        if [[ "\${LIST_OF_VERSIONS}" == "" ]]; then
          LIST_OF_VERSIONS="\${CANDIDATE}"
        else
          LIST_OF_VERSIONS="\${LIST_OF_VERSIONS},\${CANDIDATE}"
        fi
      fi
    fi
  done
  log_debug "processing unwanted items \${LIST_OF_VERSIONS}"
  IFS=',' read -r -a array <<< "\${LIST_OF_VERSIONS}"
  for candidate in \${array[@]} ; do
    if [[ -d "\${candidate}" ]]; then
      log_info "Removing old install: \${candidate}"
      rm -rf \${candidate} \${candidate}.properties
    fi
  done
else
  echo "╔╗╔╔═╗  ╔═╗╔═╗╔╗╔╔╦╗╦╔╦╗╔═╗╔╦╗╔═╗╔═╗";
  echo "║║║║ ║  ║  ╠═╣║║║ ║║║ ║║╠═╣ ║ ║╣ ╚═╗";
  echo "╝╚╝╚═╝  ╚═╝╩ ╩╝╚╝═╩╝╩═╩╝╩ ╩ ╩ ╚═╝╚═╝";
fi
# ---- end - remote_cleanup_with_manifest_snippet.sh - source

# ---- begin - remote_script_end.sh - source
log_debug "Leaving remote execution, looking for ${_user}@notty :"
# find orphaned ssh session due to the remote invocation
NOTTY_PRCS=\$(ps -u ${_user} -f | grep '@notty' | grep -v grep | grep -v APPD)
#
log_debug "Echoing pids: \${NOTTY_PRCS}"
#
[[ "\${NOTTY_PRCS}" != "" ]] && log_debug "found orphaned processes ${NOTTY}"
if [[ "\${NOTTY_PRCS}" != "" ]]; then
   echo "\${NOTTY_PRCS}" | perl -ne '@x=split/\s+/;printf " %s", \$x[1];' | xargs -i% kill %
fi
# ---- end - remote_script_end.sh - source

EOT

# ---- begin - data_wrapper_footer.sh - source
   log_debug " > target : ${MACHINE} "
   log_info " > executing : ${_ssh} | ${_script_name}"
   #${verboseFlag}
   if [[ "${local_base_dir}" != "." ]]; then
      [ ! -e ${local_base_dir} ] && mkdir -p "${local_base_dir}"
   fi
   if [[ ! -e "${local_base_dir}/master-${_uuid}.sh" ]]; then
      cat <<EOX > "${local_base_dir}/master-${_uuid}.sh"
#!/bin/bash -s
timestamp() {
  while IFS= read -r line; do
      echo "\$(date) - \$line"
  done
}
EOX
   fi
   timescript=""
   script=""
   if [[  `which timeout` != '' ]]; then
      timescript="timeout -s SIGKILL ${_timeout}s"
   fi
   cat <<EOXX >> "${local_base_dir}/master-${_uuid}.sh"
echo " __ \ /  __  __     ___  __ (${_timeout})"
echo "|_   X  |_  /   | |  |  |_ (${_ssh})"
echo "|__ / \ |__ \__ |_|  |  |__ [${local_base_dir}/${_script_name}]"
EOXX
   printf -v script '%s /usr/bin/ssh -n %s %s %s "$(<%s.txt)" 2>&1  >> "%s/stdout-%s-%s.log" &' \
   "${timescript}" "${_ssh}" "${_identity_}" "${_ssh_options}" "${local_base_dir}/${_script_name}" \
   "${local_base_dir}" "${_uuid}" "${_host}"
   echo -E "$script" >> "${local_base_dir}/master-${_uuid}.sh"
done # ---

if [[ "${_no_wait}" == "" ]]; then
   echo "echo starting" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "wait" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "echo completed" >> "${local_base_dir}/master-${_uuid}.sh"
fi


log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - data_wrapper_footer.sh - source

log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - master_execute.sh - source
