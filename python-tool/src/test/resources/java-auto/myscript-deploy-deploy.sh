#!/bin/bash
#set -e
#export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=deploy:action=stop, on level
_level=L1
level=${_level}
# ---- begin - data_wrapper_header.sh - source
function log_error {
   set > /dev/null
   echo "[ERROR]: $@"
}
function log_info {
   set > /dev/null
   echo "[INFO] : $@"
}
function log_debug {
   set > /dev/null
   echo "[DEBUG]: $@"
}
function log_warn {
   set > /dev/null
   echo "[WARN] : $@"
}
function join_by {
   local IFS="$1"; shift; echo "\$*";
}

set -m # Enable Job Control
_action="stop"
_identity_="-i /home/ak751818/.ssh/test_deploy_user_dsa"
_ssh_options="-oConnectTimeout=20 -oStrictHostKeyChecking=no -oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null"
_verbose_flag="set +xv"
_echo_envvars="set +xv"
_switch="tibcosilver"
_uuid="dashboard-web"
_no_wait="#SKIP#"
_timeout="20"

declare -a MACHINE_LIST=(
   "tibcosilver@irh00601.ute.fedex.com:/opt/tibco/silver/persistentdata/FXS/apps/fxs-dashboard-web"
 );
declare -a UPLOAD_CONTENTS=(
"dashboard-web.zip:1.2.3:::" );
_upload_elements=""dashboard-web.zip:1.2.3:::""

log_debug "elements ${_upload_elements}"

# set working base on the local host
local_base_dir="/home/ak751818/fdeploy/tmp"
[ ! -e "${local_base_dir}" ] && mkdir -p "${local_base_dir}"
[ ! -e "${local_base_dir}" ] && log_error " ! aborting unable creating : ${local_base_dir}" && exit 1
cd "${local_base_dir}"
#
for MACHINE in ${MACHINE_LIST[@]}; do
   IFS=':' read -ra URL <<< "$MACHINE"
   log_debug " > machine=${URL}"
   _uri_path="${URL[1]}"
   _url="${URL[0]}"
   _archive_version="1.2.3"
   _archive_filename="dashboard-web.zip"
   _artifactId="dashboard-web"
   IFS='@' read -ra HOST <<< "$_url"
   _user="${HOST[0]}"
   _host="${HOST[1]}"
   _ssh="${_user}@${_host}"
   if [[ "${_switch}" == "" ]]; then
      if [[ "${_user}" == *cm ]]; then
         b=${_user%cm}
         _ssh="${b}@${_host}";
         _user="${b}"
      fi
   fi
   _landing_area="/var/tmp/${_user}"
   IFS='?' read -ra URI <<< "$_uri_path"
   _target_path="${URI[0]}"
   _uri="${URI[1]}"
   _script_name="${_host}${_uuid}-${_action}"
   log_info  " < processing : target=${MACHINE} sudo=${_switch} "
   log_debug " < host=${_host} | url_path=${_uri_path} | ssh=${_ssh}"
# ---- end - data_wrapper_header.sh - source

# ---- begin - remote_script_begin.sh - source

# generate remote install
log_info " > writing remote script : ${local_base_dir}/${_script_name}"
cat <<-EOT > "${local_base_dir}/${_script_name}.txt"
#!/bin/bash
# --- begin ${local_base_dir}/${_script_name}.txt
log_error() {
  set > /dev/null
  echo "[ERROR]: \$@"
}
log_info()  {
  set > /dev/null
  echo "[INFO] : \$@"
}
log_debug() {
  set > /dev/null
  echo "[DEBUG]: \$@"
}
log_warn()  {
  set > /dev/null
  echo "[WARN] : \$@"
}
timestamp() {
    while IFS= read -r line; do
        echo "$(date) - $line"
    done
}

echo "====================================="
echo "DATE: `date` / USER: `whoami` / HOST: `hostname`" - PID: $$
echo "====================================="

# ENVIRONMENT SETTINGS
export APPD_ACCOUNT="fedex1-test"
export APPD_ACCOUNT_KEY="1dce5fc52c17"
export APPD_APPNAME="SU-SEFS-RELY"
export APPD_CONTROLLER_HOST="fedex1-test.saas.appdynamics.com"
export APP_ID="dashboard-web-FXG"
export APP_PATH="/opt/fedex/sefs/dashboard-web-FXG/current/bin"
export ARTIFACTID="dashboard-web"
export EAI_NUMBER="6268"
export EUREKA_PORT="8097"
export GROUPID="com.fedex.sefs.dashboard"
export HTTP_PORT="8096"
export JAVA_HOME="/opt/java/hotspot/8/current/"
export JAVA_TOOL_OPTIONS="-Dlogging.info.file=/var/fedex/sefs/logs/dashboard-web-FXG/sefs__dashboard-web-FXG_application_info.log -Dlogging.error.file=/var/fedex/sefs/logs/dashboard-web-FXG/sefs__dashboard-web-FXG__application_error.log -Dspring.config.location=classpath:application.yml,file:/opt/fedex/sefs/dashboard-web-FXG/current/config/L1/tem_application.properties"
export JMX_PORT="7091"
export LEVEL="L1"
export SPRING_PROFILE="oracle,L0,wsso"
export TMP_DIR1="/var/fedex/sefs/logs/dashboard-web-FXG/"
export VERSION="1.2.3"

#
#
set +xv

# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[2019-07-31 02:39:51.057554] action=stop
EOF

/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source

# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[2019-07-31 02:39:51.057554] action=stop
EOF

/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source

# ---- begin - remote_stop_snippet.sh - source
if [[ -e "${_target_path}/current/scripts" ]]; then
   cd ${_target_path}/current/scripts
   echo -n "STATUS: ${_host}:${_archive_filename}" && ./status.sh
   if [ \$? -eq 0 ]; then
      echo "found process, stopping... ${_archive_version}"
      ./stop.sh
      sleep 5
      echo -n "STATUS: ${_host}:${_archive_filename}" && ./status.sh
      # now we should get >0 back since not running.
      if [[ \$? -ne 0 || "\$?" != "" ]]; then
         echo "╔═╗╔╦╗╔═╗╔═╗╔═╗╔═╗╔╦╗";
	      echo "╚═╗ ║ ║ ║╠═╝╠═╝║╣  ║║${_artifactId}-${_archive_version}";
	      echo "╚═╝ ╩ ╚═╝╩  ╩  ╚═╝═╩╝(${_user}@${_host})";
	   else
         echo "┌─┐┌┬┐┌─┐┌─┐  ┌─┐┌─┐┬┬  ┬ ┬┬─┐┌─┐";
         echo "└─┐ │ │ │├─┘  ├┤ ├─┤││  │ │├┬┘├┤ ${_artifactId}-${_archive_version}";
         echo "└─┘ ┴ └─┘┴    └  ┴ ┴┴┴─┘└─┘┴└─└─┘(${_user}@${_host})";
      fi
   else
      echo "╔═╗╔╦╗╔═╗╔═╗╔═╗╔═╗╔╦╗";
      echo "╚═╗ ║ ║ ║╠═╝╠═╝║╣  ║║${_artifactId}-${_archive_version}";
      echo "╚═╝ ╩ ╚═╝╩  ╩  ╚═╝═╩╝(${_user}@${_host})";
   fi
else
   echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
   echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}-${_archive_version}";
   echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
   echo " ! aborting no current symlink available ${_target_path}/current."  && exit 1
fi
# ---- end - remote_stop_snippet.sh - source

# ---- begin - remote_script_end.sh - source
log_debug "Leaving remote execution, looking for ${_user}@notty :"
# find orphaned ssh session due to the remote invocation
NOTTY_PRCS=\$(ps -u ${_user} -f | grep '@notty' | grep -v grep | grep -v APPD)
#
log_debug "Echoing pids: \${NOTTY_PRCS}"
#
[[ "\${NOTTY_PRCS}" != "" ]] && log_debug "found orphaned processes ${NOTTY}"
if [[ "\${NOTTY_PRCS}" != "" ]]; then
   echo "\${NOTTY_PRCS}" | perl -ne '@x=split/\s+/;printf " %s", \$x[1];' | xargs -i% kill %
fi
# ---- end - remote_script_end.sh - source

EOT

# ---- begin - data_wrapper_footer.sh - source
   log_debug " > target : ${MACHINE} "
   log_info " > executing : ${_ssh} | ${_script_name}"
   #${verboseFlag}
   if [[ "${local_base_dir}" != "." ]]; then
      [ ! -e ${local_base_dir} ] && mkdir -p "${local_base_dir}"
   fi
   if [[ ! -e "${local_base_dir}/master-${_uuid}.sh" ]]; then
      cat <<EOX > "${local_base_dir}/master-${_uuid}.sh"
#!/bin/bash -s
timestamp() {
  while IFS= read -r line; do
      echo "\$(date) - \$line"
  done
}
EOX
   fi
   timescript=""
   script=""
   if [[  `which timeout` != '' ]]; then
      timescript="timeout -s SIGKILL ${_timeout}s"
   fi
   cat <<EOXX >> "${local_base_dir}/master-${_uuid}.sh"
echo " __ \ /  __  __     ___  __ (${_timeout})"
echo "|_   X  |_  /   | |  |  |_ (${_ssh})"
echo "|__ / \ |__ \__ |_|  |  |__ [${local_base_dir}/${_script_name}]"
EOXX
   printf -v script '%s /usr/bin/ssh -n %s %s %s "$(<%s.txt)" 2>&1  >> "%s/stdout-%s-%s.log" &' \
   "${timescript}" "${_ssh}" "${_identity_}" "${_ssh_options}" "${local_base_dir}/${_script_name}" \
   "${local_base_dir}" "${_uuid}" "${_host}"
   echo -E "$script" >> "${local_base_dir}/master-${_uuid}.sh"
done # ---

if [[ "${_no_wait}" == "" ]]; then
   echo "echo starting" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "wait" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "echo completed" >> "${local_base_dir}/master-${_uuid}.sh"
fi


log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - data_wrapper_footer.sh - source

#!/bin/bash
#set -e
#export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=deploy:action=install, on level
_level=L1
level=${_level}
# ---- begin - data_wrapper_header.sh - source
function log_error {
   set > /dev/null
   echo "[ERROR]: $@"
}
function log_info {
   set > /dev/null
   echo "[INFO] : $@"
}
function log_debug {
   set > /dev/null
   echo "[DEBUG]: $@"
}
function log_warn {
   set > /dev/null
   echo "[WARN] : $@"
}
function join_by {
   local IFS="$1"; shift; echo "\$*";
}

set -m # Enable Job Control
_action="install"
_identity_="-i /home/ak751818/.ssh/test_deploy_user_dsa"
_ssh_options="-oConnectTimeout=20 -oStrictHostKeyChecking=no -oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null"
_verbose_flag="set +xv"
_echo_envvars="set +xv"
_switch="tibcosilver"
_uuid="dashboard-web"
_no_wait="#SKIP#"
_timeout="20"

declare -a MACHINE_LIST=(
   "tibcosilver@irh00601.ute.fedex.com:/opt/tibco/silver/persistentdata/FXS/apps/fxs-dashboard-web"
 );
declare -a UPLOAD_CONTENTS=(
"dashboard-web.zip:1.2.3:::" );
_upload_elements=""dashboard-web.zip:1.2.3:::""

log_debug "elements ${_upload_elements}"

# set working base on the local host
local_base_dir="/home/ak751818/fdeploy/tmp"
[ ! -e "${local_base_dir}" ] && mkdir -p "${local_base_dir}"
[ ! -e "${local_base_dir}" ] && log_error " ! aborting unable creating : ${local_base_dir}" && exit 1
cd "${local_base_dir}"
#
for MACHINE in ${MACHINE_LIST[@]}; do
   IFS=':' read -ra URL <<< "$MACHINE"
   log_debug " > machine=${URL}"
   _uri_path="${URL[1]}"
   _url="${URL[0]}"
   _archive_version="1.2.3"
   _archive_filename="dashboard-web.zip"
   _artifactId="dashboard-web"
   IFS='@' read -ra HOST <<< "$_url"
   _user="${HOST[0]}"
   _host="${HOST[1]}"
   _ssh="${_user}@${_host}"
   if [[ "${_switch}" == "" ]]; then
      if [[ "${_user}" == *cm ]]; then
         b=${_user%cm}
         _ssh="${b}@${_host}";
         _user="${b}"
      fi
   fi
   _landing_area="/var/tmp/${_user}"
   IFS='?' read -ra URI <<< "$_uri_path"
   _target_path="${URI[0]}"
   _uri="${URI[1]}"
   _script_name="${_host}${_uuid}-${_action}"
   log_info  " < processing : target=${MACHINE} sudo=${_switch} "
   log_debug " < host=${_host} | url_path=${_uri_path} | ssh=${_ssh}"
# ---- end - data_wrapper_header.sh - source

# ---- begin - upload_snippet.sh - source
   #printf -v _local_archive_product "%s " "${UPLOAD_CONTENTS[@]}"
   for UPLOAD in ${UPLOAD_CONTENTS[@]}; do
      IFS=':' read -r -a contents <<< "${UPLOAD}"
      _local_archive="${contents[0]}"
      _path="${contents[1]}"
      _local_archive_product="${local_base_dir}/archives/${_local_archive}"
      log_debug " > upload  : (${_user}@${_host})";
      log_info " < uploading : _local_archive_products=${_local_archive_product} | _landing_area=${_landing_area}"
      log_debug " < processing : _target_path=${_target_path} | uri=${_uri} "
      #echo -n "PWD= " && pwd
      echo ${_local_archive_product}
      if [[ -e "${_local_archive_product}" ]]; then
         log_info " > uploading archive to ${_user}@${_host}:${_landing_area}, landing area: ${_landing_area}"
         /usr/bin/ssh ${_identity_} ${_ssh_options} "${_ssh}" "[[ ! -e ${_landing_area} ]] && mkdir -p ${_landing_area}" 2>&1 | tee upload_ssh.log
         log_debug " > ${MACHINE} --> ${_user}@${_host}:${_landing_area}"
         /usr/bin/scp ${_identity_} ${_ssh_options} ${_extract_contents} ${_local_archive_product} ${_user}@${_host}:${_landing_area} </dev/null 2>&1 | tee upload_ssh.log
         if [ $? -eq 0 ]; then
            echo "╦ ╦╔═╗╦  ╔═╗╔═╗╔╦╗╔═╗╔╦╗";
            echo "║ ║╠═╝║  ║ ║╠═╣ ║║║╣  ║║upload_area=${_landing_area}";
            echo "╚═╝╩  ╩═╝╚═╝╩ ╩═╩╝╚═╝═╩╝${_user}@${_host})"
            echo "target_path=${_target_path}; local_archive=${_local_archive_product}";
         else
            echo "╦ ╦╔═╗╦  ╔═╗╔═╗╔╦╗  ╔═╗╔═╗┬╦  ╔═╗╔╦╗";
            echo "║ ║╠═╝║  ║ ║╠═╣ ║║  ╠╣ ╠═╣│║  ║╣  ║║${_local_archive_product}";
            echo "╚═╝╩  ╩═╝╚═╝╩ ╩═╩╝  ╚  ╩ ╩┴╩═╝╚═╝═╩╝${_user}@${_host})";
            exit 1
         fi

      else
         echo "╦ ╦╔═╗╦  ╔═╗╔═╗╔╦╗  ╔═╗╦═╗╔╗ ╔═╗╦═╗╔╦╗╔═╗╔╦╗";
         echo "║ ║╠═╝║  ║ ║╠═╣ ║║  ╠═╣╠╦╝╠╩╗║ ║╠╦╝ ║ ║╣  ║║${_local_archive_product}";
         echo "╚═╝╩  ╩═╝╚═╝╩ ╩═╩╝  ╩ ╩╩╚═╚═╝╚═╝╩╚═ ╩ ╚═╝═╩╝${_user}@${_host})";
         echo " ! aborting no source material ${_local_archive_product} found for upload."
         exit 1
      fi
      if [[ ! -e "${_local_archive_product}" ]]; then
         echo " ! failed to write source material ${_local_archive_product} for upload."
         exit 1
      fi
   done
# ---- end - upload_snippet.sh - source

# ---- begin - remote_script_begin.sh - source

# generate remote install
log_info " > writing remote script : ${local_base_dir}/${_script_name}"
cat <<-EOT > "${local_base_dir}/${_script_name}.txt"
#!/bin/bash
# --- begin ${local_base_dir}/${_script_name}.txt
log_error() {
  set > /dev/null
  echo "[ERROR]: \$@"
}
log_info()  {
  set > /dev/null
  echo "[INFO] : \$@"
}
log_debug() {
  set > /dev/null
  echo "[DEBUG]: \$@"
}
log_warn()  {
  set > /dev/null
  echo "[WARN] : \$@"
}
timestamp() {
    while IFS= read -r line; do
        echo "$(date) - $line"
    done
}

echo "====================================="
echo "DATE: `date` / USER: `whoami` / HOST: `hostname`" - PID: $$
echo "====================================="

# ENVIRONMENT SETTINGS
export APPD_ACCOUNT="fedex1-test"
export APPD_ACCOUNT_KEY="1dce5fc52c17"
export APPD_APPNAME="SU-SEFS-RELY"
export APPD_CONTROLLER_HOST="fedex1-test.saas.appdynamics.com"
export APP_ID="dashboard-web-FXG"
export APP_PATH="/opt/fedex/sefs/dashboard-web-FXG/current/bin"
export ARTIFACTID="dashboard-web"
export EAI_NUMBER="6268"
export EUREKA_PORT="8097"
export GROUPID="com.fedex.sefs.dashboard"
export HTTP_PORT="8096"
export JAVA_HOME="/opt/java/hotspot/8/current/"
export JAVA_TOOL_OPTIONS="-Dlogging.info.file=/var/fedex/sefs/logs/dashboard-web-FXG/sefs__dashboard-web-FXG_application_info.log -Dlogging.error.file=/var/fedex/sefs/logs/dashboard-web-FXG/sefs__dashboard-web-FXG__application_error.log -Dspring.config.location=classpath:application.yml,file:/opt/fedex/sefs/dashboard-web-FXG/current/config/L1/tem_application.properties"
export JMX_PORT="7091"
export LEVEL="L1"
export SPRING_PROFILE="oracle,L0,wsso"
export TMP_DIR1="/var/fedex/sefs/logs/dashboard-web-FXG/"
export VERSION="1.2.3"

#
#
set +xv

# ---- begin - remote_overwrite_snippet.sh - source
# overwrite previous install of the same version
echo "BEGIN ---OVERWRITE SECTION"
PREVIOUSLINK=\$(readlink current)
if [[ "$_archive_version" == "\${PREVIOUSLINK}" ]]; then
  if [[ -e "${_archive_version}" ]]; then
     log_warn " ! removing previous version ${_archive_version}"
     rm -fr "${_archive_version}"
  fi
fi
# remove the link to properties
[ -e  "${_target_path}/${_archive_version}.properties" ] && rm "${_target_path}/${_archive_version}.properties"
# ---- end - remote_overwrite_snippet.sh - source
echo "END -- -OVERWRITE SECTION"

# ---- begin - remote_stage_start_snippet.sh - source
echo "╔═╗╔╦╗╔═╗╔═╗╦╔╗╔╔═╗";
echo "╚═╗ ║ ╠═╣║ ╦║║║║║ ╦${_artifactId}";
echo "╚═╝ ╩ ╩ ╩╚═╝╩╝╚╝╚═╝(${_user}@${_host})";
log_info " > landing area ${_landing_area} "
# unpack the archive in the versioned directory
# if already started, stop the current stack first
set -xv
if [[ -e "${_landing_area}/${_archive_filename}" ]]; then
    # install path
    if [[ ! -e "${_target_path}" ]]; then
      mkdir -p "${_target_path}"
    fi
    # unpacking all uploaded archives

    log_debug "staging elements '${_upload_elements}'"
    IFS=' ' read -r -a elements <<< "${_upload_elements}"
    for index in "\${!elements[@]}"
    do

      IFS=':' read -r -a contents <<< "\${elements[index]}"
      _file="\${contents[0]}"
      _path="\${contents[1]}"
      _dirs="\${contents[2]}"
      _perms="\${contents[3]}"
      _owner="\${contents[4]}"

      cd "${_target_path}"
      if [[ "\${_file}" == *zip ]]; then
         log_info "unpacking \${elements[index]} on path \${_path}"
         unzip -q -o -d "\${_path}" "${_landing_area}/\${_file}"
      else
         log_info "copying \${elements[index]} to \${_path}/bin"
         [[ ! -e  "\${_path}/bin" ]] && mkdir -p "\${_path}/bin"
         cp -v "${_landing_area}/\${_file}"  "\${_path}/bin"
      fi
      cd "\${_path}"
      if [[ -e "scripts" ]]; then
         chmod -R 755 scripts/*
      fi
      if [[ "\${_perms}" != "" ]] && [[ "\${_dirs}" != "" ]]; then
         log_info "changing permissions to \${_perms} for "
         chmod -vR "\${_perms}" ${_target_path}/\${_dirs}
      fi
      if [[ "\${_owner}" != "" ]] && [[ "\${_dirs}" != "" ]]; then
         log_info "changing ownership to \${_owner}"
         chown -vR "\${_owner}" ${_target_path}/\${_dirs}
      fi
	 done
   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
%s
EOF

# ---- end - remote_stage_snippet_begin.sh - source

# ---- begin - remote_readme_snippet.sh - source #
cat "${_target_path}/${_archive_version}/README" >> "${_target_path}/fdeploy.log"
# ---- end - remote_readme_snippet.sh - source

# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[2019-07-31 02:39:51.057820] action=install
EOF

/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source

# ---- begin - remote_properties_snippet.sh - source
   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
export APPD_ACCOUNT="fedex1-test"
export APPD_ACCOUNT_KEY="1dce5fc52c17"
export APPD_APPNAME="SU-SEFS-RELY"
export APPD_CONTROLLER_HOST="fedex1-test.saas.appdynamics.com"
export APP_ID="dashboard-web-FXG"
export APP_PATH="/opt/fedex/sefs/dashboard-web-FXG/current/bin"
export ARTIFACTID="dashboard-web"
export EAI_NUMBER="6268"
export EUREKA_PORT="8097"
export GROUPID="com.fedex.sefs.dashboard"
export HTTP_PORT="8096"
export JAVA_HOME="/opt/java/hotspot/8/current/"
export JAVA_TOOL_OPTIONS="-Dlogging.info.file=/var/fedex/sefs/logs/dashboard-web-FXG/sefs__dashboard-web-FXG_application_info.log -Dlogging.error.file=/var/fedex/sefs/logs/dashboard-web-FXG/sefs__dashboard-web-FXG__application_error.log -Dspring.config.location=classpath:application.yml,file:/opt/fedex/sefs/dashboard-web-FXG/current/config/L1/tem_application.properties"
export JMX_PORT="7091"
export LEVEL="L1"
export SPRING_PROFILE="oracle,L0,wsso"
export TMP_DIR1="/var/fedex/sefs/logs/dashboard-web-FXG/"
export VERSION="1.2.3"

EOF
# ---- end - remote_properties_snippet.sh - source

# ---- begin - remove_create_scripts_snippet.sh - source
# check ear
_log_dir=\${LOG_DIR}
[[ "${_log_dir}" == "" ]] && _log_dir="${_target_path}/logs"
[ ! -e "\${_log_dir}" ] && mkdir -p "\${_log_dir}"

[[ ! -e "${_target_path}/${_archive_version}/config" ]] && mkdir -p "${_target_path}/${_archive_version}/config"

export LOG4J_FILE="${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
export sfs_script_HOST_PRIVATE_IP=\$(/sbin/ifconfig|perl -ane 'if(\$_=~/172\./){\$F[1]=~s/addr://g;print \$F[1]}')
export sfs_script_HOST=\$(hostname -s)
echo "PRIVATE_IP=\${sfs_script_HOST_PRIVATE_IP}"
echo "HOST      =\${sfs_script_HOST}"

base_dir="${_target_path}/${_archive_version}"


[ ! -e "${_target_path}/${_archive_version}/scripts" ] && mkdir -p "${_target_path}/${_archive_version}/scripts"
cd "${_target_path}/${_archive_version}"

## 'EOF' vs EOF is very important preventing variables from expanding in the block
cat <<'EOX' > "${_target_path}/${_archive_version}/scripts/start.sh"
#!/bin/bash

[ -e ../../properties ] && . ../../properties

# Get Relative Path of the Script
pushd \$(dirname \$0) > /dev/null
SCRIPT_PATH=\$(pwd)
popd > /dev/null
cd "\$SCRIPT_PATH"

# splits the key=value key2=value2 arguments string into exports in the current environment
for _export in \$(echo \$@); do
   declare -a vars=( )
   set -- "\${_export[@]}"
   for word; do
      if [[ \$word = *"="* ]]; then
         export "\${word%%=*}"="\${word#*=}"
      fi
   done
done

# Setting Env Variables.
JAVA_HOME=\${JAVA_HOME:-/opt/java/hotspot/8/current/}
APP_ID=\${APP_ID:-${_uuid}}
LOG_PREFIX=\${LOG_PREFIX:-${_uuid}}
JMX_PORT=\${JMX_PORT:-15562}
JMX_AUTHENTICATE=\${JMX_AUTHENTICATE:-false}
JMX_SSL=\${JMX_SSL:-false}
HTTP_PORT=\${HTTP_PORT:-8104}
EUREKA_PORT=\${EUREKA_PORT:-8093}
SPRING_PROFILE=\${SPRING_PROFILE:-L1}
EAINUMBER=\${EAINUMBER:-\${EAI_NUMBER}}
LOG_ROOT=\${LOG_ROOT:-/var/fedex/\${USER}/\${EAINUMBER}}

export JAVA_HOME
export PATH=\${JAVA_HOME}/bin:\${PATH}

export TMP_DIR=\${TMP_DIR:-/var/fedex/\${USER}/\${EAINUMBER}/java/logs/\${APP_ID}}
rm -fr \${TMP_DIR}/tomcat.*

[[ ! -e \${LOG_ROOT}/java/logs/\${APP_ID} ]] && mkdir -p \${LOG_ROOT}/java/logs/\${APP_ID}

which java

# AppDynamics support (this sets JAVA_OPTIONS)
export SERVER_NAME=\${SERVER_NAME:-\$APP_ID}
if [[ -e /opt/appd/current/appagent/javaagent.jar ]]; then
  . /opt/appd/current/scripts/app_agent_appd.sh \${EAINUMBER} > \${LOG_ROOT}/appd.out 2>&1
  . /opt/appd/current/scripts/machine_agent_appd.sh \${EAINUMBER} restart > \${LOG_ROOT}/machine-appd.out 2>&1
fi


if [[ -e "/opt/appd/current/appagent/javaagent.jar" && "\${EAINUMBER}" != "" ]]; then
        # defaults
        APP_ID=\${APP_ID}
        APPD_APPNAME=\${APPD_APPNAME:-APP_ID}
        APPD_CONTROLLER_HOST=\${APPD_CONTROLLER_HOST:-fedex1-test.saas.appdynamics.com}
        APPD_ACCOUNT_KEY=\${APPD_ACCOUNT_KEY:-1dce5fc52c17}
        APPD_ACCOUNT=\${APPD_ACCOUNT:-fedex1-test}
        APPD_CONFIGURATION="-javaagent:/opt/appd/current/appagent/javaagent.jar -Dappdynamics.http.proxyHost=internet.proxy.fedex.com -Dappdynamics.http.proxyPort=3128 -Dappdynamics.agent.applicationName=\${APPD_APPNAME} \
   -Dappdynamics.agent.tierName=\${APP_ID} -Dappdynamics.agent.nodeName=\${APP_ID}-\$(hostname) -Dappdynamics.controller.ssl.enabled=true -Dappdynamics.controller.sslPort=443 \
   -Dappdynamics.agent.logs.dir=\${LOG_ROOT} -Dappdynamics.agent.runtime.dir=/var/fedex/appd/logs -Dappdynamics.controller.hostName=\${APPD_CONTROLLER_HOST} \
   -Dappdynamics.controller.port=443 -Dappdynamics.agent.accountName=\${APPD_ACCOUNT} -Dappdynamics.agent.accountAccessKey=\${APPD_ACCOUNT_KEY} "
fi


# Tangasol
if [[ "\${TANGOSOL_CACHECONFIG}" != "" && "\${TANGOSOL_OVERRIDE}" != "" ]]; then
TANGOSOL_CONFIGURATION="\
-Dtangosol.coherence.management=\${TANGOSOL_MANAGEMENT:-all} \
-Dtangosol.coherence.management.remote=\${TANGOSOL_MANAGEMENT_REMOTE:-true} \
-Dtangosol.coherence.localport.adjust=\${TANGOSOL_LOCALPORT_ADJUST:-true} \
-Dtangosol.coherence.localport=\${TANGOSOL_LOCALPORT:-59085} \
-Dtangosol.coherence.localhost=\$(host \$(hostname) | sed 's/.*\s//g') \
-Dtangosol.coherence.override=\${TANGOSOL_OVERRIDE} \
-Dtangosol.coherence.cacheconfig=\${TANGOSOL_CACHECONFIG} \
-Dtangosol.coherence.jcache.configuration.classname=\${TANGOSOL_JCACHE_CONFIG:-passthrough}"
fi

# echo out the command
set -xv
\${JAVA_HOME}/bin/java -d64 -server -Xmx\${JVM_XMX:-2048m} -Xms\${JVM_XMS:-512m} \
   -Dfdeploy.id=${_uuid} \
   -Dcom.sun.management.jmxremote=true \${APPD_CONFIGURATION} \${TANGOSOL_CONFIGURATION} \
   -Dcom.sun.management.jmxremote.port=\${JMX_PORT} \
   -Dcom.sun.management.jmxremote.local.only=false \
   -Dcom.sun.management.jmxremote.authenticate=\${JMX_AUTHENTICATE} \
   -Dcom.sun.management.jmxremote.ssl=\${JMX_SSL} \
   -Djava.rmi.server.hostname=\$(host \$(hostname) | sed 's/.*\s//g') \
   -Djava.io.tmpdir=\${TMP_DIR:-/var/fedex/sefs/logs} -Dspring.profiles.active=\${SPRING_PROFILE} \
   -Dlogging.file=\${LOG_ROOT}/java/logs/\${LOG_PREFIX}_\${APP_ID}_application.log \
   -Deureka.host=\${EUREKA_HOST} -Deureka.port=\${EUREKA_PORT} -Deureka.peers=\${EUREKA_PEERS} \
   -Dserver.port=\${HTTP_PORT} -jar ../bin/${_artifactId}*.jar &
set +xv
echo "[\$0] started \${APP_ID}*.jar
EOX
echo "generated start scripts for [${_artifactId}]"

cat <<EOX >> "${_target_path}/${_archive_version}/config/management.properties"
#com.sun.management.jmxremote.access.file=\${base_dir}/config/jmxremote.access
com.sun.management.jmxremote.authenticate=false
com.sun.management.jmxremote.local.only=true
#com.sun.management.jmxremote.password.file=\${base_dir}/config/jmxremote.password
com.sun.management.jmxremote.port=\${JMX_PORT}
com.sun.management.jmxremote.registry.ssl=false
#com.sun.management.jmxremote.ssl.config.file=\${base_dir}/config/jmxssl.properties
com.sun.management.jmxremote.ssl=false
com.sun.management.jmxremote.ssl.need.client.auth=false
EOX



cat <<'EOX' > "${_target_path}/${_archive_version}/scripts/stop.sh"
#!/bin/bash

[ ! -e ../../properties ] && . ../../properties

echo "[\$0] ending \${APP_ID:-${_uuid}}.*.jar"
pkill -f ".* -Dfdeploy.id=\${APP_ID:-${_uuid}} .*"
EXIT_VAL=\$?
if [[ \${EXIT_VAL} -ne 0 ]]; then
	echo "[\$0] \${APP_ID:-${_artifactId}}.*.jar stop failed..."
	exit \${EXIT_VAL}
else
	echo "[\$0] \${APP_ID:-${_artifactId}}.*.jar stopped..."
fi
EOX
echo "generated stop scripts for ${_artifactId}"




cat <<'EOX' > "${_target_path}/${_archive_version}/scripts/status.sh"
#!/bin/bash

[ ! -e ../../properties ] && . ../../properties

pgrep -f ".* -Dfdeploy.id=\${APP_ID:-${_uuid}} .*"
if [[ \$? -ne 0 ]] ; then
  echo "\${APP_ID:-${_uuid}}.*.jar is not running"
  exit 1
fi
exit 0
EOX
echo "generated status scripts for ${_artifactId}"

chmod -R 755 "${_target_path}/${_archive_version}/scripts/"
# ---- end - remove_create_scripts_snippet.sh - source
# ---- begin - remote_relink_snippet.sh - source
   # remove symlink to current install
   echo "--------------[relink]---------"
   cd ${_target_path}
set -xv
   PREVIOUSLINK=\$(readlink current)
   echo "linking ${_archive_version} as current, previous is '\${PREVIOUSLINK}'"
   if [[ "$_archive_version" != "\${PREVIOUSLINK}" ]]; then
     [[ -e current ]] && rm current
     ln -snf "${_archive_version}" current
     if [[ "\${PREVIOUSLINK}" != "" ]]; then
       [[ -e previous ]] && rm previous
       ln -snf "\${PREVIOUSLINK}" previous
     fi
   fi
   #  symlink to current properties
   ln -snf "${_archive_version}.properties" "./properties"
# ---- end - remote_relink_snippet.sh - source
set +xv

# ---- begin - remote_stage_snippet_end.sh - source
else
   echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
   echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}";
   echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
   echo " !!! packaging file ${_landing_area}/${_archive_filename} not found, aborting"
   exit 1
fi
# ---- end - remote_stage_snippet_end.sh - source

# ---- begin - remote_script_end.sh - source
log_debug "Leaving remote execution, looking for ${_user}@notty :"
# find orphaned ssh session due to the remote invocation
NOTTY_PRCS=\$(ps -u ${_user} -f | grep '@notty' | grep -v grep | grep -v APPD)
#
log_debug "Echoing pids: \${NOTTY_PRCS}"
#
[[ "\${NOTTY_PRCS}" != "" ]] && log_debug "found orphaned processes ${NOTTY}"
if [[ "\${NOTTY_PRCS}" != "" ]]; then
   echo "\${NOTTY_PRCS}" | perl -ne '@x=split/\s+/;printf " %s", \$x[1];' | xargs -i% kill %
fi
# ---- end - remote_script_end.sh - source

EOT

# ---- begin - data_wrapper_footer.sh - source
   log_debug " > target : ${MACHINE} "
   log_info " > executing : ${_ssh} | ${_script_name}"
   #${verboseFlag}
   if [[ "${local_base_dir}" != "." ]]; then
      [ ! -e ${local_base_dir} ] && mkdir -p "${local_base_dir}"
   fi
   if [[ ! -e "${local_base_dir}/master-${_uuid}.sh" ]]; then
      cat <<EOX > "${local_base_dir}/master-${_uuid}.sh"
#!/bin/bash -s
timestamp() {
  while IFS= read -r line; do
      echo "\$(date) - \$line"
  done
}
EOX
   fi
   timescript=""
   script=""
   if [[  `which timeout` != '' ]]; then
      timescript="timeout -s SIGKILL ${_timeout}s"
   fi
   cat <<EOXX >> "${local_base_dir}/master-${_uuid}.sh"
echo " __ \ /  __  __     ___  __ (${_timeout})"
echo "|_   X  |_  /   | |  |  |_ (${_ssh})"
echo "|__ / \ |__ \__ |_|  |  |__ [${local_base_dir}/${_script_name}]"
EOXX
   printf -v script '%s /usr/bin/ssh -n %s %s %s "$(<%s.txt)" 2>&1  >> "%s/stdout-%s-%s.log" &' \
   "${timescript}" "${_ssh}" "${_identity_}" "${_ssh_options}" "${local_base_dir}/${_script_name}" \
   "${local_base_dir}" "${_uuid}" "${_host}"
   echo -E "$script" >> "${local_base_dir}/master-${_uuid}.sh"
done # ---

if [[ "${_no_wait}" == "" ]]; then
   echo "echo starting" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "wait" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "echo completed" >> "${local_base_dir}/master-${_uuid}.sh"
fi


log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - data_wrapper_footer.sh - source

#!/bin/bash
#set -e
#export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=deploy:action=restart, on level
_level=L1
level=${_level}
# ---- begin - data_wrapper_header.sh - source
function log_error {
   set > /dev/null
   echo "[ERROR]: $@"
}
function log_info {
   set > /dev/null
   echo "[INFO] : $@"
}
function log_debug {
   set > /dev/null
   echo "[DEBUG]: $@"
}
function log_warn {
   set > /dev/null
   echo "[WARN] : $@"
}
function join_by {
   local IFS="$1"; shift; echo "\$*";
}

set -m # Enable Job Control
_action="restart"
_identity_="-i /home/ak751818/.ssh/test_deploy_user_dsa"
_ssh_options="-oConnectTimeout=20 -oStrictHostKeyChecking=no -oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null"
_verbose_flag="set +xv"
_echo_envvars="set +xv"
_switch="tibcosilver"
_uuid="dashboard-web"
_no_wait="#SKIP#"
_timeout="20"

declare -a MACHINE_LIST=(
   "tibcosilver@irh00601.ute.fedex.com:/opt/tibco/silver/persistentdata/FXS/apps/fxs-dashboard-web"
 );
declare -a UPLOAD_CONTENTS=(
"dashboard-web.zip:1.2.3:::" );
_upload_elements=""dashboard-web.zip:1.2.3:::""

log_debug "elements ${_upload_elements}"

# set working base on the local host
local_base_dir="/home/ak751818/fdeploy/tmp"
[ ! -e "${local_base_dir}" ] && mkdir -p "${local_base_dir}"
[ ! -e "${local_base_dir}" ] && log_error " ! aborting unable creating : ${local_base_dir}" && exit 1
cd "${local_base_dir}"
#
for MACHINE in ${MACHINE_LIST[@]}; do
   IFS=':' read -ra URL <<< "$MACHINE"
   log_debug " > machine=${URL}"
   _uri_path="${URL[1]}"
   _url="${URL[0]}"
   _archive_version="1.2.3"
   _archive_filename="dashboard-web.zip"
   _artifactId="dashboard-web"
   IFS='@' read -ra HOST <<< "$_url"
   _user="${HOST[0]}"
   _host="${HOST[1]}"
   _ssh="${_user}@${_host}"
   if [[ "${_switch}" == "" ]]; then
      if [[ "${_user}" == *cm ]]; then
         b=${_user%cm}
         _ssh="${b}@${_host}";
         _user="${b}"
      fi
   fi
   _landing_area="/var/tmp/${_user}"
   IFS='?' read -ra URI <<< "$_uri_path"
   _target_path="${URI[0]}"
   _uri="${URI[1]}"
   _script_name="${_host}${_uuid}-${_action}"
   log_info  " < processing : target=${MACHINE} sudo=${_switch} "
   log_debug " < host=${_host} | url_path=${_uri_path} | ssh=${_ssh}"
# ---- end - data_wrapper_header.sh - source

# ---- begin - remote_script_begin.sh - source

# generate remote install
log_info " > writing remote script : ${local_base_dir}/${_script_name}"
cat <<-EOT > "${local_base_dir}/${_script_name}.txt"
#!/bin/bash
# --- begin ${local_base_dir}/${_script_name}.txt
log_error() {
  set > /dev/null
  echo "[ERROR]: \$@"
}
log_info()  {
  set > /dev/null
  echo "[INFO] : \$@"
}
log_debug() {
  set > /dev/null
  echo "[DEBUG]: \$@"
}
log_warn()  {
  set > /dev/null
  echo "[WARN] : \$@"
}
timestamp() {
    while IFS= read -r line; do
        echo "$(date) - $line"
    done
}

echo "====================================="
echo "DATE: `date` / USER: `whoami` / HOST: `hostname`" - PID: $$
echo "====================================="

# ENVIRONMENT SETTINGS
export APPD_ACCOUNT="fedex1-test"
export APPD_ACCOUNT_KEY="1dce5fc52c17"
export APPD_APPNAME="SU-SEFS-RELY"
export APPD_CONTROLLER_HOST="fedex1-test.saas.appdynamics.com"
export APP_ID="dashboard-web-FXG"
export APP_PATH="/opt/fedex/sefs/dashboard-web-FXG/current/bin"
export ARTIFACTID="dashboard-web"
export EAI_NUMBER="6268"
export EUREKA_PORT="8097"
export GROUPID="com.fedex.sefs.dashboard"
export HTTP_PORT="8096"
export JAVA_HOME="/opt/java/hotspot/8/current/"
export JAVA_TOOL_OPTIONS="-Dlogging.info.file=/var/fedex/sefs/logs/dashboard-web-FXG/sefs__dashboard-web-FXG_application_info.log -Dlogging.error.file=/var/fedex/sefs/logs/dashboard-web-FXG/sefs__dashboard-web-FXG__application_error.log -Dspring.config.location=classpath:application.yml,file:/opt/fedex/sefs/dashboard-web-FXG/current/config/L1/tem_application.properties"
export JMX_PORT="7091"
export LEVEL="L1"
export SPRING_PROFILE="oracle,L0,wsso"
export TMP_DIR1="/var/fedex/sefs/logs/dashboard-web-FXG/"
export VERSION="1.2.3"

#
#
set +xv

# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[2019-07-31 02:39:51.057958] action=restart
EOF

/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source

# ---- begin - remote_restart_snippet.sh - source
   # restart with new process
   #stty -ixon
   echo "╦═╗╔═╗╔═╗╔╦╗╔═╗╦═╗╔╦╗╦╔╗╔╔═╗";
   echo "╠╦╝║╣ ╚═╗ ║ ╠═╣╠╦╝ ║ ║║║║║ ╦${_artifactId}-${_archive_version}";
   echo "╩╚═╚═╝╚═╝ ╩ ╩ ╩╩╚═ ╩ ╩╝╚╝╚═╝(${_user}@${_host})";
   if [[ -e "${_target_path}/current/scripts" ]]; then
      cd ${_target_path}/current/scripts
      echo -n "STATUS: ${_host}:${_archive_filename} " && ./status.sh
      if [ \$? -eq 0 ]; then
         echo "found process, stopping..."
         ./stop.sh
         sleep 5
      fi
      ./start.sh
      sleep 5
      echo -n "STATUS: ${_host}:${_archive_filename} " && ./status.sh
      echo
      if [ \$? -eq 0 ]; then
         echo "╔═╗╔╦╗╔═╗╦═╗╔╦╗╔═╗╔╦╗";
         echo "╚═╗ ║ ╠═╣╠╦╝ ║ ║╣  ║║${_artifactId}-${_archive_version}";
         echo "╚═╝ ╩ ╩ ╩╩╚═ ╩ ╚═╝═╩╝(${_user}@${_host})";
      else
         echo "┌─┐┌┬┐┌─┐┬─┐┌┬┐  ┌─┐┌─┐┬┬  ┬ ┬┬─┐┌─┐";
         echo "└─┐ │ ├─┤├┬┘ │   ├┤ ├─┤││  │ │├┬┘├┤ ${_artifactId}-${_archive_version}";
         echo "└─┘ ┴ ┴ ┴┴└─ ┴   └  ┴ ┴┴┴─┘└─┘┴└─└─┘(${_user}@${_host})";
      fi
   else
      echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
      echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}-${_archive_version}";
      echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
      echo "aborting restart ${_target_path}/current/scripts not found!"
      exit 1
   fi
# ---- end - remote_restart_snippet.sh - source

# ---- begin - remote_script_end.sh - source
log_debug "Leaving remote execution, looking for ${_user}@notty :"
# find orphaned ssh session due to the remote invocation
NOTTY_PRCS=\$(ps -u ${_user} -f | grep '@notty' | grep -v grep | grep -v APPD)
#
log_debug "Echoing pids: \${NOTTY_PRCS}"
#
[[ "\${NOTTY_PRCS}" != "" ]] && log_debug "found orphaned processes ${NOTTY}"
if [[ "\${NOTTY_PRCS}" != "" ]]; then
   echo "\${NOTTY_PRCS}" | perl -ne '@x=split/\s+/;printf " %s", \$x[1];' | xargs -i% kill %
fi
# ---- end - remote_script_end.sh - source

EOT

# ---- begin - data_wrapper_footer.sh - source
   log_debug " > target : ${MACHINE} "
   log_info " > executing : ${_ssh} | ${_script_name}"
   #${verboseFlag}
   if [[ "${local_base_dir}" != "." ]]; then
      [ ! -e ${local_base_dir} ] && mkdir -p "${local_base_dir}"
   fi
   if [[ ! -e "${local_base_dir}/master-${_uuid}.sh" ]]; then
      cat <<EOX > "${local_base_dir}/master-${_uuid}.sh"
#!/bin/bash -s
timestamp() {
  while IFS= read -r line; do
      echo "\$(date) - \$line"
  done
}
EOX
   fi
   timescript=""
   script=""
   if [[  `which timeout` != '' ]]; then
      timescript="timeout -s SIGKILL ${_timeout}s"
   fi
   cat <<EOXX >> "${local_base_dir}/master-${_uuid}.sh"
echo " __ \ /  __  __     ___  __ (${_timeout})"
echo "|_   X  |_  /   | |  |  |_ (${_ssh})"
echo "|__ / \ |__ \__ |_|  |  |__ [${local_base_dir}/${_script_name}]"
EOXX
   printf -v script '%s /usr/bin/ssh -n %s %s %s "$(<%s.txt)" 2>&1  >> "%s/stdout-%s-%s.log" &' \
   "${timescript}" "${_ssh}" "${_identity_}" "${_ssh_options}" "${local_base_dir}/${_script_name}" \
   "${local_base_dir}" "${_uuid}" "${_host}"
   echo -E "$script" >> "${local_base_dir}/master-${_uuid}.sh"
done # ---

if [[ "${_no_wait}" == "" ]]; then
   echo "echo starting" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "wait" >> "${local_base_dir}/master-${_uuid}.sh"
   echo "echo completed" >> "${local_base_dir}/master-${_uuid}.sh"
fi


log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - data_wrapper_footer.sh - source

log_debug "starting batch execution - ${local_base_dir}/master-${_uuid}.sh"
. ${local_base_dir}/master-${_uuid}.sh
log_info "completed"
# ---- end - master_execute.sh - source
