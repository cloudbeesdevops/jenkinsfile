import re
import sys
import types
import getopt
import os
import copy

from fdeploy import Options
from fnexus.pomResolver import pomResolver
from fnexus.pomResolver import pom2gav
import logging
from fnexus.gavClass import gavClass
from fdeploy.nexusVersionResolver import nexusVersionResolver
from fnexus import NEXUSv3, NEXUSv2
import fdeploy
import types
from fnexus.nexusVersionResolver import semantic_compare, uniq, get_base_number
import re

# SEE TESTCASES UNDER fdeploy NEXUS RESOLVER

def dict2pom(pom):
    if type(pom) == dict:
        pom_ = pomResolver()
        pom_.groupId = pom['groupId']
        pom_.artifactId = pom['artifactId']
        return pom_
    return pom



def main(argv=None):
    #os.environ['https_proxy']='https://internet.proxy.fedex.com:3128'
    #os.environ['http_proxy']='http://internet.proxy.fedex.com:3128'
    if argv is None:
        argv = sys.argv[1:]
    current='.'
    pomfile='pom.xml'
    nexusURL='https://nexus.prod.cloud.fedex.com:8443'
    nexusRepo='public'
    no_words=True
    fdeploy.MUTE_ALL=True
    try:
        opts, args = getopt.getopt(argv, "hxd:u:vr:f:w")
    except getopt.error, msg:
        print ("Usage: cloudbees.py [-d directory] [-f pomFileNanme] [-w] [-u nexusURL] [-r repositoryname] [-v] [-h]\n%s\n\n-h|--help      - this text" % (msg))
        print(msg)
        print ("for help use --help")
        return 2
    for o, a in opts:
        if o in ("-h", "--help"):
            print (__doc__)
            sys.exit(0)
        elif o in ("-d"):
            current=a
        elif o in ("-f"):
            pomfile = a
        elif o in ("-u"):
            nexusURL=a
        elif o in ("-r"):
            nexusRepo=a
        elif o in ("-x","--version"):
            print("version: %s" % (fdeploy_version))
            sys.exit(0)
        elif o in ("-w"):
            no_words=False
        elif o in ("-v"):
            fdeploy.LOGGER.info("verbose is enabled.")
            fdeploy.MUTE_ALL=False
            fdeploy.logLevel= fdeploy.setLogLevel(4)
    cldbz = CloudBees(nexusURL,nexusRepo,no_words)
    next = cldbz.nextVersion(current,pomfile)
    print("%s" % (next))
    return next




class CloudBees(object):

    nexusURL = NEXUSv3
    nexusRepo = 'public'
    nexusVersionResolver = None
    echo = None
    #echo = fdeploy.LOGGER
    no_words = None

    def __init__(self, _nexusURL='https://nexus.prod.cloud.fedex.com:8443', _repoName='public', _no_words=True):
        if _nexusURL is not None:
            self.nexusURL = _nexusURL
        if _repoName is not None:
            self.nexusRepo = _repoName
        self.no_words = _no_words
        o = Options(defaults={'nexusURL':self.nexusURL, 'releasesRepo' : self.nexusRepo },silent=True)
        self.nexusVersionResolver = nexusVersionResolver(o)
        if self.echo:
            print( self.__dict__)

    def nextVersion(self,pom_dir='.',pom_file='pom.xml',urls=None):
        def resolve( gav, __version):
            version = lookup_version = __version
            gav.version = None
            nexus_definitions = [ NEXUSv3, NEXUSv2 ]
            if urls:
                for u in urls.split(','):
                    nexus_definitions.append(u.strip())
            for nexusRepo in uniq(nexus_definitions):
                search_result=self.nexusVersionResolver.resolver.what_is_the_next_version_base(gav, lookup_version)
                if self.echo:
                    self.echo.warn("search_result=%s, released? %s", search_result,not  search_result.has_no_releases is None )
                if search_result is None or search_result.version is None:
                    continue
                else:
                    gav.has_no_releases = search_result.has_no_releases
                    compare = semantic_compare(lookup_version,search_result.version)
                    if self.echo:
                        self.echo.info("compare %s vs %s = %s", lookup_version,search_result.version, compare)
                    version = search_result.version if compare <= 0 else version
                    gav.version = version
                    if self.echo:
                        self.echo.info("lookup_version=%s, search_result=%s, %s  -----> searching for %s", lookup_version, search_result.version, compare, gav.version)
            if self.echo:
                self.echo.info("found-match: %s latest_released_version%s", gav.version, gav.has_no_releases)
            return gav
        def find_latest_by(pom, __version):
            gav =  resolve(pom2gav(pom), __version)
            if gav and gav.has_no_releases is None:
                gav.version = re.sub( r'-SNAPSHOT', '',__version)
            return gav
        def find_latest_by_majmin(gav, __version):
            if self.getMajorMinorVersion(__version):
                return resolve(gav,self.getMajorMinorVersion(__version)[0])
            else:
                return None

        pom = pomResolver(pom_dir,pom_file)
        gav = find_latest_by(pom2gav(pom), pom.version)
        logging.info("pom.noted=%s, version=%s, lastreleased=%s" % (pom.version, gav.version, gav.has_no_releases))
        if  gav.has_no_releases:
            gav.version = self.calculate_next(gav.has_no_releases)
        else:
            if gav.version is None:
                raise Exception("report error to the System Team")

        if self.echo:
            self.echo.info("pom.version=%10s, next=%s (found a release? %s) earlier? %s" % (pom.version, gav.version, not gav.has_no_releases is None, not has_no_releases is None))
        logging.info( "calculated.gav=%s" % (gav.version))
        return gav.version

    def calculate_next(self, versionNumber):
        has_more_than_3_digits = len(versionNumber.split('.')) > 2
        base_name,rest = get_base_number(versionNumber, False)
        majmin = self.__firstNumbers(versionNumber)
        #print "<2=%s and >2=%s" % (has_less_than_2_digits,has_more_than_3_digits)
        try:
            items=base_name.split('.')
            buildId = -1
            # find the first digit from the right
            nrs=[]
            # 7.0.1.MASTER
            _semantic=list(items)
            for idx, item in enumerate(_semantic):
                if self.echo:
                    self.echo.info("increment_version:\t\titem = %s dig=%s : %s : %s [%s]" % (item, has_more_than_3_digits, majmin,0, ''))
                buildId=int(item)
                if idx == len(items)-1 : #and len(majmin) == len(items): # last item increment
                    buildId = buildId + 1
                nrs.append(buildId)
            nrs.extend(rest)
            nn = []
            for n in nrs:
                nn.append(str(n))
            if self.echo:
                self.echo.debug("increment_version-1:\t \t%s" % ('.'.join(nn)))
            return '.'.join(nn)
        except IndexError as ie:
            print( ie)
            if len(base_name.split('.')) > 3:
                number, rest = get_base_name(versionNumber, True)
                return number + ".".join(rest)
            elif len(base_name.split('.')) < 2:
                number, rest = get_base_name(versionNumber, False)
                return number + ".".join(rest)
            else:
                return versionNumber

    def getMajorMinorVersion(self,versionNumber):
        if versionNumber:
            return get_base_number(versionNumber, True)
        return None

    def __firstNumbers(self, versionNumber):
        majmin=[]
        for item in versionNumber.split('.'):
            try:
                n = int(item)
                majmin.append(n)
            except:
                break;
        return majmin
