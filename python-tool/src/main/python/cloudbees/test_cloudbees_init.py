from cloudbees import CloudBees
from fdeploy.UtilsForTesting import get_options
import pytest
import os

class TestCloubees(object):

    options=get_options()

    def test_increment_version(self):
        c = CloudBees(self.options.defaults['nexusURL'])
        cwd = os.path.abspath("../../test/resources/local/")
        if os.path.exists(cwd) == False:
            cwd = os.path.abspath("../../../test/resources/local")



        with pytest.raises(Exception):
            # none existing pom
            assert '1.3.0' == c.nextVersion(cwd, pom_file='a-a-a.pom')
        # not in nexus local
        assert '1.3.0' == c.nextVersion(cwd, pom_file='a-not-in-nexus.pom')
        # pom.version = 1.3.0-SNAPSHOT (with no previous releases)
        assert '1.3.0' == c.nextVersion(cwd, pom_file='a-a.pom')
        # pom.version = 1.3.0-SNAPSHOT (with 1.3.0 released)
        assert '1.3.1' == c.nextVersion(cwd, pom_file='b-a.pom')
        # pom.version = 1.3-SNAPSHOT
        assert '1.3.1' == c.nextVersion(cwd, pom_file='b-b.pom')
        # pom.version = 6.0.0.0-SNAPSHOT
        assert '6.0.0.0' == c.nextVersion(cwd, pom_file='d2-a.pom')
        # pom.version = 6.0.0.0.SNAPSHOT with no release
        assert '6.0.0.0' == c.nextVersion(cwd, pom_file='d3-a.pom')
        # pom.version = 6.0.0.0.SNAPSHOT with 6.0.0.0 as last release
        assert '6.0.0.1' == c.nextVersion(cwd, pom_file='d4-a.pom')
        # pom.version = 6.0.0.0.SNAPSHOT with 6.0.0.0 and 6.0.0.1 as last release
        assert '6.0.0.2' == c.nextVersion(cwd, pom_file='d6-a.pom')
        # pom.version = 2.1.13.2.SNAPSHOT with 2.1.13.2 as last release 
        assert '2.1.13.3' == c.nextVersion(cwd, pom_file='d7-a.pom')
        # pom.version = 2.1.13.4.SNAPSHOT with 2.1.13.2 as last release 
        assert '2.1.13.3' == c.nextVersion(cwd, pom_file='d7-a2.pom')
