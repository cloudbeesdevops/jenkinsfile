# -*- coding: utf-8 -*-

import requests
import fdeploy
from fdeploy.retry import create_session

userName = 'jenkins_api_user-1459'
PAM_URL = None
#'https://pam-wtc-vip.prod.fedex.com/api.php/v1/passwords.json/%s?reason=Severity+3%%3A+Pre-production+application+testing'

def return_account_info(dict):
    if dict:
        return dict['accountName'], dict['password']
    return None

class pamAuth:

    cache = {}

    def __init__(self, pam_url='https://pam-wtc-vip.prod.fedex.com/api.php/v1/passwords.json/%s?reason=Severity+3%%3A+Pre-production+application+testing'):
        # for external testing purposes test_Fdeploy.property
        self.pam_url = pam_url
        if not PAM_URL is None:
            self.pam_url = PAM_URL
            fdeploy.LOGGER.warn('OVERWRITING PAM url = %s (%s)' %(self.pam_url, PAM_URL))

    def get_passwd(self, pam_id ):
        if pam_id in self.cache:
            return return_account_info(self.cache[pam_id])
        url = self.pam_url % (pam_id)
        fdeploy.LOGGER.debug("pam request %s", url)
        r = create_session().get(url, auth=(userName, fdeploy.JENKINS_TO_VAULT), verify=True )
        if r.ok:
            #try:
            dict = r.json()
            self.cache[pam_id] = dict
            return return_account_info(dict)
            # except Exception as ea:
            #     fdeploy.LOGGER.error('%s\n\t%s' % (ea, r.content ))
            #     #raise ea

        else:
            fdeploy.LOGGER.error("pam request %s failed", url)
            fdeploy.LOGGER.error(r.content)
            raise Exception('pam krequest failed for pam_id=%s for %s' % (pam_id,url))


