# -*- coding: utf-8 -*-

from nexusVersionResolver import absolute_repository_path
import fdeploy.menu
import fdeploy.menu.creator

import os
import signal
import socket
import sys
import errno
import glob
import logging
import json
import contextlib
import zipfile
import re
import shutil
import ConfigParser
import argparse
from fdeploy.content.fdeployComponent import fdeployComponent


TRACE=False
LOG_FORMAT = "%(asctime)-15s [%(levelname)-5s](%(filename)s:%(module)s:%(lineno)d) - %(funcName)s: %(message)s"
JENKINS_TO_VAULT = "uHgC9cq|Fz+V0jH{"
JASYPT_SECRET = JENKINS_TO_VAULT
LOGGER = None

def init_logger(level__=logging.CRITICAL):
    if globals()['LOGGER'] is None:
        logger__ = logging.getLogger('fdeploy')
        logging.basicConfig(format=LOG_FORMAT, level=level__)
        globals()['LOGGER']=logger__
    return globals()['LOGGER']

LOGGER = init_logger()
def tracer(msg):
    if TRACE:
        fdeploy.LOGGER.debug("[%s] %s" % ('TRACE', msg))
LOGGER.__dict__['trace'] = tracer

def log():
    if os.getenv('FDEBUG') and os.getenv('FDEBUG') == 'TRACE':
        fdeploy.TRACE=True
    import logging
    fdeploy.LOGGER.setLevel(logging.DEBUG)
def nolog():
    if os.getenv('FDEBUG') and os.getenv('FDEBUG') == 'TRACE':
        fdeploy.TRACE=False
    import logging
    fdeploy.LOGGER.setLevel(logging.CRITICAL)
fdeploy.LOGGER.setLevel(logging.NOTSET)

if os.getenv('FDEBUG'):
    fdeploy.log()

ACTIONS_WITH_COMPONENT_HANDLING = ['deploy','install','stage','publish','unpublish','bluegreen']
ACTIONS_WITH_NO_DOWNLOAD = ['relink','start','stop','restart','run','publish','unpublish']
ACTIONS_WITH_MANIFEST_HANDLING = ['deploy','install','inspect','stage','relink','publish','unpublish','clean']
ACTIONS_APP_USER_REQUIRED = ['start','stop','status','restart','run']
ACTIONS_HOSTS_ONLY = ['ssh','cloudenv']
ACTIONS_WITH_COMPONENT_MENU = ['deploy','install','relink','start','stop','status','wipeout',
    'restart','stage','publish','unpublish','ssh','ps','inspect','clean']
ACTIONS_WITH_SMOKKETEST = ['deploy']

FDX_PROXIES = {}
HTTP_VERSION = 1.0
CRLF = "\r\n\r\n"
IS_ON_FEDEX_NETWORK = None
def is_on_fedex_network(reset=False):
    if globals()['IS_ON_FEDEX_NETWORK'] is None or reset == True:
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect(('internet.proxy.fedex.com', 3128))
            sock.settimeout(1)
            msg = 'GET {0} HTTP/{1} {2}'
            sock.sendall(msg.format('/', HTTP_VERSION, CRLF))
            chunks = []
            while True:
                chunk = sock.recv(int('1024'))
                if chunk:
                    chunks.append(chunk)
                else:
                    break
            fdeploy.LOGGER.warn("running within fedex network")
            globals()['IS_ON_FEDEX_NETWORK'] = True
            globals()['FDX_PROXIES']={'http' : None, 'https' : None}
        except Exception as a:
            fdeploy.LOGGER.warn("running outside the fedex network (reason=%s)" % (a))
            globals()['FDX_PROXIES']={'http' : None, 'https' : None}
            globals()['IS_ON_FEDEX_NETWORK'] = False
    return globals()['IS_ON_FEDEX_NETWORK']
IS_ON_FEDEX_NETWORK = is_on_fedex_network()

# Global Variable for setting version of fdeploy
version=None
MUTE_ALL=False

class Options:
    def __init__(self, **entries):
        if not 'defaults' in entries.keys():
            entries['defaults']={}
        self.__dict__.update(entries)
    def overwrite(self, **entries):
        self.__dict__.update(entries)


class DumpEncoder(json.JSONEncoder):
    def default(self, o):
        try:
            return {'__{}__'.format(o.__class__.__name__): o.__dict__}
        except:
            return ''
# log level priority assignments
class Enumeration(set):
    def __getattr__(self, name):
        if name in self:
            return name
        if name is None or name == 'None':
            return 'MUTE'
        raise AttributeError("name!=%s / %s" %(name, type(name)))

    def __setattr__(self, name, value):
        raise RuntimeError("Cannot override values")

    def __delattr__(self, name):
        raise RuntimeError("Cannot delete values")


Priority = Enumeration(["FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE", "MUTE"])

useCache = False
suppressReporting = False
verbose = False
trace_flag = 0
logLevel = Priority.DEBUG


class LevelAction(argparse.Action):

    CHOICES = ['L0', 'L1', 'L2', 'L3', 'L4', 'L6', 'L5', 'PROD', 'PRD']
    CLOUD_CHOICES = ['development','release','staging','production']
    CODES = ['d', 'u', 'i', 's', 'v', 't', 'e', 'p', 'p']

    def __init__(self,
                 option_strings,
                 dest,
                 nargs=None,
                 const=None,
                 default=None,
                 type=None,
                 choices=None,
                 required=False,
                 help=None,
                 metavar=None):
        help = "Environment level prefix " + ",".join(self.CHOICES)
        argparse.Action.__init__(self,
                                 option_strings=option_strings,
                                 dest=dest,
                                 nargs=nargs,
                                 const=const,
                                 default=default,
                                 type=type,
                                 choices=choices,
                                 required=required,
                                 help=help,
                                 metavar=metavar,
                                 )

        return

    def __call__(self, parser, namespace, values, option_string=None):
        fdeploy.LOGGER.debug('Processing levelAction for "%s" with "%s"' %
              (self.dest, values))
        upval = values.upper()
        valid = False
        for prefix in self.CHOICES:
            if upval.startswith(prefix):
                valid = True
                break
        for prefix in self.CLOUD_CHOICES:
            if values == prefix:
                valid = True
                break
        if valid == False:
            raise KeyError("Level %s not a valid identified prefix." % (upval))
        # Save the results in the namespace using the destination
        # variable given to our constructor.
        setattr(namespace, self.dest, values)

def level2code(level):
    if level in LevelAction.CHOICES:
        return LevelAction.CODES[LevelAction.CHOICES.index(level)]
    return '?'

def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def flatten_level_targets(targets, search=None, regex=None):
    if targets is None or len(targets) == 0:
        return []
    items = []
    # if it is already a flattened structure
    if not 'items' in targets[0]:
        items = targets
    else:
        items = []
        for _t in targets:
            if search is None or search == _t['group']:
                fdeploy.LOGGER.debug("collecting targets from " + str(_t['group']))
                items.extend(_t['items'])
    if not regex is None:
        _targets = []
        # prog = re.compile(regex)
        for _t in items:
            for _rx in regex:
                if _rx in _t:
                    _targets.append(_t)
        items = _targets
    return items

''' class reference cache '''
CLASSREF = {}


def readDescriptors(argv, id, digest_function=None):
    cfg_files = []
    readme_content = ''
    #loadingDirectory = os.path.dirname(os.path.abspath('.'))
    #print loadingDirectory
    for argvJSON in argv[:]:
        # might have embedded ${ENV} vars that need resolved and might have wildcard(s) passed in that need expansion
        #globber = "%s" % ()
        cfg_descriptors = glob.glob(os.path.expandvars(argvJSON))
        cfg_files.extend(cfg_descriptors)
    if len(cfg_files) > 0:
        dir = os.path.dirname(os.path.abspath(cfg_files[0]))
        readmefile = "%s/README" % (dir)
        fdeploy.LOGGER.info(">>> %s, hasreadme=%s" % (dir,  os.path.exists(readmefile)))
        if os.path.exists(readmefile):
            with open (readmefile, "r") as myfile:
                readme_content=myfile.readlines()
        fdeploy.LOGGER.debug(">>> %s" % (readme_content))
    fdeploy.LOGGER.debug(" > found %s descriptor files." %
                  (len(cfg_files)))
    # filter by id
    # try:
    #     if id is not None and '#' not in id:
    #         if '!' in id:
    #             id=id.replace('!','')
    #             cfg_files = [x for x in cfg_files if id not in x]
    #         else:
    #             cfg_files = [x for x in cfg_files if id in x]
    #         id = None
    # except:
    #     pass
    if len(cfg_files) == 0:
        raise Exception("No files found with %s " % (argv))
    for cfgProp in cfg_files[:]:
        cfg_descriptorsFile = os.path.abspath(os.path.normpath(cfgProp))
        fdeploy.LOGGER.info("processing '%s'", cfg_descriptorsFile)
        assert os.path.isfile(
            cfg_descriptorsFile), "Properties file: " + cfg_descriptorsFile + " NOT FOUND!"
        cfg_descriptorsFileBasename = os.path.basename(
            cfg_descriptorsFile)
        fdeploy.LOGGER.debug(" < reading descriptor file: " +
                      cfg_descriptorsFile)
        if digest_function is not None:
            loadingFile = os.path.abspath(cfg_descriptorsFile)
            digest_function(loadingFile,id)
    return readme_content

def importExternalRtvs(file,map):
    map = {} if map is None else map
    afile = os.path.abspath(file)
    if os.path.exists(afile):
        rtv_desc = []
        with open(file) as descriptor:
            try:
                rtv_desc = json.loads(descriptor.read())
            except Exception, e:
                ##print "loading %s failed." % (file)
                raise Exception("loading %s failed.: reason: %s" % (file, e))
        for item in rtv_desc:
            name = str(item['name'])
            map[name] = str(item['value'])
    else:
        fdeploy.LOGGER.warn("Skipping external import '%s' does not exist. " %(file))

def setLogLevel(verbosecount):
    sys.tracebacklimit = 0
    LOGGER.setLevel(logging.WARN)
    level = 'WARN'
    if MUTE_ALL == True:
        return
    if verbosecount == 1:
        sys.tracebacklimit = 1
        LOGGER.setLevel(logging.ERROR)
        level = 'ERROR'
    if verbosecount == 2:
        LOGGER.setLevel(logging.INFO)
        sys.tracebacklimit = 5
        level = 'INFO'
    if verbosecount == 3:
        LOGGER.setLevel(logging.DEBUG)
        sys.tracebacklimit = 9
        level = 'DEBUG'
    if verbosecount > 3:
        LOGGER.setLevel(logging.DEBUG)
        level = 'TRACE'
        turn_on_http_tracing()

    reload(sys)
    return eval('Priority.%s' % (level))


def turn_on_http_tracing():
    logging.basicConfig(level=logging.DEBUG)
    LOGGER.setLevel(logging.DEBUG)
    sys.tracebacklimit = 10
    # These two lines enable debugging at httplib level (requests->urllib3->http.client)
    # You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
    # The only thing missing will be the response.body which is not logged.
    try:
        import http.client as http_client
    except ImportError:
        # Python 2
        import httplib as http_client
    http_client.HTTPConnection.debuglevel = 1
    # You must initialize logging, otherwise you'll not see debug output.
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True
    reload(sys)


def banner(message):
    print message

def dump(_object):
    json.dumps(_object, sort_keys=True, indent=4, separators=(',', ': '))

def bash_array():
    if (logLevel == Priority.WARN or
            (logLevel == Priority.ERROR)):
        return ['', '#', '#', '']
    if (logLevel == Priority.INFO or
            logLevel == Priority.WARN or logLevel == Priority.ERROR):
        return ['', '', '#', '']
    if (logLevel == Priority.DEBUG or
        logLevel == Priority.INFO or logLevel == Priority.WARN
            or logLevel == Priority.ERROR):
        return ['', '', '', '']
    return ['', '', '', '']

def load_rc(rcfile, options):
    defaults={}
    __config__ = ConfigParser.RawConfigParser()
    __config__.optionxform = str # disable lowercasing keys
    rcfile = absolute_repository_path(rcfile)
    if os.path.exists(rcfile) == False:
        raise Exception('rcfile %s does not exist.' % (os.path.abspath(rcfile)))
    fdeploy.LOGGER.warn('loading %s for defaults.' % (rcfile))
    try:
        __config__.read(rcfile)
        for item in __config__.items('DEFAULTS'):
            (a,b) = item
            # not sure why this was uncommented.
            #if 'snapshotsRepo' in a or 'releasesRepo' in a or 'nexusURL' in a:
            #    continue
            defaults[a] = b
    except:
        raise Exception('rcfile %s is missing [DEFAULTS] section' %(rcfile))
    options.defaults=defaults
    LOGGER.warn("defaults = %s" % (options.defaults))


def cleanup(user):
    if os.path.isdir("/proc"):
        pids = [pid for pid in os.listdir('/proc') if pid.isdigit()]
        for pid in pids:
            try:
                path = os.path.join('/proc', pid, 'cmdline')
                uid = os.stat(path).st_uid
                if uid == user and uid != 'jenkins':
                    cmd = open(os.path.join('/proc', pid, 'cmdline'), 'rb').read()
                    if cmd.find('=================') != -1:
                        print ("PID: %s; Command: %s" % (pid, cmd))
                        os.kill(pid,signal.SIGKILL)
            # process has already terminated
            except (IOError, OSError):
                continue


def unpack(member,source,vfilename):
    if not os.path.exists(os.path.dirname(vfilename)):
        try:
            os.makedirs(os.path.dirname(vfilename))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    fdeploy.LOGGER.debug("   %s -> %s",member,vfilename)
    target = open(vfilename, "wb+")
    with target:
        shutil.copyfileobj(source, target)
    try:
        target.close()
    except:
        pass
    try:
        source.close()
    except:
        pass
    return vfilename

def resolve_filter(content, filter={}):
    _file = content
    for key in filter.keys():
        _file = _file.replace('${%s}' % key, filter[key])
    return _file

def load_properties(filepath, sep='=', comment_char='#', filter={}):
    """
    Read the file passed as parameter as a properties file.
    """
    props = {}
    with open(filepath, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                key = key_value[0].strip()
                value = sep.join(key_value[1:]).strip().strip('"')
                props[key] = resolve_filter(value, filter)
    return props

def extract_deployment_xml(level, archiveFileName, targetdir, artifactId=None):
    level_path="/%s/" % (level)
    file_list = []
    tibcoxml = None
    fdeploy.LOGGER.info("Extracting Deployments for: %s (%s)",archiveFileName, artifactId)
    if archiveFileName.endswith(".zip"):
        with contextlib.closing(zipfile.ZipFile(archiveFileName)) as zfile:
            for member in zfile.namelist():
                if member.endswith(".ear"):
                    # print "archive=%s, member=%s" % (archiveFileName,member)
                    vfilename = "%s/%s" % (targetdir, member)
                    earFile = unpack(member,zfile.open(member),vfilename)
                    fdeploy.LOGGER.info( "unpacked %s" % (earFile) )
                    with contextlib.closing(zipfile.ZipFile(earFile)) as vfile:
                        for vmember in vfile.namelist():
                            if "TIBCO.xml" in vmember:
                                # print "analyzing: %s" % (vmember)
                                source = vfile.open(vmember)
                                vfilename = "%s/%s-TIBCO.xml" % (targetdir, artifactId)
                                tibcoxml=unpack(vmember,source,vfilename)
                elif member.endswith(".xml") and not 'log4j' in member and level_path in member:
                    fdeploy.LOGGER.info("lvl: %s, member: %s, target=%s, artifactIdv=%s" % (level_path, member, targetdir, artifactId))
                    if artifactId in member:
                        source = zfile.open(member)
                        filename = "%s/%s" % (targetdir, member)
                        filename =unpack(member,source,filename)
                        file_list.append(filename)
    else:
        fdeploy.LOGGER.error("ERROR - can only extract a file from a zip archive")
        raise Exception("ERROR - can only extract a file from a zip archive")
    return [file_list,tibcoxml]


def security_realm(cli_options):
    # guessing the -i based on the level if not provided
    if cli_options.identity is None or cli_options.identity == '':
        # set default identity if .fdeployrc exists from there or sefs defaults.
        dev_dsa_s = ['./ssh/dev_deploy_user_dsa','../ssh/dev_deploy_user_dsa','~/.ssh/test_deploy_user_dsa']
        if 'dev_identity' in cli_options.defaults:
            dev_dsa_s = [cli_options.defaults['dev_identity']]
            if 'test_identity' in cli_options.defaults:
                dev_dsa_s.append(cli_options.defaults['test_identity'])
        test_dsa = '~/.ssh/test_deploy_user_dsa' if 'test_identity' not in cli_options.defaults else cli_options.defaults['test_identity']
        prod_dsa = '~/.ssh/prod_deploy_user_dsa' if 'prod_identity' not in cli_options.defaults else cli_options.defaults['prod_identity']

        if cli_options.level in ['L0','L1', 'L2']:
            for a in dev_dsa_s:
                path_ = os.path.abspath(os.path.expanduser(os.path.normpath(a)))
                if os.path.isfile(path_) == True:
                    # the runtime moves into ./tmp so we have to step back relatively to fdeploy root
                    cli_options.identity = path_
                    break;
        elif cli_options.level in ['L3', 'L4', 'L5', 'L6'] and os.path.isfile(os.path.expanduser(test_dsa)):
            cli_options.identity = os.path.expanduser(test_dsa)
        elif cli_options.level in ['PROD'] and os.path.isfile(os.path.expanduser(prod_dsa)):
            cli_options.identity = os.path.expanduser(prod_dsa)
        if cli_options.identity is not None:
            fdeploy.LOGGER.info("autodetected identity: %s" % (cli_options.identity))


APP_GROUP_NAME_REGEX=re.compile('.+\/config\.(\w+)\/.*')

def get_app_group(argv,fallback=None):
    if isinstance(argv, fdeployComponent):
        argv = argv.source
    m = APP_GROUP_NAME_REGEX.match(os.path.abspath(argv))
    return fallback if m is None or m.group(1) is None else m.group(1)
