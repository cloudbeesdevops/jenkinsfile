
import os
from fnexus.nexusVersionResolver import nexusVersionResolver
from fdeploy.UtilsForTesting import get_options
from fdeploy.domainLoader import domainLoader

class TestDomainLoader(object):

    options=get_options()
    resolver = nexusVersionResolver(options)

    def setup_method(clz, method):
        cdw = os.path.abspath("%s/../../test/resources" % (os.getcwd()))
        clz.options.defaults['nexusURL'] = 'file://%s/local' % (cdw)

    def test_domain_loader(self):
        loader = domainLoader('../../test/resources/domainloader')
        domains = loader.digest('domain-config.json')

        assert len(domains) == 2

        assert len(loader.domain_rtvs()) == 2

        for d in loader.domain_rtvs():
            assert len(d) == 5

        assert len(loader.domain_rtv('waypoint')) == 5
