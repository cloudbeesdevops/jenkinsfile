# -*- coding: utf-8 -*-
import os
import requests  # http://docs.python-requests.org/en/latest/
import requests.packages.urllib3
import fdeploy
from requests_file import FileAdapter
import xml.etree.ElementTree as ET
from fnexus.nexusVersionResolver import nexusVersionResolver as fnexusResolver
from fnexus.manifestResolver import manifestResolver
from fnexus.pomResolver import pomResolver
from fnexus import absolute_repository_path
from fnexus.nexusVersionResolver import semantic_compare

class nexusVersionResolver(object):

    archiveDir = 'archives'
    useCache = False
    tmpDir = None
    echo = os.getenv('FDEBUG')
    __manifest_resolver__ = None
    resolver = None

    def __init__(self, options, _useCache=True, base_dir="tmp", manifestResolver=None):
        self.__manifest_resolver__ = manifestResolver
        self.resolver = fnexusResolver(options)
        self.useCache = _useCache
        self.tmpDir = base_dir + "/" + self.archiveDir

    def applyOptions(self, options):
        fdeploy.LOGGER.debug(options)
        self.resolver.applyOptions(options)

    def manifest_resolver(self):
        return self.__manifest_resolver__

    def set_manifest_resolver(self, manifestResolver):
        self.__manifest_resolver__ = manifestResolver

    def resolveGavs(self, comp, downloadRequired=False, testing=False, manifestRequired=False):
        found = None
        if self.echo:
            fdeploy.LOGGER.debug("\t %s", comp.dict)
        for gav in comp.contents:
            if comp.isNexusArtifact(gav):
                fdeploy.LOGGER.debug("resolving %s:%s, downloading required %s",gav.groupId, gav.artifactId, downloadRequired)
                fdeploy.LOGGER.debug("resolving %s:%s, manifest resolving required %s",gav.groupId, gav.artifactId, manifestRequired)
                if manifestRequired == True:
                    gav.version = self.get_artifact_version_from_manifest(gav.artifactId)
                if downloadRequired == True and testing == False:
                    fdeploy.LOGGER.debug("resolving : %s" % (gav) )
                    if self.resolve(gav) is None:
                        fdeploy.LOGGER.trace("gav %s:%s is not found" % (gav.groupId, gav.artifactId))
                        return None
                    else:
                        fdeploy.LOGGER.debug("resolved : %s" % (gav) )
                if downloadRequired == True:
                    self.resolver.download(gav)
                found = gav
        return found

    def get_artifact_version_from_manifest(self,artifactId):
        # lookup the version in the manifest
        manifest = None
        try:
            manifest = self.__manifest_resolver__.__manifest__
        except:
            pass
        fdeploy.LOGGER.debug("\n\n get_version:%s\n" % (artifactId))
        if (manifest is None or self.__manifest_resolver__.isLoaded == False):
            raise Exception("FATAL : manifest not found for %s [manifest=%s,loaded=%s]!" % (artifactId, manifest,self.__manifest_resolver__.isLoaded))
        if (manifest and manifest.has_option('RELEASE_MANIFEST', artifactId)):
            # found alternate version
            fdeploy.LOGGER.debug("artifact=%s, manifest_version=%s", artifactId, manifest.get('RELEASE_MANIFEST', artifactId))
            return manifest.get('RELEASE_MANIFEST', artifactId)
        else:
            if manifest.has_section('RELEASE_MANIFEST'):
                fdeploy.LOGGER.warn("WARNING : Unresolved version for %s thus skipping.\nWARNING : Please check your RELEASE_MANIFEST.MF, currently contains:" % (artifactId))
                for item in manifest.items('RELEASE_MANIFEST'):
                    fdeploy.LOGGER.debug("\t %s looking for %s", item, artifactId)
                return None
            else:
                fdeploy.LOGGER.debug("\nFATAL : No Release Manifest section in RELEASE_MANIFEST.MF for %s.\n",artifactId)
                return None
        return None


    ''' Main Resolving Interface Method '''
    def resolve(self, gav, org_majorMinorVersion=None):
        if not org_majorMinorVersion:
            if not gav.version:
                gav.version = self.get_artifact_version_from_manifest(gav.artifactId)
                if gav.version is None:
                    raise Exception('No artifactId version for %s' % (gav.artifactId))
                gav = self.resolver.does_version_exist_in_nexus(gav, gav.version)
            else:
                fdeploy.LOGGER.debug("resolve:\thardcoded version %s ", gav)
        else:
            gav = self.resolver.what_is_the_next_version_base(gav, org_majorMinorVersion)
        return gav

    def resolve_from_manifest(self, gav, nexus_compliance=False):
        return self.resolver.does_version_exist_in_nexus(gav, gav.version)
