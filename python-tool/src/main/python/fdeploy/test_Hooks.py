# -*- coding: utf-8 -*-


from fdeploy.hooks import mattermost, ldd_post_request
import fdeploy
import os
import pytest
from MockHttpServer import start_mock_server

os.environ['NO_PROXY']='127.0.0.1'
TEST_HTTP_PORT=None

class TestHooks(object):

    @classmethod
    def setup_class(cls):
        port = start_mock_server()
        fdeploy.test_Hooks.TEST_HTTP_PORT = port

    def xest_hook_web(self):
        options = {'ldd_url' : 'http://127.0.0.1:%s/' % (TEST_HTTP_PORT)}
        reqitems = {'loggedInUser' : 'akaan', 'applicationNm': 'A','applicationVersion' : 'v1', 'level' : 'PRD', 'opco' : 'xopv'}
        ret,res = ldd_post_request([reqitems], options, noproxy=True)
        fdeploy.LOGGER.info( "ret=%s" %(res.__dict__))
        assert 'successful' == ret
        assert '["cool"]' ==  res.content
        mattermost('hello from fdeploy unit test')
        reqitems =  {'applicationNm' : 'fxf-java-pickup-and-delivery', 'applicationVersion' : '2.4.0-SNAPSHOT', 'loggedInUser' : '751818', 'level': 'L1',
        'opco': 'FXF', 'releaseNm': 'NA'}
        ret,res = ldd_post_request([reqitems], options, noproxy=True)
        fdeploy.LOGGER.info( "ret=%s" %(res.__dict__))
        assert 'successful' == ret


    def test_hook_fail(self):
        options = {'ldd_url' : 'http://127.0.0.1:%s/' % (TEST_HTTP_PORT)}
        reqitems = {'loggedInUser' : 'akaan'}
        with pytest.raises(KeyError):
            ldd_post_request([reqitems], options)
