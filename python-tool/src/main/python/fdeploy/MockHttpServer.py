from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import json
import requests
import re
import fdeploy
from threading import Thread
import socket
import os
import ssl

os.environ['NO_PROXY'] = '127.0.0.1'
socket.gethostbyname(socket.gethostname())
BASE_URL = 'http://%s:%%s' % ('127.0.0.1')

#socks.wrapmodule(requests.packages.urllib3)
class MockServerRequestHandler(BaseHTTPRequestHandler):
    USERS_PATTERN = re.compile(r'/sefs-dashboard')

    def do_POST(self):
        fdeploy.LOGGER.info( self.path)
        if '/api/public/deploy/create' in self.path:
            self.send_response(requests.codes.ok)

            # Add response headers.
            self.send_header('Content-Type', 'application/json; charset=utf-8')
            self.end_headers()

            # Add response content.
            response_content = json.dumps(['cool'])
            self.wfile.write(response_content.encode('utf-8'))
            return

        if re.search(self.USERS_PATTERN, self.path):
            # Add response status code.
            self.send_response(requests.codes.ok)

            # Add response headers.
            self.send_header('Content-Type', 'application/json; charset=utf-8')
            self.end_headers()

            # Add response content.
            response_content = json.dumps([])
            self.wfile.write(response_content.encode('utf-8'))
            return


def get_free_port():
    s = socket.socket(socket.AF_INET, type=socket.SOCK_STREAM)
    s.bind(('localhost', 0))
    address, port = s.getsockname()
    s.close()
    return port

def start_mock_server(handler_class=MockServerRequestHandler, type='http'):
    port = get_free_port()
    fdeploy.LOGGER.warn('running mockserver on port %s with handler %s.' % (port, handler_class))
    return __start_mock_server(port, handler_class,type)

def __start_mock_server(port, handler_class=MockServerRequestHandler, type='http'):
    if 'http' == type:
        mock_server = HTTPServer(('127.0.0.1', port), handler_class)
    else:
        mock_server = HTTPServer(('127.0.0.1', port), handler_class)
        #mock_server.socket = ssl.wrap_socket(mock_server.socket, certfile='../../test/resources/localhost.pem', server_side=True)
        mock_server.socket = ssl.wrap_socket(mock_server.socket, keyfile='../../test/resources/key.pem', certfile='../../test/resources/localhost.pem', server_side=True)
    mock_server.timeout=5
    mock_server_thread = Thread(target=mock_server.serve_forever)
    mock_server_thread.setDaemon(True)
    mock_server_thread.start()
    fdeploy.LOGGER.info( "Server Started on %s" % (port))
    return port
