# -*- coding: utf-8 -*-

import fdeploy
import fdeploy.content
import os
import json

# classes
class serviceLoader(object):
    serviceMap = {}
    path = None

    def __init__(self,path):
        self.path = path

    def readDescriptors(self, argv):
        fdeploy.LOGGER.info("serviceloader cwd: %s" % (os.path.abspath(self.path)))

        if type(argv) != list:
            argv = [argv]
        files = []
        for item in argv:
            item = item.strip()
            if item.startswith('.') == True or item.startswith('/') == True:
                files.append(os.path.abspath("%s" % (item)))
            else:
                files.append(os.path.abspath("%s/%s" % (self.path,item)))
        id = None
        try:
            id = self.options['id'] if isinstance(self.options,dict) else self.options.id;
        except:
            pass
        def digester(jsonfile, id):
            self.digest(jsonfile, id)
        self.readme = fdeploy.readDescriptors(files, id, digester )


    def digest(self, jsonfile, id):
        loadingFile = os.path.abspath(jsonfile)
        self.loadingDirectory = os.path.dirname(os.path.abspath(loadingFile))
        deploy_descr = []
        with open(loadingFile) as descriptor:
            try:
                deploy_descr = json.loads(descriptor.read())
            except Exception, e:
                raise Exception("loading %s failed.: reason: %s" % (loadingFile, e))
        if 'service' in deploy_descr[0].keys():
            for service in deploy_descr[0]['service']:
                _serv = fdeploy.content.service_factory(service)
                self.serviceMap[str(_serv.service_name)]=_serv
        fdeploy.LOGGER.info("registered %s services." % (len(self.serviceMap)) )
