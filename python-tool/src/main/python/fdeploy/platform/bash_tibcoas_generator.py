# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import re
import subprocess
import fdeploy
import fdeploy.platform
import fdeploy.fdeployLoader
import time
from fdeploy.platform import load_snippet, baseGenerator
from fdeploy.platform import DEFAULT_TARGET_SPEC, _needs_nexus_resolving

#from fdeploy.platform.bash_tibcobe_generator import tibco_bwGenerator, tibco_bwDeployer
REGISTERED_ACTIONS = ['clean', 'wipeout', 'start', 'stop', 'install', 'stage', 'ml', 'machine_list',
        'deploy', 'publish', 'unpublish', 'restart', 'ssh', 'inspect', 'ps', 'cloudenv','status','properties']
REQUIRED_RTVS = ['TIBCO_HOME','JMX_PORT','AS_METASPACE', 'AS_HOME','AS_LISTEN_PORT','AS_INPUT',
        'AS_DISCOVERY', 'AS_LOG','AS_MEMBER_NAME_PREFIX','AS_DATA_STORE', 'LOG_DIR']
TARGET_URL_SPEC=DEFAULT_TARGET_SPEC

class tibco_asDeployer(baseGenerator):
    def __init__(self, component, cli_options, stage_directory):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_tibcoas_generator.REGISTERED_ACTIONS
        super(tibco_asDeployer, self).__init__(
            component, cli_options, stage_directory)
        fdeploy.LOGGER.debug(
            " < init bash-generator tibco_asDeployer platform for " + str(self.component))

    def deploying(self, files, pause):
        p = []
        i = 0
        if len(files) < 1:
            raise Exception("Abort! No files were generated for this deployment.")
        for _file in files:
            i += 1
            fdeploy.LOGGER.debug(" > deploying execution: %s of %s / %s" % (i,len(files),_file))
            fdeploy.LOGGER.trace(" > files are %s" % (files))
            __file = str(self.stage_directory) + "/" + str(_file)
            _filename = str(self.stage_directory) + \
                "/deploy-" + str(_file) + ".log"
            os.chmod(__file, 0b111101101)  # rwxr-xr-x make it executable
            # subprocess.call(["/var/tmp/fdeploy/xls.sh"])
            # remote invocation of the generated script
            with open(_filename, 'w') as __stdout:
                #subprocess.call([__file], stdout=__stdout)
                if self.options.parallel == False:
                    subprocess.call([__file], stdout=__stdout)
                else:
                    with open(os.devnull, 'r+b', 0) as DEVNULL:
                        px = subprocess.Popen([__file],
                            stdin=DEVNULL, stdout=DEVNULL, stderr=__stdout, close_fds=True)
                            #stdin=DEVNULL, stdout=__stdout, stderr=__stdout, close_fds=True)
                        p.append(px)
                        # Wait until process terminates
                        while px.poll() is None:
                            time.sleep(1)

class tibco_asGenerator(baseGenerator):

    files = []

    # TIBCO
    remote_create_scripts_snippet = load_snippet('tibco_as/remote_create_scripts_snippet.sh')


    def __init__(self, component, cli_options, stage_directory='.', target_spec = DEFAULT_TARGET_SPEC):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_tibcoas_generator.REGISTERED_ACTIONS
        self.REQUIRED_RTVS =  fdeploy.platform.bash_tibcoas_generator.REQUIRED_RTVS

        super(tibco_asGenerator, self).__init__(
            component, cli_options, stage_directory, target_spec)
        fdeploy.LOGGER.debug(
            " < init platform: bash-generator, class: tibco_asGenerator  for " + str(self.component))

    def list_machines(self, struct,target_list,search=None):
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        for target in __targets:
            _target = target.split(":")[0]
            if self.options.user is not None:
                _target = re.sub(
                    r'^(\S+)\@', self.options.user + str('@'), _target)
            if not _target in target_list:
                target_list.append(_target)

    """Generate scripts for the components on the targets in the descriptors."""
    def generate(self, struct, outfile, action='status',target_spec=None,verbose=False):
        # ssh_options
        level = struct['level']
        # replace values in the snippets
        target_list = ''
        upload_list = ''
        envvar_list = ''
        app_user=None
        # pick the first Nexus resolved component artifact.
        nexusContent = self.get_first_nexus_component_in_content(_needs_nexus_resolving(action,False))

        # agregation of steps.
        actions = []
        if action == 'deploy':
            actions.append('stop')
            actions.append('install')
            actions.append('restart')
        elif action == 'clean' and not self.component.versions_to_persist is None and len(self.component.versions_to_persist) > 1:
            self.component.rtvMapping['CANDIDATES']=','.join(self.component.versions_to_persist)
            actions.append('clean_with_manifest')
        else:
            actions.append(action)

        #
        # resolving the runtime variables (rtv), first global then components.
        #
        unused,envvar_list=self.component.get_env_vars(self.options,level['level'], self.REQUIRED_RTVS, nexusContent.version)
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        fdeploy.LOGGER.debug("hosts : %s spec %s"  % (str(__targets), target_spec))
        target,target_list=self.get_target_list(__targets, target_spec)
        if len(target_list) < 2:
            raise Exception("targetlist empty? %s" % (struct['targets']))
        #
        # generate the upload list based on the contents specified
        #
        upload_list=self.get_upload_list(struct,nexusContent.version)
        #
        # applying execute identity if applicable
        #
        pki_identiy = ' ' if self.options.identity is None or self.options.identity == '' else '-i ' + \
            str(self.options.identity)
        verboseOn = "set -xv"
        verboseOff = "set +xv"
        verboseFlag = verboseOff if verbose == False else verboseOn

        # NO WAIT FLAG
        nowaitflag = '' if self.options.no_wait == False else '#SKIP#'

        log_array = fdeploy.bash_array()
        fdeploy.LOGGER.trace("log_array=" + str(log_array))

        fdeploy.LOGGER.info( "generating for %s individual actions [%s]" % (len(actions),actions) )
        for _action in actions:
            # only echo the env vars on certain actions.
            echoEnvVars = verboseOff if _action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING else verboseFlag
            #
            # find the application user, assuming the user being supplied is the cm user
            # based on the last processed target
            # TODO: might need more attention when you have targets with differnt users
            #
            app_user = self.get_app_user(target,app_user,_action)

            print (self.data_wrapper_header % (
                self.options.command,
                _action,
                self.options.level,
                log_array[0], log_array[1], log_array[2], log_array[3],
                _action,
                pki_identiy,
                self.ssh_options,
                echoEnvVars,
                verboseFlag,
                app_user,
                struct['uuid'],
                nowaitflag,
                '20', #timeout
                target_list,
                upload_list,
                upload_list,
                self.stage_directory,
                nexusContent.version,
                nexusContent.saveArchiveName,
                nexusContent.artifactId,
                '%'
            ),file=outfile)

            data = []
            self.write_refactored(outfile,_action,data, log_array, envvar_list, verboseFlag, app_user)
            # _remote_script_begin= self.remote_script_begin% (
            #     log_array, envvar_list, verboseFlag)
            # _remote_stage_snippet_begin = self.remote_stage_snippet_begin % (envvar_list)
            # fdeploy.LOGGER.info("generating for [%s] as %s" % ( _action, app_user))
            # self.write(outfile,_action,data,_remote_script_begin,_remote_stage_snippet_begin)
        if _action not in ['test', 'inspect', 'ssh', 'ps','cloudenv']:
            print( self.data_wrapper_execute, file=outfile)
