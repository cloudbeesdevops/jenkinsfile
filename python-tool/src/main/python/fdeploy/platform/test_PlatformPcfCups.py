# -*- coding: utf-8 -*-
import os
import fdeploy
from fdeploy.platform.pcf.cups_class import cupsCredentialsClass, cupsJmsProviderClass,\
        cupsConfigVariableClass, cupsDatabaseServiceClass, cupsGitConfigServerClass,\
        cupsConfigVariableClass, cupsUriClass, cupsScaleClass, cupsGitConfigHttpServerClass
from fdeploy.platform.pcf import envClass, taskClass, build_args_list, runStatusClass, authPcfClass
from fdeploy.platform.pcf.service_class import serviceAppdClass, serviceAutoScalerClass, serviceSplunkClass
from fdeploy.platform.pcf.command_class import commandClass

from fdeploy.platform.bash_pcf_cups_generator import pcf_cupsGenerator
from fdeploy.UtilsForTesting import compare_files, skipping_if_cryptology_exists, skipping_when_missing_cryptology
from fdeploy.UtilsForTesting import get_options
from fdeploy.fdeployLoader import fdeployLoader
from fdeploy.MockHttpServer import start_mock_server
from fdeploy.test_PAM import pamAuthRequestHandler, setup_pam_url

temp_save = dict(os.environ)

class TestPlatformCups(object):

    @classmethod
    def setup_class(module):
        os.environ['NO_PROXY']='127.0.0.1'
        os.environ['MOCK_USERNAME'] = 'FXS_app3534545xx'
        port = start_mock_server(pamAuthRequestHandler)
        fdeploy.pam.PAM_URL= setup_pam_url(port)
        rtv = { 'APP_NM' : 'label-trip-test'}
        for r in rtv.keys():
            os.environ[r]=rtv[r]

    @classmethod
    def teardown_class(module):
        #os.environ.clear()
        os.environ.update(temp_save)

    def setup_method(self, method):
        os.environ['MOCK_USERNAME'] = 'FXS_app3534545xx'

    def test_build_args_list(self):
        a=['cf', 'abc', '%s', '--user']
        __new = build_args_list(a)
        assert ['cf', 'abc', 'uups', '--user' , '||', 'cf', 'abc', 'cups', '--user'] == __new

    @skipping_when_missing_cryptology
    def test_cups_credentials(self):
        a = cupsCredentialsClass('processid', {'service_name': 'name', 'type': 'Credentials', 'username' : 'uzer', 'password' : 'pazzword', 'role' : 'USER', 'env_map' : {} })
        assert 'cf uups name -p \'{"UserName": "uzer", "Password": "pazzword", "Role": "USER"}\'\
 || cf cups name -p \'{"UserName": "uzer", "Password": "pazzword", "Role": "USER"}\'' == a.toCommand('')
        args = ['cf','uups', 'name', '-p', '{"UserName": "uzer", "Password": "pazzword", "Role": "USER"}','||',
        'cf','cups', 'name', '-p', '{"UserName": "uzer", "Password": "pazzword", "Role": "USER"}']
        assert args == a.toProgramArgs('')
        args = ['cf','delete-service', 'name', '-f']
        assert args == a.toProgramArgs('delete')
        assert 'cf delete-service name -f' == a.toCommand('delete')
        #
        #
        a = cupsCredentialsClass('processid', {'service_name': 'name', 'type': 'Credentials', 'username' : 'uzer', 'password' : 'pazzword', 'role' : 'USER', 'env_map' : {'MOVETOVAULT_L1' : 'Yes', 'MOVETOVAULT_L2' : 'No'} })
        assert 'cf uups name -p \'{"UserName": "uzer", "MOVETOVAULT_L1": "Yes", "Password": "pazzword", "Role": "USER", "MOVETOVAULT_L2": "No"}\'\
 || cf cups name -p \'{"UserName": "uzer", "MOVETOVAULT_L1": "Yes", "Password": "pazzword", "Role": "USER", "MOVETOVAULT_L2": "No"}\'' == a.toCommand('')
        args = ['cf','uups', 'name', '-p', '{"UserName": "uzer", "MOVETOVAULT_L1": "Yes", "Password": "pazzword", "Role": "USER", "MOVETOVAULT_L2": "No"}','||',
        'cf','cups', 'name', '-p', '{"UserName": "uzer", "MOVETOVAULT_L1": "Yes", "Password": "pazzword", "Role": "USER", "MOVETOVAULT_L2": "No"}']
        assert args == a.toProgramArgs('')

    @skipping_if_cryptology_exists
    def test_cups_credentials_with_cryptology_enabled(self):
        a = cupsCredentialsClass('processid', {'service_name': 'name', 'type': 'Credentials', 'username' : 'uzer', 'password' : 'pazzword', 'role' : 'USER'})
        assert 'cf uups name -p \'{"UserName": "uzer", "Password": "pazzword", "Role": "USER"}\'\
 || cf cups name -p \'{"UserName": "uzer", "Password": "pazzword", "Role": "USER"}\'' == a.toCommand('')
        assert 'cf uups name -p \'{"UserName": "uzer", "Password": "pazzword", "Role": "USER"}\'\
 || cf cups name -p \'{"UserName": "uzer", "Password": "pazzword", "Role": "USER"}\'' == a.toCommand('delete')
        args = ['cf','delete-service', 'name', '-f']
        assert args == a.toProgramArgs('delete')
        assert 'cf delete-service name -f' == a.toCommand('delete')

    def test_cups_appd(self):
        a = serviceAppdClass('processid', {'service_name': 'name', 'domain': 'trip-test'})
        assert 'cf create-service appdynamics "trip-test" "name"' == a.toCommand('')
        assert ['cf','create-service','appdynamics','trip-test','name'] == a.toProgramArgs('')
        assert'cf delete-service appdynamics "trip-test" "name"' == a.toCommand('delete')
        assert ['cf','delete-service','appdynamics','trip-test','name'] == a.toProgramArgs('delete')


    def test_runStatus(self):
        a = runStatusClass('process-id','appnm')
        result = '''# --- status ---
APP_EXISTS=$(cf get-health-check appnm)
if [[ "${APP_EXISTS}" != *"FAILED"* ]]; then
  export app_exists=1
  echo "Application appnm Already Exists, refreshing (code=${app_exists})"
else
  export app_exists=0
  echo "Application appnm Does not Exist, initializing (code=${app_exists})"
fi
# --- status end ---
'''
        assert result == a.toCommand()

    def test_auth(self):
        a = authPcfClass('process-id','pam_id','space','eai_number','domain')
        result = '''hostname
which cf
cf_exists=${?}
if [[ "${cf_exists}" != 0 ]]; then
  echo "CloudFoundry_CLI was not found. Abort! (code=${cf_exists})"
  echo "Please export PATH with location of the cf binary (f.e: /opt/fedex/tibco/cf-cli/bin)"
  exit ${cf_exists}
else
  echo "CloudFoundry_CLI found at $(which cf)"
fi

echo "Login to domain as "
cf api https://domain
cf auth '' ''

echo "Set target space 'space'"
cf target -o eai_number -s space
if [[ "${?}" != 0 ]]; then
  echo "Error setting space 'space' in org 'eai_number'"
  exit 2
fi
'''
        assert result == a.toCommand('test')

    def test_env_set(self):
        os.environ['level'] = 'l3b'


        a = envClass('processid', {'service_name': 'name', 'value': '${level}-trip-test'})
        assert 'cf set-env label-trip-test "name" "l3b-trip-test"' == a.toCommand('')
        assert 'cf unset-env label-trip-test "name" "l3b-trip-test"' == a.toCommand('delete')
        assert ['cf','set-env','label-trip-test','name','l3b-trip-test' ]== a.toProgramArgs('')
        assert ['cf','unset-env','label-trip-test','name' ] == a.toProgramArgs('delete')

    def test_cups_splunk(self):
        a = serviceSplunkClass('process_id', {'service_name': 'name', 'type' : 'Splunk', 'domain': 'trip-test'})
        assert ['cf','create-service','splunk','trip-test','name' ]== a.toProgramArgs('')
        assert ['cf','delete-service','splunk','trip-test','name'] == a.toProgramArgs('delete')
        assert 'cf create-service splunk "trip-test" "name"' == a.toCommand('')
        assert 'cf delete-service splunk "trip-test" "name"' == a.toCommand('delete')

    def test_cups_git_config(self):
        a = cupsGitConfigServerClass('process-id',
            {'type':'GitConfigServer', 'service_name': 'config-server-details', 'git-url': 'https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git', 'access-token': 'fES2CPKJc4mAoD-YUQSL'})
        assert 'cf uups config-server-details -p \'{"access-token": "fES2CPKJc4mAoD-YUQSL", \
"git-url": "https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git"}\' || cf cups config-server-details -p \'{"access-token": "fES2CPKJc4mAoD-YUQSL", \
"git-url": "https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git"}\'' == \
                         a.toCommand('')
        assert ['cf','uups','config-server-details','-p','{"access-token": "fES2CPKJc4mAoD-YUQSL", \
"git-url": "https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git"}','||','cf','cups','config-server-details','-p','{"access-token": "fES2CPKJc4mAoD-YUQSL", \
"git-url": "https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git"}'] == \
                         a.toProgramArgs('')
        args = ['cf','delete-service', 'config-server-details', '-f']
        assert args == a.toProgramArgs('delete')
        assert 'cf delete-service config-server-details -f' == a.toCommand('delete')

    def test_cups_config_var(self):
        a = cupsConfigVariableClass('processid', {'type': 'ConfigVariable','service_name': 'domain-config', 'value': 'https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git'})
        assert 'cf uups domain-config -p \'{"domain-name": "https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git"}\' || cf cups domain-config -p \'{"domain-name": "https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git"}\'' == a.toCommand('')
        assert ['cf','uups','domain-config','-p', '{"domain-name": "https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git"}',
            '||', 'cf', 'cups', 'domain-config', '-p', '{"domain-name": "https://gitlab.prod.fedex.com/APP3534546/trip-config-repo.git"}'] == a.toProgramArgs('')
        args = ['cf','delete-service', 'domain-config', '-f']
        assert args == a.toProgramArgs('delete')
        assert 'cf delete-service domain-config -f' == a.toCommand('delete')

    def test_cups_uri(self):
        os.environ['level'] = 'L3'
        a = cupsUriClass('process-id',{'service_name': '${level}-trip-discovery-service', 'type' : 'Uri',
                          'uri': 'https://l3-trip-discovery-server.app.wtcdev2.paas.fedex.com/eureka'})
        assert 'cf uups L3-trip-discovery-service -p \'{"uri": "https://l3-trip-discovery-server.app.wtcdev2.paas.fedex.com/eureka"}\' || cf cups L3-trip-discovery-service -p \'{"uri": "https://l3-trip-discovery-server.app.wtcdev2.paas.fedex.com/eureka"}\'' == \
                         a.toCommand('')
        assert ['cf','uups','L3-trip-discovery-service','-p','{"uri": "https://l3-trip-discovery-server.app.wtcdev2.paas.fedex.com/eureka"}',
            '||','cf','cups','L3-trip-discovery-service','-p','{"uri": "https://l3-trip-discovery-server.app.wtcdev2.paas.fedex.com/eureka"}'] == \
                         a.toProgramArgs('')
        args = ['cf','delete-service', 'L3-trip-discovery-service', '-f']
        assert args == a.toProgramArgs('delete')
        assert 'cf delete-service L3-trip-discovery-service -f' == a.toCommand('delete')

    def test_cups_scale(self):
        os.environ['level'] = 'L3'
        a = cupsScaleClass('processid', {'service_name': 'trip-discovery-service', 'type': 'Scale', 'instances' : '3' })
        assert 'cf scale L3-trip-discovery-service -i \'3\'' == a.toCommand('')
        args= ['cf','scale','L3-trip-discovery-service','-i','3'] == a.toProgramArgs('')

    def test_cups_jms_provider_with_encoded_passwords(self):
        os.environ['level'] = 'L3'
        a = cupsJmsProviderClass('process-id',{'service_name': 'JMS-Outbound', 'type': 'JmsProvider',
                                  'context_factory': 'com.fedex.mi.decorator.jms.FedexTibcoInitialContext',
                                  'connection_factory': 'fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3',
                                  'username': 'SEFS-ORCHE-3534641',
                                  'password':  'KVE2rejEzabSzwUem4bYQRR9g',
                                  'provider_url': 'ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com'})
        assert 'cf uups L3_JMS-Outbound -p \'{\
"username": "SEFS-ORCHE-3534641", \
"connection-factory": "fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3", \
"password": "KVE2rejEzabSzwUem4bYQRR9g", \
"context-factory": "com.fedex.mi.decorator.jms.FedexTibcoInitialContext", \
"provider-url": "ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com"\
}\' || cf cups L3_JMS-Outbound -p \'{\
"username": "SEFS-ORCHE-3534641", \
"connection-factory": "fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3", \
"password": "KVE2rejEzabSzwUem4bYQRR9g", \
"context-factory": "com.fedex.mi.decorator.jms.FedexTibcoInitialContext", \
"provider-url": "ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com"\
}\'' == a.toCommand('')
        args = ['cf','uups','L3_JMS-Outbound', '-p','{"username": "SEFS-ORCHE-3534641", \
"connection-factory": "fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3", \
"password": "KVE2rejEzabSzwUem4bYQRR9g", \
"context-factory": "com.fedex.mi.decorator.jms.FedexTibcoInitialContext", \
"provider-url": "ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com"\
}', '||', 'cf', 'cups', 'L3_JMS-Outbound', '-p', '{\
"username": "SEFS-ORCHE-3534641", \
"connection-factory": "fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3", \
"password": "KVE2rejEzabSzwUem4bYQRR9g", \
"context-factory": "com.fedex.mi.decorator.jms.FedexTibcoInitialContext", \
"provider-url": "ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com"\
}']
        assert  args == a.toProgramArgs('')
        args = ['cf','delete-service', 'L3_JMS-Outbound', '-f']
        assert args == a.toProgramArgs('delete')
        assert 'cf delete-service L3_JMS-Outbound -f' == a.toCommand('delete')


    def test_cups_jms_provider(self):
        os.environ['level'] = 'L3'
        a = cupsJmsProviderClass('process-id',{'service_name': 'JMS-Outbound', 'type': 'JmsProvider',
                                  'context_factory': 'com.fedex.mi.decorator.jms.FedexTibcoInitialContext',
                                  'connection_factory': 'fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3',
                                  'username': 'SEFS-ORCHE-3534641',
                                  'password':  'KVE2rejEzabSzwUem4bYQRR9g',
                                  'provider_url': 'ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com'})
        assert 'cf uups L3_JMS-Outbound -p \'{\
"username": "SEFS-ORCHE-3534641", \
"connection-factory": "fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3", \
"password": "KVE2rejEzabSzwUem4bYQRR9g", \
"context-factory": "com.fedex.mi.decorator.jms.FedexTibcoInitialContext", \
"provider-url": "ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com"\
}\' || cf cups L3_JMS-Outbound -p \'{\
"username": "SEFS-ORCHE-3534641", \
"connection-factory": "fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3", \
"password": "KVE2rejEzabSzwUem4bYQRR9g", \
"context-factory": "com.fedex.mi.decorator.jms.FedexTibcoInitialContext", \
"provider-url": "ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com"\
}\'' == a.toCommand('')


    @skipping_when_missing_cryptology
    def test_cups_jms_provider_encoded_passwords(self):
        os.environ['level'] = 'L3'
        a = cupsJmsProviderClass('process-id',{'service_name': 'JMS-Outbound', 'type': 'JmsProvider',
                                  'context_factory': 'com.fedex.mi.decorator.jms.FedexTibcoInitialContext',
                                  'connection_factory': 'fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3',
                                  'username': 'SEFS-ORCHE-3534641',
                                  'password':  'ENC(qAe2DdctNsxVg5aHW7QeT5CInrC2owhEaH020aUvPMyrn+hM2X1RWmKkTBGp1OF4)',
                                  'provider_url': 'ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com'})
        assert 'cf uups L3_JMS-Outbound -p \'{\
"username": "SEFS-ORCHE-3534641", \
"connection-factory": "fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3", \
"password": "KVE2rejEzabSzwUem4bYQRR9g", \
"context-factory": "com.fedex.mi.decorator.jms.FedexTibcoInitialContext", \
"provider-url": "ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com"\
}\' || cf cups L3_JMS-Outbound -p \'{\
"username": "SEFS-ORCHE-3534641", \
"connection-factory": "fxClientUID=S.3534641.FDXSHIPEFS.FXE.INTERNAL.CORE.T.L3", \
"password": "KVE2rejEzabSzwUem4bYQRR9g", \
"context-factory": "com.fedex.mi.decorator.jms.FedexTibcoInitialContext", \
"provider-url": "ldap://apptstldap.corp.fedex.com:389/ou=messaging,dc=corp,dc=fedex,dc=com"\
}\'' == a.toCommand('')

    def test_cups_database_service(self):
        os.environ['level'] = 'L4'
        a = cupsDatabaseServiceClass('process-id', {'service_name': 'Database-Details', 'type': 'DatabaseService',
                                    'url': 'jdbc:oracle:thin:@ldap://oid.inf.fedex.com:3060/SEFS_ORCH_SVC1_L1,cn=OracleContext',
                                      'username': 'SEFS_ORCH_SCHEMA',
                                      'password':  'sefsMSpwd0123456789101112',
                                      'driver_class_name': 'oracle.jdbc.driver.OracleDriver'})
        assert 'cf uups L4-Database-Details -p \'{"url": "jdbc:oracle:thin:@ldap://oid.inf.fedex.com:3060/SEFS_ORCH_SVC1_L1,cn=OracleContext", \
"username": "SEFS_ORCH_SCHEMA", \
"driver-class-name": "oracle.jdbc.driver.OracleDriver", \
"password": "sefsMSpwd0123456789101112"}\' || cf cups L4-Database-Details -p \'{"url": "jdbc:oracle:thin:@ldap://oid.inf.fedex.com:3060/SEFS_ORCH_SVC1_L1,cn=OracleContext", \
"username": "SEFS_ORCH_SCHEMA", \
"driver-class-name": "oracle.jdbc.driver.OracleDriver", \
"password": "sefsMSpwd0123456789101112"}\'\
' == a.toCommand('')
        args = ['cf','uups','L4-Database-Details','-p','{"url": "jdbc:oracle:thin:@ldap://oid.inf.fedex.com:3060/SEFS_ORCH_SVC1_L1,cn=OracleContext", \
"username": "SEFS_ORCH_SCHEMA", \
"driver-class-name": "oracle.jdbc.driver.OracleDriver", \
"password": "sefsMSpwd0123456789101112"}', '||','cf','cups','L4-Database-Details','-p','{"url": "jdbc:oracle:thin:@ldap://oid.inf.fedex.com:3060/SEFS_ORCH_SVC1_L1,cn=OracleContext", \
"username": "SEFS_ORCH_SCHEMA", \
"driver-class-name": "oracle.jdbc.driver.OracleDriver", \
"password": "sefsMSpwd0123456789101112"}']
        assert args == a.toProgramArgs('')

    @skipping_when_missing_cryptology
    def test_cups_database_service_with_encoded_password(self):
        os.environ['level'] = 'L4'
        a = cupsDatabaseServiceClass('process-id', {'service_name': 'Database-Details', 'type': 'DatabaseService',
                                    'url': 'jdbc:oracle:thin:@ldap://oid.inf.fedex.com:3060/SEFS_ORCH_SVC1_L1,cn=OracleContext',
                                      'username': 'SEFS_ORCH_SCHEMA',
                                      'password':  'ENC(Ldu6CfOwc0nZ054EWcj7WjrIhybMXDowpozYHOHhaB3I5fV4fvwJFOYpKCYY/BKb)',
                                      'driver_class_name': 'oracle.jdbc.driver.OracleDriver'})
        assert 'cf uups L4-Database-Details -p \'{"url": "jdbc:oracle:thin:@ldap://oid.inf.fedex.com:3060/SEFS_ORCH_SVC1_L1,cn=OracleContext", \
"username": "SEFS_ORCH_SCHEMA", \
"driver-class-name": "oracle.jdbc.driver.OracleDriver", \
"password": "sefsMSpwd0123456789101112"}\' || cf cups L4-Database-Details -p \'{"url": "jdbc:oracle:thin:@ldap://oid.inf.fedex.com:3060/SEFS_ORCH_SVC1_L1,cn=OracleContext", \
"username": "SEFS_ORCH_SCHEMA", \
"driver-class-name": "oracle.jdbc.driver.OracleDriver", \
"password": "sefsMSpwd0123456789101112"}\'' == a.toCommand('')


    def test_service_autoscaler(self):
        a = serviceAutoScalerClass('process_id', {'service_name': 'name', 'type': 'AutoScaler', 'scaler-name': 'cpp-test', 'enabled':'true', 'instance_limits' :{  'min': 1, 'max': 4}})
        assert ['cf','create-service','app-autoscaler','standard', 'cpp-test' ]== a.toProgramArgs('')
        assert ['cf','delete-service','app-autoscaler', 'cpp-test'] == a.toProgramArgs('delete')
        assert 'cf create-service app-autoscaler standard "cpp-test"' == a.toCommand('')
        assert 'cf delete-service app-autoscaler "cpp-test"' == a.toCommand('delete')
        assert 'cf bind-service "process_id" "cpp-test" \'{"instance_limits": {"max": 4, "min": 1}, "enabled": "true"}\' && cf restage "process_id"' == a.toCommand('bind')
        assert 'cf unbind-service "process_id" "cpp-test" && cf restage "process_id"' == a.toCommand('unbind')

    def test_git_config_http_server(self):
        a = cupsGitConfigHttpServerClass('process_id', {'type' : 'GitConfigHttpServer', 'service_name' : 'cpp-repo', 'git-url': 'url', 'git-key': 'cppUrl', 'username':'user', 'user-key': 'user-key', 'password' : 'ENC(td1K9uwuxZd3EMOeptRLLoqFU0gfc+XQfwr/K1uo+y8=)', 'password-key' : 'cppPwd'})
        #assert 'cf uups cpp-repo -p \'{"cppUrl": "url", "user-key": "user", "cppPwd": "asdf"}\''
        assert ['cf','uups','cpp-repo','-p', '{"cppUrl": "url", "user-key": "user", "cppPwd": "asdf"}' 
                ,'||',
                'cf','cups','cpp-repo','-p', '{"cppUrl": "url", "user-key": "user", "cppPwd": "asdf"}'
                
                ]== a.toProgramArgs('')
        assert a.toCommand('install') == 'cf uups cpp-repo -p \'{"cppUrl": "url", "user-key": "user", "cppPwd": "asdf"}\' || cf cups cpp-repo -p \'{"cppUrl": "url", "user-key": "user", "cppPwd": "asdf"}\'; cf bind-service "process_id" "cpp-repo"  && cf restage "process_id"' 



    #@skipping_when_running_outside_fedex_network
    def test_integration(self):

        options = get_options()
        options.overwrite(action='install',user='pcf',command='deploy',X=False,level='L3')
        os.environ['level'] = 'L3'
        fdl = fdeployLoader(options)
        fdl.readDescriptors(
            ['../../test/resources/pcf-config/pcf-cups-config.json'])
        component = fdl.components[0]
        testgenerator = pcf_cupsGenerator(component, options)
        struct = {'level': component.levels[0], 'rtv': component.rtv , 'directory': '.',
                  'contents': component.content, 'targets': component.levels[0]['targets'],  'uuid': 'all'}
        with open('cups.out','w') as fh_out:
            testgenerator.generate(struct,fh_out)
        compare_files('./../../test/resources/pcf-config/cups.out', './cups.out')

    def test_integration2(self):

        options = get_options()
        options.overwrite(action='install',user='pcf',command='deploy',X=False,level='L3')
        os.environ['level'] = 'L3'
        fdl = fdeployLoader(options)
        fdl.readDescriptors(
            ['../../test/resources/pcf-config/pcf-cups-config2.json'])
        component = fdl.components[0]
        testgenerator = pcf_cupsGenerator(component, options)
        struct = {'level': component.levels[0], 'rtv': component.rtv , 'directory': '.',
                  'contents': component.content, 'targets': component.levels[0]['targets'],  'uuid': 'all'}
        with open('cups2.out','w') as fh_out:
            testgenerator.generate(struct,fh_out)
        compare_files('./../../test/resources/pcf-config/cups2.out', './cups2.out')
