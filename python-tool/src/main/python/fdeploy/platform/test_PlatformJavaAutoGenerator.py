from fdeploy.UtilsForTesting import  assertPropertiesGeneration
from fdeploy.UtilsForTesting import get_options, compare_files
from fdeploy.fdeployLoader import fdeployLoader, create_manifest_resolver

class TestJavaAutoGenerator(object):

    properties =  '''   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
export APP_ID="pmi-webber"
export ARTIFACTID="pmi-web"
export EAI_NUMBER="102354"
export GROUPID="com.fedex.ground.pmi"
export JAVA_HOME="/opt/java/whatever/"
export LEVEL="L1"
export LOG_ROOT="/opt/fedex/pmi/dev"
export TMP_DIR="/opt/fedex/pmi/dev/"
export VERSION="0.0.1-SNAPSHOT"

EOF
'''
    properties_overwrite =  '''   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
export APP_ID="pmi-webber"
export ARTIFACTID="pmi-web"
export EAI_NUMBER="102354"
export GROUPID="com.fedex.ground.pmi"
export JAVA_HOME="/opt/fedex/java"
export LEVEL="L1"
export LOG_ROOT="/opt/tmp/var/"
export TMP_DIR="/opt/fedex/pmi/dev/"
export VERSION="0.0.1-SNAPSHOT"

EOF
'''

    def test_properties_creation(self):
        options = get_options()
        options.overwrite(command='deploy',action='properties',identity='.',
        X=True, id = None, path=".", no_wait = False, level='L1',
        user='sefs')
        assertPropertiesGeneration(options,'java_autoGenerator',
            'JAVA_SCM_PMI_BOOT_%0d',
            '../../test/resources/FXE_fdeploy/scm-java-auto.json', self.properties)

    def test_properties_creation_with_overwrite(self):
        options = get_options()
        options.overwrite(command='deploy',action='properties',identity='.',
            X=True, id = None, path=".", no_wait = False, level='L1',
            user='sefs', rtv=["JAVA_HOME=/opt/fedex/java", "LOG_ROOT=/opt/tmp/var/"])
        assertPropertiesGeneration(options,'java_autoGenerator',
            'JAVA_SCM_PMI_BOOT_%0d',
            '../../test/resources/FXE_fdeploy/scm-java-auto.json', self.properties_overwrite)


    def test_deploy_deploy(self):
        manifest_resolver = create_manifest_resolver('../../test/resources/FXE_fdeploy', 'L1')
        o=get_options()
        o.command = 'deploy'
        o.action = 'deploy'
        o.level = 'L1'
        o.identity = '/home/ak751818/.ssh/test_deploy_user_dsa'
        fdl = fdeployLoader(o)
        fdl.readDescriptors(
            ['../../test/resources/FXE_fdeploy/fxs-dashboard-java-auto.json'])
        # switch from java to java_auto
        fdl.components[0].type = 'java_auto'
        fdl.generate_level('L1')
        compare_files('../../test/resources/java-auto/myscript-deploy-deploy.sh','tmp/myscript.sh')

if __name__ == '__main__':
    unittest.main()
