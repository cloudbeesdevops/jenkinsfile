# -*- coding: utf-8 -*-
from __future__ import print_function

import os
import re
import subprocess
import fdeploy
import fdeploy.platform
import fdeploy.fdeployLoader
import time
from fdeploy.platform import load_snippet, baseGenerator
from fdeploy.platform import DEFAULT_TARGET_SPEC, _needs_nexus_resolving

REQUIRED_RTVS = []
REGISTERED_ACTIONS = ['clean', 'wipeout', 'start', 'stop', 'install', 'stage', 'ml', 'machine_list',
        'deploy', 'publish', 'unpublish', 'restart', 'report', 'ssh', 'inspect', 'ps', 'cloudenv','status','relink','properties']
TARGET_URL_SPEC=DEFAULT_TARGET_SPEC

class javaDeployer(baseGenerator):
    def __init__(self, component, cli_options, stage_directory):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_java_generator.REGISTERED_ACTIONS
        self.REQUIRED_RTVS = fdeploy.platform.bash_java_generator.REQUIRED_RTVS
        super(javaDeployer, self).__init__(
            component, cli_options, stage_directory)
        fdeploy.LOGGER.debug(
            " < init bash_java_generator javaDeployer platform for " + str(self.component.prettyprint()))

    def deploying(self, files, pause):
        p = []
        i = 0
        if len(files) < 1:
            raise Exception("Abort! No files were generated for this deployment of type %s." % (self))
        for _file in files:
            i += 1
            fdeploy.LOGGER.debug(" > deploying execution: %s of %s / %s" % (i,len(files),_file))
            fdeploy.LOGGER.trace(" > files are %s" % (files))
            __file = str(self.stage_directory) + "/" + str(_file)
            _filename = str(self.stage_directory) + \
                "/deploy-" + str(_file) + ".log"
            os.chmod(__file, 0b111101101)  # rwxr-xr-x make it executable
            # subprocess.call(["/var/tmp/fdeploy/xls.sh"])
            # remote invocation of the generated script
            with open(_filename, 'w') as __stdout:
                #subprocess.call([__file], stdout=__stdout)
                if self.options.parallel == False:
                    subprocess.call([__file], stdout=__stdout)
                else:
                    with open(os.devnull, 'r+b', 0) as DEVNULL:
                        px = subprocess.Popen([__file],
                            stdin=DEVNULL, stdout=DEVNULL, stderr=__stdout, close_fds=True)
                            #stdin=DEVNULL, stdout=__stdout, stderr=__stdout, close_fds=True)
                        p.append(px)
                        # Wait until process terminates
                        while px.poll() is None:
                            time.sleep(1)

class javaGenerator(baseGenerator):

    files = []

    remote_script_begin= load_snippet('remote_script_begin.sh')
    remote_overwrite_snippet = load_snippet('remote_overwrite_snippet.sh')
    remote_stage_snippet_begin = load_snippet('remote_stage_snippet_begin.sh')
    remote_cleanup_snippet = load_snippet('remote_cleanup_snippet.sh')
    remote_wipeout_snippet = load_snippet('remote_wipeout_snippet.sh')
    remote_relink_snippet = load_snippet('remote_relink_snippet.sh')
    remote_restart_snippet = load_snippet('remote_restart_snippet.sh')
    remote_stop_snippet = load_snippet('remote_stop_snippet.sh')
    remote_run_snippet = load_snippet('remote_run_snippet.sh')
    remote_status_snippet = load_snippet('remote_status_snippet.sh')
    remote_stage_snippet_end = load_snippet('remote_stage_snippet_end.sh')
    remote_script_end = load_snippet('remote_script_end.sh')
    remote_create_scripts_snippet = ""

    def __init__(self, component, cli_options, stage_directory='.', target_spec = DEFAULT_TARGET_SPEC):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_java_generator.REGISTERED_ACTIONS
        self.REQUIRED_RTVS = fdeploy.platform.bash_java_generator.REQUIRED_RTVS
        super(javaGenerator, self).__init__(
            component, cli_options, stage_directory, target_spec)
        fdeploy.LOGGER.debug(
            " < init platform: bash_java_generator, class: javaGenerator  for " + str(self.component))

    def list_machines(self, struct,target_list,search=None):
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        for target in __targets:
            _target = target.split(":")[0]
            if self.options.user is not None:
                _target = re.sub(
                    r'^(\S+)\@', self.options.user + str('@'), _target)
            if not _target in target_list:
                target_list.append(_target)

    """Generate scripts for the components on the targets in the descriptors."""
    def generate(self, struct, outfile, action='status',target_spec=None,verbose=False):
        # ssh_options
        level = struct['level']
        # replace values in the snippets
        target_list = ''
        upload_list = ''
        envvar_list = ''
        rtv_map = {}
        app_user=None
        # pick the first Nexus resolved component artifact.
        nexusContent = self.get_first_nexus_component_in_content(_needs_nexus_resolving(action,False))

        # agregation of steps.
        actions = []
        if action == 'deploy':
            actions.append('stop')
            actions.append('install')
            actions.append('restart')
        elif action == 'clean' and not self.component.versions_to_persist is None and len(self.component.versions_to_persist) > 1:
            self.component.rtvMapping['CANDIDATES']=','.join(self.component.versions_to_persist)
            actions.append('clean_with_manifest')
        else:
            actions.append(action)
        #
        # resolving the runtime variables (rtv), first global then components.
        #
        rtv_map,envvar_list=self.component.get_env_vars(self.options,level['level'], self.REQUIRED_RTVS, nexusContent.version)
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        fdeploy.LOGGER.debug("hosts : %s spec %s"  % (str(__targets), target_spec))
        target,target_list=self.get_target_list(__targets,target_spec)
        fdeploy.LOGGER.info("\n\n %s\n%s\n%s" % (__targets, target, target_list))
        if len(target_list) < 2:
            raise Exception("targetlist empty? %s" % (struct['targets']))
        #
        # generate the upload list based on the contents specified
        #
        upload_list=self.get_upload_list(struct,nexusContent.version)
        #
        # applying execute identity if applicable
        #
        pki_identiy = ' ' if self.options.identity is None or self.options.identity == '' else '-i ' + \
            str(self.options.identity)
        verboseOn = "set -xv"
        verboseOff = "set +xv"
        verboseFlag = verboseOff if verbose == False else verboseOn

        # NO WAIT FLAG
        nowaitflag = '' if self.options.no_wait == False else '#SKIP#'

        log_array = fdeploy.bash_array()
        fdeploy.LOGGER.trace("log_array=" + str(log_array))

        fdeploy.LOGGER.info( "generating for %s individual actions [%s]" % (len(actions),actions) )
        for _action in actions:
            # only echo the env vars on certain actions.
            echoEnvVars = verboseOff if _action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING else verboseFlag
            #
            # find the application user, assuming the user being supplied is the cm user
            # based on the last processed target
            # TODO: might need more attention when you have targets with differnt users
            #
            app_user = self.get_app_user(target,app_user,_action)

            print(self.data_wrapper_header % (
                self.options.command,
                _action,
                self.options.level,
                log_array[0], log_array[1], log_array[2], log_array[3],
                _action,
                pki_identiy,
                self.ssh_options,
                echoEnvVars,
                verboseFlag,
                app_user,
                struct['uuid'],
                nowaitflag,
                '20', #timeout
                target_list,
                upload_list,
                upload_list,
                self.stage_directory,
                nexusContent.version,
                nexusContent.saveArchiveName,
                nexusContent.artifactId,
                '%'
            ), file=outfile)

            data = []
            _remote_script_begin= self.remote_script_begin % (
                log_array[0], log_array[1], log_array[2], log_array[3],
                envvar_list, verboseFlag)
            _remote_properties_snippet = self.remote_properties_snippet % (envvar_list)
            _remote_readme = None
            if 'readme' in struct.keys() and struct['readme'] is not None:
                _remote_readme = self.remote_readme_snippet % (struct['readme'])
            fdeploy.LOGGER.info("generating for [%s] as %s" % ( _action, app_user))
            self.write(outfile,_action,data,_remote_script_begin,
                _remote_properties_snippet, _remote_readme)
        if _action not in ['test', 'inspect', 'ssh', 'ps','cloudenv']:
            print(self.data_wrapper_execute,file=outfile)
