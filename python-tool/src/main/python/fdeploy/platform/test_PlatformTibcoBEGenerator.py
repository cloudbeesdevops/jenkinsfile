from fdeploy.UtilsForTesting import assertPropertiesGeneration
from fdeploy.UtilsForTesting import get_options


class TestTibcoBEGenerator(object):

    properties =  '''   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
export ARTIFACTID="sefs_suDvisPnD"
export BE_HOME="/opt/tibco/ra101/be/5.2"
export EAINUMBER="6926"
export GROUPID="com.fedex.sefs.domain"
export JAVA_HOME="/opt/java/hotspot/8/current"
export JMX_PORT="31410"
export LEVEL="L1"
export LOGICAL_NAME="FXE_SEFS_DVIS_PND_PICKUP_AND_DELIVERY_${instance}"
export LOG_DIR="/var/fedex/sefs/logs"
export PU_NAME="sefsDvisPnD"
export TIBCO_HOME="/opt/tibco/ra101"

EOF
'''

    def test_properties_creation(self):
        options = get_options()
        options.overwrite(action='properties',user='sefs')

        assertPropertiesGeneration(options,'tibco_beGenerator',
        'BE_FXE_sefs_be_suDvisPnD_PickupAndDeliveryPU_%0d',
        '../../test/resources/FXE_fdeploy/fxe-be-sefsDvisPnD.json', self.properties)


    def test_properties_creation(self):
        options = get_options()
        options.overwrite(action='properties',user='sefs')

        assertPropertiesGeneration(options,'tibco_beGenerator',
        'BE_FXE_sefs_be_suDvisPnD_PickupAndDeliveryPU_%0d',
        '../../test/resources/FXE_fdeploy/fxe-be-sefsDvisPnDWithEncodedPasswords.json', self.properties)


if __name__ == '__main__':
    unittest.main()
