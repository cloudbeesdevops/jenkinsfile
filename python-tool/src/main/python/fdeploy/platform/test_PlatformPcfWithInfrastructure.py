import copy
import fdeploy
from fdeploy import Options
from fdeploy.UtilsForTesting import compare_files
import os
from fdeploy.platform.bash_pcf_generator import pcfGenerator
from fdeploy.content.fdeployComponent import fdeployComponent
from fdeploy.MockHttpServer import start_mock_server
from fdeploy.test_PAM import pamAuthRequestHandler, setup_pam_url

#TEST_HTTP_PORT=None

def createComp():
    return fdeployComponent({

    "type": "pcf",
    "rtv": [{
        "name": "APP_NM",
        "value": "handling-proxy"
    },
        {
        "name": "EAI_NUMBER",
        "value": "3534546"
    },
        {
        "name": "NUM_INSTANCES",
        "value": "1"
    },
        {
        "name": "SERVICE_INSTANCES",
        "value": "1"
    },
        {
        "name": "APPD_NM",
        "value": "sefs-handling-proxy"
    },
        {
        "name": "LOG_LEVEL",
        "value": "1"
    },
        {
        "name": "SPRING_PROFILES_ACTIVE",
        "value": "L1"
    },
        {
        "name": "PAM_ID",
        "value": "18414"
    },
        {
        "name": "DISCOVERY_SERVICE",
        "value": "handling-discovery-service"
    }
    ],
    "levels": [{
        "rtv": [],
        "level": "L1",
        "targets": [
            "development@api.sys.wtcdev2.paas.fedex.com"
        ]
    }, {
        "rtv": [],
        "level": "L2",
        "targets": [
            "development@api.sys.wtcdev2.paas.fedex.com"
        ]
    },
        {"rtv": [],
         "level": "L3",
         "targets": [
            "release@api.sys.wtcdev2.paas.fedex.com"
        ]
    },
        {"rtv": [],
         "level": "L4",
         "targets": [
            "release@api.sys.wtcdev2.paas.fedex.com"
        ]
    },
        {
        "rtv": [],
        "level": "PROD",
        "targets": [
            "production@api.sys.edcbo1.paas.fedex.com"
        ]
    }],
    "infrastructure" : {
        'service-config' : '../../test/resources/pcf-config/pcf-cups-config.json',
        'depends-on' : 'Basic-Security,${level}-handling-unit-discovery-service',
    },
    "content": [{
        "gav": "com.fedex.sefs.core.handling:handling-proxy:1.0.3:jar",
        "saveArchiveName": "pmi-web.jar"
    }],
    "id": "handling-proxy",
    "name_format": "PCF_SEFS_TRIP_PROXY_%0d",
    "platform": "bash_generator"

}, 'inline.json')
    
def createCompWithInline():
    return fdeployComponent({

    "type": "pcf",
    "rtv": [{
        "name": "APP_NM",
        "value": "handling-proxy"
    },
        {
        "name": "EAI_NUMBER",
        "value": "3534546"
    },
        {
        "name": "NUM_INSTANCES",
        "value": "1"
    },
        {
        "name": "SERVICE_INSTANCES",
        "value": "1"
    },
        {
        "name": "APPD_NM",
        "value": "sefs-handling-proxy"
    },
        {
        "name": "LOG_LEVEL",
        "value": "1"
    },
        {
        "name": "SPRING_PROFILES_ACTIVE",
        "value": "L1"
    },
        {
        "name": "PAM_ID",
        "value": "18414"
    },
        {
        "name": "DISCOVERY_SERVICE",
        "value": "handling-discovery-service"
    }
    ],
    "levels": [{
        "rtv": [],
        "level": "L1",
        "targets": [
            "development@api.sys.wtcdev2.paas.fedex.com"
        ]
    }, {
        "rtv": [],
        "level": "L2",
        "targets": [
            "development@api.sys.wtcdev2.paas.fedex.com"
        ]
    },
        {"rtv": [],
         "level": "L3",
         "targets": [
            "release@api.sys.wtcdev2.paas.fedex.com"
        ]
    },
        {"rtv": [],
         "level": "L4",
         "targets": [
            "release@api.sys.wtcdev2.paas.fedex.com"
        ]
    },
        {
        "rtv": [],
        "level": "PROD",
        "targets": [
            "production@api.sys.edcbo1.paas.fedex.com"
        ]
    }],
    "infrastructure" : {
        'service-config' : '../../test/resources/pcf-config/pcf-inline.json',
        'depends-on' : 'blablascriptwithcfcommands,Basic-Security',
    },
    "content": [{
        "gav": "com.fedex.sefs.core.handling:handling-proxy:1.0.3:jar",
        "saveArchiveName": "pmi-web.jar"
    }],
    "id": "handling-proxy",
    "name_format": "PCF_SEFS_TRIP_PROXY_%0d",
    "platform": "bash_generator"

}, 'inline.json')


class TestPlaformPcfWithInfrastructureGenerator(object):

    options = Options(command='deploy', action='stage', identity='.',
                           X=True, id=None, path=".", no_wait=False, level='L4', user='sefs')
    rtvs = [
        {'name': 'APP_REF', 'value': '${APPMANAGE_PLAN}.backupt'},
        {'name': 'LEVEL', 'value': 'L4'}]

    @classmethod
    def setup_class(module):
        #os.environ['NO_PROXY']='127.0.0.1'
        os.environ['MOCK_USERNAME'] = 'FXS_app3534546'
        port = start_mock_server(pamAuthRequestHandler)
        fdeploy.pam.PAM_URL= setup_pam_url(port)

    #@skipping_when_running_outside_fedex_network
    def test_generate_status(self):
        os.environ['level'] = 'L4'
        rtvs = copy.deepcopy(self.rtvs)
        options = copy.deepcopy(self.options)
        component = createComp()
        generator = pcfGenerator(component, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        assert 2 ==  len(component.services)
        struct = {'level': component.levels[3], 'rtv': rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-with-infrastructure.txt", "w") as fh:
            generator.generate(struct, fh) # default action is status
        compare_files('../../test/resources/pcf/platform-pcf-with-infra1.txt',"platform-pcf-with-infrastructure.txt")

    #@skipping_when_running_outside_fedex_network
    def test_generate_deploy(self):
        rtvs = copy.deepcopy(self.rtvs)
        options = copy.deepcopy(self.options)
        component = createComp()
        generator = pcfGenerator(component, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        assert 2 ==  len(component.services)
        struct = {'level': component.levels[3], 'rtv': rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-with-infrastructure-deploy.txt", "w") as fh:
            generator.generate(struct, fh, 'deploy')
        #assert load_file('../../test/resources/pcf/platform-pcf-with-infra1-deploy.txt') == load_file('platform-pcf-with-infrastructure-deploy.txt')
        compare_files('../../test/resources/pcf/platform-pcf-with-infra1-deploy.txt','platform-pcf-with-infrastructure-deploy.txt')
    def test_generate_bluegreen(self):
        rtvs = copy.deepcopy(self.rtvs)
        options = copy.deepcopy(self.options)
        component = createComp()
        generator = pcfGenerator(component, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        assert 2 ==  len(component.services)
        struct = {'level': component.levels[3], 'rtv': rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-with-infrastructure-bluegreen.txt", "w") as fh:
            generator.generate(struct, fh, 'bluegreen')
        #assert load_file('../../test/resources/pcf/platform-pcf-with-infra1-deploy.txt') == load_file('platform-pcf-with-infrastructure-deploy.txt')
        compare_files('../../test/resources/pcf/platform-pcf-with-infra1-bluegreen.txt','platform-pcf-with-infrastructure-bluegreen.txt')

    def test_generate_inline_command(self):
        rtvs = copy.deepcopy(self.rtvs)
        options = copy.deepcopy(self.options)
        component = createCompWithInline()
        generator = pcfGenerator(component, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        assert 2 ==  len(component.services)
        struct = {'level': component.levels[3], 'rtv': rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-with-infrastructure-inline.txt", "w") as fh:
            generator.generate(struct, fh, 'deploy')
        compare_files('../../test/resources/pcf/platform-pcf-with-infrastructure-inline.txt',"platform-pcf-with-infrastructure-inline.txt")
        

