

from fdeploy import Options
from fdeploy.platform.bash_tibcobw_generator import tibco_bwGenerator
import os
from fdeploy.content.fdeployComponent import fdeployComponent
from fdeploy.content import content_factory
from fdeploy.UtilsForTesting import compare_files
import copy
import pytest

def do_component():
    return fdeployComponent({
    'id': 'componentId',
    'platform': 'bash_generator',
    'type': 'tibco_bw',
    'rtv': [
        {
            "name": "TRA_HOME",
            "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
        },
        {
            "name": "JMX_PORT",
            "value": "abcdef"
        },
        {
            "name": "PROCESS_NAME",
            "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
        },
        {
            "name": "ADMIN_TARGET_URL",
            "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
        },
        {
            "name": "EAINUMBER",
            "value": "#CLEARANCEFeed"
        },
        {
            "name": "APPMANAGE_CREDENTIALS",
            "value": "#CLEARANCEFeed"
        },
        {
            "name": "EAINUMBER",
            "value": "#CLEARANCEFeed"
        },
        {
            "name": "APPMANAGE_XML",
            "value": "../../test/resources/bw5/FXFShipFeed_template_l2.xml"
        },
        {
            "name": "APPMANAGE_PLAN",
            "value": "../../test/resources/bw5/FXFShipFeed_plan-descriptors.xml"
        }


    ],
    'content':  [{
        "gav": "com.fedex.sefs.core:sefs_suCLEARANCEFeed:1.0.0:zip",
        "relativePath": "apparchives",
        "saveArchiveName": "sefs_suCLEARANCEFeed.zip"
    }],
    'name_format': 'format',
    'levels':
    [{
        'level': 'L1',
        'filter' : { 'maxHeapSize' : 'value2'},
        'targets' : [ 'a@com', 'b@com', 'c@com']
    }]

}, 'inline.json')

def do_component_no_version():
    return fdeployComponent({
    'id': 'componentId2',
    'platform': 'bash_generator',
    'type': 'tibco_bw',
    'rtv': [
        {
            "name": "TRA_HOME",
            "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
        },
        {
            "name": "JMX_PORT",
            "value": "abcdef"
        },
        {
            "name": "PROCESS_NAME",
            "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
        },
        {
            "name": "ADMIN_TARGET_URL",
            "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
        },
        {
            "name": "EAINUMBER",
            "value": "#CLEARANCEFeed"
        },
        {
            "name": "APPMANAGE_CREDENTIALS",
            "value": "#CLEARANCEFeed"
        },
        {
            "name": "EAINUMBER",
            "value": "#CLEARANCEFeed"
        },
        {
            "name": "APPMANAGE_XML",
            "value": "../../test/resources/bw5/FXFShipFeed_template_l2.xml"
        },
        {
            "name": "APPMANAGE_PLAN",
            "value": "../../test/resources/bw5/FXFShipFeed_plan-descriptors.xml"
        }


    ],
    'content':  [{
        "gav": "com.fedex.sefs.core:sefs_suCLEARANCEFeed:zip",
        "relativePath": "apparchives",
        "saveArchiveName": "sefs_suCLEARANCEFeed.zip"
    }],
    'name_format': 'format',
    'levels':
    [{
        'level': 'L1',
        'filter' : { 'maxHeapSize' : 'value2'},
        'targets' : [ 'a@com', 'b@com', 'c@com']
    }]

}, 'inline.json')


class TestTibcoBWGenerator(object):


    rtvs =  [
        { 'name': 'APP_REF', 'value' : '${APPMANAGE_PLAN}.backupt'},
        { 'name': 'LEVEL', 'value' : 'L1'}]
    options = Options(command='deploy',action='stage',identity='.', X=False, id = None,
        path=".", no_wait = False, level='L1',user='sefs', ignore_manifest_errors=False)

    @classmethod
    def teardown_class(module):
        try:
            #os.remove('platformBW5-1.txt')
            os.remove('platformBW-2.txt')
        except OSError:
            pass


    def test_init(self):
        options = copy.deepcopy(self.options)
        tibco_bwGenerator(do_component(), options)

    def test_get_target_list(self):
        options = copy.deepcopy(self.options)
        options.path = None
        component_no_version = do_component_no_version()
        generator = tibco_bwGenerator(component_no_version, options)
        targets = component_no_version.levels[0]['targets']
        assert 3 == len(targets)
        with pytest.raises(Exception):
            generator.get_target_list(targets, generator.target_spec)
        tar=[]
        for t in targets:
            tar.append("%s:/opt/permafrost" % (t) )
        assert 3 == len(tar)
        (targets, targetstring) = generator.get_target_list(tar, generator.target_spec)
        assert 3 == len(targets)
        def validate(result,targets):
            for t in targets:
                assert t == result
        validate('sefs@com:/opt/permafrost',targets  )
        options.user = '751717'
        options.path = '/var/fedex/apps'
        generator = tibco_bwGenerator(component_no_version, options)
        (targets, targetstring) = generator.get_target_list(tar, generator.target_spec)
        validate('751717@com:/var/fedex/apps', targets)

    def test_get_target_list_overide_user(self):
        options = copy.deepcopy(self.options)
        options.user = '751818'
        component_no_version = do_component_no_version()
        generator = tibco_bwGenerator(component_no_version, options)
        tar=[]
        for t in component_no_version.levels[0]['targets']:
            tar.append("%s:/opt/permafrost" % (t) )
        assert 3 == len(tar)
        (targets, targetstring) = generator.get_target_list(tar, generator.target_spec)
        assert 3 == len(targets)
        assert tar[0] != targets[0]

    def test_generate(self):
        component = do_component()
        options = copy.deepcopy(self.options)
        generator = tibco_bwGenerator(component, options)
        targets = []
        struct = {'level': {'level':'L1','rtv' : self.rtvs }, 'directory': '.', 'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platformBW5-1.txt", "w") as fh:
            generator.generate(struct, fh)
        compare_files("../../test/resources/bw5/platformBW5-1.expected", "platformBW5-1.txt")

    def test_generate_with_version_resolve(self):
        options = copy.deepcopy(self.options)
        options.path = None
        component_no_version = do_component_no_version()
        generator = tibco_bwGenerator(component_no_version, options)
        #print "\n%s\n" % (component_no_version.content[0])
        cont = content_factory(component_no_version.content[0])
        assert "com.fedex.sefs.core:sefs_suCLEARANCEFeed:zip" ==  component_no_version.content[0]['gav']
        assert "sefs_suCLEARANCEFeed" ==  cont.artifactId
        assert cont.version is None
        struct = {'level': {'level':'L1','rtv' : self.rtvs }, 'directory': '.', 'contents': component_no_version.content,
            'targets': component_no_version.levels[0]['targets'],  'uuid': 'all'}
        with open("platformBW5-2.txt", "w") as fh:
            generator.generate(struct, fh, 'start')
        compare_files('../../test/resources/bw5/platformBW5-2.expected','platformBW5-2.txt')

    def test_override_path_only(self):
        options = copy.deepcopy(self.options)
        options.path='./var/'
        component_no_version = do_component_no_version()
        struct = {'level': {'level':'L1','rtv' : self.rtvs }, 'directory': '.', 'contents': component_no_version.content,
            'targets': component_no_version.levels[0]['targets'],  'uuid': 'all'}
        generator = tibco_bwGenerator(component_no_version, options)
        with open("platformBW5-3.txt", "w") as fh:
            generator.generate(struct, fh, 'start')
        compare_files('../../test/resources/bw5/platformBW5-3.expected','platformBW5-3.txt')




if __name__ == '__main__':
    unittest.main()
