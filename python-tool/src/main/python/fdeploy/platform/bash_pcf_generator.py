# -*- coding: utf-8 -*-
from __future__ import print_function

import fdeploy
import fdeploy.platform
import fdeploy.fdeployLoader
from fdeploy.platform import load_snippet, baseGenerator, basePcfGenerator
from fdeploy.platform import DEFAULT_TARGET_SPEC
from fdeploy.platform.bash_pcf_cups_generator import command_script_builder

REQUIRED_RTVS = ['NUM_INSTANCES',  'LOG_LEVEL','EAI_NUMBER']
REGISTERED_ACTIONS = ['clean', 'wipeout', 'start', 'stop', 'install', 'stage', 'ml', 'machine_list',
                      'restage',
                      'deploy', 'publish', 'unpublish', 'restart', 'report', 'ssh', 'inspect', 'ps', 'cloudenv','status','relink','bluegreen']
# remove staging from deploying
REGISTERED_DEPLOY_ACTIONS = ['clean', 'wipeout', 'start', 'stop', 
        'install', 'ml', 'machine_list', 'restage',
        'deploy', 'publish', 'unpublish', 'restart', 'report', 'ssh',
        'inspect', 'ps', 'cloudenv','status','relink','bluegreen']

TARGET_URL_SPEC=DEFAULT_TARGET_SPEC


class pcfDeployer(baseGenerator):
    def __init__(self, component, cli_options, stage_directory):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_pcf_generator.REGISTERED_DEPLOY_ACTIONS
        super(pcfDeployer, self).__init__(
            component, cli_options, stage_directory)
        fdeploy.LOGGER.debug(
            " < init bash-generator pcfDeployer platform for " + str(self.component))

    def deploying(self, files, verbose=False):
        if self.options.action != 'stage':
            super(pcfDeployer, self).deploying(files, verbose)

    def report(self,__reports):
        pass

class pcfGenerator(basePcfGenerator):

    files = []
    upload_script = load_snippet('pcf/upload_snippet.sh')
    authentication_script = load_snippet('pcf/pcf_authentication.sh')
    restage = load_snippet('pcf/restage_snippet.sh')
    bluegreen = load_snippet('pcf/bluegreen_upload_snippet.sh')
    restage_snippet = load_snippet('pcf/restage_snippet.sh')
    resolve_password = True

    def __init__(self, component, cli_options, stage_directory='.', target_spec = DEFAULT_TARGET_SPEC, resolve_password=True, alternate_pam=None):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_pcf_generator.REGISTERED_ACTIONS
        self.REQUIRED_RTVS =  fdeploy.platform.bash_pcf_generator.REQUIRED_RTVS
        self.resolve_password = resolve_password

        super(pcfGenerator, self).__init__(
            component, cli_options, stage_directory, target_spec, alternate_pam)
        fdeploy.LOGGER.debug(
            " < init platform: bash-generator, class: pcfGenerator  for " + str(self.component))




    """Generate scripts for the components on the targets in the descriptors."""
    def generate(self, struct, outfile, action='status', verbose=False):
        level = struct['level']
        # replace values in the snippets
        target_list = ''
        envvar_list = ''
        # pick the first Nexus resolved component artifact.
        nexusContent = self.get_first_nexus_component_in_content(self.needs_nexus_version(action))
        #
        # resolving the runtime variables (rtv), first global then components.
        #
        rtv_map={}
        target, target_list, rtv_map, envvar_list = self.rtv_mapping(
            action, rtv_map, level, struct['uuid'], struct['targets'],
            self.target_spec, self.REQUIRED_RTVS, nexusContent.version)
        fdeploy.LOGGER.debug("target => %s " % (target))

        envvar_list=self.component.as_envvars(rtv_map)

        fdeploy.LOGGER.debug( "-> struct     =%s" % (struct))
        fdeploy.LOGGER.debug( "-> rtv_map    =%s" % (rtv_map))
        fdeploy.LOGGER.debug( "-> envvar_list=%s" % (envvar_list))

        if 'original_id' in struct.keys():
            struct['uuid'] = struct['original_id']


        verboseOn = "set -xv"
        verboseOff = "set +xv"
        verboseFlag = verboseOff if verbose == False else verboseOn

        # NO WAIT FLAG
        nowaitflag = '' if self.options.no_wait == False else '#SKIP#'

        log_array = fdeploy.bash_array()
        fdeploy.LOGGER.debug("log_array=" + str(log_array))

        actions = []
        actions.append(action)

        fdeploy.LOGGER.info( "generating for %s individual actions" % (len(actions)) )
        for _action in actions:
            # only echo the env vars on certain actions.
            verboseOff if _action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING else verboseFlag
            # if action == 'stage':
            #     continue
            #
            print(self.data_wrapper_header % (
                self.options.command,
                _action,
                self.options.level,
                rtv_map['APP_NM'],
                log_array[0], log_array[1], log_array[2], log_array[3],
                struct['uuid'],
                struct['directory'],
                self.stage_directory,
                '20', #timeout
                nexusContent.version,
                nexusContent.saveArchiveName,
                nexusContent.artifactId,
                envvar_list
            ),file=outfile)
            print(self.authentication_script,file=outfile)
            if action not in ['start','stop','restart','status'] and len(self.component.services) > 0:
                command_script_builder(outfile, envvar_list, self.component.services, _action, False)
            data = []
            fdeploy.LOGGER.info("generating for [%s] " % ( _action ))
            if _action == 'install' or _action == 'stage':
                data.append(self.status_snippet)
                data.append(self.upload_snippet)
            elif _action == 'deploy':
                data.append(self.status_snippet)
                #data.append(self.stop_snippet)
                data.append(self.upload_snippet)
                data.append(self.start_snippet)
            elif _action == 'bluegreen':
                data.append(self.status_snippet)
                #data.append(self.stop_snippet)
                data.append(self.bluegreen)
                data.append(self.start_snippet)
            elif _action == 'restart':
                data.append(self.status_snippet)
                data.append(self.restart_snippet)
                if len(self.component.services) > 0:
                    data.append(self.restage)
            elif _action == 'restage':
                data.append(self.status_snippet)
                data.append(self.restage_snippet)
            elif _action == 'stop':
                data.append(self.status_snippet)
                data.append(self.stop_snippet)
            elif _action == 'start':
                data.append(self.status_snippet)
                data.append(self.start_snippet)
            elif _action == 'run':
                pass
            elif _action == 'relink':
                pass
            elif _action == 'status':
                data.append(self.status_snippet)
            elif _action == 'wipeout' or _action == 'clean':
                data.append(self.status_snippet)
                data.append(self.delete_snippet)
            elif _action == 'ssh':
                pass
            elif _action == 'inspect':
                pass
            elif _action == 'ps':
                pass
            elif _action == 'cloudenv':
                pass

            # generate all the lines
            for script_line in data:
                print( script_line,file=outfile)
            # if test no footer needed the test is selfcontanted and
            # does not need script streaming
            if _action not in ['test', 'inspect', 'ssh', 'ps','cloudenv']:
                print (self.data_wrapper_footer, file=outfile)
        return rtv_map
