
import copy
# , load_file_as_string
import os

import pytest
from fnexus.manifestResolver import manifestResolver

import fdeploy
import fdeploy.platform.bash_generator
from fdeploy import Options
from fdeploy.MockHttpServer import start_mock_server
from fdeploy.UtilsForTesting import compare_files
from fdeploy.content.fdeployComponent import fdeployComponent
from fdeploy.fdeployLoader import fdeployLoader
from fdeploy.platform import load_file
from fdeploy.platform.bash_pcf_generator import pcfGenerator, pcfDeployer
from fdeploy.test_PAM import pamAuthRequestHandler, setup_pam_url


def comp1create():
    return fdeployComponent({

        "type": "pcf",
        "rtv": [{
            "name": "APP_NM",
            "value": "trip-proxy"
        },
            {
            "name": "EAI_NUMBER",
            "value": "3534546"
        },
            {
            "name": "NUM_INSTANCES",
            "value": "1"
        },
            {
            "name": "SERVICE_INSTANCES",
            "value": "1"
        },
            {
            "name": "APPD_NM",
            "value": "sefs-trip-proxy"
        },
            {
            "name": "LOG_LEVEL",
            "value": "1"
        },
            {
            "name": "SPRING_PROFILES_ACTIVE",
            "value": "L1"
        },
            {
            "name": "PAM_ID",
            "value": "18414"
        },
            {
            "name": "DISCOVERY_SERVICE",
            "value": "trip-discovery-service"
        }
        ],
        "levels": [{
            "rtv": [],
            "level": "L1",
            "targets": [
                "development@api.sys.wtcdev2.paas.fedex.com"
            ]
        }, {
            "rtv": [],
            "level": "L2",
            "targets": [
                "development@api.sys.wtcdev2.paas.fedex.com"
            ]
        },
            {"rtv": [],
             "level": "L3",
             "targets": [
                "release@api.sys.wtcdev2.paas.fedex.com"
            ]
        },
            {
            "rtv": [],
            "level": "PROD",
            "targets": [
                "production@api.sys.edcbo1.paas.fedex.com"
            ]
        }],
        "content": [{
            "gav": "com.fedex.sefs.core.trip:trip-proxy:1.0.3:jar",
            "saveArchiveName": "pmi-web.jar"
        }],
        "id": "trip-proxy",
        "name_format": "PCF_SEFS_TRIP_PROXY_%0d",
        "platform": "bash_generator"

    }, 'inline.json')

def create2comp():
    return fdeployComponent({

        "type": "pcf",
        "rtv": [{
            "name": "APP_NM",
            "value": "trip-proxy"
        },
            {
            "name": "EAI_NUMBER",
            "value": "3534546"
        },
            {
            "name": "MANIFEST_FILE",
            "value": "../../test/resources/test_manifest_overwrite.yml"
        },
            {
            "name": "NUM_INSTANCES",
            "value": "1"
        },
            {
            "name": "SERVICE_INSTANCES",
            "value": "1"
        },
            {
            "name": "APPD_NM",
            "value": "sefs-trip-proxy"
        },
            {
            "name": "LOG_LEVEL",
            "value": "1"
        },
            {
            "name": "SPRING_PROFILES_ACTIVE",
            "value": "L1"
        },
            {
            "name": "PAM_ID",
            "value": "18414"
        },
            {
            "name": "DISCOVERY_SERVICE",
            "value": "trip-discovery-service"
        }
        ],
        "levels": [{
            "rtv": [],
            "level": "L1",
            "targets": [
                "development@api.sys.wtcdev2.paas.fedex.com"
            ]
        }, {
            "rtv": [],
            "level": "L2",
            "targets": [
                "development@api.sys.wtcdev2.paas.fedex.com"
            ]
        },
            {"rtv": [],
             "level": "L3",
             "targets": [
                "release@api.sys.wtcdev2.paas.fedex.com"
            ]
        },
            {
            "rtv": [],
            "level": "PROD",
            "targets": [
                "production@api.sys.edcbo1.paas.fedex.com"
            ]
        }],
        "content": [{
            "gav": "com.fedex.sefs.core.trip:trip-proxy:1.0.3:jar",
            "saveArchiveName": "pmi-web.jar"
        }],
        "id": "trip-proxy",
        "name_format": "PCF_SEFS_TRIP_PROXY_%0d",
        "platform": "bash_generator"

    }, 'inline-with-overwrite.json')


class TestPlaformPcfGenerator(object):

    options = Options(command='deploy', ignore_manifest_errors = False, action='stage', no_download=False, dry_run=False, identity='.', output='test.out',
                               X=True, id=None, path=".", no_wait=False, defaults={'nexusURL': '../../test/resources/local'}, level='L1', user='sefs', parallel=False)
    options.defaults['nexusURL'] = '%s/local' % (  os.path.abspath("%s/../../test/resources" % (os.getcwd())) )
    os.environ['level'] = 'L1'
    fdeploy.load_rc('../../test/resources/pcf/.fdeployrc',options)

    rtvs = [
        {'name': 'APP_REF', 'value': '${APPMANAGE_PLAN}.backupt'},
        {'name': 'LEVEL', 'value': 'L1'}]

    @classmethod
    def setup_class(module):
        #os.environ['NO_PROXY']='127.0.0.1'
        os.environ['MOCK_USERNAME'] = 'FXS_app3534546'
        port = start_mock_server(pamAuthRequestHandler)
        fdeploy.pam.PAM_URL= setup_pam_url(port)

    @classmethod
    def teardown_class(module):
        #print "stage=", os.stat('platform-pcf-stage.txt')
        pass

    def test_generate_status(self):
        options = copy.deepcopy(self.options)
        component = comp1create()
        generator = pcfGenerator(component, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        struct = {'level': component.levels[0], 'rtv': self.rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-status.txt", "w") as fh:
            generator.generate(struct, fh, 'status')
        compare_files('../../test/resources/pcf/platform-pcf-status.txt','platform-pcf-status.txt')

    def test_generate_restart(self):
        options = copy.deepcopy(self.options)
        component = comp1create()
        generator = pcfGenerator(component, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        struct = {'level': component.levels[0], 'rtv': self.rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-restart.txt", "w") as fh:
            generator.generate(struct, fh, 'restart')
        compare_files('../../test/resources/pcf/platform-pcf-restart.txt','platform-pcf-restart.txt')

    def test_generate_restage(self):
        options = copy.deepcopy(self.options)
        component = comp1create()
        generator = pcfGenerator(component, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        struct = {'level': component.levels[0], 'rtv': self.rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-restage.txt", "w") as fh:
            generator.generate(struct, fh, 'restage')
        compare_files('../../test/resources/pcf/platform-pcf-restage.txt','platform-pcf-restage.txt')

    def test_generate_deploy(self):
        options = Options(command='deploy', action='deploy', identity='.',
                               X=True, id=None, path=".", no_wait=False, level='L3', user='sefs')
        component = comp1create()
        generator = pcfGenerator(component, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        struct = {'level': component.levels[2], 'rtv': self.rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-deploy.txt", "w") as fh:
            generator.generate(struct, fh, 'deploy')
        compare_files('../../test/resources/pcf/platform-pcf-deploy.txt','platform-pcf-deploy.txt')

    def test_generate_bluegreen(self):
        options = Options(command='deploy', action='bluegreen', identity='.',
                               X=True, id=None, path=".", no_wait=False, level='L3', user='sefs')
        component = comp1create()
        generator = pcfGenerator(component, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        struct = {'level': component.levels[2], 'rtv': self.rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-bluegreen.txt", "w") as fh:
            generator.generate(struct, fh, 'bluegreen')
        compare_files('../../test/resources/pcf/platform-pcf-bluegreen.txt','platform-pcf-bluegreen.txt')

    def test_generate_stage(self):
        options = Options(command='deploy', action='stage', identity='.',
                               X=True, id=None, path=".", no_wait=False, level='L1', user='sefs', parallel=False)
        component = comp1create()
        generator = pcfGenerator(component, options)
        # no deployment
        with pytest.raises(Exception):
            pcfDeployer(component, options, '.')
        targets = ["development@api.sys.wtcdev2.paas.fedex.com"]
        struct = {'level': component.levels[0], 'rtv': self.rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-stage.txt", "w") as fh:
            generator.generate(struct, fh, 'stage')
        compare_files('../../test/resources/pcf/platform-pcf-stage.txt', 'platform-pcf-stage.txt', use_assert=False,
                      filter=False)

    def test_generate_install(self):
        options = Options(command='deploy', action='install', identity='.',
                               X=True, id=None, path=".", no_wait=False, level='L1', user='sefs')
        component = comp1create()
        generator = pcfGenerator(component, options)
        targets = ["development@api.sys.wtcdev2.paas.fedex.com"]
        assert 2 == len(self.rtvs)
        struct = {'level': component.levels[0], 'rtv': self.rtvs, 'directory': '.',
                  'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-install.txt", "w") as fh:
            rtv_map = generator.generate(struct, fh, 'install')
        assert rtv_map['SPACE'] == 'development'
        compare_files('../../test/resources/pcf/platform-pcf-install.txt','platform-pcf-install.txt')


    def test_manifest_overwrite(self):
        comp2 = create2comp()
        options = copy.deepcopy(self.options)
        generator = pcfGenerator(comp2, options)
        targets = ["release@api.sys.wtcdev2.paas.fedex.com"]
        struct = {'level': comp2.levels[0], 'rtv': self.rtvs, 'directory': '.',
                  'contents': comp2.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-overwrite-manifest.txt", "w") as fh:
            generator.generate(struct, fh)
        compare_files('../../test/resources/pcf/platform-pcf-overwrite.txt','platform-pcf-overwrite-manifest.txt')


    def test_rtvs_for_pcf(self):
        comp2 = create2comp()
        options = copy.deepcopy(self.options)
        rtvs=[]
        assert 10 == len(comp2.rtv)
        rtv_map = []
        generator = pcfGenerator(comp2, options)
        targets = ["development@api.sys.wtcdev2.paas.fedex.com"]
        struct = {'level': comp2.levels[0], 'rtv': rtvs, 'directory': '.',
                  'contents': comp2.content, 'targets': targets,  'uuid': 'all'}
        with open("platform-pcf-overwrite-manifest.txt", "w") as fh:
            rtv_map = generator.generate(struct, fh)
        assert rtv_map['APP_NM'] == 'L1-trip-proxy'
        assert rtv_map['SPACE'] == 'development'
        rtv_map, envvar = comp2.get_env_vars(
            options, comp2.levels[0], generator.REQUIRED_RTVS, '1.4', init=rtv_map)
        assert 21 == len(rtv_map)
        shadow = {'SPRING_PROFILES_ACTIVE': 'L1', 'NUM_INSTANCES': '1',
                  'MANIFEST_FILE': '../../test/resources/test_manifest_overwrite.yml','API_URL': 'api.sys.wtcdev2.paas.fedex.com',
                  'LEVEL': {'rtv': [], 'targets': ['development@api.sys.wtcdev2.paas.fedex.com'], 'level': 'L1'},
                  'APP_NM': 'L1-trip-proxy',
                  'DISCOVERY_SERVICE': 'trip-discovery-service', 'PAM_ID': '18414',
                  'APPD_NM': 'sefs-trip-proxy', 'VERSION': '1.4',
                  'GROUPID': 'com.fedex.sefs.core.trip', 'LOG_LEVEL': '1',
                  'ARTIFACTID': 'trip-proxy', 'SERVICE_INSTANCES': '1',
                  'SPACE' : 'development', 'DOMAIN' : 'api.sys.wtcdev2.paas.fedex.com',
                  'DOMAIN' : 'app.wtcdev2.paas.fedex.com', 'EAI_NUMBER': '3534546'}

        for k in shadow.keys():
            assert rtv_map[k] == shadow[k], k

        a = [env for env in comp2.as_envvars().split('\n') if 'PASSWORD' not in env and 'CF_HOME' not in env]

        envvar = "\n".join(a)



        assert envvar == '''export API_URL="api.sys.wtcdev2.paas.fedex.com"
export APPD_NM="sefs-trip-proxy"
export APP_HOST="L1-all"
export APP_NM="L1-trip-proxy"
export ARTIFACTID="trip-proxy"
export DISCOVERY_SERVICE="trip-discovery-service"
export DOMAIN="app.wtcdev2.paas.fedex.com"
export EAI_NUMBER="3534546"
export GROUPID="com.fedex.sefs.core.trip"
export LOG_LEVEL="1"
export MANIFEST_FILE="../../test/resources/test_manifest_overwrite.yml"
export NUM_INSTANCES="1"
export PAM_ID="18414"
export PCF_USER="FXS_app3534546"
export SERVICE_INSTANCES="1"
export SPACE="development"
export SPRING_PROFILES_ACTIVE="L1"
export VERSION="1.4"
'''


    def test_read_descriptor_component(self):
        options = copy.deepcopy(self.options)
        fdl = fdeployLoader(options)
        fdl.resolver.applyOptions(options.defaults)
        fdl.resolver.__manifest_resolver__ = manifestResolver('../../test/resources/pcf-config','L2')
        fdl.readDescriptors(
            ['../../test/resources/pcf-config/itinerary-discovery-cups2.json'])
        assert 1 ==  len(fdl.components)
        # validating structure
        comp = fdl.components[0]

        fdl.resolver.resolveGavs(comp, manifestRequired=True)
        assert 'itinerary-definition-service' ==  comp.id
        assert 9 ==  len(comp.rtv)
        generator = pcfGenerator(comp, options)
        #fdl.generate_level('L1')
        struct = {'level': comp.levels[0], 'rtv': self.rtvs, 'directory': '.',
                  'contents': comp.content, 'targets': ["development@api.sys.wtcdev2.paas.fedex.com"],  'uuid': 'all'}
        generator.get_first_nexus_component_in_content()
        with open("platform-pcf-stage-2.txt", "w") as fh:
            generator.generate(struct, fh, 'deploy')
        load_file("platform-pcf-stage-2.txt",True, True)
