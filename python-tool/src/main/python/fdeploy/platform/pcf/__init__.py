# -*- coding: utf-8 -*-
import os
import time
import re
import json
from fdeploy.platform import load_snippet
from fdeploy.pam import pamAuth
import fdeploy
from fdeploy.security import inline_singleton, is_encapsulated

CUPS_ACTION = ('uups', 'cups')

def build_command(commandformat, action=CUPS_ACTION):
    os.environ['_level']=(os.path.expandvars('${level}')).lower()
    commandformat = "%s || %s" % (commandformat, commandformat)
    return commandformat % action

def build_args_list(commandargs, action=CUPS_ACTION):
    newlist = []
    for n in commandargs:
        if n == '%s':
            n = action[0]
        newlist.append(n)
    newlist.append('||')
    for n in commandargs:
        if n == '%s':
            n = action[1]
        newlist.append(n)
    return newlist

def build_delete_service_command(name, format='%s'):
    name = format % (name)
    return os.path.expandvars('cf delete-service %s -f' % (name))

def build_delete_args(name, format='%s'):
    name = os.path.expandvars(format % (name))
    return ['cf', 'delete-service', name, '-f']

def build_operation(commandformat, action='-h'):
    commandformat = "cf %s" % (commandformat)
    return commandformat % action

def build_args_operation(base,args):
    newlist = []
    os.environ['_level']=(os.path.expandvars('${level}')).lower()
    while (len(base) > 0):
        b = base.pop(0)
        if '%s' in b and not b == '%%s':
            b= b % (args.pop(0))
            newlist.append(os.path.expandvars(b))
        else:
            if b == '%%s':
                b = '%s'
            newlist.append(os.path.expandvars(b))
    return newlist

def task_id_generator(_class, name=None):
    tm = time.time()
    task_id = "%s-%s" % (_class.__class__.__name__, tm)
    if name:
        task_id = "%s-%s" % (_class.__class__.__name__, name)
    task_id = re.sub(r'\.', '_', task_id)
    return task_id


class taskClass(object):

    task_id = None
    name = None

    def __init__(self, name=None):
        self.name = name
        self.task_id = task_id_generator(name)

class runStatusClass(taskClass):

    pcfStatusSnippet = load_snippet('pcf/status_snippet.tpl')

    def __init__(self, name, appnm):
        super(runStatusClass, self).__init__(name)
        self.appnm = appnm

    def toCommand(self, action='login'):
        return self.pcfStatusSnippet % (self.appnm, self.appnm, self.appnm)


class authPcfClass(taskClass):

    pcfAuthSnippet = load_snippet('pcf/pcf_authentication.tpl')

    pcf_auth_snippet=load_snippet('pcf/pcf_authentication.tm')

    def __init__(self, name, pam_id, space, eai_number, domain):
        super(authPcfClass, self).__init__(name)
        self.space = space
        self.eai_number = eai_number
        self.domain = domain
        self.pam_id = pam_id

    def toCommand(self, action=None):
        user, pwd = ('', '')
        if action is None:
            user, pwd = pamAuth().get_passwd(self.pam_id)
        fdeploy.LOGGER.info("Pam User: %s / %s" % (user, action))
        return self.pcfAuthSnippet % (self.domain, user, self.domain,
                                      user, pwd, self.space, self.eai_number, self.space, self.space, self.eai_number)


class appsListClass(taskClass):

    pcf_list_apps = load_snippet('pcf/pcf_list_apps.tpl')

    def __init__(self, name):
        super(appsListClass, self).__init__(name)

    def toCommand(self, action=None):
        return self.pcf_list_apps


class envClass(taskClass):

    def __init__(self, name, content):
        super(envClass, self).__init__(name)
        self.service_name = content['service_name']
        self.value = content['value']
    def cmd(self, action='set-env'):
        return os.path.expandvars('cf %s ${APP_NM} \"%s\" \"%s\"' % (action, self.service_name, self.value))
    def toCommand(self, action):
        if 'delete' == action:
            return self.cmd('unset-env')
        return self.cmd()
    def toProgramArgs(self, action):
        base = ['cf', 'set-env', '${APP_NM}', '%s', '%s']
        if action == 'delete':
            base = ['cf', 'unset-env', '${APP_NM}', '%s']
            return build_args_operation(base,[self.service_name])
        return build_args_operation(base,[self.service_name, self.value])


