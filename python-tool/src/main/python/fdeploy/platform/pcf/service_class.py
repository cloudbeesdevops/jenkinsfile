

from fdeploy.platform.pcf import taskClass, build_args_operation
import os
import json

class serviceClass(taskClass):

    def __init__(self, name, service_name, marketplace, domain):
        super(serviceClass, self).__init__(name)
        self.service_name = service_name
        self.marketplace = marketplace
        self.domain = domain
    def __args_array(self):
        return [self.marketplace ,self.domain, self.service_name]
    def deleteCommand(self,action='delete'):
        return os.path.expandvars("cf %s-service %s \"%s\" \"%s\"" % (action,self.marketplace ,self.domain, self.service_name))
    def creupdCommand(self):
        return  os.path.expandvars("cf create-service %s \"%s\" \"%s\"" % (self.marketplace ,self.domain, self.service_name))
    def toCommand(self, action):
        if 'delete' == action:
            return self.deleteCommand()
        return self.creupdCommand()
    def toProgramArgs(self, action):
        base = ['cf', 'create-service', '%s', '%s', '%s']
        if action == 'delete':
            base[1]='delete-service'
        args = self.__args_array()
        return build_args_operation(base,args)

class serviceAppdClass(serviceClass):

    def __init__(self, name, content):
        super(serviceAppdClass, self).__init__(
            name, content['service_name'], 'appdynamics', content['domain'])

class serviceSplunkClass(serviceClass):

    def __init__(self, name, content):
        super(serviceSplunkClass, self).__init__(
            name, content['service_name'], 'splunk', content['domain'])

class serviceAutoScalerClass(serviceClass):

    def __init__(self, name, content):
        super(serviceAutoScalerClass, self).__init__(
            name, content['service_name'], 'app-autoscaler', content['scaler-name'])
        content['instance_limits']['max']
        content['instance_limits']['min']
        self.request = { 'enabled' : content['enabled'], 'instance_limits' : content['instance_limits']}

    def __args_array(self):
        return ['app-autoscaler', self.domain]
    def deleteCommand(self,action='delete'):
        return os.path.expandvars("cf %s-service %s \"%s\"" % (action,self.marketplace ,self.domain))
    def creupdCommand(self):
        return  os.path.expandvars("cf create-service %s standard \"%s\"" % (self.marketplace ,self.domain))
    def bindCommand(self):
        return  os.path.expandvars("cf bind-service \"%s\" \"%s\" '%s' && cf restage \"%s\"" % (self.name, self.domain, json.dumps(self.request), self.name))
    def unbindCommand(self):
        return  os.path.expandvars("cf unbind-service \"%s\" \"%s\" && cf restage \"%s\"" % (self.name, self.domain, self.name))
    def toCommand(self, action):
        if 'delete' == action:
            return self.deleteCommand()
        elif 'unbind' == action:
            return self.unbindCommand()
        elif 'bind' == action:
            return self.bindCommand()
        return self.creupdCommand()
    
    def toProgramArgs(self, action='create'):
        if action == '':
            action = 'create'
        base = ['cf', '%s-service', '%s', 'standard', '%s']
        base[1]= "%s-service" % (action)
        print ',',action,',', base
        if action in ['bind', 'unbind','delete']:
            base.remove('standard')
        args = self.__args_array()
        return build_args_operation(base,args) 