
from fdeploy.platform.pcf import taskClass

class commandClass(taskClass):

    def __init__(self, name, content):
        super(commandClass, self).__init__(name)
        self.service_name = content['service_name']
        self.command = content['command']
    def toCommand(self, action):
        return self.command
