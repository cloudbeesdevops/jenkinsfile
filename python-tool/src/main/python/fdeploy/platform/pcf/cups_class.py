
from fdeploy.platform.pcf import taskClass, build_args_operation, is_encapsulated, build_command,\
    inline_singleton, build_delete_service_command, build_delete_args, build_command, build_args_list
import json
import fdeploy
import os


class cupsClass(taskClass):
    
    def __init__(self, name, content):
        super(cupsClass, self).__init__(name)
        # initialize cryptology package detection
        try:
            self.service_name = content['service_name']
            self.type = content['type']
        except KeyError as e:
            print("%s %s" % (content, e))
            raise e
    
    def bindCommand(self):
        return  os.path.expandvars("cf bind-service \"%s\" \"%s\"  && cf restage \"%s\"" % (self.name, self.service_name, self.name))
    

class cupsCredentialsClass(cupsClass):

    def __init__(self, name, content):
        super(cupsCredentialsClass, self).__init__(name, content)
        # initialize cryptology package detection
        self.user = content['username']
        self.env_map = {}
        if 'env_map' in content:
            self.env_map = content['env_map']
            for m in self.env_map.keys():
                try:
                    if is_encapsulated(self.env_map[m]):
                        self.env_map[m] = inline_singleton().decrypt(self.env_map[m])
                except:
                    fdeploy.LOGGER.error("failed to decrypt %s value" % (m))
        if 'password' in content:
            self.password = content['password']
            try:
                if not inline_singleton() is None:
                    self.password = inline_singleton().decrypt(content['password'])
            except Exception as e:
                fdeploy.LOGGER.error("failed to decypt: %s" % (e))
        else:
            self.password = None
        self.role = content['role'] if 'role' in content else None
        self.vault_token = content['vault_token'] if 'vault_token' in content else None
    def __args_array(self):
        map = {'UserName': self.user, 'Password': self.password, 'Role': self.role}
        if len(self.env_map.keys()) > 0:
            map.update(self.env_map)
        return [self.service_name, json.dumps(map)]
    def toCommand(self, action):
        if action == 'delete':
            return build_delete_service_command(self.service_name)
        map = {'UserName': self.user, 'Password': self.password, 'Role': self.role}
        if len(self.env_map.keys()) > 0:
            map.update(self.env_map)
        commandformat = os.path.expandvars(
            "cf %%s %s -p '%s'" % (self.service_name, json.dumps(map)))
        return build_command(commandformat)
    def toProgramArgs(self, action):
        base = ['cf', '%%s', '%s', '-p', '%s']
        if action == 'delete':
            return build_delete_args(self.service_name)
        args = self.__args_array()
        return build_args_list(build_args_operation(base,args))


class cupsJmsProviderClass(cupsCredentialsClass):

    def __init__(self, name, content):
        super(cupsJmsProviderClass, self).__init__(name, content)
        self.provider_url = content['provider_url']
        self.context_factory = content['context_factory']
        self.connection_factory = content['connection_factory']
    def __args_array(self):
        return [self.service_name, json.dumps(
            {'provider-url': self.provider_url, 'context-factory': self.context_factory,
             'username': self.user, 'password': self.password, 'connection-factory': self.connection_factory}
        )]
    def toCommand(self, action):
        if action == 'delete':
            return build_delete_service_command(self.service_name,'${level}_%s')
        return build_command(os.path.expandvars(
            "cf %%s ${level}_%s -p '%s'" % (self.service_name, json.dumps(
                {'provider-url': self.provider_url, 'context-factory': self.context_factory,
                 'username': self.user, 'password': self.password, 'connection-factory': self.connection_factory}
            ))))
    def toProgramArgs(self, action):
        if action == 'delete':
            return build_delete_args(self.service_name,'${level}_%s')
        base = ['cf', '%%s', '${level}_%s', '-p', '%s']
        return build_args_list(build_args_operation(base,self.__args_array()))


class cupsGitConfigHttpServerClass(cupsClass):

    user_key = 'user'
    password_key = 'password'
    git_key = 'git-url'

    def __init__(self, name, content):
        super(cupsGitConfigHttpServerClass, self).__init__(name, content)
        self.git_url = content['git-url']
        self.git_key = content['git-key']
        self.username = content['username']
        self.user_key = content['user-key']
        self.password_key = content['password-key']
        try:
            if is_encapsulated(content['password']):
                self.password = inline_singleton().decrypt(content['password'])
        except Exception as e:
            fdeploy.LOGGER.error("failed to decypt: %s" % (e))


    def __args_array(self):
        return [self.service_name, json.dumps({self.git_key: self.git_url, self.user_key : self.username, self.password_key : self.password})]

    def toCommand(self, action):
        if action == 'delete':
            return build_delete_service_command(self.service_name)
        base =  build_command(os.path.expandvars("cf %%s %s -p '%s'" % (self.service_name,json.dumps({self.git_key: self.git_url, self.user_key : self.username, self.password_key : self.password}))))
        return "%s; %s" % (base, self.bindCommand())

    def toProgramArgs(self, action):
        if action == 'delete':
            return build_delete_args(self.service_name)
        base = ['cf', '%%s', '%s', '-p', '%s']
        return build_args_list(build_args_operation(base,self.__args_array()))

    def bindCommand(self):
        return  os.path.expandvars("cf bind-service \"%s\" \"%s\"  && cf restage \"%s\"" % (self.name, self.service_name, self.name))


class cupsGitConfigServerClass(cupsClass):

    def __init__(self, name, content):
        super(cupsGitConfigServerClass, self).__init__(name, content)
        self.git_url = content['git-url']
        self.access_token = content['access-token']

    def __args_array(self):
        return [self.service_name,json.dumps({'git-url': self.git_url, 'access-token': self.access_token})]

    def toCommand(self, action):
        if action == 'delete':
            return build_delete_service_command(self.service_name)

        return  build_command(os.path.expandvars("cf %%s %s -p '%s'" % (self.service_name,json.dumps({'git-url': self.git_url, 'access-token': self.access_token}))))

    def toProgramArgs(self, action):
        if action == 'delete':
            return build_delete_args(self.service_name)
        base = ['cf', '%%s', '%s', '-p', '%s']
        return build_args_list(build_args_operation(base,self.__args_array()))


class cupsConfigVariableClass(cupsClass):

    def __init__(self, name, content):
        super(cupsConfigVariableClass, self).__init__(name, content)
        self.value = content['value']

    def __args_array(self):
        return [self.service_name,json.dumps({'domain-name': self.value})]

    def toCommand(self, action):
        if action == 'delete':
            return build_delete_service_command(self.service_name)
        return build_command(os.path.expandvars("cf %%s %s -p '%s'" % (self.service_name,json.dumps({'domain-name': self.value}))))

    def toProgramArgs(self, action):
        if action == 'delete':
            return build_delete_args(self.service_name)
        base = ['cf', '%%s', '%s', '-p', '%s']
        return build_args_list(build_args_operation(base,self.__args_array()))


class cupsUriClass(cupsClass):

    def __init__(self, name, content):
        super(cupsUriClass, self).__init__(name, content)
        self.uri = content['uri']
    def uri_level_case(self,uri):
        level = os.environ['level']
        os.environ['level']=level.lower()
        _uri = os.path.expandvars(uri)
        os.environ['level']=level
        return _uri
    def __args_array(self):
        return [self.service_name,  json.dumps({'uri': self.uri_level_case(self.uri)})]
    def toCommand(self, action):
        if action == 'delete':
            return build_delete_service_command(self.service_name)
        return build_command(os.path.expandvars("cf %%s %s -p '%s'" % (self.service_name,  json.dumps({'uri': self.uri_level_case(self.uri)}))))
    def toProgramArgs(self, action):
        if action == 'delete':
            return build_delete_args(self.service_name)
        base = ['cf', '%%s', '%s', '-p', '%s']
        return build_args_list(build_args_operation(base,self.__args_array()))

class cupsScaleClass(cupsClass):

    def __init__(self, name, content):
        super(cupsScaleClass, self).__init__(name, content)
        self.service_name = content['service_name']
        self.instances = content['instances']
    def toCommand(self, action):
        return os.path.expandvars("cf scale ${level}-%s -i '%s'" % (self.service_name, self.instances))
    def toProgramArgs(self, action):
        base = ['cf', 'scale', '${level}=%s', '-i', '%s']
        return build_args_operation(base,[self.service_name, self.instances])

class cupsDatabaseServiceClass(cupsCredentialsClass):

    def __init__(self, name, content):
        super(cupsDatabaseServiceClass, self).__init__(name, content)
        self.service_name = content['service_name']
        self.url = content['url']
        self.driverClass = content['driver_class_name']

    def __args_array(self):
        return [self.service_name, json.dumps({
            'url': self.url, 'driver-class-name': self.driverClass, 'username': self.user, 'password': self.password
        })]

    def toCommand(self, action):
        if action == 'delete':
            return build_delete_service_command(self.service_name)
        return build_command(os.path.expandvars("cf %%s ${level}-%s -p '%s'" % (self.service_name, json.dumps({
            'url': self.url, 'driver-class-name': self.driverClass, 'username': self.user, 'password': self.password
        }))))

    def toProgramArgs(self, action):
        if action == 'delete':
            return build_delete_args(self.service_name, '${level}-%s')
        base = ['cf', '%%s', '${level}-%s', '-p', '%s']
        return build_args_list(build_args_operation(base,self.__args_array()))
