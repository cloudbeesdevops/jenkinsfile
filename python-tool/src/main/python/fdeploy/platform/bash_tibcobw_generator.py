# -*- coding: utf-8 -*-
from __future__ import print_function

import re
import datetime
import fdeploy
import fdeploy.platform
from fdeploy.platform import load_snippet, baseGenerator, URL_URI_TARGET_SPEC, _needs_nexus_resolving

REQUIRED_RTVS = ['TRA_HOME', 'JMX_PORT', 'PROCESS_NAME',
    'ADMIN_TARGET_URL', 'EAINUMBER', 'APPMANAGE_CREDENTIALS']
REGISTERED_ACTIONS = ['clean', 'wipeout', 'start', 'stop', 'install', 'stage', 'uninstall',
        'deploy', 'publish', 'unpublish', 'restart', 'ssh', 'inspect', 'ps', 'cloudenv', 'relink']

class tibco_bwGenerator(baseGenerator):

    files = []

    data_wrapper_header = load_snippet('tibco_bw/appmanage_wrapper_header.sh')
    data_wrapper_footer = load_snippet('tibco_bw/appmanage_wrapper_footer.sh')

    # TIBCO
    remote_appmanage_stage_snippet_begin = load_snippet(
        'tibco_bw/remote_appmanage_stage_snippet_begin.sh')
    remote_appmanage_stage_snippet_end = load_snippet(
        'tibco_bw/remote_appmanage_stage_snippet_end.sh')
    remote_create_scripts_snippet = load_snippet(
        'tibco_bw/remote_create_scripts_snippet.sh')
    remote_appmanage_delete_snippet = load_snippet(
        'tibco_bw/remote_appmanage_delete_snippet.sh')
    remote_appmanage_deploy_snippet = load_snippet(
        'tibco_bw/remote_appmanage_deploy_snippet.sh')
    remote_appmanage_install_snippet = load_snippet(
        'tibco_bw/remote_appmanage_install_snippet.sh')
    remote_appmanage_undeploy_snippet = load_snippet(
        'tibco_bw/remote_appmanage_undeploy_snippet.sh')
    remote_appmanage_start_snippet = load_snippet(
        'tibco_bw/remote_appmanage_start_snippet.sh')
    remote_appmanage_stop_snippet = load_snippet(
        'tibco_bw/remote_appmanage_stop_snippet.sh')

    def __init__(self, component, cli_options, stage_directory='.', target_spec=URL_URI_TARGET_SPEC):
        self.REGISTERED_ACTIONS = fdeploy.platform.bash_tibcobw_generator.REGISTERED_ACTIONS
        self.REQUIRED_RTVS = fdeploy.platform.bash_tibcobw_generator.REQUIRED_RTVS
        super(tibco_bwGenerator, self).__init__(
            component, cli_options, stage_directory, target_spec)
        self.ssh_options = '-oConnectTimeout=20 -oStrictHostKeyChecking=no \
-oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null'

        fdeploy.LOGGER.debug(
            " < init platform: bash-generator, class: tibco_bwGenerator  for " + str(self.component.prettyprint()))

    def list_machines(self, struct, target_list, search=None):
        # grab the ADMIN URL as a target, not the normal targets
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        for target in __targets:
            _target = target
            if ":" in target:
                _target = target.split(":")[0]
            if self.options.user is not None:
                _target = re.sub(r'^(\S+)\@', self.options.user + str('@'), _target)
            if not _target in target_list:
                target_list.append(_target)

    def needs_nexus_version(self,action):
        retValue = True
        if 'ssh' == action or 'ps' == action or 'unpublish' == action :
            retValue = False
        return retValue


    """Generate scripts for the components on the targets in the descriptors."""
    def generate(self, struct, outfile, action='status', target_spec=URL_URI_TARGET_SPEC, verbose=False):
        assert target_spec is not None
        levelStruct = struct['level']
        # replace values in the snippets
        target_list = ''
        tlm_list = ''
        upload_list = ''
        envvar_list = ''
        app_user=None
        # pick the first Nexus resolved component artifact.
        nexusContent = self.get_first_nexus_component_in_content(_needs_nexus_resolving(action,False))

        if action == 'clean' and not self.component.versions_to_persist is None and len(self.component.versions_to_persist) > 1:
            self.component.rtvMapping['CANDIDATES']=','.join(self.component.versions_to_persist)
            _action = 'clean_with_manifest'

        #
        # resolving the runtime variables (rtv), first global then components.
        #
        rtv_map={}
        rtv_map,envvar_list=self.component.get_env_vars(self.options,levelStruct['level'], self.REQUIRED_RTVS, nexusContent.version)
        tlm_list=[rtv_map['ADMIN_TARGET_URL']]
        fdeploy.LOGGER.debug("hosts %s# : %s"  % (len(tlm_list), tlm_list))
        target, target_list = self.get_target_list(tlm_list, target_spec)
        base_dir = rtv_map['ADMIN_TARGET_URL'].split(":")[1]
        #
        # generate the upload list based on the contents specified
        #
        upload_list = self.get_upload_list(struct, nexusContent.version)

        #
        # applying execute identity if applicable
        #
        pki_identiy = ' ' if self.options.identity is None or self.options.identity == '' else '-i ' + \
            str(self.options.identity)
        verboseOff = "set +xv"
        verboseFlag = ''
        # if verbose == False else verboseOn

        # NO WAIT FLAG
        nowaitflag = '' if self.options.no_wait == False else '#SKIP#'

        # only echo the env vars on certain actions.
        echoEnvVars = verboseOff if action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING else verboseFlag

        log_array = fdeploy.bash_array()
        fdeploy.LOGGER.trace("log_array=" + str(log_array))
        fdeploy.LOGGER.trace("machine list: %s" % (target_list))

        _action = action
        fdeploy.LOGGER.info("tibco_app_user[%s]=%s" % ( _action, app_user))

        if action in ['start','stop' ,'status','restart'] and nexusContent.version is None:
            nexusContent.version='current'

        if action in ['start','stop' ,'status','restart'] and nexusContent.version is None:
            nexusContent.version='current'

        if ':' in rtv_map['APPMANAGE_CREDENTIALS']:
           args=['-user', '-pw']
           i=0
           for _key in rtv_map['APPMANAGE_CREDENTIALS'].split(':'):
              _appmanage_creds="%s %s %s" % (APPMANAGE_CREDS, args[i], _key)
              i+=1
        else:
           _appmanage_creds='-cred %s/%s' % (str(base_dir),rtv_map['APPMANAGE_CREDENTIALS'])

        print (self.data_wrapper_header % (
            self.options.command,
            _action,
            self.options.level,
            log_array[0], log_array[1], log_array[2], log_array[3],
            _action,
            pki_identiy,
            self.ssh_options,
            echoEnvVars,
            '', # verbose flag
            app_user,
            struct['uuid'],
            nowaitflag,
            "150", #timeout
            _appmanage_creds, #appmange credentials
            target_list,
            upload_list,
            upload_list,
            self.stage_directory,
            nexusContent.version,
            nexusContent.saveArchiveName,
            nexusContent.artifactId,
            '%'
        ),file=outfile)

        data = []
        _remote_script_begin = self.remote_script_begin % (
            log_array[0], log_array[1], log_array[2], log_array[3],
            envvar_list, verboseFlag)
        _remote_appmanage_stage_snippet_begin = self.remote_appmanage_stage_snippet_begin % (envvar_list)
        readme = self.remote_readme_snippet % (self.readme_info(struct,nexusContent))
        _remote_audit_snippet = self.remote_audit_snippet % (datetime.datetime.now(), _action, readme)
#         readme = None
        readme_struct = self.readme_info(struct,nexusContent)
#         if readme_struct is not None:
#             readme = self.remote_readme_snippet % (readme_struct)
#         _remote_audit_snippet = self.remote_audit_snippet % (datetime.datetime.now(), _action, readme_struct)

        if _action == 'stage':

            data.append({'upload':self.upload_snippet})
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'readme':readme})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'appmanage_stage_snippet_begin':_remote_appmanage_stage_snippet_begin})
            data.append({'relink': self.remote_relink_snippet})
            data.append({'remote_create_scripts':self.remote_create_scripts_snippet})
            data.append({'appmanage_stage_end':self.remote_appmanage_stage_snippet_end})
            data.append({'remote_script_end': self.remote_script_end})

        elif _action == 'install':

            data.append({'upload':self.upload_snippet})
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'readme':readme})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'appmanage_stage_snippet_begin':_remote_appmanage_stage_snippet_begin})
            data.append({'relink': self.remote_relink_snippet})
            data.append({'remote_create_scripts':self.remote_create_scripts_snippet})
            data.append({'undeploy':self.remote_appmanage_undeploy_snippet})
            # same as 'deploy' but includes the start
            data.append({'install':self.remote_appmanage_install_snippet})
            data.append({'appmanage_stage_end':self.remote_appmanage_stage_snippet_end})
            data.append({'remote_script_end':self.remote_script_end})

        elif _action == 'uninstall':

            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_create_scripts':self.remote_create_scripts_snippet})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'stop':self.remote_stop_snippet})
            data.append({'appmanage_delete':self.remote_appmanage_delete_snippet})
            data.append({'remote_script_end':self.remote_script_end})

        elif _action == 'deploy':

            data.append({'upload':self.upload_snippet})
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'readme':readme})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'appmanage_stage_snippet_begin':_remote_appmanage_stage_snippet_begin})
            data.append({'relink': self.remote_relink_snippet})
            data.append({'remote_create_scripts':self.remote_create_scripts_snippet})
            data.append({'undeploy':self.remote_appmanage_undeploy_snippet})
            # same as 'install' but includes the start
            # data.append({'deploy':self.remote_appmanage_deploy_snippet})
            data.append({'install':self.remote_appmanage_install_snippet})
            data.append({'appmanage_stage_end':self.remote_appmanage_stage_snippet_end})
            data.append({'remote_start':self.remote_appmanage_start_snippet})
            data.append({'remote_script_end':self.remote_script_end})

        elif _action == 'delete':
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'appmanage_delete':self.remote_appmanage_delete_snippet})
            data.append({'remote_script_end':self.remote_script_end})

        elif _action == 'restart':
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_stop':self.remote_stop_snippet})
            data.append({'remote_start':self.remote_appmanage_start_snippet})
            data.append({'remote_script_end':self.remote_script_end})
        elif _action == 'stop':
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_stop':self.remote_appmanage_stop_snippet})
            data.append({'remote_script_end':self.remote_script_end})
        elif _action == 'start':
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_stop':self.remote_appmanage_stop_snippet})
            data.append({'remote_start':self.remote_appmanage_start_snippet})
            data.append({'remote_script_end':self.remote_script_end})
        elif _action == 'run':
            raise Exception("run not available for TIBCO BW")
        elif _action == 'relink':
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_relink':self.remote_relink_snippet})
            data.append({'remote_script_end':self.remote_script_end})
        elif _action == 'clean':
            data.append(_remote_script_begin)
            data.append(self.remote_cleanup_snippet)
            data.append(self.remote_script_end)
        elif _action == 'clean_with_manifest':
            data.append(_remote_script_begin)
            data.append(self.remote_cleanup_with_manifest_snippet)
            data.append(self.remote_script_end)
        # elif _action == 'wipeout':
        #     data.append(_remote_script_begin)
        #     data.append(self.remote_wipeout_snippet)
        #     data.append(self.remote_script_end)
        elif _action == 'ssh':
            data.append({'ssh_pki':self.pki_ssh_snippet})
            data.append({'done':self.done_deal_snippet})
        elif _action == 'inspect':
            data.append(self.inspect_snippet)
            data.append(self.done_deal_snippet)
        elif _action == 'ps':
            data.append(self.ps_test_snippet)
            data.append(self.done_deal_snippet)
        elif _action == 'cloudenv':
            data.append(self.cloudenv_snippet)
            data.append(self.done_deal_snippet)

        # generate all the lines
        for script_line in data:
            for key in script_line:
                line = "writing script line [%-35s]" % (key)
                fdeploy.LOGGER.info(line)
            print(script_line[key],file=outfile)
        # if test no footer needed the test is selfcontanted and
        # does not need script streaming
        if _action not in ['test', 'inspect', 'ssh', 'ps','cloudenv','1install','2install-undeploy','3install-deploy']:
            print( self.data_wrapper_footer,file=outfile)
            print(self.data_wrapper_execute,file=outfile)

class tibco_bwDeployer(baseGenerator):

    def __init__(self, component, cli_options, stage_directory):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_tibcobw_generator.REGISTERED_ACTIONS
        super(tibco_bwDeployer, self).__init__(
            component, cli_options, stage_directory)
        fdeploy.LOGGER.debug(
            " < init bash-generator tibco_bwDeployer platform for " + str(self.component))
