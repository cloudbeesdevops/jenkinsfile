# -*- coding: utf-8 -*-
from __future__ import print_function
import codecs
import sys
import os
import re
import fdeploy
import subprocess
import time
import datetime
import tempfile
import fdeploy
from fdeploy.security import inline_singleton,is_encapsulated
from fdeploy.pam import pamAuth
from fnexus.gavClass import gavClass

DEFAULT_TARGET_SPEC = re.compile('(\S+)\@([a-z0-9-\._]+)')
URL_URI_TARGET_SPEC = re.compile('(\S+)\@([a-z0-9-\._]+):(\S+)')

def _needs_nexus_resolving(action, no_download):
    if no_download == True:
        return False
    return action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING

def load_snippet(snippetFile):
    snippet_text = ''
    path = os.path.dirname(fdeploy.__file__)
    with codecs.open ("%s/../snippets/%s" % (path, snippetFile), "r", "utf-8") as snippet:
        snippet_text=snippet.read()
    return snippet_text.encode('utf8', 'ignore')

def load_file_as_string(sfile, strip=False):
    snippet_text = ''
    with codecs.open (sfile, "r", "utf-8") as snippet:
        snippet_text=snippet.read()
    snippet_text = snippet_text.encode('utf8','ignore')
    return snippet_text


def load_file(sile,remove_password=False,generated=False):
    text = ''
    with codecs.open (sile, "r", "utf-8") as snippet:
        text=snippet.read()
    text = text.encode('utf8','ignore')
    a = text.split('\n')
    if remove_password == True:
        a = [env for env in a if 'export PCF_PASSWORD' not in env and 'tagedir=' not in env]
    text=""
    for l in a:
        if text == "":
            text = l
        else:
            text = "%s\n%s" % (text,l)
    try:
        #print "[%s]"  % text[-1]

        if generated == True and text[-1] == "\n":
            text = text[:-1]
    except:
        pass
    return text

def anti_regex_pattern(count, current_pattern):
    pattern = ''
    i = 0
    while (i < count):
        pattern += '\(.+\)'
        if i != count-1:
            pattern += '(.+)'
        i += 1
    r = re.compile(pattern).findall(current_pattern)
    #print "patter=",r, type(r[0])
    if type(r[0]) == tuple:
        r = list(r[0])
    strs = '%s'
    for i in r:
        strs += i
        strs += '%s'
    strs = strs.replace('\\','')
    strs = strs.encode('utf-8').decode('unicode_escape')
    return strs


class baseGenerator(object):
    ' ! The base class for bash generators'

    data_wrapper_header = load_snippet('data_wrapper_header.sh')
    data_wrapper_footer = load_snippet('data_wrapper_footer.sh')
    data_wrapper_execute = load_snippet("master_execute.sh")

    pki_ssh_snippet = load_snippet('pki_ssh_snippet.sh')
    ps_test_snippet = load_snippet('ps_test_snippet.sh')
    inspect_snippet = load_snippet('inspect_snippet.sh')
    upload_snippet = load_snippet('upload_snippet.sh')
    cloudenv_snippet = load_snippet('cloudenv_test_snippet.sh')
    done_deal_snippet = '''done'''

    remote_script_begin= load_snippet('remote_script_begin.sh')
    remote_overwrite_snippet = load_snippet('remote_overwrite_snippet.sh')
    remote_stage_snippet_begin = load_snippet('remote_stage_snippet_begin.sh')
    remote_cleanup_snippet = load_snippet('remote_cleanup_snippet.sh')
    remote_cleanup_with_manifest_snippet = load_snippet('remote_cleanup_with_manifest_snippet.sh')
    remote_wipeout_snippet = load_snippet('remote_wipeout_snippet.sh')
    remote_relink_snippet = load_snippet('remote_relink_snippet.sh')
    remote_restart_snippet = load_snippet('remote_restart_snippet.sh')
    remote_stop_snippet = load_snippet('remote_stop_snippet.sh')
    remote_run_snippet = load_snippet('remote_run_snippet.sh')
    remote_status_snippet = load_snippet('remote_status_snippet.sh')
    remote_stage_snippet_end = load_snippet('remote_stage_snippet_end.sh')
    remote_script_end = load_snippet('remote_script_end.sh')
    remote_properties_snippet = load_snippet('remote_properties_snippet.sh')
    remote_readme_snippet = load_snippet('remote_readme_snippet.sh')
    remote_audit_snippet = load_snippet('remote_audit_snippet.sh')
    encryptionModule = None


    def __init__(self, component, cli_options, stage_directory='.', target_spec = None):
        self.options = cli_options
        # validate if the action is registered thru introspection
        # the vocabulary of this implementation
        if type(cli_options) == dict:
            try:
                self.__class__.has_action(self, cli_options['action'])
            except KeyError as keyerr:
                raise Exception("action was not supplied in cli_options %s: reason" % (cli_options, keyerr))

        else:
            self.__class__.has_action(self, cli_options.action)
        cwd = os.getcwd()
        self.stage_directory = str(cwd) + '/' + str(stage_directory)
        self.extension = 'sh'
        self.component = component
        self.name = component.name_format
        self.id = id
        self.level = None
        self.ssh_options = '-oConnectTimeout=20 -oStrictHostKeyChecking=no \
-oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null'
        self.indicator = re.sub(r"_?%\d*[dsf]", "", self.name)
        if os.path.isdir(self.stage_directory) == False:
            os.makedirs(self.stage_directory, 0755)
                #print str(target_spec)
        self.target_spec = URL_URI_TARGET_SPEC if target_spec is None else target_spec
        if inline_singleton():
            self.encryptionModule = inline_singleton()

        fdeploy.LOGGER.debug(" < init baseGenerator: stage_directory > " +
                      str(self.stage_directory))

    def needs_nexus_version(self,action):
        return True


    def get_target_list(self, __targets, target_spec):
        targets = []
        __target_list = ''
        _continue = True
        #target_spec = self.target_spec if target_spec is None else target_spec

        for __target in __targets:
            __target = __target.encode('utf-8').decode('unicode_escape')
            __target = __target.decode('string_escape')
            if target_spec is None:
                pass
            elif target_spec.match(__target) is None:
                fdeploy.LOGGER.error(
                    " ! ERROR: '%s' is ill-formed target location." % (__target))
                _continue = False
            else:
                a = list(target_spec.findall(__target)[0])
                #print "B4: ",a, len(a),target_spec.pattern,target_spec
                regex = anti_regex_pattern(len(a),target_spec.pattern)
                if 'user' in self.options.__dict__.keys() and self.options.user is not None:
                    # print regex
                    a[0]=self.options.user
                if 'path' in self.options.__dict__.keys() and self.options.path is not None:
                    a[len(a)-1]=self.options.path
                __target = regex % tuple(a)
                #print "CHANGES: ",a, len(a),target_spec.pattern
            targets.append(__target)
            __target_list = __target_list + "   \"%s\"\n" % (__target)
        if _continue == False:
            raise Exception("No targets have been listed!")
        #print targets
        return targets,__target_list

    ''' create list of upload items in the : : : : notation for extraction on the backend '''
    def get_upload_list(self,struct,version):
        if version:
            version = version.replace('*','')
        upload_list=''
        if struct['contents'] is not None:
            for upload_item in struct['contents']:
                fdeploy.LOGGER.debug("upload_item: %s:%s" % (upload_item,upload_item.isRelative() if hasattr(upload_item, 'isRelative')  else '---'))
                path = version
                owner = ''
                perms =  ''
                dirs = ''
                if type(upload_item) == dict:
                    # turn dictionary into class
                    upload_item = fdeploy.content.content_factory(upload_item)
                if isinstance(upload_item, fdeploy.content.contentClass):
                    if  upload_item.prependpath:
                        dirs = upload_item.prependpath
                    if  upload_item.permission:
                        perms = upload_item.permission
                    if  upload_item.owner:
                        owner = upload_item.owner
                    if upload_item.isRelative() == False:
                        path = '.'
                elif isinstance(upload_item, gavClass):
                    path = version
                    if upload_item.isRelative() == True and upload_item.relativePath:
                        # backwards compatibility with older json files that have relativepaths
                        if 'apparchives' not in upload_item.relativePath:
                            path = "%s/%s" % (version, upload_item.relativePath)
                    elif upload_item.isRelative() == False:
                        path = "."
                else:
                    raise Exception("Unknown content class %s : %s" % (str(type(upload_item)),upload_item))
                if upload_list != '':
                    upload_list = upload_list  + ' '
                upload_list = upload_list + "\"%s:%s:%s:%s:%s\"" % \
                    (upload_item.saveArchiveName, path, dirs, perms, owner)
        return upload_list

    '''returning the nexus content in a list of content'''
    # version can get supplied not recommended, however added for testing purpose
    def get_first_nexus_component_in_content(self,mandatory_flag=True, version=None):
        # pick the first Nexus resolved component artifact.
        #deploy.dump(mandatory_flag)
        for nexusContent in self.component.contents:
            if self.component.isNexusArtifact(nexusContent):
                break
        fdeploy.LOGGER.debug("versioned-content: %s" % (str(nexusContent)))
        if nexusContent is None:
            raise Exception("No Nexus content was found for this component.")
        if self.component.isNexusArtifact(nexusContent) == True:
            if nexusContent.version is None and version is not None:

                nexusContent.version = version
                return nexusContent
            if nexusContent.version is None and mandatory_flag == True:
                raise Exception("unable to generate script with %s without resolved version" % (nexusContent))
        else:
            if mandatory_flag == True:
                raise Exception("unable find nexus content with %s without resolved version" % (nexusContent))

        return nexusContent

    ''' find the application user with the cloudenv user for cm and app distinguishment'''
    def get_app_user(self,target,app_user,_action):
        #
        # find the application user, assuming the user being supplied is the cm user
        # based on the last processed target
        #
        if type(target) == list:
            target = target[0]
        #print "TARGAY", target
        if  not target is None:
            if _action in fdeploy.ACTIONS_APP_USER_REQUIRED:
                # switch to app user when we are dealing with CloudVMs authority roles
                if 'cm@' in target:
                    app_user = ''
                else:
                    app_user = re.sub(r'\@.+', '', target)
            else:
                # use the default user specified in the target
                # the user will be parse by data_wrapper_header
                app_user = re.sub(r'\@.+', '', target)

            try:
                # if the user is overwritten in arguments then use that user
                if self.options.user is not None:
                        app_user = self.options.user
            except AttributeError:
                pass
            fdeploy.LOGGER.debug("using '%s' for execution" % (app_user))
        fdeploy.LOGGER.info("app_user[%s]=%s" % ( _action, app_user))
        return app_user

    def readme_info(self, struct, nexusContent):
        readme_struct = None
        if 'readme' in struct.keys():
            readme_struct = struct['readme']
            if readme_struct:
                readme_struct.append({'artifactId': nexusContent.artifactId})
                readme_struct.append({'version': nexusContent.version})
        return readme_struct

    def write_refactored(self, outfile, _action, data, log_array,
            envvar_list, verboseFlag, app_user):
        _remote_script_begin= self.remote_script_begin% (
            log_array[0], log_array[1], log_array[2], log_array[3],
            envvar_list, verboseFlag)
        _remote_properties_snippet = self.remote_properties_snippet % (envvar_list)
        fdeploy.LOGGER.info("generating for [%s] as %s" % ( _action, app_user))
        self.write(outfile,_action,data,_remote_script_begin,_remote_properties_snippet)

    def write(self, outfile, _action, data, _remote_script_begin,
            _remote_properties_snippet, readme=None):
        self.write_with_end(outfile, _action, data, _remote_script_begin,
                _remote_properties_snippet,self.data_wrapper_footer, readme)

    def write_with_end(self, outfile, _action, data, _remote_script_begin,
            _remote_properties_snippet,_data_wrapper_footer, readme_struct):
        readme = None
        if readme_struct is not None:
            readme = self.remote_readme_snippet % (readme_struct)
        _remote_audit_snippet = self.remote_audit_snippet % (datetime.datetime.now(), _action, readme_struct)
        if _action == 'stage':
            data.append(self.upload_snippet)
            data.append(_remote_script_begin)
            data.append(self.remote_stage_snippet_begin)
            data.append(readme)
            data.append(_remote_audit_snippet)
            data.append(_remote_properties_snippet)
            data.append(self.remote_stage_snippet_end)
            data.append(self.remote_script_end)
        elif _action == 'install':
            data.append(self.upload_snippet)
            data.append(_remote_script_begin)
            data.append(self.remote_overwrite_snippet)
            data.append(self.remote_stage_snippet_begin)
            data.append(readme)
            data.append(_remote_audit_snippet)
            data.append(_remote_properties_snippet)
            data.append(self.remote_create_scripts_snippet)
            data.append(self.remote_relink_snippet)
            data.append(self.remote_stage_snippet_end)
            data.append(self.remote_script_end)
        elif _action == 'properties':
            data.append(_remote_script_begin)
            data.append(readme)
            data.append(_remote_audit_snippet)
            data.append(_remote_properties_snippet)
            data.append(self.remote_script_end)
        elif _action == 'restart':
            data.append(_remote_script_begin)
            data.append(_remote_audit_snippet)
            data.append(self.remote_restart_snippet)
            data.append(self.remote_script_end)
        elif _action == 'stop':
            data.append(_remote_script_begin)
            data.append(_remote_audit_snippet)
            data.append(_remote_audit_snippet)
            data.append(self.remote_stop_snippet)
            data.append(self.remote_script_end)
        elif _action == 'start':
            data.append(_remote_script_begin)
            data.append(_remote_audit_snippet)
            data.append(_remote_audit_snippet)
            data.append(self.remote_restart_snippet)
            data.append(self.remote_script_end)
        elif _action == 'run':
            data.append(_remote_script_begin)
            data.append(_remote_audit_snippet)
            data.append(self.remote_run_snippet)
            data.append(self.remote_script_end)
        elif _action == 'relink':
            data.append(_remote_script_begin)
            data.append(_remote_audit_snippet)
            data.append(self.remote_relink_snippet)
            data.append(self.remote_script_end)
        elif _action == 'status':
            data.append(_remote_script_begin)
            data.append(_remote_audit_snippet)
            data.append(self.remote_status_snippet)
            data.append(self.remote_script_end)
        elif _action == 'clean':
            data.append(_remote_script_begin)
            data.append(_remote_audit_snippet)
            data.append(self.remote_cleanup_snippet)
            data.append(self.remote_script_end)
        elif _action == 'clean_with_manifest':
            data.append(_remote_script_begin)
            data.append(_remote_audit_snippet)
            data.append(self.remote_cleanup_with_manifest_snippet)
            data.append(self.remote_script_end)
        elif _action == 'wipeout':
            data.append(_remote_script_begin)
            data.append(_remote_audit_snippet)
            data.append(self.remote_wipeout_snippet)
            data.append(self.remote_script_end)
        elif _action == 'ssh':
            data.append(self.pki_ssh_snippet)
            data.append(self.done_deal_snippet)
        elif _action == 'inspect':
            data.append(self.inspect_snippet)
            data.append(self.done_deal_snippet)
        elif _action == 'ps':
            data.append(self.ps_test_snippet)
            data.append(self.done_deal_snippet)
        elif _action == 'cloudenv':
            data.append(self.cloudenv_snippet)
            data.append(self.done_deal_snippet)

        # generate all the lines
        for script_line in data:
            print(script_line, file=outfile)
        # if test no footer needed the test is selfcontanted and
        # does not need script streaming
        if _action not in ['test', 'inspect', 'ssh', 'ps','cloudenv']:
            print(_data_wrapper_footer, file=outfile)


    def deploying(self, files, verbose=False):
        p = []
        i = 0
        if len(files) < 1:
            raise Exception("Abort! No files were generated for this deployment of type %s." % (self))
        for _file in files:
            i += 1
            fdeploy.LOGGER.debug(" > deploying execution: %s of %s / %s" % (i,len(files),_file))
            fdeploy.LOGGER.debug(" > files are %s" % (files))
            __file = str(self.stage_directory) + "/" + str(_file)
            _filename = str(self.stage_directory) + \
                "/deploy-" + str(_file) + ".log"
            os.chmod(__file, 0b111101101)  # rwxr-xr-x make it executable
            # subprocess.call(["/var/tmp/fdeploy/xls.sh"])
            # remote invocation of the generated script
            my_env = os.environ.copy()
            with open(_filename, 'w') as __stdout:
                #subprocess.call([__file], stdout=__stdout)
                if self.options.parallel == False:
                    proc=subprocess.Popen(__file, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
                    for line in proc.stdout:
                        if verbose:
                            sys.stdout.write(line)
                        __stdout.write(line)
                    proc.wait()
                else:
                    with open(os.devnull, 'r+b', 0) as DEVNULL:
                        px = subprocess.Popen([__file],
                            stdin=DEVNULL, stdout=DEVNULL, stderr=__stdout, env=my_env,
                            close_fds=True)
                        p.append(px)
                        # Wait until process terminates
                        while px.poll() is None:
                            time.sleep(1)
            fdeploy.LOGGER.debug(" > deploying execution done: %s for %s" % (i,_file))



    def report(self,__reports):
        show_pattern = None
        if 'show' in self.options:
            if self.options.show == 'running' or self.options.show == 'up' or self.options.show == 'started':
                show_pattern = 1
            elif self.options.show == 'not-running' or self.options.show == 'down' or self.options.show == 'stopped':
                show_pattern = 2
        # uniques only
        reports = []
        for _r in __reports:
            if not _r in reports:
                reports.append(_r)
        for _filename in sorted(reports):
            fdeploy.LOGGER.info("examining: %s" % (_filename))
            #fdeploy.LOGGER.trace(">>>>>> : %s" % (os.path.dirname(_filename)))
            _filein_ = []
            with open(_filename, 'r') as f:
                _file = os.path.basename(_filename)
                _filein_ = f.readlines()
            show_file_based_on_content = False
            if not show_pattern is None:
                for line in _filein_:
                    if 'is not running' in line:
                        fdeploy.LOGGER.trace(" %d = %s" % (show_pattern, line))
                        show_file_based_on_content = True
                        break
                if show_pattern == 1:
                    show_file_based_on_content = False if show_file_based_on_content == True else True
            else:
                show_file_based_on_content = True
            if show_file_based_on_content == True:
                for line in _filein_:
                    print("%s : %s" % (_file, line)),

    def has_action(self, action):
        if action not in self.REGISTERED_ACTIONS:
            raise Exception("Action %s not registered as a fucntion with %s" % (action, self) )

class basePcfGenerator(baseGenerator):
    data_wrapper_header = load_snippet('pcf/pcf_header_snippet.sh')
    data_wrapper_footer = load_snippet('pcf/pcf_footer_snippet.sh')
    upload_snippet = load_snippet('pcf/upload_snippet.sh')
    status_snippet = load_snippet('pcf/status_snippet.sh')
    delete_snippet = load_snippet('pcf/delete_snippet.sh')
    start_snippet = load_snippet('pcf/start_snippet.sh')
    stop_snippet = load_snippet('pcf/stop_snippet.sh')
    restart_snippet = load_snippet('pcf/restart_snippet.sh')
    pamAuthModule = pamAuth()

    def __init__(self, component, cli_options, stage_directory='.', target_spec = None, alternate_pam=None):
        super(basePcfGenerator, self).__init__(
            component, cli_options, stage_directory, target_spec)
        if not alternate_pam is None:
            self.pamAuthModule = alternate_pam
        else:
            self.pamAuthModule = pamAuth()

    def needs_nexus_version(self,action):
        #print "%s -> %s" % (action, json.dumps(action,indent=5))
        if ('ssh' in action or 'start' in action or 'stop' in action):
            return False
        return True


    def list_machines(self, struct,target_list,search=None):
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        for target in __targets:
            _target = target.split(":")[0]
            if self.options.user is not None:
                _target = re.sub(
                    r'^(\S+)\@', self.options.user + str('@'), _target)
            if not _target in target_list:
                target_list.append(_target)

    def temp_directory(self):
        tempd = tempfile.mkdtemp()
        return tempd

    def rtv_mapping(self, action, rtv_map, __level, uuid, targets, target_spec, REQUIRED_RTVS, version=None):
        #
        # resolving the runtime variables (rtv), first global then components.
        #
        fdeploy.LOGGER.error(__level)
        level=__level['level'].upper()

        rtv_map={}
        __targets = fdeploy.flatten_level_targets(targets)
        fdeploy.LOGGER.debug("hosts : %s spec %s"  % (str(__targets), target_spec))
        targets__,target_list=self.get_target_list(__targets, target_spec)
        if len(target_list) < 2:
            raise Exception("targetlist empty? %s" % (targets))
        if len(targets__) > 1:
            raise Exception("PCF can only have one target for each space")
        target=__targets[0]
        rtv_map,envvar=self.component.get_env_vars(self.options,level, REQUIRED_RTVS, version, init=rtv_map)
        try:
            rtv_map['SPACE'],rtv_map['API_URL']=target.split('@')
            rtv_map['DOMAIN'] = re.sub(r'api.sys', 'app', rtv_map['API_URL'])
            #if 'APP_HOST' not in rtv_map:
            #    rtv_map['APP_HOST']= "%s" % (uuid)
            if self.resolve_password == True:
                user,pwd = self.pamAuthModule.get_passwd(rtv_map['PAM_ID'])
                rtv_map['PCF_USER']=user
                rtv_map['PCF_PASSWORD']=pwd
            rtv_map['SPRING_PROFILES_ACTIVE']=__level['level']
            rtv_map['CF_HOME'] = self.temp_directory()
            if 'PROD' not in level:
                if 'APP_HOST' not in rtv_map:
                    if (level in uuid) == False:
                        rtv_map['APP_HOST'] = "%s-%s" % (level, uuid)
                if re.findall('^!', rtv_map['APP_NM']):
                    rtv_map['APP_NM'] = re.sub('^!', '', rtv_map['APP_NM'])
                elif (level in rtv_map['APP_NM']) == False:
                    rtv_map['APP_NM'] = "%s-%s" % (level, rtv_map['APP_NM'])
            else:
                #we need to have APP_HOST variable in prod section for mandatory
                if 'APP_HOST' not in rtv_map:
                    rtv_map['APP_HOST'] = "%s" % (uuid)
                else:
                    if (level in rtv_map['APP_HOST']) == True:
                        _level = level+"-"
                        rtv_map['APP_HOST'] = re.sub(_level,'',rtv_map['APP_HOST'])

        except NameError as err:
            raise Exception("unable to parse target URL %s: %s" % (target,err))
        if action == 'install':
            rtv_map['PCF_PUSH_OPTIONS']='--no-start'
        elif 'PCF_PUSH_OPTIONS' in rtv_map.keys():
            rtv_map.pop('PCF_PUSH_OPTIONS', None)

        # decrpyt any values
        if inline_singleton():
            for key in rtv_map.keys():
                    if is_encapsulated(rtv_map[key]):
                        rtv_map[key]=inline_singleton().decrypt(rtv_map[key])

        fdeploy.LOGGER.debug("target => %s " % (target))
        #unused,envvar_list=self.component.get_env_vars(self.options,level, self.REQUIRED_RTVS, None)
        envvar_list=self.component.as_envvars(rtv_map)

        return target, target_list, rtv_map, envvar_list
