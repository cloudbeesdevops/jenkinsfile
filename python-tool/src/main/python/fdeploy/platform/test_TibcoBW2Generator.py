
from fdeploy.platform.bash_tibcobw2_generator import tibco_bw2Generator
from fdeploy.content.fdeployComponent import fdeployComponent

options = {'action': 'publish'}
component = fdeployComponent({
    'id': 'componentId',
    'platform': 'bash_generator',
    'type': 'tibco_bw2',
    'content':  [{
        "gav": "com.fedex.sefs.core:sefs_suCLEARANCEFeed:zip",
        "relativePath": "apparchives",
        "saveArchiveName": "sefs_suCLEARANCEFeed.zip"
    }],
    'name_format': 'format',
    'levels':
    [{
        'level': 'L1'
    }]

}, 'inline.json')


class TestTibcoBW2Generator(object):

    @classmethod
    def setUpClass(clz):
        pass

    def test_init(self):
        tibco_bw2Generator(component, options)

    def test_generate(self):
        tibco_bw2Generator(component, options)
        targets = []
        struct = {'level': 'L1', 'contents': component.content, 'targets': targets,  'uuid': 'all', 'rtv': [
            {
                "name": "TRA_HOME",
                "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
            },
            {
                "name": "JMX_PORT",
                "value": "abcdef"
            },
            {
                "name": "PROCESS_NAME",
                "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
            },
            {
                "name": "ADMIN_TARGET_URL",
                "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
            },
            {
                "name": "EAINUMBER",
                "value": "#CLEARANCEFeed"
            },
            {
                "name": "APPMANAGE_CREDENTIALS",
                "value": "#CLEARANCEFeed"
            },
            {
                "name": "EAINUMBER",
                "value": "#CLEARANCEFeed"
            }


        ]}
        #generator.generate(struct, 'test.txt')


if __name__ == '__main__':
    unittest.main()
