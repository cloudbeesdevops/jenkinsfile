    

from fdeploy import Options
from fdeploy.platform.bash_tibcobw2_generator import tibco_bw2Generator
import os
from fdeploy.content.fdeployComponent import fdeployComponent
from fdeploy.UtilsForTesting import compare_files
from fdeploy.deployer.TibcoBW5Configurer import TibcoBW5Configurer

component = fdeployComponent({
    'id': 'componentId',
    'platform': 'bash_generator',
    'type': 'tibco_bw2',
    'rtv': [
        {
            "name": "TRA_HOME",
            "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
        },
        {
            "name": "JMX_PORT",
            "value": "abcdef"
        },
        {
            "name": "PROCESS_NAME",
            "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
        },
        {
            "name": "ADMIN_TARGET_URL",
            "value": "tibcosilver@srh00607.ute.fedex.com:/opt/tibco/silver/persistentdata/FXE/apps/CLEARANCEFeed"
        },
        {
            "name": "EAINUMBER",
            "value": "#CLEARANCEFeed"
        },
        {
            "name": "APPMANAGE_CREDENTIALS",
            "value": "#CLEARANCEFeed"
        },
        {
            "name": "EAINUMBER",
            "value": "#CLEARANCEFeed"
        },
        {
            "name": "APPMANAGE_XML",
            "value": "../../test/resources/bw5/FXFShipFeed_template_l2.xml"
        },
        {
            "name": "PROPS_FILE_PATH",
            "value": "../../test/resources/bw5/L1-Auto.properties"
        },
        {
            "name": "APPMANAGE_PLAN",
            "value": "../../test/resources/bw5/FXFShipFeed_plan-descriptors.xml"
        }


    ],
    'content':  [{
        "gav": "com.fedex.sefs.core:sefs_suCLEARANCEFeed:1.0.0:zip",
        "relativePath": "apparchives",
        "saveArchiveName": "sefs_suCLEARANCEFeed.zip"
    }],
    'name_format': 'format',
    'levels':
    [{
        'level': 'L1',
        'filter' : { 'maxHeapSize' : 'value2'},
        'targets' : [ 'a@com', 'b@com', 'c@com']
    }]

}, 'inline.json')


class TestTibcoBW2Generator(object):

    options = None

    rtvs =  [
        { 'name': 'APP_REF', 'value' : '${APPMANAGE_PLAN}.backupt'},
        { 'name': 'LEVEL', 'value' : 'L1'}]


    def options(self):
        return Options(command='deploy',action='stage',identity='.', X=True, id = None, path=".", no_wait = False, level='L1',user='sefs')

    def teardown_class(module):
        try:
            os.remove('platformBW5-2.txt')
        except OSError:
            pass

    def test_init(self):
        tibco_bw2Generator(component, self.options())

    def test_generate(self):
        generator = tibco_bw2Generator(component, self.options())
        targets = []
        struct = {'level': {'level':'L1','rtv' : self.rtvs }, 'directory': '.', 'contents': component.content, 'targets': targets,  'uuid': 'all'}
        with open("platformBW5-2.txt", "w") as fh:
            generator.generate(struct, fh)

    def test_filterChaining(self):
       generator = tibco_bw2Generator(component, self.options())
       path=os.path.abspath('../../test/resources/bw5/common.properties')
       map=generator.readFilters(path)
       assert 'common' ==  map['value']
       assert 'value' in map.keys()
       map=generator.readFilters('../../test/resources/bw5/common.properties,../../test/resources/bw5/L1.properties')
       assert 'my_value' ==  map['my_property']
    #    overwrite
       assert 'L1' ==  map['value']

    #    reverse
       map=generator.readFilters('../../test/resources/bw5/L1.properties,../../test/resources/bw5/common.properties')
       assert 'my_value' ==  map['my_property']
    #    overwrite
       assert 'common' ==  map['value']

    #    with level variable.
       rtvs=component.rtvMapping
       for rtv_pair in self.rtvs:
           rtvs[rtv_pair['name']]=rtv_pair['value']
       map=generator.readFilters('../../test/resources/bw5/common.properties,../../test/resources/bw5/L1.properties',rtvs)
       assert 'my_value' ==  map['my_property']
    #    overwrite
       assert 'L1' ==  map['value']
       assert 'L1_NAME' ==  map['Name']
       assert 'abcdef' ==  map['jmxport']
       assert '../../test/resources/bw5/FXFShipFeed_plan-descriptors.xml.backupt' ==  map['File']

    def test_tibcobw2_applyExternalizedBindings(self):
        generator = tibco_bw2Generator(component, self.options())
        map=generator.readFilters('../../test/resources/bw5/bw52-l1.properties,../../test/resources/bw5/common.properties,../../test/resources/bw5/L1.properties')
        __targets = ['TLM1@urh01','TLM2@urh02', 'TLM3@urh03']
        configurer = TibcoBW5Configurer(
           '../../test/resources/bw5/FXFShipFeed_template_l2.xml')
        deployment_config = generator.update_deployment_desciptor(configurer, __targets, map, 'L1')
        with open('FXFShipFeed_template_l2.out','w') as f:
            f.write(deployment_config[0])
        compare_files('../../test/resources/bw5/FXFShipFeed_template_l2_externalized.xml', 'FXFShipFeed_template_l2.out', strip=True)
