# -*- coding: utf-8 -*-

from fdeploy.platform.bash_filecopy_generator import filecopyDeployer, filecopyGenerator
from fdeploy.platform.bash_tomcat_generator import tomcatDeployer, tomcatGenerator
from fdeploy.platform.bash_pcf_generator import pcfDeployer, pcfGenerator
from fdeploy.platform.bash_pcf_cups_generator import pcf_cupsDeployer, pcf_cupsGenerator
from fdeploy.platform.bash_java_generator import javaDeployer, javaGenerator
from fdeploy.platform.bash_java_auto_generator import java_autoDeployer, java_autoGenerator
from fdeploy.platform.bash_tibcoas_generator import tibco_asDeployer, tibco_asGenerator
from fdeploy.platform.bash_tibcobe_generator import tibco_beDeployer, tibco_beGenerator
from fdeploy.platform.bash_tibcobw_generator import tibco_bwDeployer, tibco_bwGenerator
from fdeploy.platform.bash_tibcobw2_generator import tibco_bw2Deployer, tibco_bw2Generator
from fdeploy.platform.bash_tibcoasleech_generator import tibco_asleechDeployer, tibco_asleechGenerator
