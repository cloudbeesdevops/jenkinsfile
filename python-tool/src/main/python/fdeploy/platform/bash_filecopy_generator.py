# -*- coding: utf-8 -*-
from __future__ import print_function

import re
import fdeploy
import fdeploy.platform
import fdeploy.fdeployLoader
from fdeploy.platform import load_snippet, baseGenerator
from fdeploy.platform import DEFAULT_TARGET_SPEC

#from fdeploy.platform.bash_tibcobw_generator import tibco_bwGenerator, tibco_bwDeployer
REQUIRED_RTVS = []
REGISTERED_ACTIONS = ['clean', 'wipeout', 'install', 'stage', 'ml', 'machine_list',
                      'deploy', 'ssh', 'inspect', 'ps', 'cloudenv','relink']
TARGET_URL_SPEC=DEFAULT_TARGET_SPEC


class filecopyDeployer(baseGenerator):
    def __init__(self, component, cli_options, stage_directory):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_filecopy_generator.REGISTERED_ACTIONS
        super(filecopyDeployer, self).__init__(
            component, cli_options, stage_directory)
        fdeploy.LOGGER.debug(
            " < init bash-generator filecopyDeployer platform for " + str(self.component))

class filecopyGenerator(baseGenerator):
    files = []
    # TIBCO
    remote_create_scripts_snippet = load_snippet('filecopy/remote_create_scripts_snippet.sh')

    def __init__(self, component, cli_options, stage_directory='.', target_spec = DEFAULT_TARGET_SPEC):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_filecopy_generator.REGISTERED_ACTIONS
        self.REQUIRED_RTVS =  fdeploy.platform.bash_filecopy_generator.REQUIRED_RTVS

        super(filecopyGenerator, self).__init__(
            component, cli_options, stage_directory, target_spec)
        fdeploy.LOGGER.debug(
            " < init platform: bash-generator, class: filecopyGenerator  for " + str(self.component))

    def list_machines(self, struct,target_list,search=None):
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        for target in __targets:
            _target = target.split(":")[0]
            if self.options.user is not None:
                _target = re.sub(
                    r'^(\S+)\@', self.options.user + str('@'), _target)
            if not _target in target_list:
                target_list.append(_target)

    def needs_nexus_version(self,action):
        return False


    """Generate scripts for the components on the targets in the descriptors."""
    def generate(self, struct, outfile, action='status',target_spec=None, verbose=False):
        # ssh_options
        level = struct['level']
        # replace values in the snippets
        target_list = ''
        upload_list = ''
        envvar_list = ''
        app_user=None
        # pick the first Nexus resolved component artifact.
        nexusContent = self.get_first_nexus_component_in_content(self.needs_nexus_version(action))

        #
        # resolving the runtime variables (rtv), first global then components.
        #
        unused,envvar_list=self.component.get_env_vars(self.options,level['level'], self.REQUIRED_RTVS)
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        fdeploy.LOGGER.debug("hosts : %s spec %s"  % (str(__targets), target_spec))
        target,target_list=self.get_target_list(__targets, target_spec)
        if len(target_list) < 2:
            raise Exception("targetlist empty? %s" % (struct['targets']))
        #
        # generate the upload list based on the contents specified
        #
        version=""
        artifactId=struct['uuid']
        try:
            version = nexusContent.version
        except:
            pass
        try:
            artifactId = nexusContent.artifactId
        except:
            pass

        upload_list=self.get_upload_list(struct,version)
        #
        # applying execute identity if applicable
        #
        pki_identiy = ' ' if self.options.identity is None or self.options.identity == '' else '-i ' + \
            str(self.options.identity)
        verboseOn = "set -xv"
        verboseOff = "set +xv"
        verboseFlag = verboseOff if verbose == False else verboseOn

        # NO WAIT FLAG
        nowaitflag = '' if self.options.no_wait == False else '#SKIP#'

        log_array = fdeploy.bash_array()
        fdeploy.LOGGER.trace("log_array=" + str(log_array))

        # agregation of steps.
        actions = []
        actions.append(action)

        fdeploy.LOGGER.info( "generating for %s individual actions" % (len(actions)) )
        for _action in actions:
            # only echo the env vars on certain actions.
            echoEnvVars = verboseOff if _action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING else verboseFlag
            #
            # find the application user, assuming the user being supplied is the cm user
            # based on the last processed target
            #
            app_user = self.get_app_user(target,app_user,_action)

            print (self.data_wrapper_header % (
                self.options.command,
                _action,
                self.options.level,
                log_array[0], log_array[1], log_array[2], log_array[3],
                _action,
                pki_identiy,
                self.ssh_options,
                echoEnvVars,
                verboseFlag,
                app_user,
                struct['uuid'],
                nowaitflag,
                '20', #timeout
                target_list,
                upload_list,
                upload_list,
                self.stage_directory,
                version,
                nexusContent.saveArchiveName,
                artifactId,
                '%'
            ),file=outfile)

            data = []
            self.write_refactored(outfile, _action, data, log_array,
                    envvar_list, verboseFlag, app_user)
