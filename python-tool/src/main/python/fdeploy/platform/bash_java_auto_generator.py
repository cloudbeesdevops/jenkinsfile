# -*- coding: utf-8 -*-
from __future__ import print_function

import re
import fdeploy
import fdeploy.platform
import fdeploy.fdeployLoader
from fdeploy.platform import load_snippet, baseGenerator
from fdeploy.platform import DEFAULT_TARGET_SPEC, _needs_nexus_resolving

#from fdeploy.platform.bash_tibcobw_generator import tibco_bwGenerator, tibco_bwDeployer
REQUIRED_RTVS = ['JAVA_HOME', 'EAI_NUMBER', 'APP_ID']
REGISTERED_ACTIONS = ['clean', 'wipeout', 'start', 'stop', 'install', 'stage', 'ml', 'machine_list',
                      'deploy', 'publish', 'unpublish', 'restart', 'report', 'ssh', 'inspect', 'ps', 'cloudenv','status','relink','properties']
TARGET_URL_SPEC=DEFAULT_TARGET_SPEC

class java_autoDeployer(baseGenerator):
    def __init__(self, component, cli_options, stage_directory):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_java_auto_generator.REGISTERED_ACTIONS
        super(java_autoDeployer, self).__init__(
            component, cli_options, stage_directory)
        fdeploy.LOGGER.debug(
            " < init bash-generator java_autoDeployer platform for " + str(self.component))

class java_autoGenerator(baseGenerator):

    files = []

    # JAVA_AUTO
    remote_create_scripts_snippet = load_snippet('java_auto/remote_create_scripts_snippet.sh')


    def __init__(self, component, cli_options, stage_directory='.', target_spec = DEFAULT_TARGET_SPEC):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_java_auto_generator.REGISTERED_ACTIONS
        self.REQUIRED_RTVS =  fdeploy.platform.bash_java_auto_generator.REQUIRED_RTVS

        super(java_autoGenerator, self).__init__(
            component, cli_options, stage_directory, target_spec)
        fdeploy.LOGGER.debug(
            " < init platform: bash-generator, class: java_autoGenerator  for " + str(self.component))
        # overwrite stage snippet begin
        self.remote_stage_snippet_begin = load_snippet('java_auto/remote_stage_snippet_begin.sh')


    def list_machines(self, struct,target_list,search=None):
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        for target in __targets:
            _target = target.split(":")[0]
            if self.options.user is not None:
                _target = re.sub(
                    r'^(\S+)\@', self.options.user + str('@'), _target)
            if not _target in target_list:
                target_list.append(_target)

    """Generate scripts for the components on the targets in the descriptors."""
    def generate(self, struct, outfile, action='status', target_spec=None,verbose=False):
        # ssh_options
        level = struct['level']
        # replace values in the snippets
        target_list = ''
        upload_list = ''
        envvar_list = ''
        rtv_map = {}
        app_user=None
        # pick the first Nexus resolved component artifact.
        nexusContent = self.get_first_nexus_component_in_content(_needs_nexus_resolving(action,False))

        # agregation of steps.
        actions = []
        if action == 'deploy':
            actions.append('stop')
            actions.append('install')
            actions.append('restart')
        elif action == 'clean' and not self.component.versions_to_persist is None and len(self.component.versions_to_persist) > 1:
            self.component.rtvMapping['CANDIDATES']=','.join(self.component.versions_to_persist)
            actions.append('clean_with_manifest')
        else:
            actions.append(action)

        #
        # resolving the runtime variables (rtv), first global then components.
        #
        #print "  %s \n  %s" % (struct,nexusContent)
        rtv_map,envvar_list=self.component.get_env_vars(self.options,level['level'], self.REQUIRED_RTVS, nexusContent.version)
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        fdeploy.LOGGER.debug("hosts : %s spec %s"  % (str(__targets), target_spec))
        target,target_list=self.get_target_list(__targets, target_spec)
        if len(target_list) < 2:
            raise Exception("targetlist empty? %s" % (struct['targets']))
        #
        # generate the upload list based on the contents specified
        #
        upload_list=self.get_upload_list(struct,nexusContent.version)
        #
        # applying execute identity if applicable
        #
        pki_identiy = ' ' if self.options.identity is None or self.options.identity == '' else '-i ' + \
            str(self.options.identity)
        verboseOn = "set -xv"
        verboseOff = "set +xv"
        verboseFlag = verboseOff if verbose == False else verboseOn

        # NO WAIT FLAG
        nowaitflag = '' if self.options.no_wait == False else '#SKIP#'

        log_array = fdeploy.bash_array()
        fdeploy.LOGGER.trace("log_array=" + str(log_array))


        fdeploy.LOGGER.info( "generating for %s individual actions" % (len(actions)) )


        for _action in actions:
            # only echo the env vars on certain actions.
            echoEnvVars = verboseOff if _action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING else verboseFlag
            #
            # find the application user, assuming the user being supplied is the cm user
            # based on the last processed target
            #
            app_user = self.get_app_user(target,app_user,_action)

            print ( self.data_wrapper_header % (
                self.options.command,
                _action,
                self.options.level,
                log_array[0], log_array[1], log_array[2], log_array[3],
                _action,
                pki_identiy,
                self.ssh_options,
                echoEnvVars,
                verboseFlag,
                app_user,
                struct['uuid'],
                nowaitflag,
                '20', #timeout
                target_list,
                upload_list,
                upload_list,
                self.stage_directory,
                nexusContent.version,
                nexusContent.saveArchiveName,
                nexusContent.artifactId,
                '%'
            ),file=outfile)

            data = []
            _remote_script_begin= self.remote_script_begin % (
                log_array[0], log_array[1], log_array[2], log_array[3],
                envvar_list, verboseFlag)
            _remote_properties_snippet = self.remote_properties_snippet % (envvar_list)
            fdeploy.LOGGER.info("generating for [%s] as %s" % ( _action, app_user))
            self.write(outfile,_action,data,_remote_script_begin,
                _remote_properties_snippet, self.readme_info(struct, nexusContent))
        if _action not in ['test', 'inspect', 'ssh', 'ps','cloudenv']:
            print( self.data_wrapper_execute,file=outfile)
