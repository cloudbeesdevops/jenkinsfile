# -*- coding: utf-8 -*-
from __future__ import print_function
import fdeploy
import os
import fdeploy.platform
import fdeploy.fdeployLoader
from fdeploy.platform import load_snippet, baseGenerator, basePcfGenerator
from fdeploy.platform import DEFAULT_TARGET_SPEC

REQUIRED_RTVS = ['EAI_NUMBER','PAM_ID']
REGISTERED_ACTIONS = ['clean', 'stage', 'install']

def command_script_builder(outfile, envvar_list, services, action, authentication_header=None):
    commands = ['#!/bin/bash']
    for __export in envvar_list.split("\n"):
        commands.append(__export)
    if authentication_header:
        commands.append( authentication_header )
    if len(services) == 0:
        fdeploy.LOGGER.warn("No cups services found for this component.")
    for __service in services:
        fdeploy.LOGGER.info("generating for [%s] " % ( __service ))
        commands.append(__service.toCommand(action))
    # generate all the lines
    for script_line in commands:
        fdeploy.LOGGER.debug(script_line)
        print(script_line,file=outfile)



class pcf_cupsDeployer(baseGenerator):
    def __init__(self, component, cli_options, stage_directory):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_pcf_cups_generator.REGISTERED_ACTIONS
        super(pcf_cupsDeployer, self).__init__(
            component, cli_options, stage_directory)
        fdeploy.LOGGER.debug(
            " < init bash-generator pcf_cupsDeployer platform for " + str(self.component))

    def report(self,__reports):
        pass

class pcf_cupsGenerator(basePcfGenerator):

    files = []
    upload_script = load_snippet('pcf/upload_snippet.sh')
    resolve_password = True

    def __init__(self, component, cli_options, stage_directory='.', target_spec = DEFAULT_TARGET_SPEC, resolve_password=True):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_pcf_cups_generator.REGISTERED_ACTIONS
        self.REQUIRED_RTVS =  fdeploy.platform.bash_pcf_cups_generator.REQUIRED_RTVS
        self.resolve_password = resolve_password

        super(pcf_cupsGenerator, self).__init__(
            component, cli_options, stage_directory, target_spec)
        fdeploy.LOGGER.debug(
            " < init platform: bash-generator, class: pcf_cupsGenerator  for " + str(self.component))
        # overwrite stage snippet begin
        self.data_authentication = load_snippet('pcf/pcf_authentication.sh')
        self.data_wrapper_header = load_snippet('pcf/pcf_header_snippet.sh')
        self.data_wrapper_footer = load_snippet('pcf/pcf_footer_snippet.sh')

    def list_machines(self, struct,target_list,search=None):
        pass

    """Generate scripts for the components on the targets in the descriptors."""
    def generate(self, struct, outfile, action='status', verbose=False):
        level = struct['level']
        # replace values in the snippets
        target_list = ''
        envvar_list = ''

        #
        # resolving the runtime variables (rtv), first global then components.
        #
        rtv_map={}
        target, target_list, rtv_map, envvar_list = self.rtv_mapping(
            action, rtv_map, level, struct['uuid'], struct['targets'],
            self.target_spec, self.REQUIRED_RTVS, "NA")
        fdeploy.LOGGER.debug("target => %s " % (target))

        fdeploy.LOGGER.debug( "-> struct     =%s" % (struct))
        fdeploy.LOGGER.debug( "-> rtv_map    =%s" % (rtv_map))
        fdeploy.LOGGER.debug( "-> envvar_list=%s" % (envvar_list))

        verboseOn = "set -xv"
        verboseOff = "set +xv"
        verboseFlag = verboseOff if verbose == False else verboseOn

        # NO WAIT FLAG

        log_array = fdeploy.bash_array()
        fdeploy.LOGGER.debug("log_array=" + str(log_array))

        # agregation of steps.
        actions = []
        actions.append(action)

        _environ = dict(os.environ)  # or os.environ.copy()
        try:
            for r in rtv_map.keys():
                os.environ[r]=rtv_map[r]
            command_script_builder(outfile, envvar_list, self.component.services, action, self.data_authentication)
        finally:
            os.environ.clear()
            os.environ.update(_environ)
