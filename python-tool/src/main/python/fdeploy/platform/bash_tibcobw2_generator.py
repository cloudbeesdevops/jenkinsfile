# -*- coding: utf-8 -*-
from __future__ import print_function
import os
import os.path
import copy
import datetime
import fdeploy
import fdeploy.deployer
import fdeploy.platform
from fdeploy.platform.bash_tibcobw_generator import tibco_bwGenerator, tibco_bwDeployer
from fdeploy.platform import DEFAULT_TARGET_SPEC
from fdeploy.deployer.TibcoBW5Configurer import TibcoBW5Configurer

REQUIRED_RTVS = ['TRA_HOME', 'JMX_PORT', 'PROCESS_NAME', 'ADMIN_TARGET_URL',
    'EAINUMBER', 'APPMANAGE_CREDENTIALS', 'APPMANAGE_XML', 'APPMANAGE_PLAN','FILTERS']
REGISTERED_ACTIONS = ['clean', 'wipeout', 'start', 'stop', 'install', 'stage', 'uninstall',
        'deploy', 'publish', 'unpublish', 'restart', 'ssh', 'inspect', 'ps', 'cloudenv', 'relink']
TARGET_URL_SPEC = DEFAULT_TARGET_SPEC


class tibco_bw2Generator(tibco_bwGenerator):

    files = []

    def __init__(self, component, cli_options, stage_directory='.', target_spec=DEFAULT_TARGET_SPEC):
        self.REGISTERED_ACTIONS = fdeploy.platform.bash_tibcobw2_generator.REGISTERED_ACTIONS
        self.REQUIRED_RTVS = fdeploy.platform.bash_tibcobw2_generator.REQUIRED_RTVS
        super(tibco_bw2Generator, self).__init__(
            component, cli_options, stage_directory, target_spec)
        self.ssh_options = '-oConnectTimeout=20 -oStrictHostKeyChecking=no \
-oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null'
        fdeploy.LOGGER.debug(
            " < init platform: bash-generator, class: tibco_bw2Generator  for " + str(self.component.prettyprint()))

    def readFilters(self, filterChain=None, base_filter=None, directory=None):
        mapping = {}
        if base_filter:
            mapping = copy.deepcopy(base_filter)
        if filterChain:
            for filter_file in filterChain.split(','):
                if directory and filter_file.startswith('/') == False :
                    filter_file = "%s/%s" % (directory, filter_file)
                filter_file = os.path.abspath(filter_file)
                map=fdeploy.load_properties(filter_file)
                for key in map.keys():
                    mapping[key]=fdeploy.resolve_filter(map[key])
        # resolve nested statements
        for value in mapping.values():
            if '${' in value:
                for key in mapping.keys():
                    if '${' in mapping[key]:
                        mapping[key]
                        mapping[key]=fdeploy.resolve_filter(mapping[key], mapping)
        return mapping


    # def needs_nexus_version(self,action):
    #     retValue = True
    #     if 'ssh' == action or 'unpublish' == action:
    #         retValue = False
    #     return retValue

    def update_deployment_desciptor(self, configurer, __targets, rtv_map, level):
        configurer.applyBindings(__targets, rtv_map,level)
        return configurer.write(pretty=True)

    """Generate scripts for the components on the targets in the descriptors."""
    def generate(self, struct, outfile, action='status',target_spec=None, verbose=False):
        levelStruct = struct['level']
        # replace values in the snippets
        target_list = ''
        tlm_list = ''
        upload_list = ''
        envvar_list = ''
        app_user = None
        # pick the first Nexus resolved component artifact.
        nexusContent = self.get_first_nexus_component_in_content(self.needs_nexus_version(action))
        #
        # resolving the runtime variables (rtv), first global then components.
        #
         # agregation of steps.
        if action == 'clean' and not self.component.versions_to_persist is None and len(self.component.versions_to_persist) > 1:
            self.component.rtvMapping['CANDIDATES']=','.join(self.component.versions_to_persist)
            _action = 'clean_with_manifest'
       
        rtv_map = {}
        rtv_map, envvar_list = self.component.get_env_vars(
                self.options, levelStruct['level'], self.REQUIRED_RTVS, nexusContent.version)
        tlm_list = [rtv_map['ADMIN_TARGET_URL']]
        fdeploy.LOGGER.debug("hosts %s# : %s" % (len(tlm_list), tlm_list))
        target, target_list = self.get_target_list(tlm_list, target_spec)
        __targets = fdeploy.flatten_level_targets(struct['targets'])
        fdeploy.LOGGER.info("\n\n %s\n%s\n%s" % (__targets, target, target_list))
        base_dir = rtv_map['ADMIN_TARGET_URL'].split("@")[1]

        # print "\n\n\n"
        # print self.__dict__
        # print "\n\n\n"
        # print str(struct)
        # print "\n\n\n"

        # configuration and scaling
        filters = {}
        if 'FILTERS' in rtv_map:
            filters = self.readFilters(rtv_map['FILTERS'], rtv_map, struct['directory'])
        deployment = rtv_map['APPMANAGE_XML'] if rtv_map['APPMANAGE_XML'].startswith(
            '/') else "%s/%s" % (struct['directory'], rtv_map['APPMANAGE_XML'])
        if not os.path.isfile(deployment):
            raise Exception("deployment xml %s does not exist" % (deployment))

        #
        # generate the upload list based on the contents specified
        #
        upload_list = self.get_upload_list(struct, nexusContent.version)

        #
        # applying execute identity if applicable
        #
        pki_identiy = ' ' if self.options.identity is None or self.options.identity == '' else '-i ' + \
            str(self.options.identity)
        verboseOff = "set +xv"
        verboseFlag = ''

        # NO WAIT FLAG
        nowaitflag = '' if self.options.no_wait == False else '#SKIP#'

        # only echo the env vars on certain actions.
        echoEnvVars = verboseOff if action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING else verboseFlag

        log_array = fdeploy.bash_array()
        fdeploy.LOGGER.trace("log_array=" + str(log_array))
        fdeploy.LOGGER.trace("machine list: %s" % (target_list))

        _action = action
        app_user = self.get_app_user(target,app_user,_action)
        fdeploy.LOGGER.info("tibco_app_user[%s]=%s" % (_action, app_user))

        if action in ['start','stop' ,'status','restart'] and nexusContent.version is None:
            nexusContent.version='current'

        if ':' in rtv_map['APPMANAGE_CREDENTIALS']:
            args = ['-user', '-pw']
            i = 0
            for _key in rtv_map['APPMANAGE_CREDENTIALS'].split(':'):
                _appmanage_creds = "%s %s %s" % (APPMANAGE_CREDS, args[i], _key)
                i += 1
        else:
            base_dir = base_dir.split(':')[1]
            _appmanage_creds='-cred %s/%s' % (str(base_dir),rtv_map['APPMANAGE_CREDENTIALS'])

        # compilation of the deployment plan based on the template and plan stored with the
        #   deployment items.
        if _action in ['install', 'deploy', 'stage']:
            configurer = TibcoBW5Configurer(deployment, filters)
            deployment_config = self.update_deployment_desciptor(configurer, __targets, filters, levelStruct['level'])
            configurer.replaceConfig(nexusContent, deployment_config)

        if action in ['start','stop' ,'status','restart'] and nexusContent.version is None:
            nexusContent.version='current'

        #print "\n\n\t\t %s \n\n" % (_appmanage_creds)

        print (self.data_wrapper_header % (
            self.options.command,
            _action,
            self.options.level,
            log_array[0], log_array[1], log_array[2], log_array[3],
            _action,
            pki_identiy,
            self.ssh_options,
            echoEnvVars,
            '', # verbose flag
            app_user,
            struct['uuid'],
            nowaitflag,
            "150", #timeout
            _appmanage_creds, #appmange credentials
            target_list,
            upload_list,
            upload_list,
            self.stage_directory,
            nexusContent.version,
            nexusContent.saveArchiveName,
            nexusContent.artifactId,
            '%'
        ),file=outfile)

        data = []
        _remote_script_begin = self.remote_script_begin % (
            log_array[0], log_array[1], log_array[2], log_array[3],
            envvar_list, verboseFlag)
        _remote_appmanage_stage_snippet_begin = self.remote_appmanage_stage_snippet_begin % (envvar_list)
        readme = self.remote_readme_snippet % (self.readme_info(struct,nexusContent))
        _remote_audit_snippet = self.remote_audit_snippet % (datetime.datetime.now(), _action, self.readme_info(struct,nexusContent))

        if _action == 'stage':

            data.append({'upload':self.upload_snippet})
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'readme':readme})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'appmanage_stage_snippet_begin':_remote_appmanage_stage_snippet_begin})
            data.append({'relink': self.remote_relink_snippet})
            data.append({'remote_create_scripts':self.remote_create_scripts_snippet})
            data.append({'appmanage_stage_end':self.remote_appmanage_stage_snippet_end})
            data.append({'remote_script_end': self.remote_script_end})

        elif _action == 'install':

            data.append({'upload':self.upload_snippet})
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'readme':readme})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'appmanage_stage_snippet_begin':_remote_appmanage_stage_snippet_begin})
            data.append({'relink': self.remote_relink_snippet})
            data.append({'remote_create_scripts':self.remote_create_scripts_snippet})
            data.append({'undeploy':self.remote_appmanage_undeploy_snippet})
            # same as 'deploy' but includes the start
            data.append({'install':self.remote_appmanage_install_snippet})
            data.append({'appmanage_stage_end':self.remote_appmanage_stage_snippet_end})
            data.append({'remote_script_end':self.remote_script_end})

        elif _action == 'uninstall':

            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_create_scripts':self.remote_create_scripts_snippet})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'stop':self.remote_stop_snippet})
            data.append({'appmanage_delete':self.remote_appmanage_delete_snippet})
            data.append({'remote_script_end':self.remote_script_end})

        elif _action == 'deploy':

            data.append({'upload':self.upload_snippet})
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'readme':readme})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'appmanage_stage_snippet_begin':_remote_appmanage_stage_snippet_begin})
            data.append({'relink': self.remote_relink_snippet})
            data.append({'remote_create_scripts':self.remote_create_scripts_snippet})
            data.append({'undeploy':self.remote_appmanage_undeploy_snippet})
            # same as 'install' but includes the start
            # data.append({'deploy':self.remote_appmanage_deploy_snippet})
            data.append({'install':self.remote_appmanage_install_snippet})
            data.append({'appmanage_stage_end':self.remote_appmanage_stage_snippet_end})
            data.append({'remote_start':self.remote_appmanage_start_snippet})
            data.append({'remote_script_end':self.remote_script_end})

        elif _action == 'delete':
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'appmanage_delete':self.remote_appmanage_delete_snippet})
            data.append({'remote_script_end':self.remote_script_end})

        elif _action == 'restart':
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_stop':self.remote_stop_snippet})
            data.append({'remote_start':self.remote_appmanage_start_snippet})
            data.append({'remote_script_end':self.remote_script_end})
        elif _action == 'stop':
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_stop':self.remote_appmanage_stop_snippet})
            data.append({'remote_script_end':self.remote_script_end})
        elif _action == 'start':
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_stop':self.remote_appmanage_stop_snippet})
            data.append({'remote_start':self.remote_appmanage_start_snippet})
            data.append({'remote_script_end':self.remote_script_end})
        elif _action == 'run':
            raise Exception("run not available for TIBCO BW")
        elif _action == 'relink':
            data.append({'remote_script_begin':_remote_script_begin})
            data.append({'remote_audit':_remote_audit_snippet})
            data.append({'remote_relink':self.remote_relink_snippet})
            data.append({'remote_script_end':self.remote_script_end})
        elif _action == 'clean':
            data.append(_remote_script_begin)
            data.append(self.remote_cleanup_snippet)
            data.append(self.remote_script_end)
        elif _action == 'clean_with_manifest':
            data.append(_remote_script_begin)
            data.append(self.remote_cleanup_with_manifest_snippet)
            data.append(self.remote_script_end)
        # elif _action == 'wipeout':
        #     data.append(_remote_script_begin)
        #     data.append(self.remote_wipeout_snippet)
        #     data.append(self.remote_script_end)
        elif _action == 'ssh':
            data.append({'ssh_pki':self.pki_ssh_snippet})
            data.append({'done':self.done_deal_snippet})
        elif _action == 'inspect':
            data.append(self.inspect_snippet)
            data.append(self.done_deal_snippet)
        elif _action == 'ps':
            data.append(self.ps_test_snippet)
            data.append(self.done_deal_snippet)
        elif _action == 'cloudenv':
            data.append(self.cloudenv_snippet)
            data.append(self.done_deal_snippet)

        # generate all the lines
        for script_line in data:
            for key in script_line:
                line = "writing script line [%-35s]" % (key)
                fdeploy.LOGGER.info(line)
            print( script_line[key], file=outfile)
        # if test no footer needed the test is selfcontanted and
        # does not need script streaming
        if _action not in ['test', 'inspect', 'ssh', 'ps','cloudenv','1install','2install-undeploy','3install-deploy']:
            print( self.data_wrapper_footer, file=outfile)
            print( self.data_wrapper_execute, file=outfile)

class tibco_bw2Deployer(tibco_bwDeployer):

    def __init__(self, component, cli_options, stage_directory):
        self.REGISTERED_ACTIONS =  fdeploy.platform.bash_tibcobw2_generator.REGISTERED_ACTIONS
        super(tibco_bw2Deployer, self).__init__(
            component, cli_options, stage_directory)
        fdeploy.LOGGER.debug(
            " < init bash-generator tibco_bw2Deployer platform for " + str(self.component))
