from fdeploy.UtilsForTesting import assertPropertiesGeneration
from fdeploy.UtilsForTesting import get_options

class TestTibcoASGenerator(object):

# //'TIBCO_HOME','JMX_PORT','AS_METASPACE', 'AS_HOME','AS_LISTEN_PORT','AS_INPUT',
#         'AS_DISCOVERY', 'AS_LOG','AS_MEMBER_NAME_PREFIX','AS_DATA_STORE', 'LOG_DIR'

    properties =  '''   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
export APPD_ACCOUNT="fedex1-test"
export APPD_ACCOUNT_KEY="1dce5fc52c17"
export APPD_APPNAME="SU-SEFS-RELY"
export APPD_CONTROLLER_HOST="fedex1-test.saas.appdynamics.com"
export ARTIFACTID="SDSActiveSpace"
export AS_DATA_STORE="/opt/asfiles/1"
export AS_DISCOVERY="tcp://172.21.28.36:11751;172.21.28.37:11751"
export AS_HOME="/opt/tibco/ra101/as/2.1"
export AS_INPUT="SDS-SpaceDefinitions.msd"
export AS_LISTEN_PORT="11751"
export AS_LOG="/var/fedex/sefs/logs/fxg_ActiveSpaceSDS01_std.log"
export AS_MEMBER_NAME_PREFIX="SDS-FXG_SDS1"
export AS_METASPACE="SDS"
export EAINUMBER="6926"
export GROUPID="com.fedex.ground.sefs"
export JAVA_HOME="/opt/java/hotspot/8/current"
export JMX_PORT="31154"
export LEVEL="L1"
export LOG_DIR="/var/fedex/sefs/logs"
export TIBCO_HOME="/opt/tibco/ra101"
export VERSION="3.0.0-SNAPSHOT"

EOF
'''

    def test_properties_creation(self):
        options = get_options()
        options.overwrite(command='deploy',action='properties',identity='.',
        X=True, id = None, path=".", no_wait = False, level='L1',
        user='sefs')
        assertPropertiesGeneration(options,'tibco_asGenerator',
        'AS_FXG_SDSActiveSpace_%0d',
        '../../test/resources/FXE_fdeploy/fxe-as-sds.json', self.properties)

if __name__ == '__main__':
    unittest.main()
