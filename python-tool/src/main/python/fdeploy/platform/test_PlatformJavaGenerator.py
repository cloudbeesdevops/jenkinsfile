from fdeploy.UtilsForTesting import  assertPropertiesGeneration
from fdeploy.UtilsForTesting import get_options, compare_files
from fdeploy import Options
from fdeploy.fdeployLoader import fdeployLoader, create_manifest_resolver


class TestJavaGenerator(object):

    properties =  '''   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
export APPD_ACCOUNT="fedex1-test"
export APPD_APPNAME="SU-SEFS-RELY-L1"
export APPD_CONTROLLER_HOST="fedex1-test.saas.appdynamics.com"
export ARTIFACTID="share-cache-plugin"
export EUREKA_HOST="urh00600.ute.fedex.com"
export GROUPID="com.fedex.sefs.cache.plugins"
export HTTP_PORT="8096"
export LEVEL="L1"
export SPRING_PROFILE="L1"

EOF
'''
    options = Options(command='deploy',action='deploy',identity='.', X=False, id = None,
        path="tmp", no_wait = False, level='L1',user='sefs')

    def xtest_properties_creation(self):
        options = get_options()
        options.overwrite(command='deploy',action='properties',identity='.',
        X=True, id = None, path=None, no_wait = False, level='L1',
        user='sefs')
        assertPropertiesGeneration(options,'javaGenerator',
            'JAVA_SHARE_CACHE_SERVICE_%0d',
            '../../test/resources/FXE_fdeploy/fxe-share-cache-plugin.json',self.properties)

    def test_clean_with_persist(self):
        options = get_options()
        options.overwrite(command='deploy',action='clean',identity='.',
        X=True, id = None, path=None, no_wait = False, level='L1',
        user='sefs')
        assertPropertiesGeneration(options,'javaGenerator',
            'JAVA_SHARE_CACHE_SERVICE_%0d',
            '../../test/resources/FXE_fdeploy/fxe-share-cache-plugin.json',self.properties)


    def test_deploy_deploy(self):
        manifest_resolver = create_manifest_resolver('../../test/resources/FXE_fdeploy', 'L1')
        o=get_options()
        o.command = 'deploy'
        o.action = 'deploy'
        o.level = 'L1'
        o.identity = '/home/ak751818/.ssh/test_deploy_user_dsa'
        fdl = fdeployLoader(o)
        fdl.readDescriptors(
            ['../../test/resources/FXE_fdeploy/fxs-dashboard-java.json'])
        fdl.generate_level('L1')
        compare_files('../../test/resources/java/myscript-deploy-deploy.sh','tmp/myscript.sh')


if __name__ == '__main__':
    unittest.main()
