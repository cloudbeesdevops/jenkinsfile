
import fdeploy
import copy
import pytest
from fdeploy.platform import DEFAULT_TARGET_SPEC
from fdeploy.platform import URL_URI_TARGET_SPEC, anti_regex_pattern, baseGenerator, basePcfGenerator
from fdeploy.content.fdeployComponent import fdeployComponent
from fdeploy.UtilsForTesting import get_options
from fdeploy.fdeployLoader import fdeployLoader

REQUIRED_RTVS = []
REGISTERED_ACTIONS = ['clean', 'wipeout', 'start', 'stop', 'install', 'stage', 'ml', 'machine_list',
        'deploy', 'publish', 'unpublish', 'restart', 'report', 'ssh', 'inspect', 'ps', 'cloudenv','status']

class testGenerator(baseGenerator):
    def __init__(self, component, cli_options):
        self.REGISTERED_ACTIONS =  fdeploy.platform.test_BashGeneratorMethods.REGISTERED_ACTIONS
        self.REQUIRED_RTVS = fdeploy.platform.test_BashGeneratorMethods.REQUIRED_RTVS
        super(testGenerator, self).__init__(
            component, cli_options)


class testPcfGenerator(basePcfGenerator):
    resolve_password = False
    def __init__(self, component, cli_options):
        self.REGISTERED_ACTIONS =  fdeploy.platform.test_BashGeneratorMethods.REGISTERED_ACTIONS
        self.REQUIRED_RTVS = fdeploy.platform.test_BashGeneratorMethods.REQUIRED_RTVS
        super(testPcfGenerator, self).__init__(
            component, cli_options, target_spec=DEFAULT_TARGET_SPEC)

def createComp():
    return fdeployComponent({
    'id': 'componentId',
    'platform': 'bash_generator',
    'name_format' : 'format',
    'type': 'tibco_bw2',
    'filter' :
        { "var1" : "value1", "var2" : "value2"}
    ,
    'content':  [{
        "gav": "com.fedex.sefs.core:sefs_suCLEARANCEFeed:zip",
        "relativePath": "${level}/${var1}.sh",
        "saveArchiveName": "sefs_suCLEARANCEFeed.zip"
    }],
    'name_format': 'format',
    'levels':
    [{
        'level': 'L1',
        'filter' : { 'var1' : 'value2'},
        'targets' : [ 'a@com', 'b@com', 'c@com', 'd@com', 'e@com']
    },{
        'level': 'L2',
        'filter' : { 'var1' : 'value2'},
        'targets' : [ 'a@com', 'b@com', 'c@com', 'd@com', 'e@com','f@com', 'g@com', 'h@com', 'i@com', 'j@com']
    },{
        'level': 'L3',
        'filter' : { 'var1' : 'value2'},
        'targets' : [ 'a@com']
    }]

}, 'inline.json')


class TestBashGeneratorMethods(object):

    def test_rtv_global_reading(self):
        options = copy.deepcopy(get_options())
        fdl = fdeployLoader(options)
        fdl.readDescriptors(['../../test/resources/java-cache-mixin.json'])
        component = fdl.components[0]
        testGenerator(component, options )
        rtv_map={}
        assert 0 ==  len(rtv_map.keys())
        assert 2 == len(component.rtv)
        rtvs=component.get_rtvs(rtv_map, component.rtv)
        #rtvs=base.get_rtvs(rtv_map,component.rtv)
        fdeploy.LOGGER.debug(rtv_map.keys())
        assert 2 ==  len(rtv_map.keys())

        assert '/opt/home/gbs' ==  rtv_map['G_HOME']

        # overwrite global RTVs
        rtvs=component.get_rtvs(rtv_map,[{"name": "G_HOME", "value" : "/home/f5149900"}])
        assert '/home/f5149900' ==  rtv_map['G_HOME']

    def test_rtv_values_with_appnm_globally(self):
        options = copy.deepcopy(get_options())
        options.level = 'L2'
        fdl = fdeployLoader(options)
        fdl.readDescriptors(['../../test/resources/pcf/performance-entity-identifier-service-appnm-override.json'])
        comp = fdl.components[0]
        generator = testPcfGenerator(comp, options)
        rtv_map={}
        assert 0 ==  len(rtv_map.keys())
        assert 11    == len(comp.rtv)
        levelStruct =  fdl.find_level(comp,options.level)
        targets = fdl.find_targets_in_level(comp, levelStruct)
        target, target_list, rtv_map, envvar_list = generator.rtv_mapping(
            'action', rtv_map, levelStruct, comp.id, targets,
            generator.target_spec, generator.REQUIRED_RTVS, None)
        assert rtv_map['NFR_TEST'] == "sefsapp1Thisisaverylongpwd"
        assert rtv_map['APP_NM'] == "NIX_FIXED_NAME"
        assert rtv_map['LEVEL'] == "L2"

    def test_rtv_values_with_appnm(self):
        options = copy.deepcopy(get_options())
        options.level = 'L1'
        fdl = fdeployLoader(options)
        fdl.readDescriptors(['../../test/resources/pcf/performance-entity-identifier-service-appnm-override.json'])
        comp = fdl.components[0]
        generator = testPcfGenerator(comp, options)
        rtv_map={}
        assert 0 ==  len(rtv_map.keys())
        assert 11    == len(comp.rtv)
        levelStruct =  fdl.find_level(comp,options.level)
        targets = fdl.find_targets_in_level(comp, levelStruct)
        target, target_list, rtv_map, envvar_list = generator.rtv_mapping(
            'action', rtv_map, levelStruct, comp.id, targets,
            generator.target_spec, generator.REQUIRED_RTVS, None)
        assert rtv_map['NFR_TEST'] == "nfr_test_into_L1"
        assert rtv_map['APP_NM'] == "NY_FIXED_NAME"
        assert rtv_map['LEVEL'] == "L1"


    def test_rtv_values_with_encryption(self):
        options = copy.deepcopy(get_options())
        options.level = 'L2'
        fdl = fdeployLoader(options)
        fdl.readDescriptors(['../../test/resources/pcf/performance-entity-identifier-service.json'])
        comp = fdl.components[0]
        generator = testPcfGenerator(comp, options)
        rtv_map={}
        assert 0 ==  len(rtv_map.keys())
        assert 11    == len(comp.rtv)
        levelStruct =  fdl.find_level(comp,options.level)
        targets = fdl.find_targets_in_level(comp, levelStruct)
        target, target_list, rtv_map, envvar_list = generator.rtv_mapping(
            'action', rtv_map, levelStruct, comp.id, targets,
            generator.target_spec, generator.REQUIRED_RTVS, None)
        assert rtv_map['NFR_TEST'] == "sefsapp1Thisisaverylongpwd"
        assert rtv_map['APP_NM'] == "L2-performance-entity-identifier-service"

    def test_rtv_values_with_encryption_l1(self):
        options = copy.deepcopy(get_options())
        fdl = fdeployLoader(options)
        fdl.readDescriptors(['../../test/resources/pcf/performance-entity-identifier-service.json'])
        comp = fdl.components[0]
        generator = testPcfGenerator(comp, options)
        rtv_map={}
        levelStruct =  fdl.find_level(comp,'L1')
        targets = fdl.find_targets_in_level(comp, levelStruct)
        target, target_list, rtv_map, envvar_list = generator.rtv_mapping(
            'action', rtv_map, levelStruct, comp.id, targets,
            generator.target_spec, generator.REQUIRED_RTVS, None)
        assert rtv_map['NFR_TEST'] == "nfr_test_into_L1"

    def test_rtv_values_with_encryption_l3(self):
        options = copy.deepcopy(get_options())
        fdl = fdeployLoader(options)
        fdl.readDescriptors(['../../test/resources/pcf/performance-entity-identifier-service.json'])
        comp = fdl.components[0]
        generator = testPcfGenerator(comp, options)
        rtv_map={}
        levelStruct =  fdl.find_level(comp,'L3')
        targets = fdl.find_targets_in_level(comp, levelStruct)
        target, target_list, rtv_map, envvar_list = generator.rtv_mapping(
            'action', rtv_map, levelStruct, comp.id, targets,
            generator.target_spec, generator.REQUIRED_RTVS, None)
        assert rtv_map['NFR_TEST'] == "nfr_top_level_rtv"

    def test_rtv_env_vars_reading(self):
        options = copy.deepcopy(get_options())
        fdl = fdeployLoader(options)
        fdl.readDescriptors(['../../test/resources/java-cache-mixin.json'])
        component = fdl.components[0]
        testGenerator(component, options )
        fdeploy.LOGGER.debug("\\\->\\\\>>> %s" , component.rtv)
        options.overwrite(
            rtv= ["G_HOME=/home/f5149900","JAVA_HOME=/opt/java/hotspot"]
        )
        mapping,envvar_list=component.get_env_vars(options,'L1',[],'nexusContent.buildId')
        fdeploy.LOGGER.debug(component.rtvMapping)
        assert 7 ==  len(component.rtvMapping.keys())
        # taken from the Level L1 rtv's
        assert '/opt/delta/DL2180/Lihue' ==  component.rtvMapping['F_HOME']
        assert '/home/f5149900' ==  component.rtvMapping['G_HOME']
        assert '/opt/java/hotspot' ==  component.rtvMapping['JAVA_HOME']

    def test_no_rtv_env_vars_reading(self):
        options = copy.deepcopy(get_options())
        test_component = createComp()
        testGenerator(test_component, options )
        struct = {'contents': 'comp.contents',
            'targets': 'targets','level': 'L1', 'uuid': 'comp.id'
         }
        rtv_map={}
        unused,envvar_list=test_component.get_env_vars(
            options,'L1',[],'nexusContent.buildId')
        assert 0 ==  len(rtv_map.keys())

    def test_find_first_component(self):
        options = copy.deepcopy(get_options())
        test_component = createComp()
        base = testGenerator(test_component, options)
        with pytest.raises(Exception):
            com = base.get_first_nexus_component_in_content()
        com = base.get_first_nexus_component_in_content(False)
        assert None ==  com.version
        assert 'com.fedex.sefs.core' ==  com.groupId
        assert 'sefs_suCLEARANCEFeed' ==  com.artifactId

    def test_get_targets_all(self):
        options = copy.deepcopy(get_options())
        options.overwrite(user='teatime')
        test_component = createComp()
        base = testGenerator(test_component, options)
        assert type(base.target_spec) == type(URL_URI_TARGET_SPEC)
        assert base.target_spec.pattern == URL_URI_TARGET_SPEC.pattern
        with pytest.raises(Exception) as exp:
            targets = base.get_target_list(test_component.levels[0]['targets'])
        _correct=[]
        for t in test_component.levels[0]['targets']:
            _correct.append("%s:/ipt/var/tmp/" % (t))
        assert 5 == len(_correct)
        targets,targetlist = base.get_target_list(_correct, URL_URI_TARGET_SPEC)
        assert 'teatime@com:/ipt/var/tmp/' == targets[0]
        targets, targetlist = base.get_target_list(['andre@kaan'], DEFAULT_TARGET_SPEC)
        assert 'teatime@kaan' == targets[0]
        base.target_spec=None
        targets, targetlist = base.get_target_list(['andre@kaan'], None)
        assert 'andre@kaan' == targets[0]

    def test_anti_pattern(self):
        regex=anti_regex_pattern(3, URL_URI_TARGET_SPEC.pattern)
        #assert '\(.+\)(.+)\(.+\)(.+)\(.+\)' == regex.pattern
        assert '%s@%s:%s' == regex
        assert 3 == regex.count('%s')
        assert 'a@com:/opt' == regex % ('a','com','/opt')

