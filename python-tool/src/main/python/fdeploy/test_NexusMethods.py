
import os
import pytest
from fdeploy import Options, LOGGER
from fnexus import absolute_repository_path
from fdeploy.content.fdeployComponent import fdeployComponent
from fdeploy.fdeployLoader import fdeployLoader
from fnexus.gavClass import gavClass
from fdeploy.nexusVersionResolver import nexusVersionResolver
from fnexus.manifestResolver import manifestResolver



os.environ['NO_PROXY']='127.0.0.1'
test_component = fdeployComponent({
    'id': 'componentId',
    'platform': 'bash_generator',
    'type': 'java',
    'filter':
        {"var1": "value1", "var2": "value2"},
    'content':  [{
        "gav": "com.fedex.ground.sefs.pd.domainview.coreshipment:fxg-pd-domainview-coreshipment:zip",
        "relativePath": "L1/rel.sh",
        "saveArchiveName": "sefs_suCLEARANCEFeed.zip"
    }],
    'name_format': 'format',
    'levels':
    [{
        'level': 'L1',
        'rtv': [{'name': 'JMX_PORT', 'value': '40313'}],
        'targets': ['a@fedex.com', 'b@fedex.com', 'c@fedex.com', 'd@fedex.com', 'e@fedex.com']
    }],
    'rtv': [
            {'name': 'JAVA_HOME',
             'value': '/opt/java/hotspot'
             },
            {'name': 'JMX_PORT', 'value': 'ABCDEF'},
            {'name': "APPMANAGE_XML", 'value': "/var/tmp/${level}/appmanage.xml"}
    ]

}, 'inline.json')


def reset():
    return gavClass({'gav': 'a:f:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})

def get_options():
    cwd = os.path.abspath("%s/../../test/resources" % (os.getcwd()))
    options = Options(command='control',X=True,action='status', app_group='FXE', ignore_manifest_errors= False,
        level='L1',identity='.',no_wait=True,rtv=[], group=-1, regex=None, group_size=None, group_number=-1,
        defaults={'nexusURL':'file://%s/local' % (cwd),'absURL':'%s/local' % (cwd), 'ldd_url' : 'http://c0009869.test.cloud.fedex.com:8090/sefs-dashboard', 'cwd' : cwd})
    return options

class TestNexusResolver(object):

    resolver = nexusVersionResolver(get_options())
    mani = None

    def setup_method(self, method):
        self.resolver.applyOptions(get_options())
        self.mani = manifestResolver('../../test/resources', 'L1')
        self.resolver.set_manifest_resolver(self.mani)
        LOGGER.info('absurl=%s' % (get_options().defaults['absURL']))
        LOGGER.info('nxsurl=%s' % (get_options().defaults['nexusURL']))
        assert get_options().defaults['nexusURL'] == self.resolver.resolver.nexusURL

    def test_fdeployrc(self):
        options = Options(command='deploy',X=True,action='stage', app_group='FXE',
            level='L1',identity='.',no_wait=True,rtv=[], group=-1, regex=None, group_size=None, group_number=-1,
            defaults={'nexusURL':'https://nexus.prod.cloud.fedex.com:8443', 'releasesRepo' : '5431-ui'})
        self.resolver = nexusVersionResolver(options)
        assert self.resolver.resolver.releasesRepo == '5431-ui'

    def test_absolute_path(self):
        cwd = os.path.abspath('.')
        assert cwd ==  absolute_repository_path('.')
        assert "file://%s" % (cwd) ==  absolute_repository_path('.' ,  True)
        cwd = os.path.abspath('../okay')
        assert "file://" + cwd, absolute_repository_path('file://../okay')
        assert cwd ==  absolute_repository_path('file://../okay', False)
        assert 'http://localhost/api', absolute_repository_path('http://localhost/api')
        assert 'http://localhost/api', absolute_repository_path('http://localhost/api', True)

    def test_nexus_init(self):
        assert get_options().defaults['nexusURL'] ==  self.resolver.resolver.nexusURL
        with pytest.raises(AttributeError):
            self.resolver.resolve(None)

    def test_gav_class_init(self):
        # resolving junit 3.8.1
        gav = gavClass(
            {'gav': 'org.junit:junit:3.8.1:jar', 'saveArchiveName': 'junit.zip'})
        #
        assert 'org.junit' ==  gav.groupId
        assert 'junit' ==  gav.artifactId
        assert '3.8.1' ==  gav.version
        self.resolver.resolve(gav)
        assert '3.8.1' ==  gav.version

    def test_gav_class_with_classifier_init(self):
        # resolving junit 3.8.1
        gav = gavClass(
            {'gav': 'com.fedex.ground:TLABroker:1.0.2-SNAPSHOT:zip:bin', 'saveArchiveName': 'junit.zip'})
        #
        assert 'com.fedex.ground' ==  gav.groupId
        assert 'TLABroker' ==  gav.artifactId
        assert 'bin' ==  gav.classifier
        assert '1.0.2-SNAPSHOT' ==  gav.version
        assert 'zip' ==  gav.packaging
        self.resolver.resolve(gav)
        assert '1.0.2-SNAPSHOT' ==  gav.version

    def test_gav_class_with_version_classifier(self):
        # resolving com.fedex.sefs.core:sefs_CoreParent:5.13.1.Peak
        gav = gavClass(
            {'gav': 'com.fedex.sefs.core:sefs_CoreParent:5.13.1.Peak:zip:bin', 'saveArchiveName': 'Peak.zip'})
        #
        assert 'com.fedex.sefs.core' ==  gav.groupId
        assert 'sefs_CoreParent' ==  gav.artifactId
        assert 'bin' ==  gav.classifier
        assert '5.13.1.Peak' ==  gav.version
        assert 'zip' ==  gav.packaging
        self.resolver.resolve(gav)
        assert '5.13.1.Peak' ==  gav.version

    def test_gav_class_with_classifier_resolve_init(self):
        # resolving junit 3.8.1
        gav = gavClass(
            {'gav': 'com.fedex.ground:TLABroker:zip:bin', 'saveArchiveName': 'junit.zip'})
        #
        assert 'com.fedex.ground' ==  gav.groupId
        assert 'TLABroker' ==  gav.artifactId
        assert 'bin' ==  gav.classifier
        assert None ==  gav.version
        assert 'zip' ==  gav.packaging

    def test_gav_class_resolve_major_minor(self):
        # resolving maven-model 3.2* to 3.2.5
        gav = gavClass({'gav': 'org.apache.maven:maven-model:jar',
                                'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        #
        assert 'org.apache.maven' ==  gav.groupId
        assert 'maven-model' ==  gav.artifactId
        assert None ==  gav.version
        self.resolver.resolve(gav, '3.2')
        assert '3.2.5' ==  gav.version


    def test_resolve_3(self):
        mani = manifestResolver('../../test/resources', 'LZ')
        self.resolver.set_manifest_resolver(mani)
        gav = gavClass(
            {'gav': 'a:a:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert 'a' ==  self.resolver.resolve(gav,  None).artifactId
        assert 'a' ==  self.resolver.resolve(gav,  None).groupId
        assert None is  self.resolver.resolve(gav,  None).version
        gav = gavClass(
            {'gav': 'a:b:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.3.0-SNAPSHOT' == \
                         self.resolver.resolve(gav, None).version
        gav = gavClass(
            {'gav': 'a:c:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.3.1' ==  self.resolver.resolve(gav,  None).version
        gav = gavClass(
            {'gav': 'a:e:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.3.2.MASTER' == self.resolver.resolve(gav, None).version
        #
        gav = gavClass(
            {'gav': 'a:b:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.3.0' ==  self.resolver.resolve(gav,  '1.3').version
        # hard coded
        assert '1.4.0' ==  self.resolver.resolve(gav,  '1.4').version


    def test_resolve_from_manifest(self):

        # nothing available
        #assert '0.0.0' ==  self.resolver.get_artifact_version_from_manifest(None == None)
        mani = manifestResolver('../../test/resources', 'LZ')
        self.resolver.set_manifest_resolver(mani)
        gav = gavClass(
            {'gav': 'a:z:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            None== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        gav = gavClass(
            {'gav': 'a:z:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert self.resolver.resolve_from_manifest(gav).version is None
        #
        gav = gavClass(
            {'gav': 'a:e:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.3.2.MASTER'== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        gav = gavClass(
            {'gav': 'a:e:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.3.2.MASTER', self.resolver.resolve_from_manifest(gav).version

        #
        gav = gavClass(
            {'gav': 'a:a:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.3.0'== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        gav = gavClass(
            {'gav': 'a:a:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.3.0', self.resolver.resolve_from_manifest(gav).version
        #
        #
        gav = gavClass(
            {'gav': 'a:b:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.3.0-SNAPSHOT'== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        gav = gavClass(
            {'gav': 'aaa:b:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert None is self.resolver.resolve_from_manifest(gav,'100.0.0')

        #
        gav = gavClass({'gav': 'a:c:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.3.1'== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        gav = gavClass(
            {'gav': 'a:c:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.3.1', self.resolver.resolve_from_manifest(gav).version

        #
        gav = gavClass(
            {'gav': 'a:d:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.3.1*'== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        # metadata does not have 1.3.1 only 6.0.0-SNAPSHOT
        gav = gavClass(
            {'gav': 'a:d:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert None is self.resolver.resolve_from_manifest(gav).version
        # resolve base vers
        gav = gavClass(
            {'gav': 'a:d:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '6.0.0', self.resolver.resolver.what_is_the_next_version_base(gav, '6.0').version

        #
        gav = gavClass(
            {'gav': 'a:g:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.3.1*'== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        # metadata does not have 1.3.1 only 6.0.0-SNAPSHOT
        gav = gavClass({'gav': 'a:g:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert None is self.resolver.resolve_from_manifest(gav).version

        #
        gav = gavClass(
            {'gav': 'a:e:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.3.2.MASTER'== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        gav = gavClass(
            {'gav': 'a:e:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.3.2.MASTER', self.resolver.resolve_from_manifest(gav).version

    def test_resolve_from_manifest_failure_with_strick_complaince(self):
        # logging.basicConfig(level=logging.DEBUG)
        # fdeploy.LOGGER.setLevel(logging.DEBUG)
        mani = manifestResolver('../../test/resources', 'L1')
        self.resolver.set_manifest_resolver(mani)
        with pytest.raises(Exception):
            gav = gavClass({'gav': 'a:i0:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
            self.resolver.resolve_from_manifest(gav, True).version
        gav = gavClass({'gav': 'a:i:1.1.5:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        self.resolver.resolve_from_manifest(gav, True).version
        assert gav.version is None

    def test_resolve_from_manifest2(self):
        mani = manifestResolver('../../test/resources', 'L1')
        self.resolver.set_manifest_resolver(mani)
        #
        gav = gavClass(
            {'gav': 'a:h:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.5.10.1'== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        gav = gavClass(
            {'gav': 'a:h:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.5.10.1', self.resolver.resolve_from_manifest(gav).version
        gav = gavClass(
            {'gav': 'a:h:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.5.10.1', self.resolver.resolver.what_is_the_next_version_base(gav, '1.5.10').version

        # exact copy of manifest.mf
        gav = gavClass(
            {'gav': 'a:i:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.0.7-SNAPSHOT'== self.resolver.get_artifact_version_from_manifest(gav.artifactId )
        gav = gavClass(
            {'gav': 'a:i:1.0.7:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert \
            '1.0.7', self.resolver.resolve_from_manifest(gav).version
        gav = gavClass(
            {'gav': 'a:i:1.0.5:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.0.5', self.resolver.resolver.what_is_the_next_version_base(gav, '1.0').version
        gav = gavClass(
            {'gav': 'a:i:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.0.7', self.resolver.resolver.what_is_the_next_version_base(gav, '1.0').version
        gav = gavClass(
            {'gav': 'a:i:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.0.7', self.resolver.resolver.what_is_the_next_version_base(gav, '1.0').version

        gav = gavClass(
            {'gav': 'a:i:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert None == self.resolver.resolver.does_version_exist_in_nexus(
            gav, '1.1-SNAPSHOT').version
        gav = gavClass(
            {'gav': 'a:i:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.1.0' == self.resolver.resolver.what_is_the_next_version_base(
            gav, '1.1-SNAPSHOT').version
        gav = gavClass(
            {'gav': 'a:i:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})


    def test_gav_class_resolve_major_minor_x(self):
        # resolving dashboard-web 4.0 to 4.0.0
        gav = gavClass({'gav': 'com.fedex.sefs.dashboard:dashboard-web:jar',
                                'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        #
        assert 'com.fedex.sefs.dashboard' ==  gav.groupId
        assert 'dashboard-web' ==  gav.artifactId
        assert '4.0.0' ==  self.resolver.resolve(gav,  '4.0').version

    def test_gav_class_resolve_snapshot_only(self):
        mani = manifestResolver('../../test/resources', 'L1')
        self.resolver.set_manifest_resolver(mani)

        # resolving dashboard-web 4.0 to 4.0.0
        gav = gavClass({'gav': 'com.fedex.ground.sefs:fxg-core-cacheservice-duplicatecheck:zip',
                                'repoName': 'public', 'saveArchiveName': 'model.zip'})
        #
        assert 'com.fedex.ground.sefs' ==  gav.groupId
        assert 'fxg-core-cacheservice-duplicatecheck' == gav.artifactId
        self.resolver.resolve(gav, '3.0')
        # as listed in the manifest
        assert '3.0.0' ==  gav.version
        assert None is gav.has_no_releases
        gav.version = None
        # find the latest 4.0 (irregardles of the manifest)
        gav.version = None
        assert '4.0.7' ==  self.resolver.resolve(gav,  '4.0').version
        # from the maniifest
        gav.version = '4.1.5'  # hardcoded
        assert '4.1.5' ==  self.resolver.resolve(gav).version
        gav.version = None
        assert '4.0.5' ==  self.resolver.resolve(gav).version
        # from hardcoded
        gav.version = '4.X.Y.Z'
        assert '4.X.Y.Z' ==  self.resolver.resolve(gav).version
        # preset to majonir
        gav.artifactId = "x%s2" % (gav.artifactId)
        gav.version = None
        assert None ==  self.resolver.resolve(gav,  '4.0').version
        gav.version = None
        with pytest.raises(Exception):
            assert None ==  self.resolver.resolve(gav).version

    def test_resolve_method(self):
        self.resolver = nexusVersionResolver(
            get_options(), manifestResolver=self.mani)
        gav = gavClass({'gav': 'com.fedex.sefs.common:sefs-parent:pom',
                                'repoName': 'public', 'saveArchiveName': 'pom-model.zip'})
        #print "reslove:", self.resolver.resolve(gav,'1.1')
        gav.version = None
        assert '1.0.5' ==  self.resolver.resolve(gav,  '1.0').version
        gav.version = None
        assert '0.0.0' ==  self.resolver.resolve(gav,  '0.0').version
        gav.version = None
        assert '1.1.0' ==  self.resolver.resolve(gav,  '1.1').version

    def test_resolve_method_no_released_items_yet(self):
        self.resolver = nexusVersionResolver(
            get_options(), manifestResolver=self.mani)
        gav = gavClass({'gav': 'com.fedex.sefs.common:sefs-parent:pom',
                                'repoName': 'public', 'saveArchiveName': 'pom-model.zip'})
        assert '1.1.0' ==  self.resolver.resolve(gav,  '1.1').version
        gav = gavClass({'gav': 'com.fedex.sefs.common:sefs-parent:pom',
                                'repoName': 'public', 'saveArchiveName': 'pom-model.zip'})
        assert '1.0.5' ==  self.resolver.resolve(gav,  '1.0').version

        self.resolver = nexusVersionResolver(
            get_options(), manifestResolver=self.mani)
        gav = gavClass({'gav': 'com.fedex.sefs.common:sefs-parent:pom',
                                'repoName': 'public', 'saveArchiveName': 'pom-model.zip'})
        assert '0.0.0' ==  self.resolver.resolve(gav,  '0.0').version

    def test_resolve_method_snapshots_metadata(self):
        self.resolver = nexusVersionResolver(
            get_options(), manifestResolver=self.mani)
        gav = gavClass({'gav': 'com.fedex.ground.sefs.:ShipmentGatewayFXG:pom',
                                'repoName': 'public', 'saveArchiveName': 'bom-model.zip'})
        assert '3.3.0' ==  self.resolver.resolve(gav,  '3.3').version

    def test_resolveGavs(self):
        resolver = nexusVersionResolver(get_options())
        fdl = fdeployLoader(get_options())
        fdl.digest('../../test/resources/fxg-pd-domainview.json', None)
        resolver.resolveGavs(fdl.components[0])

    def test_parse_metadata_release_refactored(self):
        nexusresolver = nexusVersionResolver(get_options())
        mani = manifestResolver('../../test/resources', 'L1')
        nexusresolver.set_manifest_resolver(mani)
        gav = gavClass({'gav': 'com.fedex.ground.sefs.:ShipmentGatewayFXG:pom',
                            'repoName': 'public', 'saveArchiveName': 'bom-model.zip'})
        # resolved from manifest
        gav = nexusresolver.resolve(gav)
        assert '3.3.0-SNAPSHOT' ==  gav.version
        # resolved with major/minor
        gav.version = None
        gav = nexusresolver.resolve(gav, '3.3')
        assert '3.3.0' ==  gav.version

    def test_use_case_hu(self):
        nexusVersionResolver(get_options())
        gav = gavClass(
            {'gav': 'b:b:jar', 'repoName': 'public', 'saveArchiveName': 'maven-model.zip'})
        assert '1.3.4' == self.resolver.resolver.what_is_the_next_version_base(
            gav, '1.3').version
        gav.version = None
        assert '1.3.4' == self.resolver.resolver.what_is_the_next_version_base(
            gav, '1.3').version
