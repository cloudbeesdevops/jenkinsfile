
import copy
from fdeploy.content.fdeployComponent import fdeployComponent
from fdeploy.UtilsForTesting import get_options
from fdeploy.fdeployLoader import fdeployLoader

class TestServiceLoader(object):

    options = get_options()

    def test_service_loading(self):

        test_component = fdeployComponent({
            'id': 'componentId',
            'platform': 'bash_generator',
            'type': 'tibco_bw2',
            'filter' :
                { "var1" : "value1", "var2" : "value2"}
            ,
            'infrastructure' : {
                'service-config' : '../../test/resources/pcf-config/pcf-cups-config.json',
                'depends-on' : 'Basic-Security,${level}_${IDENTITY}_JMS_outbound_staging,blabla'
            },
            'content':  [{
                "gav": "com.fedex.sefs.core:sefs_suCLEARANCEFeed:zip",
                "relativePath": "${level}/${var1}.sh",
                "saveArchiveName": "sefs_suCLEARANCEFeed.zip"
            },
        {
                        "directory"       : ".",
                        "prependpath"     : "config",
                        "includes"        : "L1/*.*,*.txt,apps/log4j.xml,apps/be-engine.tra",
                        "filtering" : 'true',
                        "saveArchiveName" : "resources.zip"
                    },
        {
                        "directory"       : ".",
                        "prependpath"     : "config",
                        "includes"        : "L1/*.*,*.txt",
                        "filtering" : 'true',
                        "normalize" : 'false',
                        "saveArchiveName" : "resources2.zip"
                    }

            ],
            'name_format': 'format',
            'levels':
            [{
                'level': 'L1',
                'filter' : { 'var1' : 'value2'}
            }]
        }, 'inline.json')
        assert not test_component.service is None
        assert 3 ==  len(test_component.services)

    def test_cups_types(self):
        opt = copy.deepcopy(self.options)
        fdl = fdeployLoader(opt)
        fdl.readDescriptors(['../../test/resources/pcf-config/itinernary-discovery-cups.json'])
        assert 1 == len(fdl.components)
        assert not fdl.components[0].infrastructure is None
        assert 2 ==  len(fdl.components[0].services)

    def test_service_from_relative_path(self):
        opt = copy.deepcopy(self.options)
        fdl = fdeployLoader(opt)
        fdl.readDescriptors(['../../test/resources/pcf/pcf-infra-depends.json'])
        assert 1 == len(fdl.components)
        assert not fdl.components[0].infrastructure is None
        assert 2 ==  len(fdl.components[0].services)

if __name__ == '__main__':
    unittest.main()
