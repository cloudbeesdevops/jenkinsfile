# -*- coding: utf-8 -*-

import fdeploy.deployer
import pytest
from fdeploy.nexusVersionResolver import absolute_repository_path
import os
from fdeploy import get_app_group, Options, resolve_filter, importExternalRtvs
from fdeploy.content.test_component_helper import create_test_component

class TestInitMethods(object):

    source_file =  '''   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
EOF
'''

    filter = {'_target_path' : 'mypath', '_archive_version' : '2.503'}


    maxDiff = None

    def test_app_group_name_by_String(self):
        assert 'FXE' == get_app_group('./config.FXE/blablabla.json')
        assert 'FXF' == get_app_group('./config.FXF/config.FXE.json')
        assert None == get_app_group('./conig.FXF/config.FXE.json')
        assert 'blabla' == get_app_group('./conig.FXF/config.FXE.json','blabla')

    def test_app_group_name_by_component(self):
        comp = create_test_component(type='pcf')
        comp.source = './config.verification/pcs.json'
        assert 'verification' == get_app_group(comp)


    def test_filtering_script(self):
        result = self.source_file
        try:
            result.index('$')
        except:
            self.assertTrue(False)
        result = resolve_filter(result, self.filter)
        try:
            result.index('$')
            self.assertTrue(False)
        except:
            pass
        assert 72 == result.index('mypath')
        assert 79 == result.index('2.503')

    def test_file_filitering(self):
        asstring = fdeploy.platform.load_file('../../test/resources/bw5/FXFShipFeed_template_l2.xml')
        result = resolve_filter(asstring,{ 'maxHeapSize' : '4096', '_archive_version' : '1.0-SNAPSHOT',
        'JMS_conn_External_Reject_JNDIPassword' : 'secret99',
        'JMS_conn_External_Reject_JNDIUser' : 'secret99', 'JMS_conn_External_Reject_QueueConnectionFactory' : 'reject',
        'JMS_conn_External_Reject_Queue' : 'value',
        '_target_path' : 'PATHTOTARGET', 'maxLogFileSize' : '20000', 'maxLogFileCount' : '5', 'threadCount' : '10'})
        assert type(result) ==  str
        ii=1
        for i in result.split('\n'):
            assert i.count('${') == 0
            ii+=1


    def test_load_file_password(self):
        a = fdeploy.platform.load_file('../../test/resources/pcf-config/cups2.out',False,True)
        assert ('export PCF_PASSWORD' in a) == True
        assert 41 == len(a.split("\n"))
        a = fdeploy.platform.load_file('../../test/resources/pcf-config/cups2.out',True,True)
        assert ('export PCF_PASSWORD' in a) == False
        assert 40 == len(a.split("\n"))


    def test_load_rc(self):
        options = Options(defaults={})
        apath = os.path.abspath('../../test/resources/')
        path = absolute_repository_path('../../test/resources/')
        assert os.path.exists(path)
        assert apath ==  path
        with pytest.raises(Exception):
            fdeploy.load_rc('../../test/resources/.fdeployrc',options)
        fdeploy.load_rc('../../test/resources/FXE_fdeploy/.fdeployrc',options)
        assert 'nexusURL' in options.defaults
        assert 'snapshotsRepo' in options.defaults
        assert 'releasesRepo' in options.defaults
        #assert 'file://%s/local' % (apath) == options.defaults['nexusURL']

    def test_import_external_rtv(self):
        map = {}
        importExternalRtvs('../../test/resources/importExternals/external-rtvs.json', map)
        assert '6269' ==  map['EAINUMBER']
        importExternalRtvs('../../test/resources/importExternals/external-rtvs2.json', map)
        assert '3534455' ==  map['EAINUMBER']
        #importExternalRtvs('../../test/resources/importExternals/external-rtvs.json', map)

    def test_options(self):
        o = Options(aaa='x',bbb='xx',ccc='andsoandso')
        assert 'xx' == o.bbb
        o.overwrite(bbb='yy',ddd='z')
        assert o.bbb == 'yy'
        assert o.ddd == 'z'

if __name__ == '__main__':
    unittest.main()
