import os
from ldd.lddPublisher import dns_cname
import socket
import glob


def gav(component):
    for nexusContent in component.contents:
        if component.isNexusArtifact(nexusContent):
            return nexusContent
    return None

def application_report(loader):

    report=[]
    report.append("| FdeployId | EAI | Type | Target |")
    report.append("|--|--|--|--|")

    content = {}
    for component in loader.components:
        rtv_map={}
        level = loader.find_level(component,'L4')
        if level is None:
            continue
        print( level)
        #['level']
        rtv_map,envvar=component.get_env_vars({},level['level'], [], 'NA', init=rtv_map)
        print( rtv_map)
        try:
            eai = rtv_map['EAI_NUMBER'] if 'EAI_NUMBER' in rtv_map else rtv_map['EAINUMBER']
        except:
            eai = 'NA'
        content[component.id]= "| %s | %s | %s | %s |" % (component.id, eai,component.type,'')

    for h in report:
        print( h)
    for k in sorted(content.keys()):
        print( content[k])

def application_target_report(loader, level_name='L4'):

    report=[]
    report.append("| FdeployId | EAI | Type | Target |")
    report.append("|--|--|--|--|")

    content = {}
    targets={}

    for component in loader.components:
        rtv_map={}
        level = loader.find_level(component,level_name)
        if level is None:
            continue
        print( level)
        #['level']
        rtv_map,envvar=component.get_env_vars({},level['level'], [], 'NA', init=rtv_map)
        print( rtv_map)
        try:
            eai = rtv_map['EAI_NUMBER'] if 'EAI_NUMBER' in rtv_map else rtv_map['EAINUMBER']
        except:
            eai = 'NA'
        content[component.id]= "| %s | %s | %s | %s |" % (component.id, eai,component.type,'')
        target_list = loader.find_targets_in_level(component, level_name)
        for t in target_list:
            if not component.id in targets.keys():
                targets[component.id]=[]
            targets[component.id].append(t)
        # load properties abs
        print( component.resources)
        gv = gav(component)
        if gv:
            print( gv)
            appsdir = "%s/apps/%s" % (os.path.dirname(component.source),gv.artifactId)
            print( appsdir)
            if os.path.exists(appsdir):
                files =glob.glob('%s/**/*.properties' % (appsdir))
                for f in files:
                    print ("-------> %s <----------" % (f))

    wikiroot="./tmp/dual-data-center.wiki/%s"%(level_name)
    if not os.path.exists(wikiroot):
        os.makedirs(wikiroot)

    for k in sorted(content.keys()):
        with open("%s/%s.md" % (wikiroot, k),"w+") as f:
            for h in report:
                f.write("%s\r\n" % (h))
            f.write("%s\r\n" % (content[k]))
            f.write("\r\n")
            f.write("| User | Alias | Host | IP |\r\n")
            f.write("|---|---|----|----|\r\n")
            try:
                for t in targets[k]:
                    try:
                        host = t.split('@')
                        if ':' in host[1]:
                            host[1] = host[1].split(':')[0]
                        else:
                            pass
                        host.append( dns_cname(host[1])[0] )
                        host.append( socket.gethostbyname(host[1]))
                    except:
                       host = ['', t,'','']
                    f.write( "| %s | %s | %s | %s |\r\n" % (host[0],host[1],host[2],host[3]))
            except Exception:
                pass
            f.write("\r\n")
            f.write("\r\n")
