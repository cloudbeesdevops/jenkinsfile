
import os
import re
import unicodedata
import fdeploy
from fdeploy.fdeployLoader import fdeployLoader
import pytest
from fdeploy.platform import load_file, load_file_as_string
from fdeploy import Options

skipping_when_running_outside_fedex_network = pytest.mark.skipif(fdeploy.is_on_fedex_network() == False ,
                                reason="cant run outside of fedex network")
skipping_when_missing_cryptology = pytest.mark.skipif(fdeploy.security.inline_singleton() is None and fdeploy.security.initialized == True  ,
                                reason="cant run because cryptology is missing")
skipping_if_cryptology_exists = pytest.mark.skipif(not fdeploy.security.inline_singleton() is None and fdeploy.security.initialized == True ,
                                reason="cant run when cryptology exists ")

def assertLines(jsonFile, expected,actual, skip=True):
    assert actual is not None
    lines_a = expected.split("\n")
    lines_b = actual.split("\n")
    j = 0
    for i in lines_b:
        fdeploy.LOGGER.info("generating for [%s] " % ( i ))
        if skip == True:
            if re.search('# ---- begin - remote_properties_snippet.sh',i):
                skip=False
            continue
        if skip == False and re.search('# ---- end - remote_properties_snippet.sh',i):
            break
        print("[%s]\n[expect]\t%3d '%s'\n[actual]\t%3d '%s'" % (jsonFile,j,lines_a[j],j,i))
        if not 'PCF_PASSWORD' in lines_a[j] and not 'CF_HOME' in lines_a[j] and not lines_a[j].startswith('[20]'):
            assert lines_a[j] == i
        j +=1

def compare_files(expected, result, use_assert=False, filter=True, strip=False):
    assert os.path.exists(os.path.abspath(expected))
    assert os.path.exists(os.path.abspath(result))
    #print "--->", expected, result, filter
    was = load_file_as_string(result) if filter == False else load_file(result,True)
    expected_was = load_file_as_string(expected) if filter == False else load_file(expected,True)
    if use_assert == True:
        assert was != ''
        assert expected_was == was
    else:
        assertlines(expected_was, was, remove_empty_lines=False, strip=strip, f1=expected, f2=result)

# assertPropertiesGeneration(options,'tibco_beGenerator',
# 'BE_FXE_sefs_be_suDvisPnD_PickupAndDeliveryPU_%0d',
# '../../test/resources/FXE_fdeploy/fxe-be-sefsDvisPnD.json', self.properties)

def assertPropertiesGeneration(options, fdeployType,fdeployName, jsonFile, properties):
    fdeployTypeFile = "testfile.%s" % (fdeployType)
    fdl = fdeployLoader(options)
    fdl.readDescriptors(
        [jsonFile])
    assert 1 ==  len(fdl.components)
    clz,classname=fdl.components[0].get_generator_class(options,'.')
    assert fdeployType ==  str(classname)
    assert fdeployName ==  str(clz.name)
    with open(fdeployTypeFile, 'w') as outfile:
        clz.generate({'contents': None, 'targets': ['sefs@urh00601.ute.fedex.com'],
            'level': fdl.components[0].get_level(options.level), 'uuid': 'all'},outfile,action=options.action)
    outfile.close()
    actual = load_file(fdeployTypeFile)
    #print actual
    assertLines(jsonFile, properties,actual)
    #
    try:
        fdl.deploying('L1',[])
    except:
        pass

def assertlines(generated,expected, remove_empty_lines=True, strip=False,f1=None, f2=None):
    assert expected is not None
    assert generated is not None
    lines1 = generated.split('\n')
    lines2 = expected.split('\n')
    __assertlines(lines1,lines2,remove_empty_lines,strip,f1,f2)
    __assertlines(lines2,lines1,remove_empty_lines,strip,f1,f2)


def __assertlines(lines1, lines2, remove_empty_lines=True, strip=False, f1=None, f2=None):
    #print "strip=%s, remove_empty_lines=%s" % (strip, remove_empty_lines)
    i=1
    for l in lines1:
        try:
            l2 = lines2[i-1]
        except:
            continue
        if  isinstance(l, unicode):
            l = remove_control_characters(l)
        if strip == True:
            l = re.sub( r'^\s*','', l)
            l = re.sub( r'\s*$','', l)
        if  isinstance(l2, unicode):
            l2 = remove_control_characters(l2)
        if strip == True:
            l2= l2.lstrip()
            l2 = l2.rstrip()
            l2 = l2.strip()

        if remove_empty_lines == True and l == "":
            continue
        if l == "" and l2 == "":
            i+=1
            continue
        if l2 == "":
            i+=1
            continue
        #print " lines %s:  match \n\t'%s' \n\t'%s'" % (i,l,l2)
        # ignore certain lines 
        if 'src/main/python' not in l2 and 'PCF_PASSWORD' not in l2 and 'CF_HOME' not in l2 and not l2.startswith('[20') and '_workdir' not in l2 and 'local_base_dir' not in l2:
            if l != l2:
                raise Exception(" lines %s: dont match \n'%s' vs \n'%s' \nfiles %s vs %s" % (i,l,l2, f1, f2))
        i+=1
        #if i > 10:
        #    break
    #assert len(lines1) == len(lines2)

def remove_control_characters(s):
    return "".join(ch for ch in s if unicodedata.category(ch)[0]!="C").strip()

def get_compnent_from_json(level, file, options=None):
    if options is None:
        options = get_options()
    options.overwrite(level=level)
    fdl = fdeployLoader(options)
    fdl.readDescriptors([file])
    comp = fdl.components[0]
    comp.source= 'config.FXE/%s' % (os.path.basename(file))
    artifact = comp.contents[0]
    return artifact, comp, fdl


def get_options():
    cdw = os.path.abspath("%s/../../test/resources" % (os.getcwd()))
    options = Options(command='control',X=True,action='status', app_group='FXE', id=None, ignore_manifest_errors = False,
        level='L1',identity='.',dry_run=False,no_wait=True,rtv=[], type=None,group=-1, output='myscript.sh', no_download=False, regex=None, group_size=None, group_number=-1,
        defaults={'nexusURL':'file://%s/local' % (cdw),'absURL':'%s/local' % (cdw), 'ldd_url' : 'http://c0009869.test.cloud.fedex.com:8090/sefs-dashboard'})
    return options




if __name__ == '__main__':
    unittest.main()
