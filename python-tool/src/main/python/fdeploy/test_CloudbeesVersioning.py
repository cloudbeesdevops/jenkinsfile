
import cloudbees
from fdeploy.nexusVersionResolver import absolute_repository_path
from fdeploy.UtilsForTesting import get_options

class TestCloudbeesVersion(object):

    options=get_options()

    # def test_latest_released_version(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     gav={'groupId' : 'com.fedex', 'artifactId' : 'xedef' }
    #     res = cldbz.latest_released_version(gav, '1.0.0-SNAPSHOT', 'public')
    #     assert None ==  res.version
    #     res = cldbz.increment_major_minor_version(gav, '1.0.0-SNAPSHOT', 'public')
    #     assert '1.0.0' ==  res.version
    #
    #
    # def __increment_equals(self, cldbz, gav, target, last_version, increment):
    #     msg=''
    #     res = cldbz.latest_released_version(gav, target, 'public')
    #     #msg = 'latest   : %s != %s for \n\tgav %s \n\tres %s' % (last_version, res.version, gav, res)
    #     assert last_version, res.version ==  msg
    #     ret = cldbz.increment_major_minor_version(res.__dict__, res.version, 'public')
    #     #msg = 'increment: %s != %s for \n\tgav %s -> \n\tret %s' % (increment, ret.version, gav, ret.__dict__)
    #     assert increment, ret.version == msg
    #
    # def test_cloudbees_latest_released_and_increment_version(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     gav={'groupId' : 'org.apache.maven'}
    #     gav2={'artifactId' : 'maven-model'}
    #     # with pytest.raises(AttributeError):
    #     #     cldbz.latest_released_version(gav,'3.2')
    #     # with pytest.raises(AttributeError):
    #     #     cldbz.latest_released_version(gav2,'3.2')
    #     gav['artifactId']=gav2['artifactId']
    #     #assert '3.2.5',cldbz.latest_released_version(gav,'3.2' == 'public').version
    #
    #     gav = { 'artifactId' : 'sefs-parent', 'groupId' : 'com.fedex.sefs.common'}
    #     self.__increment_equals(cldbz, gav, '0.0.1-SNAPSHOT', '0.0.1', '0.0.1')
    #     self.__increment_equals(cldbz, gav, '1.0.5-SNAPSHOT', '1.0.5', '1.0.6')
    #     self.__increment_equals(cldbz, gav, '1.1.0-SNAPSHOT', '1.1.0', '1.1.0')
    #     gav = { 'artifactId' : 'spring-boot-bom-parent', 'groupId' : 'com.fedex.sefs.common'}
    #     self.__increment_equals(cldbz, gav, '1.5.10.0-SNAPSHOT', '1.5.10.0', '1.5.10.0')
    #     gav = { 'artifactId' : 'spring-boot-bom-parent2', 'groupId' : 'com.fedex.sefs.common'}
    #     self.__increment_equals(cldbz, gav, '1.5.10.0-SNAPSHOT', '1.5.10.0', '1.5.10.1')
    #
    # def test_cloudbees_get_major_minor_version(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     assert '2.0' == cldbz.getMajorMinorVersion('2')[0]
    #     assert '2.1' == cldbz.getMajorMinorVersion('2.1')[0]
    #     assert '2.1' == cldbz.getMajorMinorVersion('2.1.1')[0]
    #     assert '2.1.1' == cldbz.getMajorMinorVersion('2.1.1.2')[0]
    #     assert 'ABC.2' == cldbz.getMajorMinorVersion('ABC.2')[0]
    #     assert '2.1.1' == cldbz.getMajorMinorVersion('2.1.1.2.AND')[0]
    #     assert '3.0' == cldbz.getMajorMinorVersion('R-3.0.0.Master')[0]
    #     assert '1.5.10' == cldbz.getMajorMinorVersion('1.5.10.0-SNAPSHOT')[0]
    #
    # def test_cloudbees_increment_version(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     assert '2.0.0',cldbz.increment_version('2.0' == None)
    #     assert '2.1.0',cldbz.increment_version('2.1' == None)
    #     assert '2.1.0',cldbz.increment_version('2.1' == '2.1')
    #     assert '2.1.0',cldbz.increment_version('2.1.0' == '2.1')
    #     assert '2.1.1',cldbz.increment_version('2.1.0' == '2.1.1')
    #     assert '3.1.2',cldbz.increment_version('3.1.1' == None)
    #     assert '3.2.6',cldbz.increment_version('3.2.5' == None)
    #     assert '2.1.1',cldbz.increment_version('2.1.0' == None)
    #     assert '2.1.0',cldbz.increment_version('2.1.0-SNAPSHOT' == '2.1')
    #     assert '2.3.2.2',cldbz.increment_version('2.3.2.1' == None)
    #     assert '2.3.2.1',cldbz.increment_version('2.3.2.1' == '2.3.2.1')
    #     assert '2.3.1.MASTER',cldbz.increment_version('2.3.0.MASTER-SNAPSHOT' == None)
    #     assert '2.3.0',cldbz.increment_version('2.3.0.MASTER-SNAPSHOT' == '2.3')
    #     assert '2.3.1.MASTER',cldbz.increment_version('2.3.MASTER-SNAPSHOT' == None)
    #     assert '2.3.1.M1.MASTER',cldbz.increment_version('2.3.M1.MASTER-SNAPSHOT' == None)
    #     assert '1.5.10.0',cldbz.increment_version('1.5.10.0-SNAPSHOT' == '1.5.10.0')
    #     assert '1.5.10.1',cldbz.increment_version('1.5.10.0-SNAPSHOT' == None)
    #
    #
    # def test_cloudbees_increment_major_minor_version(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     gav={}
    #     gav['groupId']='org.apache.maven'
    #     gav['artifactId']='maven-model'
    #     gavresult = cldbz.resolve_version_nexus(gav,'3.2','public')
    #     assert '3.2.5' ==  gavresult.version
    #     gavresult = cldbz.latest_released_version(gav,'3.2','public')
    #     assert '3.2.5' ==  gavresult.version
    #     gavresult = cldbz.increment_major_minor_version(gav,'3.2.10','public')
    #     assert '3.2.6' ==  gavresult.version
    #
    # def test_cloudbees_increment_major_minor_noversion_from_pom(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     vers=cldbz.nextVersion('../../../src/test/resources','pom-parent-noversion.xml')
    #     assert '1.0.8' == vers
    #
    # def test_cloudbees_increment_major_minor_with_version_from_pom(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     vers=cldbz.nextVersion('../../../src/test/resources','pom-parent.xml')
    #     assert '3.3.0' == vers
    # def test_cloudbees_increment_major_minor_with_version_from_pom2(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     vers=cldbz.nextVersion('../../../src/test/resources','pom-parent2.xml')
    #     assert '3.2.0' == vers
    # def test_cloudbees_increment_major_minor_with_version_from_pom4digit(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     vers=cldbz.nextVersion('../../../src/test/resources/cloudbees.py','pom-4digit1.xml')
    #     assert '2.1.7.0' == vers
    #
    # def test_cloudbees_snapshot_current_release_cut(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     gav={'groupId':'com.fedex.ground.sefs','artifactId':'BSConnectorGrid'}
    #     gavresult = cldbz.latest_released_version(gav,'2.1.0','public')
    #     assert not gavresult is None
    #     gavresult = cldbz.resolve_version_nexus(gav,'2.1','public')
    #     assert '2.1.0' ==  gavresult.version
    #     gavresult = cldbz.latest_released_version(gav,'2.1.0','public')
    #     assert '2.1.0' ==  gavresult.version
    #     gavresult = cldbz.increment_major_minor_version(gav,'2.1.0','public')
    #     assert '2.1.0' ==  gavresult.version
    #
    # def test_cloudbees_snapshot_next_no_release_cut(self):
    #     cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
    #     gav2={'groupId' :'com.fedex.ground.sefs','artifactId' :'BSConnectorGrid'}
    #     gavresult = cldbz.increment_major_minor_version(gav2,'2.1.0-SNAPSHOT','local')
    #     assert not gavresult is None
    #     assert '2.1.0' ==  gavresult.version

    def __nextVersion(self,result,poms=None, cldbz=None):
        if cldbz is None:
            cldbz = cloudbees.CloudBees(self.options.defaults['nexusURL'])
        for pom_xml in poms:
            actual = cldbz.nextVersion('../../test/resources/cloudbees.py/',pom_xml)
            assert result,actual  == pom_xml + ": "+ result + " != " + actual

    def test_cloudbees_sefsnextversion(self):
        poms = ['pom2-1.1.0-SNAPSHOT.xml']
        self.__nextVersion('1.1.1',poms)

        poms = ['bom.xml']
        self.__nextVersion('1.5.10.0',poms)

        poms = ['pom3-1.5.10.1-SNAPSHOT.xml']
        self.__nextVersion('1.5.10.1',poms)

        poms = ['pom-waypoint.xml']
        self.__nextVersion('3.1.1',poms)

    def test_cloudbees_sefsnextversion_two_repositories(self):
        nexus2 = absolute_repository_path('../../test/resources/local0')
        cldbz = cloudbees.CloudBees("%s,%s" % (self.options.defaults['nexusURL'], nexus2))
        poms = ['pom0.xml']
        self.__nextVersion('3.2.6',poms,cldbz)

        cldbz = cloudbees.CloudBees("%s,%s" % (nexus2,self.options.defaults['nexusURL']))
        self.__nextVersion('3.2.6',poms,cldbz)


if __name__ == '__main__':
    unittest.main()
