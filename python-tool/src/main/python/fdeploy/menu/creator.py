import curses
import fdeploy.menu
import fdeploy


class creator(object):


    def __init__(self, stdscreen, loader,_groups):
        self.screen = stdscreen
        curses.curs_set(0)
        _comps = loader.components
        self.level_selection = None
        self.comp_selection = None
        self.command_selection = None
        self.action_selection = None
        self.target_selection = None
        invalidate = False

        level_items = self.getlevel_select(_comps)
        while self.target_selection is None:
            while self.comp_selection is None:
                while (self.action_selection is None): # action loop
                    while (self.command_selection is None): # command loop
                        while (self.level_selection is None):
                            self.level_selection, level_index = fdeploy.menu.pick(
                                level_items,'select level: ','>',0,invalidate,1,
                                x=1,y=1,invalidate=False)
                            if self.level_selection is not None:
                                invalidate = False
                                break
                        fdeploy.LOGGER.debug( "level %s" % (self.level_selection))
                        self.command_selection, command_index = fdeploy.menu.pick(
                            self.getcommand_select(_groups), 'select command: ',
                                '>',0,False,1,x=25,y=1,invalidate=invalidate)
                        if self.command_selection is not None:
                            break
                        if command_index < 0:
                            invalidate = False
                            self.level_selection = None
                            self.command_selection = None
                    fdeploy.LOGGER.debug( "command %s" % (self.command_selection))
                    self.action_selection, action_index = fdeploy.menu.pick(
                        self.getaction_select(_groups, self.command_selection),
                            'select action %s: ' % (self.level_selection),'>', 0, False, 1,x=42,y=1,invalidate=invalidate)
                    invalidate = False
                    if self.action_selection is not None:
                        self.action_selection = self.action_selection.strip()
                        break
                    if action_index < 0: # -1 = going back
                        invalidate = False
                        self.command_selection = None
                    fdeploy.LOGGER.debug( "action %s" % (self.action_selection))
                # select action
                if self.action_selection in fdeploy.ACTIONS_WITH_COMPONENT_MENU:
                    comp_title = "select components for %s: \n(select one or more components with space)" % (self.level_selection)
                    comp_items = self.getcomponent_select(_comps, self.level_selection)
                    if 'test' == self.command_selection:
                        comp_items.insert(0,'--all--')
                    else:
                        comp_items.append('--all--')
                    self.comp_selection = fdeploy.menu.pick(comp_items, comp_title,'>',0,True,1,invalidate=True,x=1,y=1)
                    if self.comp_selection is None: # hit the back key
                        invalidate = True
                        continue
                else:
                    self.comp_selection = [('--all--',-1)]

            # second screen for detailed selection if applicable
            if self.action_selection in fdeploy.ACTIONS_APP_USER_REQUIRED:
                machines = self.getmachine_select(self.comp_selection,self.level_selection,_comps)
                _machines = list(machines)
                _machines.insert(0,'--all--')
                __selection = fdeploy.menu.pick(
                    _machines,'select machines to %s: ','>',0,True,1,
                    x=50,y=1,invalidate=False)
                #print("self.target_selection= %s" % (str(__selection)))
                if __selection is not None:
                    self.target_selection=[]
                    for _tgt in __selection:
                        #print( "loopcheck=" + str(_tgt))
                        (t,i)=(_tgt)
                        if t == '--all--':
                            self.target_selection = machines
                            break
                        self.target_selection.append(t)
                    fdeploy.LOGGER.debug( "targets %s" % (str(self.target_selection)))
            else:
                # no target selection needed or available for this command
                break

        self.selections = { 'action' : self.action_selection, 'command' : self.command_selection,
            'components' : self.comp_selection, 'level' : self.level_selection, 'targets' : self.target_selection }

    def getmachine_select(self,components, level, cobjects=None):
        _targets=[]
        for item in components:
            if "--all--" in item:
                components = []
                for _item in cobjects:
                    components.append( (_item.id, 0) )
                break

        #print( "componn: " + str(components))
        for __comp in components:
            comp_id, id = (__comp)
            #print str(comp_id)
            comp = self.getcomponent_by_id(cobjects,comp_id)
            # get all uniqu levels across all components.
            for _level in comp.levels:
                if  _level['level'] == level:
                    for target in _level['targets']:
                        if target not in _targets:
                            _targets.append(target)
        return _targets


    def getlevel_select(self,components):
        levels=[]
        for comp in components:
            # get all uniqu levels across all components.
            for _level in comp.levels:
                if not _level['level'] in levels:
                    levels.append(_level['level'])
        return levels

    def getcomponent_by_id(self, components, id):
        for comp in components:
            if comp.id == id:
                return comp
        return None

    def getcomponent_select(self,components, level):
        _components=[]
        for comp in components:
            if not comp.id in _components:
                _components.append(comp.id)
        return _components

    def getcommand_select(self, groupz):
        list=[]
        for (g,i) in groupz:
            list.append(g)
        return list

    def getaction_select(self,groupz, selected_group):
        list=[]
        for (g,i) in groupz:
            if g == selected_group:
                for _a in i._actions:
                    if _a.dest == 'action':
                        list=_a.choices
        if len(list) == 0:
            raise Exception("no actions found for %s" % (selected_group))
        _list=[]
        # formating
        for _item in list:
            _list.append("%-15s" % (_item))
        return _list
