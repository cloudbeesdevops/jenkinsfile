from fdeploy.pam import pamAuth
import fdeploy
import requests
import json
import os
from MockHttpServer import BASE_URL, start_mock_server
from BaseHTTPServer import BaseHTTPRequestHandler

def setup_pam_url(port):
    PAM_URL_FMT = BASE_URL + "/api.php/v1/passwords.json/%%s"
    return PAM_URL_FMT % (port)

PAM_URL= None

PAM_ID_LOOKKUP={ '18409': 'FXS_app3534642',  '18410' : 'FXS_app3534545', '18414' : 'FXS_app3534546', '334001' : 'FXS_app3534731'}

class pamAuthRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        fdeploy.LOGGER.info("request = %s" % (self.path))
        if 'api.php/v1/passwords.json/' in self.path:
            self.send_response(requests.codes.ok)

            #print self.path.split('/')
            pp = self.path.split('/')

            pam_id = pp[len(pp)-1]

            # Add response headers.
            self.send_header('Content-Type', 'application/json; charset=utf-8')
            self.end_headers()

            # Add response content.
            #print "\n\t user=%s\t%s pamd_id=%s\n" % (os.environ['MOCK_USERNAME'], self.path, pam_id)
            response_content = json.dumps({'accountName' : PAM_ID_LOOKKUP[pam_id], 'password' : 'passowrd'})
            self.wfile.write(response_content.encode('utf-8'))
            return

class TestPAM(object):

    @classmethod
    def setup_class(module):
        #os.environ['NO_PROXY']='127.0.0.1'
        os.environ['MOCK_USERNAME'] = 'FXS_app3534642'
        port = start_mock_server(pamAuthRequestHandler)
        fdeploy.pam.PAM_URL= setup_pam_url(port)
        fdeploy.turn_on_http_tracing()
        requests.get('http://localhost:%s/api.php/v1/passwords.json/' % (port))

    def test_getpwd(self):
        pam = pamAuth(pam_url=PAM_URL)
        uid,pwd = pam.get_passwd(18409)
        assert not pwd is None
        assert 'FXS_app3534642' == uid
