# -*- coding: utf-8 -*-
import fdeploy
from fdeploy.security import deconstruct, is_encapsulated

class Jasypt(object):

    key = fdeploy.JASYPT_SECRET
    cryptor = None

    def __init__(self, key=None):
        if not key is None:
            self.key = key
        try:
            from jasypt4py.encryptor import StandardPBEStringEncryptor
            self.cryptor = StandardPBEStringEncryptor('PBEWITHSHA256AND256BITAES-CBC')
        except:
            self.cryptor = None

    def decrypt(self, encrypted_value):
        original = encrypted_value
        #print "decrypt: %s, cryptor=%s, is_encapsulated=%s" % (encrypted_value, self.cryptor, is_encapsulated(encrypted_value))
        if self.cryptor is None:
            return encrypted_value
        if is_encapsulated(encrypted_value) == True:
            encrypted_value=deconstruct(encrypted_value)
            # try:
            return self.cryptor.decrypt(self.key, encrypted_value, 4000)
            # except TypeError:
            #     return encrypted_value
        if original == encrypted_value:
            raise Exception('unable to decrypt value %s is is_encapsulated = %s' % (original, is_encapsulated(original)))
        return encrypted_value

    def encrypt(self, unencrypted_value):
        if self.cryptor is None:
            return unencrypted_value
        if is_encapsulated(unencrypted_value) == True:
            return unencrypted_value
        return 'ENC(%s)' % (self.cryptor.encrypt(self.key, unencrypted_value, 4000))

    def is_encrypted(self, value):
        try:
            v = deconstruct(value)
            v = self.cryptor.decrypt(self.key, v, 4000)
            return True
        except:
            return False
