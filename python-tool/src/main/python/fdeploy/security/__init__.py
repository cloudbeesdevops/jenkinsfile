# -*- coding: utf-8 -*-
import fdeploy
import re

ENCODED_VALUE = re.compile('ENC\((.+)\)')

def is_encapsulated(value):
    m = ENCODED_VALUE.match(value)
    return not m is None

def deconstruct(value):
    m = ENCODED_VALUE.match(value)
    if m:
        return m.groups(0)[0]
    return value


singleton=None
initialized=False
def inline_singleton(key=None, force_refresh=False):
    #raise Exception("trace")
    if force_refresh == True:
        fdeploy.security.initialized = False
        fdeploy.security.singleton = None

    if fdeploy.security.singleton is None and fdeploy.security.initialized == False:
        try:
            from fdeploy.security.Jasypt import Jasypt
            fdeploy.security.singleton = Jasypt()
            if key:
                fdeploy.security.singleton = Jasypt(key)
            fdeploy.security.initialized=True
            fdeploy.LOGGER.warn("ENABLED CRYPTOLOGY found package jasypt4py")
        except AttributeError as e:
            fdeploy.LOGGER.warn("DISABLED CRYPTOLOGY due to missing package jasypt4py. Reason: %s" % (e))
            raise e
    return fdeploy.security.singleton
