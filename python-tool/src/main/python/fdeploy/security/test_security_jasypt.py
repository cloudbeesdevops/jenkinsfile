# -*- coding: utf-8 -*-
import fdeploy
from fdeploy.security import inline_singleton, is_encapsulated
from fdeploy.security.Jasypt import Jasypt
from fdeploy.UtilsForTesting import skipping_when_missing_cryptology

jasypt = inline_singleton('secret99')

class TestSecurity(object):

    @skipping_when_missing_cryptology
    def test_security_encrypt(self):
        value = 'andre'
        assert fdeploy.security.test_security_jasypt.jasypt.is_encrypted(value) == False
        vvvv = fdeploy.security.test_security_jasypt.jasypt.encrypt(value)
        assert vvvv.startswith('ENC(')
        assert 'andre' == fdeploy.security.test_security_jasypt.jasypt.decrypt(vvvv)

    @skipping_when_missing_cryptology
    def test_enc_parse( self):
        vvvv = fdeploy.security.test_security_jasypt.jasypt.encrypt('boohoo')
        assert vvvv != 'boohoo'
        assert fdeploy.security.test_security_jasypt.jasypt.is_encrypted(vvvv)
        assert 'boohoo' == fdeploy.security.test_security_jasypt.jasypt.decrypt(vvvv)

    @skipping_when_missing_cryptology
    def test_enc_dec(self):
        fdeploy.security.test_security_jasypt.jasypt = Jasypt('bonzo')
        d = fdeploy.security.test_security_jasypt.jasypt.encrypt('MANDATORY')
        assert is_encapsulated(d)
        assert 'MANDATORY' == fdeploy.security.test_security_jasypt.jasypt.decrypt(d)

    @skipping_when_missing_cryptology
    def test_singleton_enc_dec(self):
        inline_singleton(key='securityTop', force_refresh=True)
        d = inline_singleton().encrypt('MANDATORY')
        assert is_encapsulated(d)
        assert 'MANDATORY' == inline_singleton().decrypt(d)
