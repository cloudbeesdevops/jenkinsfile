# -*- coding: utf-8 -*-
from fdeploy.security import deconstruct, is_encapsulated

class TestSecurity(object):

    def test_security__has_encrypted_value(self):
        value = 'andre'
        assert is_encapsulated(value) == False
        value = 'andre'
        assert is_encapsulated('ENC(GL8pjFDhIPAc5QVE3HGYlPWEexAo+dZdpAeYPi4rP3E=)') == True

#
# ENC(GL8pjFDhIPAc5QVE3HGYlPWEexAo+dZdpAeYPi4rP3E=)
#
    def test_deconstruct(self):
        assert 'Fedex' == deconstruct('ENC(Fedex)')
