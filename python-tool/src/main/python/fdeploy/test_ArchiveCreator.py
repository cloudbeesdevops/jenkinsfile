
import fdeploy
import shutil
import os.path
import zipfile
from fdeploy.content.fdeployComponent import fdeployComponent
from fdeploy.UtilsForTesting import get_options
from artifactArchiveCreator import artifactArchiveCreator


file_as_string = '''<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE log4j:configuration SYSTEM "//WEB-INF/log4j.dtd">
<log4j:configuration xmlns:log4j="http://jakarta.apache.org/log4j/" debug="false">
    <appender name="logStandard" class="org.apache.log4j.RollingFileAppender">
        <errorHandler class="org.apache.log4j.helpers.OnlyOnceErrorHandler" />
        <param name="MaxFileSize"    value="5MB" />
        <param name="MaxBackupIndex" value="5" />
        <param name="Threshold"      value="${debugLevel}"/>
        <param name="Append"         value="true"/>
        <param name="File"           value="/var/fedex/tibco/logs/${engine.name}_${instance.id}_application.log"/>
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="datetime=%d{yyyy-MM-dd HH:mm:ss.SSSZ},level=%p,thread=%t,%m%n"/>
        </layout>
    </appender>
    <category name="${logPrefixName}.std" additivity="false">
        <priority value="${debugLevel}" />
        <appender-ref ref="logStandard"/>
    </category>
</log4j:configuration>
'''
result_as_string = '''<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE log4j:configuration SYSTEM "//WEB-INF/log4j.dtd">
<log4j:configuration xmlns:log4j="http://jakarta.apache.org/log4j/" debug="false">
    <appender name="logStandard" class="org.apache.log4j.RollingFileAppender">
        <errorHandler class="org.apache.log4j.helpers.OnlyOnceErrorHandler" />
        <param name="MaxFileSize"    value="5MB" />
        <param name="MaxBackupIndex" value="5" />
        <param name="Threshold"      value="WARN"/>
        <param name="Append"         value="true"/>
        <param name="File"           value="/var/fedex/tibco/logs/my_engine_01_application.log"/>
        <layout class="org.apache.log4j.PatternLayout">
            <param name="ConversionPattern" value="datetime=%d{yyyy-MM-dd HH:mm:ss.SSSZ},level=%p,thread=%t,%m%n"/>
        </layout>
    </appender>
    <category name="com.fedex.my_app.std" additivity="false">
        <priority value="WARN" />
        <appender-ref ref="logStandard"/>
    </category>
</log4j:configuration>
'''

test_component = fdeployComponent({
    'id': 'componentId',
    'platform': 'bash_generator',
    'type': 'tibco_bw2',
    'filter' :
        { "var1" : "value1", "var2" : "value2"}
    ,
    'content':  [{
        "gav": "com.fedex.sefs.core:sefs_suCLEARANCEFeed:zip",
        "relativePath": "${level}/${var1}.sh",
        "saveArchiveName": "sefs_suCLEARANCEFeed.zip"
    },
{
                "directory"       : ".",
                "prependpath"     : "config",
                "includes"        : "L1/*.*,*.txt,apps/log4j.xml,apps/be-engine.tra",
                "filtering" : 'true',
                "saveArchiveName" : "resources.zip"
            },
{
                "directory"       : ".",
                "prependpath"     : "config",
                "includes"        : "L1/*.*,*.txt",
                "filtering" : 'true',
                "normalize" : 'false',
                "saveArchiveName" : "resources2.zip"
            }

    ],
    'name_format': 'format',
    'levels':
    [{
        'level': 'L1',
        'filter' : { 'var1' : 'value2'}
    }]

}, 'inline.json')

class TestArchiveCreator(object):

    options=get_options()

    def setup_method(self,method):
        #fdeploy.setLogLevel('DEBUG', 4)
        with open('../../test/resources/archiveCreator/file_do_not_check_ini.txt', 'w') as file:
            file.write(file_as_string)
        if os.path.isfile('./tmp/archives/resources.zip') == True:
            shutil.rmtree('./tmp/archives', ignore_errors=True, onerror=None)


    def test_filtering_flag(self):
        contentObject= fdeploy.content.content_factory(test_component.content[1])
        assert contentObject.filtering

    def assertFileFiltering(self,zf, result_as_string):
        for info in zf.infolist():
            if 'file_do_not_check_ini' in info.filename:
                end_as_string = zf.read('config/file_do_not_check_ini.txt')
                assert result_as_string ==  end_as_string

    def test_create_archive(self):
        ac = artifactArchiveCreator('../../test/resources/archiveCreator')
        contentObject= fdeploy.content.content_factory(test_component.content[1])
        ac.create(test_component, contentObject)
        assert True ==  os.path.isfile('./tmp/archives/resources.zip')
        zf = zipfile.ZipFile('./tmp/archives/resources.zip', mode='r')
        #for z in zf.infolist():
            #print z.filename
        count = len(zf.infolist())
        assert 3 ==  count
        # no filtering !!
        self.assertFileFiltering(zf, file_as_string)

    def test_create_archive_with_filtering(self):
        ac = artifactArchiveCreator('../../test/resources/archiveCreator')
        contentObject= fdeploy.content.content_factory(test_component.content[1])
        ac.create(test_component, contentObject, filter={ 'debugLevel' : 'WARN', 'logPrefixName' : 'com.fedex.my_app', 'engine.name' : 'my_engine','instance.id' : '01'})
        assert True ==  os.path.isfile('./tmp/archives/resources.zip')
        zf = zipfile.ZipFile('./tmp/archives/resources.zip', mode='r')

        count = len(zf.infolist())
        assert 3 ==  count
        self.assertFileFiltering(zf, result_as_string)


    def test_string_filtering(self):
        _filter =  { 'debugLevel' : 'WARN', 'logPrefixName' : 'com.fedex.my_app', 'engine.name' : 'my_engine','instance.id' : '01'}
        base = artifactArchiveCreator('')
        end_as_string = base.filter(_filter, file_as_string)
        assert result_as_string ==  end_as_string

    def test_file_filtering(self):
        # write file
        _filter =  { 'debugLevel' : 'WARN', 'logPrefixName' : 'com.fedex.my_app', 'engine.name' : 'my_engine','instance.id' : '01'}
        base = artifactArchiveCreator('.')
        with open('file.txt', 'w') as file:
            file.write(file_as_string)
        base.filterFile(_filter, 'file.txt')
        with open('file.txt', 'r') as file:
            end_as_string = file.read()
        assert result_as_string ==  end_as_string

    def test_filter_vars_reading(self):
        base = artifactArchiveCreator('.')
        assert 2 ==  len(test_component.filter)
        struct = {'contents': test_component.content,
            'targets': 'targets','level': 'L1', 'uuid': 'comp.id'
        }
        filter_map={}
        base.get_filters(filter_map,test_component,'L1')
        assert 2 ==  len(filter_map.keys())
        assert 'value2' ==  filter_map['var2']
        assert 'value2' ==  filter_map['var1']

    def test_e2e_filtering_issue(self):
        ac = artifactArchiveCreator('../../test/resources/FXE_fdeploy')
        contentObject= fdeploy.content.content_factory(test_component.content[1])
        if os.path.isfile('./tmp/archives/resources.zip') == True:
            shutil.rmtree('./tmp/archives', ignore_errors=True, onerror=None)
        ac.create(test_component, contentObject, filter={ 'debugLevel' : 'WARN', 'logPrefixName' : 'com.fedex.my_app', 'engine.name' : 'my_engine','instance.id' : '01'})
        assert True ==  os.path.isfile('./tmp/archives/resources.zip')
        zf = zipfile.ZipFile('./tmp/archives/resources.zip', mode='r')
        count = len(zf.infolist())
        assert 3 ==  count
        self.assertFileFiltering(zf, result_as_string)

    def test_e2e_no_normalize(self):
        ac = artifactArchiveCreator('../../test/resources/FXE_fdeploy')
        contentObject= fdeploy.content.content_factory(test_component.content[2])
        if os.path.isfile('./tmp/archives/resources2.zip') == True:
            shutil.rmtree('./tmp/archives', ignore_errors=True, onerror=None)
        ac.create(test_component, contentObject, filter={ 'debugLevel' : 'WARN', 'logPrefixName' : 'com.fedex.my_app', 'engine.name' : 'my_engine','instance.id' : '01'})
        assert True ==  os.path.isfile('./tmp/archives/resources2.zip')
        zf = zipfile.ZipFile('./tmp/archives/resources2.zip', mode='r')
        count = len(zf.infolist())
        assert 1 ==  count
        assert 'config/L1/RELEASE_MANIFEST.MF' ==  zf.infolist()[0].filename


if __name__ == '__main__':
    unittest.main()
