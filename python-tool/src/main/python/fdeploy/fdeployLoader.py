# -*- coding: utf-8 -*-

import json
import os
import re
import sys
import fdeploy
from fdeploy import banner
import fdeploy.content
import glob
from fdeploy.nexusVersionResolver import nexusVersionResolver
from fnexus.manifestResolver import manifestResolver
from fdeploy.artifactArchiveCreator import artifactArchiveCreator
import codecs
import fdeploy.platform.bash_generator
import subprocess
from uuid import uuid4
import fdeploy.platform
import requests
from fdeploy.domainLoader import domainLoader
from ldd.lddPublisher import lddPublisher
from fdeploy.hooks import ldd_post_request
from fdeploy.content.fdeployComponent import fdeployComponent

# set default tracelevel to none
sys.tracebacklimit = 10
reload(sys)

def create_manifest_resolver(_loaddir_,level):
    return manifestResolver(_loaddir_, level)

def loggedinuser():
    loggeduser = None
    try:
        loggedasuser = os.getlogin()
        fdeploy.LOGGER.debug("user : %s",loggedasuser)
        if (loggedasuser == "jenkins" and not loggeduser):
            if 'BUILD_URL' in os.environ:
                BUILD_API=os.environ['BUILD_URL']+"api/json"
                # only run is its injenkins and NOT fdeploy tool itself.
                if not 'XOPCO_COMMON_TOOLS_FDEPLOY' in BUILD_API:
                    ret = requests.get(BUILD_API)
                    response = 'successful' if ret.ok in [200, 201] or ret.ok == True else "failed: code %s" % (ret.ok)
                    if (response == 'successful'):
                        content = ret.json()
                        loggeduser = content["actions"][1]["causes"][0]['userId']
                    else:
                        loggeduser = re.sub('[A-Za-z]','', loggedasuser)
    except Exception as e:
        fdeploy.LOGGER.error("error: %s", e)
        if 'unable to' in "%s" % (e):
            loggeduser = 'root' # running in docker container perhaps
        else:
            if 'CI_SERVER_NAME' in os.environ:
                loggeduser=os.environ['CI_SERVER_NAME']
            else:
                loggeduser='anonymous'
    return loggeduser

def split(a, n):
    t=[]
    s=[]
    for i in range(0,len(a)):
        if i%n == 0:
            s=[]
            t.append(s)
        s.append(a[i])
    return t

def _needs_nexus_resolving(action, no_download):
    if no_download == True:
        return False
    return action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING

def _needs_manifest_resolving(action):
    return action in fdeploy.ACTIONS_WITH_MANIFEST_HANDLING


# classes
class fdeployLoader:
    components = None
    targets = None
    deployable_items = None
    action = None
    verboseFlag = False
    loadingDirectory = None
    options = None
    id_filter = None
    resolver = None
    readme = None
    ignore_level_errors = False

    def __init__(self, cli_options, importComponents=None):
        self.options = cli_options
            # print cli_options
            # import traceback
            # traceback.print_exc()
            # raise Exception('sfg')

        if not cli_options is None and isinstance(cli_options, object) and 'command' in cli_options.__dict__.keys() and cli_options.command != 'menu':
            self.action = cli_options.action
            self.verboseFlag = cli_options.X
            try:
                self.id_filter = cli_options.id
            except:
                pass
        self.components = [] if importComponents is None else importComponents
        self.base_dir = 'tmp'
        if os.path.isdir(self.base_dir) == False:
            os.makedirs(self.base_dir, 0755)
        fdeploy.LOGGER.debug(" < init fdeployLoader @ " + str(self.base_dir))

        # create artifact resovler with caching features.
        self.resolver = nexusVersionResolver(self.options,
                                                fdeploy.useCache, self.base_dir)
        if type(self.options) == dict and 'defaults' in self.options and 'nexusURL' in self.options.defaults:
            self.resolver.applyOptions(self.options)
        self.ignore_level_errors = self.options.ignore_manifest_errors

    def readDescriptors(self, argv):
        id = self.options.id;
        def digester(jsonfile, id):
            self.digest(jsonfile, id)
        self.readme = fdeploy.readDescriptors(argv, id, digester )

    ''' digest '''
    def digest(self, loadingFile, id):
        self.loadingDirectory = os.path.dirname(os.path.abspath(loadingFile))
        deploy_descr = []
        with open(loadingFile) as descriptor:
            fdeploy.LOGGER.info('loading %s descriptor.' % (loadingFile))
            try:
                deploy_descr = json.loads(descriptor.read())
            except Exception, e:
                raise Exception("loading %s failed.: reason: %s" % (loadingFile, e))
            #
            # inner function for domain
            def applyDomain(comps, loader, component):
                fdeploy.LOGGER.info("%s" % (component))
                if loader is None:
                    comp = fdeployComponent(component, loadingFile)
                    comps.append(comp)
                    fdeploy.LOGGER.info("created %s." % (comp.prettyprint()))
                else:
                    loader.digest(component['domain-config'])
                    for d in loader.domainsMap.keys():
                        comp = fdeployComponent(component, loadingFile)
                        comp.applyGlobalRtvs(loader.domainsMap[d], d)
                        fdeploy.LOGGER.info("created %s and filtering with id %s." % (comp.prettyprint(), id))
                        comps.append(comp)

        comps = []
        for component in deploy_descr:
            loader = None
            fdeploy.LOGGER.info( component['type'])
            if component['type'] == 'pcf-aggregate':
                component['type'] = 'pcf'
                loader =domainLoader(self.loadingDirectory)
                if not 'domain-config' in component.keys():
                    raise Exception('missing "domain-config" reference for "pcf-aggregate" type')
            applyDomain(comps, loader,component)
        for c in comps:
            def appender(self, comp, field, type):
                eve = eval("comp.%s" % (field))

                #print 'bw' in comp.id, 'bw' in comp.type, ", eve=", eve
                if '!' in type:
                    type = type.replace('!','')
                    if type not in eve:
                        self.components.append(comp)
                        fdeploy.LOGGER.info(
                            "added generated domain component '%s'. %s=%s" % (comp.id,field, eve))
                else:
                    if type in eve:
                        self.components.append(comp)
                        fdeploy.LOGGER.info(
                            "added generated domain component '%s'. %s=%s" % (comp.id,field, eve))
            if id is None:
                self.components.append(c)
            elif '#' in id:
                # TYPE filtering
                type=id.replace('#','')
                appender(self, c, 'type', type)
            else:
                # NAME filtering
                appender(self, c, 'id', id)


    def set_targets(self, _targets):
        self.targets = _targets

    def set_components(self, _comp_):
        __components = self.components
        _filtered_list = []
        for _c in __components:
            for __c in _comp_:
                _s, _i = __c
                if _s == '--all--':
                    fdeploy.LOGGER.info("selected all components.")
                    return
                if _c.id == _s:
                    _filtered_list.append(_c)
        self.components = _filtered_list

    def find_level(self,comp,level):
        if level is None:
            return None
        for _level in comp.levels:
            if not _level['level'] is None and _level['level'].strip() == level.strip():
                return _level
        return None

    def find_targets_in_level(self,comp,level):
        if type(level) != dict:
            _level = self.find_level(comp, level)
            if _level is None:
                leev = ", ".join(comp.get_levels())
                if self.ignore_level_errors == False:
                    raise Exception("no level '%s' found [available=%s] for %s from '%s'" % (level,leev,comp.id,comp.source))
                else:
                    fdeploy.LOGGER.warn("no level '%s' found [available=%s] for %s from '%s' IGNORED with --ignore-manifest-errors." % (level,leev,comp.id,comp.source))
            level = _level

        if level:
            if self.options.command != 'control':
                return fdeploy.flatten_level_targets(level['targets'])
            _group = self.options.group
            _regex = self.options.regex
            fdeploy.LOGGER.warn( self.options.__dict__ )
            _groupSize = -1 if self.options.group_size is None else self.options.group_size
            _groupNumber = -1 if self.options.group_number is None else self.options.group_number
            targets = fdeploy.flatten_level_targets(level['targets'], _group, _regex)
            if _groupSize > 0:
                _groupNumber = 0 if _groupNumber < 0 else _groupNumber
                groups = split(targets,_groupSize)
                fdeploy.LOGGER.debug(groups)
                fdeploy.LOGGER.info("targets grouped by %s in size, and selected group number %s" % (_groupSize, _groupNumber))
                if _groupNumber < len(groups):
                    return groups[_groupNumber]
                else:
                    raise KeyError("illegal group number %s must be >0 and <=%s for groupsize %s" % (_groupNumber, len(groups),_groupSize))
            else:
                return targets

        return []


    def generate_level(self, level):
        # using manifest resolver for nexus versions
        manifest_resolver = None
        resolver = None
        ##banner("OPTIONS_GENERATE: %s") %( self.options )
        _loaddir_ = '.' if self.loadingDirectory is None else self.loadingDirectory
        fdeploy.LOGGER.info(" > generating level %s for action '%s' from %s with %s components." %
                     (level, self.action, _loaddir_, len(self.components)))
        fdeploy.LOGGER.info(" > generating level %s for action '%s' with %s components." %
                     (level, self.action, len(self.components)))
        if len(self.components) < 1:
            banner("╔╗╔╔═╗  ╔═╗╔═╗╔╦╗╔═╗╔═╗╔╗╔╔═╗╔╗╔╦╗╔═╗  ╔═╗╔═╗╦ ╦╔╗╔╔╦╗")
            banner("║║║║ ║  ║  ║ ║║║║╠═╝║ ║║║║║╣ ║║║║ ╚═╗  ╠╣ ║ ║║ ║║║║ ║║")
            banner("╝╚╝╚═╝  ╚═╝╚═╝╩ ╩╩  ╚═╝╝╚╝╚═╝╝╚╝╩ ╚═╝  ╚  ╚═╝╚═╝╝╚╝═╩╝")
            sys.exit(1)
        manifest_resolver = None
        for comp in self.components:
            if comp.contents is not None and len(comp.contents) > 0:
                # use manifest only with nexus components gav
                manifest_resolver = create_manifest_resolver(_loaddir_, level)
        # create artifact resovler with caching features.
        self.resolver.set_manifest_resolver(manifest_resolver)
        resolver = self.resolver
        # some actions are not intented for by component level but by
        #   target level

        if not self.action in fdeploy.ACTIONS_HOSTS_ONLY:
            # - - - -
            # bailout=None
            j = 0
            for comp in self.components:
                j += 1
                fdeploy.LOGGER.info("processing component: %s of %s : %s of type %s" %
                             (j, len(self.components), comp, comp.type))
                _level = self.find_level(comp,level)
                bailout = None
                if _level is None and self.ignore_level_errors == True:
                    continue
                # loop thru all the content types and execute the appropriate
                # resolving or creation of these artifacts.
                i = 0
                for content in comp.contents:
                    i += 1
                    fdeploy.LOGGER.info("processing content: %s of %s : %s" %
                                 (i, len(comp.contents), content))
                    # execution loop
                    if self.action == 'run':
                        # execution of command does not need any resolving.
                        fdeploy.LOGGER.debug("execute(): %s" % (self.action))
                        self.___generate_content_by_component_targets(
                            self.options.output, comp, _level, target_key='execute')
                    else:
                        if comp.isNexusArtifact(content) == True:
                            # resolving contents for the component
                            #  = lookup versions in the manifest and resolve against Nexus
                            #  = downloading archives from specified in the content
                            fdeploy.LOGGER.trace("deterimator download: %s %s action_required? %s %s" % (comp, content, self.action in fdeploy.ACTIONS_WITH_NO_DOWNLOAD,
                                                                                                  self.options.dry_run or self.options.no_download))
                            no_download_flag = self.action in fdeploy.ACTIONS_WITH_NO_DOWNLOAD \
                                or self.options.dry_run or self.options.no_download
                            # special case where we need the query information from the deployment config
                            if (comp.lddtype == "tibco_bw" or comp.lddtype == "tibco_bw2") and self.action == 'publish':
                                fdeploy.LOGGER.info("downloading BW app for publishing")
                                no_download_flag = False
                            fdeploy.LOGGER.debug(" > action=%s, download flag = %s" % (
                                self.action, no_download_flag))
                            found = resolver.resolveGavs(
                                comp, _needs_nexus_resolving(self.action, no_download_flag), self.options.dry_run, _needs_manifest_resolving(self.action))
                            fdeploy.LOGGER.debug("found component: %s" % (found))
                            if found is None:
                                # next component, current comp not in manifest
                                fdeploy.LOGGER.warn(
                                    "skipping %s: no manifest entry." ,comp.id)
                                bailout = comp
                            else:
                                revisions = resolver.manifest_resolver().lookup_persisted(content.artifactId)
                                comp.versions_to_persist= revisions
                        elif comp.isAssembledArtifact(content) == True:
                            if self.action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING:
                                fdeploy.LOGGER.info("assembly content artifact %s" % (comp.id))
                                # assemble the artfact for inclusion in upload content
                                # artifact creator instance and feed the filter values
                                rtv_map={}
                                rtv_map,envvar_list=comp.get_env_vars(self.options, level, [])
                                fdeploy.LOGGER.debug("filter values: %s/%s" % (level, rtv_map))
                                creator = artifactArchiveCreator(_loaddir_, True)
                                creator.create(comp, content, rtv_map)
                        else:
                            #banner comp
                            raise Exception(
                                'Unable to create or resolve the artfiact content ' + str(content) + ".")
                    if bailout:
                        fdeploy.LOGGER.info("next component")
                        continue
                    else:
                        leev = []
                        for l in comp.levels:
                            leev.append(l['level'])
                        fdeploy.LOGGER.debug(
                            "generating  %s - %s (available %s)",level,  comp.id, ", ".join(leev))
                        if comp.isNexusArtifact(content) == True:
                            self.___generate_content_by_component_targets(
                                self.options.output, comp, _level)
                for service in comp.services:
                    fdeploy.LOGGER.info(service)
        else:
            # target focused calls only (meaning not component focused but host based)
            classref = {}
            target_refs = {}
            for comp in self.components:
                ref = "%s%s" % (comp.type, 'Generator')
                try:
                    _class = classref[ref]
                except:
                    fdeploy.LOGGER.trace(" module: %s, class=%s" %
                                  (comp.loader.__name__, ref))
                    fdeploy.LOGGER.trace(" resolved to class: %s for level %s" %
                                  (getattr(comp.loader, ref), level))
                    classref[ref] = getattr(comp.loader, ref)(
                        comp, self.options, self.base_dir)
                    _class = classref[ref]
                for _level in comp.levels:
                    if _level['level'] == level:
                        # assign empty array fo the target list
                        # of this generator type
                        if not _class in target_refs:
                            target_refs[_class] = []
                        _class.list_machines(
                            _level, target_refs[_class], self.options.group)

            if _class not in target_refs or len(target_refs[_class]) < 1:
                banner("╔╗╔╔═╗  ╦  ╔═╗╦  ╦╔═╗╦    ┌─%s─┐  ┌─┐┌─┐┬ ┬┌┐┌┌┬┐") % (level)
                banner("║║║║ ║  ║  ║╣ ╚╗╔╝║╣ ║    │ %s │  ├┤ │ ││ ││││ ││") % (level)
                banner("╝╚╝╚═╝  ╩═╝╚═╝ ╚╝ ╚═╝╩═╝  └─%s─┘  └  └─┘└─┘┘└┘─┴┘") % (level)
                sys.exit(1)

            # ---
            for _class in target_refs.keys():
                targets = target_refs[_class]
                filename = _class.__class__.__name__ + level + '.' + \
                    _class.extension if self.options.output is None else self.options.output
                full_path = self.base_dir + '/' + filename
                fdeploy.LOGGER.debug(" > writing '%s' for '%s' type in '%s'.",full_path, level, os.getcwd())
                with codecs.open(full_path, 'w', 'utf-8') as outfile:
                    # load dynamically the generator instance
                    # generating deployment script
                    _class.generate(
                        {'contents': None, 'targets': targets,
                            'level': _level, 'original_id' : 'all', 'uuid': 'all', 'directory' : self.loadingDirectory, 'readme' : self.readme },
                        outfile, self.action)
                outfile.close()
                fdeploy.LOGGER.info(" < wrote '%s' for action '%s'" ,filename, self.action)
                if filename not in comp.files:
                    comp.files.append(filename)
            if len(comp.files) < 1:
                banner("╔╗╔╔═╗  ╔═╗╔═╗╔╦╗╔═╗╔═╗╔╗╔╔═╗╔╗╔╦╗╔═╗  ╔═╗╔═╗╦ ╦╔╗╔╔╦╗")
                banner("║║║║ ║  ║  ║ ║║║║╠═╝║ ║║║║║╣ ║║║║ ╚═╗  ╠╣ ║ ║║ ║║║║ ║║")
                banner("╝╚╝╚═╝  ╚═╝╚═╝╩ ╩╩  ╚═╝╝╚╝╚═╝╝╚╝╩ ╚═╝  ╚  ╚═╝╚═╝╝╚╝═╩╝(level=$level)")
                sys.exit(1)
        return comp.files

    def ___generate_content_by_component_targets(self, filename, comp, levelStruct, target_key='targets'):
        fdeploy.LOGGER.debug(" < found level %s for %s type with id %s writing as %s." ,levelStruct, comp.type,comp.id, filename)
        # if target_key not in level or len(level[target_key]) < 1)
        _level = levelStruct
        ##banner str(comp.__dict__)
        for a in comp.__dict__:
            value = comp.__dict__[a]
            if (a == 'file' or a == 'regex') and not value is None:
                value = ",".join(value)
            fdeploy.LOGGER.trace(" %15s = %s " % ( a,value))
        _class,className = comp.get_generator_class(self.options,self.base_dir)
        # _class = getattr(comp.loader, comp.type +
        #                  'Generator')(comp, self.options, self.base_dir)
        filename = comp.id +str(uuid4())+ '.' + _class.extension if filename is None else filename
        full_path = self.base_dir + '/' + filename
        fdeploy.LOGGER.debug(" > writing '%s' for '%s' type in '%s'." ,full_path, comp.id, os.getcwd())
        # target_key = 'targets' or 'execute' to pick targets for installation or execution
        # #banner("target_key=%s => %s") % (target_key, _level[target_key])
        targets = self.find_targets_in_level(comp, levelStruct)
        # targets = fdeploy.flatten_level_targets(
        #     _level[target_key], self.options.group, self.options.regex)
        if targets is None or len(targets) == 0:
            raise Exception("no targets selection %s %s " % (levelStruct,comp))
        if self.targets is not None:
            fdeploy.LOGGER.debug("< using selected targets: %s", targets)
            targets = self.targets
        with open(full_path, 'w') as outfile:
            fdeploy.LOGGER.debug("> writing %s",full_path)
            # load dynamically the generator instance
            # generating deployment script
            rtvs = _level['rtv'] if 'rtv' in _level else []
            rtvs = comp
            #banner("\n\n\n%s\n\n\n") % (self.readme)
            _class.generate(
                {'contents': comp.contents,
                    'targets': targets, 'directory' : self.loadingDirectory,
                    'rtv': rtvs, 'level': levelStruct, 'original_id' : comp.original_id, 'uuid': comp.id, 'readme' : self.readme
                 }, outfile, self.action)
        outfile.close()
        if filename not in comp.files:
            comp.files.append(filename)
        fdeploy.LOGGER.info(" < wrote '%s' (%s#) for action '%s' by components ." % (
            filename, len(comp.files), self.action))

    def get_machines(self, level):
        for comp in self.components:
            for _level in comp.levels:
                if _level['level'] == level:
                    return {'targets': fdeploy.flatten_level_targets(
                        _level['targets'], self.options.group, self.options.regex)}
        return None

    def list_machines(self, level, report=True):
        machines = []
        for comp in self.components:
            _class = getattr(comp.loader, comp.type +
                             'Generator')(comp, self.options, self.base_dir)
            _tuple_level = self.get_machines(level)
            if _tuple_level is not None:
                _class.list_machines(_tuple_level, machines)
        if report == True:
            for machine in sorted(machines):
                fdeploy.LOGGER.info(machine)
        return sorted(machines)

    def publishing(self, options, level, useDNS = False):
        found = None
        lddService = lddPublisher(level, options, self.base_dir,self.resolver, useDNS=useDNS)
        fdeploy.LOGGER.info("publishing %s components in %s." ,len(self.components), level)
        _queue = []
        for comp in self.components:
            for _level in comp.levels:
                if level.startswith(_level['level']):
                    found = level
                    if (found != "PROD"):
                        if hasattr(comp, 'contents'):
                            nexusContent = None
                            for nexusContent in comp.contents:
                                if comp.isNexusArtifact(nexusContent):
                                    break
                            fdeploy.LOGGER.debug("action: %s" ,options.action)
                            if nexusContent is not None and comp.isNexusArtifact(nexusContent):
                                fdeploy.LOGGER.info(nexusContent)
                                if options.action == 'unpublish' :
                                    _queue = lddService.unpublish(level, nexusContent, comp)
                                elif options.action in ['publish','report'] :
                                    _queue = lddService.publish(level, nexusContent, comp)
                                else:
                                    raise Exception('illegal use of publishing method')
                            else:
                                fdeploy.LOGGER.warn("component content '%s' does not have publish capabilities." ,comp.type )
                        else:
                            print("publish , unpublish and report actions are disabled for PROD")
        fdeploy.LOGGER.info("prepared %s requests for LDD" % (len(_queue)))
        if found and len(lddService.queue) > 0:
            if options.action == 'report':
                lddService.reportRequests()
                return _queue
            elif options.action == 'unpublish':
                lddService.unpublishRequests()
                lddService.refresh(level)
                return _queue
            elif options.action == 'publish':
                lddService.publishRequests()
                lddService.refresh(level)
                return _queue
            else:
                raise Exception('illegal use of publishing method')
        else:
            return _queue

    def deploying(self, level, filelist):
        __class = None
        pause = 3
        statistics = []
        log_user = loggedinuser()
        for comp in self.components:
            for _level in comp.levels:
                if _level['level'] == level:
                    fdeploy.LOGGER.debug("deploying(): loading %sDeployer from %s" % (comp.type,comp.loader))
                    _class = getattr(comp.loader, comp.type +
                                     'Deployer')(comp, self.options, self.base_dir)
                    if __class is None:
                        __class = _class
                    # only us a longer pause when deploying or installing
                    pause = 6 if _needs_manifest_resolving(
                        self.action) == True else 3
                    # reporting.extend(
                    #fdeploy.LOGGER.trace(str(__class) + str(dir(_class)))
                    fdeploy.LOGGER.trace("comp= %s" % (comp) )
                    if (len(comp.files) != 0):
                        _class.deploying(comp.files, pause)
                    else:
                        if (len(comp.content) == 0):
                            fdeploy.LOGGER.info("> %s",comp.prettyprint())
                            fdeploy.LOGGER.warn("deploying(): no files found for %s", comp.prettyprint())
                        else:
                            _class.deploying(comp.files, pause)

                    # prepare statistics
                    if hasattr(comp, 'contents'):
                        rtv_map={}
                        rtv_map,envvar_list=comp.get_env_vars(self.options, level, [])
                        m = fdeploy.get_app_group(comp)
                        #app_group = "NA"
                        if m:
                            app_group = m
                        else:
                            app_group = rtv_map['EAI_NUMBER']  if 'EAI_NUMBER' in rtv_map else "NA"

                        found = None
                        for nexusContent in comp.contents:
                            if comp.isNexusArtifact(nexusContent):
                                found = nexusContent
                                break
                        if found:
                            statistics.append({'applicationNm': nexusContent.artifactId,
                                           'applicationVersion': nexusContent.version, 'level': level,
                                           'opco': app_group, 'releaseNm': 'NA','loggedInUser':log_user})
        if __class is None:
            banner("┌┐┌┌─┐  ┌─┐┌─┐┌┬┐┌─┐┌─┐┌┐┌┌─┐┌┐┌┬┐┌─┐  ┬┌┐┌  ╦  ┌─┐┬  ┬┌─┐┬  ")
            banner("││││ │  │  │ ││││├─┘│ ││││├┤ ││││ └─┐  ││││  ║  ├┤ └┐┌┘├┤ │  ")
            banner("┘└┘└─┘  └─┘└─┘┴ ┴┴  └─┘┘└┘└─┘┘└┘┴ └─┘  ┴┘└┘  ╩═╝└─┘ └┘ └─┘┴─┘[%s]") % (level)
            raise Exception("no components found! %s " % (level))
        subprocess.call(['sleep', str(pause)])
        if self.options.no_reporting == False:
            reports = [f for f_ in [glob.glob(e) for e in (str(
                __class.stage_directory) + "/deploy*", str(__class.stage_directory) + "/stdout*")] for f in f_]
            __class.report(reports)
        if self.options.upload == False:
            # send up statistics
            if self.options.action in fdeploy.ACTIONS_WITH_COMPONENT_HANDLING:
                if self.options.no_stats == False:
                    for req in statistics:
                        fdeploy.LOGGER.info("deploying(): sending statistics for %s",req['applicationNm'])
                        fdeploy.LOGGER.info("Deploying request : %s", req.items())
                        ldd_post_request(req,self.options)
