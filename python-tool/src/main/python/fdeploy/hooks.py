# -*- coding: utf-8 -*-

import fdeploy
import requests

#      mattermostSend  channel: '#town-square', endpoint: ' https://mattermost.paas.fedex.com/hooks/brcuuinskidoxq5kx7fnfr9mpw',
# message: "${subject}", text: "${icon} ${context}:${env.JOB_NAME}\n[${step} ${status} ${icon}] :Message: ${statusMessage}#\n[SEE CONSOLE](${env.BUILD_URL}console)"


def mattermost(message,url='https://mattermost.paas.fedex.com', hook='hooks/brcuuinskidoxq5kx7fnfr9mpw'):

    payload={
      "channel": "town-square",
      "icon_url": "http://uwb00078.ute.fedex.com/images/fsharp.png",
      "text": message
      }
    ret = requests.post(
        '%s/%s' % (url,hook), json=payload, proxies=fdeploy.FDX_PROXIES)
    response = 'successful' if ret.ok in [
        200, 201] or ret.ok == True else "failed: code %s" % (ret.ok)

#curl -i -X POST --data-urlencode 'payload={"text": "Hello, this is some text\nThis is more text. :tada:"}' http://{your-mattermost-site}/hooks/xxx-generatedkey-xxx

# request structure : {'applicationNm': u'fxf-java-pickup-and-delivery', 'applicationVersion': '2.4.16', 'loggedInUser': '751818', 'level': 'L1', 'opco': 'FXF', 'releaseNm': 'NA'}

# def post(queue, lastrec):
#     '''
#             return {'deployId': str(deployId), 'artifactId' : str(artifact.artifactId), 'version' : str(artifact.version), \
#                 'level' : str(level), 'opco' : str(app_group), 'queryHost' : str(cname), \
#                 'host' : str(fqname), 'queryCmd' : str(query), 'type' : str(_type)}
# '''
#     ret = requests.post(requrl, json=queue, proxies=fdeploy.FDX_PROXIES)
#     response = 'successful' if ret.ok in [200, 201] or ret.ok == True else "failed: code %s" % (ret.ok)
#     fdeploy.LOGGER.info("last artifact registered: %s %s - %s" % (message, req['artifactId'], req['version']))
#     message = "Deployed: %s-%s to %s for %s", artifact.artifactId, req['applicationVersion'],req['level'],req['opco']


def ldd_post_request(req, options, operation='create', url='http://c0009869.test.cloud.fedex.com:8090/sefs-dashboard'):
    req = req if type(req) == list else [req]
    proxies = []
    # if fdeploy.IS_ON_FEDEX_NETWORK == True:
    #     proxies = fdeploy.FDX_PROXIES
    #     os.environ['NO_PROXY']='127.0.0.1'
    #     fdeploy.LOGGER.error('Not running in fedex network, disabling proxy settings')
    if 'ldd_url' in options:
        url = options['ldd_url']
        fdeploy.LOGGER.warn("publishing to LDD with custom url: %s" % (options['ldd_url']))
    fdeploy.LOGGER.warn("proxies: %s (%s)" % (proxies,fdeploy.IS_ON_FEDEX_NETWORK))
    ret = requests.post(
        '%s/api/public/deploy/%s' % (url,operation), json=req, proxies=proxies)
    response = 'successful' if ret.ok in [
        200, 201] or ret.ok == True else "failed: code %s" % (ret.ok)
    fdeploy.LOGGER.debug("upload ldd %s on request %s", response, req)
    message = "Deployed by %s: %s-%s to %s for %s", req[0]['loggedInUser'], req[0]['applicationNm'], req[0]['applicationVersion'],req[0]['level'],req[0]['opco']
    #mattermost(message)
    return response,ret
