
import os
import pytest
import fdeploy
import shutil
from fdeploy.fdeployLoader import split, loggedinuser, create_manifest_resolver
from fdeploy import level2code
from fdeploy.content.fdeployComponent import fdeployComponent
from fdeploy.UtilsForTesting import get_options, compare_files
from fdeploy.fdeployLoader import fdeployLoader
from fdeploy.MockHttpServer import start_mock_server
import BaseHTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler

REQUIRED_RTVS = []
REGISTERED_ACTIONS = ['clean', 'wipeout', 'start', 'stop', 'install', 'stage', 'ml', 'machine_list',
                      'deploy', 'publish', 'unpublish', 'restart', 'report', 'ssh', 'inspect', 'ps', 'cloudenv', 'status']
# ls -lrt ../../test/resources/FXE_fdeploy/*.json | wc -l
TOTAL_COUNT = 44
JAVA_COUNT = 8
BW_COUNT = 17
BW_NAME_COUNT = 16
BE_COUNT = 10


class testGenerator(fdeploy.platform.baseGenerator):

    def __init__(self, component, cli_options):
        self.REGISTERED_ACTIONS = fdeploy.test_FdeployLoader.REGISTERED_ACTIONS
        self.REQUIRED_RTVS = fdeploy.test_FdeployLoader.REQUIRED_RTVS
        super(testGenerator, self).__init__(
            component, cli_options)

def create_component():
    return fdeployComponent({
        'id': 'componentId',
        'platform': 'bash_generator',
        'type': 'pcf',
        'filter':
            {"var1": "value1", "var2": "value2"},
        'content':  [{
            "gav": "com.fedex.sefs.core:sefs_suAddress:6.1.0:zip",
            "relativePath": "${level}/${var1}.sh",
            "saveArchiveName": "sefs_suAddress.zip"
        }],
        'name_format': 'format',
        'levels':
        [{
            'level': 'L1',
            'rtv': [{'name': 'JMX_PORT', 'value': '40313'}],
            'targets': ['a@fedex.com', 'b@fedex.com', 'c@fedex.com', 'd@fedex.com', 'e@fedex.com']
        }, {
            'level': 'L2',
            'targets': ['a@fedex.com', 'b@fedex.com', 'c@fedex.com', 'd@fedex.com', 'e@fedex.com', 'f@fedex.com', 'g@fedex.com', 'h@fedex.com', 'i@fedex.com', 'j@fedex.com']
        }, {
            'level': 'L3',
            'targets': ['a@fedex.com']
        }],
        'rtv': [
                {'name': 'JAVA_HOME',
                 'value': '/opt/java/hotspot'
                 },
             {'name': 'JMX_PORT', 'value': 'ABCDEF'},
                {'name': "APPMANAGE_XML", 'value': "/var/tmp/${level}/appmanage.xml"}
            ]

    }, 'config.FXE/inline.json')


class LDDServiceHandler(BaseHTTPRequestHandler):
    timeout = 5

    def setup(self):
        BaseHTTPServer.BaseHTTPRequestHandler.setup(self)
        self.request.settimeout(5)

    def do_POST(self):
        self.send_response(requests.codes.ok)
        # Add response headers.
        self.send_header('Content-Type', 'application/json; charset=utf-8')
        self.end_headers()
        response_content = json.dumps([])
        fdeploy.LOGGER.debug("received: %s", response_content)
        self.wfile.write(response_content.encode('utf-8'))
        return

os.environ['NO_PROXY']='127.0.0.1'
fdeploy.is_on_fedex_network(True)
TEST_HTTP_PORT = start_mock_server(LDDServiceHandler)


class TestFdeployLoader(object):

    REQUIRED_RTVS = ['F_HOME', 'G_HOME']
    cdw = None

    def setup_method(self, method):
        if os.path.exists('tmp'):
            shutil.rmtree('tmp')
        #ldd.test_LddPublisher.TEST_HTTP_PORT = start_mock_server(LDDServiceHandler)
        self.cdw = os.path.abspath("%s/../../test/resources" % (os.getcwd()))

    def test_level2code(self):
        assert 'u' == level2code('L1')
        assert '?' == level2code('LB')

    def test_read_descriptor_not_exist(self):
        fdl = fdeployLoader(get_options())
        with pytest.raises(Exception):
            fdl.readDescriptors(['../../test/resources/does-not-exist.json'])

    def test_cups_types(self):
        fdl = fdeployLoader(get_options())
        fdl.readDescriptors(
            ['../../test/resources/pcf-config/pcf-cups-config.json'])

    def test_loggedinusers(self):
        assert 'anonymous' != loggedinuser()

    def test_read_descriptor_filter_id(self):
        o = get_options()
        o.id = 'bw'
        fdl = fdeployLoader(o)
        fdl.readDescriptors(['../../test/resources/FXE_fdeploy/*.json'])
        assert fdeploy.test_FdeployLoader.BW_NAME_COUNT == len(fdl.components)
        len(fdl.components)
        o = get_options()
        o.id = None
        fdl = fdeployLoader(o)
        fdl.readDescriptors(['../../test/resources/FXE_fdeploy/*.json'])
        assert fdeploy.test_FdeployLoader.TOTAL_COUNT == len(fdl.components)
        o = get_options()
        o.id = '!bw'
        fdl = fdeployLoader(o)
        fdl.readDescriptors(['../../test/resources/FXE_fdeploy/*.json'])
        assert fdeploy.test_FdeployLoader.TOTAL_COUNT - fdeploy.test_FdeployLoader.BW_NAME_COUNT == len(fdl.components)

    def test_read_descriptor_filter_type(self):
        o = get_options()
        fdl = fdeployLoader(o)
        fdl.readDescriptors(['../../test/resources/FXE_fdeploy/*.json'])

        assert fdeploy.test_FdeployLoader.TOTAL_COUNT == len(fdl.components)
        o = get_options()
        o.id = '#java'
        fdl = fdeployLoader(o)
        fdl.readDescriptors(['../../test/resources/FXE_fdeploy/*.json'])
        assert fdeploy.test_FdeployLoader.JAVA_COUNT  == len(fdl.components)
        neg = fdeploy.test_FdeployLoader.TOTAL_COUNT- fdeploy.test_FdeployLoader.JAVA_COUNT
        o = get_options()
        o.id = '!#java'
        fdl = fdeployLoader(o)
        fdl.readDescriptors(['../../test/resources/FXE_fdeploy/*.json'])
        assert neg == len(fdl.components)

    def test_read_descriptor(self):
        fdl = fdeployLoader(get_options())
        fdl.readDescriptors(['../../test/resources/java-cache-mixin.json'])
        assert 1 == len(fdl.components)
        comp = fdl.components[0]
        assert 'sefs-fxs-java-probes' == comp.id
        assert 2 == len(comp.rtv)
        assert "F_HOME" == comp.rtv[1]["name"]
        assert "/opt/home/fbs" == comp.rtv[1]["value"]

    def test_get_env_vars(self):
        fdl = fdeployLoader(get_options())
        fdl.readDescriptors(['../../test/resources/java-cache-mixin.json'])
        assert 1 == len(fdl.components)
        comp = fdl.components[0]
        env, envvar_list = comp.get_env_vars(get_options(), 'L1', [])
        assert not env is None
        assert '/opt/delta/DL2180/Lihue' == env['F_HOME']
        assert '/opt/home/gbs' == env['G_HOME']
        env, envvar_list = comp.get_env_vars(get_options(), 'L2', [])
        assert '/opt/home/fbs' == env['F_HOME']
        assert '/opt/home/gbs' == env['G_HOME']
        assert 'probe-client' == env['ARTIFACTID']
        assert 'com.fedex.sefs.dashboard' == env['GROUPID']

    def test_get_env_vars_exterenal_overwrite(self):
        o = get_options()
        o.rtv.append("JAVA_HOME=/opt/java/hotspot/9/latest")
        fdl = fdeployLoader(o)
        fdl.readDescriptors(['../../test/resources/java-cache-mixin.json'])
        assert 1 == len(fdl.components)
        comp = fdl.components[0]
        env, envvar_list = comp.get_env_vars(o, 'L1', REQUIRED_RTVS)
        assert not env is None
        assert '/opt/java/hotspot/9/latest' == env['JAVA_HOME']

        # format error rtv specification
        o.rtv = ["JAVA_HOME"]
        fdl = fdeployLoader(o)
        with pytest.raises(IndexError):
            comp.get_env_vars(o, 'L1', [])

    def test_get_env_vars_missing_required_RTV(self):
        o = get_options()
        o.rtv.append("JAVA_HOME=/opt/java/hotspot/9/latest")
        fdl = fdeployLoader(o)
        fdl.readDescriptors(
            ['../../test/resources/java-cache-mixin-missing.json'])
        assert 1 == len(fdl.components)
        comp = fdl.components[0]
        env, envvar_list = comp.get_env_vars(o, 'L1', REQUIRED_RTVS)
        assert not env is None
        #print env
        assert '/opt/java/hotspot/9/latest' == env['JAVA_HOME']

        # format error rtv specification
        o = get_options()
        o.rtv.append("JAVA_HOME")
        fdl = fdeployLoader(o)
        with pytest.raises(IndexError):
            comp.get_env_vars(o, 'L1', [])

    def test_read_FXE(self):
        fdl = fdeployLoader(get_options())
        fdl.readDescriptors(
            ['../../test/resources/FXE_fdeploy/fxe-be-sefsShipment_ClearancePU.json'])
        assert 1 == len(fdl.components)
        # print "\n\t\t%s\n" % (fdl.components)
        env, envvar_list = fdl.components[0].get_env_vars(
            get_options(), 'L1', REQUIRED_RTVS)
        assert not env is None
        assert 'urh00606.FXE_SEFS_SHIPMENT_CLEARANCE_1' == env['sefs_suShipment_AS_MEMBER_NAME']


    def test_level_replacement_in_rtv(self):
        os.environ['level'] = 'L1'

        loader = fdeployLoader(
            get_options(), importComponents=[create_component()])
        assert 1 == len(loader.components)
        comp = loader.components[0]
        assert not comp.rtv[0]['name'] is None
        rtv_map, envvar_list = comp.get_env_vars(get_options(), 'L1', [])
        assert not rtv_map['APPMANAGE_XML'] is None
        assert "/var/tmp/L1/appmanage.xml" == rtv_map['APPMANAGE_XML']
        assert "40313" == rtv_map['JMX_PORT']

        rtv_map, envvar_list = comp.get_env_vars(get_options(), 'L2', [])
        assert not rtv_map['APPMANAGE_XML'] is None
        assert "/var/tmp/L1/appmanage.xml" == rtv_map['APPMANAGE_XML']
        assert "ABCDEF" == rtv_map['JMX_PORT']


    def test_get_persisted_manifest_values1(self):
        options = get_options()
        options.level = 'L2'
        options.action = 'clean'
        manifest_resolver = create_manifest_resolver('../../test/resources', 'L2')
        fdl = fdeployLoader(get_options())
        fdl.action = 'clean'
        fdl.readDescriptors(
            ['../../test/resources/java-cache.json'])
        #print "version" ,manifest_resolver.lookup_persisted('probe-client')
        #print manifest_resolver.lookup_persisted('dashboard')
        assert 1 == len(fdl.components)
        # take second component
        env, envvar_list = fdl.components[0].get_env_vars(
            get_options(), 'L2', REQUIRED_RTVS)
        assert not env is None
        env, envvar_list = fdl.components[0].get_env_vars(
            get_options(), 'L2', REQUIRED_RTVS)
        fdl.generate_level('L2')
        assert 'probe-client' == env['ARTIFACTID']
        assert 'com.fedex.sefs.dashboard' == env['GROUPID']
        assert ['1.3.0-SNAPSHOT','1.4.1','1.3.1'] == fdl.components[0].versions_to_persist
        compare_files('tmp/myscript.sh', '../../test/resources/myscript.sh')


    def test_get_persisted_manifest_values_for_share_cache_used_inplatformtest(self):
        options = get_options()
        options.level = 'L1'
        options.action = 'clean'
        manifest_resolver = create_manifest_resolver('../../test/resources/FXE_fdeploy', 'L1')
        fdl = fdeployLoader(get_options())
        fdl.action = 'clean'
        fdl.readDescriptors(
            ['../../test/resources/FXE_fdeploy/fxe-share-cache-plugin.json'])
        #print "version" ,manifest_resolver.lookup_persisted('share-cache-plugin')
        assert ['1.0.3', '0.0.1', '0.1.0'] ==  manifest_resolver.lookup_persisted('share-cache-plugin')
        assert 1 == len(fdl.components)
        # versions to persist is only found at generation time
        assert 0 == len(fdl.components[0].versions_to_persist)
        env, envvar_list = fdl.components[0].get_env_vars(
            get_options(), 'L1', REQUIRED_RTVS)
        assert not env is None
        fdl.generate_level('L1')
        env, envvar_list = fdl.components[0].get_env_vars(
            get_options(), 'L1', REQUIRED_RTVS)
        # versions to persist is only found after generation time
        assert 3 == len(fdl.components[0].versions_to_persist)
        assert 'share-cache-plugin' == env['ARTIFACTID']
        assert 'com.fedex.sefs.cache.plugins' == env['GROUPID']
        assert ['1.0.3', '0.0.1', '0.1.0'] == fdl.components[0].versions_to_persist
        assert '1.0.3,0.0.1,0.1.0' == env['CANDIDATES']
        compare_files('tmp/myscript.sh', '../../test/resources/myclean-persist.sh')


    def test_get_env_vars_2in1(self):
        fdl = fdeployLoader(get_options())
        fdl.readDescriptors(
            ['../../test/resources/java-cache-mixin-multiple-comps-1file.json'])
        assert 2 == len(fdl.components)
        # take second component
        env, envvar_list = fdl.components[1].get_env_vars(
            get_options(), 'L1', REQUIRED_RTVS)
        assert not env is None
        with pytest.raises(KeyError):
            assert '/opt/delta/DL2180/Lihue' == env['F_HOME']
        assert '/opt/linux/level1_z' == env['Z_HOME']
        env, envvar_list = fdl.components[1].get_env_vars(
            get_options(), 'L2', REQUIRED_RTVS)
        assert '/opt/linux/src/global_z' == env['Z_HOME']
        assert 'probe-client' == env['ARTIFACTID']
        assert 'com.fedex.sefs.dashboard' == env['GROUPID']

    def test_read_descriptor_2in1(self):
        fdl = fdeployLoader(get_options())
        fdl.readDescriptors(
            ['../../test/resources/java-cache-mixin-multiple-comps-1file.json'])
        assert 2 == len(fdl.components)
        # validating structure
        comp = fdl.components[0]
        assert 'sefs-fxs-java-probes' == comp.id
        assert 2 == len(comp.rtv)
        assert "F_HOME" == comp.rtv[1]["name"]
        assert "/opt/home/fbs" == comp.rtv[1]["value"]
        comp = fdl.components[1]
        assert 'sefs-fxs-java-probes2' == comp.id
        assert 2 == len(comp.rtv)
        assert "Z_HOME" == comp.rtv[1]["name"]
        assert "/opt/linux/src/global_z" == comp.rtv[1]["value"]
        # first level(0) is
        assert "/opt/linux/level1_z" == comp.levels[0]['rtv'][0]["value"]

    def test_find_targets(self):
        test_component = create_component()
        loader = fdeployLoader(
            get_options(), importComponents=[create_component()])
        assert 1 == len(loader.components)
        fdeploy.LOGGER.debug(loader.components[0].__dict__.keys())
        assert 'levels' in loader.components[0].__dict__.keys()
        comp = loader.components[0]
        assert 3 == len(comp.levels)
        fdeploy.LOGGER.debug(comp.levels[0]['level'])
        assert 'L1' == comp.levels[0]['level']
        assert 5 == len(comp.levels[0]['targets'])
        #loader.find_targets_in_level(test_component, level='L1')
        assert 5 == len(loader.find_targets_in_level(test_component, 'L1'))
        assert 10 == len(loader.find_targets_in_level(test_component, 'L2'))
        assert 1 == len(loader.find_targets_in_level(test_component, 'L3'))

    def test_split(self):
        a = ['a', 'b', 'c', 'd', 'e']
        aa = split(a, 2)
        assert 3 == len(aa)
        assert 'e' == aa[2][0]

    def test_get_generator_class(self):
        test_component = create_component()
        o=get_options()
        o.action = 'properties'
        loader = fdeployLoader(
            get_options(), importComponents=[create_component()])
        test_component.type = 'java'
        clz, name = test_component.get_generator_class(get_options(), ".")
        assert "javaGenerator" == name
        test_component.type = 'bla'
        with pytest.raises(AttributeError):
            clz, name = loader.get_generator_class(test_component)

    def test_find_targets_by_groupsize(self):
        test_component = create_component()
        loader = fdeployLoader(
            get_options(), importComponents=[create_component()])
        loader.options.group_size=-1
        assert 5 == len(loader.find_targets_in_level(test_component, 'L1'))
        assert 10 == len(loader.find_targets_in_level(test_component, 'L2'))
        assert 1 == len(loader.find_targets_in_level(test_component, 'L3'))
        loader.options.group_size = 2
        assert 2 == len(loader.find_targets_in_level(test_component, 'L1'))
        assert 2 == len(loader.find_targets_in_level(test_component, 'L2'))
        assert 1 == len(loader.find_targets_in_level(test_component, 'L3'))

    def test_get_targets_all(self):
        test_component = create_component()
        assert 3 ==  len(test_component.levels)
        loader = fdeployLoader(
            get_options(), importComponents=[create_component()])
        base = testGenerator(test_component, {'action': 'status'})
        level = loader.find_level(test_component, 'L1')
        __targets = fdeploy.flatten_level_targets(level['targets'])
        assert 5 == len(__targets)
        with pytest.raises(Exception):
            loader.find_targets_in_level(test_component, 'L11')

    def test_get_target_group(self):
        test_component = create_component()
        loader = fdeployLoader(
            get_options(), importComponents=[create_component()])
        base = testGenerator(test_component, {'action': 'status'})
        level = loader.find_level(test_component, 'L1')
        __targets = fdeploy.flatten_level_targets(level['targets'])
        assert 5 == len(__targets)
        loader.options.group_size = 3
        __grouped = loader.find_targets_in_level(test_component, 'L1')
        assert 3 == len(__grouped)
        assert 'a@fedex.com' == __grouped[0]
        with pytest.raises(KeyError):
            loader.options.group_number = 2
            __grouped = loader.find_targets_in_level(
                test_component, 'L1')
        loader.options.group_number = 1
        __grouped = loader.find_targets_in_level(test_component, 'L1')
        assert 2 == len(__grouped)
        assert 'd@fedex.com' == __grouped[0]
        loader.options.group_number = 0
        __grouped = loader.find_targets_in_level(test_component, 'L1')
        assert 3 == len(__grouped)
        assert 'b@fedex.com' == __grouped[1]

    def test_pcf_domain_loader_aggregate_deploy(self):
        loader = fdeployLoader( get_options())
        loader.readDescriptors(['../../test/resources/domainloader/uuid-common.json'])
        assert len(loader.components) == 2

        assert loader.components[0].rtvMapping['APPD_NM'] == 'waypoint-proxy-server'
        assert loader.components[1].rtvMapping['APPD_NM'] == 'itinerary-proxy-service'
        assert loader.components[0].id == 'waypoint'
        assert loader.components[1].id == 'itinerary'
        assert loader.components[1].rtvMapping['EAI_NUMBER'] == '3534642'
        assert loader.components[0].rtvMapping['EAI_NUMBER'] == '3534645'

        with pytest.raises(Exception):
            loader.deploying('L1',['tmp/archive/t3x.sh'])

    def test_pcf_domain_loader_aggregate_generate(self):
        loader = fdeployLoader( get_options())
        # select only one domain
        loader.options.id = 'waypoint'
        loader.options.no_download = True
        loader.action = 'install'
        #print loader.options.__dict__
        loader.readDescriptors(['../../test/resources/domainloader/uuid-common.json'])
        assert len(loader.components) == 1

        assert loader.components[0].rtvMapping['APPD_NM'] == 'waypoint-proxy-server'
        assert loader.components[0].id == 'waypoint'
        assert loader.components[0].rtvMapping['EAI_NUMBER'] == '3534645'

        assert loader.components[0].id != loader.components[0].original_id
        assert loader.components[0].original_id == 'common-config-service'
        loader.generate_level('L1')

        compare_files('tmp/myscript.sh', '../../test/resources/domainloader/myscript.sh')


    def test_publishing_unpublish(self):
        test_component = create_component()
        options = get_options()
        options.action = 'publish'
        options.defaults['nexusURL'] = 'file://%s/local' % (self.cdw)
        options.defaults['ldd_url'] = 'http://127.0.0.1:%s' % (TEST_HTTP_PORT)
        loader = fdeployLoader(
            get_options(), importComponents=[create_component()])
        found = loader.resolver.resolveGavs(test_component, True, True, False)
        assert found is not None
        #stest_component.contents[0]=found
        assert test_component.isNexusArtifact(test_component.contents[0])
        #assert test_component == loader.components[0]
        #loader.lddService.level = 'L1'
        q = loader.publishing(options,'L1')
        assert len(q) == 5
        options.action = 'unpublish'
        q = loader.publishing(options,'L1')
        assert len(q) == 5
        options.action = 'report'
        q = loader.publishing(options,'L1')
        assert len(q) == 5
