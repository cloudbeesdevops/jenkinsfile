# -*- coding: utf-8 -*-
from fnexus.gavClass import gavClass
from fdeploy.content.contentClass import contentClass
from fdeploy.platform.pcf.cups_class import cupsCredentialsClass, cupsJmsProviderClass,\
        cupsConfigVariableClass, cupsDatabaseServiceClass, cupsGitConfigServerClass,\
        cupsConfigVariableClass, cupsUriClass, cupsGitConfigHttpServerClass
from fdeploy.platform.pcf import envClass, taskClass
from fdeploy.platform.pcf.service_class import serviceAppdClass, serviceAutoScalerClass, serviceSplunkClass
from fdeploy.platform.pcf.command_class import commandClass
import fdeploy

'''
CONTENT FACTORY
'''
def content_factory(content):
    if content is None:
        raise Exception("No content specified!")
    if 'gav' in content:
        return gavClass(content)
    elif 'directory' in content:
        return contentClass(content)
    else:
        raise Exception("ContentException: " + str(content))

'''
SERVICE FACTORY
'''
def service_factory(content,name=None):
    if content is None:
        raise Exception("No service specified!")
    if 'class' in content and content['class'] == 'cups' and 'type' in content:
        try:
            for type in ['Credentials','JmsProvider','GitConfigHttpServer', 'GitConfigService', 'ConfigVariable', 'Appd', 'Splunk', 'DatabaseService', 'Uri', 'Scale']:
                clazz = 'cups'
                if content['type'] == type:
                    if type == 'GitConfigService':
                        type = 'GitConfigServer'
                    if type in ['Appd', 'Splunk']:
                        clazz = 'service'
                    return eval("%s%sClass(name,content)" % (clazz,type))
        except Exception as e:
            raise Exception("failure to instantiate service : %s. Reason: %s" % (content, e))
        if content['type'] == 'Env':
            return envClass(name,content)
        else:
            raise Exception('%s is unregistered service type.' % (content['type']))
    elif 'class' in content and content['class'] == 'command':
        if content['type'] == 'Inline':
            return commandClass(name,content)
    elif 'class' in content and content['class'] == 'service':
        try:
            #for type in ['Appd', 'Splunk', 'AutoScaler']:
            return eval("service%sClass(name,content)" % (content['type']))
        except Exception as e:
            raise Exception("failure to instantiate service %s: %s. Reason: %s" % (content['type'], content,e))
    else:
        raise Exception("ServiceException: " + str(content))


'''
SERVICE RESOLVER
'''
def infrastructure_resolver(path, content):
    # avioding circular dependency
    from fdeploy.serviceLoader import serviceLoader
    if content is None:
        raise Exception("No infrastructure specified!")
    if not 'service-config' in content.keys():
        raise Exception("No infrastructure configuration reference specified!")
    if not 'depends-on' in content.keys():
        raise Exception("No dependency 'depends-on' reference specified!")
    loader = serviceLoader(path)
    fdeploy.LOGGER.info("loading services from '%s'", content['service-config'])
    loader.readDescriptors(content['service-config'])
    # depends-on
    services = []
    for name in [str(x).strip() for x in content['depends-on'].split(',')]:
        fdeploy.LOGGER.debug("depends on : %s",name)
        if not name in loader.serviceMap.keys():
            raise Exception("service %s not found in service mapping: %s" % (name, loader.serviceMap.keys()))
        else:
            services.append(loader.serviceMap[name])
    return services
