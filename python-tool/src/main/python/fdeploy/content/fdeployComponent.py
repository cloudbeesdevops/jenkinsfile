import re
import fdeploy
import sys
import os
from fnexus.gavClass import gavClass
from fdeploy.content.contentClass import contentClass
import fdeploy.content
import importlib
from fdeploy.security import inline_singleton, is_encapsulated
#import fdeploy.fdeployLoader

class fdeployComponent(object):

    cryptor = None

    def __init__(self, dict, source):
        if dict is None:
            return None
        self.id = None
        self.dict = dict
        self.levels = dict['levels']
        if 'rtv' in dict:
            self.rtv = dict['rtv']
        else:
            self.rtv = []
        self.id = dict['id']
        self.original_id = dict['id']
        self.platform = dict['platform']
        self.type = dict['type']
        self.lddtype = re.sub(r'tibco_', '', dict['type'])
        self.lddtype = re.sub(r'tibco', '', self.type)
        self.lddtype = re.sub(r'leech', '', self.type)

        self.files = []
        self.source = source
        self.execute = dict['execute'] if 'execute' in dict else None
        self.name_format = dict['name_format'] if 'name_format' in dict else None
        self.content = dict['content'] if 'content' in dict else []
        self.service = dict['service'] if 'service' in dict else []
        self.filter = dict['filter'] if 'filter' in dict else {}
        importlib.import_module("fdeploy.platform." + self.platform)
        self.loader = sys.modules["fdeploy.platform." + self.platform]
        self.services = []
        self.servicertv = []
        self.infrastructure = dict['infrastructure'] if 'infrastructure' in dict else None
        self.domain_deploy = dict['domain_deploy'] if 'domain_deploy' in dict else None
        self.domains = []
        self.contents = []
        self.rtvMapping = {}
        for item in self.rtv:
            name = str(item['name'])
            value = str(item['value'])
            if is_encapsulated(value):
                value = inline_singleton().decrypt(value)
                item['value']=value
            self.rtvMapping[name] = value

        self.archivePath = None
        # use in combination with the manifest file [PERSIST_MANIFEST] section to
        # keep certain versions on disk all others would be deleted on a clean.
        self.versions_to_persist = []
        fdeploy.LOGGER.trace(self.dict)
        # if self.domain_deploy:
        #     self.domains.extend(fdeploy.content.domain_deploy_resolver(
        #         os.path.dirname(os.path.abspath(self.source)), self.domain_deploy))
        # fdeploy.LOGGER.info( self.domains )
        #
        # if self.domains:
        #     self.servicertv.extend(fdeploy.content.domain_deploy_resolver(
        #         os.path.dirname(os.path.abspath(self.source)), self.domain_deploy,"domain_rtv"))
        #fdeploy.LOGGER.info( self.servicertv )

        # if self.servicertv:
        #     servicertvMapping = {}
        #     if (type(self.servicertv) == list ):
        #         for item in self.servicertv:
        #             for _domains in self.domains:
        #                 if  item[_domains] :
        #                     for _item in item[_domains]:
        #                         name = str(_item['name'])
        #                         servicertvMapping[name] = str(_item['value'])
        #     self.rtvMapping.update(servicertvMapping)

        if self.infrastructure:
            self.services.extend(fdeploy.content.infrastructure_resolver(
                os.path.dirname(os.path.abspath(self.source)), self.infrastructure))
        fdeploy.LOGGER.info('infrastructure elements %s' %  ( self.infrastructure ))

        if self.content:
            for _cont in self.content:
                self.contents.append(fdeploy.content.content_factory(_cont))
        if self.service:
           for _serv in self.service:
               self.services.append(fdeploy.content.service_factory(_serv))
        # validate if there are duplicate level names in the descriptor
        dups = []
        for _lvl in self.levels:
            if _lvl['level'] not in dups:
                dups.append(_lvl['level'])
            else:
                raise Exception("Duplicate level assignment in descriptor for %s at level %s" % (
                    self.id, _lvl['level']))
        fdeploy.LOGGER.trace(self.__dict__)

    def applyGlobalRtvs(self, rtvs, comp_id):
        for rtv in rtvs:
            self.rtvMapping[rtv['name']]=rtv['value']
        if comp_id is not None:
            self.id = comp_id

    def isNexusArtifact(self, _content):
        var_ = isinstance(_content, gavClass)
        fdeploy.LOGGER.debug("isNexusArtifact: content = " +
                      str(type(_content)) + " : " + str(_content) + str(var_))
        return var_

    def isAssembledArtifact(self, _content):
        var_ = isinstance(_content, contentClass)
        fdeploy.LOGGER.debug("isAssembledArtifact: content = " +
                      str(type(_content)) + " : " + str(_content) + str(var_))
        return var_

    def hasExecution(self):
        return False if self.execute is None else self.execute

    def prettyprint(self):
        return "%s(%s) - %s" % (self.id, self.type, self.platform)

    def dump(self):
        #LOGGER.debug(json.dumps(self.dict, sort_keys=True, indent=4, separators=(',', ': '))
        pass

    def get_generator_class(self,options,base_dir):
        ref = "%s%s" % (self.type, 'Generator')
        #self.set_target_list(classref,target_refs,machines)
        if options and type(options) == dict and len(options.keys()) == 0:
            fdeploy.LOGGER.warn("options supplied is empty.")
        fdeploy.LOGGER.debug(" module: %s, class=%s" % (self.loader.__name__, ref))
        _class = getattr(self.loader, ref)
        fdeploy.LOGGER.debug(" resolved to class: %s" % (_class) )
        _class = getattr(self.loader, ref)(self, options, base_dir) #, sys.modules[_class.__module__].TARGET_URL_SPEC)
        return _class,ref

    def get_level(self,lword):
        for l in self.levels:
            if l['level'] == lword:
                return l
        raise KeyError("%s not present in json" % (lword))

    def get_levels(self):
        leev=[]
        for l in self.levels:
            leev.append(l['level'])
        return leev

    def get_rtvs(self, basemap, comp_sources):
        for sources in comp_sources:
            _value = os.path.expandvars(sources['value'].strip())
            basemap[str(sources['name'].strip())]=str(_value)

    def get_env_vars(self, options, levelStruct, REQUIRED_RTVS, propertyVersion=None, init=None):
        fdeploy.LOGGER.trace("current_raw_rtvs=%s" % (self.rtv))
        if init is None:
            for item in self.rtv:
                name = str(item['name'])
                self.rtvMapping[name] = str(item['value'])

        if init:
            self.rtvMapping.update(init)
        envvar_list=""
        if propertyVersion is not None:
            self.rtvMapping['VERSION'] = propertyVersion
        if self.content and 'gav' in self.content[0]:
            contentObject = fdeploy.content.content_factory(self.content[0])
            self.rtvMapping['ARTIFACTID'] = str(contentObject.artifactId)
            self.rtvMapping['GROUPID'] = str(contentObject.groupId)
            try:
                if contentObject.version is not None:
                    self.rtvMapping['VERSION'] = str(contentObject.version)
            except:
                pass
            if propertyVersion is not None:
                self.rtvMapping['VERSION'] = str(propertyVersion)
                try:
                    if contentObject.version is None:
                        contentObject.version = str(propertyVersion)
                except:
                    contentObject.version = str(propertyVersion)

        if levelStruct:
            self.rtvMapping['LEVEL'] = levelStruct
            for _lvl_ in self.levels:
                if 'rtv' in _lvl_.keys() and 'level' in _lvl_.keys() and str(_lvl_['level']) == levelStruct:
                    # apply level specific overwrites
                    fdeploy.LOGGER.debug("override level %s specific rtvs: %s" % (_lvl_['level'],_lvl_['rtv']))
                    self.get_rtvs(self.rtvMapping, _lvl_['rtv'])
        else:
            raise Exception("No level defined")

        # apply external overwrites
        if options and 'rtv' in options.__dict__.keys() and options.rtv is not None:
            for rtv in options.rtv:
                name = rtv.split("=")[0]
                value = rtv.split("=")[1]
                if value is None or name is None:
                    raise Exception(
                        "malformed --rtv value supplied %s needs corrected." % (rtv))
                self.rtvMapping[name]=value
        # check if the required rtvs are supplied
        missing_rtvs = []
        for required in REQUIRED_RTVS:
            checked = False
            for key in self.rtvMapping.keys():
                if key == required:
                    checked = True
            if checked == False:
                missing_rtvs.append(required)
        if len(missing_rtvs) > 0:
            raise Exception("Following required keys are missing %s for '%s' defined in '%s'" % (missing_rtvs, self.id, self.source))
        # assemble

        # resolve variables level and systemlevel
        fdeploy.LOGGER.trace( str(self.rtvMapping.keys()))
        for rtvkey in self.rtvMapping.keys():
            if rtvkey.startswith('@'):
                include = self.rtvMapping[rtvkey].lstrip()
                importExternalRtvs(include,self.rtvMapping)
        for vars in ['level','systemlevel']:
            for rtvkey in self.rtvMapping.keys():
                text = self.rtvMapping[rtvkey]
                if isinstance(text, unicode) or isinstance(text, str):
                    fdeploy.LOGGER.debug("name=%s, value=%s, vars=%s, exist=%s, in environment=%s" % (rtvkey, text, vars, vars in text, vars in os.environ))
                    if vars in text and vars in os.environ:
                        fdeploy.LOGGER.trace("found %s key='%s' in %s (%s)\n" % (vars,rtvkey, text, self.rtvMapping[rtvkey]))
                        try:
                            text = text.replace('${%s}' % vars, os.environ[vars])
                        except KeyError:
                            pass
                    self.rtvMapping[rtvkey]=text

        envvar_list = self.as_envvars()
        return self.rtvMapping, envvar_list

    def as_envvars(self,rtvs=None):
        envvar_list = ''
        rtvs = self.rtvMapping if rtvs is None else rtvs
        #print "\n \t %s \n " % (rtvs)
        try:
            for baskey in sorted(rtvs.keys()):
                if isinstance(rtvs[baskey], str) or isinstance(rtvs[baskey], unicode):
                    fdeploy.LOGGER.trace("%25s = %s" % (baskey,rtvs[baskey]))
                    envvar_list = envvar_list + \
                        "export %s=\"%s\"\n" % (baskey.strip(), rtvs[baskey].strip())
        except KeyError as err:
            fdeploy.LOGGER.error( "KEY_MAPPING: %s" % (rtvs))
            fdeploy.LOGGER.error( "KEY_OPTIONS: %s" % (options))
            raise err
        except AttributeError as err:
            fdeploy.LOGGER.error( "ATTR_MAPPING: %s" % (rtvs))
            try:
                fdeploy.LOGGER.error( "ATTR_OPTIONS: %s" % (options))
            except NameError:
                pass
            raise err
        return envvar_list
