
from fdeploy.content.fdeployComponent import fdeployComponent

def create_test_component(type='java'):
    return fdeployComponent({
        'id': 'componentId',
        'platform': 'bash_generator',
        'type': type,
        'filter':
            {"var1": "value1", "var2": "value2"},
        'content':  [{
            "gav": "com.fedex.ground.sefs.pd.domainview.coreshipment:fxg-pd-domainview-coreshipment:zip",
            "relativePath": "L1/rel.sh",
            "saveArchiveName": "sefs_suCLEARANCEFeed.zip"
        }],
        'name_format': 'format',
        'levels':
        [{
            'level': 'L1',
            'rtv': [{'name': 'JMX_PORT', 'value': '40313'}],
            'targets': ['a@fedex.com', 'b@fedex.com', 'c@fedex.com', 'd@fedex.com', 'e@fedex.com']
        }],
        'rtv': [
                {'name': 'JAVA_HOME',
                 'value': '/opt/java/hotspot'
                 },
                {'name': 'JMX_PORT', 'value': 'ABCDEF'},
                {'name': "APPMANAGE_XML", 'value': "/var/tmp/${level}/appmanage.xml"}
        ]

    }, 'inline.json')


class TestFdeployComponent(object):

    def test_apply_rtv_globs(self):
        comp = create_test_component()
        assert len(comp.rtvMapping) == 3
        assert comp.rtvMapping['JAVA_HOME'] == '/opt/java/hotspot'

        rtv = [
                {'name': 'EAI_NUMBER',
                 'value': '124553'
                 },
                {'name': 'APPDM', 'value': 'Mrigank'},
                {'name': "JMX_PORT", 'value': "overwrite"}
        ]
        comp.applyGlobalRtvs(rtv,'c')
        assert len(comp.rtvMapping) == 5
        assert comp.rtvMapping['JAVA_HOME'] == '/opt/java/hotspot'
        assert comp.rtvMapping['EAI_NUMBER'] == '124553'
        assert comp.id == 'c'
        assert comp.original_id == 'componentId'
        rtv = [
                {'name': 'EAI_NUMBER',
                 'value': '1kl51'
                 }]

        comp.applyGlobalRtvs(rtv, 'new-id')
        assert comp.rtvMapping['JAVA_HOME'] == '/opt/java/hotspot'
        assert comp.rtvMapping['EAI_NUMBER'] == '1kl51'
        assert comp.id == 'new-id'
