# -*- coding: utf-8 -*-

class contentClass(object):

    def __init__(self, content):
        self.directory = content['directory']
        self.includes = content['includes'] if 'includes' in content else '*'
        self.saveArchiveName = content['saveArchiveName']
        self.prependpath = content['prependpath'] if 'prependpath' in content else None
        self.phase = content['phase'] if 'phase' in content else None
        self.permission = content['permission'] if 'permission' in content else None
        self.owner = content['owner'] if 'owner' in content else None
        self.filtering = content['filtering'] if 'filtering' in content else 'False'
        self.normalize = content['normalize'] if 'normalize' in content else 'False'
        self.filtering = True if self.filtering.lower() == 'true' else False
        self.normalize = True if self.normalize.lower() == 'true' else False

    def isRelative(self):
        if self.prependpath is None:
            return True
        return not self.prependpath.startswith('/')

    def usesFiltering(self):
        return self.filtering
