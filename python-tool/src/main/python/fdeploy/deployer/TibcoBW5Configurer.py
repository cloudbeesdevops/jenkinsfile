import os
import fdeploy
import xmltodict
import copy
import contextlib
import zipfile
import tempfile
import collections


def load_binding(binding, level='L1', filter_map={}):
    xml_structure = []
    with open(binding) as fd:
        file_contents = fdeploy.resolve_filter(fd.read(),filter_map)
        xml_structure = xmltodict.parse(file_contents)
    base_struct = xml_structure['plan']['levels']['level']
    if type(base_struct) is not list:
        # make list if only one level is specified
        base_struct=[base_struct]
    # find the element with level
    b = (item for item in base_struct if item['name'] == level).next()

    return b

def load_template(template, filter_map={}):
    xml_structure = []
    try:
        with open(template) as fd:
            file_contents = fdeploy.resolve_filter(fd.read(), filter_map)
            xml_structure = xmltodict.parse(file_contents, strip_whitespace=True)
    except os:
        pass
    return xml_structure


BINDING_SECTION = '''
				<binding name="">
					<machine>${machineName}</machine>
					<product>
						<type>bwengine</type>
                        <version/>
                        <location/>
					</product>
					<setting>
					    <startOnBoot>false</startOnBoot>
						<enableVerbose>false</enableVerbose>
						<maxLogFileSize>${maxLogFileSize}</maxLogFileSize>
						<maxLogFileCount>${maxLogFileCount}</maxLogFileCount>
						<threadCount>${threadCount}</threadCount>
						<java>
						    <prepandClassPath>${prepandClassPath}</prepandClassPath>
						    <appendClassPath >${appendClassPath}</appendClassPath >
							<initHeapSize>${initHeapSize}</initHeapSize>
							<maxHeapSize>${maxHeapSize}</maxHeapSize>
							<threadStackSize>${threadStackSize}</threadStackSize>
						</java>
					</setting>
					<shutdown>
                        <checkpoint>false</checkpoint>
                        <timeout>0</timeout>
                    </shutdown>
				</binding>
'''


class TibcoBW5Configurer(object):

    template = None
    base = []
    binding = []
    
    def __init__(self, template, filters={}):
        self.template = template
        # default to the template for the base template (if no bindings are applied)
        self.base = [load_template(copy.deepcopy(self.template),filters)]
        self.filters = filters
        self.binding = []
    def before(self,value, a):
        # Find first part and return slice before it.
        pos_a = value.find(a)
        if pos_a == -1: return ""
        return value[0:pos_a] 

    def change_name_to_attribute(self, binding_list):
        for binding in binding_list:
            binding['@name']=binding.pop('name')
            if binding['@name'] is None:
                binding['@name']=''

    def applyBindings(self, targetList, rtvMap, level='L1', split=False):
        self.base = []
        __base = load_template(copy.deepcopy(self.template),rtvMap)
        self.base.append(__base)
        __base['application']['services']['bw']['bindings']['binding']=[]
        for target in targetList:
            copy_section = copy.deepcopy(BINDING_SECTION)
            target_TLM = self.before(target,"@")
            rtvMap['machineName']= target_TLM
            if 'maxLogFileSize' not in rtvMap:
                rtvMap['maxLogFileSize']='20000'
            if 'maxLogFileCount' not in rtvMap:
                rtvMap['maxLogFileCount']='5'
            if 'maxHeapSize' not in rtvMap:
                rtvMap['maxHeapSize']='2048'
            if 'threadCount' not in rtvMap:
                rtvMap['threadCount']='32'
            bindingSection = fdeploy.resolve_filter(copy_section, rtvMap)
            bindingAsMap = xmltodict.parse(bindingSection,strip_whitespace=True)
            if '@name' not in bindingAsMap:
                bindingAsMap['@name']=''
            __base['application']['services']['bw']['bindings']['binding'].append(bindingAsMap['binding'])
            self.binding.append(bindingAsMap)
        return __base

    #
    # merging deployment configuration into the content archive
    def replaceConfig(self,content, xml,targetdir='config',overwriteArtifactId=None,testing=False):
        content.artifactId if overwriteArtifactId is None else overwriteArtifactId
        # look up all xml docs in the archive and replace deploymetn XML
        filelist=[]
        #print str(content.__dict__)
        if content.archiveFileName is None:
            content.archiveFileName = str(content.saveArchiveName)
        # validate archive exists
        if os.path.isfile(content.archiveFileName) == True:
            with contextlib.closing(zipfile.ZipFile(content.archiveFileName,'r')) as zfile:
                for filename in zfile.namelist():
                    if filename.endswith(".xml"):
                        filelist.append(filename)
        for filename in filelist:
            tmpfd, tmpname = tempfile.mkstemp(dir=os.path.dirname(os.path.abspath(content.archiveFileName)))
            os.close(tmpfd)
            with contextlib.closing(zipfile.ZipFile(content.archiveFileName,'r')) as zin:
                with contextlib.closing(zipfile.ZipFile(tmpname, 'w')) as zout:
                    zout.comment = zin.comment # preserve the comment
                    for item in zin.infolist():
                        if item.filename != filename:
                            zout.writestr(item, zin.read(item.filename))
                # replace with the temp archive
                os.remove(content.archiveFileName)
                os.rename(tmpname, content.archiveFileName)
                # now add filename with its new data
                with contextlib.closing(zipfile.ZipFile(content.archiveFileName, mode='a', compression=zipfile.ZIP_DEFLATED)) as zf:
                    zf.writestr(filename, xml[0])
    #
    # write out the resulting deployment configuration(s).
    def write(self, outfile=None, ddict=None, pretty=None):
        if ddict is None:
            ddict = self.base
        if type(ddict) != list:
            ddict = [ddict]
        if pretty is None:
            pretty = True
        __base = []
        index = 1
        for base in ddict:
            if type(base) != collections.OrderedDict and type(base) != dict:
                continue
            #collect the filenames of the xml pieces in an array
            filename = "%s-%s.xml" % (outfile, index)
            xmlstring = xmltodict.unparse(base, pretty=pretty)
            if outfile is None:
                # collect the xml pieces in an array
                __base.append(xmlstring)
            else:
                with open(filename,"w") as fh:
                    fh.write(xmlstring)
                __base.append(filename)
                index += 1
        return __base
