# -*- coding: utf-8 -*-
import os

import fdeploy.deployer
import fdeploy
import xmltodict
from TibcoBW5Configurer import load_binding, load_template, TibcoBW5Configurer
from fdeploy.UtilsForTesting import load_file_as_string, assertlines
import fdeploy.UtilsForTesting
import pytest


def getDeployer(templateFile='../../test/resources/bw5/FXFShipFeed_template_l2.xml'):
    return TibcoBW5Configurer(templateFile)

class TestTibcoBW5(object):

    source_file =  '''   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
EOF
'''

    filter = {'_target_path' : 'mypath', '_archive_version' : '2.503'}

    maxDiff = None

    @classmethod
    def setUpClass(clz):
        pass
    def readFilters(self, filterChain=None, base_filter=None):
        mapping = {}
        if base_filter:
            mapping = copy.deepcopy(base_filter)
        if filterChain:
            for filter_file in filterChain.split(','):
                map=fdeploy.load_properties(filter_file)
                for key in map.keys():
                    mapping[key]=fdeploy.resolve_filter(map[key])
        # resolve nested statements
        for value in mapping.values():
            if '${' in value:
                for key in mapping.keys():
                    if '${' in mapping[key]:
                        mapping[key]
                        mapping[key]=fdeploy.resolve_filter(mapping[key], mapping)
        return mapping

    def test_filtering_script(self):
        result = self.source_file
        try:
            result.index('$')
        except:
            assert False
        result = fdeploy.resolve_filter(result, self.filter)
        try:
            result.index('$')
            assertFalse
        except:
            pass
        assert 72 == result.index('mypath')
        assert 79 == result.index('2.503')

    def test_filtering_binding(self):
        struct = load_binding('../../test/resources/bw5/binding_filter_plan.xml', filter_map={})
        struct_filtered = load_binding('../../test/resources/bw5/binding_filter_plan.xml', filter_map=self.filter)
        assert not struct is None
        assert struct['sourceXml'] ==  "${_target_path}/L1/FXFShipFeed_template.xml"
        assert not struct['bindings']['binding']['machine'] is None
        assert struct_filtered['sourceXml'] ==  "mypath/L1/FXFShipFeed_template.xml"
        assert not struct_filtered['bindings']['binding']['machine'] is None


    def test_filtering_template(self):
        e = 6
        f = 24
        struct = load_template('../../test/resources/bw5/FXFShipFeed_template_l2.xml', filter_map={})
        sfilte = load_template('../../test/resources/bw5/FXFShipFeed_template_l2.xml', filter_map=self.filter)
        assert not struct is None
        assert not sfilte is None
        assert struct['application']['NVPairs']['NameValuePair'][1] == sfilte['application']['NVPairs']['NameValuePair'][1]
        assert struct['application']['NVPairs']['NameValuePair'][e]['name'] == sfilte['application']['NVPairs']['NameValuePair'][e]['name']
        assert self.filter['_target_path'] == sfilte['application']['NVPairs']['NameValuePair'][e]['value']
        assert self.filter['_archive_version'] == sfilte['application']['NVPairs']['NameValuePair'][f]['value']


    def test_template_load(self):
        deployer = getDeployer()
        assert not deployer is None
        assert not deployer.base is None

    def test_template_load_not_exist(self):
        with pytest.raises(IOError):
            deployer = getDeployer(
                '../../test/resources/bw5/FXFShipFeed_plan-missing.xml')

    def test_binding_load(self):
        deployer = getDeployer()
        base = deployer.applyBindings(['FXF_L1_TLM1@urh01','FXF_L1_TLM2@urh03','FXF_L1_TLM3@urh02' ], {})
        assert not base is None
        assert not deployer.base is None
        assert type(deployer.base) == list

    def test_binding_load_not_exist(self):
        with pytest.raises(IOError):
            deployer = TibcoBW5Configurer(
                '../../test/resources/bw5/FXFShipFeed_template_xl2.xml')

    def test_binding_level_L1(self):
        deployer = getDeployer()
        b = deployer.applyBindings(['a', 'b'], {})
        assert not b is None
        assert '' ==  deployer.binding[0]['@name']

    def test_binding_loadwithlevelselect(self):
        deployer = TibcoBW5Configurer(
            '../../test/resources/bw5/FXFShipFeed_template_l2.xml')
        b = deployer.applyBindings(['FXF_L2_FXF_BW_TLM_1@urh01','FXF_L2_FXF_BW_TLM_2@urh01'],{})
        assert not b is None

        assert 'FXF_L2_FXF_BW_TLM_1' ==  str(deployer.binding[0]['binding']['machine'])
        assert 'FXF_L2_FXF_BW_TLM_2' ==  str(deployer.binding[1]['binding']['machine'])

    def test_end_structure_one_binding(self):
        deployer = TibcoBW5Configurer(
            '../../test/resources/bw5/FXFShipFeed_template_l2.xml')
        assert 1 ==  len(deployer.base)
        b = deployer.base[0]['application']['services']['bw']['bindings']
        #self.pp.pprint(b)
        assert not b is None
        assert '<class \'collections.OrderedDict\'>' ==  str(type(b))
        assert 1 ==  len(b)
        assert '%%Process Archive.par-machine%%' ==  b['binding']['machine']

        b = deployer.applyBindings(['FXF_L1_FXF_BW_TLM_1@urh01','FXF_L1_FXF_BW_TLM_2@urh01'],{},'L2')
        assert not b is None
        assert 2 ==  len(deployer.binding)
        assert '' ==  str(deployer.binding[0]['@name'])
        #print str(deployer.base)
        b = deployer.base[0]['application']['services']['bw']['bindings']
        assert 'FXF_L1_FXF_BW_TLM_1' ==  b['binding'][0]['machine']

    def test_end_structure_more_than_one_binding(self):
        deployer = TibcoBW5Configurer(
            '../../test/resources/bw5/FXFShipFeed_template_l2.xml')
        assert 1 ==  len(deployer.base)
        #print str(deployer.base[0]['application'])
        b = deployer.base[0]['application']['services']['bw']['bindings']
        assert not b is None
        assert '<class \'collections.OrderedDict\'>' ==  str(type(b))
        assert 1 ==  len(b)
        assert '%%Process Archive.par-machine%%' ==  b['binding']['machine']

        b = deployer.applyBindings(['FXF_L3_FXF_BW_TLM_1@urh01','FXF_L3_FXF_BW_TLM_2@urh02'],{}, 'L3')
        assert not b is None
        b = b['application']['services']['bw']['bindings']
        assert 'FXF_L3_FXF_BW_TLM_1' ==  b['binding'][0]['machine']
        assert 'FXF_L3_FXF_BW_TLM_2' ==  b['binding'][1]['machine']

    def test_write(self):
        dicti = { 'user' : { 'password' : 'secret99', 'location' : 'disk'}}
        deployer = TibcoBW5Configurer(
            '../../test/resources/bw5/FXFShipFeed_template_l2.xml')
        xml = deployer.write(ddict=dicti)
        result = '''<?xml version="1.0" encoding="utf-8"?>
<user>
	<password>secret99</password>
	<location>disk</location>
</user>'''.decode('utf-8')
        assert result == xml[0]

    def test_skinny_template_with_defaults_unparse(self):
        deployer = TibcoBW5Configurer(
            '../../test/resources/bw5/skinny_template.xml')
        b = deployer.applyBindings(['FXF_L2_FXF_BW_TLM_1@urh12'],
            { 'maxHeapSize' : '4096'}, 'L2')
        assert 1 ==  len(deployer.base)
        xml = deployer.write()
        assert not xml is None
        result = '''<?xml version="1.0" encoding="utf-8"?>
<application xmlns="http://www.tibco.com/xmlns/ApplicationManagement" name="FXFShipFeed">
	<services>
		<bw name="FXFShipFeed.par">
			<enabled>true</enabled>
			<bindings>
				<binding name="">
					<machine>FXF_L2_FXF_BW_TLM_1</machine>
					<product>
						<type>bwengine</type>
						<version></version>
                        <location></location>
					</product>
					<setting>
					<startOnBoot>false</startOnBoot>
						<enableVerbose>false</enableVerbose>
						<maxLogFileSize>20000</maxLogFileSize>
						<maxLogFileCount>5</maxLogFileCount>
						<threadCount>32</threadCount>
						<java>
<prepandClassPath>${prepandClassPath}</prepandClassPath>
<appendClassPath>${appendClassPath}</appendClassPath>
							<initHeapSize>${initHeapSize}</initHeapSize>
							<maxHeapSize>4096</maxHeapSize>
							<threadStackSize>${threadStackSize}</threadStackSize>
						</java>
					</setting>
					<shutdown>
                    <checkpoint>false</checkpoint>
                    <timeout>0</timeout>
                    </shutdown>
				</binding>
			</bindings>
			<NVPairs name="Adapter SDK Properties">
				<NameValuePair>
					<name>Trace.Task.*</name>
					<value>false</value>
				</NameValuePair>
				<NameValuePair>
					<name>bw.plugin.http.client.checkForStaleConnections</name>
					<value>${HTTP_checkForStaleConnections}</value>
				</NameValuePair>
			</NVPairs>
			<failureCount>0</failureCount>
			<failureInterval>0</failureInterval>
			<bwprocesses>
				<bwprocess name="Processes/Starter/JMS/JMSReceive_ShipPkupHist.process">
					<starter>JMS ShipPkupHist Receiver</starter>
					<enabled>true</enabled>
					<maxJob>32</maxJob>
					<activation>true</activation>
					<flowLimit>32</flowLimit>
				</bwprocess>
			</bwprocesses>
			<isFt>false</isFt>
		</bw>
	</services>
</application>'''
        assertlines(xml[0], result.decode('utf-8'))

    def test_skinny_template_unparse(self):
        deployer = TibcoBW5Configurer(
            '../../test/resources/bw5/skinny_template.xml')
        b = deployer.applyBindings(['FXF_L2_FXF_BW_TLM_1@urh02', 'FXF_L2_FXF_BW_TLM_1@urh01'],
            { 'maxHeapSize' : '4096', 'maxLogFileSize' : '20000', 'maxLogFileCount' : '5', 'threadCount' : '10'}, 'L2')
        assert 1 ==  len(deployer.base)
        xml = deployer.write()
        assert not xml is None
        result = '''<?xml version="1.0" encoding="utf-8"?>
<application xmlns="http://www.tibco.com/xmlns/ApplicationManagement" name="FXFShipFeed">
	<services>
		<bw name="FXFShipFeed.par">
			<enabled>true</enabled>
			<bindings>
				<binding name="">
					<machine>FXF_L2_FXF_BW_TLM_1</machine>
					<product>
						<type>bwengine</type>
						<version></version>
						<location></location>
					</product>
					<setting>
					<startOnBoot>false</startOnBoot>
						<enableVerbose>false</enableVerbose>
						<maxLogFileSize>20000</maxLogFileSize>
						<maxLogFileCount>5</maxLogFileCount>
						<threadCount>10</threadCount>
						<java>
<prepandClassPath>${prepandClassPath}</prepandClassPath>
<appendClassPath>${appendClassPath}</appendClassPath>
							<initHeapSize>${initHeapSize}</initHeapSize>
							<maxHeapSize>4096</maxHeapSize>
							<threadStackSize>${threadStackSize}</threadStackSize>
						</java>
					</setting>
					<shutdown>
                    <checkpoint>false</checkpoint>
                    <timeout>0</timeout>
                    </shutdown>
				</binding>
				<binding name="">
					<machine>FXF_L2_FXF_BW_TLM_1</machine>
					<product>
						<type>bwengine</type>
						<version></version>
                        <location></location>
					</product>
					<setting>
					<startOnBoot>false</startOnBoot>
						<enableVerbose>false</enableVerbose>
						<maxLogFileSize>20000</maxLogFileSize>
						<maxLogFileCount>5</maxLogFileCount>
						<threadCount>10</threadCount>
						<java>
<prepandClassPath>${prepandClassPath}</prepandClassPath>
<appendClassPath>${appendClassPath}</appendClassPath>
							<initHeapSize>${initHeapSize}</initHeapSize>
							<maxHeapSize>4096</maxHeapSize>
							<threadStackSize>${threadStackSize}</threadStackSize>
						</java>
					</setting>
					<shutdown>
                    <checkpoint>false</checkpoint>
                    <timeout>0</timeout>
                    </shutdown>
				</binding>
			</bindings>
			<NVPairs name="Adapter SDK Properties">
				<NameValuePair>
					<name>Trace.Task.*</name>
					<value>false</value>
				</NameValuePair>
				<NameValuePair>
					<name>bw.plugin.http.client.checkForStaleConnections</name>
					<value>${HTTP_checkForStaleConnections}</value>
				</NameValuePair>
			</NVPairs>
			<failureCount>0</failureCount>
			<failureInterval>0</failureInterval>
			<bwprocesses>
				<bwprocess name="Processes/Starter/JMS/JMSReceive_ShipPkupHist.process">
					<starter>JMS ShipPkupHist Receiver</starter>
					<enabled>true</enabled>
					<maxJob>32</maxJob>
					<activation>true</activation>
					<flowLimit>32</flowLimit>
				</bwprocess>
			</bwprocesses>
			<isFt>false</isFt>
		</bw>
	</services>
</application>'''
        assertlines(xml[0], result.decode('utf-8'))

#
#     # not part of the run since the order is out of order.
#     def end_structure_writeout_l2(self):
#         deployer = TibcoBW5Configurer(
#             '../../test/resources/bw5/FXFShipFeed_template_l2.xml')
#         b = deployer.base['application']['services']['bw']['bindings']
#         assert not b is None
#         #print type(b)
#         assert '<class \'collections.OrderedDict\'>' ==  str(type(b))
#         assert 1 ==  len(b)
#         assert '%%Process Archive.par-machine%%' ==
#                           b['binding']['machine'])
#
#         b = deployer.applyBindings(
#             '../../test/resources/bw5/FXFShipFeed_plan-descriptors.xml', 'L2')
#         assert not b is None
#         assert 'L2' ==  str(deployer.binding['name'])
#         b = deployer.base['application']['services']['bw']['bindings']
#         assert 'FXF_L2_FXF_BW_TLM_1' ==  b['binding'][0]['machine']
#
#         xml = deployer.write(pretty=True)
#         assert not xml is None
#         # loading
#         result = load_file_as_string('../../test/resources/bw5/FXFShipFeed_final_l2.xml')
#         with open("output_l2.txt", "w") as fh:
#             fh.write(xml)
#         assert result ==  xml
#
#
#     # not part of the run since the order is out of order.
#     def end_structure_writeout_l3(self):
#         deployer = TibcoBW5Configurer(
#             '../../test/resources/bw5/FXFShipFeed_template_l3.xml')
#         b = deployer.base['application']['services']['bw']['bindings']
#         assert not b is None
#         #print type(b)
#         assert '<class \'collections.OrderedDict\'>' ==  str(type(b))
#         assert 1 ==  len(b)
#         assert '%%Process Archive.par-machine%%' ==
#                           b['binding']['machine'])
#
#         b = deployer.applyBindings(
#             '../../test/resources/bw5/FXFShipFeed_plan-descriptors.xml', 'L3')
#         assert not b is None
#         assert 'L3' ==  str(deployer.binding['name'])
#         b = deployer.base['application']['services']['bw']['bindings']
#         assert 'FXF_L3_FXF_BW_TLM_1' ==  b['binding'][0]['machine']
#
#         xml = deployer.write(pretty=True)
#         assert not xml is None
#         # loading
#         result = load_file_as_string('../../test/resources/bw5/FXFShipFeed_final_l3.xml')
#         with open("output_l3.txt", "w") as fh:
#             fh.write(xml)
#         assert result ==  xml
#
#     # not part of the run since the order is out of order.
#     def test_end_structure_writeout_l3_splitted_xmlarray(self):
#         deployer = TibcoBW5Configurer(
#             '../../test/resources/bw5/FXFShipFeed_template_l3.xml')
#         b = deployer.base[0]['application']['services']['bw']['bindings']
#         assert not b is None
#         #print type(b)
#         assert '<class \'collections.OrderedDict\'>' ==  str(type(b))
#         assert 1 ==  len(b)
#         assert '%%Process Archive.par-machine%%' ==
#                           b['binding']['machine'])
#         deployer.applyBindings(
#             '../../test/resources/bw5/FXFShipFeed_plan-descriptors.xml', 'L3', split=True)
#         assert 'L3' ==  str(deployer.binding['name'])
#         assert 2 ==  len(deployer.base)
#         assert 'FXF_L3_FXF_BW_TLM_1' ==  deployer.base[0]['application']['services']['bw']['bindings']['binding']['machine']
#         xml = deployer.write(pretty=True)
#         assert not xml is None
#         assert 2 ==  len(xml)
#
#     # def test_end_structure_writeout_l3_splitted_xmlfiles(self):
#     #     deployer = TibcoBW5Configurer(
#     #         '../../test/resources/bw5/FXFShipFeed_template_l3.xml')
#     #     deployer.applyBindings(['FXF_L3FXF_L2_FXF_BW_TLM_1','FXF_L3FXF_L2_FXF_BW_TLM_2'],{},'L3', split=True)
#     #     assert 'FXF_L3_FXF_BW_TLM_1' ==  deployer.base['application']['services']['bw']['bindings']['binding']['machine']
#     #     xml = deployer.write(pretty=True, outfile="l3_splitted")
#     #     assert not xml is None
#     #     assert 2 ==  len(xml)
#     #     assert 2 ==  len(glob.glob("l3_splitted*.xml"))
#     #
#     #
#     # # not part of the run since the order is out of order.
#     # def test_end_structure_writeout_l2_splitted_xmlarray(self):
#     #     deployer = TibcoBW5Configurer(
#     #         '../../test/resources/bw5/FXFShipFeed_template_l2.xml')
#     #     deployer.applyBindings(['machine1', 'machine2'],
#     #         { 'maxHeapSize' : '4096', 'maxLogFileSize' : '20000', 'maxLogFileCount' : '5', 'threadCount' : '10'})
#     #     assert 'L2' ==  str(deployer.binding['name'])
#     #     assert 2 ==  len(deployer.base)
#     #     assert 'FXF_L2_FXF_BW_TLM_1' ==  deployer.base[0]['application']['services']['bw']['bindings']['binding']['machine']
#     #     xml = deployer.write(pretty=True)
#     #     assert not xml is None
#     #     assert 2 ==  len(xml)

    def test_filtering_bindings(self):
        filter =  { 'maxHeapSize' : '0011', '_archive_version' : '1.0-SNAPSHOT','initHeapSize' : '512' ,
         'threadStackSize' : '1024',
            'JMS_conn_External_Reject_JNDIPassword' : 'secret99',
            'prepandClassPath' : '/tmp/andre',
            'appendClassPath' : 'x',
            'JMS_conn_External_Reject_JNDIUser' : 'secret99',
            'JMS_conn_External_Reject_QueueConnectionFactory' : 'reject',
            'JMS_conn_External_Reject_Queue' : 'value',
            '_target_path' : 'PATHTOTARGET', 'maxLogFileSize' : '20000',
            'maxLogFileCount' : '5', 'threadCount' : '10'}
        deployer = TibcoBW5Configurer(
            '../../test/resources/bw5/FXFShipFeed_template_l2.xml',filter)
        template = deployer.applyBindings(['TLM1@urh01', 'TLM2@urh02', 'TLM3@urh03'],filter)
        assert 1 ==  len(deployer.base)
        xmlstring = xmltodict.unparse(template, pretty=True,indent='  ')
        #print xmlstring
        expected = load_file_as_string('../../test/resources/bw5/FXFShipFeed_template_l2_externalized.xml')
        assertlines(xmlstring,expected,strip=True)
    def test_sub_bindings(self):
        base_prop = os.path.abspath('../../test/resources/bw5/L1.properties')
        map=self.readFilters(base_prop)
        deployer = TibcoBW5Configurer(
            '../../test/resources/bw5/appmanage.xml',map)
        template = deployer.applyBindings(['machine1', 'machine2'],map)
        assert 1 ==  len(deployer.base)
        #result = fdeploy.resolve_filter(template, map)
        xmlstring = xmltodict.unparse(template, pretty=True,indent='  ')
        fdeploy.resolve_filter(xmlstring, map)
        #print result
        #expected = load_file_as_string('../../test/resources/bw5/appmanage_exter.xml')
        #assertlines(result,expected)
    def test_sub_bindings_2(self):
        base_prop = os.path.abspath('../../test/resources/bw5/L1.properties')
        map=self.readFilters(base_prop)
        deployer = TibcoBW5Configurer(
            '../../test/resources/bw5/appmanage.xml',map)
        deployer.applyBindings(['machine1', 'machine2'],map)
        #print template
        deployment_config = deployer.write(pretty=True, outfile=None)
        #print deployment_config
        assert 1 ==  len(deployer.base)
        fdeploy.resolve_filter(deployment_config[0], map)

    def test_sub_bindings_3(self):
        base_prop = os.path.abspath('../../test/resources/bw5/L1.properties')
        map=self.readFilters(base_prop)
        expected = load_file_as_string('../../test/resources/bw5/appmanage.xml')
        fdeploy.resolve_filter(expected, map)
        #print result


if __name__ == '__main__':
    unittest.main()
