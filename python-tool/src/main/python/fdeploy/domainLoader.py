# -*- coding: utf-8 -*-

import fdeploy
import fdeploy.content
import os
import json

# classes
class domainLoader(object):
    domainsMap = {}
    domainrtvMap = {}
    path = None

    def __init__(self,path):
        self.path = path

    def digest(self, jsonfile):
        loadingFile = os.path.abspath(self.path + "/" + jsonfile) # domain-config.json
        self.loadingDirectory = os.path.dirname(os.path.abspath(loadingFile))
        deploy_descr = []
        with open(loadingFile) as descriptor:
            try:
                deploy_descr = json.loads(descriptor.read())
            except Exception, e:
                raise Exception("loading %s failed.: reason: %s" % (loadingFile, e))
            for domains in deploy_descr:
                self.domainsMap[domains['domain_name']]=domains['rtv']

        fdeploy.LOGGER.info("registered %s domains." % (len(self.domainsMap)) )
        fdeploy.LOGGER.info("registered domains are %s." % (self.domainsMap.keys()) )
        return self.domainsMap.keys()

    def domain_rtvs(self):
        return self.domainsMap.values()
    def domain_rtv(self, id):
        if id in self.domainsMap.keys():
            return self.domainsMap[id]
        else:
            raise Exception('id %s not found' % (id))
