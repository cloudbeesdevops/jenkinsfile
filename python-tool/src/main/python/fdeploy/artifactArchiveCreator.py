# -*- coding: utf-8 -*-

import os
import fdeploy
import glob2
import re
import sys
import os.path
import zipfile
import fileinput
import shutil

def is_binary(filename):
    """
    Return true if the given filename appears to be binary.
    File is considered to be binary if it contains a NULL byte.
    FIXME: This approach incorrectly reports UTF-16 as binary.
    """
    with open(filename, 'rb') as f:
        for block in f:
            if b'\0' in block:
                return True
    return False


class artifactArchiveCreator(object):

    archiveBaseDir = 'archives'
    useCache = False
    nexusOriginalArchiveNames = {}
    archiveCreations = {}
    archiveDir = None
    filter_temp_dir = 'target'

    def __init__(self, loadingDirectory, _useCache=True, base_dir="tmp"):
        self.useCache = _useCache
        self.loadingDirectory = loadingDirectory
        self.archiveDir = "%s/%s" % (base_dir, self.archiveBaseDir)

    # CREATE
    def create(self, comp, content, filter=None):
        _archiveDir = os.path.abspath(os.path.normpath(self.archiveDir))
        content_base = os.path.join(self.loadingDirectory, content.directory)
        normalize = content.normalize

        fdeploy.LOGGER.trace("\tcontent_base=%s" % (content_base))
        fdeploy.LOGGER.trace("\tcontent=%s" % (str(content)))

        content_base = os.path.expandvars(content_base)

        fdeploy.LOGGER.trace("\tcontent.dir=%s,content_base=%s" % (content.directory, content_base))

        if os.path.exists(self.filter_temp_dir) == True:
            shutil.rmtree(self.filter_temp_dir, ignore_errors=True, onerror=None)

        if os.path.exists(_archiveDir) == False:
            shutil.rmtree(_archiveDir, ignore_errors=True, onerror=None)
            fdeploy.mkdir_p(_archiveDir)
        fdeploy.LOGGER.debug("\tarchiveDir=%s\tcontent_base=%s" % (_archiveDir,content_base))

        archiveFileName = os.path.abspath(
            os.path.normpath(
                os.path.join(self.archiveDir, content.saveArchiveName)))

        # put into filter area / copy
        if os.path.exists(content_base):
            shutil.copytree(content_base,self.filter_temp_dir)

        files = []
        includes_pattern = content.includes.split(',')
        for idx, item in enumerate(includes_pattern):
            includes_pattern[idx]=os.path.expandvars(item)
        fdeploy.LOGGER.trace("\t\tinclude patterns=%s" % (str(includes_pattern)))
        # if no includes are specified use default
        if (len(includes_pattern) == 0):
            includes_pattern = [ '*.*' ]
        for ext in includes_pattern:
            pathjoun = os.path.join(self.filter_temp_dir, ext)
            fdeploy.LOGGER.trace("glob path: %s" % (pathjoun))
            ext_files = glob2.glob(pathjoun)
            if ext_files is not None:
                if content.filtering == True and filter is not None:
                    for filter_file in ext_files:
                        fdeploy.LOGGER.debug("\tfiltering: %s = %s" % (filter_file, 'NO' if is_binary(filter_file) else 'YES'))
                        if is_binary(filter_file) == False:
                            self.filterFile(filter,filter_file)
                        files.append(filter_file)
                else:
                    # no filter present so just plainly copy the list of files
                    files.extend(ext_files)
            else:
                fdeploy.LOGGER.error("includes pattern %s results in no result" % (ext))
        zf = zipfile.ZipFile(archiveFileName, mode='w')
        try:
            for _file in files:
                #print "archiving file %s " % (_file)
                _arcname = re.sub(r'%s' % self.filter_temp_dir, '', _file)
                if not content.prependpath is None:
                    _arcname = "%s/%s" % (content.prependpath, _arcname)
                # normalize the level out of the archive name.
                if normalize == True:
                    for _level in ['L0/','L1/','L2/','L2_CL2/','L3/','L3_CL3/','L4/','L5/','PROD/']:
                            _arcname=re.sub(r'%s' % _level, '', _arcname)
                fdeploy.LOGGER.debug("\tarchiving: %s = %s" % (_arcname, _file))
                zf.write(_file, arcname=_arcname)
        finally:
            zf.close()

        # Save a cache of the URL to local file name so we don't have to repull from nexus over and over
        shutil.rmtree(self.filter_temp_dir, ignore_errors=True, onerror=None)

        self.archiveCreations[archiveFileName] = archiveFileName
        content.archiveFileName = archiveFileName
        return


    def filter(self, filters, text):
        if text is None:
            return ''
        for key in filters.keys():
            text = text.replace('${%s}' % key, filters[key])
        return text

    def filterFile(self, filters, _filename):
        for line in fileinput.input(_filename, inplace=True, backup='.bak'):
            sys.stdout.write(self.filter(filters, line)) 

    def get_filters(self,filtermap, component, level=None):
        if level is None:
            level = self.options.level
        if filtermap is None:
            filtermap={}
        for source in component.filter.keys():
            _value = os.path.expandvars(component.filter[source].strip())
            filtermap[source]=_value
        for _lvl_ in component.levels:
            if 'level' in _lvl_ and _lvl_['level'] == level and 'filter' in _lvl_.keys() and _lvl_['filter']:
                for source in _lvl_['filter']:
                    _value = os.path.expandvars(_lvl_['filter'][source].strip())
                    filtermap[source]=_value
        return filtermap
