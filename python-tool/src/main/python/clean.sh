find . -name '*.pyc' -exec rm {} \;
find . -type d -name '__pycache__' -exec rm -fr {} \;
rm -fr build dist usr *.patch *.stp Python-2.7.15* *.rpm
unset HTTPS_PROXY HTTP_PROXY https_proxy http_proxy
