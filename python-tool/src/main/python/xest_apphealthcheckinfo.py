# -*- coding: utf-8 -*-
import pytest
import os
import requests
import fdeploy
from fdeploy.platform import load_file_as_string
import BaseHTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler


class ProbeHandler(BaseHTTPRequestHandler):

    def setup(self):
        BaseHTTPServer.BaseHTTPRequestHandler.setup(self)
        print "setup handler %s" % (self)

    def do_GET(self):
        self.send_response(requests.codes.ok)
        # Add response headers.
        self.send_header('Content-Type', 'application/json; charset=utf-8')
        self.end_headers()
        response_content = load_file_as_string('../../test/resources/probe_output.json')
        self.wfile.write(response_content.encode('utf-8'))
        return

os.environ['NO_PROXY']='localhost'
handler_class='ProbeHandler'
handler_module='test_apphealthcheckinfo'
port=7002
protocol='https'

@pytest.mark.usefixtures("mock_server")
class TestAppHealthInfo(object):

    def test_tibco_be(self):
        ah = fdeploy.apphelathcheckinfo()
        target = ['andre@localhost:/top/secret']
        rtvmap = {'PU_NAME': 'ShipmentPU', 'LOGICAL_NAME': 'FXE_SEFS_SHIPMENT_1'}
        assert ah.apphelathCheck('tibco_be',target, rtvmap)
        assert ah.apphelathCheck('tibco_be',target, {'JMX_PORT' : '31100', 'PU_NAME': 'PN', 'LOGICAL_NAME': 'FXE_SEFS_SHIPMENTandre'}) == False

    def test_tibco_bw(self):
        ah = fdeploy.apphelathcheckinfo()
        target = ['andre@localhost:/top/secret']
        rtvmap = {'DOMAIN_NAME':'FXG_L3_ADMIN_CORE_Domain', 'PROCESS_NAME': 'fxg-bw-shipmentGateway-ShipmentGatewayFXG'}
        assert ah.apphelathCheck('tibco_bw',target, rtvmap)
        assert ah.apphelathCheck('tibco_bw',target, {'DOMAIN_NAME':'x', 'PROCESS_NAME' : 'andre'}) == False

    def test_tibco_java(self):
        ah = fdeploy.apphelathcheckinfo()
        target = ['andre@localhost:/top/secret']
        rtvmap = { 'ARTIFACTID' : 'FXE_SEFS_SU_ADDRESS_AGENT_1'}
        assert ah.apphelathCheck('java',target, rtvmap)
        assert ah.apphelathCheck('java',target, {'ARTIFACTID' : 'blue'}) == False
