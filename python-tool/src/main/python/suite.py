import sys
# path = sys.path[0]
# print str(sys.path)
# sys.path.remove(sys.path[0])
# sys.path.append(path)
print str(sys.path)

import os
import os.path
import fdeploy
import unittest
import coverage

fdeploy.MUTE_ALL=True

if __name__ == '__main__':
    sys.stderr.write("start suite\n")
    scope="Test*.py"
    try:
        scope=sys.argv[1]
    except:
        scope="Test*.py"
    print "executing %s" % (scope)
    _path = os.path.abspath('../../../target/surefire-reports')
    cov = coverage.Coverage(cover_pylib=False) #,include='ldd,fdeploy,analytics,fedex,cloudbees')
    cov.start()
    test_results = xmlrunner.XMLTestRunner(output=_path).run(unittest.TestLoader().discover(".", scope))
    cov.stop()
    cov.save()

    cov.xml_report()
