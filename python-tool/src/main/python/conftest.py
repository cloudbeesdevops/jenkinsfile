import pytest
from threading import Thread
import os
import ssl
from BaseHTTPServer import HTTPServer
#import test_apphealthcheckinfo
import importlib
import sys
sys.path.append('path')

@pytest.fixture(scope="module")
def mock_server(request):
    os.environ["NO_PROXY"]='127.0.0.1'
    port = getattr(request.module, "port", 8080)
    handler_class = getattr(request.module, "handler_class", "fakeclass")
    handler_module = getattr(request.module, "handler_module", "fakkemodule")
    type = getattr(request.module, "protocol", "http")
    handler_class = getattr(importlib.import_module(handler_module), handler_class)
    if 'http' == type:
        mock_server = HTTPServer(('127.0.0.1', port), handler_class)
    else:
        mock_server = HTTPServer(('127.0.0.1', port), handler_class)
        mock_server.socket = ssl.wrap_socket(mock_server.socket, keyfile='../../test/resources/key.pem', certfile='../../test/resources/localhost.pem', server_side=True)
    mock_server.timeout=5
    #mock_server.serve_forever
    mock_server_thread = Thread(target=mock_server.serve_forever)
    mock_server_thread.setDaemon(True)
    mock_server_thread.start()
    print (">>>>>>>>>>>>>>>>>>> %s -> %s %s" % (mock_server_thread, handler_class, handler_module))
    #yield mock_server_thread
    #mock_server_thread.join()

@pytest.fixture(scope='function', autouse=True)
def testcase_result(request):
    print("Test '{}' STARTED".format(request.node.nodeid))
    def fin():
        print("Test '{}' COMPLETED".format(request.node.nodeid))
        print("Test '{}' DURATION={}".format(request.node.nodeid,request.node.rep_call.duration))
    request.addfinalizer(fin)


@pytest.mark.tryfirst
def pytest_runtest_makereport(item, call, __multicall__):
    rep = __multicall__.execute()
    setattr(item, "rep_" + rep.when, rep)
    return rep
