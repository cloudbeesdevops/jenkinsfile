# --- status ---
APP_EXISTS=$(cf get-health-check %s)
if [[ "${APP_EXISTS}" != *"FAILED"* ]]; then
  export app_exists=1
  echo "Application %s Already Exists, refreshing (code=${app_exists})"
else
  export app_exists=0
  echo "Application %s Does not Exist, initializing (code=${app_exists})"
fi
# --- status end ---
