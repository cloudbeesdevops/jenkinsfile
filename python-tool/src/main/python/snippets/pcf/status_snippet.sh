# --- status ---
APP_EXISTS=$(cf get-health-check ${APP_NM})
if [[ "${APP_EXISTS}" != *"FAILED"* ]]; then
  export app_exists=1
  echo "Application ${APP_NM} Already Exists, refreshing (code=${app_exists})"
else
  export app_exists=0
  echo "Application ${APP_NM} Does not Exist, initializing (code=${app_exists})"
fi
# --- status end ---
