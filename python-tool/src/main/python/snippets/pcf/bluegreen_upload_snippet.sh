# --- upload ---
function fail {
  echo $1 >&2
  echo "Deleting the app ${APP_NM}-green"
  cf stop ${APP_NM}-green
  exit 1
}

function retry {
  local n=1
  local max=5
  local delay=5
  while true; do
    "$@" && break || {
      if [[ $n -lt $max ]]; then
        ((n++))
        echo "Command failed. Attempt $n/$max:"
        sleep $delay;
      else
        fail "The command has failed after $n attempts."
      fi
    }
  done
}
echo "Checking for space in the organizations"
REQD_FREE_SPACE="1"
GUID_ORG=`cf org $EAI_NUMBER --guid`
USED_SPACE=`cf curl /v2/organizations/$GUID_ORG//summary |grep -o -P '(?<="mem_dev_total":).*(?=,)'|awk '{ SUM += $1 } END { print SUM }'`
USED_SPACE_GB=`echo $USED_SPACE|awk '{ used_space = $1 /1024;print used_space }'`
ALLOC_SPACE=`cf quota  $EAI_NUMBER |grep "Total Memory"|tr -cd [:digit:]`
if [ $ALLOC_SPACE -le $(expr "$USED_SPACE_GB" + $REQD_FREE_SPACE ) ]
then
echo "ORGANIZATIONS MEMORY IS NOT SUFFICEINT TO DEPLOY THE APP" && exit 1
else
echo "ORGANIZATIONS MEMORY IS  SUFFICEINT TO DEPLOY THE APP"
fi

echo "app_exists_exit=${app_exists} - ${APP_NM} for ${APP_HOST} in ${DOMAIN}"
set -xv
if [[ "${app_exists}" -ne 0 ]]; then
	cf app ${APP_NM}-backout > /dev/null 2>&1
	    backout_exists=${?}
	    if [[ "${backout_exists}" -eq 0 ]]; then
	    	cf delete -f ${APP_NM}-backout
	fi
	cf p ${APP_NM}-green -f  ${_stagedir}/manifest-${_uuid}.yml -p ${_target_path}/${_archive_filename} -i 1 --hostname ${APP_HOST}-green --vars-file <(echo EUREKA_NAME: ${APP_NM}-green)
	echo "Check green application health"
	sleep 10
	retstatus=0
	loopcnt=0
	sleepretry=4
	while [[ `cf app ${APP_NM}-green | grep '^#' | grep -v 'running' | wc -l` -ne 0 && $loopcnt -ne $sleepretry ]]
	do
	   loopcnt=`expr $loopcnt + 1`
	   sleep 5
	   [[ $loopcnt == $sleepretry ]] && retstatus=1
	done
	if [ $retstatus -ne 0 ];then
		exit 1;
	else
		echo "Updating EUREKA_REGISTRY NAME"
		retry cf set-env ${APP_NM}-green EUREKA_APP_NAME ${APP_NM}
		retry cf restart ${APP_NM}-green
		echo "Mapping overwrite route for green"
		retry cf map-route ${APP_NM}-green ${DOMAIN} -n ${APP_HOST}
		echo "Delete current active route for ${APP_NM}"
		retry cf delete-route -f ${DOMAIN} -n ${APP_HOST}-green
		echo "renaming  ${APP_NM} as ${APP_NM}-backout"
		retry cf rename ${APP_NM} ${APP_NM}-backout
		echo "renaming  ${APP_NM}-green as ${APP_NM}"
		retry cf rename ${APP_NM}-green ${APP_NM}
		echo "Restaging ${APP_NM}"
		retry cf restage ${APP_NM}
		sleep 10
		echo "sacling up  ${APP_NM} and scaling down ${APP_NM}-backout"
		instance=1
		while [ "2" -le "$NUM_INSTANCES" ]
		do
			cf scale ${APP_NM}-backout -i $((NUM_INSTANCES-1))
			sleep 5
			cf scale ${APP_NM} -i $((instance+1))
			sleep 5
			NUM_INSTANCES=$((NUM_INSTANCES-1))
			instance=$((instance+1))
			echo $NUM_INSTANCES
		done
		echo "Setting EUREKA_APP_NAME as backout for ${APP_NM}"
		retry cf set-env ${APP_NM}-backout EUREKA_APP_NAME ${APP_NM}-backout
		echo "unmapping route for ${APP_NM}"
		retry cf unmap-route ${APP_NM}-backout ${DOMAIN} -n ${APP_HOST}
		echo "mapping route as backout for  ${APP_NM}"
		retry cf map-route ${APP_NM}-backout ${DOMAIN} -n ${APP_HOST}-backout
		echo "Deleting the app ${APP_NM}-backout"
		retry cf delete -f ${APP_NM}-backout
	fi
else
    echo "Pushing ${_target_path}/${_archive_filename} in ${SPACE}"
    cf app ${APP_NM}-green > /dev/null 2>&1
    green_exists=${?}
    if [[ "${green_exists}" -eq 0 ]]; then
    	cf delete -f ${APP_NM}-green
    fi
    cf p ${APP_NM} -f ${_stagedir}/manifest-${_uuid}.yml -p ${_target_path}/${_archive_filename} --hostname ${APP_HOST} ${PCF_PUSH_OPTIONS} --vars-file <(echo EUREKA_NAME: ${APP_NM})
fi
# --- end upload ---
