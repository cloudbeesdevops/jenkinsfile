#!/bin/bash
#set -e

export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=%s:action=%s, on level
level=%s
APP_NM=%s
# ---- begin - data_wrapper_header.sh - source
#%secho "[ERROR]: $@"
#%secho "[INFO] : $@"
#%sset -xv
#%secho "[WARN] : $@"

set -m # Enable Job Control
_uuid="%s"
_workdir="%s"
_stagedir="%s"
_timeout="%s"
_archive_version="%s"
_archive_filename="%s"
_artifactId="%s"
_target_path="${_stagedir}/archives"

if [[ -z "${MANIFEST_FILE}" ]]; then
  if [[ -f "${_workdir}/apps/${_uuid}/manifest-${level}.yml" ]]; then
    export MANIFEST_FILE="${_workdir}/apps/${_uuid}/manifest-${level}.yml"
  else
    export MANIFEST_FILE="${_workdir}/apps/${_uuid}/manifest.yml"
  fi
else
  echo "OVERWRITING MANIFEST FILE LOGIC with hardcoded manifest @ ${MANIFEST_FILE}"
fi

# Generating the current properties for this version
cat <<EOF > "${_stagedir}/${_archive_version}-${APP_NM}${_uuid}.properties"
#!/bin/bash
# Generated property file for application.
%s
EOF

# load the RTV's
source ${_stagedir}/${_archive_version}-${APP_NM}${_uuid}.properties

# expanding variables in YML
while read
do
    eval "echo \"${REPLY}\""
done < "${MANIFEST_FILE}" > ${_stagedir}/manifest-${_uuid}.yml

echo "------generated manifest------"
cat ${_stagedir}/manifest-${_uuid}.yml
echo "------generated manifest------"
