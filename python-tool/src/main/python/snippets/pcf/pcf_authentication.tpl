hostname
which cf
cf_exists=${?}
if [[ "${cf_exists}" != 0 ]]; then
  echo "CloudFoundry_CLI was not found. Abort! (code=${cf_exists})"
  echo "Please export PATH with location of the cf binary (f.e: /opt/fedex/tibco/cf-cli/bin)"
  exit ${cf_exists}
else
  echo "CloudFoundry_CLI found at $(which cf)"
fi

echo "Login to %s as %s"
cf api https://%s
cf auth '%s' '%s'

echo "Set target space '%s'"
cf target -o %s -s %s
if [[ "${?}" != 0 ]]; then
  echo "Error setting space '%s' in org '%s'"
  exit 2
fi
