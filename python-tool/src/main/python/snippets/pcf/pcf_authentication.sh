
hostname
which cf
cf_exists=${?}
if [[ "${cf_exists}" != 0 ]]; then
  echo "CloudFoundry_CLI was not found. Abort! (code=${cf_exists})"
  echo "Please export PATH with location of the cf binary (f.e: /opt/fedex/tibco/cf-cli/bin)"
  exit ${cf_exists}
else
  echo "CloudFoundry_CLI found at $(which cf)"
fi

echo "Login to ${API_URL}"
cf api https://${API_URL}
cf auth ${PCF_USER} ${PCF_PASSWORD}

echo "Set target space '${SPACE}'"
cf target -o ${EAI_NUMBER} -s ${SPACE}
if [[ "${?}" != 0 ]]; then
  echo "Error setting space ${SPACE} in org ${EAI_NUMBER}"
  exit 2
fi
