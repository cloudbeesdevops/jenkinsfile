hostname
which cf
cf_exists=${?}
if [[ "${cf_exists}" != 0 ]]; then
  echo "CloudFoundry_CLI was not found. Abort! (code=${cf_exists})"
  echo "Please export PATH with location of the cf binary (f.e: /opt/fedex/tibco/cf-cli/bin)"
  exit ${cf_exists}
else
  echo "CloudFoundry_CLI found at $(which cf)"
fi

echo "Login to {{ params.domain}} as {{ params.user}}"
cf api https://{{ params.domain}}
cf auth '{{ params.user }}' '{{ params.pwd }}'

echo "Set target space '{{ params.space }}'"
cf target -o {{ params.eai_number }} -s {{ params.space }}
if [[ "${?}" != 0 ]]; then
  echo "Error setting space '{{ params.space }}' in org '{{ params.eai_number}}'"
  exit 2
fi
