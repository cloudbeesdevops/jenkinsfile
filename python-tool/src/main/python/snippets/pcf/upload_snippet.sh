# --- upload ---
echo "Pushing ${_target_path}/${_archive_filename} in ${SPACE}"
cf app ${APP_NM}-green > /dev/null 2>&1
green_exists=${?}
if [[ "${green_exists}" -eq 0 ]]; then
	cf delete -f ${APP_NM}-green
fi
cf p ${APP_NM} -f ${_stagedir}/manifest-${_uuid}.yml -p ${_target_path}/${_archive_filename} --hostname ${APP_HOST} ${PCF_PUSH_OPTIONS} --vars-file <(echo EUREKA_NAME: ${APP_NM})
# --- end upload ---
