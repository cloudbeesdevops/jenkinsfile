# ---- begin - remote_properties_snippet.sh - source
   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
%s
EOF
# ---- end - remote_properties_snippet.sh - source
