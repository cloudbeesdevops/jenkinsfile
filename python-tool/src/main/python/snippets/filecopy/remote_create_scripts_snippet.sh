# ---- begin - remove_create_scripts_snippet.sh - source
# check ear
_log_dir=\${LOG_DIR}
[[ "${_log_dir}" == "" ]] && _log_dir="${_target_path}/logs"
[ ! -e "\${_log_dir}" ] && mkdir -p "\${_log_dir}"

[[ ! -e "${_target_path}/${_archive_version}/config" ]] && mkdir -p "${_target_path}/${_archive_version}/config"
