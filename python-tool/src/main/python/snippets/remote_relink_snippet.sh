# ---- begin - remote_relink_snippet.sh - source
   # remove symlink to current install
   echo "--------------[relink]---------"
   cd ${_target_path}
set -xv
   PREVIOUSLINK=\$(readlink current)
   echo "linking ${_archive_version} as current, previous is '\${PREVIOUSLINK}'"
   if [[ "$_archive_version" != "\${PREVIOUSLINK}" ]]; then
     [[ -e current ]] && rm current
     ln -snf "${_archive_version}" current
     if [[ "\${PREVIOUSLINK}" != "" ]]; then
       [[ -e previous ]] && rm previous
       ln -snf "\${PREVIOUSLINK}" previous
     fi
   fi
   #  symlink to current properties
   ln -snf "${_archive_version}.properties" "./properties"
# ---- end - remote_relink_snippet.sh - source
set +xv
