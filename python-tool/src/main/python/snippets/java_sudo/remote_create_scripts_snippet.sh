# ---- begin - remove_create_scripts_snippet.sh - source
# check ear
_log_dir=\${LOG_DIR}
[ ! -e "\${LOG_DIR}" ] && mkdir -p "\${LOG_DIR}"
[[ ! -e "${_target_path}/${_archive_version}/config/" ]] && rm -fr "${_target_path}/${_archive_version}/config" && mkdir -p "${_target_path}/${_archive_version}/config/"

export LOG4J_FILE="${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
export sfs_script_HOST_PRIVATE_IP=\$(/sbin/ifconfig|perl -ane 'if(\$_=~/172\./){\$F[1]=~s/addr://g;print \$F[1]}')
export ARTIFACT_ID="${_artifactId}"
echo "PRIVATE_IP=\${sfs_script_HOST_PRIVATE_IP}"

base_dir="${_target_path}/${_archive_version}"
if [[ -e "${_target_path}/${_archive_version}/bin" ]]; then
   base_dir="${_target_path}/${_archive_version}/bin"
fi

export LOG4J_FILE="${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
if [[ ! -e "${_target_path}/${_archive_version}/config/${_level}/log4j.xml" ]]; then
   if [[ ! -e "${_target_path}/${_archive_version}/config/log4j.xml" ]]; then
      echo "╔═╗╦═╗╦═╗╔═╗╦═╗";
      echo "║╣ ╠╦╝╠╦╝║ ║╠╦╝";
      echo "╚═╝╩╚═╩╚═╚═╝╩╚═";
      echo "No log4j.xml found for ${_artifactId} in installation."
      exit 1
   else
      export LOG4J_FILE="${_target_path}/${_archive_version}/config/log4j.xml"
   fi
fi
# deleting unused elements, need to clean up in the source
find "${_target_path}/${_archive_version}" \( -name '*template*' -o -name '*PU.properties' -o -name '*Appmanage*' -o -name 'tmp*' \) -exec rm -v {} \;

_ear_artifact="${_artifactId}"
_ear_cdd=\$( find "${_target_path}/${_archive_version}" -name "${_artifactId}*.cdd" )
_ear_properties=\$( find "${_target_path}/${_archive_version}" -name '*.properties' -print0 | grep -FzZ "${_level}/${_artifactId}"  )
_ear=\$( find "${_target_path}/${_archive_version}" -name '*.ear'  )

echo "ear_xml=\$_ear_properties"
echo "ear    =\$_ear_cdd"

echo ">> properties ${_ear_properties} / \${_ear_properties}"

if [[ "\${_ear_cdd}" == "" || "\${_ear}" == "" || "\${_ear_properties}" == ""  ]]; then
   #_ear_cdd="${_target_path}/${_archive_version}/config/${_level}/${_artifactId}_template.cdd"
   echo "╔═╗╦═╗╦═╗╔═╗╦═╗ = cdd: \${_ear_cdd}";
   echo "║╣ ╠╦╝╠╦╝║ ║╠╦╝ = prp: \${_ear_properties}";
   echo "╚═╝╩╚═╩╚═╚═╝╩╚═ = ear: \${_ear}";
   echo "No CDD ${_ear_cdd} found for ${_artifactId}"
   exit 1
fi

# replace private IP Addresses
cp -v \${_ear_properties} "${_target_path}/${_archive_version}/config/${_artifactId}.properties"
find "${_target_path}/${_archive_version}" -type f \( -name log4j.xml -o -name '*.properties' -o -name '*.tra' \) -exec perl -i -pe 's/.{sfs_script_HOST_PRIVATE_IP}/\$ENV{sfs_script_HOST_PRIVATE_IP}/g;s/^(Engine.Log.Dir=).+/\$1\$ENV{LOG_DIR}/g;s/.{SILVERFABRIC_BE_LOG4J}/\$ENV{LOG4J_FILE}/g' {} \; -ls
find "${_target_path}/${_archive_version}" -type f \( -name log4j.xml -o -name '*.properties' -o -name '*.tra' \) -exec perl -i -pe 's/\/var\/fedex\/tibco\/logs/\$ENV{LOG_DIR}/g;s/(tibco.clientVar.fedex.log.config.path=).+/\$1\$ENV{LOG4J_FILE}/g' {} \; -ls
find "${_target_path}/${_archive_version}" -type f \( -name log4j.xml -o -name '*.properties' -o -name '*.tra' \) -exec perl -i -pe 's/.{artifactId}/\$ENV{ARTIFACT_ID}/g;' {} \; -ls
#"${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
cp -v \${_ear_cdd} "${_target_path}/${_archive_version}/config"
cp -v \${_ear} "${_target_path}/${_archive_version}/config"

[ ! -e "${_target_path}/${_archive_version}/scripts" ] && mkdir -p "${_target_path}/${_archive_version}/scripts"

## 'EOF' vs EOF is very important preventing variables from expanding in the block
cat <<'EOX' > "${_target_path}/${_archive_version}/scripts/start.sh"
#!/bin/bash

[ -e ../../properties ] && . ../../properties

# Get Relative Path of the Script
pushd `dirname $0` > /dev/null
SCRIPT_PATH=`pwd`
popd > /dev/null
cd "$SCRIPT_PATH"

# splits the key=value key2=value2 arguments string into exports in the current environment
for _export in \$(echo $@); do
   declare -a vars=( )
   set -- "${_export[@]}"
   for word; do
      if [[ $word = *"="* ]]; then
         export "\${word%%=*}"="\${word#*=}"
      fi
   done
done


# Setting Env Variables.
JAVA_HOME=\${JAVA_HOME:-/opt/java/hotspot/8/current/}
APP_ID=\${APP_ID:-plefs-cache-plugin}
HTTP_PORT=\${HTTP_PORT:-8097}
SPRING_PROFILE=\${SPRING_PROFILE:-L1}
EUREKA_HOST=8091
EUREKA_PORT=9105
LOGDIR=\${LOGDIR:-/var/fedex/sefs/plefs-cache-plugin/logs}

export JAVA_HOME
export PATH=${JAVA_HOME}/bin:${PATH}

which java
export TMP_DIR=/var/fedex/sefs/plefs-cache-plugin/logs
# clean up tomcat cache data
if [[ ! -e \${LOGDIR} ]]; then
	 mkdir -p "\${LOGDIR}"
	if [[ $? -ne 0 ]] ; then
		echo "unable to create LOGDIR=\${LOGDIR}"
		exit 1
	fi
fi
rm -fr "\${TMP_DIR}/tomcat.*"
EOX

cat <<EOX >> "${_target_path}/${_archive_version}/scripts/start.sh"
  truncate --size=0 \${_log_dir}/stdout.log \${_log_dir}/stderr.log
#set +xv
exit 0
EOX


cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/start.sh"
# AppDynamics support (this sets JAVA_OPTIONS)
export EAINUMBER=\${EAINUMBER:-3530949}
export SERVER_NAME=\${SERVER_NAME:-APP_ID}
if [[ -e /opt/appd/current/appagent/javaagent.jar ]]; then
  . /opt/appd/current/scripts/app_agent_appd.sh \${EAINUMBER} > \${LOG_DIR}/appd.out 2>&1
  . /opt/appd/current/scripts/machine_agent_appd.sh \${EAINUMBER} restart > \${LOG_ROOT}/machine-appd.out 2>&1
fi


ulimit unlimited


if [[ -e "/opt/appd/current/appagent/javaagent.jar" && "\${EAINUMBER}" != "" ]]; then
        # defaults
        APP_ID=\${APP_ID}
        APPD_APPNAME=\${APPD_APPNAME:-APP_ID}
        APPD_CONTROLLER_HOST=\${APPD_CONTROLLER_HOST:-fedex1-test.saas.appdynamics.com}
        APPD_ACCOUNT_KEY=\${APPD_ACCOUNT_KEY:-1dce5fc52c17}
        APPD_ACCOUNT=\${APPD_ACCOUNT:-fedex1-test}
        APPD_CONFIGURATION="-javaagent:/opt/appd/current/appagent/javaagent.jar -Dappdynamics.http.proxyHost=internet.proxy.fedex.com -Dappdynamics.http.proxyPort=3128 -Dappdynamics.agent.applicationName=\${APPD_APPNAME} \
   -Dappdynamics.agent.tierName=\${APP_ID} -Dappdynamics.agent.nodeName=\${APP_ID}-\$(hostname) -Dappdynamics.controller.ssl.enabled=true -Dappdynamics.controller.sslPort=443 \
   -Dappdynamics.agent.logs.dir=\${LOG_ROOT} -Dappdynamics.agent.runtime.dir=/var/fedex/appd/logs -Dappdynamics.controller.hostName=\${APPD_CONTROLLER_HOST} \
   -Dappdynamics.controller.port=443 -Dappdynamics.agent.accountName=\${APPD_ACCOUNT} -Dappdynamics.agent.accountAccessKey=\${APPD_ACCOUNT_KEY} "
fi


set -xv
nohup \${JAVA_HOME}/bin/java -d64 -server -Xmx\${MEM_MAX:-2048m} -Xms\${MEM_STACK:-512m} \${JAVA_OPTIONS} \\
   -Djava.io.tmpdir=\${TMP_DIR} -Dspring.profiles.active=\${SPRING_PROFILE} \\
   -Djava.rmi.server.hostname=\$(host \$(hostname) | sed 's/.*\\s//g') \\

EOX
cat <<EOX >> "${_target_path}/${_archive_version}/scripts/start.sh"
  1> \${_log_dir}/stdout.log 2> \${_log_dir}/stderr.log < /dev/null &
#set +xv
exit 0
EOX
echo "generated start scripts for [${_artifactId}]"


echo "generated status scripts for ${_artifactId}"
chmod -R 755 "${_target_path}/${_archive_version}/scripts/"
# ---- end - remove_create_scripts_snippet.sh - source