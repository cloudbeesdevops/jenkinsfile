# ---- begin - remote_wipeout_snippet.sh - source
# cleanup the whole application target area if it exists
if [[ -e ${_target_path} ]]; then
     cd ${_target_path}
     ls -l
     hostname
     whoami
     # remove the current link
     [[ -e "current" ]] && rm current 2>&1
     [[ -e "previous" ]] && rm current 2>&1
     # remove the hard link
     rm -vfr ${_target_path}/* 2>&1
else
   echo "target path ${_target_path} does not exist."
fi
# ---- end - remote_wipeout_snippet.sh - source
