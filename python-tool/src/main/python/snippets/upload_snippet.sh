# ---- begin - upload_snippet.sh - source
   #printf -v _local_archive_product "%s " "${UPLOAD_CONTENTS[@]}"
   for UPLOAD in ${UPLOAD_CONTENTS[@]}; do
      IFS=':' read -r -a contents <<< "${UPLOAD}"
      _local_archive="${contents[0]}"
      _path="${contents[1]}"
      _local_archive_product="${local_base_dir}/archives/${_local_archive}"
      log_debug " > upload  : (${_user}@${_host})";
      log_info " < uploading : _local_archive_products=${_local_archive_product} | _landing_area=${_landing_area}"
      log_debug " < processing : _target_path=${_target_path} | uri=${_uri} "
      #echo -n "PWD= " && pwd
      echo ${_local_archive_product}
      if [[ -e "${_local_archive_product}" ]]; then
         log_info " > uploading archive to ${_user}@${_host}:${_landing_area}, landing area: ${_landing_area}"
         /usr/bin/ssh ${_identity_} ${_ssh_options} "${_ssh}" "[[ ! -e ${_landing_area} ]] && mkdir -p ${_landing_area}" 2>&1 | tee upload_ssh.log
         log_debug " > ${MACHINE} --> ${_user}@${_host}:${_landing_area}"
         /usr/bin/scp ${_identity_} ${_ssh_options} ${_extract_contents} ${_local_archive_product} ${_user}@${_host}:${_landing_area} </dev/null 2>&1 | tee upload_ssh.log
         if [ $? -eq 0 ]; then
            echo "╦ ╦╔═╗╦  ╔═╗╔═╗╔╦╗╔═╗╔╦╗";
            echo "║ ║╠═╝║  ║ ║╠═╣ ║║║╣  ║║upload_area=${_landing_area}";
            echo "╚═╝╩  ╩═╝╚═╝╩ ╩═╩╝╚═╝═╩╝${_user}@${_host})"
            echo "target_path=${_target_path}; local_archive=${_local_archive_product}";
         else
            echo "╦ ╦╔═╗╦  ╔═╗╔═╗╔╦╗  ╔═╗╔═╗┬╦  ╔═╗╔╦╗";
            echo "║ ║╠═╝║  ║ ║╠═╣ ║║  ╠╣ ╠═╣│║  ║╣  ║║${_local_archive_product}";
            echo "╚═╝╩  ╩═╝╚═╝╩ ╩═╩╝  ╚  ╩ ╩┴╩═╝╚═╝═╩╝${_user}@${_host})";
            exit 1
         fi

      else
         echo "╦ ╦╔═╗╦  ╔═╗╔═╗╔╦╗  ╔═╗╦═╗╔╗ ╔═╗╦═╗╔╦╗╔═╗╔╦╗";
         echo "║ ║╠═╝║  ║ ║╠═╣ ║║  ╠═╣╠╦╝╠╩╗║ ║╠╦╝ ║ ║╣  ║║${_local_archive_product}";
         echo "╚═╝╩  ╩═╝╚═╝╩ ╩═╩╝  ╩ ╩╩╚═╚═╝╚═╝╩╚═ ╩ ╚═╝═╩╝${_user}@${_host})";
         echo " ! aborting no source material ${_local_archive_product} found for upload."
         exit 1
      fi
      if [[ ! -e "${_local_archive_product}" ]]; then
         echo " ! failed to write source material ${_local_archive_product} for upload."
         exit 1
      fi
   done
# ---- end - upload_snippet.sh - source
