# ---- begin - remote_overwrite_snippet.sh - source
# overwrite previous install of the same version
echo "BEGIN ---OVERWRITE SECTION"
PREVIOUSLINK=\$(readlink current)
if [[ "$_archive_version" == "\${PREVIOUSLINK}" ]]; then
  if [[ -e "${_archive_version}" ]]; then
     log_warn " ! removing previous version ${_archive_version}"
     rm -fr "${_archive_version}"
  fi
fi
# remove the link to properties
[ -e  "${_target_path}/${_archive_version}.properties" ] && rm "${_target_path}/${_archive_version}.properties"
# ---- end - remote_overwrite_snippet.sh - source
echo "END -- -OVERWRITE SECTION"
