# ---- begin - remote_audit_snippet.sh - source
# limitting the file size for this session
cat <<EOF > "${_target_path}/rotate.conf"
${_target_path}/fdeploy.log {
    minsize 1M
    rotate 0
    copytruncate
    missingok
}
EOF
cat <<EOF >> "${_target_path}/fdeploy.log"
[%s] action=%s
EOF
%s
/usr/sbin/logrotate --verbose -s "${_target_path}/logrotate.state" "${_target_path}/rotate.conf"
# ---- end - remote_audit_snippet.sh - source
