# ---- begin - remote_stage_start_snippet.sh - source
echo "╔═╗╔╦╗╔═╗╔═╗╦╔╗╔╔═╗";
echo "╚═╗ ║ ╠═╣║ ╦║║║║║ ╦${_artifactId}";
echo "╚═╝ ╩ ╩ ╩╚═╝╩╝╚╝╚═╝(${_user}@${_host})";
log_info " > landing area ${_landing_area} "
# unpack the archive in the versioned directory
# if already started, stop the current stack first
set -xv
if [[ -e "${_landing_area}/${_archive_filename}" ]]; then
    # install path
    if [[ ! -e "${_target_path}" ]]; then
      mkdir -p "${_target_path}"
    fi
    # unpacking all uploaded archives

    log_debug "staging elements '${_upload_elements}'"
    IFS=' ' read -r -a elements <<< "${_upload_elements}"
    for index in "\${!elements[@]}"
    do

      IFS=':' read -r -a contents <<< "\${elements[index]}"
      _file="\${contents[0]}"
      _path="\${contents[1]}"
      _dirs="\${contents[2]}"
      _perms="\${contents[3]}"
      _owner="\${contents[4]}"

      cd "${_target_path}"
      if [[ "\${_file}" == *zip ]]; then
         log_info "unpacking \${elements[index]} on path \${_path}"
         unzip -q -o -d "\${_path}" "${_landing_area}/\${_file}"
      else
         log_info "copying \${elements[index]} to \${_path}/bin"
         [[ ! -e  "\${_path}/bin" ]] && mkdir -p "\${_path}/bin"
         cp -v "${_landing_area}/\${_file}"  "\${_path}/bin"
      fi
      cd "\${_path}"
      if [[ -e "scripts" ]]; then
         chmod -R 755 scripts/*
      fi
      if [[ "\${_perms}" != "" ]] && [[ "\${_dirs}" != "" ]]; then
         log_info "changing permissions to \${_perms} for "
         chmod -vR "\${_perms}" ${_target_path}/\${_dirs}
      fi
      if [[ "\${_owner}" != "" ]] && [[ "\${_dirs}" != "" ]]; then
         log_info "changing ownership to \${_owner}"
         chown -vR "\${_owner}" ${_target_path}/\${_dirs}
      fi
	 done
   # Generating the current properties for this version
   cat <<EOF > "${_target_path}/${_archive_version}.properties"
#!/bin/bash
# Generated property file for application.
%s
EOF

# ---- end - remote_stage_snippet_begin.sh - source
