# ---- begin - remove_create_scripts_snippet.sh - source
# check ear
_log_dir=\${LOG_DIR}
[[ "${_log_dir}" == "" ]] && _log_dir="${_target_path}/logs"
[ ! -e "\${_log_dir}" ] && mkdir -p "\${_log_dir}"

[[ ! -e "${_target_path}/${_archive_version}/config" ]] && mkdir -p "${_target_path}/${_archive_version}/config"

export LOG4J_FILE="${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
export sfs_script_HOST_PRIVATE_IP=\$(/sbin/ifconfig|perl -ane 'if(\$_=~/172\./){\$F[1]=~s/addr://g;print \$F[1]}')
export sfs_script_HOST=\$(hostname -s)
echo "PRIVATE_IP=\${sfs_script_HOST_PRIVATE_IP}"
echo "HOST      =\${sfs_script_HOST}"

base_dir="${_target_path}/${_archive_version}"


[ ! -e "${_target_path}/${_archive_version}/scripts" ] && mkdir -p "${_target_path}/${_archive_version}/scripts"
cd "${_target_path}/${_archive_version}"

## 'EOF' vs EOF is very important preventing variables from expanding in the block
cat <<'EOX' > "${_target_path}/${_archive_version}/scripts/start.sh"
#!/bin/bash

[ -e ../../properties ] && . ../../properties

# Get Relative Path of the Script
pushd \$(dirname \$0) > /dev/null
SCRIPT_PATH=\$(pwd)
popd > /dev/null
cd "\$SCRIPT_PATH"

# splits the key=value key2=value2 arguments string into exports in the current environment
for _export in \$(echo \$@); do
   declare -a vars=( )
   set -- "\${_export[@]}"
   for word; do
      if [[ \$word = *"="* ]]; then
         export "\${word%%=*}"="\${word#*=}"
      fi
   done
done

# Setting Env Variables.
JAVA_HOME=\${JAVA_HOME:-/opt/java/hotspot/8/current/}
APP_ID=\${APP_ID:-${_uuid}}
LOG_PREFIX=\${LOG_PREFIX:-${_uuid}}
JMX_PORT=\${JMX_PORT:-15562}
JMX_AUTHENTICATE=\${JMX_AUTHENTICATE:-false}
JMX_SSL=\${JMX_SSL:-false}
HTTP_PORT=\${HTTP_PORT:-8104}
EUREKA_PORT=\${EUREKA_PORT:-8093}
SPRING_PROFILE=\${SPRING_PROFILE:-L1}
EAINUMBER=\${EAINUMBER:-\${EAI_NUMBER}}
LOG_ROOT=\${LOG_ROOT:-/var/fedex/\${USER}/\${EAINUMBER}}

export JAVA_HOME
export PATH=\${JAVA_HOME}/bin:\${PATH}

export TMP_DIR=\${TMP_DIR:-/var/fedex/\${USER}/\${EAINUMBER}/java/logs/\${APP_ID}}
rm -fr \${TMP_DIR}/tomcat.*

[[ ! -e \${LOG_ROOT}/java/logs/\${APP_ID} ]] && mkdir -p \${LOG_ROOT}/java/logs/\${APP_ID}

which java

# AppDynamics support (this sets JAVA_OPTIONS)
export SERVER_NAME=\${SERVER_NAME:-\$APP_ID}
if [[ -e /opt/appd/current/appagent/javaagent.jar ]]; then
  . /opt/appd/current/scripts/app_agent_appd.sh \${EAINUMBER} > \${LOG_ROOT}/appd.out 2>&1
  . /opt/appd/current/scripts/machine_agent_appd.sh \${EAINUMBER} restart > \${LOG_ROOT}/machine-appd.out 2>&1
fi


if [[ -e "/opt/appd/current/appagent/javaagent.jar" && "\${EAINUMBER}" != "" ]]; then
        # defaults
        APP_ID=\${APP_ID}
        APPD_APPNAME=\${APPD_APPNAME:-APP_ID}
        APPD_CONTROLLER_HOST=\${APPD_CONTROLLER_HOST:-fedex1-test.saas.appdynamics.com}
        APPD_ACCOUNT_KEY=\${APPD_ACCOUNT_KEY:-1dce5fc52c17}
        APPD_ACCOUNT=\${APPD_ACCOUNT:-fedex1-test}
        APPD_CONFIGURATION="-javaagent:/opt/appd/current/appagent/javaagent.jar -Dappdynamics.http.proxyHost=internet.proxy.fedex.com -Dappdynamics.http.proxyPort=3128 -Dappdynamics.agent.applicationName=\${APPD_APPNAME} \
   -Dappdynamics.agent.tierName=\${APP_ID} -Dappdynamics.agent.nodeName=\${APP_ID}-\$(hostname) -Dappdynamics.controller.ssl.enabled=true -Dappdynamics.controller.sslPort=443 \
   -Dappdynamics.agent.logs.dir=\${LOG_ROOT} -Dappdynamics.agent.runtime.dir=/var/fedex/appd/logs -Dappdynamics.controller.hostName=\${APPD_CONTROLLER_HOST} \
   -Dappdynamics.controller.port=443 -Dappdynamics.agent.accountName=\${APPD_ACCOUNT} -Dappdynamics.agent.accountAccessKey=\${APPD_ACCOUNT_KEY} "
fi


# Tangasol
if [[ "\${TANGOSOL_CACHECONFIG}" != "" && "\${TANGOSOL_OVERRIDE}" != "" ]]; then
TANGOSOL_CONFIGURATION="\
-Dtangosol.coherence.management=\${TANGOSOL_MANAGEMENT:-all} \
-Dtangosol.coherence.management.remote=\${TANGOSOL_MANAGEMENT_REMOTE:-true} \
-Dtangosol.coherence.localport.adjust=\${TANGOSOL_LOCALPORT_ADJUST:-true} \
-Dtangosol.coherence.localport=\${TANGOSOL_LOCALPORT:-59085} \
-Dtangosol.coherence.localhost=\$(host \$(hostname) | sed 's/.*\s//g') \
-Dtangosol.coherence.override=\${TANGOSOL_OVERRIDE} \
-Dtangosol.coherence.cacheconfig=\${TANGOSOL_CACHECONFIG} \
-Dtangosol.coherence.jcache.configuration.classname=\${TANGOSOL_JCACHE_CONFIG:-passthrough}"
fi

# echo out the command
set -xv
\${JAVA_HOME}/bin/java -d64 -server -Xmx\${JVM_XMX:-2048m} -Xms\${JVM_XMS:-512m} \
   -Dfdeploy.id=${_uuid} \
   -Dcom.sun.management.jmxremote=true \${APPD_CONFIGURATION} \${TANGOSOL_CONFIGURATION} \
   -Dcom.sun.management.jmxremote.port=\${JMX_PORT} \
   -Dcom.sun.management.jmxremote.local.only=false \
   -Dcom.sun.management.jmxremote.authenticate=\${JMX_AUTHENTICATE} \
   -Dcom.sun.management.jmxremote.ssl=\${JMX_SSL} \
   -Djava.rmi.server.hostname=\$(host \$(hostname) | sed 's/.*\s//g') \
   -Djava.io.tmpdir=\${TMP_DIR:-/var/fedex/sefs/logs} -Dspring.profiles.active=\${SPRING_PROFILE} \
   -Dlogging.file=\${LOG_ROOT}/java/logs/\${LOG_PREFIX}_\${APP_ID}_application.log \
   -Deureka.host=\${EUREKA_HOST} -Deureka.port=\${EUREKA_PORT} -Deureka.peers=\${EUREKA_PEERS} \
   -Dserver.port=\${HTTP_PORT} -jar ../bin/${_artifactId}*.jar &
set +xv
echo "[\$0] started \${APP_ID}*.jar
EOX
echo "generated start scripts for [${_artifactId}]"

cat <<EOX >> "${_target_path}/${_archive_version}/config/management.properties"
#com.sun.management.jmxremote.access.file=\${base_dir}/config/jmxremote.access
com.sun.management.jmxremote.authenticate=false
com.sun.management.jmxremote.local.only=true
#com.sun.management.jmxremote.password.file=\${base_dir}/config/jmxremote.password
com.sun.management.jmxremote.port=\${JMX_PORT}
com.sun.management.jmxremote.registry.ssl=false
#com.sun.management.jmxremote.ssl.config.file=\${base_dir}/config/jmxssl.properties
com.sun.management.jmxremote.ssl=false
com.sun.management.jmxremote.ssl.need.client.auth=false
EOX



cat <<'EOX' > "${_target_path}/${_archive_version}/scripts/stop.sh"
#!/bin/bash

[ ! -e ../../properties ] && . ../../properties

echo "[\$0] ending \${APP_ID:-${_uuid}}.*.jar"
pkill -f ".* -Dfdeploy.id=\${APP_ID:-${_uuid}} .*"
EXIT_VAL=\$?
if [[ \${EXIT_VAL} -ne 0 ]]; then
	echo "[\$0] \${APP_ID:-${_artifactId}}.*.jar stop failed..."
	exit \${EXIT_VAL}
else
	echo "[\$0] \${APP_ID:-${_artifactId}}.*.jar stopped..."
fi
EOX
echo "generated stop scripts for ${_artifactId}"




cat <<'EOX' > "${_target_path}/${_archive_version}/scripts/status.sh"
#!/bin/bash

[ ! -e ../../properties ] && . ../../properties

pgrep -f ".* -Dfdeploy.id=\${APP_ID:-${_uuid}} .*"
if [[ \$? -ne 0 ]] ; then
  echo "\${APP_ID:-${_uuid}}.*.jar is not running"
  exit 1
fi
exit 0
EOX
echo "generated status scripts for ${_artifactId}"

chmod -R 755 "${_target_path}/${_archive_version}/scripts/"
# ---- end - remove_create_scripts_snippet.sh - source