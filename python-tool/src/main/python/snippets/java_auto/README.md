# The anatomy of the Spring Boot Microservices deployment descriptor for ∱deploy.

## Required RTV's 

| RTV Name | Description | Example | 
|----------|-------------|--------|
| APP_ID | The application id, typically the `fdeploy.id` in the descriptor | `fxg-sefs-core-proxy` |
| JAVA_HOME | The home to the java installation  | `/opt/java/hotspot/7/current` |
| EAI_NUMBER | The Enterprise Application Inventory number | `6270` |


## Optional RTV's

| RTV Name | Description | Example (default) | 
|----------|-------------|--------|
| LOG_PREFIX | The jmx port for external management | `fxg_sefs_core` |
| APPD_ACCOUNT | AppDynamics account | `fedex1-test` |
| APPD_ACCOUNT_KEY | AppDynamics account key | `1dce5fc52c17` |
| APPD_APPNAME | AppDynamics application group name | `SU-SEFS-RELY-L1` |
| APPD_CONTROLLER_HOST | AppDynamics host controller | `fedex1-test.saas.appdynamics.com` |
| EUREKA_PORT | The eureka discovery port | `8093` | 
| EUREKA_HOST | The eureka discovery host | `fdsefscore-mem-da01.test.cloud.fedex.com` | 
| HTTP_PORT | The application http port | `FXG_${level}_BW_ADMIN_DOMAIN` |
| JMX_PORT | The jmx port for external management | `15034` |
| JMX_AUTHENTICATE | The jmx using authentication | `false` |
| JMX_SSL | The jmx is using SSL  | `false` |
| SPRING_PROFILE | The active spring profile | `L1,wsso` |
| TANGOSOL_OVERRIDE | The override xml |  |
| TANGOSOL_CACHECONFIG | The cache configuration | |
| TANGOSOL_MANAGEMENT_REMOTE | The remote management flag | `true` |
| TANGOSOL_MANAGEMENT | The general management setting | `all` |
| TANGOSOL_LOCALPORT_ADJUST | The adjustment flag for local port | `true` |
| TANGOSOL_LOCALPORT | The communication port | `59085` |
| TANGOSOL_JCACHE_CONFIG | JCache Configuration flag | `passthrough` | 


If appd installation is detected the machine agent will be automatically started.

If `TANGOSOL_OVERRRIDE` AND `TANGOSOL_CACHECONFIG` are defined it will extend the `JAVA_OPTIONS` with the tangosol cache management JVM variables.



## Target URL Syntax

The target url syntax matches the `bash_java_generator` url notation due to the fact that their installation very similar.


## Example:

```json
[{
  "type": "java_auto",
  "levels": [{
      "rtv": [{
          "value": "/opt/java/hotspot/8/latest",
          "name": "JAVA_HOME"
        },{
            "value": "3534545",
            "name": "EAINUMBER"
        },{
            "value": "scm-pmi-boot",
            "name": "APP_ID"
          }],
      "level": "L1",
      "targets": [
        "pmidev@urh00110.ground.fedex.com:/opt/fedex/pmi/dev"
      ]
    }

  ],
  "content": [{
      "gav": "com.fedex.ground.pmi:pmi-web:0.0.1-SNAPSHOT:jar",
      "saveArchiveName": "pmi-web.jar"
    }
  ],
  "id": "scm-pmi-boot",
  "name_format": "JAVA_SCM_PMI_BOOT_%0d",
  "platform": "bash_generator"
}]

```


# Moving From `java` to `java_auto`


```json
[
	{
		"type" : "java",
		"rtv"
		: [
			{ "name" : "HTTP_PORT",   "value" : "8094" },
			{ "name" : "EUREKA_PORT", "value" : "8093" },
			{ "name" : "APP_ID",      "value" : "fxg-sefs-core-proxy" },
			{ "name" : "JMX_PORT",    "value" : "15553" },
			{ "name" : "JMX_AUTHENTICATE", "value" : "false" },
			{ "name" : "JMX_SSL",          "value" : "false" },
			{ "name" : "APPD_ACCOUNT",     "value" : "fedex1-test" },
			{ "name" : "APPD_ACCOUNT_KEY", "value" : "1dce5fc52c17" },
			{ "name"  : "APPD_CONTROLLER_HOST", 	"value" : "fedex1-test.saas.appdynamics.com"},
			{ "name" : "EAI_NUMBER", "value" : "6873" },
			{ "name" : "LOG_PREFIX", "value" : "fxg_sefs_core" }
		],
		"includeBaseDirectory" : "true",
		"relink"               : "true",
		"overwriteInstall"     : "false",
		"levels"               : [
			{
				"level" : "L1",
				"rtv"
				: [
					{ "name" : "SPRING_PROFILE", "value" : "L1" },
					{
						"name"  : "EUREKA_HOST",
						"value" : "fgsefscore-mem-da01.test.cloud.fedex.com"
					},
					{ "name" : "APPD_APPNAME", "value" : "SU-SEFS-RELY-L1" }
				],
				"targets"
				: [
"sefscm@fgsefscore-mem-da01.test.cloud.fedex.com:/opt/fedex/sefs/fxg-sefs-core-proxy"
				]
```

to 
only the `JAVA_HOME` needs added, the others are all honored:

```json
[
	{
		"type" : "java",
		"rtv"
		: [
			{ "name" : "HTTP_PORT",   "value" : "8094" },
			{ "name" : "EUREKA_PORT", "value" : "8093" },
			{ "name" : "APP_ID",      "value" : "fxg-sefs-core-proxy" },
			{ "name" : "JMX_PORT",    "value" : "15553" },
			{ "name" : "JAVA_HOME",  "value" : "/opt/java/hotspot/8/current/" },
			{ "name" : "JMX_AUTHENTICATE", "value" : "false" },
			{ "name" : "JMX_SSL",          "value" : "false" },
			{ "name" : "APPD_ACCOUNT",     "value" : "fedex1-test" },
			{ "name" : "APPD_ACCOUNT_KEY", "value" : "1dce5fc52c17" },
			{ "name"  : "APPD_CONTROLLER_HOST", 	"value" : "fedex1-test.saas.appdynamics.com"},
			{ "name" : "EAI_NUMBER", "value" : "6873" },
			{ "name" : "LOG_PREFIX", "value" : "fxg_sefs_core" }
		],
		"includeBaseDirectory" : "true",
		"relink"               : "true",
		"overwriteInstall"     : "false",
		"levels"               : [
			{
				"level" : "L1",
				"rtv"
				: [
					{ "name" : "SPRING_PROFILE", "value" : "L1" },
					{
						"name"  : "EUREKA_HOST",
						"value" : "fgsefscore-mem-da01.test.cloud.fedex.com"
					},
					{ "name" : "APPD_APPNAME", "value" : "SU-SEFS-RELY-L1" }
				],
				"targets"
				: [
"sefscm@fgsefscore-mem-da01.test.cloud.fedex.com:/opt/fedex/sefs/fxg-sefs-core-proxy"
				]

```
