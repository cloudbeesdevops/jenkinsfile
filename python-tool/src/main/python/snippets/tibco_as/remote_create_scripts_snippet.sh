# ---- begin - remove_create_scripts_snippet.sh - source
# check ear
_log_dir=\${LOG_DIR}
[ ! -e "\${LOG_DIR}" ] && mkdir -p "\${LOG_DIR}"


export LOG4J_FILE="${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
export sfs_script_HOST_PRIVATE_IP=\$(/sbin/ifconfig|perl -ane 'if(\$_=~/172\./){\$F[1]=~s/addr://g;print \$F[1]}')
export sfs_script_HOST=\$(hostname -s)
echo "PRIVATE_IP=\${sfs_script_HOST_PRIVATE_IP}"
echo "HOST      =\${sfs_script_HOST}"

base_dir="${_target_path}/${_archive_version}"

# export LOG4J_FILE="${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
# if [[ ! -e "${_target_path}/${_archive_version}/config/${_level}/log4j.xml" ]]; then
#    if [[ ! -e "${_target_path}/${_archive_version}/config/log4j.xml" ]]; then
#       echo "╔═╗╦═╗╦═╗╔═╗╦═╗";
#       echo "║╣ ╠╦╝╠╦╝║ ║╠╦╝";
#       echo "╚═╝╩╚═╩╚═╚═╝╩╚═";
#       echo "No log4j.xml found for ${_artifactId} in installation."
#       exit 1
#    else
#       export LOG4J_FILE="${_target_path}/${_archive_version}/config/log4j.xml"
#    fi
# fi

_as_artifact="${_artifactId}"
_as_input="${_target_path}/${_archive_version}/config/${_level}/\${AS_INPUT}"
if [[ ! -e "\${_as_input}" ]]; then
   echo "╔═╗╦═╗╦═╗╔═╗╦═╗";
   echo "║╣ ╠╦╝╠╦╝║ ║╠╦╝";
   echo "╚═╝╩╚═╩╚═╚═╝╩╚═";
   echo "No metaspace file found for \${_as_input} in installation."
   exit 1
fi
# move msd to neutral area.
cp -v \${as_input} "${_target_path}/${_archive_version}/config/${_artifactId}.msd"



echo ">> metaspace definition file ${_as_input} / \${_as_input}"

[ ! -e "${_target_path}/${_archive_version}/scripts" ] && mkdir -p "${_target_path}/${_archive_version}/scripts"
cd "${_target_path}/${_archive_version}"

## 'EOF' vs EOF is very important preventing variables from expanding in the block
cat <<EOX > "${_target_path}/${_archive_version}/scripts/start.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties

export JAVA_HOME=\${JAVA_HOME:-/opt/java/hotspot/8/current}
export AS_HOME=\${BE_HOME:-/opt/tibco/as/2.1}
export PATH=\${PATH}:\${JAVA_HOME}/bin:\${AS_HOME}/bin
export LD_LIBRARY_PATH=\${AS_HOME}/lib:\${LD_LIBRARY_PATH}
export sfs_script_HOST=\$(hostname -s)
ulimit unlimited
set -xv

if [[ -e /opt/appd/current/appagent/javaagent.jar ]]; then
  . /opt/appd/current/scripts/machine_agent_appd.sh \${EAINUMBER} restart > \${LOG_ROOT}/machine-appd.out 2>&1
fi


nohup \${JAVA_HOME}/bin/java -cp \${AS_HOME}/lib/as-common.jar:\${AS_HOME}/lib/antlr-3.2.jar:\${AS_HOME}/lib/as-agent.jar \
-server -d64 -Xms256m -Xmx4G \
-Dcom.sun.management.config.file=\${base_dir}/config/management.properties \
-Dfdeploy.id=${_artifactId} \
com.tibco.as.space.agent.AgentConsole \
-metaspace \${AS_METASPACE} \
-listen 'tcp://\${sfs_script_HOST_PRIVATE_IP}:\${AS_LISTEN_PORT}' \
-discovery '\${AS_DISCOVERY}' \
-member_name \${sfs_script_HOST}.\${AS_MEMBER_NAME_PREFIX} \
-log_debug 3 -log \${AS_LOG} \
-debug 3 \
-data_store \${AS_DATA_STORE} \
-worker_thread_count 32 \
-rx_buffer_size 50000 \
-monitor_system true \
-input \${_as_input} \
-member_timeout 30000 \
-cluster_suspend_threshold -1 2> \${LOG_DIR}/stderr.log 1> \${LOG_DIR}/stdout.log < /dev/null &
exit 0
EOX
echo "generated start scripts for [${_artifactId}]"

cat <<EOX >> "${_target_path}/${_archive_version}/config/management.properties"
#com.sun.management.jmxremote.access.file=\${base_dir}/config/jmxremote.access
com.sun.management.jmxremote.authenticate=false
com.sun.management.jmxremote.local.only=true
#com.sun.management.jmxremote.password.file=\${base_dir}/config/jmxremote.password
com.sun.management.jmxremote.port=\${JMX_PORT}
com.sun.management.jmxremote.registry.ssl=false
#com.sun.management.jmxremote.ssl.config.file=\${base_dir}/config/jmxssl.properties
com.sun.management.jmxremote.ssl=false
com.sun.management.jmxremote.ssl.need.client.auth=false
EOX



cat <<EOX > "${_target_path}/${_archive_version}/scripts/stop.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
# trap empty values
[[ "\${AS_MEMBER_NAME_PREFIX}" == "" ]] && exit 2
[[ "\${sfs_script_HOST}" == "" ]] && exit 2
EOX
cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/stop.sh"
pgrep -f "\${sfs_script_HOST}.\${AS_MEMBER_NAME_PREFIX}" | xargs -i% -t kill -9 %
EOX
echo "generated stop scripts for ${_artifactId}"




cat <<EOX > "${_target_path}/${_archive_version}/scripts/status.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
# trap empty values
[[ "\${AS_MEMBER_NAME_PREFIX}" == "" ]] && exit 2
[[ "\${sfs_script_HOST}" == "" ]] && exit 2
EOX
cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/status.sh"
rc=\$(pgrep -f "\${sfs_script_HOST}.\${AS_MEMBER_NAME_PREFIX}")
if [[ "\$rc" == "" ]] ; then
  echo "as-agent \${sfs_script_HOST}.\${AS_MEMBER_NAME_PREFIX} is not running"
  exit 1
fi
echo \$rc
exit 0
EOX

echo "generated status scripts for ${_artifactId}"

chmod -R 755 "${_target_path}/${_archive_version}/scripts/"
# ---- end - remove_create_scripts_snippet.sh - source