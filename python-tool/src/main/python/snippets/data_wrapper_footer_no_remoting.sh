# ---- begin - data_wrapper_footer.sh - source
   log_info " > executing : ${MACHINE} | ${_ssh} | ${_script_name}"
   #${verboseFlag}
   if [[ "${local_base_dir}" != "." ]]; then
      [ ! -e ${local_base_dir} ] && mkdir -p "${local_base_dir}"
   fi
   [ ! -e "${local_base_dir}/master.sh" ] && echo "#!/bin/bash -xvs" > "${local_base_dir}/master.sh"

   printf -v script 'exec /usr/bin/ssh %s %s %s "$(<%s.txt)" 2>&1 > "%s/stdout-%s-%s.log" &' "${_ssh}" "${_identity_}" "${_ssh_options}" "${local_base_dir}/${_script_name}" "${local_base_dir}" "${_host}" "${_artifactId}"
   echo -E "$script" >> "${local_base_dir}/master.sh"
done # --- 

log_info "starting batch execution"
. ${local_base_dir}/master.sh
log_info "completed"
# ---- end - data_wrapper_footer.sh - source
