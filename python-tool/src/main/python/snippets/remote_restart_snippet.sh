# ---- begin - remote_restart_snippet.sh - source
   # restart with new process
   #stty -ixon
   echo "╦═╗╔═╗╔═╗╔╦╗╔═╗╦═╗╔╦╗╦╔╗╔╔═╗";
   echo "╠╦╝║╣ ╚═╗ ║ ╠═╣╠╦╝ ║ ║║║║║ ╦${_artifactId}-${_archive_version}";
   echo "╩╚═╚═╝╚═╝ ╩ ╩ ╩╩╚═ ╩ ╩╝╚╝╚═╝(${_user}@${_host})";
   if [[ -e "${_target_path}/current/scripts" ]]; then
      cd ${_target_path}/current/scripts
      echo -n "STATUS: ${_host}:${_archive_filename} " && ./status.sh
      if [ \$? -eq 0 ]; then
         echo "found process, stopping..."
         ./stop.sh
         sleep 5
      fi
      ./start.sh
      sleep 5
      echo -n "STATUS: ${_host}:${_archive_filename} " && ./status.sh
      echo
      if [ \$? -eq 0 ]; then
         echo "╔═╗╔╦╗╔═╗╦═╗╔╦╗╔═╗╔╦╗";
         echo "╚═╗ ║ ╠═╣╠╦╝ ║ ║╣  ║║${_artifactId}-${_archive_version}";
         echo "╚═╝ ╩ ╩ ╩╩╚═ ╩ ╚═╝═╩╝(${_user}@${_host})";
      else
         echo "┌─┐┌┬┐┌─┐┬─┐┌┬┐  ┌─┐┌─┐┬┬  ┬ ┬┬─┐┌─┐";
         echo "└─┐ │ ├─┤├┬┘ │   ├┤ ├─┤││  │ │├┬┘├┤ ${_artifactId}-${_archive_version}";
         echo "└─┘ ┴ ┴ ┴┴└─ ┴   └  ┴ ┴┴┴─┘└─┘┴└─└─┘(${_user}@${_host})";
      fi
   else
      echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
      echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}-${_archive_version}";
      echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
      echo "aborting restart ${_target_path}/current/scripts not found!"
      exit 1
   fi
# ---- end - remote_restart_snippet.sh - source
