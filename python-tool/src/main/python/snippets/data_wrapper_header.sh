#!/bin/bash
#set -e
#export PS4='+(${BASH_SOURCE}:${LINENO}): ${FUNCNAME[0]:+${FUNCNAME[0]}(): }'
#export PS4='+(\${BASH_SOURCE}:\${LINENO}): \${FUNCNAME[0]:+\${FUNCNAME[0]}(): }'
#set -xv
# command=%s:action=%s, on level
_level=%s
level=${_level}
# ---- begin - data_wrapper_header.sh - source
function log_error {
   set > /dev/null
   %secho "[ERROR]: $@"
}
function log_info {
   set > /dev/null
   %secho "[INFO] : $@"
}
function log_debug {
   set > /dev/null
   %secho "[DEBUG]: $@"
}
function log_warn {
   set > /dev/null
   %secho "[WARN] : $@"
}
function join_by {
   local IFS="$1"; shift; echo "\$*";
}

set -m # Enable Job Control
_action="%s"
_identity_="%s"
_ssh_options="%s"
_verbose_flag="%s"
_echo_envvars="%s"
_switch="%s"
_uuid="%s"
_no_wait="%s"
_timeout="%s"

declare -a MACHINE_LIST=(
%s );
declare -a UPLOAD_CONTENTS=(
%s );
_upload_elements="%s"

log_debug "elements ${_upload_elements}"

# set working base on the local host
local_base_dir="%s"
[ ! -e "${local_base_dir}" ] && mkdir -p "${local_base_dir}"
[ ! -e "${local_base_dir}" ] && log_error " ! aborting unable creating : ${local_base_dir}" && exit 1
cd "${local_base_dir}"
#
for MACHINE in ${MACHINE_LIST[@]}; do
   IFS=':' read -ra URL <<< "$MACHINE"
   log_debug " > machine=${URL}"
   _uri_path="${URL[1]}"
   _url="${URL[0]}"
   _archive_version="%s"
   _archive_filename="%s"
   _artifactId="%s"
   IFS='@' read -ra HOST <<< "$_url"
   _user="${HOST[0]}"
   _host="${HOST[1]}"
   _ssh="${_user}@${_host}"
   if [[ "${_switch}" == "" ]]; then
      if [[ "${_user}" == *cm ]]; then
         b=${_user%ccm}
         _ssh="${b}@${_host}";
         _user="${b}"
      fi
   fi
   _landing_area="/var/tmp/${_user}"
   IFS='?' read -ra URI <<< "$_uri_path"
   _target_path="${URI[0]}"
   _uri="${URI[1]}"
   _script_name="${_host}${_uuid}-${_action}"
   log_info  " < processing : target=${MACHINE} sudo=${_switch} "
   log_debug " < host=${_host} | url_path=${_uri_path} | ssh=${_ssh}"
# ---- end - data_wrapper_header.sh - source
