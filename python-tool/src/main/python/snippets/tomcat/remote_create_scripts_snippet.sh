# ---- begin - remove_create_scripts_snippet.sh - source
# check ear
_log_dir=\${LOG_DIR}
[ ! -e "\${LOG_DIR}" ] && mkdir -p "\${LOG_DIR}"
export CATALINA_BASE="${_target_path}/${_archive_version}"
[ ! -e "${_target_path}/${_archive_version}/scripts" ] && mkdir -p "${_target_path}/${_archive_version}/scripts"

cd "\${CATALINA_BASE}"

cat <<EOX > "${_target_path}/${_archive_version}/bin/setenv.sh"
# CATALINA_HOME and CATALINA_BASE have to be set outside of setenv.sh and thus were exported above
export JAVA_HOME=\${JAVA_HOME:-/opt/java/hotspot/7/current}
export JRE_HOME=\${JRE_HOME:-/opt/java/hotspot/7/current/jre/}
export CATALINA_HOME=\${CATALINA_HOME:-/opt/tomcat/current}
export CATALINA_BASE=\${CATALINA_BASE}
export CATALINA_TMPDIR=\${CATALINA_BASE}/temp
export LOGGING_CONFIG="-Djava.util.logging.config.file=\${CATALINA_BASE}/config/logging.properties"
export CATALINA_OUT=\${LOG_DIR}/catalina.out
export CATALINA_PID=\${CATALINA_BASE}/tomcat.pid
export TOMCAT_HTTP_PORT=\${TOMCAT_HTTP_PORT:-8089}
export TIBCO_HOME=\${TIBCO_HOME:-/opt/tibco}
export AS_HOME=\${AS_HOME:-/opt/tibco/as/2.1}
export CATALINA_OUT=\${LOG_DIR}/catalina.out
export LD_LIBRARY_PATH=\${AS_HOME}/lib:\${LD_LIBRARY_PATH}
EOX
chmod 755 "${_target_path}/${_archive_version}/bin/setenv.sh"
source "${_target_path}/${_archive_version}/bin/setenv.sh"

#
# Create conf/* files from conf/template/*
#
for file in \$(ls -1 \${CATALINA_BASE}/config/template); do
    envsubst < \${CATALINA_BASE}/config/template/\${file} > \${CATALINA_BASE}/config/\${file}
    diff \${CATALINA_BASE}/config/template/\${file} \${CATALINA_BASE}/config/\${file}
done

[[ ! -e ./conf ]] && ln -snf config conf
[[ ! -e ./webapps ]] && mkdir webapps
cp -v ./bin/*.*ar ./webapps



## 'EOF' vs EOF is very important preventing variables from expanding in the block
cat <<EOX > "${_target_path}/${_archive_version}/scripts/start.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
cd \${CATALINA_BASE}
if [ -r "\$CATALINA_BASE/bin/setenv.sh" ]; then
  . "\$CATALINA_BASE/bin/setenv.sh"
fi
if [[ -e /opt/appd/current/appagent/javaagent.jar && "${EAINUMBER}" != "" ]]; then
  . /opt/appd/current/scripts/app_agent_appd.sh \${EAINUMBER} > \${LOG_ROOT}/appd.out 2>&1
  . /opt/appd/current/scripts/machine_agent_appd.sh \${EAINUMBER} restart > \${LOG_ROOT}/machine-appd.out 2>&1
fi

\${CATALINA_HOME}/bin/catalina.sh start >\${LOG_DIR}/catalina_sh_start_out.log 2>&1
[ -e \${LOG_DIR}/catalina_sh_start_out.log ] && tail \${LOG_DIR}/catalina_sh_start_out.log
[ -e \${LOG_DIR}/catalina.out ] && tail \${LOG_DIR}/catalina.out
EOX
echo "generated start scripts for [${_artifactId}]"



cat <<'EOX' > "${_target_path}/${_archive_version}/scripts/stop.sh"
#!/bin/bash
function isrunning {
   echo \$(pgrep -f ".+\${CATALINA_BASE}")
}
function taillog {
   [ -e \${LOG_DIR}/catalina_sh_start_out.log ] && tail \${LOG_DIR}/catalina_sh_start_out.log
   [ -e \${LOG_DIR}/catalina.out ] && tail \${LOG_DIR}/catalina.out
}
[[ -e ../../properties ]] && . ../../properties
EOX
cat <<EOX >> "${_target_path}/${_archive_version}/scripts/stop.sh"
cd \${CATALINA_BASE}
if [ -r "\$CATALINA_BASE/bin/setenv.sh" ]; then
  . "\$CATALINA_BASE/bin/setenv.sh"
fi
EOX
cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/stop.sh"
rc="\$(isrunning)"
if [[ "\$rc" == "0" || "\$rc" == "" ]] ; then
EOX
cat <<EOX >> "${_target_path}/${_archive_version}/scripts/stop.sh"
  echo "tomcat \${CATALINA_BASE} is not running"
  exit 0
else
EOX
cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/stop.sh"
   echo "killing=\$rc"
   kill -9 \$rc
   [ -e "\${CATALINA_PID}" ] && rm \${CATALINA_PID}
   taillog
fi
EOX
echo "generated stop scripts for [${_artifactId}]"



cat <<EOX > "${_target_path}/${_archive_version}/scripts/status.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
export PID_FILE=\${CATALINA_BASE}/tomcat.pid
EOX
cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/status.sh"
if [[ -r \${PID_FILE} ]]; then
    TOMCAT_PID=\$(cat \${PID_FILE})
    ps -fp \${TOMCAT_PID} >/dev/null 2>&1
    if [[ \$? -eq 0 ]]; then
        echo "Tomcat running with PID \${TOMCAT_PID}"
    else
        echo "PID file found but Tomcat process is not running with PID \${TOMCAT_PID}"
        exit 1
    fi
else
    echo "PID file not found.  Tomcat status unknown."
    exit 1
fi
EOX
echo "generated status scripts for [${_artifactId}]"
chmod -R 755 "${_target_path}/${_archive_version}/scripts/"
# ---- end - remove_create_scripts_snippet.sh - source