# ---- begin - remote_stop_snippet.sh - source
if [[ -e "${_target_path}/current/scripts" ]]; then
   cd ${_target_path}/current/scripts
   echo -n "STATUS: ${_host}:${_archive_filename}" && ./status.sh
   if [ \$? -eq 0 ]; then
      echo "found process, stopping... ${_archive_version}"
      ./stop.sh
      sleep 5
      echo -n "STATUS: ${_host}:${_archive_filename}" && ./status.sh
      # now we should get >0 back since not running.
      if [[ \$? -ne 0 || "\$?" != "" ]]; then
         echo "╔═╗╔╦╗╔═╗╔═╗╔═╗╔═╗╔╦╗";
	      echo "╚═╗ ║ ║ ║╠═╝╠═╝║╣  ║║${_artifactId}-${_archive_version}";
	      echo "╚═╝ ╩ ╚═╝╩  ╩  ╚═╝═╩╝(${_user}@${_host})";
	   else
         echo "┌─┐┌┬┐┌─┐┌─┐  ┌─┐┌─┐┬┬  ┬ ┬┬─┐┌─┐";
         echo "└─┐ │ │ │├─┘  ├┤ ├─┤││  │ │├┬┘├┤ ${_artifactId}-${_archive_version}";
         echo "└─┘ ┴ └─┘┴    └  ┴ ┴┴┴─┘└─┘┴└─└─┘(${_user}@${_host})";
      fi
   else
      echo "╔═╗╔╦╗╔═╗╔═╗╔═╗╔═╗╔╦╗";
      echo "╚═╗ ║ ║ ║╠═╝╠═╝║╣  ║║${_artifactId}-${_archive_version}";
      echo "╚═╝ ╩ ╚═╝╩  ╩  ╚═╝═╩╝(${_user}@${_host})";
   fi
else
   echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
   echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}-${_archive_version}";
   echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
   echo " ! aborting no current symlink available ${_target_path}/current."  && exit 1
fi
# ---- end - remote_stop_snippet.sh - source
