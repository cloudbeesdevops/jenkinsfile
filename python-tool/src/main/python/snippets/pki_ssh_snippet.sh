   # ---- begin - pki_ssh_snippet.sh - source
   log_info " > testing ssh ${_user}@${_host}";
   /usr/bin/ssh -n -T ${_identity_} ${_ssh_options} "${_ssh}" uname -a </dev/null
   if [ $? -eq 0 ]; then
      echo "╔═╗╔═╗╦ ╦  ╔═╗╔═╗╔═╗╔═╗╔═╗╔╦╗";
      echo "╚═╗╚═╗╠═╣  ╠═╝╠═╣╚═╗╚═╗║╣  ║║";
      echo "╚═╝╚═╝╩ ╩  ╩  ╩ ╩╚═╝╚═╝╚═╝═╩╝(ssh ${_identity_} ${_user}@${_host})";
   else
      echo "╔═╗╔═╗╦ ╦  ╔═╗╔═╗┬╦  ╔═╗╔╦╗";
      echo "╚═╗╚═╗╠═╣  ╠╣ ╠═╣│║  ║╣  ║║";
      echo "╚═╝╚═╝╩ ╩  ╚  ╩ ╩┴╩═╝╚═╝═╩╝(ssh ${_identity_} ${_user}@${_host})";
   fi
   # ---- end - pki_ssh_snippet.sh - source
