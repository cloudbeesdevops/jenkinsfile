# ---- begin - remove_create_scripts_snippet.sh - source
# check ear
_log_dir=\${LOG_DIR}
[ ! -e "\${LOG_DIR}" ] && mkdir -p "\${LOG_DIR}"


export LOG4J_FILE="${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
export sfs_script_HOST_PRIVATE_IP=\$(/sbin/ifconfig|perl -ane 'if(\$_=~/172\./){\$F[1]=~s/addr://g;print \$F[1]}')
export sfs_script_HOST_PUBLIC_IP=\$(hostname -i)
export sfs_script_HOST=\$(hostname -s)
echo "PRIVATE_IP=\${sfs_script_HOST_PRIVATE_IP}"
echo "PUBLIC_IP =\${sfs_script_HOST_PUBLIC_IP}"
echo "HOST      =\${sfs_script_HOST}"

base_dir="${_target_path}/${_archive_version}"

export AGENT_HOST_IP="\${sfs_script_HOST_PRIVATE_IP}"
if [[ "\${LEECH_TYPE}" == "PUBLIC" ]]; then
   AGENT_HOST_IP="\${sfs_script_HOST_PUBLIC_IP}"
fi
_as_artifact="${_artifactId}"
_as_input="${_target_path}/${_archive_version}/config/${_level}/\${AS_INPUT}"

[ ! -e "${_target_path}/${_archive_version}/scripts" ] && mkdir -p "${_target_path}/${_archive_version}/scripts"
cd "${_target_path}/${_archive_version}"

## 'EOF' vs EOF is very important preventing variables from expanding in the block
cat <<EOX > "${_target_path}/${_archive_version}/scripts/start.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
export JAVA_HOME=\${JAVA_HOME:-/opt/java/hotspot/8/current}
export AS_HOME=\${BE_HOME:-/opt/tibco/as/2.1}
export PATH=\${PATH}:\${JAVA_HOME}/bin:\${AS_HOME}/bin
export LD_LIBRARY_PATH=\${AS_HOME}/lib:\${LD_LIBRARY_PATH}
export sfs_script_HOST=\$(hostname -s)
ulimit unlimited

set -xv
cd \${AS_HOME}/bin
./as-agent \
-metaspace \${AS_METASPACE} \
-listen 'tcp://\${sfs_script_HOST_PRIVATE_IP}' \
-discovery '\${AS_DISCOVERY}' \
-member_name \${sfs_script_HOST}.\${AS_MEMBER_NAME_PREFIX} \
-remote_listen tcp://\${AGENT_HOST_IP}:\${AS_REMOTE_LISTEN_PORT} \
-log \${AS_LOG} \
-autojoin.role leech  2> \${LOG_DIR}/stderr.log 1> \${LOG_DIR}/stdout.log < /dev/null &
exit 0
EOX
echo "generated start scripts for [${_artifactId}]"

cat <<EOX > "${_target_path}/${_archive_version}/scripts/stop.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
# trap empty values
[[ "\${AS_MEMBER_NAME_PREFIX}" == "" ]] && exit 2
[[ "\${sfs_script_HOST}" == "" ]] && exit 2
EOX
cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/stop.sh"
pgrep -f "\${sfs_script_HOST}.\${AS_MEMBER_NAME_PREFIX}" | xargs -i% -t kill -9 %
EOX
echo "generated stop scripts for ${_artifactId}"

if [[ -e /opt/appd/current/appagent/javaagent.jar ]]; then
  . /opt/appd/current/scripts/machine_agent_appd.sh \${EAINUMBER} restart > \${LOG_ROOT}/machine-appd.out 2>&1
fi

cat <<EOX > "${_target_path}/${_archive_version}/scripts/status.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
# trap empty values
[[ "\${AS_MEMBER_NAME_PREFIX}" == "" ]] && exit 2
[[ "\${sfs_script_HOST}" == "" ]] && exit 2
export member_prefix="\${sfs_script_HOST}.\${AS_MEMBER_NAME_PREFIX}"
EOX
cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/status.sh"
rc=\$(pgrep -f "\${member_prefix}")
if [[ "\$rc" == "" ]] ; then
  echo "as-agent \${member_prefix} is not running"
  exit 1
fi
echo \$rc
exit 0
EOX

echo "generated status scripts for ${_artifactId}"

chmod -R 755 "${_target_path}/${_archive_version}/scripts/"
# ---- end - remove_create_scripts_snippet.sh - source