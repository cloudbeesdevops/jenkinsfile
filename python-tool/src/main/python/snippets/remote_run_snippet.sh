# ---- begin - remote_run_snippet.sh - source
if [[ -e "${_uri_path}" ]]; then
   . "${_uri_path}"
   if [ \$? -eq 0 ]; then
      echo "╔═╗═╗ ╦╔═╗╔═╗╦ ╦╔╦╗╦╔═╗╔╗╔  ╔═╗╔═╗╔═╗╔═╗╔═╗╔╦╗(script=${_uri_path}";
      echo "║╣ ╔╩╦╝║╣ ║  ║ ║ ║ ║║ ║║║║  ╠═╝╠═╣╚═╗╚═╗║╣  ║║(app=${_archive_filename})";
      echo "╚═╝╩ ╚═╚═╝╚═╝╚═╝ ╩ ╩╚═╝╝╚╝  ╩  ╩ ╩╚═╝╚═╝╚═╝═╩╝(${_user}@${_host})";
   else
      echo "╔═╗═╗ ╦╔═╗╔═╗╦ ╦╔╦╗╦╔═╗╔╗╔  ╔═╗╔═╗╦ ╦  ╔═╗╔╦╗(script=${_uri_path}";
      echo "║╣ ╔╩╦╝║╣ ║  ║ ║ ║ ║║ ║║║║  ╠╣ ╠═╣║ ║  ║╣  ║║(app=${_archive_filename})";
      echo "╚═╝╩ ╚═╚═╝╚═╝╚═╝ ╩ ╩╚═╝╝╚╝  ╚  ╩ ╩╩ ╩═╝╚═╝═╩╝(${_user}@${_host})";
   fi
else
   echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
   echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}";
   echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
   echo " ! aborting no script available ${_uri_path}" && exit 1
fi
# ---- end - remote_script_run.sh - source
