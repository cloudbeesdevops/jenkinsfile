# ---- begin - remove_create_scripts_snippet.sh - source
# check ear
_log_dir=\${LOG_DIR}
[ ! -e "\${LOG_DIR}" ] && mkdir -p "\${LOG_DIR}"
[[ ! -e "${_target_path}/${_archive_version}/config/" ]] && mkdir -p "${_target_path}/${_archive_version}/config/"

export LOG4J_FILE="${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
export sfs_script_HOST_PRIVATE_IP=\$(/sbin/ifconfig|perl -ane 'if(\$_=~/172\./){\$F[1]=~s/addr://g;print \$F[1]}')
export ARTIFACT_ID="${_artifactId}"
echo "PRIVATE_IP=\${sfs_script_HOST_PRIVATE_IP}"

base_dir="${_target_path}/${_archive_version}"
if [[ -e "${_target_path}/${_archive_version}/bin" ]]; then
   base_dir="${_target_path}/${_archive_version}/bin"
fi

export LOG4J_FILE="${_target_path}/${_archive_version}/config/${_level}/log4j.xml"
if [[ ! -e "${_target_path}/${_archive_version}/config/${_level}/log4j.xml" ]]; then
   if [[ ! -e "${_target_path}/${_archive_version}/config/log4j.xml" ]]; then
      echo "╔═╗╦═╗╦═╗╔═╗╦═╗";
      echo "║╣ ╠╦╝╠╦╝║ ║╠╦╝";
      echo "╚═╝╩╚═╩╚═╚═╝╩╚═";
      echo "No log4j.xml found for ${_artifactId} in installation."
      exit 1
   else
      export LOG4J_FILE="${_target_path}/${_archive_version}/config/log4j.xml"
   fi
fi
# deleting unused elements, need to clean up in the source
find "${_target_path}/${_archive_version}" \( -name '*template*' -o -name '*PU.properties' -o -name '*Appmanage*' -o -name 'tmp*' \) -exec rm -v {} \;

_ear_artifact="${_artifactId}"
_ear_cdd=\$( find "${_target_path}/${_archive_version}" -name "${_artifactId}*.cdd" | head -1 )
_ear_properties=\$( find "${_target_path}/${_archive_version}" -name '*.properties' -print0 | grep -FzZ "${_level}/${_artifactId}"  )
_ear=\$( find "${_target_path}/${_archive_version}" -name '*.ear' | head -1 )

echo "ear_xml=\$_ear_properties"
echo "ear    =\$_ear_cdd"

echo ">> properties ${_ear_properties} / \${_ear_properties}"

if [[ "\${_ear_cdd}" == "" || "\${_ear}" == "" || "\${_ear_properties}" == ""  ]]; then
   #_ear_cdd="${_target_path}/${_archive_version}/config/${_level}/${_artifactId}_template.cdd"
   echo "╔═╗╦═╗╦═╗╔═╗╦═╗ = cdd: \${_ear_cdd}";
   echo "║╣ ╠╦╝╠╦╝║ ║╠╦╝ = prp: \${_ear_properties}";
   echo "╚═╝╩╚═╩╚═╚═╝╩╚═ = ear: \${_ear}";
   echo "No CDD ${_ear_cdd} found for ${_artifactId}"
   exit 1
fi

# replace private IP Addresses
cp -v \${_ear_properties} "${_target_path}/${_archive_version}/config/${_artifactId}.properties"

find "${_target_path}/${_archive_version}" -type f \( -name log4j.xml -o -name '*.properties' -o -name '*.tra' \) -exec perl -i -pe 's/.{sfs_script_HOST_PRIVATE_IP}/\$ENV{sfs_script_HOST_PRIVATE_IP}/g;s/^(Engine.Log.Dir=).+/\$1\$ENV{LOG_DIR}/g;s/.{SILVERFABRIC_BE_LOG4J}/\$ENV{LOG4J_FILE}/g' {} \;

find "${_target_path}/${_archive_version}" -type f \( -name log4j.xml -o -name '*.properties' -o -name '*.tra' \) -exec perl -i -pe 's/\/var\/fedex\/tibco\/logs/\$ENV{LOG_DIR}/g' {} \;

find "${_target_path}/${_archive_version}" -type f \( -name log4j.xml -o -name '*.properties' -o -name '*.tra' \) -exec perl -i -pe 's/.{artifactId}/\$ENV{ARTIFACT_ID}/g;' {} \;
#"${_target_path}/${_archive_version}/config/${_level}/log4j.xml"

if [[ "\$_ear" != "" || "\$_ear_xml" != "" ]]; then
  rm -rf "${_target_path}/${_archive_version}/config/\${_ear_artifact}.ear"
  sleep 3
  cp -v \${_ear_cdd} "${_target_path}/${_archive_version}/config"
  cp -v \${_ear} "${_target_path}/${_archive_version}/config"
fi


[ ! -e "${_target_path}/${_archive_version}/scripts" ] && mkdir -p "${_target_path}/${_archive_version}/scripts"

## 'EOF' vs EOF is very important preventing variables from expanding in the block
cat <<'EOX' > "${_target_path}/${_archive_version}/scripts/start.sh"
#!/bin/bash

[[ -e ../../properties ]] && . ../../properties

export JAVA_HOME=\${JAVA_HOME:-/opt/java/hotspot/8/current}
export BE_HOME=\${BE_HOME:-/opt/tibco/be/5.2}
export PATH=\${PATH}:\${JAVA_HOME}/bin:\${BE_HOME}/bin
export ENGINE_NAME=\$(hostname -s)

ulimit unlimited

cd \${BE_HOME}/bin

if [[ -e /opt/appd/current/appagent/javaagent.jar ]]; then
  . /opt/appd/current/scripts/machine_agent_appd.sh \${EAINUMBER} restart > \${LOG_ROOT}/machine-appd.out 2>&1
fi


#set -xv
nohup \${BE_HOME}/bin/be-engine --propFile ${_target_path}/${_archive_version}/config/be-engine.tra \\
   -p ${_target_path}/${_archive_version}/config/${_level}/${_artifactId}.properties \\
   --propVar jmx_port=\${JMX_PORT:-31000} \\
   -u \${PU_NAME} \\
   -n \${LOGICAL_NAME} \\
EOX
cat <<EOX >> "${_target_path}/${_archive_version}/scripts/start.sh"
-c \${_ear_cdd} \${_ear} 1> \${_log_dir}/stdout-${_uuid}.log 2> \${_log_dir}/stderr-${_uuid}.log < /dev/null &
#set +xv
exit 0
EOX
echo "generated start scripts for [${_artifactId}]"

if [[ -e "${_target_path}/${_archive_version}/config/be-engine.tra" ]]; then
  #export classpath_dir=\$(find ${_target_path}/${_archive_version}/lib/ -name *jar | awk '{printf "%s:",\$1}' )
  cat <<OEPS > ${_target_path}/${_archive_version}/tmp$$.sh
  export base_dir="${_target_path}/${_archive_version}"
  export classpath_dir="${_target_path}/${_archive_version}/lib/"
  export engine_name=\$(hostname -s)
  export log4j_dir="${LOG4J_FILE}"
OEPS
  cat <<'OEPS' >> ${_target_path}/${_archive_version}/tmp$$.sh
  export level="${_level}"

  perl -i -pe 's/^(java.property.BE_HOME\s).+$/\$1\$ENV{BE_HOME}/g;s/=\/opt\/tibco/=\$ENV{TIBCO_HOME}/g' \${base_dir}/config/be-engine.tra
  perl -i -pe 's/\[LOGDIR\]/\$ENV{LOG_DIR}/g' \${base_dir}/config/${_artifactId}.cdd
  # note really needed but perform these explicitly if they differ
  perl -i -pe 's/^(tibco.env.TIB_HOME=).+\$/\$1\$ENV{TIBCO_HOME}/g;s/^(tibco.env.BE_HOME=).+\$/\$1\$ENV{BE_HOME}/g;s/^(tibco.env.AS_HOME=).+\$/\$1\$ENV{TIBCO_HOME}\/as\/2.1/g' \${base_dir}/config/be-engine.tra
  # adjusting the prepend path
  perl -i -pe 's/^(tibco.env.CUSTOM_EXT_PREPEND_CP=).*/\$1\$ENV{classpath_dir}/g;' \${base_dir}/config/be-engine.tra
  perl -i -pe 's/^(java.extended.properties=)(.+)\$/\$1\$2 -Dlog4j.debug -Dsf.log4j.log.dir=\$ENV{LOG_DIR} -Dlog4j.configuration=file:\/\/\$ENV{LOG4J_FILE}/g' \${base_dir}/config/be-engine.tra
  find \${base_dir}/config/ -name '*.properties' -exec perl -i -pe 's/\\\$\{.+AS_MEMBER_NAME\}/\$ENV{engine_name}/g' {} \; -ls
OEPS
  source ${_target_path}/${_archive_version}/tmp$$.sh

fi


cat <<EOX > "${_target_path}/${_archive_version}/scripts/stop.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties

EOX

cat <<EOX >> "${_target_path}/${_archive_version}/scripts/stop.sh"
pgrep -f 'be-engine.*${_target_path}/' | xargs -i% -t kill -9 %
EOX
echo "generated stop scripts for ${_artifactId}"



cat <<EOX > "${_target_path}/${_archive_version}/scripts/status.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
# trap empty values
[[ "\${LOGICAL_NAME}" == "" ]] && exit 2
export _logical_name=\${LOGICAL_NAME}
EOX
cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/status.sh"
rc=\$(pgrep -f "be-engine.*${_target_path}/${_archive_version}")
if [[ \$? -ne 0 ]] ; then
  echo "be-engine \${PU_NAME} is not running"
  exit 1
fi
echo \$rc
exit 0
EOX

echo "generated status scripts for ${_artifactId}"
chmod -R 755 "${_target_path}/${_archive_version}/scripts/"
# ---- end - remove_create_scripts_snippet.sh - source