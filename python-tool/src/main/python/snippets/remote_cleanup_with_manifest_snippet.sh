#
# ---- begin - remote_cleanup_with_manifest_snippet.sh - source
cd "${_target_path}"
CURRENTLINK=\$(readlink current)
log_info "current link version \${CURRENTLINK}"
PREVIOUSLINK=\$(readlink previous)
log_info "previous link version \${PREVIOUSLINK}"
CURRENT_VERSIONS=\$(find *  -type d -prune | xargs ls -1rtd )
if [[ "\${CANDIDATES}" != "" ]]; then
  IFS=',' read -r -a array <<< "\${CANDIDATES}"
  log_debug "Search candidates \${CANDIDATES}"
  LIST_OF_VERSIONS=""
  for CANDIDATE in \${CURRENT_VERSIONS} ; do
    if [[ "\${CANDIDATE##*/}" == "\${CURRENTLINK}" ]] ; then
      log_warn "Skipping \${CANDIDATE##*/} as it is the currently linked version"
    elif [[ "\${CANDIDATE##*/}" == "\${PREVIOUSLINK}" ]] ; then
      log_warn "Skipping \${CANDIDATE##*/} as it is the previously linked version"
    elif [[ ! -d "\${CANDIDATE}" ]] ; then
      log_warn "Skipping \${CANDIDATE} as it does not appear to be a directory?"
    else
      MATCH=""
      for candidate in \${array[@]} ; do
        if [[ "\${candidate}" == "\${CANDIDATE}" ]] ; then
          if [[ -d "\${candidate}" ]]; then
            MATCH="\${CANDIDATE}"
          fi
        fi
      done
      if [[ "\${MATCH}" ==  "" ]]; then
        if [[ "\${LIST_OF_VERSIONS}" == "" ]]; then
          LIST_OF_VERSIONS="\${CANDIDATE}"
        else
          LIST_OF_VERSIONS="\${LIST_OF_VERSIONS},\${CANDIDATE}"
        fi
      fi
    fi
  done
  log_debug "processing unwanted items \${LIST_OF_VERSIONS}"
  IFS=',' read -r -a array <<< "\${LIST_OF_VERSIONS}"
  for candidate in \${array[@]} ; do
    if [[ -d "\${candidate}" ]]; then
      log_info "Removing old install: \${candidate}"
      rm -rf \${candidate} \${candidate}.properties
    fi
  done
else
  echo "╔╗╔╔═╗  ╔═╗╔═╗╔╗╔╔╦╗╦╔╦╗╔═╗╔╦╗╔═╗╔═╗";
  echo "║║║║ ║  ║  ╠═╣║║║ ║║║ ║║╠═╣ ║ ║╣ ╚═╗";
  echo "╝╚╝╚═╝  ╚═╝╩ ╩╝╚╝═╩╝╩═╩╝╩ ╩ ╩ ╚═╝╚═╝";
fi
# ---- end - remote_cleanup_with_manifest_snippet.sh - source
