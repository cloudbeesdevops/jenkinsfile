# ---- begin - cloudenv_test_snippet.sh - source
   log_info " > cloudenv ssh ${_user}@${_host}:";
   /usr/bin/ssh -n -T ${_identity_} ${_ssh_options} "${_ssh}" "if [[ -e /opt/fedex/cloud/etc/cloudenv.yaml ]];\
    then cat /opt/fedex/cloud/etc/cloudenv.yaml;\
else echo '=====NOT A CLOUDENV=====';fi"

# ---- end - cloudenv_test_snippet.sh - source
