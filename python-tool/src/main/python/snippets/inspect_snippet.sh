# ---- begin - inspect_snippet.sh - source
   echo " > inspecting ssh ${_user}@${_host}:${_target_path}";
   /usr/bin/ssh -n -T ${_identity_} ${_ssh_options} "${_ssh}" find /var/fedex/*/${_artifactId}/logs ${_target_path} ! -type d -ls </dev/null
   echo " > inspection succeeded (ssh ${_identity_} ${_user}@${_host})";
# ---- end - inspect_snippet.sh - source
