# ---- begin - remote_script_begin.sh - source

# generate remote install
log_info " > writing remote script : ${local_base_dir}/${_script_name}"
cat <<EOT > "${local_base_dir}/${_script_name}.txt"
#!/bin/bash
# --- begin ${local_base_dir}/${_script_name}.txt
log_error() {
  set > /dev/null
  %secho "[ERROR]: \$@"
}
log_info()  {
  set > /dev/null
  %secho "[INFO] : \$@"
}
log_debug() {
  set > /dev/null
  %secho "[DEBUG]: \$@"
}
log_warn()  {
  set > /dev/null
  %secho "[WARN] : \$@"
}
timestamp() {
    while IFS= read -r line; do
        echo "$(date) - $line"
    done
}

echo "====================================="
echo "DATE: `date` / USER: `whoami` / HOST: `hostname`" - PID: $$
echo "====================================="

# ENVIRONMENT SETTINGS
%s
#
#
%s
