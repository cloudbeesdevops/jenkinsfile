# ---- begin - remote_script_end.sh - source
log_debug "Leaving remote execution, looking for ${_user}@notty :"
# find orphaned ssh session due to the remote invocation
NOTTY_PRCS=\$(ps -u ${_user} -f | grep '@notty' | grep -v grep | grep -v APPD)
#
log_debug "Echoing pids: \${NOTTY_PRCS}"
#
[[ "\${NOTTY_PRCS}" != "" ]] && log_debug "found orphaned processes ${NOTTY}"
if [[ "\${NOTTY_PRCS}" != "" ]]; then
   echo "\${NOTTY_PRCS}" | perl -ne '@x=split/\s+/;printf " %s", \$x[1];' | xargs -i% kill %
fi
# ---- end - remote_script_end.sh - source

EOT
