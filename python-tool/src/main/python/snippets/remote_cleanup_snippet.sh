# ---- begin - remote_clean_snippet.sh - source
	cd "${_target_path}"
	CURRENTLINK=\$(readlink current)
	log_info "current link version \${CURRENTLINK}"
	PREVIOUSLINK=\$(readlink previous)
	log_info "previous link version \${PREVIOUSLINK}"
	CANDIDATES=\$(find ${_target_path}/*  -type d -prune | xargs ls -1rtd |grep -v local)
	log_debug "Search candidates \${CANDIDATES}"
	for CANDIDATE in \${CANDIDATES} ; do
	  if [[ "\${CANDIDATE##*/}" == "\${CURRENTLINK}" ]] ; then
	    log_warn "Skipping \${CANDIDATE##*/} as it is the currently linked version"
		elif [[ "\${CANDIDATE##*/}" == "\${PREVIOUSLINK}" ]] ; then
		  log_warn "Skipping \${CANDIDATE##*/} as it is the previously linked version"
	  elif [[ ! -d "\${CANDIDATE}" ]] ; then
	    log_warn "Skipping \${CANDIDATE} as it does not appear to be a directory?"
	  else
	    log_info "Removing old install: \${CANDIDATE}"
	    rm -rf \${CANDIDATE} \${CANDIDATE}.properties
	  fi
	done
# ---- end - remote_clean_snippet.sh - source
