# ---- begin - remote_appmanage_install_snippet.sh - source

echo "╔═╗╔╦╗╔═╗╦═╗╔╦╗     (${_ear})";
echo "╚═╗ ║ ╠═╣╠╦╝ ║      (${artifactId})";
echo "╚═╝ ╩ ╩ ╩╩╚═ ╩      (\${DOMAIN_NAME}_DOMAIN)";

source "${_target_path}/${_archive_version}/scripts/start.sh"
# ---- end - remote_appmanage_install_snippet.sh - source
