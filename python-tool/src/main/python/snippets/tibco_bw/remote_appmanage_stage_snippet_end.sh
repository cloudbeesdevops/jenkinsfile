# ---- begin - /tibco_bw/remote_appmanage_stage_snippet_end.sh - source
else
   echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
   echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}";
   echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
   echo " !!! packaging file ${_landing_area}/${_archive_filename} not found, aborting"
   exit 1
fi
# ---- end - /tibco_bw/remote_appmanage_stage_snippet_end.sh - source
