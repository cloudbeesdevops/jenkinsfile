# ---- begin - ps_test_snippet.sh - source
   log_info " > testing ssh ${_user}@${_host}:";
   /usr/bin/ssh -n -T ${_identity_} ${_ssh_options} "${_ssh}" ps -ef | grep "PID\|${_artifactId}"
   if [ $? -ne 0 ]; then
      echo "╔═╗╔═╗  ╔═╗╔═╗┬╦  ╔═╗╔╦╗";
      echo "╠═╣╚═╗  ╠╣ ╠═╣│║  ║╣  ║║";
      echo "╚  ╚═╝  ╚  ╩ ╩┴╩═╝╚═╝═╩╝(ssh ${_identity_} ${_user}@${_host})";
   fi
# ---- end - ps_test_snippet.sh - source
