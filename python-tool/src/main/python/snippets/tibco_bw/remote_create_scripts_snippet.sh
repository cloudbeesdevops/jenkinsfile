# ---- begin - remote_appmanage_install_snippet.sh - source

# check ear
ls -l  "${_target_path}/${_archive_version}/config"
[[ ! -e "${_target_path}/${_archive_version}/config/" ]] && rm -fr "${_target_path}/${_archive_version}/config" && mkdir -p "${_target_path}/${_archive_version}/config/"

base_dir="${_target_path}/${_archive_version}"
if [[ -e "${_target_path}/${_archive_version}/bin" ]]; then
   base_dir="${_target_path}/${_archive_version}/bin"
fi
_ear_artifact="${_artifactId}"
_display_name="${_uuid}"
_appmanage_creds="${_appmanage_creds}"

[ ! -z "${DISPLAY_NAME}" ] && _display_name="${DISPLAY_NAME}"
[[ ! -e "${_target_path}/${_archive_version}/config/\${_ear_artifact}.ear" ]] && rm "${_target_path}/${_archive_version}/config/\${_ear_artifact}.ear"

_ear=\$( find "${_target_path}/${_archive_version}" -name "${_artifactId}*.ear" | grep -v config | head -n 1 )
_ear_xml=\$( find "${_target_path}/${_archive_version}" -name '*.xml' -print | grep -F "config/${_level}/${_artifactId}" | head -n 1)
_ear_xml_short=\${_ear_xml##*/}

echo "ear_xml=\$_ear_xml"
echo "ear    =\$_ear"

if [[ "\$_ear" == "" || "\$_ear_xml" == "" ]]; then
  echo "╔╗╔╔═╗  ╔═╗╦═╗╔═╗╦ ╦╦╦  ╦╔═╗  ╔═╗╔═╗╦ ╦╔╗╔╔╦╗";
  echo "║║║║ ║  ╠═╣╠╦╝║  ╠═╣║╚╗╔╝║╣   ╠╣ ║ ║║ ║║║║ ║║";
  echo "╝╚╝╚═╝  ╩ ╩╩╚═╚═╝╩ ╩╩ ╚╝ ╚═╝  ╚  ╚═╝╚═╝╝╚╝═╩╝";
  echo "'\${base_dir}/.../${_artifactId}.ear' not found"
  exit 0
else
  ls -l \${_ear}
  ls -l "${_target_path}/${_archive_version}/config/\${_ear_artifact}.ear"
  sleep 3
  cp -vf \${_ear} "${_target_path}/${_archive_version}/config/\${_ear_artifact}.ear"
  cp -vf \${_ear_xml} "${_target_path}/${_archive_version}/config/\${_ear_xml_short}"
  ls -l  "${_target_path}/${_archive_version}/config"
fi

[ ! -e "${_target_path}/${_archive_version}/scripts" ] && mkdir -p "${_target_path}/${_archive_version}/scripts"
echo "generating scripts for BW control/deployment"
   cat <<EOX > "${_target_path}/${_archive_version}/scripts/start.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
cd \${TRA_HOME}/bin
./AppManage -start -app \${_display_name}  \${_appmanage_creds} -domain \${DOMAIN_NAME}
rc=\$?
echo 'exit=\$rc'
echo
echo "LOG INFORMATION"
echo "check \${DOMAIN_DATA_DIR}/\${DOMAIN_NAME}/logs/Administrator.log"
#tail -10 \${DOMAIN_DATA_DIR}/\${DOMAIN_NAME}/logs/Administrator.log
echo "check \${DOMAIN_DATA_DIR}/\${DOMAIN_NAME}/logs/ApplicationManagement.log for output"
#tail -30 \${DOMAIN_DATA_DIR}/\${DOMAIN_NAME}/logs/ApplicationManagement.log
EOX
echo "generated start scripts for ${_artifactId}"

cat <<EOX > "${_target_path}/${_archive_version}/scripts/stop.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
cd \${TRA_HOME}/bin
./AppManage -stop -app \${_display_name} \${_appmanage_creds} -domain \${DOMAIN_NAME}
rc=\$?
echo
echo "exit=\$rc"
echo "LOG INFORMATION"
echo "check \${DOMAIN_DATA_DIR}/domaindata/tra/\${DOMAIN_NAME}/logs/Administrator.log"
#tail -10 \${DOMAIN_DATA_DIR}/domaindata/tra/\${DOMAIN_NAME}/logs/Administrator.log
echo "check \${DOMAIN_DATA_DIR}/domaindata/tra/\${DOMAIN_NAME}/logs/ApplicationManagement.log for output"
#tail -30 \${DOMAIN_DATA_DIR}/domaindata/tra/\${DOMAIN_NAME}/logs/ApplicationManagement.log
EOX
echo "generated stop scripts for ${_artifactId}"

cat <<EOX > "${_target_path}/${_archive_version}/scripts/status.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
pgrep -f ".*/application/.+/\${PROCESS_NAME}\..*"
if [[ $? -ne 0 ]] ; then
  echo "BW ${_artifactId} application is not running"
  exit 1
fi
exit 0
EOX
echo "generated status scripts for ${_artifactId}"

cat <<EOX > "${_target_path}/${_archive_version}/scripts/undeploy.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
cd \${TRA_HOME}/bin
./AppManage -batchUndeploy -domain \${DOMAIN_NAME} \${_appmanage_creds} -dir "${_target_path}/${_archive_version}/config" -serialize -nostart < /dev/null
rc=\$?
echo "exit=\$rc"
EOX
echo "generated undeploy scripts for ${_artifactId}"

cat <<EOX > "${_target_path}/${_archive_version}/scripts/deploy.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
cd \${TRA_HOME}/bin
./AppManage -batchDeploy -domain \${DOMAIN_NAME}  \${_appmanage_creds} -dir "${_target_path}/${_archive_version}/config" -serialize -breaklock -timeout 240 < /dev/null
rc=\$?
echo "exit=\$rc"
EOX
echo "generated deploy scripts for ${_artifactId}"

cat <<EOX > "${_target_path}/${_archive_version}/scripts/install.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
cd \${TRA_HOME}/bin
./AppManage -batchDeploy -domain \${DOMAIN_NAME}  \${_appmanage_creds} -dir "${_target_path}/${_archive_version}/config" -serialize -nostart -breaklock -timeout 240 < /dev/null
echo EXIT=\$?
EOX
echo "generated install scripts for ${_artifactId}"

cat <<EOX > "${_target_path}/${_archive_version}/scripts/delete.sh"
#!/bin/bash
[[ -e ../../properties ]] && . ../../properties
cd \${TRA_HOME}/bin
./AppManage -batchUndeploy -domain \${DOMAIN_NAME}  \${_appmanage_creds} -dir "${_target_path}/${_archive_version}/config" -serialize < /dev/null
rc=\$?
echo "exit=\$rc"
./AppManage -batchDelete -domain \${DOMAIN_NAME}  \${_appmanage_creds} -dir "${_target_path}/${_archive_version}/config" -breaklock -timeout 240 < /dev/null
rc=\$?
echo "exit=\$rc"
EOX
echo "generated delete scripts for ${_artifactId}"

cat <<EOX > "${_target_path}/${_archive_version}/scripts/status.sh"
exit 0
EOX

export _port='\$port'
export _remoteip='\$remoteip'
PERL_EXE=`which perl`
cat <<EOX > "${_target_path}/${_archive_version}/scripts/status_check.sh"
#!\${PERL_EXE}
use IO::Socket;
## Array of ports that you want to check.
\${_port} = "\${JMX_PORT}";
\${_remoteip} = "\${_host}";
EOX

cat <<'EOX' >> "${_target_path}/${_archive_version}/scripts/status_check.sh"
print "\nChecking for service at \$remoteip\n";
\$connected = 0;
\$checkport = IO::Socket::INET->new(
              PeerAddr => "\$remoteip",
              PeerPort => "\$port",
              Proto => 'tcp',
              Timeout => '0') or \$connected = 1;
if (!(\$connected)) {
   print "  Port \$port is up.\n";
} else {
  print "  Port \$port is down.\n";
}
close \$checkport;
EOX
echo "generated status scripts for ${_artifactId}"

cat <<LEKKER_VAKANTIE > "${_target_path}/${_archive_version}/config/AppManage.batch"
<?xml version="1.0" encoding="UTF-8"?>
<apps>
<app name="\${_display_name}" ear="\${_ear_artifact}.ear" xml="\${_ear_xml_short}"/>
</apps>
LEKKER_VAKANTIE

chmod -R 755 "${_target_path}/${_archive_version}/scripts/"
# ---- end - remote_appmanage_install_snippet.sh - source
