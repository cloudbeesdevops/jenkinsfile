# ---- begin - remote_appmanage_deploy_snippet.sh - source
echo "╔╦╗╔═╗╔═╗╦  ╔═╗╦ ╦╦╔╗╔╔═╗(\${_ear})";
echo " ║║║╣ ╠═╝║  ║ ║╚╦╝║║║║║ ╦(\${artifactId})";
echo "═╩╝╚═╝╩  ╩═╝╚═╝ ╩ ╩╝╚╝╚═╝(\${DOMAIN_NAME})";
source "${_target_path}/${_archive_version}/scripts/deploy.sh"
# ---- end - remote_appmanage_deploy_snippet.sh - source
