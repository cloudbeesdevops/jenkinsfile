# ---- begin - data_wrapper_footer.sh - source
   log_debug " > target : ${MACHINE} "
   log_info " > executing : ${_ssh} | ${_script_name}"
   #${verboseFlag}
   if [[ "${local_base_dir}" != "." ]]; then
      [ ! -e ${local_base_dir} ] && mkdir -p "${local_base_dir}"
   fi
   if [[ ! -e "${local_base_dir}/master-${_uuid}.sh" ]]; then
      cat <<EOX > "${local_base_dir}/master-${_uuid}.sh"
#!/bin/bash -s
timestamp() {
  while IFS= read -r line; do
      echo "\$(date) - \$line"
  done
}
EOX
   fi
   timescript=""
   script=""
   if [[  `which timeout` != '' ]]; then
      echo ""
      #timescript="timeout -s SIGKILL ${_timeout}s"
   fi
   cat <<EOXX >> "${local_base_dir}/master-${_uuid}.sh"
echo " __ \ /  __  __     ___  __ (${_timeout})"
echo "|_   X  |_  /   | |  |  |_ (${_ssh})"
echo "|__ / \ |__ \__ |_|  |  |__ [${local_base_dir}/${_script_name}]"
EOXX
   printf -v script '%s /usr/bin/ssh -n %s %s %s "$(<%s.txt)" 2>&1  >> "%s/stdout-%s-%s.log" &' \
   "${timescript}" "${_ssh}" "${_identity_}" "${_ssh_options}" "${local_base_dir}/${_script_name}" \
   "${local_base_dir}" "${_uuid}" "${_host}"
   echo -E "$script" >> "${local_base_dir}/master-${_uuid}.sh"
done # ---

if [[ "${_no_wait}" == "" ]]; then
	echo "wait" >> "${local_base_dir}/master-${_uuid}.sh"
fi
# ---- end - data_wrapper_footer.sh - source
