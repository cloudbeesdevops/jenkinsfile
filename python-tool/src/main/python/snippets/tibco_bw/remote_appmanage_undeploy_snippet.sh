# ---- begin - remote_appmanage_undeploy_snippet.sh - source
echo "╦ ╦╔╗╔╔╦╗╔═╗╔═╗╦  ╔═╗╦ ╦╦╔╗╔╔═╗(\${_ear})";
echo "║ ║║║║ ║║║╣ ╠═╝║  ║ ║╚╦╝║║║║║ ╦(\${artifactId})";
echo "╚═╝╝╚╝═╩╝╚═╝╩  ╩═╝╚═╝ ╩ ╩╝╚╝╚═╝\${DOMAIN_NAME}";
source "${_target_path}/${_archive_version}/scripts/undeploy.sh"
# ---- end - remote_appmanage_undeploy_snippet.sh - source
