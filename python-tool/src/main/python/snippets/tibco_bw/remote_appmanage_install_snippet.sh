# ---- begin - remote_appmanage_install_snippet.sh - source

echo "╦╔╗╔╔═╗╔╦╗╔═╗╦  ╦  (${_ear})";
echo "║║║║╚═╗ ║ ╠═╣║  ║  (${artifactId})";
echo "╩╝╚╝╚═╝ ╩ ╩ ╩╩═╝╩═╝(\${DOMAIN_NAME})";
source "${_target_path}/${_archive_version}/scripts/install.sh"
# ---- end - remote_appmanage_install_snippet.sh - source
