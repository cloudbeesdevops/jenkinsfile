# ---- begin - remote_status_snippet.sh - source
if [[ -e "${_target_path}/current/scripts" ]]; then
   cd ${_target_path}/current/scripts
   echo -n "STATUS: ${_host}:${_archive_filename}" && ./status.sh
   if [ \$? -eq 0 ]; then
     echo "╔═╗╦═╗╔═╗╔═╗╔═╗╔═╗╔═╗  ╦═╗╦ ╦╔╗╔╔╗╔╦╔╗╔╔═╗(dir=${_target_path}/current/script)";
     echo "╠═╝╠╦╝║ ║║  ║╣ ╚═╗╚═╗  ╠╦╝║ ║║║║║║║║║║║║ ╦(app=${_archive_filename}-${_archive_version}";
     echo "╩  ╩╚═╚═╝╚═╝╚═╝╚═╝╚═╝  ╩╚═╚═╝╝╚╝╝╚╝╩╝╚╝╚═╝(${_user}@${_host})";
   else
     echo "╔═╗╦═╗╔═╗╔═╗╔═╗╔═╗╔═╗  ╔╗╔╔═╗╔╦╗  ╦═╗╦ ╦╔╗╔╔╗╔╦╔╗╔╔═╗(dir=${_target_path}/current/script)";
     echo "╠═╝╠╦╝║ ║║  ║╣ ╚═╗╚═╗  ║║║║ ║ ║   ╠╦╝║ ║║║║║║║║║║║║ ╦(app=${_archive_filename})-${_archive_version}";
     echo "╩  ╩╚═╚═╝╚═╝╚═╝╚═╝╚═╝  ╝╚╝╚═╝ ╩   ╩╚═╚═╝╝╚╝╝╚╝╩╝╚╝╚═╝(${_user}@${_host})";
   fi
else
   echo "┌─┐┌┐ ┌─┐┬─┐┌┬┐┬┌┐┌┌─┐";
   echo "├─┤├┴┐│ │├┬┘ │ │││││ ┬${_artifactId}-${_archive_version}";
   echo "┴ ┴└─┘└─┘┴└─ ┴ ┴┘└┘└─┘(${_user}@${_host})";
   echo " ! aborting no current symlink available ${_target_path}/current." && ls -l .. && exit 1
fi
# ---- end - remote_status_snippet.sh - source
