import unittest
import sys
import os
import xmltodict

class TestXmltodict(unittest.TestCase):


    def test_parse_cdata(self):

        XML='''<root>
        <key> my spaced data key </key>
        <branch><![CDATA[ my spaced data field ]]></branch>
        
        </root>
'''
        struct = xmltodict.parse(XML, strip_whitespace=True)
        self.assertIsNotNone(struct)
        self.assertEquals('my spaced data key', struct['root']['key'])
        self.assertEquals(' my spaced data field ', struct['root']['branch'])
