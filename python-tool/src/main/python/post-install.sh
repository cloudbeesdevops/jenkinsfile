#!/bin/bash
export HTTPS_PROXY=http://internet.proxy.fedex.com:3128
export HTTP_PROXY=http://internet.proxy.fedex.com:3128
export PATH=${HOME}/local/bin:${HOME}/.local/bin:${PATH}
export LD_LIBRARY_PATH=${HOME}/.local/lib
export PYTHONPATH=/var/tmp/tools/usr/lib64/python2.7/site-packages/:${HOME}/.local/lib/python2.7/site-packages
if [[ -z `pip` ]]; then
  curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
  python get-pip.py --user
fi

__PATH=`pwd`

[[ ! -e /var/tmp/tools ]] && mkdir -p /var/tmp/tools
if [[ ! -d /var/tmp/tools/usr/lib64/python2.7/site-packages/Crypto ]]; then
  cd /var/tmp/tools
  curl -o python-crypto.rpm  http://ftp.tu-chemnitz.de/pub/linux/dag/redhat/el7/en/x86_64/rpmforge/RPMS/python-crypto-2.6.1-1.el7.rf.x86_64.rpm
  rpm2cpio python-crypto.rpm | cpio -idv
fi

cd ${__PATH}
pip install  -U -r ./requirements.txt --user --extra-index-url=http://c0009869.test.cloud.fedex.com:8081 --trusted c0009869.test.cloud.fedex.com
pip install jasypt4py --user --no-deps

# reinstall package
# pip install fnexus --extra-index-url=http://urh00614.ute.fedex.com:8080 --trusted urh00614.ute.fedex.com --user
