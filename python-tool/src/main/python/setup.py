from setuptools import setup, find_packages
import os

os.environ['https_proxy']='http://internet.proxy.fedex.com:3128'
os.environ['http_proxy']='http://internet.proxy.fedex.com:3128'
PACKAGES=find_packages(where='.', exclude=('dns*','glob2', 'requests_cache', 'jasypt4py', 'googleapiclient*',
    'requests*','xmltodict', 'xmlrunner', 'beaker*', 'apiclient'))
PACKAGES.append('snippets')
PACKAGES.append('snippets/pcf')
PACKAGES.append('snippets/tibco_be')
PACKAGES.append('snippets/java_sudo')
PACKAGES.append('snippets/tibco_bw')
PACKAGES.append('snippets/tomcat')
PACKAGES.append('snippets/java_auto')
PACKAGES.append('snippets/tibco_as')
PACKAGES.append('snippets/filecopy')
PACKAGES.append('snippets/tibco_asleech')

print PACKAGES

setup(name="fdeploy",
      version='7.2.10',
      description='fdeploy deployment tool',
      url='https://gitlab.prod.fedex.com/APP3534596/xopco-common-tools-fdeploy',
      author='SEFS System Team',
      author_email='akaan@fedex.com',
      license='FedEx',
      #packages=find_packages(exclude=['dns','glob2', 'requests_cache', 'jasypt4py', 'googleapiclient', 'requests',]),
      #packages=['fdeploy','ldd','cloudbees', 'fdeploy.content', 'fdeploy.platform', 'fdeploy.platform.pcf', 'fdeploy.security', 'fdeploy.menu', 'fdeploy.deployer'],
      packages=PACKAGES,
      package_dir={'': '.'},
      package_data={ '' : ['*.sh','*.tpl','*.tm']},
      #package_dir={'': 'fdeploy'},
#      scripts=['fdeploy.py', 'cloudbees.py'],
      install_requires=[
          'xmltodict', 'requests', 'dnspython', 'dnslib', 'glob2', 'google-api-python-client',
          'requests_cache', 'apiclient', 'jasypt4py', 'requests_file', 'importlib', 'fdeploy'
      ],
      zip_safe=False)
