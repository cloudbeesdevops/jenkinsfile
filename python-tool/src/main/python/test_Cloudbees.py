# -*- coding: utf-8 -*-
import copy
import imp
from io import TextIOWrapper, BytesIO
import io
import logging
import re
import sys

import cloudbees
import fdeploy  # needed for log initializatgion


class StdoutBuffer(TextIOWrapper):
    def write(self, string):
        try:
            return super(StdoutBuffer, self).write(string.decode('utf-8'))
        except TypeError as t:
            with io.open('tex.txt', 'w') as f:
                f.write("%s" % (t) )
            # redirect encoded byte strings directly to buffer
            return super(StdoutBuffer, self).buffer.write(string.decode('utf-8'))

logging.basicConfig(level=logging.DEBUG)

class TestCloudbees(object):

    VERBOSE_LEVEL=''

    _original_argv_ = []

    def testTestCloudBeesIncrementorNoStdout(self):
        fdeploy.LOGGER = fdeploy.init_logger(logging.DEBUG)
        if len(self._original_argv_) < 1:
            self._original_argv_ = copy.deepcopy(sys.argv)
        sys.argv = copy.deepcopy(self._original_argv_)
        # setup the environment
        old_stdout = sys.stdout
        with StdoutBuffer(BytesIO(), sys.stdout.encoding) as sys.stdout:
            sys.argv = ['cloudbees.py','-d','../../test/resources','-u', 'file://../../test/resources/local']
            imp.load_source('__main__', 'cloudbees.py')
            sys.stdout.seek(0)      # jump to the start
            out = sys.stdout.read() # read output
        sys.stdout = old_stdout
        sys.argv = self._original_argv_
        # do stuff with the output
        out = re.sub('\n', '', out)
        assert '1.0.6' ==  out
        
    def testTestCloudBeesIncrementor2(self):
        fdeploy.LOGGER = fdeploy.init_logger(logging.DEBUG)
        if len(self._original_argv_) < 1:
            self._original_argv_ = copy.deepcopy(sys.argv)
        sys.argv = copy.deepcopy(self._original_argv_)
        # setup the environment
        sys.argv = ['cloudbees.py','-d','../../test/resources','-u', 'file://../../test/resources/local']
        imp.load_source('__main__', 'cloudbees.py')
        sys.stdout.seek(0)      # jump to the start
        out = sys.stdout.read() # read output
        sys.argv = self._original_argv_
        # do stuff with the output
        out = re.sub('\n', '', out)
        assert '1.0.6' ==  out

    def testVerbose(self):
        fdeploy.LOGGER = fdeploy.init_logger(logging.DEBUG)
        if len(self._original_argv_) < 1:
            self._original_argv_ = copy.deepcopy(sys.argv)
        sys.argv = copy.deepcopy(self._original_argv_)
        sys.argv = ['cloudbees.py','-v','-d','../../test/resources','-u', 'file://../../test/resources/local']
        imp.load_source('__main__', 'cloudbees.py')
        sys.argv = self._original_argv_
        fdeploy.MUTE_ALL = True
        fdeploy.setLogLevel(0)


    def test_edge_case_sdom_4digit_externally_called(self):
        fdeploy.LOGGER = fdeploy.init_logger(logging.DEBUG)
        if len(self._original_argv_) < 1:
            self._original_argv_ = copy.deepcopy(sys.argv)
        sys.argv = copy.deepcopy(self._original_argv_)
        old_stdout = sys.stdout
        with StdoutBuffer(BytesIO(), sys.stdout.encoding) as sys.stdout:
            sys.argv = ['cloudbees.py','-f', '../../resources/local/d7-a.pom', '-d','../../test/resources/local','-u', 'file://../../test/resources/local']
            imp.load_source('__main__', 'cloudbees.py')
            sys.stdout.seek(0)
            out = sys.stdout.read()
        sys.stdout = old_stdout
        sys.argv = self._original_argv_
        fdeploy.MUTE_ALL = True
        fdeploy.setLogLevel(0)
        out = re.sub('\n', '', out)
        assert '2.1.13.3' ==  out
        
  
    def test_edge_case_sdom_4digit(self):
        fdeploy.LOGGER = fdeploy.init_logger(logging.DEBUG)
        version = cloudbees.main(['-f', '../../resources/local/d7-a.pom', '-d','../../test/resources/local','-u', 'file://../../test/resources/local'])
        assert '2.1.13.3' ==  version


    def testErrorOption(self):
        fdeploy.LOGGER = fdeploy.init_logger(logging.DEBUG)
        self._original_argv_ = copy.deepcopy(sys.argv)
        sys.argv = ['cloudbees.py','-z']
        imp.load_source('__main__', 'cloudbees.py')
        sys.argv = self._original_argv_
