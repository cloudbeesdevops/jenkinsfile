# -*- coding: utf-8 -*-
#import fdeploy.deployer
import imp
import sys
import copy
import pytest
import glob2
import shutil
# importing to force proxy calculation
import fdeploy
import os
from fdeploy.MockHttpServer import start_mock_server
from fdeploy.test_PAM import pamAuthRequestHandler, setup_pam_url
from ldd.test_LddPublisher import LDDServiceHandler
from fdeploy.UtilsForTesting import compare_files
def addOrReplace(base, command):
    for c in command:
        base.append(c)
    #print "base: ", str(base)

os.environ['level']='L1'
TEST_HTTP_PORT=None
LDD_HTTP_PORT=None

def doMenu(jsonfile,level='L1'):
    addOrReplace(sys.argv, [TestFdeploy.VERBOSE_LEVEL, '-t', 'menu', '-f', jsonfile])
    try:
        imp.load_source('__main__', 'fdeploy.py')
    except:
        pass
def doSsh(jsonfile,level='L1'):
    addOrReplace(sys.argv, [TestFdeploy.VERBOSE_LEVEL, '-t', 'test', '-a', 'ssh', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doEncrypt(plaintext='plaintext'):
    addOrReplace(sys.argv, [TestFdeploy.VERBOSE_LEVEL, '-t', 'security', '-a', 'encrypt', plaintext])
    imp.load_source('__main__', 'fdeploy.py')
def doDecrypt(plaintext='plaintext'):
    addOrReplace(sys.argv, [TestFdeploy.VERBOSE_LEVEL, '-t', 'security', '-a', 'decrypt',  'ENC(izvD7NG+78SMHOuV+lFybW83avg9rVf8v+JNO0VcYrM=)', 'ENC(lZuKgsn5fWBzm6Ym79StElrMsBjkXVt3m7+Ls6cSwJY=)'])
    imp.load_source('__main__', 'fdeploy.py')
def doMl(jsonfile,level='L1'):
    addOrReplace(sys.argv, [TestFdeploy.VERBOSE_LEVEL, '-t', 'test', '-a', 'ml', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doPs(jsonfile,level='L1'):
    addOrReplace(sys.argv, [TestFdeploy.VERBOSE_LEVEL, '-t', 'test', '-a', 'ps', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')

def doDeployStage(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'deploy', '-a', 'stage', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doDeployInstall(jsonfile,level='L1',no_download=True):
    arr=['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'deploy', '-a', 'install', '-f', jsonfile, '-l', level]
    if no_download == True:
        arr.insert(0,'-nd')
    addOrReplace(sys.argv, arr)
    imp.load_source('__main__', 'fdeploy.py')
def doDeployInstallIgnoreManifest(jsonfile,level='L1',no_download=True):
    arr=['-nd',  '-ime',  '-t',TestFdeploy.VERBOSE_LEVEL, 'deploy', '-a', 'install', '-f', jsonfile, '-l', level]
    if no_download == True:
        arr.insert(0,'-nd')
    addOrReplace(sys.argv, arr)
    imp.load_source('__main__', 'fdeploy.py')
def doDeployUninstall(jsonfile,level='L1',no_download=True):
    arr=['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'deploy', '-a', 'uninstall', '-f', jsonfile, '-l', level]
    if no_download == True:
        arr.insert(0,'-nd')
    addOrReplace(sys.argv, arr)
    imp.load_source('__main__', 'fdeploy.py')
def doDeployDeploy(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-nd','-t',TestFdeploy.VERBOSE_LEVEL, 'deploy', '-a', 'deploy', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doDeployClean(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-nd','-t',TestFdeploy.VERBOSE_LEVEL, 'deploy', '-a', 'clean', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doControlRestage(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'control', '-a', 'restage', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doControlStop(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'control', '-a', 'stop', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doControlStopById(jsonfile,level='L1',id='fxe-'):
    addOrReplace(sys.argv, ['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'control', '-a', 'stop', '-f', jsonfile, '-l', level, '-id',id])
    imp.load_source('__main__', 'fdeploy.py')
def doControlStatus(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'control', '-a', 'status', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doControlStatusWithKeep(jsonfile, arr, level='L1'):
    x = ['-nd', '-t',TestFdeploy.VERBOSE_LEVEL]
    x.extend(arr)
    x.extend([ 'control', '-a', 'status', '-f', jsonfile, '-l', level] )
    addOrReplace(sys.argv, x)
    imp.load_source('__main__', 'fdeploy.py')
def doControlStart(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'control', '-a', 'start', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doControlPublish(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-ns', '-nr', '-rc','../../test/resources/FXE_fdeploy/.fdeployunittest', '-t',TestFdeploy.VERBOSE_LEVEL,'control', '-a', 'publish', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doControlUnpublish(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'control', '-a', 'unpublish', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')
def doControlReport(jsonfile,level='L1'):
    addOrReplace(sys.argv, ['-nd', '-t',TestFdeploy.VERBOSE_LEVEL, 'control', '-a', 'report', '-f', jsonfile, '-l', level])
    imp.load_source('__main__', 'fdeploy.py')


class TestFdeploy(object):

    VERBOSE_LEVEL='-q'

    _original_argv_ = []

    pam_module = None

    @classmethod
    def setup_class(module):
        #os.environ['NO_PROXY']='127.0.0.1'
        os.environ['MOCK_USERNAME'] = 'FXS_app3534546'
        port = start_mock_server(pamAuthRequestHandler)
        fdeploy.pam.PAM_URL= setup_pam_url(port)
        start_mock_server(LDDServiceHandler)


    def teardown_method(self,methods):
        if os.path.exists('./tmp'):
            shutil.rmtree('./tmp')
    def setup_method(self,methods):
        if len(self._original_argv_) < 1:
            self._original_argv_ = copy.deepcopy(sys.argv)
        #print "setUp: %s (class-argv=%s)" % (self, TestFdeploy._original_argv_)
        sys.argv = copy.deepcopy(TestFdeploy._original_argv_)
        self.options = {'nexusURL': 'http://sefsmvn.ute.fedex.com:9999'}
        self.options = {'ldd_url' : 'http://localhost:%s' % (LDD_HTTP_PORT)}

    # @pytest.mark.skipif('fdeploy.platform.init_crypto() == True',reason="does not run on without pycrypto")
    # def testTestEncrypt(self):
    #     doEncrypt()
    def testSecurityEncrypt(self):
        doEncrypt('alacazam')
    def testSecurityDecrypt(self):
        doDecrypt('alacazam')
    def testTestMenu(self):
        doMenu('../../test/resources/FXE_fdeploy/fxe-be-sefsContainer.json')

#  ╔╦╗╦╔╗ ╔═╗╔═╗  ╔╗ ╔═╗
#   ║ ║╠╩╗║  ║ ║  ╠╩╗║╣
#   ╩ ╩╚═╝╚═╝╚═╝  ╚═╝╚═╝
    def testTestSshBE(self):
        doSsh('../../test/resources/FXE_fdeploy/fxe-be-sefsContainer.json')
    def testDeployStageBE(self):
        doDeployStage('../../test/resources/FXE_fdeploy/fxe-be-sefsContainer.json')
    def testDeployInstallBE(self):
        doDeployInstall('../../test/resources/FXE_fdeploy/fxe-be-sefsContainer.json')
    def testControlStopBE(self):
        doControlStop('../../test/resources/FXE_fdeploy/fxe-be-sefsContainer.json')
    def testControlStatusBE(self):
        doControlStatus('../../test/resources/FXE_fdeploy/fxe-be-sefsContainer.json')
    def testControlPublishBE(self):
        doControlPublish('../../test/resources/FXE_fdeploy/fxe-be-sefsContainer.json')
    def testControlUnpublishBE(self):
        doControlUnpublish('../../test/resources/FXE_fdeploy/fxe-be-sefsContainer.json')
    def testControlReportBE(self):
        with pytest.raises(Exception):
            doControlReport('../../test/resources/FXE_fdeploy/fxe-be-sefsContainer.json')
#  ╔╦╗╦╔╗ ╔═╗╔═╗  ╔╗ ╦ ╦
#   ║ ║╠╩╗║  ║ ║  ╠╩╗║║║
#   ╩ ╩╚═╝╚═╝╚═╝  ╚═╝╚╩╝
    def testSshBW(self):
        doSsh('../../test/resources/FXE_fdeploy/fxe-bw-sefsAutoDGFeed.json')
    def testDeployInstallBW(self):
        doDeployInstall('../../test/resources/FXE_fdeploy/fxe-bw-sefsAutoDGFeed.json')
    def testDeployInstallAnotherBW(self):
        doDeployInstall('../../test/resources/fxg-bw-test.json',no_download=False)
    def testDeployStageBW(self):
        doDeployStage('../../test/resources/FXE_fdeploy/fxe-bw-sefsAutoDGFeed.json')
    def testControlStatusBW(self):
        try:
            doControlStatus('../../test/resources/FXE_fdeploy/fxe-bw-sefsAutoDGFeed.json')
            raise Exception("should have failed no control status")
        except:
            pass
    def testControlStopBW(self):
        doControlStop('../../test/resources/FXE_fdeploy/fxe-bw-sefsAutoDGFeed.json')
    def testControlUnPublishBW(self):
        doControlUnpublish('../../test/resources/bw5/FXE_SULEGACY.json')
        # tests local download as well
    def testControlPublishBW(self):
        doControlPublish('../../test/resources/bw5/FXE_SULEGACY.json')
#  ╔╦╗╦╔╗ ╔═╗╔═╗  ╔╗ ╦ ╦  ┌┬┐┬ ┬┌─┐
#   ║ ║╠╩╗║  ║ ║  ╠╩╗║║║   │ ││││ │
#   ╩ ╩╚═╝╚═╝╚═╝  ╚═╝╚╩╝   ┴ └┴┘└─┘
    def testTestSshBW2(self):
        doSsh('../../test/resources/FXE_fdeploy/fxe-bw2-ClearanceFeed.json')
    def testDeployInstallBW2(self):
        doDeployInstall('../../test/resources/FXE_fdeploy/fxe-bw2-ClearanceFeed.json')
    def testDeployUninstallBW2(self):
        doDeployUninstall('../../test/resources/FXE_fdeploy/fxe-bw2-ClearanceFeed.json')
    def testDeployDeployBW2(self):
        doDeployDeploy('../../test/resources/FXE_fdeploy/fxe-bw2-ClearanceFeed.json')
    def testDeployStageBW2(self):
        doDeployStage('../../test/resources/FXE_fdeploy/fxe-bw2-ClearanceFeed.json')
    def testControlStatusBW2(self):
        try:
            doControlStatus('../../test/resources/FXE_fdeploy/fxe-bw2-ClearanceFeed.json')
            raise Exception("should have failed no control status")
        except:
            pass
    def testControlStopBW2(self):
        doControlStop('../../test/resources/FXE_fdeploy/fxe-bw2-ClearanceFeed.json')
    def testControlStartBW2(self):
        doControlStart('../../test/resources/FXE_fdeploy/fxe-bw2-ClearanceFeed.json')
#  ╔╦╗╦╔╗ ╔═╗╔═╗  ╔═╗╔═╗
#   ║ ║╠╩╗║  ║ ║  ╠═╣╚═╗
#   ╩ ╩╚═╝╚═╝╚═╝  ╩ ╩╚═╝
    def testTestSshAS(self):
        doSsh('../../test/resources/FXE_fdeploy/fxg-sds-as-engine.json')
    def testDeployStageAS(self):
        doDeployStage('../../test/resources/FXE_fdeploy/fxg-sds-as-engine.json')
    def testDeployDeployAS(self):
        doDeployDeploy('../../test/resources/FXE_fdeploy/fxg-sds-as-engine.json')
    def testControlStatusAS(self):
        doControlStatus('../../test/resources/FXE_fdeploy/fxg-sds-as-engine.json')
    def testControlStopAS(self):
        doControlStop('../../test/resources/FXE_fdeploy/fxg-sds-as-engine.json')

#  ╔╦╗╦╔╗ ╔═╗╔═╗  ╔═╗╔═╗╦  ╔═╗╔═╗╔═╗╦ ╦
#   ║ ║╠╩╗║  ║ ║  ╠═╣╚═╗║  ║╣ ║╣ ║  ╠═╣
#   ╩ ╩╚═╝╚═╝╚═╝  ╩ ╩╚═╝╩═╝╚═╝╚═╝╚═╝╩ ╩

    def testTestSshASLeech(self):
        doSsh('../../test/resources/FXE_fdeploy/fxg-sds-as-leech.json')
    def testDeployStageASLeech(self):
        doDeployStage('../../test/resources/FXE_fdeploy/fxg-sds-as-leech.json')
    def testControlStatusASLeech(self):
        doControlStatus('../../test/resources/FXE_fdeploy/fxg-sds-as-leech.json')
    def testControlStopASLeech(self):
        doControlStop('../../test/resources/FXE_fdeploy/fxg-sds-as-leech.json')
#   ╦╔═╗╦  ╦╔═╗
#   ║╠═╣╚╗╔╝╠═╣
#  ╚╝╩ ╩ ╚╝ ╩ ╩
    def testTestMlJava(self):
        doMl('../../test/resources/FXE_fdeploy/fxe-plefs-cache-plugin.json')
    def testTestPSJava(self):
        doPs('../../test/resources/FXE_fdeploy/fxe-plefs-cache-plugin.json')
    def testTestSshJava(self):
        doSsh('../../test/resources/FXE_fdeploy/fxe-plefs-cache-plugin.json')
    def testDeployStageJava(self):
        doDeployStage('../../test/resources/FXE_fdeploy/fxe-plefs-cache-plugin.json')
    def testDeployInstallJava(self):
        doDeployInstall('../../test/resources/FXE_fdeploy/fxe-plefs-cache-plugin.json')
    def testControlStopJava(self):
        doControlStop('../../test/resources/FXE_fdeploy/fxe-plefs-cache-plugin.json')
    def testControlStopJavaById(self):
        doControlStopById('../../test/resources/FXE_fdeploy/fxe-plefs-cache-plugin.json')
    def testControlStatusJava(self):
        doControlStatus('../../test/resources/FXE_fdeploy/fxe-plefs-cache-plugin.json')
    def testDeployCleanJava(self):
        doDeployClean('../../test/resources/FXE_fdeploy/fxe-plefs-cache-plugin.json')
#   ╦╔═╗╦  ╦╔═╗    ╔═╗╦ ╦╔╦╗╔═╗
#   ║╠═╣╚╗╔╝╠═╣    ╠═╣║ ║ ║ ║ ║
#  ╚╝╩ ╩ ╚╝ ╩ ╩────╩ ╩╚═╝ ╩ ╚═╝
    def testTestSshJavaAuto(self):
        doSsh('../../test/resources/FXE_fdeploy/scm-java-auto.json')
    def testDeployStageJavaAuto(self):
        doDeployStage('../../test/resources/FXE_fdeploy/scm-java-auto.json')
    def testDeployInstallJavaAuto(self):
        doDeployInstall('../../test/resources/FXE_fdeploy/scm-java-auto.json')
    def testControlStopJavaAuto(self):
        doControlStop('../../test/resources/FXE_fdeploy/scm-java-auto.json')
    def testControlStatusJavaAuto(self):
        doControlStatus('../../test/resources/FXE_fdeploy/scm-java-auto.json')
#  ╔═╗╔═╗╔═╗  ╔═╗╦ ╦╔═╗╔═╗
#  ╠═╝║  ╠╣   ║  ║ ║╠═╝╚═╗
#  ╩  ╚═╝╚    ╚═╝╚═╝╩  ╚═╝
    #@skipping_when_running_outside_fedex_network
    def testDeployStagePCFCups(self):
        doDeployStage('../../test/resources/pcf-config/trip.json')
    #@skipping_when_running_outside_fedex_network
    def testDeployInstallPCFCups(self):
        doDeployInstall('../../test/resources/pcf-config/trip.json')
    #@skipping_when_running_outside_fedex_network
    def testDeployInstallPCFCupsServicesOnly(self):
        doDeployInstall('../../test/resources/pcf-config/pcf_service_only.json')
    def testDeployInstallPCFWithCupsServicesOnly(self):
        doDeployInstall('../../test/resources/pcf-config/cpp-inquiry.json', level='L3-CL')
    def testDeployInstallPCFWithCupsServicesOnly(self):
        doDeployInstall('../../test/resources/pcf-config/cpp-inquiry.json', level='development')

#  ╔═╗╔═╗╔═╗
#  ╠═╝║  ╠╣
#  ╩  ╚═╝╚
    #@skipping_when_running_outside_fedex_network
    def testTestSshPCF(self):
        doSsh('../../test/resources/FXE_fdeploy/scm-pmi-boot-pcf.json')
    #@skipping_when_running_outside_fedex_network
    def testControlRestagePCF(self):
        doControlRestage('../../test/resources/FXE_fdeploy/scm-pmi-boot-pcf.json')
    #@skipping_when_running_outside_fedex_network
    def testDeployStagePCF(self):
        doDeployStage('../../test/resources/FXE_fdeploy/scm-pmi-boot-pcf.json')
    #@skipping_when_running_outside_fedex_network
    def testDeployInstallPCF(self):
        doDeployInstall('../../test/resources/FXE_fdeploy/scm-pmi-boot-pcf.json')
    #@skipping_when_running_outside_fedex_network
    def testDeployInstallPCFWithInfrastructure(self):
        doDeployInstall('../../test/resources/pcf/pcf-infra-depends.json')
    #@skipping_when_running_outside_fedex_network
    def testDeployInstallPCFWithInfrastructureAndScriptValidation(self):
        doDeployDeploy('../../test/resources/pcf/performance-entity-identifier-service.json')
        reports = glob2.glob('./tmp/performance-entity-identifier-service*.sh')
        assert len(reports) == 1
        compare_files(reports[0], '../../test/resources/pcf/uuid-perf-script.sh')
    def testDeployInstallPCFIgnoringConfigServerNotExisting(self):
        doDeployInstallIgnoreManifest('../../test/resources/pcf/pcf-ignore-manifest*.json')
    #@skipping_when_running_outside_fedex_network
    def testDeployInstallPCFWithInfrastructure2(self):
        doDeployInstall('../../test/resources/pcf-config/itinerary-discovery-cups2.json')
    #@skipping_when_running_outside_fedex_network
    def testControlStopPCF(self):
        doControlStop('../../test/resources/FXE_fdeploy/scm-pmi-boot-pcf.json')
    #@skipping_when_running_outside_fedex_network
    def testControlStatusPCF(self):
        doControlStatus('../../test/resources/FXE_fdeploy/scm-pmi-boot-pcf.json')
    def testControlStatusPCFwithKeep(self):
        doControlStatusWithKeep('../../test/resources/FXE_fdeploy/scm-pmi-boot-pcf.json',['-k'])
#  ╔╦╗╔═╗╔╦╗╔═╗╔═╗╔╦╗
#   ║ ║ ║║║║║  ╠═╣ ║
#   ╩ ╚═╝╩ ╩╚═╝╩ ╩ ╩
    def testTestSshTomcat(self):
        doSsh('../../test/resources/FXE_fdeploy/tomcat.json')
    def testDeployStageTomcat(self):
        doDeployStage('../../test/resources/FXE_fdeploy/tomcat.json')
    def testDeployInstallTomcat(self):
        doDeployInstall('../../test/resources/FXE_fdeploy/tomcat.json')
    def testControlStopTomcat(self):
        doControlStop('../../test/resources/FXE_fdeploy/tomcat.json')
    def testControlStatusTomcat(self):
        doControlStatus('../../test/resources/FXE_fdeploy/tomcat.json')
#  ╔═╗┬╦  ╔═╗╔═╗╔═╗╔═╗╦ ╦
#  ╠╣ │║  ║╣ ║  ║ ║╠═╝╚╦╝
#  ╚  ┴╩═╝╚═╝╚═╝╚═╝╩   ╩
    def testTestSshFileCopy(self):
        doSsh('../../test/resources/FXE_fdeploy/fxe-filecopy.json')
    def testDeployStageFileCopy(self):
        doDeployStage('../../test/resources/FXE_fdeploy/fxe-filecopy.json')
    def testDeployInstallFileCopy(self):
        doDeployInstall('../../test/resources/FXE_fdeploy/fxe-filecopy.json')
    def testControlStopFilecopy(self):
        try:
            doControlStop('../../test/resources/FXE_fdeploy/fxe-filecopy.json')
            raise Exception("should have failed no control status")
        except:
            pass
    def testControlStatusFileCopy(self):
        try:
            doControlStatus('../../test/resources/FXE_fdeploy/fxe-filecopy.json')
            raise Exception("should have failed no control status")
        except:
            pass

# if __name__ == '__main__':
#     unittest.main()
