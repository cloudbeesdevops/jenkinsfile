#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil
import sys

# used for binding any RPM package that are in the tools space
sys.path.append('/var/tmp/tools/usr/lib64/python2.7/site-packages')
sys.path.append('/var/tmp/tools/usr/lib64/python2.6/site-packages')


import time
import time
import fdeploy
from fdeploy.menu.creator import creator
import argparse
import curses
import requests
import copy
from fdeploy import security_realm
from fdeploy.security import inline_singleton, deconstruct
from fdeploy.fdeployLoader import fdeployLoader

# set default tracelevel to none
sys.tracebacklimit = 3
reload(sys)
sys.setdefaultencoding('utf8')
# set warnings SSL
try:
    requests.packages.urllib3.disable_warnings()
    fdeploy.LOGGER.trace("urllib3 disable_warnings() invoked for python 2.6")
except AttributeError as ar:
    pass

# fdeploy revision
fdeploy.version = "fdeploy-${project.version} ${buildNumber.timestamp}"
#os.environ['HTTPS_PROXY'] = "http://internet.proxy.fedex.com:3128"
#os.environ['HTTP_PROXY'] = "http://internet.proxy.fedex.com:3128"
# GA User Agent
r_parser = None
d_parser = None
c_parser = None
t_parser = None
s_parser = None
parser = None

def validate_commands(cli_options):
    if 'action' in cli_options:
        if cli_options.command == 'security':
            if len(cli_options.value) == 0 and s_parser is not None:
                s_parser.print_help()
            else:
                sys.stdout.write("use -h or --help for command syntax.")
        elif cli_options.action is None or cli_options.file is None or cli_options.level is None:
            if cli_options.command == 'deploy':
                d_parser.print_help()
            elif cli_options.command == 'control':
                c_parser.print_help()
            elif cli_options.command == 'test':
                t_parser.print_help()
            elif cli_options.command == 'release':
                r_parser.print_help()
            elif cli_options.command == 'security':
                s_parser.print_help()
            else:
                parser.print_help()
            if cli_options.action is None:
                raise Exception("ERROR: Missing -a [action]")
            elif cli_options.file is None:
                raise Exception("ERROR: Missing -f [file]")
            elif cli_options.level is None:
                raise Exception("ERROR: Missing -l [L1|L2|L3|L4|L5|PROD]")
    else:
        if cli_options.command == 'menu' and cli_options.file is None:
            raise Exception("ERROR: Missing -f [jsonfiles]")

def main(__argv):
    # Building the command line options for ucontrol
    parser = argparse.ArgumentParser(fromfile_prefix_chars='@')
    parser.add_argument('--version',action='version', version=str(fdeploy.version))
    group_housekeeping = parser.add_argument_group('runtime execution')
    group_housekeeping.add_argument('--defaults', default={}, action='store',
                               help='Loading defaults for the fdeploy framework.')
    group_housekeeping.add_argument('-c', '--cache', default=False, action='store_true',
                               help='Use previously downloaded artifacts, if present.')
    group_housekeeping.add_argument('-u', '--upload', default=False, action='store_true',
                               help='Upload statistics.')
    group_housekeeping.add_argument('-ime', '--ignore-manifest-errors', default=False, action='store_true',
                               help='Ignore manifest errors especially for PCF with shared services on same space.')
    group_housekeeping.add_argument('-k','--keep', default=True, action='store_true',
                               help='Keep history of previously run sessions, if present.')
    group_housekeeping.add_argument('-nd','--no-download', default=False, action='store_true',
                               help='No download, if needed.')
    group_housekeeping.add_argument('--parallel', default=False, action='store_true',
                               help='Execute threads in parallel if possible.')
    group_housekeeping.add_argument('-nw','--no-wait', default=False, action='store_true',
                               help='Omit waiting for processes to finish.')
    group_housekeeping.add_argument('-t','--dry-run', default=False, action='store_true', help='test in a dry-run and generate all the scripting but do not execute.')
    group_housekeeping.add_argument('-ns','--no-stats', default=False, action='store_true', help='do not send statistics for LDD.')
    group_housekeeping.add_argument('-nr','--no-reporting', default=False, action='store_true', help='do not send output reporting for LDD.')
    group_housekeeping.add_argument('-i', '--identity', action='store',default="", help='using an alternate identity for fdeploy to interact with target systems')
    group_housekeeping.add_argument('-rc', action='store',default="", help='using an alternate .fdeployrc file')
    group_housekeeping.add_argument('--analytics', action='store_true',default=False, help='using segment analytics for reporting.')

    group_logging = parser.add_argument_group('logging')
    log_level_grp = group_logging.add_mutually_exclusive_group()
    log_level_grp.add_argument('--log-level', choices=('WARN','ERROR','INFO','DEBUG','TRACE'), action='store',default="ERROR")
    log_level_grp.add_argument('-v', action='count', help="verbosity level. (v=INFO, vv=DEBUG,vvv=TRACE, default=ERROR) see also --log-level for explicit level set.")
    group_logging.add_argument('-o', '--output', action='store',
                               help='Alternate output filename (optional)')
    #parser.add_argument('-r', '--regex', action='store')
    group_logging.add_argument('-q', '--quiet', default=False, action='store_true', help='suppressing reporting and other informational messages')
    group_logging.add_argument('-X',default=False, action='store_true',
                               help='Enabling debugging in the fdeploy platform engines.')



    parent_file_parser = argparse.ArgumentParser(add_help=False)
    parent_file_parser.add_argument('-f', '--file',  required=True, action="store", nargs="+", help='Deployment descriptor file in JSON format (required)')
    parent_file_parser.add_argument('-g', '--group', action='store',
                               help='Select specific target group if defined in the deployment descriptor.')
    parent_file_parser.add_argument('-r', '--regex', action='append' , # nargs="+",
                               help='Select target name regex.')
    parent_file_parser.add_argument('-id', action='store' , # nargs="+",
                               help='Select filtering by means of id value.')
    parent_file_parser.add_argument('-u', '--user', action='store',
                               help='Overwrite the specified user for the targets, useful for debugging with own accounts without altering the JSON.')
    parent_file_parser.add_argument('-p', '--path', action='store',
                               help='Overwrite the specified path for the targets, useful for debugging with own accounts without altering the JSON.')
    parent_file_parser.add_argument('--rtv', action='append',
                               help='Overwrite the rtv values without altering the JSON.')


    # Building the command line interface.
    subparsers = parser.add_subparsers(help='Commands',dest='command')
    c_parser = subparsers.add_parser('control', parents=[parent_file_parser], help='runtime control commands')
    group_c = c_parser.add_argument_group('Control arguments for fdeploy')
    group_c.add_argument('-a','--action', required=True, choices=('start','stop','status','restart', 'restage', 'run','publish','unpublish','report'), action='store', default=None, help='control actions.')
    group_c.add_argument('-l','--level', required=True, action=fdeploy.LevelAction, help='environment level.')
    group_c.add_argument('-s','--show', choices=('running','not-running','down','up'), action='store', help='show certain contents.')
    group_c.add_argument('-ag', '--app-group', action='store',
                               help='OPCO abbreviation or Internal Application Abbreviation to find the deployment descriptors.')
    group_c.add_argument('-gs', '--group-size', type=int, default=-1, action='store', help='The groupsize for grouped actions.')
    group_c.add_argument('-gn', '--group-number', type=int, default=-1, action='store', help='The group number to invoke the control action for grouped actions when -gs is defined.')


    d_parser = subparsers.add_parser('deploy', parents=[parent_file_parser],  help='deployment commands')
    d_parser.set_defaults(type='deploy')
    group_d = d_parser.add_argument_group('Deployment arguments for fdeploy')
    group_d.add_argument('-a','--action', choices=('deploy','install','stage','relink','uninstall','clean','wipeout','properties','bluegreen'), action='store', default=None, help='deployment actions.')
    group_d.add_argument('-l','--level', required=True, action=fdeploy.LevelAction, help='environment level.')
    group_d.add_argument('--properties-only', default=False, action='store_true', help='Only regenerating the properties as part of the deployment.')
    group_d.add_argument('-nu','--no-unpublishing', default=False, action='store_true', help='Disable unpublishing as part of deploy or install commands.')
    #

    m_parser = subparsers.add_parser('menu', parents=[parent_file_parser], help='menu commands')
    m_parser.set_defaults(type='menu')
    group_m = m_parser.add_argument_group('Menu UI for fdeploy')
    group_m.add_argument('-nu','--no-unpublishing', default=False, action='store_true', help='Disable unpublishing as part of deploy or install commands.')
    group_m.add_argument('-gs', '--group-size', type=int, default=-1, action='store', help='The groupsize for grouped actions.')
    group_m.add_argument('-gn', '--group-number', type=int, default=-1, action='store', help='The group number to invoke the control action for grouped actions when -gs is defined.')

    t_parser = subparsers.add_parser('test', parents=[parent_file_parser], help='test commands')
    t_parser.set_defaults(type='test')
    group_t = t_parser.add_argument_group('Test arguments for fdeploy')
    group_t.add_argument('-a','--action', choices=('ssh','inspect','ml','machine_list','ps','cloudenv'), action='store', default=None, help='test actions.')
    group_t.add_argument('-l','--level', required=True, action=fdeploy.LevelAction, help='environment level.')

    s_parser = subparsers.add_parser('security', parents=[], help='security commands')
    s_parser.set_defaults(type='security')
    group_s = s_parser.add_argument_group('Security arguments for fdeploy')
    group_s.add_argument('-a','--action', choices=('encrypt','decrypt'), action='store', default=None, help='encrypt/decrypt actions.')
    group_s.add_argument('-k','--key', action='store', default=None, help='alternate key to encrypt and decrypt.')
    group_s.add_argument('value', nargs='*', default=[], help='security actions.')

    # parse options and arguments and save groups for menu operations
    cli_options = parser.parse_args(__argv)
    command_groups = [('test', group_t), ('deploy', group_d), ('control', group_c),('security', group_s)]
    if '-h' in __argv or '--help' in __argv:
        parser.print_help()
        sys.exit(0)
    fdeploy.logLevel= fdeploy.setLogLevel(cli_options.v)
    fdeploy.LOGGER.debug(str(cli_options))

    # loading environment defaults from .fdeployrc file if it exists.
    if cli_options.rc and not os.path.isfile(cli_options.rc):
        raise Exception("ERROR: the rc file '%s' does not exist." % (cli_options.rc))
    elif cli_options.rc and os.path.isfile(os.path.normpath(os.path.expanduser(cli_options.rc))):
        fdeploy.load_rc(os.path.normpath(os.path.expanduser(cli_options.rc)), cli_options)
    elif (cli_options.rc is None or cli_options.rc == '') and os.path.exists(os.path.normpath(os.path.expanduser('~/.fdeployrc')))==True:
        # do not autodiscover rc files... test
        pass
    if 'USER' in os.environ:
        fdeploy.cleanup(os.environ['USER'])

    # validating all mandatory options
    if cli_options.identity is not None and cli_options.identity != '' and not os.path.isfile(cli_options.identity):
        raise Exception("ERROR: Identity file ''%s' does not exist." % (cli_options.identity))

    validate_commands(cli_options)

    fdeploy.useCache=cli_options.cache
    fdeploy.suppressReporting=cli_options.quiet
    fdeploy.platform.verbose=cli_options.X
    # export variables to the environment

    if cli_options.command == 'security':
        for v in cli_options.value:
            value = inline_singleton().encrypt(v) if cli_options.action == 'encrypt' else inline_singleton().decrypt(v)
            print ("\t [%s] %s -> %s" % (cli_options.action, v, value))
        return

    loader = fdeployLoader(cli_options)
    loader.readDescriptors(cli_options.file)

    if cli_options.command == 'menu':
        pipeStdout = fdeploy.menu.StdOutWrapper()
        try:
            sys.stdout = pipeStdout
            curses.initscr()
            instance = creator(curses.initscr(),loader, command_groups)
        except:
            sys.stdout = sys.__stdout__
            sys.stdout.write(pipeStdout.get_text())
            raise
        sys.stdout = sys.__stdout__
        sys.stdout.write(pipeStdout.get_text())
        fdeploy.LOGGER.debug("menu selections: " + str(instance.selections))
        cli_options.type = instance.selections['command']
        cli_options.level = instance.selections['level']
        cli_options.command = instance.selections['command']
        cli_options.action = instance.selections['action'].rstrip()
        loader.action = instance.selections['action'].strip()
        loader.set_components(instance.selections['components'])
        loader.set_targets(instance.selections['targets'])
        sys.stdout.write("./fdeploy.py %s -a %s -l %s -f %s" % (cli_options.command, cli_options.action, cli_options.level, instance.selections['components']))
        fdeploy.dump(cli_options.__dict__)

    os.environ["level"]=cli_options.level
    os.environ["systemlevel"]='prod' if cli_options.level == 'PROD' else 'test'

    # print all the provided options
    for a in cli_options.__dict__:
        value = cli_options.__dict__[a]
        if (a == 'file' or a == 'regex') and not value is None:
            value = ",".join(value)
        fdeploy.LOGGER.debug(" %10s = %s " % ( a,value))

    security_realm(cli_options)

    fdeploy.LOGGER.trace("advancing to process compoents")
    if cli_options.level:
        if cli_options.action in ['ml','machine_list']:
            loader.list_machines(cli_options.level)
        else:
            if os.path.isdir('tmp'):
                if cli_options.keep is False or cli_options.level in ['L0', 'L1', 'L2', 'L3', 'L4', 'L5']:
                    shutil.rmtree('tmp', True)
                    shutil.rmtree('history', True)
                else:
                    timestamp=time.strftime("%Y%m%d%H%M%S", time.gmtime())
                    shutil.move('tmp','history/%s' % (timestamp))
                fdeploy.LOGGER.info("from fdeploy.py generating '%s' level %s" % (cli_options.action, cli_options.level))
                files = loader.generate_level(cli_options.level)
                fdeploy.LOGGER.debug("generated %s files" % (files))
                if cli_options.dry_run is False:
                    options = copy.deepcopy(cli_options)
                    if cli_options.action in ['publish','unpublish','report']:
                        fdeploy.LOGGER.info("publishing components")
                        loader.publishing(cli_options, cli_options.level)
                    else:
                        if cli_options.action in ['deploy', 'install'] and cli_options.no_unpublishing == False:
                            options.action='unpublish'
                            loader.publishing(options, cli_options.level)
                        try:
                            loader.deploying(cli_options.level, files)
                            if cli_options.action in ['deploy', 'install'] and cli_options.no_unpublishing == False:
                                options.action='publish'
                                loader.publishing(options, cli_options.level)
                        except Exception as err:
                            fdeploy.LOGGER.exception(err)
                    if 'USER' in os.environ:
                        fdeploy.cleanup(os.environ['USER'])
    else:
       raise Exception('Must specify -l for generation of component levels')

if __name__ == "__main__":
    __args = sys.argv
    # strip of the test arguments and take
    if sys.argv[0]=='python -m unittest':
        __args = sys.argv[2:] if 'TestFdeploy.TestFdeploy' in sys.argv[1] else sys.argv[6:]
        __args = sys.argv[4:] if 'TestFdeploy.py' in sys.argv[3] else __args
    else:
        __args = sys.argv[1:]
    main(__args)
else:
    main(sys.argv[2:])
