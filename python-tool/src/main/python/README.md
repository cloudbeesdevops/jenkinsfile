

# Crypto

```
738  tar xfvp ~/Downloads/pycrypto-2.6.1.tar.gz
740  cd pycrypto-2.6.1/
755  python setup.py install
```
https://github.com/fareliner/jasypt4py

# Rsync

```
akaanfxg:python ak751818$ rsync -varz --exclude=tmp --exclude=__pycache__ --exclude=**/*.pyc .* uwb00078.ute.fedex.com:./fdeploy
```

# Remote development
```
export PYTHONPATH=.:/opt/fedex/jenkins/.local/lib/python2.6/site-packages
export PATH=/opt/fedex/jenkins/.local/bin:${PATH}
rsync -varz --exclude=.git --exclude=tmp --exclude=**/__pycache__ --exclude=**/*.pyc --exclude=target . uwb00078.ute.fedex.com:./devl
```

# Debugging

```
 ./clean.sh  ; pytest --cov=. --log-cli-level=30
pytest --log-cli-level DEBUG ldd/test_tibcoBWextractor.py --cov=. --cov-report html:cov_html
```

# Code Coverage:

Since we distribute many dependencies out of the box one needs to make sure that when you change `src/main/python/.coveragerc` to omit modules you should put that in the pom file. The oneliner below generates the excludes...


```
cat .coveragerc | perl -pe 's/\s+(.+)$/<exclude>$1<\/exclude>/g'
```
# Cleanup

```
autoflake -i --remove-unused-variables  -r fdeploy ldd cloudbees
```

# setuptools

```
python setup.py develop
```

### publish to pypi

```
python setup.py sdist bdist_wheel
twine upload --repository urh00614 dist/*
```
