# -*- coding: utf-8 -*-
import pytest
import re

test_eit_struct = {
 'project1' : [
  'tcp://mitsg.ute.fedex.com:52034',
  'tcp://mitsg.ute.fedex.com:52034'
 ]
}

class TestAppMd(object):

    def test_deconstruct(self):
        assert 'project1' ==  test_eit_struct.keys()[0]
        for i in test_eit_struct.keys():
            for j in test_eit_struct[i]:
                tcp = re.sub(r'tcp://','',j).split(':')
                print "%s,%s,%s" % ( i, tcp[0], tcp[1])
