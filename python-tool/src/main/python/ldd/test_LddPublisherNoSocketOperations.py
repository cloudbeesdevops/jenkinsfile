import pytest
import os
from ldd.lddPublisher import lddPublisher, process_pcf_publishing, normalize_level
from fdeploy.UtilsForTesting import get_compnent_from_json, get_options


class TestLddPublisherNoSockker(object):

    options = get_options()
    extractor = None
    docs = None
    resolver = None

    def setup_method(self, method):
        cdw = os.path.abspath("%s/../../test/resources" % (os.getcwd()))
        self.options.defaults['nexusURL'] = 'file://%s/local' % (cdw)

        #LOGGER.debug(">>> init >>>"
        bindings_file = [
            '../../test/resources/bw5/sefs_suAutoDGFeed_template.xml']


    def test_process_pcf_publish(self):
        (artifact, comp, fdl) = get_compnent_from_json('L3','../../test/resources/pcf/handling-unit-matching-service.json')
        req = process_pcf_publishing('L3', artifact,'PCF_JAVA', 'releases@host.fedex.com','publish','FXE','FXE_SULEGACY',0, False)
        assert not req is None
        req = process_pcf_publishing('L3', artifact,'PCF_JAVA', 'releases@api.sys.fedex.com','publish','FXE','FXE_SULEGACY',0, False)
        assert not req is None

    def test_get_compnent_from_json(self):
        x = get_compnent_from_json('L3', '../../test/resources/bw5/FXE_SULEGACY.json')
        assert 3 == len(x)
        comp = x[1]
        assert 'config.FXE/FXE_SULEGACY.json' == comp.source

    def test_normalize_level(self):
        assert 'L3' == normalize_level('l3')
        assert 'L4' == normalize_level('l4b')
        assert 'PRD' == normalize_level('PRD')
        assert 'PROD' == normalize_level('PROD')
        with pytest.raises(Exception):
            normalize_level('DRAP')

    def test_ldd_pcf_query_creation(self):
        level = 'L3'
        (artifact,comp,fdl) = get_compnent_from_json(level, '../../test/resources/pcf/handling-unit-matching-service.json')
        lddService = lddPublisher(level, self.options,
                                       'target', self.resolver, useDNS=False)

        assert 'pcf' ==  comp.lddtype
        queue = []
        queue = lddService.queue_query_request('publish', level, comp, artifact, comp.levels[0]['targets'])
        assert 1 ==  len(queue)
        assert 'PCF_JAVA' ==  queue[0]['type']
        assert 'L3-handling-unit-matching-service.app.wtcdev2.paas.fedex.com' ==  queue[0]['host']
        assert 'L3-handling-unit-matching-service' ==  queue[0]['queryCmd']
        assert 'L3-handling-unit-matching-service.app.wtcdev2.paas.fedex.com' ==  queue[0]['queryHost']
if __name__ == '__main__':
    unittest.main()
