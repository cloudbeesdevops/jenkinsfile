from ldd.tibcoBWextractor import get_instance_names, read_bw_bindings
from ldd import dns_cname, LOGGER
import fdeploy
import re

def create_publish_request(lddtype, host, artifact, operation,app_group, query, level,index, useDNS):
    _type = re.sub( r'TIBCO_', '',lddtype.upper())
    cname,fqname = (host,host) if useDNS == False else dns_cname(host)
    cname = cname.lower()
    fqname = fqname.lower()
    deployId = "%s_%s" % (artifact.artifactId, index)
    deploy_id_act= "%s@%s" % (deployId,operation)

    #if str(deploy_id_act) not in self.queue_ref:
    return deploy_id_act, {'deployId': str(deployId), 'artifactId' : str(artifact.artifactId),
        'version' : str(artifact.version), \
        'level' : str(level), 'opco' : str(app_group), 'queryHost' : str(cname), \
        'host' : str(fqname), 'queryCmd' : str(query), 'type' : str(_type)}
    #return None

def parse_hostname(host_name):
    COM=re.compile('(\S+)\@([a-z0-9-\._]+)')
    return COM.match(host_name)

class abstract_builder(object):


    def __init__(self, comp, artifact, level, useDNS, app_group='FXE',operation='publish'):
        self.comp = comp
        self.artifact = artifact
        self.level = level
        self.operation = operation
        self.app_group = fdeploy.get_app_group(comp)
        self.query_ref = []
        self.useDNS = useDNS
        if self.app_group is None:
            self.app_group = app_group

    def process_targets(self, targets):
        requests = []
        index = 1
        for host_name in targets:
            LOGGER.debug("%s -> %s",self.comp, host_name)
            req = self.process(host_name, index)
            if req:
                index += 1
                requests.append(req)
        return requests

class pcf_java_builder(abstract_builder):

    def process(self, host_name, index):
        qname = self.artifact.artifactId
        h = parse_hostname(host_name)
        if h is not None and h.group(2):
            deploy_action_id, req = create_publish_request(self.comp.lddtype,h.group(2),self.artifact,self.operation,self.app_group,qname,self.level,index, self.useDNS)
            if req:
                req['type']='PCF_JAVA'
                appid = req['queryCmd']
                domain = req['host']
                try:
                    req['host'].index('api.sys')
                    domain = req['host'].split('api.sys')[1]
                except:
                    pass
                req['host']= "%s-%s.app%s" % (self.level, appid, domain)
                req['queryCmd']="%s-%s" % (self.level, appid)
                req['queryHost']=req['host']
                return req
        return None


class java_builder(abstract_builder):

    def process(self, host_name, index):
        qname = self.artifact.artifactId
        h = parse_hostname(host_name)
        if h is not None and h.group(2):
            deploy_action_id, req = create_publish_request(self.comp.lddtype,h.group(2),self.artifact,self.operation,self.app_group,qname,self.level,index, self.useDNS)
            if req:
                req['type']='JAVA'
                req['queryHost']=req['host']
                return req
        return None

class be_builder(abstract_builder):

    def process(self, host_name, index):
        qname = self.artifact.artifactId
        h = parse_hostname(host_name)
        if h is not None and h.group(2):
            deploy_action_id, req = create_publish_request(self.comp.lddtype,h.group(2),self.artifact,self.operation,self.app_group,qname,self.level,index, useDNS=self.useDNS)
            if req:
                req['type']='BE'
                return req
        return None




class tibcobw_builder(abstract_builder):


    deployment_cache = {}

    def __init__(self, comp, artifact, level, useDNS, resolver=None):
        super(tibcobw_builder,self).__init__(comp, artifact, level, useDNS)
        self.resolver = resolver

    def process_targets(self, targets):
        index = 1
        request_cache = {}
        for host_name in targets:
            h = parse_hostname(host_name)
            if h is not None and h.group(2) and ('register' == self.operation or self.operation == 'publish'):
                LOGGER.debug("operation=%s, grouping=%s" %  (self.operation, h.group(2)))
                instances = self.fdeployBWQueryHost(targets, h.group(2))
                LOGGER.debug("%s -> %s, num of instances: %s",self.comp, host_name, len(instances))
                for inst in instances:
                    LOGGER.debug("%s, %s" % (inst, inst.keys()))
                    if 'host' in inst.keys():
                        # and 'name' in inst.keys():
                        req = self.process(inst['host'], index)
                        if not req:
                            continue
                        #LOGGER.debug('instance %s [%s]' % (inst, req))
                        try:
                            key = "%s-%s" % (req['host'],inst['name'])
                            if not key in request_cache.keys():
                                LOGGER.info('adding instance %s [%s]' % (inst['host'], req['deployId']))
                                index += 1
                                request_cache[key]= req
                        except Exception as e :
                            raise Exception(e,req)
        LOGGER.info("count=%s %s" % (len(request_cache.values()), request_cache.values()))
        return request_cache.values()

    def process(self, host_name, index):
        if host_name is not None:
            if 'register' == self.operation or self.operation == 'publish':
                deploy_action_id, req = create_publish_request(
                    self.comp.lddtype,host_name,self.artifact,self.operation,self.app_group, 'query',self.level,index, self.useDNS)
                return req
            elif 'unregister' == self.operation or 'unpublish' == self.operation:
                deploy_action_id, req = create_publish_request(
                    self.comp.lddtype,host_name,self.artifact,self.operation,self.app_group,'query',self.level,index, self.useDNS)
                return req
        return None

    def fdeployBWQueryHost(self,targets, host_name, clear_cache=True):
        queries = []
        # Download the artifact and extract it ...
        if self.resolver is None:
            raise Exception("resolver is required for BW")
        deployment_config_xml = None
        tibcoxml = None
        if clear_cache == True:
            self.deployment_cache = {}
        if not self.artifact.artifactId in self.deployment_cache.keys():
            found = self.resolver.resolveGavs(self.comp, True)
            if not found:
                raise Exception("component %s cannot be resolved or found" %(self.comp))
            deployment_config_xml, tibcoxml = fdeploy.extract_deployment_xml(self.level, self.artifact.archiveFileName, \
                'tmp/target', artifactId=self.artifact.artifactId)
            self.deployment_cache[self.artifact.artifactId] = [ deployment_config_xml, tibcoxml, found]
        deployment_config_xml, tibcoxml, found = self.deployment_cache[self.artifact.artifactId]
        LOGGER.info(' extracting BW bindings from %s' % (deployment_config_xml))
        #print(' extracting BW bindings from %s' % (deployment_config_xml))
        docs = read_bw_bindings(deployment_config_xml, self.artifact.artifactId)
        instances, mapping = get_instance_names(docs,targets)
        for instance in instances:
            if  host_name == instance['host']:
                queries.append( instance )
        return queries
