#import fedex
import fdeploy
import re
import dns.resolver
import logging


LOGGER = None
TRACE=False
LOG_FORMAT = "%(asctime)-15s [%(levelname)-5s](%(filename)s:%(module)s:%(lineno)d) - %(funcName)s: %(message)s"

def init_logger(level__=logging.CRITICAL):
    if globals()['LOGGER'] is None:
        logger__ = logging.getLogger('ldds')
        logging.basicConfig(format=LOG_FORMAT, level=level__)
        globals()['LOGGER']=logger__
    return globals()['LOGGER']

LOGGER = init_logger()
def tracer(msg):
    if TRACE:
        LOGGER.debug("[%s] %s" % ('TRACE', msg))
LOGGER.__dict__['trace'] = tracer
LOGGER.setLevel(logging.NOTSET)

def dns_cname(host):
    # first see if there is an alias
    if fdeploy.IS_ON_FEDEX_NETWORK == False:
        return host,host
    try:
        answers = dns.resolver.query(host, 'CNAME')
        fdeploy.LOGGER.trace('query qname: %s num ans. %s' % (answers.qname,  len(answers)))
        for rdata in answers:
            target = re.sub(r'\..+$', '', str(rdata.target))
            LOGGER.debug('cname target address: %s',target)
            return target,host
    except:
        pass
    # when arriving here there was not CNAME available
    hosts=[]
    if host.find('.') < 0:
        hosts.append( "%s.ute.fedex.com" % (host))
        hosts.append( "%s.test.cloud.fedex.com" % (host))
        hosts.append( "%s.paas.fedex.com" % (host))
    for qhost in hosts:
        try:
            answers = dns.resolver.query(qhost)
            #print answers
            # if answer is found
            for rdata in answers:
                target = re.sub(r'\..+$', '', str(answers.qname))
                qname = re.sub(r'\.$','', str(answers.qname))
                return target, qname
        except Exception:
            pass
    return host, host
