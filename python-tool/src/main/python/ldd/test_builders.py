import os
from fdeploy.UtilsForTesting import get_options
from fdeploy.UtilsForTesting import get_compnent_from_json
from ldd.request_builders import java_builder, be_builder


class TestBuilders(object):

    options = get_options()
    extractor = None
    docs = None
    resolver = None
    lddService = None

    @classmethod
    def setup_class(module):
        os.environ['NO_PROXY'] = '127.0.0.1'

    def setup_method(self, method):
        cdw = os.path.abspath("%s/../../test/resources" % (os.getcwd()))
        self.options.defaults['nexusURL'] = 'file://%s/local' % (cdw)


#{'deployId': 'dashboard-process_1', 'version': '6.1.0-SNAPSHOT', 'queryCmd': 'dashboard-process', 'level': 'L2', 'host': 'irh00600.ute.fedex.com', 'opco': 'FXS', 'type': 'JAVA', 'queryHost': 'irh00600.ute.fedex.com', 'artifactId': 'dashboard-process'}
    def test_process_java_publish(self):
        (artifact, comp, fdl) = get_compnent_from_json(
            'L2', '../../test/resources/ldd/fxs-dashboard-process.json')

        result = {'deployId': 'dashboard-process_1',
            'version': 'None',
            'queryCmd': 'dashboard-process',
            'level': 'L2',
            'host': 'irh00600.ute.fedex.com',
            'opco': 'FXE',
            'type': 'JAVA',
            'queryHost': 'irh00600.ute.fedex.com',
            'artifactId': 'dashboard-process'}
        builder = java_builder(comp, artifact, 'L2', False)

        req = builder.process(
            'tibcosilver@irh00600.ute.fedex.com:/opt/tibco/silver/persistentdata/FXS/apps/dashboard-process/', 1)

        assert not req is None
        assert ['deployId', 'version', 'queryCmd', 'level', 'host',
                'opco', 'type', 'queryHost', 'artifactId'] == req.keys()
        #assert result.values() == req.values()
        for k in result.keys():
            assert result[k] == req[k]

    def test_process_be_publish(self):
        result = {'deployId': 'sefs_suStop_1', 'version': 'None', 'queryCmd': 'sefs_suStop', 'level': 'L2',
        'host': 'irh00606.ute.fedex.com', 'opco': 'FXE',
        'type': 'BE', 'queryHost': 'irh00606.ute.fedex.com', 'artifactId': 'sefs_suStop'}
        (artifact, comp,fdl) = get_compnent_from_json('L2', '../../test/resources/ldd/fxe_be_Stop.json')
        builder = be_builder(comp, artifact, 'L2', False)

        req = builder.process(
            'tibco@irh00606.ute.fedex.com:/opt/fedex/sefs/FXE/apps/sefs_suStop', 1)

        assert not req is None
        assert ['deployId', 'version', 'queryCmd', 'level', 'host',
                'opco', 'type', 'queryHost', 'artifactId'] == req.keys()
        #assert result.values() == req.values()
        for k in result.keys():
            assert result[k] == req[k]


if __name__ == '__main__':
    unittest.main()
