import os
from fdeploy.UtilsForTesting import get_options
from fdeploy.UtilsForTesting import get_compnent_from_json
from ldd.request_builders import pcf_java_builder

class TestPcfJavaBuilder(object):

    options = get_options()
    extractor = None
    docs = None
    resolver = None
    lddService = None

    @classmethod
    def setup_class(module):
        os.environ['NO_PROXY']='127.0.0.1'

    def setup_method(self, method):
        #if os.path.exists('./tmp') == True:
        #    shutil.rmtree('./tmp', ignore_errors=True, onerror=None)
        cdw = os.path.abspath("%s/../../test/resources" % (os.getcwd()))
        self.options.defaults['nexusURL'] = 'file://%s/local' % (cdw)

    def test_process_pcf_publish(self):
        (artifact, comp,fdl) = get_compnent_from_json('L3','../../test/resources/pcf/handling-unit-matching-service.json')

        builder = pcf_java_builder(comp,artifact,'L3', False)
        req = builder.process('development@api.sys.wtcdev.fedex.com', 1)
        assert not req is None
        assert ['deployId', 'version', 'queryCmd', 'level', 'host', 'opco', 'type', 'queryHost', 'artifactId'] == req.keys()

if __name__ == '__main__':
    unittest.main()
