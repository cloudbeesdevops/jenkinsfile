import os
from fdeploy.UtilsForTesting import get_options
from ldd.tibcoBWextractor import get_app_name, get_archive_name, get_docs_by_artifactId, get_instance_name, get_instance_number, get_num_of_instances, read_bw_bindings


class TestTibcoBwextractor(object):

    options = get_options()
    extractor = None
    docs = None
    resolver = None

    def setup_method(self, method):
        #if os.path.exists('./tmp') == True:
        #    shutil.rmtree('./tmp', ignore_errors=True, onerror=None)

        cdw = os.path.abspath("%s/../../test/resources" % (os.getcwd()))
        self.options.defaults['nexusURL'] = 'file://%s/local' % (cdw)
        deployment_config_xml = ['../../test/resources/bw5/sefs_suAddress_template.xml','../../test/resources/bw5/sefs_suAddress_template2.xml','../../test/resources/bw5/sefs_suAddress_template3.xml']
        self.docs = read_bw_bindings(deployment_config_xml, 'sefs_suAddress')

    def test_get_instance_names(self):
        pass

    def test_read_bw_bindings(self):
        deployment_config_xml = '../../test/resources/bw5/sefs_suAddress_template.xml'
        docs = read_bw_bindings(deployment_config_xml, 'sefs_suAddress')
        assert 1 == len(docs)
        assert 3 == get_num_of_instances(docs[0])
        assert 3 == len(self.docs)
        instance = get_instance_number(self.docs[0], 0)['machine']
        assert 'FXE_L4_FXE_BW_TLM_VM2' == instance
        # one binding
        deployment_config_xml = '../../test/resources/bw5/sefs_suAddress_template-one-binding.xml'
        docs = read_bw_bindings(deployment_config_xml, 'SUAddressOnlyOne', clear_cache=True)
        assert 1 == get_num_of_instances(docs[0])
        assert ['SUAddress', 'Process_Archive', 1, ['SUAddress']] == get_instance_name(docs,docs[0])
        assert 'FXE_L4_FXE_BW_TLM_2' == get_instance_number(docs[0], 0)['machine']


    def test_docs_by_artifactIds(self):
        dlist = get_docs_by_artifactId(self.docs,'sefs_suAddress')
        assert 3 == len(dlist)
        dlist = get_docs_by_artifactId(self.docs,'sefs_suAdd')
        assert 0 == len(dlist)


    def test_get_archive_name_get_app_name(self):
        assert 'Process_Archive' == get_archive_name(self.docs[0])
        assert 'SUAddress' == get_app_name(self.docs[0])
        assert 3 == get_num_of_instances(self.docs[0])
        assert 'Process_Archive' == get_archive_name(self.docs[1])
        assert 'SUAddress' == get_app_name(self.docs[1])
        assert 3 == get_num_of_instances(self.docs[1])
        assert 'Process_Archive' == get_archive_name(self.docs[2])
        assert 'SUAddress' == get_app_name(self.docs[2])
        assert 3 == get_num_of_instances(self.docs[2])
