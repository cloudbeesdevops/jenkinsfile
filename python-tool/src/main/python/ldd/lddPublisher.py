# -*- coding: utf-8 -*-

import re
import fdeploy
import requests
from ldd import dns_cname, LOGGER
from ldd.request_builders import pcf_java_builder, tibcobw_builder
#from fdeploy import app_group_name

def ldd_request_builder(comp,artifact,level,resolver, useDNS):
    type = comp.type
    #print comp.__dict__
    if type == 'pcf':
        return pcf_java_builder(comp,artifact,level, useDNS)
    elif 'tibco_bw' in type:
        return tibcobw_builder(comp,artifact,level, useDNS, resolver=resolver)
    # elif 'tibco_be' in type:
    #     return
    # elif 'java' in type:
    #     return
    else:
        #print 'undefined type \'%s\' for building request' % (type)
        LOGGER.error('undefined type \'%s\' for building request')


def normalize_level(level):
    level = level.upper()
    if level.startswith('L'):
        m = re.match('^(L[0-9])', level)
        level = m.groups()[0]
    elif level.startswith('PR'):
        m = re.match('^(PRO?D)',level)
        level = m.groups()[0]
    else:
        raise Exception('level notation %s not supported must start with L1/2/3/4/5/6 or PRD/PROD.')
    return level


class lddPublisher(object):

    options = None
    base_dir = None
    queue = None
    # dcvqueue_ref = []
    resolver = None

    def __init__(self,level,options,base_dir,resolver, useDNS=True):
        self.base_dir = base_dir
        self.options = options
        self.useDNS = useDNS
        #self.level = level
        self.resolver = resolver
        self.queue = []
        self.ldd_url = 'http://c0009869.test.cloud.fedex.com:8090/sefs-dashboard'
        defaults=options.defaults
        if defaults is not None:
            if 'ldd_url' in defaults:
                self.ldd_url = defaults['ldd_url']
                LOGGER.warn("overwriting LDD url to '%s'" % (self.ldd_url))

    ''' needed for publishing : artifact : {version,artifactId}, comp : { source}, level, targets'''
    def publish(self,level, artifact,comp,meta_data=None):
        LOGGER.info(" artifact %s / version %s" % (artifact.artifactId, artifact.version))
        if artifact.version is None:
            LOGGER.warn("skipping artifact %s, since there is no version available." % (artifact))
        LOGGER.debug("publishing %s in %s for %s specified in %s" % (artifact.artifactId, level, artifact.version,comp.source))
        # extracting the opco or app group
        targets = None
        for _level in comp.levels:
            #if level.startswith(_level['level']):
            if level == _level['level']:
                targets = fdeploy.flatten_level_targets(_level['targets'],self.options.group, self.options.regex)
        if targets is None:
            raise Exception(">>>no targets for this environment")
        LOGGER.debug("using selected targets: %s" % (targets) )
        return self.queue_query_request('register', level, comp,artifact,targets) #sss,'publish', self.queue)

    ''' gathering unpublishing requests and queue them up '''
    def unpublish(self,level, artifact, comp):
        LOGGER.info(" artifact %s / version %s in level %s" % (artifact.version, artifact.artifactId, level))
        LOGGER.debug("unpublishing %s for %s specified in %s" % (artifact.artifactId, artifact.version,comp.source))
        # extracting the opco or app group
        targets = None
        for _level in comp.levels:
            if level.startswith(_level['level']):
                targets = fdeploy.flatten_level_targets(_level['targets'],self.options.group, self.options.regex)
        if targets is None:
            raise Exception(">>>no targets for this environment")
        LOGGER.debug("using selected targets: %s" % (targets) )
        return self.queue_query_request('unregister', level, comp,artifact,targets) #,'unpublish', self.queue)


    def unpublishRequests(self, path="unregisterApplications", message="unpublishing"):
        self.__doRequests(path, message)

    def publishRequests(self,path="registerApplications", message="publishing"):
        self.__doRequests(path, message)

    def reportRequests(self,path="registerApplications", message="publishing"):
        self.__doRequests(path, message, None, True)

    def queue_query_request(self, operation, level, comp, artifact, targets):
        __queue = []
        app_group = fdeploy.get_app_group(comp)
        if artifact.version is None and 'unregister' != operation:
            LOGGER.error("no archive found for %s" % (artifact))
            return __queue
        if app_group is None:
            LOGGER.debug("no opco/app_group found in %s" % (comp.source))
            return __queue
        LOGGER.info("publishing for app_group %s to %d hosts." % (app_group,len(targets)))

        builder = ldd_request_builder(comp,artifact,level, self.resolver, self.useDNS)
        LOGGER.debug("builder=%s (#%s targets)" % (builder, len(targets)))
        if builder:
            new_requests = builder.process_targets(targets)
            LOGGER.info("new requests from #%s targets is %s" % (len(targets), len(new_requests)))
            for req in new_requests:
                __queue.append(req)
        return __queue

    def doRequests(self, path='unregisterApplications', message='unpublishing', queue=None):
        self.__doRequests(path, message, queue)

    ''' BATCH DEPLOYMENT REGISTRATION '''
    def __doRequests(self, path='unregisterApplications', message='unpublishing', queue=None, reportOnly=False):
        if queue is not None:
            self.queue = queue
        if self.queue is None or len(self.queue) == 0:
            raise Exception("no requests in queue, publish or unpublish first on the service")
        for req in self.queue:
            level = normalize_level(req['level'])
            if req['level'] != level:
                req['level']=level
        requrl = "%s/api/public/query/%s" % ( self.ldd_url, path)
        LOGGER.info("%s to %s",message,requrl)
        LOGGER.debug("issuing request to %s for %s",path, requrl)
        LOGGER.debug("issued request: ",self.queue)
        if reportOnly == False:
            ret = requests.post(requrl, json=self.queue, proxies=fdeploy.FDX_PROXIES)
            response = 'successful' if ret.ok in [200, 201] or ret.ok == True else "failed: code %s" % (ret.ok)
            LOGGER.info("last artifact registered: %s %s - %s" % (message, req['artifactId'], req['version']))
        if reportOnly == True:
            self.reporting()

    def refresh(self,level):
        LOGGER.info("refreshing %s" % (level))
        m =  re.match('^(.+:).+',self.ldd_url)
        if m and m.groups():
            # refresh_url = "%s7003/ping" % (m.groups()[0])
            # fdeploy.LOGGER.trace("refreshing harvester with by calling %s" % (refresh_url))
            # ret = requests.get(refresh_url, proxies=fdeploy.FDX_PROXIES)
            # response = 'successful' if ret.ok in [200, 201] or ret.ok == True else "failed: code %s" % (ret.ok)
            # LOGGER.debug("LDD     - refresh response %s",response)
            pass
        else:
            LOGGER.debug("LDD     - refresh response")
            LOGGER.error("failed to send refresh, skipping %s",self.ldd_url)

    def reporting(self):
        index = 1
        report = []
        fdeploy.LOGGER.trace( str(self.queue))
        for req in self.queue:
            LOGGER.debug( "%s of %s : %s",index,len(self.queue), req)
            report.append( "%03d | %4s | %15s | %30s | %30s | %60s | %s  " % (index, req['type'], req['queryHost'], req['artifactId'], req['deployId'], req['queryCmd'],  req['host'] ))
            index += 1
        file = open("Reporting.log", "w")
        for report_line in report:
            file.write("%s\n" % (report_line))
        LOGGER.warn("see Reporting.log for details")

def create_publish_request(lddtype, host, artifact, operation,app_group, query, level,index, useDNS=True):
    _type = re.sub( r'TIBCO_', '',lddtype.upper())
    cname,fqname = (host,host) if useDNS == False else dns_cname(host)
    cname = cname.lower()
    fqname = fqname.lower()
    deployId = "%s_%s" % (artifact.artifactId, index)
    deploy_id_act= "%s@%s" % (deployId,operation)

    #if str(deploy_id_act) not in self.queue_ref:
    return deploy_id_act, {'deployId': str(deployId), 'artifactId' : str(artifact.artifactId), 'version' : str(artifact.version), \
        'level' : str(level), 'opco' : str(app_group), 'queryHost' : str(cname), \
        'host' : str(fqname), 'queryCmd' : str(query), 'type' : str(_type)}
    #return None


def process_pcf_publishing(level, artifact, lddtype, host_name, operation, app_group, qname, index, useDNS ):
    qname = artifact.artifactId
    COM=re.compile('(\S+)\@([a-z0-9-\._]+)')
    h = COM.match(host_name)
    if h is not None and h.group(2):
        deploy_action_id, req = create_publish_request(lddtype,h.group(2),artifact,operation,app_group,qname,level,index, useDNS)
        if req:
            req['type']='PCF_JAVA'
            appid = req['queryCmd']
            domain = req['host']
            try:
                req['host'].index('api.sys')
                domain = req['host'].split('api.sys')[1]
            except:
                pass

            req['host']= "%s-%s.app%s" % (level, appid, domain)
            req['queryCmd']="%s-%s" % (level, appid)
            req['queryHost']=req['host']
            return req
    return None
#
# for level in ['L4', 'L4B', 'PROD', 'PRD-COS', 'L4C-COS']:
#     LOGGER.debug("BEFORE %s" % (level)
#     if level.startswith('L'):
#         m = re.match('^(L[0-9])', level)
#         level = m.groups()[0]
#     elif level.startswith('PR'):
#         m = re.match('^(PRO?D)',level)
#         level = m.groups()[0]
#     else:
#         raise Exception('level notation %s not supported must start with L1/2/3/4/5/6 or PRD/PROD.')
#     LOGGER.debug("AFTER %s" % (level)
