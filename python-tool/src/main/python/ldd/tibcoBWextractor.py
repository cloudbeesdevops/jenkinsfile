import xmltodict
import os
import os.path
import fdeploy
import re
from ldd import LOGGER
import copy

BINDINGS_CACHE = {}

def read_bw_bindings(bindings_files, artifactId, clear_cache=False):
	docs = []
	ii = 1
	if type(bindings_files) != list:
		bindings_files = [bindings_files]
	if clear_cache == True:
		LOGGER.warn('deleting cached bindings, removing %s binding references' % (len(BINDINGS_CACHE.keys())))
		globals()['BINDINGS_CACHE'] = {}
	for bindings_file in  bindings_files:
		if not os.path.isfile(bindings_file):
			raise Exception("Bindings XML File %s not existing or readable [%s]." % (bindings_file,bindings_files))
		if bindings_file not in BINDINGS_CACHE.keys():
			LOGGER.debug("reading %s -> %s", bindings_file,artifactId)
			with open(bindings_file) as fd:
				doc = xmltodict.parse(fd.read(), dict_constructor=dict)
				BINDINGS_CACHE[bindings_file]=doc
		else:
			doc=BINDINGS_CACHE[bindings_file]
			doc = copy.deepcopy(doc)
		doc['instance']=ii
		doc['documentName']=bindings_file
		doc['artifactId']=artifactId
		ii+=1
		docs.append(doc)
	LOGGER.info("read %s bindings." % (len(BINDINGS_CACHE.keys())))
	return docs

def get_docs_by_artifactId(docs,artifactId):
	doc_list = []
	for doc in docs:
		#LOGGER.debug(" --> %s -->\n" % (doc)
		if doc['artifactId'] != artifactId:
			LOGGER.debug("[tibcoBWextractor.extractBindingsAsHost] %s %s", doc['artifactId'], doc['documentName'])
		else:
			doc_list.append(doc)
	return doc_list

def get_app_name(doc):
	try:
		return doc['application']['@name']
	except KeyError:
		pass

def get_archive_name(doc):
	archive_name = None
	domain = None
	try:
		archive_name = re.sub(
			r'\.pa.+$', '', doc['application']['services']['bw']['@name'])
	except KeyError:
		try:
			archive_name = doc['application']['@name']
		except KeyError:
			pass
	# Here the rules ... if there is
	# a remoInstanceName the namign will be [repoInstanceName]-[idx]-]appname(par)]
	try:
		# remove the first part %DOMAIN%-fxg-bw-bla-bla to fxg-bw-bla-bla
		domain = "-".join(doc['application']['repoInstanceName'].split('-')[1:])
		fdeploy.LOGGER.trace( "alters     = %s" % (domain))
	except:
		pass
	if archive_name is None:
		archive_name = domain

	archive_name = re.sub(r'\s', '_', archive_name)
	return archive_name

def get_instance_number(doc,value):
	try:
		if get_num_of_instances(doc) > 1:
			return doc['application']['services']['bw']['bindings']['binding'][value]
		else:
			return doc['application']['services']['bw']['bindings']['binding']
	except:
		return None

def get_num_of_instances(doc):
	if type(doc['application']['services']['bw']['bindings']['binding']) == list:
		return len(doc['application']['services']['bw']['bindings']['binding'])
	else:
		return 1

def get_instance_names(docs, targets):
	instances_list =[]
	totalCount = 0
	mapping = {}
	i = 0
	for target in targets:
		(tlm, host) = target.split('@')
		mapping[str(tlm)]=str(host)
		mapping[str(host)]=str(tlm)
	for doc in docs:
		nofinstances = get_num_of_instances(doc)
		totalCount+=1
		app_name = get_app_name(doc)
		archive_name = get_archive_name(doc)
		if i != 0:
			app_name = "%s-%s" % (app_name, i)
		if nofinstances > 1:
			# if there is one document for this artifact just add appname index (i)
			for value in range(0,nofinstances):
				name = __building_instance_name(app_name, archive_name, value)
				tlm = get_instance_number(doc, value)['machine']
				if len(mapping) != 0:
					try:
						host = mapping[tlm]
					except:
						raise Exception("%s not found in mapping %s" % (tlm,mapping))
				else:
					host = 'NA'
				instances_list.append({ 'name' : name, 'host' : host, 'tlm' : tlm, 'instance' : value, 'total' : totalCount, 'app#' : app_name, 'archive_name' : archive_name  })
		else:
			# if there is one documets for this artifact just add appname index (i)
			instances_list.append({ 'name' : app_name, 'host' : host, 'tlm' : tlm, 'instance' : 1, 'total' : 1, 'app#' : app_name, 'archive_name' : archive_name  })
		i+=1
	return instances_list, mapping

def __building_instance_name(app_name, archive_name, value):
	if int(value) < 1:
		return "%s-%s" % (app_name, archive_name)
	else:
		return "%s-%s-%s" % (app_name, archive_name, value)

def get_instance_name(docs,doc,self_domain=None):
	app_name = None
	domain = None
	try:
		app_name = re.sub(
			r'\.pa.+$', '', doc['application']['services']['bw']['@name'])
	except KeyError:
		pass
	# Here the rules ... if there is
	# a remoInstanceName the namign will be [repoInstanceName]-[idx]-]appname(par)]
	try:
		fdeploy.LOGGER.trace("repoInstance = %s" % (doc['application']['repoInstanceName']))
	except:
		fdeploy.LOGGER.trace("repoInstance = %s" % ('NA'))
	try:
		fdeploy.LOGGER.trace("PP-->Nm        %s" % (doc['application']['@name']))
	except:
		fdeploy.LOGGER.trace("app-->name   = %s" % ('NA'))
	try:
		# remove the first part %DOMAIN%-fxg-bw-bla-bla to fxg-bw-bla-bla
		shadow_domain = "-".join(doc['application']['repoInstanceName'].split('-')[1:])
		domain = shadow_domain
		fdeploy.LOGGER.trace( "alters     = %s" % (domain))
	except:
		domain = self_domain
	if app_name is None:
		app_name = domain

	app_name = re.sub(r'\s', '_', app_name)
	instances_list =[]
	if len(get_docs_by_artifactId(docs, doc['artifactId'])) > 1 and doc['instance'] > 1:
		# if there is one document for this artifact just add appname index (i)
		instances_list.append( "%s-%s" % (domain, doc['instance']-1))
	else:
		# if there is one documets for this artifact just add appname index (i)
		instances_list.append( "%s" % (domain))
	return [domain,app_name,doc['instance'], instances_list]
