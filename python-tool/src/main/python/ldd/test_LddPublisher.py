import json
import os
from ldd import dns_cname, LOGGER
import fdeploy
import requests
from ldd.lddPublisher import lddPublisher, process_pcf_publishing
from ldd.tibcoBWextractor import read_bw_bindings
from fdeploy.UtilsForTesting import get_options, skipping_when_running_outside_fedex_network, get_compnent_from_json
#from fdeploy.UtilsForTesting import get_options, skipping_when_running_outside_fedex_network
from fdeploy.MockHttpServer import start_mock_server
import BaseHTTPServer
from BaseHTTPServer import BaseHTTPRequestHandler
from fdeploy.nexusVersionResolver import nexusVersionResolver

TEST_HTTP_PORT=None
os.environ['NO_PROXY']='127.0.0.1'

class LDDServiceHandler(BaseHTTPRequestHandler):
    timeout = 5

    def setup(self):
        BaseHTTPServer.BaseHTTPRequestHandler.setup(self)
        self.request.settimeout(5)

    def do_POST(self):
        self.send_response(requests.codes.ok)
        # Add response headers.
        self.send_header('Content-Type', 'application/json; charset=utf-8')
        self.end_headers()
        response_content = json.dumps([])
        self.wfile.write(response_content.encode('utf-8'))
        return

os.environ['NO_PROXY']='127.0.0.1'
fdeploy.is_on_fedex_network(True)
TEST_HTTP_PORT = start_mock_server(LDDServiceHandler)


class TestLddPublisher(object):

    options = get_options()
    extractor = None
    docs = None
    resolver = None

    def setup_method(self, method):
        #if os.path.exists('tmp'):
        #    shutil.rmtree('tmp')
        #ldd.test_LddPublisher.TEST_HTTP_PORT = start_mock_server(LDDServiceHandler)
        cdw = os.path.abspath("%s/../../test/resources" % (os.getcwd()))
        self.options.defaults['nexusURL'] = 'file://%s/local' % (cdw)
        self.options.defaults['ldd_url'] = 'http://127.0.0.1:%s' % (TEST_HTTP_PORT)

        #LOGGER.debug(">>> init >>>"
        bindings_file = [
            '../../test/resources/bw5/sefs_suAutoDGFeed_template.xml']
        # create artifact resovler with caching features.
        self.resolver = nexusVersionResolver(self.options,
                                                     fdeploy.useCache, 'target')
        self.extractor = lddPublisher('L1', self.options, '', self.resolver, False)
        self.docs = read_bw_bindings(bindings_file, 'FXFShipFeed')
        LOGGER.warn(fdeploy.FDX_PROXIES)
        ret = requests.post(self.options.defaults['ldd_url'], proxies=fdeploy.FDX_PROXIES)
        assert ret.ok

    def test_process_pcf_publish(self):
        (artifact, comp, fdl) = get_compnent_from_json('L3','../../test/resources/pcf/handling-unit-matching-service.json')
        req = process_pcf_publishing('L3', artifact,'PCF_JAVA', 'releases@host.fedex.com','publish','FXE','FXE_SULEGACY',0, False)
        assert not req is None
        req = process_pcf_publishing('L3', artifact,'PCF_JAVA', 'releases@api.sys.fedex.com','publish','FXE','FXE_SULEGACY',0, False)
        assert not req is None

    def test_publish_ldd(self):
        o = get_options()
        o.action = 'stage'
        o.command = 'deploy'
        (a,comp,fdl) = get_compnent_from_json('L2', '../../test/resources/bw5/FXE_SULEGACY.json',o)
        assert 'config.FXE/FXE_SULEGACY.json' == comp.source
        self.options.level = 'L2'
        lddService = lddPublisher('L2', self.options, 'target', fdl.resolver, False)
        lddService.publish('L2', a,comp)

#     @skipping_when_running_outside_fedex_network
# disabled due to lookup against a live dns server
#     def test_dns_cname(self):
#         assert 'c0009869.test.cloud.fedex.com' ==  dns_cname('c0009869.test.cloud.fedex.com')[0]
#         assert 'c0008549' ==  dns_cname('fgsefscore-cos-pa05.prod.cloud.fedex.com')[0]
#         assert 'wtcdev2.paas.fedex.com' ==  dns_cname('wtcdev2.paas.fedex.com')[0]
#         assert 'c0009869' ==  dns_cname('c0009869')[0]

    def test_ldd_pcf_query_creation(self):
        level = 'L3'
        (artifact,comp,fdl) = get_compnent_from_json(level, '../../test/resources/pcf/handling-unit-matching-service.json')
        lddService = lddPublisher(level, self.options, 'target', self.resolver, useDNS=False)

        assert 'pcf' ==  comp.lddtype
        queue = []
        queue = lddService.queue_query_request('publish', level, comp, artifact, comp.levels[0]['targets'])
        assert 1 ==  len(queue)
        assert 'PCF_JAVA' ==  queue[0]['type']
        assert 'L3-handling-unit-matching-service.app.wtcdev2.paas.fedex.com' ==  queue[0]['host']
        assert 'L3-handling-unit-matching-service' ==  queue[0]['queryCmd']
        assert 'L3-handling-unit-matching-service.app.wtcdev2.paas.fedex.com' ==  queue[0]['queryHost']

        lddService.reporting()
