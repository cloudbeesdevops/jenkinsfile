import os
import fdeploy
from fdeploy.UtilsForTesting import get_options, get_compnent_from_json
from ldd.request_builders import tibcobw_builder
from ldd.lddPublisher import lddPublisher
from fdeploy.fdeployLoader import fdeployLoader
from ldd import LOGGER
from fdeploy.nexusVersionResolver import nexusVersionResolver

class TestTibcoBwBuilder(object):

    options = get_options()
    extractor = None
    docs = None
    resolver = None
    cdw = None
    lddService = None

    @classmethod
    def setup_class(module):
        os.environ['NO_PROXY']='127.0.0.1'

    def setup_method(self, method):
        #if os.path.exists('tmp') == True:
        #    shutil.rmtree('tmp', ignore_errors=True, onerror=None)
        self.cdw = os.path.abspath("%s/../../test/resources" % (os.getcwd()))
        self.options.defaults['nexusURL'] = 'file://%s/local' % (self.cdw)
        self.resolver = nexusVersionResolver(self.options, fdeploy.useCache, 'target')


    def test_ldd_bw2_fdeployBW(self):
        level = 'L1'
        self.options.level = 'L1'
        (artifact,comp,fdl) = get_compnent_from_json(level, '../../test/resources/ldd/fxe-bw2-EADDFeed.json')


    def test_ldd_bw_fdeployBW(self):
        level = 'L4'
        self.options.level = 'L4'
        self.options.defaults['nexusURL'] = 'file://%s/local' % (self.cdw)
        (artifact,comp,fdl) = get_compnent_from_json(level, '../../test/resources/bw5/FXE_SULEGACY.json')
        found = fdl.resolver.resolveGavs(comp, True, True, False)
        assert found is not None
        assert os.path.isfile(found.archiveFileName)

        targets = [
            "FXE_L4_FXE_BW_TLM_1@c0005131.test.cloud.fedex.com",
            "FXE_L4_FXE_BW_TLM_2@c0005132.test.cloud.fedex.com",
            "FXE_L4_FXE_BW_TLM_3@c0005133.test.cloud.fedex.com",
            "FXE_L4_FXE_BW_TLM_4@c0005134.test.cloud.fedex.com",
            "FXE_L4_FXE_BW_TLM_5@c0005135.test.cloud.fedex.com",
            "FXE_L4_FXE_BW_TLM_6@c0005136.test.cloud.fedex.com",
            "FXE_L4_FXE_BW_TLM_7@c0005137.test.cloud.fedex.com",
            "FXE_L4_FXE_BW_TLM_8@c0005138.test.cloud.fedex.com",
            "FXE_L4_FXE_BW_TLM_9@c0005139.test.cloud.fedex.com"
        ]
        builder = tibcobw_builder(comp,artifact,'L4',False, resolver=self.resolver)
        query = builder.fdeployBWQueryHost(targets, 'c0005131.test.cloud.fedex.com') #, clear_cache=True)
        assert 0 ==  len(query)
        query = builder.fdeployBWQueryHost(targets, 'c0005132.test.cloud.fedex.com')
        assert 3 ==  len(query)
        assert 'SUAddress-Process_Archive' ==  query[0]['name']
        assert 'c0005132.test.cloud.fedex.com' ==  query[0]['host']
        assert 'SUAddress-1-Process_Archive' ==  query[1]['name']
        assert 'c0005132.test.cloud.fedex.com' ==  query[1]['host']
        assert 'SUAddress-2-Process_Archive' ==  query[2]['name']
        assert 'c0005132.test.cloud.fedex.com' ==  query[2]['host']
        query = builder.fdeployBWQueryHost(targets, 'c0005134.test.cloud.fedex.com')
        assert 3 ==  len(query)
        assert 'SUAddress-Process_Archive-2' ==  query[0]['name']
        assert 'c0005134.test.cloud.fedex.com' ==  query[0]['host']
        assert 'SUAddress-2-Process_Archive-2' ==  query[2]['name']
        assert 'c0005134.test.cloud.fedex.com' ==  query[2]['host']

        lddService = lddPublisher(level,self.options,'base_dir',self.resolver, False)

        queue_ref = lddService.queue_query_request('publish',level, comp, artifact, targets)
        assert 9 ==  len(queue_ref)
        queue_ref = lddService.queue_query_request('register',level, comp, artifact, targets)
        assert 9 ==  len(queue_ref)
        queue_ref = lddService.queue_query_request('unregister',level, comp, artifact,  targets)
        assert 9 ==  len(queue_ref)
        queue_ref = lddService.queue_query_request('unpublish',level, comp, artifact,  targets)
        assert 9 ==  len(queue_ref)
