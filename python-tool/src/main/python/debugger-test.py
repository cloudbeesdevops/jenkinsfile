# -*- coding: utf-8 -*-
import copy
import imp
from io import TextIOWrapper, BytesIO
import io
import os
import logging
import re
import sys

import cloudbees
import fdeploy  # needed for log initializatgion


class StdoutBuffer(TextIOWrapper):

    def write(self, string):
        try:
            return super(StdoutBuffer, self).write(string.decode('utf-8'))
        except TypeError as t:
            with io.open('tex.txt', 'w') as f:
                f.write("%s" % (t))
            # redirect encoded byte strings directly to buffer
            return super(StdoutBuffer, self).buffer.write(string.decode('utf-8'))


logging.basicConfig(level=logging.DEBUG)


class TestDebug(object):

    VERBOSE_LEVEL = ''

    _original_argv_ = []

    def testTestCloudBeesIncrementorNoStdout(self):
        fdeploy.LOGGER = fdeploy.init_logger(logging.DEBUG)
        if len(self._original_argv_) < 1:
            self._original_argv_ = copy.deepcopy(sys.argv)
        print os.getcwd()
        sys.argv = ['fdeploy.py', '-i', '%s/.ssh/test_deploy_user_dsa' %
                    (os.environ['HOME']), '-vv', 'deploy', '-a', 'deploy', '-l', 'L1', '-f' 'config.FXE/fxe-bw2-Address.json']
        imp.load_source('__main__', 'fdeploy.py')
        sys.argv = self._original_argv_
        # do stuff with the output
        out = re.sub('\n', '', out)
        assert '1.0.6' == out
