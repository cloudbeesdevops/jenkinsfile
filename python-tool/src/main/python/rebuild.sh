rm ~/airflow/airflow.db
find . -name '*.pyc' -exec rm {} \;
PYTHONPATH=`pwd` airflow initdb
PYTHONPATH=`pwd` airflow webserver -p 8080
kill -9 $(ps -ef | grep airflow | grep -v grep | awk '{ print $2}')
