# -*- coding: utf-8 -*-

import os
from fdeploy import Options
import fdeploy


class TestFdeployMethods(object):

    def xest_security_realm(self):

        cli_options = Options(level='L3',action='deploy',command='release',identity=None,defaults=[])
        print fdeploy.__dict__.keys()
        fdeploy.security_realm(cli_options)
        assert cli_options.identity == "%s/.ssh/test_deploy_user_dsa" % (os.getenv('HOME'))
