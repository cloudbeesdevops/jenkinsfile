

import fdeploy
import fdeploy.document
import re
import json
from ldap import initialize, SCOPE_SUBTREE, RES_SEARCH_ENTRY
from fdeploy.fdeployLoader import fdeployLoader


cache_opts = {
    'cache.type': 'file',
    'cache.data_dir': '/tmp/cache/data',
    'cache.lock_dir': '/tmp/cache/lock'
}




def oracle_net_dict(oraclen):
    # (DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=vcd09801-vip.ute.fedex.com)(PORT=1526))(ADDRESS=(PROTOCOL=TCP)(HOST=vcd09804-vip.ute.fedex.com)(PORT=1526))(ADDRESS=(PROTOCOL=TCP)(HOST=vcd09802-vip.ute.fedex.com)(PORT=1526))(ADDRESS=(PROTOCOL=TCP)(HOST=vcd09803-vip.ute.fedex.com)(PORT=1526)))(CONNECT_DATA=(SERVER=dedicated)(SERVICE_NAME=sefs_fxf_svc1.ute.fedex.com)))
    x='(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=vcd09801-vip.ute.fedex.com)(PORT=1526))(ADDRESS=(PROTOCOL=TCP)(HOST=vcd09804-vip.ute.fedex.com)(PORT=1526))(ADDRESS=(PROTOCOL=TCP)(HOST=vcd09802-vip.ute.fedex.com)(PORT=1526))(ADDRESS=(PROTOCOL=TCP)(HOST=vcd09803-vip.ute.fedex.com)(PORT=1526)))(CONNECT_DATA=(SERVER=dedicated)(SERVICE_NAME=sefs_fxf_svc1.ute.fedex.com)))'
    x=oraclen
    if x is None:
        return None
    has_address_list = "ADDRESS_LIST" in x
    x = re.sub( r'\n', '', x)
    x = re.sub( r'\r', '', x)
    #print x
    x = re.sub( r'\(', '{\'', x)
    x = re.sub( r'\)', '\'}', x)
    x = re.sub( r'=', '\' : ', x)
    x = re.sub( r'}{', ',', x)
    x = re.sub( r'}\'', '}', x)
    x = re.sub( r'\'\s:\s', '\' : \'', x)
    x = re.sub( r'\'{\'', '{\'', x)
    x = re.sub( r'\'', '\"', x)
    x = re.sub( r'ADDRESS_LIST" :','ADDRESS_LIST" : [',x)
    x = re.sub( r'},\s*"CONNECT','}], "CONNECT',x)
    x = re.sub( r'{"ADDRESS" : {', '{',x)
    x = re.sub( r'},"ADDRESS" : {','},{', x)
    if has_address_list:
        x = re.sub( r'"}}]','"}]', x)
    else:
        x = re.sub( r'"ADDRESS" : {','"ADDRESS_LIST" : [{',x)
    # LIST": {
    #print "json-raw=%s" % (x)
    y = json.loads(x)
    oidlist=[]
    if 'DESCRIPTION' in y and 'ADDRESS_LIST' in y['DESCRIPTION']:
        for hosts in y['DESCRIPTION']['ADDRESS_LIST']:
            oidlist.append("%s:%s" % (hosts['HOST'], hosts['PORT']))
    #print "oidlist = %s" % (oidlist)
    return oidlist

def url_dn(uri):
    url = uri.split('/',3)
    dn = url.pop()
    url = '/'.join(url)
    return url,dn


def eit_lookup(url, attribute, extra_method, filter):
    filter = "(&(fxClientUID=%s))" % (filter)
    #filter = "fxJMSClientServerURL?sub?fxClientUID=%s" % (filter)
    (url,dn) = url_dn(url)
    x = ldap_lookup(url,dn, 'fxJMSClientServerURL',  extra_method, filter)
    eitlist=[]
    if x:
        for tcp in x:
            if not tcp in eitlist:
                eitlist.append(tcp)
    print "result=%s\n\n" % (x)
    return eitlist


def OID_lookup(url, attribute, extra_method, filter='(&(objectClass=orclNetService))'):
    (url,dn) = url_dn(url)
    return ldap_lookup(url,dn,'orclnetdescstring',extra_method,filter)


def ldap_lookup(url, dn, attribute, extra_method, filter):
    return ldap2_lookup(url, dn, attribute, extra_method, filter)

def ldap2_lookup(url, dn, attribute, extra_method, filter):
    #print "dn=%s" % (dn)
    #print "u=%s -> %s" % (url, url)
    #print "t=%s" % ((url))
    #print "f=%s" % (filter)
    l = initialize(url)
    l.simple_bind_s()
    searchScope = SCOPE_SUBTREE
    searchAttribute = [attribute]
    ldap_result_id = l.search(dn, searchScope, filter, searchAttribute)
    #Bind to the server
    result_set = []
    while 1:
        result_type, result_data = l.result(ldap_result_id, 0)
        if (result_data == []):
            break
        else:
            ## if you are expecting multiple results you can append them
            ## otherwise you can just wait until the initial result and break out
            if result_type == RES_SEARCH_ENTRY:
                a =extra_method(attribute, result_data)
                l.unbind_s()
                return a
    l.unbind_s()
    return result_set

f = open('jms-jdbc.properties','r')
eitlist={}
oracle={}
for l in f:
    if 'PRD' in l or 'PROD' in l or 'L4' in l:
        print "examaning: %s" % (l)
        x=l.rstrip().split(':',1)
        xx=l.rstrip().split(':')[0].split('/')
        xx.reverse()
        filename=xx[0]
        print xx
        print x
        project = x[0].split('/')[1]
        jdbc = x[1].split('jdbc:oracle:thin:@')
        dbc = x[1].split('jdbc:db2://')
        jms = x[1].split('fxClientUID=')
        print jms

        if jdbc and len(jdbc) > 1:
            def extract(attribute,result_data):
                result_set = result_data[0]
                #print ">>> %s" % (result_set[1][attribute][0])
                return result_set[1][attribute][0]
            jdbc = re.sub( r':3060/',':3060/cn=', jdbc[1])
            #print " %s --> " % (jdbc)
            type = 'jdbc-oracle'
            if not project in oracle.keys():
                oracle[project]=[]
            if ':3060' in jdbc:
                decl = OID_lookup(jdbc,'orclnetdescstring',extract)
                adddresss= oracle_net_dict(decl)
                oracle[project].extend(oracle_net_dict(decl))
            else:
                jdbc = jdbc.split(':')
                jdbc.pop()
                print ":".join(jdbc)
        #elif jms and len(jms) > 1:
        elif jms and len(jms) > 1:
            def extract(attribute,result_data):
                print "extract=%s" % (result_data)
                try:
                    return result_data[0][1]['fxJMSClientServerURL'] if len(result_data[0]) > 1 else None
                except:
                    return None
            jdbc = jms[1]
            ldap= 'ldap://apptstldap.corp.fedex.com/ou=messaging,dc=corp,dc=fedex,dc=com'
            if 'PROD' in filename or 'PRD' in filename:
                ldap = 'ldap://appldap.prod.fedex.com/ou=messaging,dc=prod,dc=fedex,dc=com'
            type = 'jms'

            if not project in eitlist.keys():
                eitlist[project]=[]
            eitlist[project].extend( eit_lookup(ldap,jdbc,extract,jdbc))
        elif dbc and len(dbc) > 1:
            jdbc = dbc[1]
            type = 'jdbc-db2'
        print "inspected: %s,%s,%s,%s" % (type, xx[0],  project, jdbc)

#
loader = fdeployLoader({'command' : 'menu'})
# #loader.readDescriptors(['config.FXE/fxe-java-repla*.json'])
# # loader.readDescriptors(['config.FXE/*.json'])
# #fdeploy.document.application_target_report(loader)
#
# # loader = fdeployLoader({'command' : 'menu'})
loader.readDescriptors(['config.FXE/*.json'])
fdeploy.document.application_target_report(loader,'L4')
