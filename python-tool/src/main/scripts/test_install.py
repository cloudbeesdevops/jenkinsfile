# -*- coding: utf-8 -*-

import pytest
import os
import time
import logging

from install import parse_source_repo_url, parse_nexus_url, elvis, parse_metadata, download_url_builder, gav_to_tuple
from install import creation_date, write_deploy_manifest, subject_pull
META_DATA = '''<metadata modelVersion="1.1.0">
<groupId>com.fedex.ground.sefs</groupId>
<artifactId>b</artifactId>
<version>1.3.0-SNAPSHOT</version>
<versioning>
<snapshot>
<timestamp>20180727.065118</timestamp>
<buildNumber>2</buildNumber>
</snapshot>
<lastUpdated>20180727065118</lastUpdated>
<snapshotVersions>
<snapshotVersion>
<extension>pom</extension>
<value>1.3.0-20180727.065118-2</value>
<updated>20180727065118</updated>
</snapshotVersion>
<snapshotVersion>
<extension>jar</extension>
<value>1.3.0-20180727.065118-2</value>
<updated>20180727065118</updated>
</snapshotVersion>
</snapshotVersions>
</versioning>
</metadata>

'''


class TestInstall():

    def test_parse_url(self):
        g = "git@gitlab.prod.fedex.com:/APP482552/repo.git"
        rel,prj,url = parse_source_repo_url(g)
        assert 'master' == rel
        assert 'config.repo' == prj
        assert 'git@gitlab.prod.fedex.com:/APP482552/repo.git' == url
        g = "git@gitlab.prod.fedex.com:/APP482552/repo.git->infrastructure"
        rel,prj,url = parse_source_repo_url(g)
        assert 'infrastructure' == rel
        assert 'config.repo' == prj
        assert 'git@gitlab.prod.fedex.com:/APP482552/repo.git' == url
        g = "https://gitlab+deploy-token-58:28Jq-325k47JxisqUnRD@gitlab.prod.fedex.com/APP3534642/itinerary-deploy.git->infrastructure"
        rel,prj,url = parse_source_repo_url(g)
        assert 'infrastructure' == rel
        assert 'config.itinerary-deploy' == prj
        assert 'https://gitlab+deploy-token-58:28Jq-325k47JxisqUnRD@gitlab.prod.fedex.com/APP3534642/itinerary-deploy.git' ==  url
        g = "EAI:git@gitlab.prod.fedex.com:/APP482552/repo.git->infrastructure"
        rel,prj,url = parse_source_repo_url(g)
        assert 'infrastructure' == rel
        assert 'config.EAI' == prj

    def test_nexus_url(self):
        g = 'FXE:com.fedex.sefs.core:fdeploy_FXE_CORE:1.12.3:zip'
        fd = {'FD_REPO':'bla'}
        fdrepo, arr, prok = parse_nexus_url(fd, g)
        assert 'bla' == fdrepo
        assert ['com.fedex.sefs.core', 'fdeploy_FXE_CORE', '1.12.3', 'zip'] == arr
        assert 'FXE' == prok

    def test_elvis(self):
        assert 'CDS' == elvis('CDS','default')
        assert 'default' == elvis(None,'default')
        assert os.environ['HOME'] == elvis('HOME','default',{})

    def test_parse_metadata(self):
        assert '3.2.4' == parse_metadata(META_DATA,'zip','3.2.4')
        assert '1.3.0-20180727.065118-2' == parse_metadata(META_DATA,'jar','1.3.0-SNAPSHOT')
        assert '1.3.0-20180727.065118-2' == parse_metadata(META_DATA,'jar','3.2-SNAPSHOT')

    def test_gav_to_tuple(self):
        assert ('com.fedex.sefs.core', 'fdeploy_FXE_CORE', '1.12.3', 'zip') == gav_to_tuple(['com.fedex.sefs.core', 'fdeploy_FXE_CORE', '1.12.3', 'zip'])
        assert ('com.fedex.sefs.common', 'core', None, None ) == gav_to_tuple(['com.fedex.sefs.common', 'core'])
        assert ('com.fedex.sefs.core', 'fdeploy_FXE_CORE', '1.12.3', None) == gav_to_tuple(['com.fedex.sefs.core', 'fdeploy_FXE_CORE', '1.12.3'])

    def test_download_builder(self):
        assert 'https://nexus.prod.cloud.fedex.com/nexus/repository/SEFS-6270-releases/com/fedex/sefs/core/fdeploy_FXE_CORE/1.12.3/fdeploy_FXE_CORE-1.12.3.zip' == download_url_builder('https://nexus.prod.cloud.fedex.com','com.fedex.sefs.core', 'fdeploy_FXE_CORE', '1.12.3', 'zip')
        assert None == download_url_builder('https://nexus.prod.cloud.fedex.com','com.fedex', 'core', '1.12.3-SNAPSHOT', 'zip')

    def test_creation_date(self):
        open("test.txt", 'w')
        time.sleep(1)
        open("test2.txt", 'w')
        assert creation_date('test.txt') < creation_date('test2.txt')
        assert creation_date('test2.txt') == creation_date('test2.txt')


    def test_write_deploy_manifest(self):
        write_deploy_manifest("1.1",'2.43','.')
        ll = open('README','r').read()
        l = ll.split('\n')
        assert '2.43' in l[1]
        assert '1.1' in l[0]
