#!/bin/bash
VERSION=7.2.5
[[ "${TERM}" != "dumb" ]] && cd
if [[ "${1}" == "-v" ]]; then
        shift
        VERSION="${1}"
        shift
fi
if [[ "${1}" == "" ]]; then
  FXE_VERSION="6.0.0.Master-SNAPSHOT"
  FXG_VERSION="7.1.0.Master-SNAPSHOT"
  FXF_VERSION="3.0.0.Master-SNAPSHOT"
  FXS_VERSION="2.1.0-SNAPSHOT"
else
  echo "explicitly searching for ${1}"
  FXE_VERSION="${1}"
  FXG_VERSION="${1}"
  FXF_VERSION="${1}"
  FXS_VERSION="${1}"
fi
echo "PATH=`pwd`"
echo "[workarea=`pwd`]"
set +xv
#ls fdeploy
if [[ ! -d fdeploy ]]; then
   if [[ "${USER}" =~ [0-9]+$ ]]; then
      mkdir fdeploy
   fi
fi
[[ -d fdeploy ]] && cd fdeploy
[[ -e 'install.py' ]] && rm 'install.py'
curl -fSL -R -o install.py "http://sefsmvn.ute.fedex.com/install.py"
if [[ "$FXE_VERSION" == *"git@"* ]] || [[ "${FXE_VERSION}" == *"https://"* ]]; then
  python install.py -v $VERSION "${FXE_VERSION}"
else
  python install.py -v $VERSION "FXE:com.fedex.sefs.core:fdeploy_FXE_CORE:${FXE_VERSION}:zip" "FXF:com.fedex.fxf.core:fdeploy_FXF:${FXF_VERSION}:zip" "FXG:com.fedex.ground.sefs:fdeploy_FXG_CORE:${FXG_VERSION}:zip" "FXS:com.fedex.sefs.common:fdeploy_FXS:${FXS_VERSION}:zip"
fi

#
# Installing the default development users
[[ ! -e ~/.ssh ]] && mkdir -p ~/.ssh && chmod -R 700 ~/.ssh
[[ -e dev_deploy_user_dsa ]] && cp dev_deploy_user_dsa ~/.ssh/dev_deploy_user_dsa && chmod 700 ~/.ssh/dev_deploy_user_dsa
[[ ! -e ~/.bashrc ]] && echo ". /etc/bashrc" > ~/.bashrc && chmod 755 ~/.bashrc
[[ ! -e ~/.bash_profile ]] && echo ". ~/.bashrc" > ~/.bash_profile && chmod 755 ~/.bash_profile

FILE=~/.bash_profile
LINE='alias 2="cd ~/fdeploy/; chmod 755 ./fdeploy.py && ./fdeploy.py menu -f config.FXS/*.json"'
grep -qF "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
LINE='alias 2e="cd ~/fdeploy/; chmod 755 ./fdeploy.py && ./fdeploy.py menu -f config.FXE/*.json"'
grep -qF "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
LINE='alias 2f="cd ~/fdeploy/;chmod 755 ./fdeploy.py && ./fdeploy.py menu -f config.FXF/*.json"'
grep -qF "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
LINE='alias 2g="cd ~/fdeploy/;chmod 755 ./fdeploy.py && ./fdeploy.py menu -f config.FXG/*.json"'
grep -qF "$LINE" "$FILE" || echo "$LINE" >> "$FILE"
LINE='alias fd="bash <(curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh)"'
grep -qF "$LINE" "$FILE" || echo "$LINE" >> "$FILE"

source ~/.bash_profile
echo "currently installed fdeploy version is ${VERSION}"
echo "shortcuts were installed for this session:"
echo " 2  = FXS fdeploy menu"
echo " 2e = FXE fdeploy menu"
echo " 2f = FXF fdeploy menu"
echo " 2g = FXG fdeploy menu"
echo " fd = install fdeploy"
[[ -d fdeploy ]] && cd fdeploy
