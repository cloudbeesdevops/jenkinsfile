#!/usr/bin/env python

import os
import sys
import getopt
import urllib2
import urlparse
import httplib
import shutil
import install
import itertools
import zipfile
import socket
import base64
import ssl
import shutil
import time
import re
import datetime
import logging
import xml.etree.ElementTree as ET
LOGGER = logging.getLogger('installer')


NEXUSv3='https://nexus.prod.cloud.fedex.com:8443'
NEXUSv2='http://sefsmvn.ute.fedex.com:9999'
download_and_unzip=True

def elvis(var, default, fd=None):
    value = default
    if fd is not None:
        try:
            value = os.environ[str(var)] if str(var) in os.environ.keys() else default
        except:
            value = default
        fd[var] = value
        return value
    else:
        return var if var is not None else default

def creation_date(path_to_file):
    """
    Try to get the date that a file was created, falling back to when it was
    last modified if that isn't possible.
    See http://stackoverflow.com/a/39501288/1709587 for explanation.
    """
    import platform
    if platform.system() == 'Windows':
        return os.path.getctime(path_to_file)
    else:
        stat = os.stat(path_to_file)
        try:
            return stat.st_birthtime
        except AttributeError:
            # We're probably on Linux. No easy way to get creation dates here,
            # so we'll settle for when its content was last modified.
            return stat.st_mtime

def __download(url, gavstring, fileName=None):
    def getFileName(url, openUrl):
        if 'Content-Disposition' in openUrl.info():
            # If the response has Content-Disposition, try to get filename from it
            cd = dict(map(
                lambda x: x.strip().split('=') if '=' in x else (x.strip(), ''),
                openUrl.info()['Content-Disposition'].split(';')))
            if 'filename' in cd:
                filename = cd['filename'].strip("\"'")
                if filename:
                    return filename
        # if no filename was found above, parse it out of the final URL.
        return os.path.basename(urlparse.urlsplit(openUrl.url)[2])
    r = None
    try:
        request = urllib2.Request(url)
        #if "https://" in url:
        try:
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE
            r = urllib2.urlopen(request, context=ctx)
        except Exception as e:
            try:
                r = urllib2.urlopen(request)
            except:
                return None

        modified = re.sub(r'\s\w+$','',r.info().getheaders('Last-Modified')[0])
        meta_modifiedtime = time.mktime(datetime.datetime.strptime( modified, "%a, %d %b %Y %X").timetuple())
        #base64string = base64.b64encode('%s:%s' % ('sefsdev', 'sefsdev'))
        #request.add_header("Authorization", "Basic %s" % base64string)
        fileName = fileName or getFileName(url, r)
        with open(fileName, 'wb') as f:
            shutil.copyfileobj(r, f)
        os.utime(fileName, (meta_modifiedtime, meta_modifiedtime))
    except urllib2.HTTPError as err:
        install.LOGGER.warn("failed to download: (reason:%s)" % (err))
        install.LOGGER.warn("failed to download: %s" % (gavstring))
        return None
    finally:
        if r:
            r.close()
    return fileName

def parse_source_repo_url(gav_giturl):
    fd_release_tag='master'
    fd_project_dir=None
    g=gav_giturl.split(':')
    install.LOGGER.info("g %s, gs=%s",g, gav_giturl)
    if 'git@' in g[1] or 'gitlab@' in g[1] or 'https://' in g[1]:
        fd_project_dir = g[0]
        gav_giturl = gav_giturl.replace("%s:" % g[0],'')
    install.LOGGER.info("url= %s, project_dir=%s" %(gav_giturl, fd_project_dir))
    if ('->' in gav_giturl):
        fd_release_tag = gav_giturl.split('->')[1].strip()
        if fd_project_dir is None:
            fd_project_dir = gav_giturl.split('->')[0].strip()

    if fd_project_dir is None:
        fd_project_dir = gav_giturl

    __fd_project_url = fd_project_dir.split('/')
    fd_project_dir = "config.%s" % __fd_project_url[len(__fd_project_url)-1].replace('.git','')
    install.LOGGER.info("checkout to %s" % (fd_project_dir))
    print fd_project_dir, fd_release_tag
    return fd_release_tag,fd_project_dir, gav_giturl.split('->')[0].strip()

def parse_nexus_url(fd,gavString):
    tarr = "%s" % (gavString)
    arr = tarr.strip().split(':')
    arr.reverse()
    project = arr.pop()
    arr.reverse()
    fdrepo = fd['FD_REPO']
    if '-SNAPSHOT' in gavString and fdrepo == 'releases':
        fdrepo = 'snapshots'
    return fdrepo, arr, project

def download_metadata_version(url,packaging,version):
    install.LOGGER.debug(" retrieve meta data  : [%s] %s" % (packaging,url))
    metadata_file = __download(url, url)
    if metadata_file is None:
        return None
    metadata_xml = open('very_Important.txt', 'r').read()
    return parse_metadata(metadata_xml,packaging,version)

def parse_metadata(metadata_xml,packaging,version):
    install.LOGGER.info(" retrieved meta data [%s]: %s" % (packaging,metadata_xml))
    lines=[]
    tree = ET.fromstring(metadata_xml)
    resolved = version
    res=[]
    for versions in tree.findall('.//snapshotVersion' ):
        i=0
        #install.LOGGER.debug(versions)
        __version = versions.find('value').text
        install.LOGGER.debug(__version)
        __pack = versions.find('extension').text
        #install.LOGGER.info("pa k: %s / %s -- eq = %s" % ( __pack, packaging, __pack  == packaging))
        install.LOGGER.debug(" resolved from %s to %s", __version, resolved )
        res.append(__version)
        if __pack  == packaging:
            resolved = __version
    res = sorted(res)
    res.reverse()
    #resolved = res[0]
    return resolved

def gav_to_tuple( *argv):
    if (type(argv) == tuple):
        argv = list(itertools.chain.from_iterable(argv))
    argv.reverse()
    gavstring = ":".join(argv)
    install.LOGGER.info("--->%s" % (gavstring))
    groupId = argv.pop()
    artifactId = argv.pop()
    version = None
    packaging = None
    if len(argv) > 0:
        version = argv.pop()
        # if not '-SNAPSHOT' in version:
        #     repo = 'releases'
        # if "nexus.prod.cloud.fedex.com" in svr and 'SEFS-6270' not in repo:
        #     repo = "SEFS-6270-%s" % (repo)
    if len(argv) > 0:
        packaging = argv.pop()
    return groupId,artifactId,version,packaging


def download_url_builder(svr,groupId,artifactId,version,packaging):
    repo = 'snapshots'
    if not '-SNAPSHOT' in version:
        repo = 'releases'
    if "nexus.prod.cloud.fedex.com" in svr and 'SEFS-6270' not in repo:
        repo = "SEFS-6270-%s" % (repo)
    install.LOGGER.debug("--> repo %s -> %s:%s:%s:%s from %s" % (repo, groupId,artifactId,version,packaging, svr))
    resolved = version
    URL_PATTERN='%s/nexus/service/local/artifact/maven/redirect?r=%s&g=%s&a=%s&v=%s&p=%s'
    if "nexus.prod.cloud.fedex.com" in svr:
        # switch to rest v3 : https://help.sonatype.com/repomanager3/rest-and-integration-api/search-api#SearchAPI-SearchandDownloadAsset
        if "-SNAPSHOT" in version:
            META_DATA='%s/nexus/content/repositories/%s/%s/%s/%s/maven-metadata.xml' % (svr,repo, groupId.replace('.','/'), artifactId,version)
            if packaging is None:
                packaging = 'jar'
            resolved=download_metadata_version(META_DATA,packaging, version)
            if resolved is None:
                return None
            install.LOGGER.info("version=%s resolved to %s" % (version,resolved))
        URL_PATTERN='%s/nexus/repository/%s/%s/%s/%s/%s-%s.%s' %  (svr, repo, groupId.replace('.','/'), artifactId, install.elvis(version , 'LATEST'), artifactId, install.elvis(resolved, version), install.elvis(packaging, 'zip'))
    else:
        URL_PATTERN= URL_PATTERN % (svr, repo, groupId, artifactId, install.elvis(version , 'LATEST'), install.elvis(packaging, 'zip'))
    return URL_PATTERN

''' download '''
def download(__svr, repo, *argv):
    if download_and_unzip == False:
        return 'dummy.txtr','version'
    outputFile = None
    # Nexus v2
    groupId,artifactId,version,packaging=gav_to_tuple(argv)
    gavstring=":".join([packaging,version,artifactId,groupId])
    #print __svr, "servers"
    for svr in __svr.split(','):
        install.LOGGER.info(" querying %s" % (svr))
        URL_PATTERN = download_url_builder(svr,groupId,artifactId,version,packaging)
        if URL_PATTERN is None:
            continue
        outputFile = install.elvis(outputFile, "%s.%s" % (install.elvis(artifactId, 'ARTIFACT'),install.elvis(packaging, 'zip')))
        install.LOGGER.debug("---> download source %s" % (URL_PATTERN))
        outputFileNEW = __download(URL_PATTERN, gavstring)
        if outputFileNEW:
            if os.path.exists(outputFile):
                print "created %s = %s" % (outputFile,creation_date(outputFile))
                print "created %s = %s" % (outputFileNEW,creation_date(outputFileNEW))
                if creation_date(outputFileNEW) > creation_date(outputFile):
                    print "created %s = %s" % (outputFileNEW,creation_date(outputFileNEW))
                    outputFile = outputFileNEW
            else:
                outputFile = outputFileNEW
    return outputFile, version

def subject_download(fd, gavString):
    '''com.fedex.sefs.common:sefs_silverControl'''
    install.LOGGER.debug("pull deployment data from")
    install.LOGGER.debug("\t%s ", gavString)
    install.LOGGER.debug("\t%s ", fd['FD_SVR'])
    if "git@" in gavString or "@gitlab" in gavString:
        install.LOGGER.info("\n\n")
        install.LOGGER.info("pull deployment data from %s with %s" % (gavString,fd['FD_SVR']))
        return subject_pull(fd,gavString)
    else:
        install.LOGGER.info("\n\n")
        install.LOGGER.info("download deployment data from nexus as %s with %s" % (gavString,fd['FD_SVR']))
        return subject_nexus(fd,gavString)

def subject_pull(fd,gav_giturl):
    fd_release_tag, fd_project_dir,giturl = parse_source_repo_url(gav_giturl)
    if os.path.exists(fd_project_dir):
        os.system("rm -fr %s" % (fd_project_dir))
    proc = os.system("set -xv && git clone -b %s %s %s" %(fd_release_tag, giturl,fd_project_dir))
    return { fd_project_dir : fd_project_dir}, fd_release_tag

def subject_nexus(fd, gavString):
    fdrepo, arr, project = parse_nexus_url(fd, gavString)
    fid, version = download(fd['FD_SVR'], fdrepo, arr)
    if fid:
        return {"config.%s" % (project): fid}, version
    else:
        return None

def unzip(fid):
    # no unzipping
    if download_and_unzip == False:
        return
    install.LOGGER.info("----> unpack: %s -> %s" % (fid.keys()[0], fid[fid.keys()[0]]))
    zip_ref = zipfile.ZipFile(fid[fid.keys()[0]], 'r')
    if fid.keys()[0] == '.':
        zip_ref.extractall()
    else:
        if os.path.exists(fid.keys()[0]) == True:
            shutil.rmtree(fid.keys()[0])
        zip_ref.extractall()
        if os.path.exists('config') == True:
            shutil.move('config', fid.keys()[0])
    zip_ref.close()

def write_deploy_manifest(fdversion, deployversion, configdir):
    a = "%s/README" % (configdir)
    with open(a, "w") as myfile:
        myfile.write("FDEPLOY=%s\n" % (fdversion))
        myfile.write("RELEASE=%s\n" % (deployversion))

def main(argv=None):
    os.environ['https_proxy'] = 'https://internet.proxy.fedex.com:3128'
    os.environ['http_proxy'] = 'http://internet.proxy.fedex.com:3128'
    logging.basicConfig(level=logging.INFO)
    stripped = lambda s: "".join(i for i in s if 31 < ord(i) < 127)
    if argv is None:
        argv = sys.argv
    #install.LOGGER.info("args=%s" % (str(argv)))
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hnv:s:dqt:")
    except getopt.error, msg:
        print "Usage: install.py [-v version] [-h]\n%s\n-v [version]  - version of fdeploy (default=LATEST)\n-q   - quiet\n-d   - debugging\n-h|--help      - this text" % (
            msg)
        print >>sys.stderr, msg
        print >>sys.stderr, "for help use --help"
        return 2
    fd = {}
    filesToUnpack = []
    elvis('FD_SOURCE', 'nexus', fd)
    elvis('FD_SVR', NEXUSv3, fd)
    elvis('FD_USER', 'sefsdev', fd)
    elvis('FD_PASS', 'sefsdev', fd)
    elvis('FD_REPO', 'releases', fd)
    elvis('FD_MODULE', './fdeploy/src/main/resources', fd)
    elvis('PACKAGING', 'zip', fd)
    # process options
    fdeploy_version = "7.1.0-SNAPSHOT"
    fdeploy_repo = 'snapshots'
    fdeploy_base_install = True
    target_dir_readme = None
    #install.LOGGER.info("FD=${fd}")
    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
        elif o in ("-s"):
            fdeploy_repo = 'snapshots'
        elif o in ("-t"):
            target_dir_readme = a
        elif o in ("-q"):
            LOGGER.setLevel(logging.WARNING)
        elif o in ("-n", "--no-base"):
            fdeploy_base_install = False
        elif o in ("-nd", "--no-download"):
            download_and_unzip = False
        elif o in ("-d", "--debug"):
            LOGGER.setLevel(logging.DEBUG)
        elif o in ("-v", "--version"):
            fdeploy_version = stripped(a)
            if '-SNAPSHOT' not in fdeploy_version:
                fdeploy_repo = 'releases'
    if fdeploy_base_install == True:
        FDEPLOY = "xopco.common.tools:xopco-common-tools-fdeploy:%s:zip" % (fdeploy_version)
        if fdeploy_version.startswith("6."):
            FDEPLOY = "com.fedex.sefs.common:sefs_silverControl:%s:zip" % (fdeploy_version)
            # fdeploy_repo = 'SEFS-6270-%s' % (fdeploy_repo)
            # elvis('FD_REPO', fdeploy_repo , fd)
            elvis('FD_SVR', "%s" % (NEXUSv2) , fd)
        gav_fdeploy = FDEPLOY.split(':')
        install.LOGGER.info("retrieved fdeploy version: %s -> %s" % (
            gav_fdeploy, fdeploy_repo))
        unzip(
            {'.': download("%s" % (fd['FD_SVR']), fdeploy_repo, gav_fdeploy)[0]})
        install.LOGGER.info("\n\n")

    for gav_download_artifact in args:
        if fdeploy_version.startswith('7.'):
            elvis('FD_SVR', "%s,%s" % (NEXUSv3,NEXUSv2), fd)
        else:
            elvis('FD_SVR', "%s,%s" % (NEXUSv2,NEXUSv3), fd)
        try:
            fid,version = subject_download(fd, gav_download_artifact)
            if fid:
                if not ("git@" in gav_download_artifact or "@gitlab" in gav_download_artifact):
                    unzip(fid)
                if target_dir_readme is None:
                    target_dir_readme = fid.keys()[0]
                write_deploy_manifest(fdeploy_version, version, target_dir_readme )
                print "SUCCESS to process %s" % (gav_download_artifact)
        except:
            install.LOGGER.critical("FAILED to process %s" % (gav_download_artifact))
            pass
        install.LOGGER.info("\n\n")


if __name__ == "__main__":
    sys.exit(main())
