RELEASE NOTES:

Preferred installation method with python virtualenv:

```shell
bash <( curl -s http://sefsmvn.ute.fedex.com/virtualenv.sh ) -v 8.1.10 git@gitlab.prod.fedex.com:APP73541/deploy.git
```

([deprecated installation instructions](#depr))


### 8.1.10 - pcf quoting
| issue | description |
|-----|----|
| pcf  | #71 double quote service names in pcf cups creation |
| pcf  | #62 start stop does not require download |
| pam  | #69 enable retry logic for pam to lessen the chances of failure |
| pcf  | #73 feature to specify inline cf commands and adding create, delete, bind and rebind for autoscaler |


### 8.1.9 - hotfix adding machine_agent_appd restart.
| issue | description |
|-----|----|
| appd | #62 adding machine agent restart for appd for Java_auto/Java_sudo/BE/tomcat/AS/AS_leech/ deployments  |

### 8.1.8 - hotfix sslVerification Error on pam requests.
| issue | description |
|-----|----|
| pam | disabling sslVerify on the pam requests module |
| post-install | changed 58080 py server to 8081 on c0009869.test.cloud.fedex.com |

### 8.1.7 - fixed import reference (Aug 21, 2019)
| issue | description |
|-----|----|
| imports | incompatible imports for absolute_path that moved to fnexus 1.1.0 |


### 8.1.6 - double script generation (Jul 25, 2019)
| issue | description |
|-----|----|
| fdeployloader | #57 the loader is creating many scripts with different content types |
| binary cleanup | #53 the ability to persist certain versions in the cleanup by manifest |



### 8.1.5 - overcalculating (Jul 25, 2019)
| issue | description |
|-----|----|
| cloudbees | #54 overcalculation for 4 digits fix and removed dependency on fdeploy nexus resolver |



### 8.1.4 - issue with cloudbees
| issue | description |
|-----|----|
| cloudbees | #54 calculation for 4 digits fix |
| clean | first iteration for clean up |


### 8.1.4 - cleanup binaries with fdeploy based on manifest
| issue | description |
|-----|----|
| pcf | #55 clean up binaries based on the manifest [PERSIST_MANIFEST] section in the release_manifest.mf |
| deps | #56 resolved circular dependencies virgin install without pyc |

### 8.1.3 - issue with control stop on pcf
| issue | description |
|-----|----|
| pcf | #54 omit downloading for pcf stop |


### 8.1.1/2 - upgrade fnexus package for sdom calculation
| issue | description |
|-----|----|
| calculation | multidigit calculation refactored.  |



### 8.1.0 - blue green deployment external control
| issue | description |
|-----|----|
| blue green| split up blue green deployment in pieces so it can be controlled externally  #51|
| commmon | ability to select id from list of common domain deployments. #50  |

### 8.0.2 - hotfix control menu
| issue | description |
|-----|----|
| menu | AttributeError: 'Namespace' object has no attribute 'group_size'  |


### 8.0.1 - adding ENC() support for name value pairs
| issue | description |
|-----|----|
| enc() | see #49, enabling encryption on env_map fields with ENC() notation in cupsCredentials() |
| postinstall | improved the path notations for post install. |
| enc() | fix for RTV ENC() mappings causing type error. |


### 8.0.0 - code coverage above 80% - setuptools / bluegreen deployment / common deployments / credential env mappings
| issue | description |
|-----|----|
| jasypt | see #36, enabling encryption on RTV fields with ENC() notation |
| jasypt | see #36, enabling encryption on password fields for pcf cups with ENC() notation |
| silver | removed unused methods / calls to get code coverage above 70%  |
| network | removing / skipping tests with dependency on the FedEx network |  
| fdeploy | adding the `test -a encryption` feature for support to encrypt and decrypt values to support #36 |
| pytest | adding annotation for detecting cryptology and in/out fedex network state |
| source |removing unused variables and imports with autoflake |
| package | removed unused packages and relying on setup.py for managing dependencies |
| setup.py | changing packaging to native pip packaging which includes infrastructure on http://urh00604.ute.fedex.com:8080 to support this, eventually we will publish these packages to Nexus |
| fnexus | extracting the nexus resolver out of fdeploy for usage elsewhere and making focussing on smaller releasable items |
| bluegreen | Enhancing blue-green deployment script |
| bluegreen | Modified the condition for app health check while doing blue green deployment. |
| calculate | Calculate next version using cloudbees.py for jenkins |
| bluegreen | Retry mechanism for each pcf command |
| bluegreen | Condition to check Available memory in the org to proceed with the deployment |
| ldd | Fixied LDD pubish and unpublish issues with below error( TypeError: queue_query_request() takes exactly 5 arguments (8 given)) |
| menu | TypeError: 'module' object is not callable while using menu command in fdeploy. |
| ldd | fixed the LDD publish for Tibco_bw extractor if we have one istances in targets |
| java |Deploy action is not performing start action for java applications|
| cups | adding additional env mappings for credentials |


### 7.2.9 - fixing target spec refactoring
| issue | description |
|-----|----|
| tibco | fixing target_specs causing _target_path to dissapear see #41|


### 7.2.8 - target spec refactoring + pcf command changes
| issue | description |
|-----|----|
| pcf | see #40 |
| tibco | target_specs causing _target_path to dissapear |


### 7.2.7 - hotfix tibco_bw/bw2/be
| issue | description |
|-----|----|
| user | user overwrite None |


### 7.2.6 -
| issue | description |
|-----|----|
| issue-37 | blue green deployment  |
| issue-38 | pcf-cups fixes and enhancements |

### 7.2.5 - hotfix cloudbees
| issue | description |
|-----|----|
| calculator | pointing to nexus corp for latest version information (single source of truth) |

### 7.2.4 - cloudbees issue && pytest refactoring
| issue | description |
|-----|----|
| calculator | suppressing stdout/stderr |
| issue | description |
|----|----|
| unittest | switches to all pytest framework |

### 7.2.3 - pcf issues (Tue Feb 5, 2019 18:20 MDT)
| issue | description |
|----|----|
| pcf-cups | issue with manifest disconnect #48dcb7a8 |
| fdeploy | target filtering based on regex #34 |
| installer | added installer test cases after type in external script # 38eda7f6 |

### 7.2.0/1/2 - Rann Release Stabilization pcf (Tue Jan 29, 2019 16:25 MDT)
| issue | description |
|----|----|
| pcf-cups | altered the uups and cups invocation  |
| pcf-cups | adding restage function when infrastructure is attached to a component |
| pcf-cups | adding set-env and unset-env commands |
| pcf | fixing the double level indicators |

### 7.1.4/5/6 - Rann Release Stabilization pcf (Thu Jan 24, 2019 12:08 MDT)
| issue | description |
|----|----|
| pcf | omit deploy when action stage is used |
| pcf | Header snippet: The main changes were in the snippets with manifest interpolation + putting the blue green deployment back… |
| pcf | __init__.py RTV’s like APP_NM were overwritten in a second loop and reset.. which was related to level not prepending to the APP_NM |
| pcf | The deploy:stage for PCF type errored out trying to deploy whilst it should not invoke any deploying method since the staging is local to the build (ie we do not need an extra step to scp code base in a directory on a remote system. |
| coverage |  Added code coverage generation for Sonar and Jenkins for Python projects (different from Java) |
| unittest |  Switched the testing framework from unittest to pytest which take advantage of many different forms of reporting and code coverage. |


### 7.1.3 - adding README info (Tue Jan 8, 2019 13:12 MDT)
| issue | description |
|----|----|
| pcf-cups | adding the cupsScaleClass |
| pcf | adding MANIFEST_FILE overwrite function |


### 7.1.2 - adding README info (Thu Dec 20, 2018 13:12 MDT)
| issue | description |
|----|----|
| pcf-cups | expanding cups validation and adding uups |
| platform | adding README and fdeploy log to java deployments for production auditing |


### 7.1.1 - adding PAM support (Thu Dec 20, 2018 13:12 MDT)
| issue | description |
|----|----|
| pcf-cups | expanding cups validation and adding uups |
| hooks | fixed request ldd into list call |
| autonumber | fixed auto number on initial load |


### 7.1.0 - adding PAM support (Wed Oct 24, 2018 17:11 MDT)
| issue | description |
|----|----|
| pcf-pam | integrating PAM into the pcf module with PAM_ID as mandatory item in json |
| pcf-ldd | ablitiy to push pcf information to LDD |
| firewall | adding firewall request generator logic |
| hooks | externalizing hooks like mattermost or ldd from main logic |
| pcf-cups | user provider services integration with fdeploy |
| calculator | fixed order of version calc in right order v3 and then v2 |


### 7.0.11 - fixing snapshot downloads across multiple repos (Fri Oct 19, 2018 13:44 MDT)
| issue | description |
|----|----|
| install | scan Nexusv3 and NexusV2 for snapshot version and take creation date for calculation |

### 7.0.10 - fixing early break in exact versioning (Thu Oct 18, 2018 17:48 MDT)
| issue | description |
|----|----|
| nexusresolver | scan Nexusv3 and NexusV2 for exact version calculation and not returning None |


### 7.0.9 - fixing incrementor (Wed Oct 17 13:50:58 MDT)
| issue | description |
|----|----|
| cloudbees.py | scan Nexusv3 and NexusV2 for next version calculation |


### 7.0.7 - fixing incrementor (Mon Oct 15 14:47:58 MDT)
| issue | description |
|----|----|
| cloudbees.py | changing the order to retrieve last version for incrementor starting with Nexusv3 |


### 7.0.4 - fixing the mavenmetadata xml  (Sat Oct 13 16:58 MDT)
| issue | description |
|----|----|
| metadata | fixing the Nexus v3 url for snapshot meta data retrieval  |


### 7.0.3 - install.py failure to unpack (Tue Sep 14 10:58 MDT)
| issue | description |
|----|----|
| unpack deployment items | install.py failure to unpack deployment items from nexus after successful download  |



### 7.0.2 - cloudbees.py numbering defect (Fri Sep 14 18:52 MDT)
| issue | description |
|----|----|
| cloudbees | problem with virgin installs getting back the base number |


### 7.0.1 - Nexus v2 and v3 auto-detect
| issue | description |
|----|----|
| auto-detect | Enabling artifact resolving from two or more repositories v2 and v3 |


### 7.0.0 - Nexus v2 and v3 extended / Silver Retirement
| issue | description |
|----|----|
|install | Addressed changes in the installation process for Nexus v3 to work |
| retire | Retiring all silver scripts components and renaming the package to standard CM naming |

### 6.2.5 - Nexus v2 and v3
| issue | description |
|----|----|
|nexusv3 | After migration of corporate nexus to v3.x PRO the download urls by means of REST are not working, refactored nexusresolver. |


### 6.2.4 - Analytics Integration

| issue | description |
|-----|-----|
| analytics | integration hooks with Google Analytics  |
| BW | multiple issues resolved related to `tibco_bw` and `tibco_bw2`. |


### 6.2.3 - Refactored Calculation and added more testing


| issue | description |
|-----|-----|
| incrementor | the calculation rules would be distorted based on the type of resolving that was done. Splitted it up in an next version track and manifest track. |
| fixed FXG publishing | addressed issue in lddpublisher to make the opco dynamic based on the configuration loaded. |
| snapshot resolving | fixed the snapshot resolving of meta data |
| LDD BW registration | improved the BW way to look up the ps signature to pickup BW processes. |
| smoketests | added remote invocation of smoke and regression tests from the pipeline to integrate with fdeploy |

### 6.2.2 - Tangosol Java_Auto Fix

| issue | description |
|-----|-----|
| tangasol | `java_auto` TANGASOL not starting up, or not seeing the TANGASOL parameters on the startup footprint of the java process.   |

### 6.2.1 -

| issue | description |
|-----|-----|
| number increments | aligned the cloudbees numbering incrementor and resolver + test cases |
| manifest resolving | fixes manifest resolve issue and fixed the bulk test on Jenkins  |


### 6.2.0 -

| issue | description |
|-----|-----|
| bw_level_fix | removed escaping level variable in the root remote shells to prevent problems with level specific operations |
| PCF | added blue green deployment |


### 6.1.104 - PCF routing, BW naming, Java_auto tangasol (May 31st, 2018)

| issue | description |
|-----|-----|
| 6.1.43 | Issue with log4j.xml replacement in suStop BE engine |
| 6.1.24 |  adding target selection when doing deploy -> deploy |
| 6.1.41 | 41-pcf-routing-host-naming-has-wip-in-the-naming-and-should-be-removed |
| 6.1.34 | 34-adding-corporate-load-environments-to-all-options-in-silvercontrol |
| 6.1.36 | 36-bw-engine-is-getting-over-ridden-while-deployment-on-l3-and-is-running-on-new-tlms-as-per-cl3 |
| 6.1.44 | 44-java_auto-hostname-replacement-takes-the-build-host-not-the-runtime-host |

### 6.1.103 - release MTP Java_auto release (May 16th,2018)

| issue | description |
|-----|-----|
| 6.1.26 | 26-deploy-wipeout-option-not-working-with-f-deploy'  |
| adhoc | fixing java_auto snippet EAINUMBER to EAI_NUMBER |
| 6.1.37 | 37-pcf-fdeploy-all-applications-in-a-space-are-getting-deleted-when-we-use-deploy-clean |
| 6.1.38 | 38-pcf-fdeploy-deploy-install-of-an-application-also-starts-the-apps-replicating-the-functionality-of-deploy-deploy |
| 6.1.39 | 39-to-capture-user-information-while-deployments-in-deployment-history-table |
| 6.1.40 | 40-pcf-fdeploy-redeploying-an-existing-application-on-pcf the-newly-deployed-application-gets-deployed by-a-different-name-from-the-json-defined-app_nm |

### 6.1.102 - release (May 11th, 2018)

Includes:

| issue/feature | description |
|-----|-----|
| 6.1.31 | https://gitlab.prod.fedex.com/bananaman/fdeploy/issues/31 : filecopy type for BW log4j.xml distribution |
| 6.1.36 | BW in stacked environment cannot be artifactId but must be unique for each instance. |
| patch-1 | filecoyp fix |
| 6.1.33 | java_auto-changes-for-rely-jvm-controls + deploy:properties command introduction |
| pcf    | pcf generator introduction |
| 6.1.8  | fdeploy-relink-option-not-working-for-bash-deploys-since-at-before-6-0-21 |
| 6.1.18 | remove 18-adding-a-previous-link-when-relinking-and-application |
| 6.1.29 | 29-unable-to-create-stdout-log-and-stderr-log-while-starting-up-be-applications-in-l2-for-fxf |
| 6.1.27 | Unable to pass this ENGINE_USERNAME in case of AS member name in the RTV section of the JSON file for L4 level onwards |




<a name="depr">Deprecated installation instructions:</a>

```shell
bash <(curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh) -v 8.1.8 "FXE:com.fedex.sefs.core:fdeploy_FXE_CORE:1.0.55" "FXF:com.fedex.fxf.core:fdeploy_FXF:3.0.15-SNAPSHOT"
bash <(curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh) -v 8.1.8 git@gitlab.prod.fedex.com:APPxxxx/deployment_items.git
# getting specific release branch use
bash <(curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh) -v 8.1.8 'git@gitlab.prod.fedex.com:APPxxxx/deployment_items.git->development'
```
After running the statement above of the base install of `fdeploy` one must install its required dependencies with the `post-install` step. The post install will install the python package manager (pip) and install all required dependencies plus binaries for the encrypting.

```bash
. post-install.sh
```
