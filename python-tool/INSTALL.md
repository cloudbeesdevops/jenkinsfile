# Installation Steps performing a release 

*  Merge feature branch into master 
*  update the README.md in master with the upcoming version and what feature was added
*  goto master branch in Jenkins (https://jenkins-shipment.web.fedex.com:8443/jenkins/job/3534596-FDEPLOY/job/XOPCO_COMMON_TOOLS_FDEPLOY/job/master/)
*  under "Build With Parameters", select [X] PERFORM_RELEASE_CUT
*  next goto

```
ssh $USER@uwb00078.ute.fedex.com
sudo su - tibco
cd webroot
vi fdeploy-install.sh
# change the version number on line #1
```

