
# Post Install Step

Earlier this week FDeploy 8.0.0 was released.  This has a bunch of enhancements which you can read about HERE  It also is the first release cut that has the password encryption support I’ve been asking for.  So, I pushed an update to our pull_fdeploy_for_UUID.sh script.  If you use it with no parameters now, it will pull 8.0.0 by default.   
You can add 7.2.9 on the command line to drop back to the last version of FDeploy that you’ve probably been using if desired.  Assuming you go with the latest at some point, please see the PRE and POST (in yellow) below.  
You have to add some settings to your .bashrc.  After pulling 8.0.0, you have to source in the post-install.sh at least once.  
You don’t have to run post-install.sh every time, just once successfully. 
 
``` 
[nr336731@c0004535-cvdda01 ~]$ ./pull_fdeploy_for_UUID.sh
 
********************************************************************************
FDEPLOY ENVIRONMENT REQUIREMENTS - make sure these are set first in your .bashrc
********************************************************************************
 
export https_proxy=http://internet.proxy.fedex.com:3128
export http_proxy=http://internet.proxy.fedex.com:3128
 
ADD TO BEGINNING OF PATH             ${HOME}/.local/bin
ADD TO BEGINNING OF LD_LIBRARY_PATH  ${HOME}/.local/lib
ADD TO BEGINNING OF PYTHONPATH       /var/tmp/tools/usr/lib64/python2.7/site-packages/:${HOME}/.local/lib/python2.7/site-packages
 
```

fdeploy-install.sh reports this default = VERSION=7.2.9
 
``` 
Pulling FDeploy version we requested from cmd line or our default version = 8.0.0
 
(…snip…)
 
********************************************************************************
FDEPLOY 8.0.0 (or later) POST INSTALL REQUIREMENTS - make sure you run this once
********************************************************************************
cd ~/fdeploy; . ./post-install.sh
```


# Development Setup

Set your `.bashrc` paths:

### RedHat el7

For `uname -r` version `3.10.0-327.22.2.` `el7` `.x86_64` : *el7*

```
export PATH=/opt/fedex/jenkins/local/bin:/opt/fedex/jenkins/.local/bin:${PATH}:/home/tibco/bin
export LD_LIBRARY_PATH=/opt/fedex/jenkins/local/lib/
export PYTHONPATH=/var/tmp/tools/usr/lib64/python2.7/site-packages/:/opt/fedex/jenkins/.local/lib/python2.7/site-packages
```

Test

```
git clone https://gitlab.prod.fedex.com/APP3534596/xopco-common-tools-fdeploy.git
cd xopco-common-tools-fdeploy/src/main/python
pytest
========================================================== test session starts ===========================================================
platform linux2 -- Python 2.7.16, pytest-4.4.0, py-1.8.0, pluggy-0.9.0
rootdir: /tmp/xopco-common-tools-fdeploy/src/main/python
plugins: cov-2.6.1
collected 222 items
.....
```

### RedHat el6

For `uname -r` version `2.12.1-27.41.2.` `el6` `.x86_64` : *el6*

```
export PATH=/opt/fedex/jenkins/local/bin:/opt/fedex/jenkins/.local/bin:${PATH}:/home/tibco/bin
export LD_LIBRARY_PATH=/opt/fedex/jenkins/local/lib/
export PYTHONPATH=/var/tmp/tools/usr/lib64/python2.6/site-packages/:/opt/fedex/jenkins/.local/lib/python2.7/site-packages
```


# INFRASTRUCTURE SETUP


```
export https_proxy=http://internet.proxy.fedex.com:3128
export http_proxy=http://internet.proxy.fedex.com:3128
```

 *  Local python2.7 installation (https://www.python.org/downloads/source/)

In this case installing it under `/opt/fedex/tibcosilver/`
```
./configure --prefix=/opt/fedex/tibcosilver/local --enable-optimizations --enable-shared LDFLAGS="-Wl,--rpath=/usr/lib64,--rpath=/opt/fedex/tibcosilver/local"
make install
```

 *  Installing Crypto Packages

Extracting the rpm stored in Nexus to `/var/tmp/tools/` which will create `/var/tmp/tools/usr/lib64/python2.[6|7]/site-packages/` area

```
  PACKAGE=$(echo $(uname -r) |  awk -F. '{ print "https://nexus.prod.cloud.fedex.com:8443/nexus/repository/SEFS-6270-releases/python-crypto/python-crypto/2.6.1-1."$6"."$7"/python-crypto-2.6.1-1."$6"."$7".rpm" }')
set -xv
  curl -o python-crypto.rpm  "${PACKAGE}"
  rpm2cpio python-crypto.rpm | cpio -idv
  rm python-crypto.rpm
```

  *  Change `.bashrc` according to *el6* or *el7* 

  *  Install Python Package Manager (pip)

```
  export GETPIP=https://bootstrap.pypa.io/get-pip.py
  [[ `python --version 2>&1 ` == *2.6* ]] && export GETPIP=https://bootstrap.pypa.io/2.6/get-pip.py
  export https_proxy=http://internet.proxy.fedex.com:3128
  export http_proxy=http://internet.proxy.fedex.com:3128
  curl -v -O "${GETPIP}"
  python get-pip.py --proxy="http://internet.proxy.fedex.com:3128"
```
  
  * Install Base Packages 
  
```
chmod -R 755 /opt/fedex/tibcosilver/.local
pip install six importlib python-dateutil purest pytest-cov
```
