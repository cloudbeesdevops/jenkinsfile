#!/bin/bash
export http_proxy=http://internet.proxy.fedex.com:3128
export https_proxy=http://internet.proxy.fedex.com:3128
export PATH=$PATH:$HOME/.local/bin
export LD_LIBRARY_PATH=$HOME/.local/lib/
export PYTHONPATH=/var/tmp/tools/usr/lib64/python2.6/site-packages/:$HOME/.local/lib/python2.7/site-packages

clean() {
  cd $HOME ; 
  for i in virtualws fdeploy .local usr
  do 
    [[ -d $i ]] && rm -fr $i
  done
}

install_pip() {
  curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
  python get-pip.py --user
  rm -fr get-pip.py
}
install_cryptology() {
  [[ ! -e /var/tmp/tools ]] && mkdir -p /var/tmp/tools
  cd /var/tmp/tools
  export http_proxy=http://internet.proxy.fedex.com:3128
  export https_proxy=http://internet.proxy.fedex.com:3128
  PACKAGE=$(echo $(uname -r) |  awk -F. '{ print "https://nexus.prod.cloud.fedex.com:8443/nexus/repository/SEFS-6270/python-crypto/python-crypto/2.6.1-1."$6"."$7"/python-crypto-2.6.1-1."$6"."$7".rpm" }')
  set -xv
  curl -o python-crypto.rpm  "${PACKAGE}"
  rpm2cpio python-crypto.rpm | cpio -idv
  rm python-crypto.rpm
}

clean
[[ -z `pip --version` ]] && install_pip
[[ -z `virtualenv --version` ]] && pip install virtualenv --user
if [[ ! -e /var/tmp/tools/usr/lib64/python2.7/site-packages/Crypto ]] && [[ ! -e /var/tmp/tools/usr/lib64/python2.6/site-packages/Crypto ]]; then
  install_cryptology
fi

CWD=`pwd`
mkdir -p $HOME/virtualws
virtualenv $HOME/virtualws/fdeploy
cd $HOME/virtualws/fdeploy/bin
chmod +x $HOME/virtualws/fdeploy/bin
. $HOME/virtualws/fdeploy/bin/activate
cd $HOME/virtualws/fdeploy
source bin/activate
pip install jasypt4py --no-deps
pip install pytest==4.6.5 pytest-cov==2.7.1

# installation of fdeploy 
bash <( curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh ) $@ 
mv $HOME/fdeploy/* $HOME/virtualws/fdeploy
find . -name '*.pyc' -delete
pip install -r requirements-dev.txt -i http://c0009869.test.cloud.fedex.com:8081 --trusted c0009869.test.cloud.fedex.com
#pip install pycrypto==2.6.1 --no-compile --no-deps
cd $HOME/virtualws/fdeploy && source bin/activate
python fdeploy.py --version

echo
echo "usage: cd $HOME/virtualws/fdeploy && source bin/activate"
echo
echo
echo