RELEASE NOTES

([site docs](http://sefsmvn.ute.fedex.com/site/sefs-jenkins-shared-library/sefs-jenkins-shared-library-5.0.0-SNAPSHOT/sefs-jenkins-shared-library/index.html))



### 5.2.9 - false quality gate failures sonar

| issue | description |
|----|----|
| #15  | adding version information to the project id so release branches do not get compared |


### 5.2.8 - merge CD-fixes into master for Australia Release

| issue | description |
|----|----|
| ?  | Support features for Australia |
| fdeploy | removed the hardcoding of FD_VERSION and use default installation |

### 5.2.7 - build disregarding saving space

| issue | description |
|----|----|
| #12 | adding build disregarder and rotation limit of 7  |


### 5.2.6 - Disable

| issue | description |
|----|----|
| disable | adding DISABLE_ITTEST and DISABLE_SMOKETEST for bypass |
| timeout | adding timout overwrite with env.TIMEOUT |

### 5.2.5 - Workflow cucmber testing

| issue | description |
|----|----|
| workflow | added workflow testing with cucumber to depict the path of execution |
| cobutera | added karma coverage for publishing |

### 5.2.4 - Restrict

| issue | description |
|----|----|
| restrict | restrict build jobs based on the APP_TYPE, if no `APP_TYPE` environment variable has been defined it defaults to nodes with labels `sefs` |
| tibco | added the RC workflow |
| workflow | added visual console output on what stages are calculated for the run in the build setup. |


### 5.2.3 - Tool Version Overwrite

| issue | description |
|----|----|
| tools | custom tool overrides for NodeJs and Maven with env vars. |
| tibco | fixed BE / BW workflow  |
| cleanup | do customized cleanup even when build fails |
| job_Type | added ST (smoke test) phase |


### 5.2.2 - Refactoring Pipeline For Release Management

| issue | description |
|----|----|
| workflow | splitting the tagging from the build cycle |
| uploader | have ability to push snapshot vs releases and manipulate the pom file |


### 5.2.1 - Refactoring Pipeline Triggers

| issue | description |
|----|----|
| fdeploy | adding the ability to set fdeploy with FD_VERSION |
| upload | adding ability to upload multiple items from different poms |
| libraryResource | loading python scripts from libraryResource and having unittests in resources for these scripts |
| sdom-parent-tool | helper function for generating parent poms for sdom models |
| ask | enabled `NO_AUTO_DEPLOY_QUESTION` environment variable to noADG via environment |
| workflow | refined the workflow in setWorkflow in Definitions and assertions |
| prompt | disable all CD steps if autodeploy gate times out |



### 5.2.0 - Refactoring Pipeline Triggers

| issue | description |
|----|----|
| refactoring | flag files and job type alignmeht for Autobahn |
| license | removed static code analysis for BW |
| timeout | increase timeout from 15 to 30min |

### 5.1.4 - CD Fixes

| issue | description |
|------|------|
| install.py | issue with install.py being overwritten |

### 5.1.3 - ????


### 5.1.2 - JenkinsfileTibco

| issue | description |
|------|------|
| tibco | changing Tibco JenkinsfileCICD to a declarative pipeline |


### 5.1.1 - SEFS Nexus Proxy fix

| issue | description |
|------|------|
| nexus | recently published artifacts cannot be retrieved or wrong version of snapshot is returned due to SEFS proxy not being refreshed after upload to Corp nexus |

### 5.1.0 - Autobahn V

|issue | descriptionn |
|----|----|
| notify | added Office 365 notification hooks for all domains |
| e2e | integrated the e2e testing stage |
| tibco | integrating tibco in declarative pipeline (phase 1)|


### 5.0.6 - Autobahn IV

|issue | descriptionn |
|----|----|
| notify | added Office 365 [SEFS-ST-CICD webhook](https://teams.microsoft.com/l/channel/19%3a7ee18be058c24f19ba059d45de26156c%40thread.skype/SEFS-ST-CICD?groupId=9f2bd5a1-809f-4fbb-afc6-f37377e61cb0&tenantId=b945c813-dce6-41f8-8457-5a12c2fe15bf)	 notification |
| notify | added build started Office 365 [SEFS-ST-CICD webhook](https://teams.microsoft.com/l/channel/19%3a7ee18be058c24f19ba059d45de26156c%40thread.skype/SEFS-ST-CICD?groupId=9f2bd5a1-809f-4fbb-afc6-f37377e61cb0&tenantId=b945c813-dce6-41f8-8457-5a12c2fe15bf) notification |
| ittest | further feature enhancement |
| notify | added Office 365 [SEFS Auto Deploy Gates webhook](https://teams.microsoft.com/l/channel/19%3a397adea4a6a14e52a0e06e2974659d75%40thread.skype/SEFS%2520Auto%2520Deploy%2520Gates?groupId=9f2bd5a1-809f-4fbb-afc6-f37377e61cb0&tenantId=b945c813-dce6-41f8-8457-5a12c2fe15bf) notification pointing to the autodeploy gate on Blue Ocean |
| notify | added Office 365 notification for the Autobahn TEAM Channel |
| schedule | changed the schedule for periodical build to once on Sunday |


### 5.0.5 - Autobahn III-fix

|issue | descriptionn |
|----|----|
| prepare | fix locking issue when nodejs is being used and prepare did not exclude node |
| tagging | changed the naming convention to prefix with 'R-' to avoid problems when tags and branches are named the same  |
| nexus | nexus upload builder fix for parent pom situations |
| settings | moved the settings.xml to libraryResource so it can be version controlled |
| ittest | concept finalization of the IntegrationTesting step |
| enforce | added `env.noEnforcer` check to disable the enforcer plugin. hook for parent pom releasing |

### 5.0.3/4 - Autobahn III

|issue | descriptionn |
|----|----|
| nexus | refactoring the nexus upload to be independent of maven or pom.xml |
| publish | dynamically compile the publishing of test data based on the project type  |
| test | added unit tests for the inline python code |


### 5.0.2 - Autobahn II

|issue | descriptionn |
|----|----|
| declarative | refactoring the main line including exposing testing in workflow |
| uploadToNexus | revamped the whole upload for single module where we now publish to a local repo and reap artifacts after testing |
| ldd-cicd-parser | more the ldd cicd parser to tool section thus saving execution time |

### 5.0.1 - Autobahn Effort Fix

|issue | descriptionn |
|----|----|
| deploy | spring boot repackage enabled |


### 5.0.0 - Autobahn Effort

|issue | descriptionn |
|----|----|
| flow | enabling flow control based on APP_TYPE, JOB_TYPE or overrides |
| docs | update to site docs, revamping |
| findbugs | added findbugs to quality measuring |
| detect | automatic test detectoin in pom.xml, if no tests are available execute them from command line pertaining to jacoco, findbugs, pmd. |
| smoketest | better interagtion for GTM calls and refined calls for specific scenario's |
| ldd | added the LDD publishing as part of the publish results so we collect them for all runs |
| sonar | refined buildSetup to auto detect multi module source / test directories |



### 4.2.5 - modified TIBCO pipeline to limit source stash (Fri Oct 26, 2018 - 09:06 MDT )
| issue | description |
|----|----|
| source | limiting the scope of source stash, excluding defaults and archives / node / target directories, causing instability if not omitted. |
| poms | stash poms for usage in the deploy step |
| nexus | remove the divertion to sefsmvn for TIBCO products |
| npm | skip npm in the deploy phase causing rebuild and locking issues |
| source | removed source unstashing after the Nexus fix |
| nexus | force all deployments to nexus with `-DaltDeploymentReleases=` |


### 4.2.4 - incrementor scan for fdeploy itself, installation process (Thu Oct 16, 2018 - 19:32 MDT )
| issue | description |
|----|----|
| installation | making fdeploy-install.sh and install.py detect the version that should be downloaded |
| deployatend | make deploy at end false |


### 4.2.3 - incrementor scan (Wed Oct 15, 2018 - 13:32 MDT )
| issue | description |
|----|----|
| cloudbees | letting the incrementor for version calculation scan both NexusV3 and SEFS NexusV2  |


### 4.2.2 - moved java pipeline to publish to sefsmvn (Tue Oct 14, 2018 - 16:32 MDT )
| issue | description |
|----|----|
| publish | moved JAVA publising  back to standard publishing to SEFS Nexus  |

### 4.2.1 - remove stopgap and put in firewall enabled publishing (Tue Oct 14, 2018 - 16:02 MDT )
| issue | description |
|----|----|
| publish | moved back to standard publishing to Corp Nexus  |


### 4.2.0 - upgrade to  (Sun Oct 14, 2018 - 16:02 MDT )
| issue | description |
|----|----|
| fdeploy | upgrade to 7.0.8 |


### 4.1.9 - stop gap publishing from tibco nodes  (Sun Oct 14, 2018 - 16:02 MDT )
| issue | description |
|----|----|
| publish | added `-Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.insecure=true` to allow to publish to on node in Corp Nexus (Stop Gap)  |
| nodes | manually added `python2.7` to the `urh00613/4` to for pinning fdeploy execution |
| agent | made the label for the agent for CI a variable that can be set to pin builds  |
| pinning | pinned down fdeploy actions to `python2.7` nodes to stabilize execution since Corporate Nodes have 2.6 python |


### 4.1.8 - refactored release cut flow (Sun Oct 14, 2018 - 16:02 MDT )
| issue | description |
|----|----|
| release_cut | refactored java/tibco release cut to be efficient |


### 4.1.7 - (interim release) fixed java repackaging (Thu Oct 12, 2018 - 12:41 MDT )
| issue | description |
|----|----|
| declarative | java declarative pipeline defect skipping packaging leaving empty archives |

### 4.1.6 - (interim release) PCF PAM integration (Thu Oct 12, 2018 - 12:41 MDT )
| issue | description |
|----|----|
| pcf_pam | stabilize java declarative pipeline defect. |

### 4.1.5 - (interim release) Java Declarative stability. (Thu Oct 12, 2018 - 12:05 MDT )
| issue | description |
|----|----|
| builder | stabilize java declarative pipeline defect. |

### 4.1.4 - (interim release) BW and BE CI part stability. (Thu Oct 12, 2018 - 08:57 MDT )
| issue | description |
|----|----|
| build | stabilize BW and BE fixing defects while testing. |


### 4.1.3 - (interim release) integrated PAM service accounts and deployment for declarative pipeline. (Thu Oct 11, 2018 - 09:17 MDT )
| issue | description |
|----|----|
| pam | pam integration for PCF deployments |
| deployment | integrated deployments in declarative pipeline |


### 4.1.2 - node assignment for BE/BW pinned to SEFS hardware. (Mon Oct 9, 2018 - 09:17 MDT )
| issue | description |
|----|----|
| jacoco | enabling jacoco under the declarative pipeline |
| checkout | made checkout a procedure rather than class |
| publish | made publishTestResults a procedure |


### 4.1.1 - node assignment for BE/BW pinned to SEFS hardware. (Mon Oct 8, 2018 - 23:14 MDT )
| issue | description |
|----|----|
| node | narrowed down the hardware that BE/BW can run under |


### 4.1.0 - conversion tagging to procedural methods and externalized deployment to nexus to corporate nodes. (Mon Oct 8, 2018 - 23:10 MDT )
| issue | description |
|----|----|
| deployment | Moved deployment step in buildPlugin to a corporate build node to migigate firewall and other environment issue |
| tagging | made the tagging process procedural which is more reliable in moving to a declaritive pipeline  |
| checkout | fixed error in checkout for SVN that prevented tagging step |
| declarative | created a declarative pipeline for Java only projects streamlining |

### 4.0.2 - upgrade fdeploy and mattermost integration  (Tue Sep 25, 2018 - 10:58 MDT )
| issue | description |
|----|----|
| fdeploy | Upgrade to 7.0.3 as stable release fixing install.py unpack problem  |
| notify.groovy | Added mattermost system team notification channel  |


### 4.0.1 - upgrade fdeploy and readMavenPom (Fri Sep 14, 2018 - 19:10 MDT )
| issue | description |
|----|----|
| fdeploy | Upgrade to 7.0.2 as stable release fixed cloudbees calculation anomily |
| readmavenpom | added dir statements when reading pom.xml with `readMavenPom` operator |



### 4.0.0 - Nexus v2 and v3 extended / Silver Retirement
| issue | description |
|----|----|
|install | Addressed changes in the installation process for Nexus v3 and Nexus v2 to work |
| retire | Retiring all silver scripts components and renaming the package to standard CM naming |
