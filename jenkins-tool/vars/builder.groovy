import com.fedex.ci.*

// vars/builder.groovy
def call( Map config = [:]) {
   //echo "MAPPING: ${config}"
    def setup = config.setup
    def extra_build_options = ''
    def module = '.'
    try { extra_build_options = '' ?: BUILD_OPTIONS } catch (error_x) {}
    try { module = config.module ?: BUILD_MODULE } catch (error_y) {}
    Log.header(this,"BUILDER")
    Log.echoBuildStatus(this, currentBuild, "begin of builder")
    def wreport = new WorkflowReporter(setup)
    //echo "${wreport.toString()}"
    // STEP VARIABLES
    extra_build_options = "${extra_build_options}"
    // command line assignments
    def java_home = tool name : 'JAVA_8'
    def m2_home = tool name : Definitions.APACHE_MAVEN_VERSION
    Log.debug(this,"builder currentBuild begin state '${currentBuild.result}'")
    def build_goals = "clean org.jacoco:jacoco-maven-plugin:0.8.1:prepare-agent pmd:pmd -Daggregate=true -Dpmd.typeResolution=false deploy org.jacoco:jacoco-maven-plugin:0.8.1:report"
    dir("${env.WORKSPACE}/${module}") {
      if (fileExists('tmp/setup.py')) {
        sh 'mv tmp/setup*.* .'
      }
      def a = fileExists('BUILT')
      def b = fileExists('RELEASED')
      def c = fileExists('RELEASE_CUT')
      def d = fileExists('DEPLOYED_TO_NEXUS')
      def buildSuccess = 1
      if (!fileExists('pom.xml')) {
        Log.warnerror(this, "Skipping build, no pom.xml found.")
        touch file: 'BUILT'
        touch file: 'RELEASED'
        touch file: 'RELEASE_CUT'
        touch file: 'DEPLOYED_TO_NEXUS'
        return
      }
      if (fileExists('BUILT') == false) {
            def next_version = calculate_next_version()
            setup.nextVersion = next_version.replaceAll("(?:\\n|\\r)", "")
            echo "${setup.toString()}"
            if ("${setup.performReleaseCut}" == "true") {
                Log.header(this,"PERFORMING RELEASE CUT")
                // LIBRARIES GET LOADED in the PREPARE STEP
                Log.info(this, "cutting release for version ${next_version}")
                setup.our_build(this, "${extra_build_options}","versions:set -DnewVersion=${next_version}")
                stash excludes: "**/*.zip,**/*.*ar,**/target/**,**/*.class,**/node**", useDefaultExcludes: true, name: 'build.sources'
                stash includes: '**/pom.xml', useDefaultExcludes: true, name: 'build.poms'
                if (fileExists('profiles.xml') && setup.doEnforcer) {
                  sh 'rm profiles.xml'
                }
            }
            else {
              if (!"${setup.version}".contains('-SNAPSHOT')) {
                Log.compileErrorAndExit(this, 'Illegal Operation: ' + setup.version + ' is a released version notation')
              }
            }
            build_goals = command_builder()
            // the first build based on the supplied goal,
            setup.our_build(this, "${extra_build_options}" + setup.getWorkspaceRepositoryOptions(),"${build_goals}", "${setup.appType}")
            stash includes: "**", excludes: 'target/**,**/tags/**,**/branches/**', name: 'build.sources'
            //stash includes: "**", includes: 'target/m2/**', name: 'build.binaries'
            touch file: 'BUILT', timestamp: 0
            if ("${setup.performReleaseCut}" == "true") {
              touch file: 'RELEASE_CUT', timestamp: 0
              Log.debug(this,"builder RELEASE_CUT currentBuild begin state '${currentBuild.result}'")
            } else {
              Log.debug(this,"builder BUILD currentBuild end state '${currentBuild.result}'")
            }
      } else {
          //unstash name: 'build.sources'
          // already passed BUILD and TEST phase
          if (fileExists('DEPLOYED_TO_NEXUS') == false ) {
            if (fileExists('RELEASE_CUT') && !fileExists('TAGGED')) {
                  // 4. TAG VERSION in source repository
                  unstash name: 'build.poms'
                  Log.header(this, "PERFORMING TAGGING")
                  Log.debug(this,"builder tag currentBuild begin state '${currentBuild.result}'")
                  tagPlugin(module)
                  touch file: 'TAGGED', timestamp: 0
            }
            else {
              Log.debug(this,"builder deploy2nexus currentBuild begin state '${currentBuild.result}'")
              setup.our_deploy(this,"${extra_build_options} -DdeployAtEnd=false")
              Log.debug(this,"builder deploy2nexus currentBuild end state '${currentBuild.result}'")
              touch file: 'DEPLOYED_TO_NEXUS', timestamp: 0
            }
          }
          else {
            Log.warn(this, "already in status DEPLOYED_TO_NEXUS")
          }
          touch file: 'RELEASED', timestamp: 0
      } // tagging stage
    } //dir
    Log.echoBuildStatus(this, currentBuild, "end of builder")
}
