#!/usr/bin/env groovy
import com.fedex.ci.*

def call(Definitions setup) {
	def scannerHome = tool 'SonarQube_SEFS';
	def src = setup.getSonarSourcePaths()
	def test = setup.getSonarTestPaths()
  def jacocoReportsXml = (setup.hasTests && setup.jacocoReportsXml != null) ? "-Dsonar.coverage.jacoco.xmlReportPaths=${setup.jacocoReportsXml}" : ""
	def pythonCoverageXml = (setup.hasTests && setup.cobuteraReportsXml != null) ? "-Dsonar.python.coverage.reportPath=${setup.cobuteraReportsXml}" : ""
	def binaries = (setup.binaries != null) ? setup.binaries : "target"
	def scmDisabled = (setup.scmDisabled != null) ? "-Dsonar.scm.disabled=${setup.scmDisabled}" : ""
	def repo = (setup.scmRepo != null) ? "-Dsonar.scm.provider=${setup.scmRepo}" : ""
	def exclusion = setup.hasTests ? jacoco_exclusions() : ""
  Log.debug(this,"sonar currentBuild begin state '${currentBuild.result}'")

	def wEnv = setup.defaultEnv()
	wEnv << "SONAR_SCANNER_OPTS=-Dhttps.proxyHost=internet.proxy.fedex.com -Dhttps.proxyPort=3128"

	withEnv(wEnv) {
    withSonarQubeEnv('SonarQube') {
			sh 'find target -type f -maxdepth 2 2> /dev/null'
			sh "set -xv && ${scannerHome}/bin/sonar-scanner \
-Dsonar.projectKey='${setup.getProject()[2]}' \
-Dsonar.projectName='${setup.getSonarName()}' \
-Dsonar.projectVersion=${setup.version} \
-Dsonar.java.binaries=${binaries}  \
-Dsonar.sources=${src} \
-Dsonar.sourceEncoding=UTF-8 -Dsonar.tests=${test} \
${jacocoReportsXml} ${pythonCoverageXml} ${repo}  \
${scmDisabled} -Dsonar.exclusions=${exclusion}"
			}
	}
  Log.debug(this,"sonar currentBuild end state '${currentBuild.result}'")
}
