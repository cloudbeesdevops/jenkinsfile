import com.fedex.ci.*
import java.io.*
// vars/publishDashboard.groovy
def call(Map config = [:]) {
  def abortFlag = 0
  def module = '.'
  def t = null
  def goalresultss = []
	def cnt = -1
  try { module = config.module ?: BUILD_MODULE } catch (error_y) {}
  dir(module) {
      t =  readMavenPom file: ''
  }
  def branch = t.getVersion()
  def repoType    = ''
  def releaseName = ""
  def svnPath     = ""
  try { releaseName = config.releaseName ?: "" } catch (err) {}
  try { repoType = config.repoType ?: "" } catch (err) {}
  try { svnPath = config.svnPath ?: ""} catch (err) {}
  def APP_NAME = env.APP_NAME
  def APP_TYPE = env.APP_TYPE
  def OPCO = env.OPCO
  def ARTIFACT_ID =  t.getArtifactId()
  def VERSION = branch.tokenize("-")[0]
  def APP_LEVEL = 'L0'
  def javahome = tool name: 'JAVA_8', type: 'jdk'
  def m2home = tool name : Definitions.APACHE_MAVEN_VERSION
  def archnode = env.ARCH_NODE ?: 'uwb00078'
  def mailrecp = env.BUILD_RECEPIENTS ? env.BUILD_RECEPIENTS : "SEFS-${OPCO}-DeploymentNotify@corp.ds.fedex.com,SEFS_CICD@corp.ds.fedex.com"
  try {
    withEnv(["JAVA_HOME=${javahome}",
        "M2_HOME=${m2home}", "PATH+MAVEN=${m2home}/bin:${javahome}/bin",
            "MAVEN_OPTS=-Xmx2048m -Xms2048m -Djava.security.egd=file:/dev/urandom -Dmaven.artifact.threads=10",
            "https_proxy=http://internet.proxy.fedex.com:3128",
            "http_proxy=http://internet.proxy.fedex.com:3128"
    ]) {
      dir(module) {
        unstash "build.prepare"
        // reaping the results of the test cycle
        try {
            def scanReportSh = "find . -name 'TEST*.xml' -print0 | xargs -0 grep '<testsuite ' | sed -e 's/.errors=\"//' -e 's/\".//' | awk '{ SUM += \$1} END { print SUM }' "
            scanReport =  sh returnStdout: true, script: scanReportSh
            Log.info(this, "Scan Report : " + scanReport.toString())
        } catch (error) {
           Log.warnerror(this,"error processing report scans")
           goalresultss[++cnt] = [ 'name' : 'UnitTest', 'message' : 'Unit Test creation/publishing failed', 'errcode' : '20' , 'continue' : 'true' , 'actualerr' : error]
        }

        node(archnode){
          if (config.has_tests) {
            // bugfix in the stashing mechanism...
            def targetPrep = '''[[ -f target ]] && rm -v target; [[ ! -e target/surefire-reports ]] && mkdir -p ./target/surefire-reports'''
            sh returnStatus: true, script: targetPrep
            unstash 'test.reports'
            unstash 'jacoco.reports'
            unstash 'xml.reports'
          }
          else {
            Log.compileWarning(this, "${env.APP_NAME} does not have any tests associated, skipping the legacy execution.")
            return
          }

          def archreporter = libraryResource 'scripts/archreports.sh'
          writeFile file: 'target/archreports.sh', text: archreporter
          sh 'find . -type f -exec chmod 775 {} \\;'
          def archsh =  '''
          ./target/archreports.sh -o '''+ OPCO + ''' -a  '''+ APP_TYPE + ''' -r CODE_COVERAGE -key '''+ BUILD_NUMBER + ''' -d ''' + WORKSPACE + '''/target/site -s ''' + APP_NAME + '''_Ver''' + VERSION +
          '''
          ./target/archreports.sh -o '''+ OPCO + ''' -a  '''+ APP_TYPE + ''' -r UNIT_TEST -key '''+ BUILD_NUMBER + ''' -d ''' + WORKSPACE + '''/target/surefire-reports -s ''' + APP_NAME + '''_Ver''' + VERSION +
          '''
          ./target/archreports.sh -o '''+ OPCO + ''' -a  '''+ APP_TYPE + ''' -r STATIC_CODE_ANALYSIS -key '''+ BUILD_NUMBER + ''' -d ''' + WORKSPACE + '''/target/pmd.xml -s ''' + APP_NAME + '''_Ver''' + VERSION
          def retCd = sh returnStatus: true, script: archsh

          if ( retCd > 0 ) {
              Log.info(this,"Placeholder")
          }
          def ManifestFile =  "/var/fedex/tibco/cicd_reports/reports_jenkin/${OPCO}_${APP_TYPE}_${APP_NAME}_manifest_${BUILD_NUMBER}.txt"
          def Manifest = "${OPCO},${APP_TYPE},${APP_NAME},SCA,${BUILD_NUMBER}," + BUILD_URL + "pmdResult" + '/' + ",${VERSION},${releaseName},${repoType},${svnPath}"
          Manifest += "\n${OPCO},${APP_TYPE},${APP_NAME},CC,${BUILD_NUMBER}," + BUILD_URL + "jacoco" + '/' + ",${VERSION},${releaseName},${repoType},${svnPath}"
          Manifest += "\n${OPCO},${APP_TYPE},${APP_NAME},UT,${BUILD_NUMBER}," + BUILD_URL + "testReport" + '/' + ",${VERSION},${releaseName},${repoType},${svnPath}"
          writeFile file: ManifestFile, text: Manifest

          scanReport = scanReport.trim()
          if ( "".equals(scanReport) || scanReport.toInteger() != 0 ) {
              goalresultss[++cnt] = [ 'name' : 'UnitTest', 'message' : "Unit Test execution for ${OPCO}_${APP_TYPE}_${APP_NAME} FAILED. Aborting the build and your application will not be deployed to any test levels until this issue is fixed.", 'errcode' : '60' , 'continue' : 'false' , 'actualerr' : 'Unit Test execution for ${SEFS_OPCO}_${APP_TYPE}_${APP_NAME} FAILED. Aborting the build and your application will not be deployed to any test levels until this issue is fixed.']
          }
          sh returnStatus : true, script: '''
  cat -n ${ManifestFile}
  cp /var/tmp/tools/report-parser.jar .
  java -jar report-parser.jar
  '''
          def errOut = ''
          if (cnt>-1) {
              for (j=0;j<=cnt;j++) {
                echo " Analysis= " + j + " : " +  goalresultss[j]
                 errOut += "<BR><font color=red>" + goalresultss[j].message + "</font><BR>"
                 abortFlag = (goalresultss[j].continue == 'false') ? ++abortFlag : abortFlag
                 // what is this used for?
                 errOut += (goalresultss[j].continue == 'false') ? goalresultss[j].name + " - " + goalresultss[j].message + "\n" : ""
                 mailrecp = env.BUILD_FAILURE_RECEPIENTS ? env.BUILD_FAILURE_RECEPIENTS : "SEFS-${OPCO}-DeploymentNotify@corp.ds.fedex.com,SEFS_CICD@corp.ds.fedex.com"
              }
          }
        	def OUT_HTML = errOut + "<BR><BR>Please find the attachment for ${APP_NAME}/${VERSION} test reports."
          OUT_HTML += "<BR><BR><B> OPCO          : </B>${OPCO}"
          OUT_HTML += "<BR><B> Component Type    : </B>${APP_TYPE}"
          OUT_HTML += "<BR><B> ApplicationName   : </B>${APP_NAME}"
          OUT_HTML += "<BR><B> ArtifactID        : </B>${ARTIFACT_ID}"
          OUT_HTML += "<BR><B> Branch            : </B>${VERSION}"
          OUT_HTML = repoType == "" ? OUT_HTML : OUT_HTML + "<BR><B> Repo Type         : </B>${repoType}"
          OUT_HTML = releaseName == "" ? OUT_HTML : OUT_HTML + "<BR><B> Release           : </B>${releaseName}"
          OUT_HTML += "<BR><BR><B>JOB URL<B><BR>${JOB_URL}"
          def IN_HTML = "<BR><BR>Code Coverage Link:</P>"
        	IN_HTML += "<BR><a href='${BUILD_URL}/jacoco/'>Code Coverage Analysis for ${APP_NAME}/${VERSION} </a>"
        	IN_HTML += "<BR><BR>Static Code Link:</P>"
        	IN_HTML += "<BR><a href='${BUILD_URL}/pmdResult/'>Static Code Analysis for ${APP_NAME}/${VERSION} </a>"
        	IN_HTML += "<BR><BR>Unit Test Link:</P>"
        	IN_HTML += "<BR><a href='${BUILD_URL}/testReport/'>Unit Test Analysis for ${APP_NAME}/${VERSION} </a>"
          def dest = "**/target/pmd.xml"
          dest += (abortFlag > 0) ? "" : ",**/target/site/jacoco/jacoco.csv"
          dest += (abortFlag > 0) ? "" : ",**/target/surefire-reports/TEST-*.xml"

          OUT_HTML +=  (abortFlag > 0) ? "" : IN_HTML + "<BR><BR>Thanks,<BR>CI/CD Team<BR>"
          def BUILD_FAIL = (abortFlag > 0) ? "BuildFailed" : "Reports"
          def OUT_HTML_SUB  = "CICD " + BUILD_FAIL + " for - ${OPCO} - ${APP_TYPE} - ${APP_NAME}/${VERSION}"
          OUT_HTML_SUB = releaseName == "" ? OUT_HTML_SUB : OUT_HTML_SUB + "/Release: " + releaseName + " "
          OUT_HTML_SUB += "- ${APP_LEVEL} - BuildNumber : ${BUILD_NUMBER}"
          emailext attachmentsPattern: dest, body: OUT_HTML, subject: OUT_HTML_SUB, to: mailrecp
          if (abortFlag > 0) {
              Log.info(this, "currrentBuild-${currentBuild.result}")
              currentBuild.result='UNSTABLE'
          }
        } //node
      } //dir
    } // withenv
  } catch (error) {
   Log.error(this, "publishDashboard exception:", error )
   throw error
  }
} //call
