// check if jacoco has any inline excludes
def call() {
  output = sh returnStdout: true, script: '''#!/bin/bash
  python <( cat << EOF
import xml.etree.ElementTree as ET
root = ET.parse('pom.xml').getroot()
context = root.attrib[root.attrib.keys()[0]].split(' ')[0]
a = ".//{%s}exclude" % (context)
print ','.join([element.text for element in root.findall(a)])
EOF
)
'''
  output.replaceAll("(?:\\n|\\r)", "");

  return output
}
