import com.fedex.ci.*

def call(com.fedex.ci.Definitions setup) {
  // 1. fetch deployment properties and load them fi exist on anynode
  def props=[:]
  echo "\u2705 \u2705 \u2705 [ Deployment ] \u2705 \u2705 \u2705"
	try {
    unstash name: 'deploy.flag'
  } catch (Exception e) {
    Log.error(this, e)
  }
  Log.echoBuildStatus(this, currentBuild, "entering of ${env.STAGE_NAME}")
  if (fileExists(".deploy")) {
        echo "${setup.toString()}"
        // now build, based on the configuration provided
        // ,deploy will contain all the default settings
        // and can be overwritten in the console.
        //sh 'ls -alrt && cat .deploy'
        props = readProperties file: '.deploy'
        if (props['LEVEL'] == null) {
          props['LEVEL'] = setup.selected_level
        }
        echo ".deploy contents: ${props}"
        // 2. if we have an externalized deployment defined.
        node('python2.7') {
            if (props['deploy.library.repository'] != null) {
                library identifier: props['deploy.library.identifier'] , retriever: modernSCM(
                    [$class: 'GitSCMSource', remote: props['deploy.library.repository'], credentialsId: props['deploy.library.creds']])
                echo "library loaded invoking deployProxy"
                props['setup']=setup
                externalDeployProxy(props)
            }
            else {
                // 3. default internal (SEFS defined) deployment mechanism
                executeCICDeploy(setup, props)
            }
        }
  }
  else {
    echo "\u2623 \u2623 \u2623 \u2623 \u2623 \u2623 \u2623 \u2623 \u2623\u2623 \u2623 \u2623"
    echo "\u2623 \u2623 \u2623 NO DEPLOYMENT CONFIG FOUND in .deploy  \u2623 \u2623 \u2623"
    echo "\u2623 \u2623 \u2623 \u2623 \u2623 \u2623 \u2623 \u2623 \u2623\u2623 \u2623 \u2623"
    currentBuild.result = 'UNSTABLE'
  }
  Log.echoBuildStatus(this, currentBuild, "end of ${env.STAGE_NAME}")
}
