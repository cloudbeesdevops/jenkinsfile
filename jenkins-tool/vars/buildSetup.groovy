import com.fedex.ci.*

def static isEmpty(string) {
	 return "${string}" == "null" ||  "${string}" == ""
}

// vars/buildSetup.groovy
def call(Map config=[:] ) {
   def d = config.setup
   def __error = null
   def flags = []
   try {
      Log.echoBuildStatus(this, currentBuild, "begining of buildSetup")
      //Log.echoBuildStatus(this, currentBuild, "${config}")
      def jobpath = "${env.JOB_NAME}".split("/")

      echo "\u2705 \u2705 \u2705 Setup \u2705 \u2705 \u2705\nJOB_NAME=${env.JOB_NAME}"

      def module = '.'
      try { module = config.module ?: BUILD_MODULE } catch (error_y) {}
      nodeName = "${env.NODE_NAME}".replaceAll('\'', '')
      // now build, based on the configuration provided in the closure
    echo "${params}"
    def envs = sh(returnStdout: true, script: 'env | sort').split('\n')
    echo "${envs}"
    echo "${config}"
    echo "noADG params=${config.noADG}, env=${env.NO_AUTO_DEPLOY_QUESTION}"
    d.appType = env.APP_TYPE
    d.nodeName = env.NODE_NAME
    if (isEmpty("${config.selected_level}")) {
    	if (isEmpty("${config.default_level}")) {
    		d.selected_level = "L1"
    	}
    	else {
        	d.selected_level = "${config.default_level}"
    	}
    } else {
    	d.selected_level = "${config.selected_level}"
    }
    d.module = module
    d.fortifyScan = "${config.fortify}"
    d.performReleaseCut = "${config.perform_release_cut}" == 'true' || config.perform_release_cut == true
    d.jobpath = "${env.JOB_NAME}"
    d.WORKSPACE = "${env.WORKSPACE}"

    //tool name : 'PY_TEST_FRAMEWORK'
    tool name : 'LDD_CICD_PARSER'
    tool name : 'CF_CLIENT_INSTALL'
    // scm wrapper
    // custom pre-build step hook
    scmWrapper(d)

    try {
      dir(module) {
        if (fileExists('setup.py')) {
          sh '[[ ! -d tmp ]] && mkdir tmp;mv setup.* ./tmp'
        }
        if (fileExists('pom.xml')) {
          t =  readMavenPom file: ''
          d.artifactId = t.getArtifactId()
          d.groupId = t.getGroupId()
          d.version = t.getVersion()
          d.versionBase = d.version.tokenize("-")[0]
          sh returnStatus: true, script: 'rm -fr target'
        }
      }
    } catch (error) {
      Log.error(this, "failed to init_definitions", error)
    }
    parallel(
      install_cicd_tools : {
        sh returnStatus: true, script: 'rm TAGGED RELEASED RELEASE_CUT BUILT DEPLOYED_TO_NEXUS UPLOADED 2> /dev/null'
        sh script: '''#!/bin/bash
curl -s -O http://sefsmvn.ute.fedex.com/install.py
echo $(curl -s http://sefsmvn.ute.fedex.com/fdeploy-install.sh | grep VERSION= | head -n 1 | awk -F '=' '{ print $2 }') > DEPLOYMENT_PACKAGE_VERSION
mkdir -p vars
'''
      // loading settings.xml from the shared resources
      def settings_xml = libraryResource 'settings.xml'
      writeFile file: 'settings.xml', text: settings_xml
      // setup fortify
      if (config.fortity) {
        try {
          node('master') {
            dir("${env.JENKINS_HOME}/userContent/fortify") {
              stash includes: '*.sh', name: 'build.fortify'
            }
          }
        } catch (Exception e) {
          echo "${e}"
        }
        try {
          unstash "build.fortify"
        } catch (Exception e) {
          echo "${e}"
        }
      }
      sh '''#!/bin/bash
if [[ "${FD_VERSION}" == "" ]]; then
   FD_VERSION=$(cat DEPLOYMENT_PACKAGE_VERSION)
fi
python install.py -v "${FD_VERSION}"
# overwrite a packaged install.py
curl -s -O http://sefsmvn.ute.fedex.com/install.py
'''
        d.fdeployVersion = readFile file: 'DEPLOYMENT_PACKAGE_VERSION'
        d.fdeployVersion.replaceAll("(?:\n|\r)", "")
      },
      calculate_pipeline_execution : {
        def configName = d.getConfigName(env, config.name)
        try {
          office365ConnectorSend( color: "#ffffff",  status: "Started build.", message: 'View Build', webhookUrl: Definitions.SEFS_ST_CICD_TEAMS_HOOK_URL)
          def build_history = Definitions.createLddRecord(env,nodeName, configName, 0, '', 'no message')
          sh(returnStdout: true, script: "curl -s -f -d '${build_history}'  -X POST '" + Definitions.LDD_HOOK_URL + "/api/public/build/create' -H 'Content-Type: application/json'")
        } catch (error) {
          Log.error(this, "${error}", error)
        }
        dir(d.module) {
          def flagmap = Definitions.getFlags()
          for (key in flagmap.keySet()) {
            def exists = fileExists(".${key}")
            flagmap[key] = exists
            if (exists) {
              stash includes: ".${key}", name: "${key}.flag"
            }
          }
          d.flagmap = flagmap
        }
        if (fileExists(".git")) {
          try {
            sh 'git remote -v > VCS_SOURCE'
          } catch (exception) {
            Log.error(exception)
          }
          stash includes: 'VCS_SOURCE', name: 'vcs.source'
        } else if (fileExists(".svn")) {
          sh 'echo "SVN" > VCS_SOURCE'
          stash includes: 'VCS_SOURCE', name: 'vcs.source'
        }

        d.hasPoms = false
        try {
          stash includes: '**/pom.xml', useDefaultExcludes: true, name: 'build.poms'
          d.hasPoms = true
        } catch (error) {
          Log.compileError(this, "pom stash failed: ${error}")
        }

        workflow_setup = libraryResource 'scripts/workflow_setup.py'
        writeFile file: 'workflow_setup.py', text: workflow_setup
        try { if ("${env.noTesting}" != 'null' || "${env.noTesting}" == "") { d.hasTests = false } } catch (error_y) {}

        command = sh returnStdout: true, script : 'python ./workflow_setup.py -t'
        d.hasTests = false
        d.testPaths = ''
        if (!"".equals(command)) {
          d.hasTests = true
          d.testPaths = command
          Log.info(this, "test paths = '" + d.testPaths + "'")
        }
        else {
          Log.compileWarning(this, 'NO TESTS FOUND')
        }
        try { if ("${env.noTesting}" != 'null' || "${env.noTesting}" == "") {
          d.hasTests = false
          d.testPaths = ''
          Log.compileWarning(this, 'testing disabled by environment variable')
         }
       } catch (error_y) {}

        d.hasCode = false
        d.sourcePaths = ''
        command = sh returnStdout: true, script : 'python ./workflow_setup.py -s'
        if (!"".equals(command)) {
          d.hasCode = true
          d.sourcePaths = command
          Log.info(this, "source paths = " + d.sourcePaths)
        }
        else {
          Log.compileWarning(this, 'NO SOURCE PATHS FOUND')
        }
        try {
          if (env.APP_TYPE != null) {
            d.appType = env.APP_TYPE
          }
        } catch (errrr) {}
      // calculate workflow
      d.setWorkflow(env, params)

      },
      stash_sources : {
        if (config.fortity) {
          try {
            stash excludes: "**/*.zip,**/*.*ar,**/target/**,**/*.class,**/node**", useDefaultExcludes: true, name: 'build.sources'
          } catch (error) {
            Log.error(this, "failed to stash sources", error)
          }
        }
      }
    )
    stash name: "build.prepare", excludes: "**/src/*,**/node/*", useDefaultExcludes: true
    d.JAVA_HOME = tool name: 'JAVA_8', type: 'jdk'
    d.MAVEN_HOME = tool name : Definitions.APACHE_MAVEN_VERSION
  } catch (error) {
    Log.error(this, error)
    __error = error
  }
  Log.echoBuildStatus(this, currentBuild, "end of buildSetup")
  Log.warn(this,"${d.toString()}")
  if (__error) {
    throw __error
  }
  return d

}
