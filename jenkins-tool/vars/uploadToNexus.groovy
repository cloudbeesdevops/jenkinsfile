import com.fedex.ci.*
import groovy.json.JsonSlurper

// vars/uploadToNexus.groovy
def call(com.fedex.ci.Definitions setup, release) {
  Log.header(this, "Upload To Nexus")
  Log.echoBuildStatus(this, currentBuild, "begin of upload to nexus")
  dir("${env.WORKSPACE}/${setup.module}") {
    //sh "find target/m2 -type f -ls | grep -v 'sha1\$\\|xml\$\\|md5\$'"

    def repo_url = setup.get_repo_url()

    def cmd = release ? 'deployFile' : 'deploySnapshotsOnly'
    uploader_script = libraryResource 'scripts/uploadBuilder.py'
    writeFile file: 'uploadBuilder.py', text: uploader_script
    sh returnStdout: true, script : "python ./uploadBuilder.py -f ${cmd}"
    sh returnStdout: true, script : 'python ./uploadBuilder.py -f proxyRefresh'

    command = readFile("${cmd}.json")
    command2 = readFile('proxyRefresh.json')
    print "response[1]: " + command
    print "response[2]: " + command2
    def jsonSlurper = new JsonSlurper()
    def artifacts = jsonSlurper.parseText command
    def artifact_urls = jsonSlurper.parseText command2

    echo "artifacts recorded= ${artifacts}"

    repo_url.replaceAll("(?:\\n|\\r)", "");

    artifacts.each {
      it.replaceAll("(?:\\n|\\r)", "");
          setup.our_execute(this, "${it} ${repo_url}", 'deploy:deploy-file')
    }
    artifact_urls.each {
      it.replaceAll("(?:\\n|\\r)", "")
          sh returnStatus: true, script: "curl ${it} > /dev/null"
    }
    sh '''
echo "commit=" git rev-parse HEAD > target/m2/METADATA
echo "build_number=${BUILD_NUMBER}" > target/m2/METADATA
'''
    if (env.NO_ARCHIVING != '') {
      archiveArtifacts artifacts: 'target/m2/**', fingerprint: true, onlyIfSuccessful: true
    }

// nexusArtifactUploader - Change FORMAT variable in python
// withEnv(['http_proxy=http://internet.proxy.fedex.com:3128', 'https_proxy=http://internet.proxy.fedex.com:3128']) {
//     nexusArtifactUploader artifacts : artifacts, credentialsId : 'sefs-nexus-cm', \
//        nexusVersion: 'nexus3',\
//        protocol: 'https',\
//        nexusUrl: repo,\
//       groupId: setup.groupId,\
//       version: version, \
//       repository: repoInstance
// }
//
// nexusPublisher - Change FORMAT variable in python
//  withEnv(['http_proxy=http://internet.proxy.fedex.com:3128', 'https_proxy=http://internet.proxy.fedex.com:3128']) {
//     nexusPublisher nexusInstanceId: repoInstance, nexusRepositoryId: repoInstance, packages: [[$class: 'MavenPackage', mavenAssetList: artifacts, \
//         mavenCoordinate: [artifactId: setup.artifactId, groupId: setup.groupId, packaging: 'jar', version: version ]]]
// }

  } //dir
  Log.echoBuildStatus(this, currentBuild, "end of uploadToNexus")
  return command
}
