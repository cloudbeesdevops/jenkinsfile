import com.fedex.ci.*
// vars/RegressionTestProxy.groovy
def call(Map config = [:]) {
	// evaluate the body block, and collect configuration into the object
  try {
    Log.debug(this,"RegressionTest groovy file for loop : ${config}")
    echo "$config"
    t =  readMavenPom file: ''
    def ARTIFACT_ID =  t.getArtifactId()
    def branch = t.getVersion()
    def VERSION = branch.tokenize("-")[0]
    def SUITE = config['SUITE_NAME']
    def LEVEL = env.LEVEL
    Log.debug(this,"smokeTestProxy groovy file for loop : ${config}")
    withEnv([
      "ARTIFACT_ID=${ARTIFACT_ID}",
      "VERSION=${VERSION}",
      "SUITE=${SUITE}",
      "LEVEL=${LEVEL}",
      "https_proxy=http://internet.proxy.fedex.com:3128",
      "http_proxy=http://internet.proxy.fedex.com:3128"
    ]) {
       def response="400"  
      timeout(45) {  
        response=sh returnStdout: true, script: '''wget -qO- http://199.82.226.78:8080/SEFSRestWS/rest/RegressionExecution/PIs/${SUITE}/${LEVEL}/${ARTIFACT_ID}/${VERSION}'''
      }
      if ( response.toString().trim() == "200" )
      {
       Log.compileInfo(this, "RegressionTest Passed")
      }
      else
      {
       currentBuild.result='FAILURE'
      }
    }
  } catch (error_x) {
    // email notification of malfunctioning pipeline
    notification {
  		context = 'TEST'
  		status = 'Failed'; message = "failure: ${error_x}"
      recepients = 'SEFS-ST-CICD@corp.ds.fedex.com'
    }
		currentBuild.result='FAILURE'
  }
}