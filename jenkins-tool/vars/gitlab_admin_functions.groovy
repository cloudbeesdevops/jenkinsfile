// build the maven command based on current contents in pom.xml
def call() {
  withEnv(["PYTHONPATH=${WORKSPACE}"]) {
    echo "\u2705 \u2705 \u2705 gitlab_admin_functions \u2705 \u2705 \u2705"
    // build the maven command based on current contents in pom.xml
    builder_script = libraryResource 'scripts/gitlab_admin_functions.py'
    writeFile file: 'gitlab_admin_functions.py', text: builder_script
    command = sh returnStdout: true, script : "python ./gitlab_admin_functions.py -x -u '${GIT_URL}' -v"
    return command
  }
}
