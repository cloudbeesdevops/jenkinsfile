import com.fedex.ci.*
import java.io.*
// vars/IntegrationTesting.groovy
def call(com.fedex.ci.Definitions d) {
	try {
			unstash name : 'e2e.flag'
			if (fileExists(".e2e")) {
				echo "\u2705 \u2705 \u2705 End To End Testing \u2705 \u2705 \u2705"
				def props = readProperties file: '.e2e'
				props['artifactId']=d.artifactId
				props['groupId']=d.groupId
				props['version']=d.version
				props['versionBase']=d.versionBase
				Log.debug(this, "${props}")
				if (props['e2e.library.repository'] != null) {
						library identifier: props['e2e.library.identifier'], retriever: modernSCM(
 	  			 		[$class: 'GitSCMSource', remote: props['e2e.library.repository'], credentialsId: props['e2e.library.creds']])
 						e2eTester(d,props)
				} else {
					//2eTestProxy(props)
				}
			 }
		 } catch (exception) {
			 echo "${exception}"
		 }
}
