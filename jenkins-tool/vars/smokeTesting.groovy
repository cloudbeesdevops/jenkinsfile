import com.fedex.ci.*
import java.io.*
// vars/smokeTesting.groovy
def call(com.fedex.ci.Definitions d) {
	try {
		unstash name : 'smoketest.flag'
	} catch (Exception e) {
		Log.error(this,e)
	}
	def check_disable = "${env.DISABLE_SMOKETEST}" != "null" && "${env.DISABLE_SMOKETEST}" != "" && "${env.DISABLE_SMOKETEST}" != "false"
	Log.echoBuildStatus(this, currentBuild, "entering of ${env.STAGE_NAME} (disabled=${check_disable})")
	if (fileExists(".smoketest") && !check_disable) {
		echo "\u2705 \u2705 \u2705 Smoke Testing \u2705 \u2705 \u2705"
		echo "${setup.toStringTesting()}"
		def props = readProperties file: '.smoketest'
		props['artifactId']=setup.artifactId
		props['groupId']=setup.groupId
		props['version']=setup.version
		props['versionBase']=setup.versionBase
		props['level']=setup.selected_level
		Log.debug(this, "${props}")
		if (props['smoketest.library.repository'] != null) {
					library identifier: props['smoketest.library.identifier'], retriever: modernSCM(
  			 			[$class: 'GitSCMSource', remote: props['smoketest.library.repository'], credentialsId: props['smoketest.library.creds']])
				externalSmokeTestProxy(setup, props)
		} else {
			smokeTestProxy(props, setup)
		}
	} else {
		if (check_disable) {
			Log.warn(this, "DISABLE_SMOKETEST has been set, skipping smoke tests." )
		}
	}
	
	Log.echoBuildStatus(this, currentBuild, "end of ${env.STAGE_NAME}")
}
