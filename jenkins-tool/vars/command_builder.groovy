// build the maven command based on current contents in pom.xml
def call() {
  builder_script = libraryResource 'scripts/command_builder.py'
  writeFile file: 'command_builder.py', text: builder_script
  command = sh returnStdout: true, script : 'python ./command_builder.py'
  return command
}
