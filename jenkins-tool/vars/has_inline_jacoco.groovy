// check if jacoco is defined
def call() {
  exit = sh returnStatus: true, script: '''#!/usr/bin/python
from lxml import etree as ET
import sys
file = 'pom.xml' if len(sys.argv) < 2 else sys.argv[1]
root = ET.parse(file).getroot()
context = root.attrib[root.attrib.keys()[0]].split(' ')[0]
d = "/{%s}" % (context)
cc = d + d.join(['build', 'plugins', 'plugin'])
ff = d + d.join(['profile','build', 'plugins', 'plugin'])
inbuilds = "./%s/[{%s}artifactId='jacoco-maven-plugin']/{%s}artifactId" % (cc,context,context)
inprofilebuilds = "./%s/[{%s}artifactId='jacoco-maven-plugin']/{%s}artifactId" % (ff,context,context)

exit = 0
# jacoco is in the profiles
if (len(root.findall(inprofilebuilds)) > 0):
    sys.exit(0)
# if the jacoco is in the build and the profiles =
if (len(root.findall(inbuilds)) > 1):
    sys.exit(1)

sys.exit( len(root.findall(inbuilds)) + len(root.findall(inprofilebuilds)))

'''
  return exit != 0
}
r
