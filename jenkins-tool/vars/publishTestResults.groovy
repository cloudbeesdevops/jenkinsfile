import com.fedex.ci.*
// vars/publishTestResults.groovy
def call(Definitions setup, String app_type='java') {

  if (app_type == 'POM') {
    return
  }
    // now build, based on the configuration provided
    Log.info(this,"\u2705 \u2705 \u2705 Publish Test Results \u2705 \u2705 \u2705")
    def default_report_test = true
    def default_report_pmd = true
    def default_report_jacoco = true
    def default_report_findbugs = false
    def default_report_checkstyle = false
    def default_report_inttest = false
    try { default_report_test = d.hasTests   } catch (error1) {}
    try { default_report_jacoco = d.hasTests    } catch (error1) {}
    try { default_report_pmd = d.hasCode         } catch (error2) {}

     Log.info(this,"default_report_test\t= ${default_report_test}\n" +
        "default_report_inttest\t= ${default_report_inttest}\n" +
        "default_report_pmd\t\t= ${default_report_pmd}\n" +
        "default_report_checkstyle\t= ${default_report_checkstyle}\n" +
        "default_report_findbugs\t= ${default_report_findbugs}\n" +
        "default_report_jacoco\t= ${default_report_jacoco}\n")

    try {

    def parallel_map  = [:]

    Log.echoBuildStatus(this, currentBuild, "begin of publishTestResults")
    if (setup.hasTests) {
        try {
            def JacocoReports = sh returnStatus: true, script: 'find */target/site/jacoco/jacoco.xml -type f -print 2> /dev/null | python -c "import sys;print \',\'.join([l.rstrip() for l in sys.stdin ])" > JACOCO_REPORTS'
            if (JacocoReports < 1) {
                if (fileExists('JACOCO_REPORTS')) {
                    lines = readFile('JACOCO_REPORTS').trim()
                    if ("" == lines || !fileExists('./target/site/jacoco/jacoco.xml')) {
                      setup.jacocoReportsXml = null
                    }
                    else if ("" != lines) {
                      setup.jacocoReportsXml = lines
                    }
                    Log.info(this, "jacoco reports = " + setup.jacocoReportsXml)
                }
            }
        } catch (error) {
          setup.doLegacyPublishing.off()
           Log.warn(this,"Jacoco Caught: ${error}")
        }
        try {
            //sh returnStatus: true, script: 'find target -type f | grep coverage'
            def paths = ["target/coverage.xml" /* python */,
               "target/coverage/cobertura-coverage.xml" /* karma */ ] as String[]
              for (i=0; i< paths.length; i++) {
                Log.info(this, "path=" + paths[i] + ", " + fileExists(paths[i]))
                if (fileExists(paths[i])) {
                  setup.cobuteraReportsXml = '**/' + paths[i]
                  break
                }
              }
          } catch (error) {
        	Log.warn(this, "${error}")
        	throw error
            setup.cobuteraReportsXml = null
          }
          Log.warn(this, "cobutera_coverage: " + setup.cobuteraReportsXml)
    }

    if (setup.doSonar.isOn()) {
        parallel_map << [  sonar : {
            try {

                def binaries = sh returnStatus: true, script: 'find . -type d -regex \'.*\\/target$\' -print 2> /dev/null | python -c "import sys;print \',\'.join([l.rstrip() for l in sys.stdin  ])" > BINARIES'
                if (binaries < 1) {
                    if (fileExists('BINARIES')) {
                        lines = readFile('BINARIES').trim()
                        if ("" != lines) {
                          setup.binaries = lines
                        }
                        Log.info(this, "binaries = " + setup.binaries)
                    }
                }
                Log.compileInfo(this, "${setup.toString()}")
                sonarScan(setup)
            } catch (error) {
             Log.warn(this,"Sonar Caught: ${error}" )
             throw error
            }
        }
      ]
    }
    try { stash includes: '**/target/failsafe-reports/TEST-*.xml', name: 'failsafe.reports' } catch (error) {  }
    try { stash includes: '**/target/surefire-reports/*.xml', name: 'test.reports'  } catch (error) { }
    try { stash includes: '**/target/*.xml', name: 'xml.reports' } catch (error) { Log.warn(this,"Pmd Caught: ${error}") }
    try { stash includes: '**/target/site/jac*/*.*,**/target/*.exec,**/target/**/*.class,**/src/main/java/**', name: 'jacoco.reports'} catch (error) { setup.doLegacyPublishing.off() }

    parallel_map << [
      pom : {
        try{
            Log.debug(this," -> unstashing pom file for jacoco exclusion")
            unstash name: 'build.poms'
          } catch (error){
            Log.warnerror(this, "Unstashing pom.xml")
          }
        }
      ]
      if (setup.doLegacyPublishing.isOn()) {
        parallel_map << [
        LEGACY_PUBLISHER : {
             try {
              Log.debug(this," -> publishing to LDD CICD database")
              publishDashboard( 'has_tests' : setup.hasTests )
              if (currentBuild.result == 'UNSTABLE') {
                Log.debug(this," -> Build UNSTABLE for LEGACY_PUBLISHER report data")
              }
             } catch (error_x) {
               Log.error(this, "Error publishing LDD report data", error_x)
              throw error_x
             }
           }
        ]
      }
      if (setup.hasTests) {
          parallel_map << [ unittest : {
          if (default_report_test && setup.hasTests) {
            try {
             step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/*.xml'])
              if(currentBuild.result == 'UNSTABLE') {
               Log.debug(this," -> Build UNSTABLE for unittest report data")
              }
            } catch (errory) {
            	 Log.error(this,errory)
               Log.warnerror(this," Error publishing unittest report data")
               currentBuild.result = 'FAILURE'
             }
          }
        }]
      }
      if (default_report_inttest && setup.hasTests) {
          parallel_map << [failsafe : {
            try {
             step([$class: 'JUnitResultArchiver', testResults: '**/target/failsafe-reports/TEST-*.xml'])
              if(currentBuild.result == 'UNSTABLE') {
               Log.debug(this," -> Build UNSTABLE for inttest report data")
              }
            } catch (errory) {
           	 Log.error(this,errory)
                Log.warnerror(this,"Error publishing intttest report data")
                 currentBuild.result = 'UNSTABLE'
            }
          }
        ]
      }
      if (default_report_pmd) {
          parallel_map << [ pmd : {
           try {
              step([$class: 'PmdPublisher', canComputeNew: false, canRunOnFailed: true, defaultEncoding: '', healthy: '', pattern: '**/target/pmd.xml', shouldDetectModules: true, unHealthy: ''])
              if(currentBuild.result == 'UNSTABLE') {
                      Log.debug(this," -> Build UNSTABLE for PMD report data")
              }
           } catch (errory) {
          	 Log.error(this,errory)

             Log.warnerror(this,"Error publishing PMD report data")
             currentBuild.result = 'UNSTABLE'
           }
        }]
      }
      if (default_report_jacoco && setup.hasTests && setup.doLegacyPublishing.isOn() ) {
        parallel_map << [ jacoco : {
            try {
               sh script: 'find . -type f -name "*.exec" | wc -l '

               def execpat = ''
                try{
                    execpat = jacoco_exclusions()
                    Log.info(this,"Jacoco Exclusion list --> " + execpat)
                }
                catch(err)
                {
                    Log.error(this,"--> Error while extracting Jacoco Exclusion list",err)
                }

                  step([$class: 'JacocoPublisher', classPattern: '**/target/classes', exclusionPattern: execpat, execPattern: '**/**/**.exec', maximumBranchCoverage: '80', maximumClassCoverage: '80', maximumInstructionCoverage: '80', maximumLineCoverage: '80', maximumMethodCoverage: '80'])
              if (currentBuild.result == 'UNSTABLE') {
                Log.debug(this," -> Build UNSTABLE for Jacoco report data")
              }

            } catch (errory) {
              Log.warnerror(this," Error publishing Jacoco report data")
              currentBuild.result = 'UNSTABLE'
            }
        }]
      }

      if (default_report_checkstyle) {
        parallel_map << [  checkstyle : {
            try {
               step([$class: 'hudson.plugins.checkstyle.CheckStylePublisher', pattern: '**/target/checkstyle-result.xml', unstableTotalAll:'0'])
               if (currentBuild.result == 'UNSTABLE') {
                 Log.debug(this," -> Build UNSTABLE for Checkstyle report data")
               }
            } catch (errorx) {
              Log.warnerror(this,"Error publishing checkstyle report data")
            }
        }]
      }

      if (default_report_findbugs) {
        parallel_map << [  findbugs : {
           try {
             step([$class: 'FindBugsPublisher', pattern: '**/findbugsXml.xml', thresholdLimit:'high'])
            if (currentBuild.result == 'UNSTABLE') {
              Log.debug(this," -> Build UNSTABLE for findbugs report data")
            }
           } catch (error_x) {
            Log.warnerror(this, "Error publishing findbugs report data", error_x)
           }
        }]
      }
      if (setup.cobuteraReportsXml != null) {
        parallel_map << [ cobutera_coverage : {
          cobertura autoUpdateStability: false, classCoverageTargets: '70, 0, 0', coberturaReportFile: setup.cobuteraReportsXml, conditionalCoverageTargets: '70, 0, 0', failUnhealthy: false, failUnstable: false, lineCoverageTargets: '70, 0, 0', maxNumberOfBuilds: 0, methodCoverageTargets: '70, 0, 0', sourceEncoding: 'ASCII'

          }]
      }

      parallel parallel_map
      Log.echoBuildStatus(this, currentBuild, "end of publishTestResults")

    } catch(error) {
      Log.error(this,error)
      throw error
    }

}
