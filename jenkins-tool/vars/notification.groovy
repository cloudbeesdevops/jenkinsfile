import com.fedex.ci.*

// vars/notify.groovy
def call(Map config = [:]) {
    def setup = config.setup
    def jobpath = "${env.JOB_NAME}".split("/")
    def project = Definitions.getProjectFromEnv("${env.JOB_NAME}")
    def sonarProjectKey = setup.getProject()[2]
    def configName = config.name ?: project[1] + "@${env.BRANCH_NAME}"
    Log.debug(this, "${config.name} / cn=${configName}/ bn=${env.BRANCH_NAME}")
    def bluepath = ''
    if (!config.name && ("${env.BRANCH_NAME}" == "null" || env.BRANCH_NAME == null )) {
       // is a trunk
      configName = jobpath[jobpath.length-1]
    }
    // poorman's join
    for (i=0;i < jobpath.length-1; i++) {
       if (i == 0) {
          bluepath = jobpath[i]
       } else {
         bluepath = bluepath + '%2F' + jobpath[i]
       }
    }
    bluepath = "${env.JOB_NAME}".replaceAll("/","\\\\%2F")
    echo "\n\n\n${bluepath}\n\n"
    def statusMessage = config.message ?: 'no message'
    def status = config.status ?: ''
    def context = config.context ?: 'PRJ'
    def emailTo = config.recipients ?: env.BUILD_RECEPIENTS
    //emailTo = emailTo ?: 'akaan@fedex.com'
    def failureEmailTo = config.failure_recipients ?: env.BUILD_FAILURE_RECEPIENTS
    failureEmailTo = failureEmailTo ?: 'akaan@fedex.com'
    def step = config.step ?: 'Build'
    def icon = ''
    status_key = status.toLowerCase()
    def color = '#00FF00'
    if ( status_key =~ /fail/) {
      icon = '\u2614\ufe0f'
      color = "#ff0000"
      emailTo = emailTo + ',' + failureEmailTo + ',SEFS-BuildFailures@corp.ds.fedex.com'
    } else if ( status_key =~ /aborted/ ) {
      icon = '\u2614\ufe0f'
      color = '#dddddd'
      emailTo = emailTo + ',' + failureEmailTo + ',SEFS-BuildFailures@corp.ds.fedex.com'
    } else if ( status_key =~ /success/ ) {
      icon = '\u2600\ufe0f'
    } else {
      color = "#000000"
      icon = '\u27BF'
    }
    int jobDuration = (System.currentTimeMillis() - currentBuild.startTimeInMillis)/1000;
    def nodeName = "${env.NODE_NAME}".replaceAll('\'','')
    def build_history =
      "{\"jobName\":\"${env.JOB_NAME}\"," +
      "\"nodeNm\":\"${nodeName}\",\"issuer\":null,\"lastBuild\":null," +
      "\"sourceLocation\":\"${configName}\",\"comments\":\"${statusMessage}\"," +
      "\"notification\":\"${env.BUILD_RECEPIENTS}\"," +
      "\"failedAt\":null,\"buildNumber\":\"${env.BUILD_NUMBER}\"," +
      "\"buildCause\":\"${env.BUILD_CAUSE}\"," +
      "\"duration\":\"${jobDuration}\"," +
      "\"releaseTag\":\"${env.RELEASE_TAG}\"," +
        "\"status\":\"${status}\"}";
    def subject = "${icon} ${context}:${configName} [${step} ${status}] : #(${env.BUILD_NUMBER})"
    echo "\u2705 \u2705 \u2705 Notifying \u2705 \u2705 \u2705 ${icon}"
    echo "change_author: ${env.CHANGE_AUTHOR_EMAIL}"
    echo "change_author: ${env.CHANGE_AUTHOR}"
    echo "email        : ${emailTo}"
    echo "failureEmailTo:${failureEmailTo}"
    echo "\n\n\n\n     ${bluepath} \n\n\n\n"
//https://jenkins-shipment.web.fedex.com:8443/jenkins/blue/organizations/jenkins/SHIPMENT-3534550/SHIPMENT_DEFINITION/detail/1.3.0/89/pipeline/
// ${JOB_URL}/blue/organizations/jenkins/${bluepath}/detail/${BRANCH_NAME}/${BUILD_NUMBER}/pipeline
    def body = "<table>\
    <tr><td>Job</td><td><a href=\"${env.JOB_URL}\">Job Configuration</a> #${env.BUILD_NUMBER}</td></tr>\
    <tr><td></td><td><a href=\"${env.BUILD_URL}\">Execution job details</a> executed on node '${env.NODE_LABELS}' (${step}).</td></tr>\
    <tr><td></td><td><a href=\"${env.BUILD_URL}changes\">Source Changes (${step})</a>.</td></tr>\
    <tr><td></td><td><a href=\"${env.JENKINS_URL}/blue/organizations/jenkins/${bluepath}/branches\">Job branches</a>.</td></tr>\
    <tr><td>Sources</td><td><a href=\"${env.GIT_URL}\">${env.GIT_URL}</a>.</td></tr>\
    <tr><td>Console</td><td><a href=\"${env.BUILD_URL}console\">End of Console</a></td></tr>\
    <tr><td></td><td><a href=\"${env.BUILD_URL}consoleFull\">Full Console Output</a> </td></tr>\
    <tr><td>Dashboards</td><td><a href=\"https://esso.secure.fedex.com/sefs-dashboard/cicd\">Live Data Dashboard</a> </td></tr>\
    <tr><td></td><td><a href=\"${env.JENKINS_URL}/blue/organizations/jenkins/${bluepath}/detail/${BRANCH_NAME}/${BUILD_NUMBER}/pipeline\">Pipeline Workflow</a>(blue ocean - ${step}).</td></tr>\
    <tr><td></td><td><a href=\"https://sonar.prod.cloud.fedex.com:9443/dashboard?id=${sonarProjectKey}\">Sonar Project Link</a> </td></tr>\
    <tr><td></td><td><a href=\"https://splunk.prod.fedex.com/en-US/app/splunk_app_jenkins/overview\">Jenkins Statistics</a> (req. splunk_user_app35300768) </td></tr>\
    <tr><td>Channels</td><td><a href=\"" + Definitions.SEFS_ST_CICD_CHANNEL + "\">System Team Teams Channel</a> </td></tr>\
    <tr><td></td><td><a href=\"https://mattermost.paas.fedex.com/systemteam/channels/town-square\">Mattermost System Team Town Square</a> </td></tr>\
    </table>"

   if ("${emailTo}" != "" && "${emailTo}" != "null") {
      mail mimeType: 'text/html', subject: "${subject}",
        body: "${icon} ${env.JOB_NAME}#${env.BUILD_NUMBER}\n[${step} ${status} ${icon}] :<br> ${body}",
                to: "${emailTo}",
                replyTo: 'no-reply@jenkins-shipment.web.fedex.com',
                from: 'no-reply@jenkins-shipment.web.fedex.com'
   }

    post_to_teams("${env.JOB_NAME}", color, "${body}", "${icon} ${env.JOB_NAME}#${env.BUILD_NUMBER} [${step} ${status} ${icon}]")


    if (setup != null) {
      echo "Sonar: https://sonar.prod.cloud.fedex.com:9443/dashboard?id=${sonarProjectKey}"
    }

    try {
       sh(returnStdout: true, script: "curl -s -f -d '${build_history}'  -X POST '" + Definitions.LDD_HOOK_URL + "/api/public/build/update' -H 'Content-Type: application/json'")
    } catch (error_x) {}
    try {
      unstash "build.prepare"
      //overwrite status for analytics
      status = "${currentBuild.result}"
    } catch (error_x) {}
}


def post_to_teams(jobname, color, message, status) {
  def teamsHooksJson = libraryResource 'teams-hook-definition-creator.json'
  def webhook = Definitions.teamsHooks(jobname, teamsHooksJson)
  if (webhook != Definitions.SEFS_ST_CICD_TEAMS_HOOK_URL) {
    // send to Domain Channels for SEFS
    echo "Found the webhook for ${jobname} = \n${webhook}"
    office365ConnectorSend( color: color,  message: message,    status:     status,      webhookUrl: webhook)
  }
  // send to public CICD channel for SEFS
  office365ConnectorSend( color: color,  message: message,    status:     status,      webhookUrl: Definitions.SEFS_ST_CICD_TEAMS_HOOK_URL)

}
