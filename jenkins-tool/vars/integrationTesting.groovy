import com.fedex.ci.*
import java.io.*
// vars/IntegrationTesting.groovy
def call(com.fedex.ci.Definitions setup) {
	try {
		unstash name : 'ittest.flag'
	} catch (Exception e) {
		Log.info(this,"no ittest ${e}")
	}
	def tests_disabled = "${env.DISABLE_ITTEST}" != "null" && "${env.DISABLE_ITTEST}" != "" && "${env.DISABLE_ITTEST}" != "false"
	Log.echoBuildStatus(this, currentBuild, "entering of integration testing (disabled=${tests_disabled})")
	if (fileExists(".ittest") && !tests_disabled) {
		echo "\u2705 \u2705 \u2705 Integration Testing \u2705 \u2705 \u2705"
		def props = readProperties file: '.ittest'
		props['artifactId']=setup.artifactId
		props['groupId']=setup.groupId
		props['version']=setup.version
		props['versionBase']=setup.versionBase
		Log.debug(this, "${props}")
		if (props['ittest.library.repository'] != null) {
			library identifier: props['ittest.library.identifier'], retriever: modernSCM(
			 		[$class: 'GitSCMSource', remote: props['ittest.library.repository'], credentialsId: props['ittest.library.creds']])
			externalItTestProxy(setup,props)
		} else {
			itTestProxy(props)
		}
	} else {
		if (tests_disabled) {
			Log.warn(this, "DISABLE_ITTEST has been set, skipping integration tests." )
		}
	}
	Log.echoBuildStatus(this, currentBuild, "end of integration testing")
}
