import com.fedex.ci.*

import java.io.*
import com.fedex.ci.*

// vars/builder.groovy
def call( com.fedex.ci.Definitions setup) {
	if (fileExists("vars/checkoutHook.groovy")) {
	  checkoutHook = load 'vars/checkoutHook.groovy'
		echo "\u2705 \u2705 \u2705 \u2705 \u2705 \u2705 Start Custom checkoutHook \u2705 \u2705 \u2705 \u2705 \u2705 \u2705"
		def props = readProperties file: '.build'
		checkoutHook(setup, props)
		echo "\u2705 \u2705 \u2705 \u2705 \u2705 \u2705 End Custom checkoutHook \u2705 \u2705 \u2705 \u2705 \u2705 \u2705"
	}
}
