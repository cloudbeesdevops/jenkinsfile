import groovy.json.*
import com.fedex.ci.*
import com.fedex.jenkins.*

def call(Map config=[:] ) {
   def setup = config.setup
   def fd_project_info = []
   def fd_project_id = 'default'
   def fd_release_tag = 'master'
   def pcf_user = ''
   def pcf_password = ''
   def fd_install_info = config.fdinstall
   def fd_credentials = config.credentials ?: "test_deploy_user"
   def fd_svr = config.fdsvr ?: "https://nexus.prod.cloud.fedex.com:8443"

   if (config.fdinstall != null && config.fdinstall.contains('.git')) {
      if (config.fdinstall.contains('->')) {
         fd_install_info = config.fdinstall.split('->')[0]
         fd_release_tag = config.fdinstall.split('->')[1]
      } else {
         fd_install_info = config.fdinstall
      }
      try { fd_release_tag =  "${env.RELEASE_BRANCH}" == 'null' || "${env.RELEASE_BRANCH}" == '' ? props['fd_release_tag'] : "${env.RELEASE_BRANCH}" } catch (exception) { }
      __fd_install_info = fd_install_info.split('/')
      fd_project_id = __fd_install_info[__fd_install_info.length-1]
      fd_project_id = fd_project_id.replaceAll('.git','')
      // use a different default when dealing with git.
      fd_credentials = config.credentials ?: 'gitlab-user-for-master'
   }
   else {
      if (config.fdinstall == null) {
         error("FD_INSTALL_INFO format null was detected 'projectid:groupid:artifactid:version' or git repository reference")
      }
      try { fd_project_info = config.fdinstall.trim().split(":") } catch (error_x) {}
      if (fd_project_info.length != 5 || config.fdinstall.contains(".git")) {
         error("FD_INSTALL_INFO format ${fd_project_info} 'projectid:groupid:artifactid:version' or git repository reference")
      }
      fd_release_tag = fd_project_info[3]
      fd_project_id = fd_project_info[0]
      fd_install_info = config.fdinstall
   }
  try {
   unstash name: 'build.prepare'
   echo " RN=${fd_project_info} | rn=${fd_release_tag}  "
   echo ""+
   "fd_project_id\t\t= ${fd_project_id}\n"+
   "fd_install_info\t\t= ${fd_install_info}\n"+
   "fd_release_tag\t\t= ${fd_release_tag}\n" +
   "fd_level\t\t= ${config.fdlevel}\n"+
   "fd_json\t\t= ${config.fdjson}\n" +
   "fd_credentials\t\t= ${config.credentials}\n" +
   "fd_action\t\t= ${config.fdaction}\n"
   "fd_domain_deploy\t\t= ${config.jobslist}\n"

   echo "\u2705 \u2705 \u2705 DEPLOYING \u2705 \u2705 \u2705"
   def status = 0
   withEnv([
      "FD_PROJECT_ID=${fd_project_id}",
      "FD_PROJECT_INFO=${fd_project_info}",
      "FD_INSTALL_INFO=${fd_install_info}",
      "FD_SVR=${fd_svr}",
      "FD_JSON=${config.fdjson}",
      "FD_ACTION=${config.fdaction}",
      "PATH+CF_CLI_PATH=${env.CF_CLI_PATH}",
      "PROJECT_VERSION=${fd_version}",
      "FD_CREDS=${fd_credentials}",
      "PCF_USER=${pcf_user}",
      "PCF_PASSWORD=${pcf_password}",
      "PYTHONPATH=/var/tmp/tools/usr/lib64/python2.6/site-packages/:~/.local/lib/python2.7/site-packages"
   ]) {
      if (!fileExists('fdeploy.py')) {
        unstash 'build.prepare'
      }
      // install fdeploy engine
      if ("${fd_install_info}".contains(".git")) {
         sshagent(['gitlab-user-for-master']) {
            // checkout the deployment items
            echo "checkout ${fd_release_tag}@${fd_install_info} to config.${fd_project_id}"
            checkout([$class: 'GitSCM',
               branches: [[name: "*/${fd_release_tag}"]],
               doGenerateSubmoduleConfigurations: false,
               poll: false,
               extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "config.${fd_project_id}"]],
               submoduleCfg: [],
               userRemoteConfigs: [[credentialsId: 'gitlab-user-for-master', url: "${fd_install_info}"]]]
            )
         }
      } else {
         // if we have a INSTALL INFO Nexus Style
         status = sh returnStatus: true, script: '''#!/bin/bash -l
   python -V
   python install.py -n ${FD_INSTALL_INFO}
   find . -type f -name '*.zip' -exec rm {} \\;
   #find `pwd`/config* -type f -maxdepth 2 2> /dev/null
   '''
              echo "status of nexus fdeploy install = ${status}"
         }
         // install fdeploy engine
         def selectLevel = "${config.fdlevel}".split(",");
         if (selectLevel.length == 0) {
            selectLevel = ["${config.fdlevel}"]
         }
         def credentials = [file(credentialsId: "${env.FD_CREDS}", variable: 'DEPLOY_USER_KEY')]
         def pcf_install_home = tool name : 'CF_CLIENT_INSTALL'
         withCredentials(credentials) {
            echo "levelArr=${selectLevel} / CF_CLIENT_INSTALL=${pcf_install_home}"
            for (int i=0;i<selectLevel.length;i++) {
               stage("Level\n" + selectLevel[i])
               if(FD_JSON.contains(",")){
			        FD_JSONS = FD_JSON.split(",")
			    }
			   else{
			   		FD_JSONS = [FD_JSON]
			   }
			    
               for (int j=0;j<FD_JSONS.size();j++) {
               	  def FD_PROJECT_JSON = FD_JSONS[j].trim()
                  withEnv([ "FD_LEVEL=${selectLevel[i]}", "PATH+CF_CLIENT_INSTALL=${pcf_install_home}", "FD_JSON=${FD_PROJECT_JSON}"]) {
                     echo "printing main"
                     echo "${FD_JSON}"
                     status = sh returnStatus: true, script: '''#!/bin/bash
   if [[ ${FD_ACTION} == *":"* ]]; then
      IFS=':' read -ra COMMAND <<< "${FD_ACTION}"
   else
      IFS='-' read -ra COMMAND <<< "${FD_ACTION}"
   fi
   set  | grep 'FD_\\|PATH'
   _section="${COMMAND[0]}"
   _action="${COMMAND[1]}"
   set -xv
   RC=""
   [[ -e "config.${FD_PROJECT_ID}/.fdeployrc" ]] && export RC="-rc config.${FD_PROJECT_ID}/.fdeployrc"
   python fdeploy.py --version
   sh post-install.sh
   time python fdeploy.py -v -i ~/.ssh/test_deploy_user_dsa -vvv ${_section} -a ${_action} -l "${FD_LEVEL}" -f config.${FD_PROJECT_ID}/${FD_JSON}
   finalExit=$(($finalExit + $?))
   set +xv
   exit ${finalExit}
   '''
                  } // withEnv
               }  // for refjson
               if (status != 0) {
                  error("\u274C\u274C\u274C\u274C FDeploy Deployment Descriptor JSON load failed for ${config.fdinstall} (errcd=${status}).\u274C\u274C\u274C\u274C")
               }
            } // for levels
         } // withCredentials
      } // withEnv
   } catch (Exception e) {
      echo "${e}"
      error(e.getMessage())
   }
}
