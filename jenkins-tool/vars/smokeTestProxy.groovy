import com.fedex.ci.*

// vars/smokeTestProxy.groovy
def call(Map config = [:], com.fedex.ci.Definitions setup ) {
	// evaluate the body block, and collect configuration into the object
  try {
    echo "$config"
    t =  readMavenPom file: ''
    def ARTIFACT_ID = config['artifactId']
    def branch = config['version']
    def VERSION = branch.tokenize("-")[0]
    def SUITE = config['SUITE_NAME']
    def LEVEL = config['level']
    Log.debug(this,"smokeTestProxy groovy file for loop : ${config}")
    withEnv([
      "ARTIFACT_ID=${ARTIFACT_ID}",
      "VERSION=${VERSION}",
      "SUITE=${SUITE}",
      "LEVEL=${LEVEL}",
      "https_proxy=http://internet.proxy.fedex.com:3128",
      "http_proxy=http://internet.proxy.fedex.com:3128"
    ]) {

      def response="400"
      def default_skip_smoke = false
      try { default_skip_smoke = "${env.SKIP_SMOKE}" == 'true'    } catch (error1) {}
      timeout(45) {
        response=sh returnStdout: true, script: '''wget -qO- http://199.82.226.78:8080/SEFSRestWS/rest/RegressionExecution/PIs/${SUITE}/${LEVEL}/${ARTIFACT_ID}/${VERSION}'''
        Log.compileInfo(this, "Smoke Test - Return Code : ${response}")
         return "${response}"
      }
      if ( response.toString().trim() == "200" || default_skip_smoke )
      {
        currentBuild.result='SUCCESS'
       Log.compileInfo(this, "SmokeTest Passed")
      }
      else
      {
       currentBuild.result='FAILURE'
       Log.compileErrorAndExit(this, "Deployment Failed!! Aborting build at deployment.")
        throw new Exception("Smoke Test Failed. Deployment Failed!! Aborting build at deployment.")
        error "Program failed, please read logs..."
      }
    }
  } catch (exc) {
    // email notification of malfunctioning pipeline
	notification(status : 'Failure', message: "failure: ${error_x}", context: "Test", setup: setup)	  
	currentBuild.result='FAILURE'
  }
  
  return "{response}"
}
