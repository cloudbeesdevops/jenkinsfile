package com.fedex.ci

// Add functions to register hooks and steps to this script.
this.metaClass.mixin(cucumber.api.groovy.Hooks)
this.metaClass.mixin(cucumber.api.groovy.EN)

// Define a world that represents the test environment.
// Hooks can set up and tear down the environment and steps
// can change its state, e.g. store values used by later steps.
class CustomWorld {
    def setup
    def name
    def Map flags = [:]

    String customMethod() {
        "foo"
    }
}

// Create a fresh new world object as the test environment for each scenario.
// Hooks and steps will belong to this object so can access its properties
// and methods directly.
World {
    new CustomWorld()
}

// This closure gets run before each scenario
// and has direct access to the new world object
// but can also make use of script variables.
Before() {
   env = [:]
   define = new Definitions() // belongs to this script
   define.env = env
   define.flagmap = [:]
   params = [:]
   varn = ''
   varv = ''
}

// Register another that also gets run before each scenario tagged with @notused.
Before("@notused") {
    throw new RuntimeException("Never happens")
}

// Register another that also gets run before each scenario tagged with
// (@notused or @important) and @alsonotused.
Before("(@notused or @important) and @alsonotused") {
    throw new RuntimeException("Never happens")
}

static isTrue(value) {
	value = value.trim()
	return value.contains('x') || value.contains('X')
}


static isSet(value) {
	return !"".equals(value) && !"null".equals(value) && !"false".equals(value)
}

Given(~/^I want to write a workflow with (.+)$/) { String name ->
	define.name = name
}

Given(~/^(\S+) (\S+) (\S+) (\S+) (\S+) flag file exists/) { String b, d, s, i, e  ->
	define.flagmap['build']  = isTrue(b)
   define.flagmap['deploy'] = isTrue(d)
   define.flagmap['smoketest'] = isTrue(s)
	define.flagmap['ittest'] = isTrue(i)
	define.flagmap['e2e']    = isTrue(e)
}
Given(/b=([-x]) d=([-x]) flag file exists/) {  b,d ->
	define.flagmap['build']  = isTrue(b)
	define.flagmap['deploy'] = isTrue(d)
}

Given(/flag files for smoketest, integration and e2e testing exist,/) { ->
	define.flagmap['smoketest'] = true
	define.flagmap['ittest'] = true
	define.flagmap['e2e']    = true
}


Given(/I want to write a workflow and test toString/) {  ->
   define.w.toString()
}


Given(~/^job type is set to (.+),$/) { String name ->
	env['JOB_TYPE']= name
}
Given(~/^application type is set to (.+),$/) { String name ->
	env['APP_TYPE']= name
}

Given(/perform release cut checkbox is checked, ([-x]),/) {  b ->
   params['PERFORM_RELEASE_CUT'] = isTrue(b)
}

Given(/NO_ADG is set -/) {  ->
// Write code here that turns the phrase above into concrete actions
   params['noADG'] = false
}


Given(/set parameter checkbox noADG ([x-]), release cut ([-x]),/) { String pnoadg, perfrc ->
    if (isSet(perfrc)) {
    	params['PERFORM_RELEASE_CUT'] = isTrue(perfrc)
    }
    if (isSet(pnoadg)) {
    	params['noADG'] = isTrue(pnoadg)
    }
7}

Given(/set env noADG variable ([-x])/) { String enoadg ->
	if (isSet(enoadg)) {
		env['noADG'] = isTrue(enoadg)
	}
}


When(/calculating the workflow/) {  ->
	define.setWorkflow(env, params)
}

Then(~/^I verify the (\S+), (\S+), (\S+) and (\S+) in step/) { String doci, docd, doct, dost ->
	assert define.doCI == isTrue(doci)
	assert define.doCD == isTrue(docd)
	assert define.doCT == isTrue(doct)
	assert define.doST == isTrue(dost)

}

Given(~/^with source analysis has source (\S+), has tests (\S+), has pom (\S+)/) { String s,t,p  ->
   define.hasCode = isTrue(s)
   define.hasTests = isTrue(t)
   define.hasPoms = isTrue(p)
}

Then(~/^stages for build (\S+), publish results (\S+), quality gate (\S+), upload snapshot (\S+)/) { String b,p,q,us ->
   assert define.doBuildStage() == isTrue(b)
   assert define.doPublishReportingStage() == isTrue(p)
   assert define.doQualityGate() == isTrue(q)
   assert define.doUploadSnapshotToNexus() == isTrue(us)
}

Then(~/^should we ask for user input, (\S+), auto deployment gate (\S+),/) { String ask,adg ->
   assert define.doAutoDeployGateStage() == isTrue(adg)
}

Then(~/^pre-smoketest ([-x]), deployment ([-x]), post smoketest ([-x]),/) { String p,d,s  ->
   assert define.doPreSmokeTestingStage() == isTrue(p)
   assert define.doDeploymentStage() == isTrue(d)
   assert define.doPostSmokeTestingStage() == isTrue(s)
}

Then(~/pre-smoketest <pre>, deployment ([-x]), post smoketest ([-x]),/) {  ->
   assert define.doDeploymentStage() == isTrue(d)
   assert define.doPostSmokeTestingStage() == isTrue(s)
}

Then(~/^deployment ([-x]), post smoketest ([-x]),/) { String d,s  ->
   assert define.doPreSmokeTest.on()
   assert define.doDeploymentStage() == isTrue(d)
   assert define.doPostSmokeTestingStage() == isTrue(s)
}
Then(~/^integration testing (\S+), end-to-end testing (\S+)/) { String i, e  ->
   assert define.doIntegrationTestingStage() == isTrue(i)
   assert define.doE2ETestingStage() == isTrue(e)
}

Then(/deployment ([-x]), use the autodeploygate ([-x]),/) { String deploy, autodeploygate ->
	assert define.doDeploymentStage() == isTrue(deploy)
	assert define.doAutoDeployGateStage() == isTrue(autodeploygate)
}

Then(~/^release gate ([-x]) enabled, do tagging ([-x]) and upload release to nexus ([-x])/) { String g,t,u ->
   assert define.doUploadReleaseToNexus.state == isTrue(u)
   assert define.doReleaseGate.state == isTrue(g)
   assert define.doTagging.state == isTrue(t)
}

Given(~/NO_SONAR is set ([-x])/) { String set ->
// Write code here that turns the phrase above into concrete actions
	if (isTrue(set))
		env['NO_SONAR'] = 'true'
}

Given(~/NO_LEGACY_PUB is set ([-x])/) { String set ->
   // Write code here that turns the phrase above into concrete actions
   if (isTrue(set)) {
   	define.doLegacyPublishing.on()
   }
   else {
      define.doLegacyPublishing.off()
   }
}
Given(~/NO_TESTING is set ([-x])/) { String set ->
// Write code here that turns the phrase above into concrete actions
if (isTrue(set))
	env['NO_TESTING'] = 'true'
}


Given(/sources and tests exists/) {  ->
   define.hasCode = true
   define.hasTests = true
   define.hasPoms = true
}

Then(~/^stages for build ([-x]), upload snapshot ([-x]),/) { String b, us ->
   assert define.doBuild.state == isTrue(b)
   assert define.doUploadSnapshotToNexus.state == isTrue(us)
}

Then(/new release is (.+) and (.+)/) { String version, String dorelease ->
	assert define.doRelease.state == isTrue(dorelease)
	assert define.doRelease() == isTrue(dorelease)
	//assert define.doUploadSnapshotToNexus.state == isTrue(us)
}



Then(~/before deployment we ask the autodeploy question ([-x]),/) { String adg ->
// Write code here that turns the phrase above into concrete actions
   assert define.doAutoDeployGateStage() == isTrue(adg)
}

Then(~/([-x]), ([-x]), ([-x]) publishing/) { String dosonar, dolegacy, dotesting ->
	assert define.doLegacyPublishing.state == isTrue(dolegacy)
	assert define.doSonar.state == isTrue(dosonar)
}

// Given(~/([-x]) auto deploygate param or ([-x]) is set,/) {  String bypass, noq ->
//    if (isTrue(bypass)) {
//       env['BY_PASS_AUTO_DEPLOY_GATE'] = 'true'
//    }
// }
Given(~/([-x]) auto deploygate param or quality gate is set to ([-x]),/) {  String bypass, noq ->
   if (isTrue(bypass)) {
      env['BY_PASS_AUTO_DEPLOY_GATE'] = 'true'
   }
   if (isTrue(noq)) {
      env['noQG'] = 'true'
   }
}
Given(~/that (.+) is set to (.+)/) { String v, b ->
	env[v] = "${b}"
	varn = v
	varv = b
	//print v + " ->" + env
}

When(~/^I validate I should see/) {  ->
	define.setWorkflow(env,env)
}



Then(~/^we assume (.+) is (.+)$/) { String v, b ->
	def varname = evaluate('return {"${define.' + v + '.state}" }')
	def envname = "${env['" + varn + "']}"
	//println("assume: env= " + envname + ", step.state: " + varname() + " - " + "${b} for step " + v)
	//assert varname().equals("${b}")
	def c = define.isSet(varname())
	def a = define.isSet("${env['varn']}")
	if (b) {
		assert a == c
	}
	else {
		assert a!= c
	}
}
