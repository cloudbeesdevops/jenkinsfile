package com.fedex.ci


class TestDefinitions {

  static Definitions d = null

  static getmap() {
    def flagmap = ["build": false, "deploy": false, "smoketest": false, "ittest": false, 'e2e': false, 'release': false]
    return flagmap
  }

  static void testDoCDBlockMissingDeployFlag() {
    // usage: groovy com/fedex/ci/TestDefinitions.groovy to run tests
    d = new com.fedex.ci.Definitions()
    def flagmap = getmap()
    assert flagmap['deploy'] == false
    def env = ['JOB_TYPE': 'CD']
    assert env.JOB_TYPE == 'CD'
    d.flagmap=flagmap
    d.setWorkflow(env, env)
    d.hasCode = true
    d.hasPoms = true
    assert !d.doAutoDeployGate
    assert !d.doPreSmokeTest
    assert !d.doDeployment
    assert !d.doPublishReportingStage()
    assert !d.doQualityGate()
    assert !d.askAutoDeployQuestion
    assert !d.doDeploymentStage()
    assert !d.doRelease
    assert !d.doPostSmokeTest
    assert !d.doITs
    assert !d.doE2Es
    assert !d.doCD
    assert !d.doCI
    assert !d.doCT
  }

  static void testDoCDwithDeployFlag() {
    def flagmap = getmap()
    def env = ['JOB_TYPE': 'CD']
    flagmap['deploy'] = true
    flagmap['build'] = true
    assert env.JOB_TYPE == 'CD'
    d = new com.fedex.ci.Definitions()
    d.flagmap = flagmap
    d.hasCode = true
    d.hasPoms = true
    d.setWorkflow(env, env)
    assert !d.doCI
    assert !d.doPublishReportingStage()
    assert !d.doQualityGate()
    assert d.doCD
    assert !d.doPreSmokeTest
    assert !d.askAutoDeployQuestion
    assert d.doDeployment
    assert !d.doPostSmokeTest
    assert !d.doITs
    assert !d.doE2Es
    assert !d.doRelease
    assert !d.doCT
  }

  static void testDoCDWithDeployAndSmoketest() {
    def flagmap = getmap()
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    def env = ['JOB_TYPE': 'CD']
    d = new Definitions()
    d.flagmap = flagmap
    assert env.JOB_TYPE == 'CD'
    d.setWorkflow(env, env)
    assert d.doPreSmokeTest
    assert !d.askAutoDeployQuestion
    assert d.doPostSmokeTest
    assert !d.doRelease
    assert !d.doPublishReportingStage()
    assert d.doDeployment
    assert !d.doITs
    assert !d.doE2Es
    assert !d.doRelease
  }

  static void testDoCTWithDeployAndSmoketest() {
    def flagmap = getmap()
    def env = ['JOB_TYPE': 'CT']
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    flagmap['ittest'] = true
    assert env.JOB_TYPE == 'CT'
    d = new Definitions()
    d.flagmap = flagmap
    d.setWorkflow(env, env)
    assert !d.doPreSmokeTest
    assert !d.askAutoDeployQuestion
    assert !d.doAutoDeployGate
    assert d.doPostSmokeTest
    assert d.doITs
    assert !d.doRelease
    assert !d.doPublishReportingStage()
    assert !d.doDeployment
    assert !d.doE2Es // no e2eflags
    assert !d.doRelease
  }
  static void testDoRCWithoutPerformReleaseCutChecked() {
    def flagmap = getmap()
    d = new Definitions()
    def env = ['JOB_TYPE': 'RC']
    def params = ['PERFORM_RELEASE_CUT' : false ]
    flagmap['build'] = true
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    assert env.JOB_TYPE == 'RC'
    d.hasPoms = true
    d.hasCode = true
    d.flagmap = flagmap
    d.setWorkflow(env, params)
    assert d.doCI
    assert !d.doPublishReportingStage()
    assert !d.doUploadSnapshotToNexus()
    assert !d.doCD
    assert !d.doCT

    assert !d.askAutoDeployQuestion
    assert !d.doPreSmokeTest
    assert !d.doDeployment
    assert !d.doPostSmokeTest
    assert !d.doITs
    assert !d.doE2Es
    assert d.doRelease
    assert d.doUploadReleaseToNexus()
  }
  static void testDoRCWithPerformReleaseCutChecked() {
    def flagmap = getmap()
    d = new Definitions()
    def env = ['JOB_TYPE': 'RC']
    def params = ['PERFORM_RELEASE_CUT' : true ]
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    assert env.JOB_TYPE == 'RC'
    d.flagmap = flagmap
    d.hasPoms = true
    d.setWorkflow(env, params)
    assert !d.doCI
    assert !d.doCD
    assert !d.doCT
    assert !d.doST
    d.flagmap['build']=true
    d.setWorkflow(env, params)
    assert d.doCI
    assert d.doPublishReportingStage()
    assert !d.doUploadSnapshotToNexus()
    assert !d.doCD
    assert !d.doCT
    assert !d.askAutoDeployQuestion
    assert !d.doPreSmokeTest
    assert !d.doDeployment
    assert !d.doPostSmokeTest
    assert !d.doITs
    assert !d.doE2Es
    assert d.doRelease
    assert d.doUploadReleaseToNexus()

  }
  static void testDoCICDWithDeploySmokeFlagAndReleaseCut() {
    def flagmap = getmap()
    def env = ['JOB_TYPE': 'CICD']
    def params = ['PERFORM_RELEASE_CUT' : true, 'JOB_TYPE': 'CICD' ]
    def d = new Definitions()
    flagmap['build']= true
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    flagmap['ittest'] = false
    d.flagmap = flagmap
    d.hasPoms = true
    d.hasTests = true
    d.hasCode = true
    assert env.JOB_TYPE == 'CICD'
    d.workflow_string = ""
    d.setWorkflow(env, params)
    assert d.doBuildStage()
    assert d.doCI
    assert d.doPublishReportingStage()
    assert !d.doUploadSnapshotToNexus()
    assert d.doCD
    assert !d.doCT
    assert d.askAutoDeployQuestion
    assert d.doPreSmokeTest
    assert d.doDeployment
    assert d.doPostSmokeTest
    assert !d.doITs // no flag file
    assert !d.doE2Es // no flag file
    assert d.doRelease
    assert d.doUploadReleaseToNexus()

    // no autodeploygate
    env['noADG'] = true
    d.setWorkflow(env, params)
    assert d.doCI
    assert d.doPublishReportingStage()
    assert !d.doUploadSnapshotToNexus()
    assert d.doCD
    assert !d.doCT
    assert !d.askAutoDeployQuestion
    assert d.doPreSmokeTest
    assert d.doDeployment
    assert d.doPostSmokeTest
    assert !d.doITs // no flag file
    assert !d.doE2Es // no flag file
    assert d.doRelease
    assert d.doUploadReleaseToNexus()
  }


  static void testDoCICDWithDSIAndByPassDeployGate() {
    def flagmap = getmap()
    def env = ['JOB_TYPE': 'CICD']
    def params = ['PERFORM_RELEASE_CUT' : false, 'JOB_TYPE': 'CICD', 'BY_PASS_AUTO_DEPLOYMENT_GATE' : true ]
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    flagmap['ittest'] = true
    assert env.JOB_TYPE == 'CICD'
    d = new Definitions(flagmap)
    d.hasPoms = true
    d.setWorkflow(env, params)
    assert !d.doAutoDeployGate
    assert d.doPreSmokeTest
    assert d.doDeployment
    assert d.doPostSmokeTest
    assert !d.doRelease
    assert !d.doPublishReportingStage()
    assert !d.askAutoDeployQuestion
    assert d.doITs
    assert !d.doE2Es
    assert d.doCD
    assert !d.doCI // .build file
    assert !d.doCT
  }
  static void testDoCICDWithDSIAndByPassByEnvsDeployGate() {
    def flagmap = getmap()
    def env = ['JOB_TYPE': 'CICD','noADG' : true]
    def params = ['PERFORM_RELEASE_CUT' : false, 'JOB_TYPE': 'CICD', 'BY_PASS_AUTO_DEPLOYMENT_GATE' : false ]
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    flagmap['ittest'] = true
    assert env.JOB_TYPE == 'CICD'
    d = new Definitions()
    d.flagmap = flagmap
    d.setWorkflow(env, params)
    assert d.doAutoDeployGate
    assert d.doPreSmokeTest
    assert d.doDeployment
    assert d.doPostSmokeTest
    assert !d.doRelease
    assert d.doPublishReportingStage()
    assert !d.askAutoDeployQuestion
    assert d.doITs
    assert !d.doE2Es
    assert d.doCD
    assert d.doCI
    assert !d.doCT
  }
  static void testAutoDeployGateMethod() {
    def flagmap = getmap()
    def env = ['noADG ': 'true', 'JOB_NAME': '', 'JOB_TYPE': 'CD']
    def params = ['BY_PASS_AUTO_DEPLOYMENT_GATE': 'false', 'JOB_NAME': '', 'JOB_TYPE': '']
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    flagmap['ittest'] = true
    assert env.JOB_TYPE == 'CD'
    d = new Definitions()
    d.flagmap = flagmap

    d.setWorkflow(env, params)
    assert !d.doAutoDeployGateStage()
    assert d.askAutoDeployQuestion == false
    assert d.doAutoDeployGate == true
    assert d.doPreSmokeTest
    assert d.doDeployment
    assert d.doPostSmokeTest
    assert !d.doRelease
    assert !d.doPublishReportingStage()
    assert d.doITs
    assert !d.doE2Es
    assert d.doCD
    assert !d.doCI
    assert !d.doCT
    params = ['noADG': true, 'JOB_NAME': '', 'JOB_TYPE': '']
    d.setWorkflow(env, params)
    assert !d.doAutoDeployGateStage()
    assert !d.askAutoDeployQuestion
    assert d.doAutoDeployGate == true
    assert d.doPreSmokeTest
    assert d.doDeployment
    assert d.doPostSmokeTest
    assert !d.doRelease
    assert !d.doPublishReportingStage()
    assert d.doITs
    assert !d.doE2Es
    assert d.doCD
    assert !d.doCI
    assert !d.doCT
  }
  static void testAutoDeployGateCICDMethod() {
    def flagmap = getmap()
    def env = ['noADG ': 'true', 'JOB_NAME': '', 'JOB_TYPE': 'CICD']
    def params = ['BY_PASS_AUTO_DEPLOYMENT_GATE': 'false', 'JOB_NAME': '', 'JOB_TYPE': '']
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    flagmap['ittest'] = true
    assert env.JOB_TYPE == 'CICD'
    d = new Definitions()
    d.flagmap = flagmap
    d.setWorkflow(env, params)
    assert !d.doAutoDeployGateStage()
    assert d.askAutoDeployQuestion == false
    assert d.doAutoDeployGate == true
    assert d.doPreSmokeTest
    assert d.doDeployment
    assert d.doPostSmokeTest
    assert !d.doRelease
    assert d.doPublishReportingStage()
    assert d.doITs
    assert !d.doE2Es
    assert d.doCD
    assert d.doCI
    assert !d.doCT
    params = ['noADG': true, 'JOB_NAME': '', 'JOB_TYPE': '']
    d.setWorkflow(env, params)
    assert d.doAutoDeployGateStage()
    assert !d.askAutoDeployQuestion
    assert d.doAutoDeployGate == true
    assert d.doPreSmokeTest
    assert d.doDeployment
    assert d.doPostSmokeTest
    assert !d.doRelease
    assert !d.doPublishReportingStage()
    assert d.doITs
    assert !d.doE2Es
  }

  static void test() {
    // CI track, no autodeploygate
    def flagmap = getmap()
    flagmap['deploy'] = true
    flagmap['smoketest'] = true
    flagmap['ittest'] = true
    def env = ['noADG ': 'true', 'JOB_NAME': '', 'JOB_TYPE': 'CD']
    def params = ['BY_PASS_AUTO_DEPLOYMENT_GATE': 'false', 'JOB_NAME': '', 'JOB_TYPE': 'CI']
    assert env.JOB_TYPE == 'CD'
    d = new Definitions()
    d.flagmap = flagmap
    d.hasTests = true
    d.hasCode = true
    d.hasPoms = true
    d.setWorkflow(env, params)

    assert d.doCI
    assert d.doPublishReportingStage()
    assert d.doUploadSnapshotToNexus()
    assert !d.doCD
    assert !d.doCT
    assert !d.askAutoDeployQuestion
    assert !d.doPreSmokeTest
    assert !d.doDeployment
    assert !d.doPostSmokeTest
    assert !d.doITs
    assert !d.doE2Es
    assert !d.doRelease
    assert !d.doUploadReleaseToNexus()


  }
  static void testCICD_noQG_noDG() {
    // CICD includes CT without quality gate and no deploygate
    def env = ['noADG': 'true', 'noQG': 'true', 'JOB_TYPE': 'CICD']
    def params = ['BY_PASS_AUTO_DEPLOYMENT_GATE': 'false', 'JOB_NAME': '', 'JOB_TYPE': 'CICD']
    def flagmap = getmap()
    flagmap['deploy'] = true
    d = new Definitions()
    d.flagmap = flagmap
    d.hasTests = true
    d.hasCode = true
    d.hasPoms = true
    d.setWorkflow(env, params)
    assert d.doCI == true
    assert d.doCD == true
    assert d.doCT == false
    assert d.doPublishReportingStage() == true
    assert !d.doQualityGate()
    assert !d.doAutoDeployGateStage()
    assert d.askAutoDeployQuestion == false
    assert d.doAutoDeployGate == true
    assert d.doSonar == true
    //d.setDoRelease('yes')
  }

  static void testST() {
    // CICD includes CT without quality gate and no deploygate
    def env = ['noADG': 'true', 'noQG': 'true', 'JOB_TYPE': 'ST']
    def params = ['BY_PASS_AUTO_DEPLOYMENT_GATE': 'false', 'JOB_NAME': '', 'JOB_TYPE': 'ST']
    def flagmap = getmap()
    d = new Definitions()
    d.flagmap = flagmap
    d.hasTests = true
    d.hasCode = true
    d.hasPoms = true
    d.setWorkflow(env, params)
    assert d.doCI == false
    assert d.doCD == false
    assert d.doCT == false
    assert d.doPostSmokeTest == true
    assert !d.doPublishReportingStage()
    assert !d.doQualityGate()
    assert !d.doAutoDeployGateStage()
    assert d.askAutoDeployQuestion == false
    assert !d.doAutoDeployGate == true
    assert d.doPostSmokeTestingStage() == true
    //d.setDoRelease('yes')
}


  static void testAppTypePom() {
    // CICD includes CT without quality gate and no deploygate
    def env = ['noADG': 'true', 'noQG': 'true', 'JOB_TYPE': 'CICD', 'APP_TYPE': 'POM']
    def params = ['BY_PASS_AUTO_DEPLOYMENT_GATE': 'false', 'JOB_NAME': '', 'JOB_TYPE': 'ST']
    def flagmap = getmap()
    flagmap['build']=true

    d = new Definitions()
    d.flagmap = flagmap
    d.hasTests = true
    d.hasCode = true
    d.hasPoms = true
    d.setWorkflow(env, params)
    assert d.doCI == true
    assert d.doCD == false
    assert d.doCT == false
    assert d.doPostSmokeTest == false
    assert !d.doPublishReportingStage()
    assert !d.doQualityGate()
    assert !d.doAutoDeployGateStage()
    assert d.askAutoDeployQuestion == false
    assert !d.doAutoDeployGate == true
    assert !d.doPostSmokeTestingStage() == true
    //d.setDoRelease('yes')
}


  static void main(String[] args) {
    // usage: groovy com/fedex/ci/TestDefinitions.groovy to run tests
    testAppTypePom()
    testDoCDwithDeployFlag()
    testDoCDBlockMissingDeployFlag()
    testDoCDWithDeployAndSmoketest()
    testDoCTWithDeployAndSmoketest()
    testDoRCWithoutPerformReleaseCutChecked()
    testDoRCWithPerformReleaseCutChecked()
    testDoCICDWithDeploySmokeFlagAndReleaseCut()
    // testDoCICDWithDSIAndByPassDeployGate()
    // testDoCICDWithDSIAndByPassByEnvsDeployGate()
    // testAutoDeployGateMethod()
    // test()
    // testCICD_noQG_noDG()
    // testST()
  }

}
