
Feature: Workflow Settings Env Vars
  I want to use this template for my feature file

  @tag2
  Scenario Outline: scenario # <case> with <name>
    Given I want to write a workflow with <name>
    And b=<b> d=<d> s=x i=x e=x flag file exists
    And sources and tests exists
    And job type is set to <jobtype>,
    And application type is set to JAVA,
    When calculating the workflow
    Then I verify the <doci>, <docd>, <doct> and <dost> in step
    And stages for build <bld>, upload snapshot <upsnap>,
    And deployment <depl>, post smoketest <post>,
    And integration testing <it>, end-to-end testing <e2e>
    And release gate <rlgt> enabled, do tagging <tag> and upload release to nexus <uprl>
    And new release is <version>

    Examples:
  | case |jobtype| b | d | r | perf RC | doci | docd | dorc | dost | bld  |upsnap| depl |post| it | e2e |rlgt|tag| uprl |version| name                 |
  | 01   |   CI  | - | - | - |     -   |  x   |  -   |  -   |  -   |  -   |  -   |   -  | -  |  - |  -  |  - | - |  -   |  1.0.0| 01 ci with no flag files |
  | 02   |   CI  | x | - | - |     -   |  x   |  -   |  -   |  -   |  x   |  x   |   -  | -  |  - |  -  |  - | - |  -   |  1.0.0| 02 ci with no flag files |
  | 03   |  CICD | x | - | - |     -   |  x   |  x   |  -   |  -   |  x   |  x   |   -  | -  |  - |  -  |  - | - |  -   |  1.0.0| 03 cicd with no flag files |
  | 04   |  CICD | x | x | x |     x   |  x   |  x   |  -   |  -   |  x   |  x   |   x  | x  |  x |  x  |  - | x |  x   |  1.0.0| 04 cicd with no flag files |
