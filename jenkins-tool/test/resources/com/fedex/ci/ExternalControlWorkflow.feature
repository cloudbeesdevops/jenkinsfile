
Feature: Workflow Settings Env Vars
  I want to use this template for my feature file


  @tag2
Scenario Outline:  scenario # <case> with <name> overrides from environment
  Given I want to write a workflow with <name>
  And <b> <d> <s> <i> <e> flag file exists
  And job type is set to <jobtype>,
  And application type is set to <apptype>,
  And NO_SONAR is set <no_s>
  And NO_LEGACY_PUB is set <no_l>
  And NO_TESTING is set <no_t>
  And <bypass> auto deploygate param or quality gate is set to <no_qg>,
  And with source analysis has source <hs>, has tests <ht>, has pom <hp>
  When calculating the workflow
  Then I verify the <doci>, <docd>, <doct> and <dost> in step
  And stages for build <bld>, publish results <pub>, quality gate <qgate>, upload snapshot <upsnap>,
  And before deployment we ask the autodeploy question <adg>,
  And <dosonar>, <nolegacy>, <notesting> publishing

    Examples:
   | case   | jobtype | apptype | b | d | s | i | e | no_l | no_s | no_t |no_qg | no_adg | bypass | nolegacy | dosonar | notesting | hs|ht|hp| doci | docd | doct | dost | bld  | pub  | qgate|upsnap | adg | uprel | name                 |
   | 01     |   CI    |   PCF   | - | - | - | - | - |  -   |   -  |   -  |   -  |   -    |    -   |    -     |   -     | -         | x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -    |  -  |    -  | ci with no flag files |
   | 01b    |   CI    |   PCF   | x | - | - | - | - |  -   |   -  |   -  |   -  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  x   |  x    |  -  |    -  | ci with no flag files |
   | 01c    |   CI    |   PCF   | x | - | - | - | - |  -   |   -  |   -  |   x  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 01d    |   CI    |   PCF   | x | - | - | - | - |  -   |   -  |   -  |   x  |   x    |    -   |    x     |   x     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 01e    |   CI    |   PCF   | x | - | - | - | - |  -   |   -  |   -  |   x  |        |    x   |    x     |   x     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |

   | 02     |   CI    |   PCF   | x | - | - | - | - |  x   |   -  |   -  |   -  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  x   |  x    |  -  |    -  | ci with no .build ~ noleg files |
   | 02b    |   CI    |   PCF   | x | - | - | - | - |  x   |   -  |   -  |   -  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  x   |  x    |  -  |    -  | ci with no flag files |
   | 02c    |   CI    |   PCF   | x | - | - | - | - |  x   |   -  |   -  |   x  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 02d    |   CI    |   PCF   | x | - | - | - | - |  x   |   -  |   -  |   x  |   x    |    -   |    x     |   x     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 02e    |   CI    |   PCF   | x | - | - | - | - |  x   |   -  |   -  |   x  |        |    x   |    x     |   x     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |

   | 03     |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   -  |   -  |   -    |    -   |    x     |   -     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no .bild files |
   | 03b    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   -  |   -  |   -    |    -   |    x     |   -     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 03c    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   -  |   x  |   -    |    -   |    x     |   -     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 03d    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   -  |   x  |   x    |    -   |    x     |   -     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 03e    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   -  |   x  |        |    x   |    x     |   -     | -         | x |x |x |  x   |  -   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |


   | 04     |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   x  |   -  |   -    |    -   |    x     |   -     | x         | x |x |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no .bild files |
   | 04b    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   x  |   -  |   -    |    -   |    x     |   -     | x         | x |x |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 04c    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   x  |   x  |   -    |    -   |    x     |   -     | x         | x |x |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 04d    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   x  |   x  |   x    |    -   |    x     |   -     | x         | x |x |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 04e    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   x  |   x  |        |    x   |    x     |   -     | x         | x |x |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |


   | 05     |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   -  |   -  |   -    |    -   |    x     |   -     | -         | - |- |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no .bild files |
   | 05b    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   x  |   -  |   -    |    -   |    x     |   -     | x         | - |- |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 05c    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   x  |   x  |   -    |    -   |    x     |   -     | x         | - |- |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 05d    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   x  |   x  |   x    |    -   |    x     |   -     | x         | - |- |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 05e    |   CI    |   PCF   | x | - | - | - | - |  x   |   x  |   x  |   x  |   x    |    x   |    x     |   -     | x         | - |- |x |  x   |  -   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |

   | 06     |   CICD  |   PCF   | - | - | - | - | - |  -   |   -  |   -  |   -  |   -    |    -   |    -     |   -     | -         | x |x |x |  x   |  x   |  -   |  -   |  -   |  -   |  -   |  -    |  -  |    -  | ci with no flag files |
   | 06b    |   CICD  |   PCF   | x | - | - | - | - |  -   |   -  |   -  |   -  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  x   |  x   |  x    |  -  |    -  | ci with no flag files |
   | 06c    |   CICD  |   PCF   | x | - | - | - | - |  -   |   -  |   -  |   x  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 06d    |   CICD  |   PCF   | x | - | - | - | - |  -   |   -  |   -  |   x  |   x    |    -   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 06e    |   CICD  |   PCF   | x | - | - | - | - |  -   |   -  |   -  |   x  |        |    x   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  x   |  -   |  x    |  -  |    -  | ci with no flag files |

   | 07b    |   CICD  |   PCF   | x | x | - | - | - |  -   |   -  |   -  |   -  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  x   |  x   |  x    |  x  |    -  | ci with no flag files |
   | 07c    |   CICD  |   PCF   | x | x | - | - | - |  -   |   -  |   -  |   x  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  x   |  -   |  x    |  x  |    -  | ci with no flag files |
   | 07d    |   CICD  |   PCF   | x | x | - | - | - |  -   |   -  |   -  |   x  |   x    |    -   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  x   |  -   |  x    |  x  |    -  | ci with no flag files |
   | 07e    |   CICD  |   PCF   | x | x | - | - | - |  -   |   -  |   -  |   x  |        |    x   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  x   |  -   |  x    |  x  |    -  | ci with no flag files |

   | 08     |   CICD  |   POM   | - | - | - | - | - |  -   |   -  |   -  |   -  |   -    |    -   |    -     |   -     | -         | x |x |x |  x   |  x   |  -   |  -   |  -   |  -   |  -   |  -    |  -  |    -  | ci with no flag files |
   | 08b    |   CICD  |   POM   | x | - | - | - | - |  -   |   -  |   -  |   -  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 08c    |   CICD  |   POM   | x | - | - | - | - |  -   |   -  |   -  |   x  |   -    |    -   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 08d    |   CICD  |   POM   | x | - | - | - | - |  -   |   -  |   -  |   x  |   x    |    -   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
   | 08e    |   CICD  |   POM   | x | - | - | - | - |  -   |   -  |   -  |   x  |        |    x   |    x     |   x     | -         | x |x |x |  x   |  x   |  -   |  -   |  x   |  -   |  -   |  x    |  -  |    -  | ci with no flag files |
