#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Workflow Settings
  I want to use this template for my feature file


  @tag2
  Scenario Outline: scenario # <case> with <name>
    Given I want to write a workflow with <name>
    And b=<b> d=<d> s=<s> i=<i> e=<e> flag file exists
    And job type is set to <jobtype>,
    And application type is set to <apptype>,
    And NO_SONAR is set -
    And NO_LEGACY_PUB is set -
    And NO_TESTING is set -
    And NO_ADG is set -
    And with source analysis has source <hs>, has tests <ht>, has pom <hp>
    When calculating the workflow
    Then I verify the <doci>, <docd>, <doct> and <dost> in step
    And stages for build <bld>, publish results <pub>, quality gate <qgate>, upload snapshot <upsnap>,
    And should we ask for user input, <ask>, auto deployment gate <adg>,
    And pre-smoketest <pre>, deployment <depl>, post smoketest <post>,
    And integration testing <it>, end-to-end testing <e2e>
    And release gate <rlgt> enabled, do tagging <tag> and upload release to nexus <uprl>

    Examples:
  | case   | jobtype | apptype | b | d | s | i | e | doci |hs|ht|hp| docd | doct | dost | bld  | pub  | qgate|upsnap| ask | adg |   pre |   depl | post | it |   e2e |   rlgt |   tag  | uprl | name                 |
  | 01     |   CI    |   PCF   | - | - | - | - | - |  x   |x |x |x |  -   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 01 ci with no flag files |
  | 02     |   CD    |   PCF   | - | - | - | - | - |  -   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 02 cd with no flag files |
  | 03     |   CT    |   PCF   | - | - | - | - | - |  -   |x |x |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 03 ct with no flag files |
  | 04     |   ST    |   PCF   | - | - | - | - | - |  -   |x |x |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 04 st with no flag files |

  | 05     |   CI    |   PCF   | x | - | - | - | - |  x   |x |x |x |  -   |  -   |  -   |  x   |  x   |  x   |  x   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 05 ci with .build only   |
  | 05 a   |   CI    |   PCF   | x | - | - | - | - |  x   |x |- |x |  -   |  -   |  -   |  x   |  x   |  x   |  x   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 05 a ci with .build only   |
  | 05 b   |   CI    |   PCF   | x | - | - | - | - |  x   |- |x |x |  -   |  -   |  -   |  x   |  -   |  -   |  x   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 05 b ci with .build only   |
  | 05 c   |   CI    |   PCF   | x | - | - | - | - |  x   |- |- |x |  -   |  -   |  -   |  x   |  -   |  -   |  x   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 05 c ci with .build only   |

  | 06     |  CICD   |   PCF   | x | - | - | - | - |  x   |x |x |x |  x   |  -   |  -   |  x   |  x   |  x   |  x   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 06 cicd with .build only   |
  | 06 a   |  CICD   |   PCF   | x | - | - | - | - |  x   |x |- |x |  x   |  -   |  -   |  x   |  x   |  x   |  x   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 05 a cicd with .build only   |
  | 06 b   |  CICD   |   PCF   | x | - | - | - | - |  x   |- |x |x |  x   |  -   |  -   |  x   |  x   |  -   |  x   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 05 b cicd with .build only   |
  | 06 c   |  CICD   |   PCF   | x | - | - | - | - |  x   |- |- |x |  x   |  -   |  -   |  x   |  -   |  -   |  x   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 05 c cicd with .build only   |
  | 06 d   |  CICD   |   POM   | x | - | - | - | - |  x   |- |- |x |  x   |  -   |  -   |  x   |  -   |  -   |  x   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 05 c cicd with .build only   |

  | 07     |   CD    |   PCF   | x | - | - | - | - |  -   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 07 cd with .build only   |
  | 08     |   CT    |   PCF   | x | - | - | - | - |  -   |x |x |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 08 ct with .build only   |
  | 09     |   ST    |   PCF   | x | - | - | - | - |  -   |x |x |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 09 st with .build only   |

  | 10     |   CI    |   PCF   | - | x | - | - | - |  x   |x |x |x |  -   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 10 ci with .deploy only  |
  | 11     |   CD    |   PCF   | - | x | - | - | - |  -   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    x   |  -   |  - |     - |     -  |    -   |  -   | 11 cd with .deploy only   |
  | 12     |  CICD   |   PCF   | - | x | - | - | - |  x   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    x   |  -   |  - |     - |     -  |    -   |  -   | 12 cicd with .deploy only  |
  | 13     |  CT     |   PCF   | - | x | - | - | - |  -   |x |x |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 13 ct with .deploy only  |
  | 14     |  ST     |   PCF   | - | x | - | - | - |  -   |x |x |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 14 st with .deploy only  |

  | 15     |   CI    |   PCF   | - | - | x | - | - |  x   |x |x |x |  -   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 15 ci with .smoketest only  |
  | 16     |   CD    |   PCF   | - | - | x | - | - |  -   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 16 cd with .smoketest only   |
  | 17     |  CICD   |   PCF   | - | - | x | - | - |  x   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 17 cicd with .smoketest only  |
  | 18     |  CT     |   PCF   | - | - | x | - | - |  -   |x |x |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  - |     - |     -  |    -   |  -   | 18 ct with .smoketest only  |
  | 19     |  ST     |   PCF   | - | - | x | - | - |  -   |x |x |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  - |     - |     -  |    -   |  -   | 19 st with .smoketest only  |

  | 20     |   CI    |   PCF   | - | - | - | x | - |  x   |x |x |x |  -   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 20 ci with .ittest only  |
  | 21     |   CD    |   PCF   | - | - | - | x | - |  -   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 21 cd with .ittest only   |
  | 22     |  CICD   |   PCF   | - | - | - | x | - |  x   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 22 cicd with .ittest only  |
  | 23     |  CT     |   PCF   | - | - | - | x | - |  -   |x |x |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  x |     - |     -  |    -   |  -   | 23 ct with .ittest only  |
  | 24     |  ST     |   PCF   | - | - | - | x | - |  -   |x |x |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    -  |    -   |  -   |  - |     - |     -  |    -   |  -   | 24 st with .ittest only  |

 # build and deploy defined  - CICD typical

  | 26     |  CICD   |   PCF   | x | x | - | - | - |  x   |x |x |x |  x   |  -   |  -   |  x   |  x   |  x   |  x   |  x  |  x  |    -  |    x   |  -   |  - |     - |     -  |    -   |  -   | 26 cicd with .build & .deploy   |
  | 26 a   |  CICD   |   PCF   | x | x | - | - | - |  x   |x |- |x |  x   |  -   |  -   |  x   |  x   |  x   |  x   |  x  |  x  |    -  |    x   |  -   |  - |     - |     -  |    -   |  -   | 26 a cicd with .build & .deploy no tests  |
  | 26 b   |  CICD   |   PCF   | x | x | - | - | - |  x   |- |x |x |  x   |  -   |  -   |  x   |  x   |  -   |  x   |  x  |  x  |    -  |    x   |  -   |  - |     - |     -  |    -   |  -   | 26 b cicd with .build & .deploy no source  |
  | 26 c   |  CICD   |   PCF   | x | x | - | - | - |  x   |- |- |x |  x   |  -   |  -   |  x   |  -   |  -   |  x   |  -  |  -  |    -  |    x   |  -   |  - |     - |     -  |    -   |  -   | 26 c cicd with .build & .deploy no source and tests  |
  | 26 d   |  CICD   |   POM   | x | x | - | - | - |  x   |- |- |x |  x   |  -   |  -   |  x   |  -   |  -   |  x   |  -  |  -  |    -  |    x   |  -   |  - |     - |     -  |    -   |  -   | 26 d cicd with .build & .deploy for POM  |
  | 26 e   |  CICD   |PCF-JAVA | x | x | - | - | - |  x   |x |x |x |  x   |  -   |  -   |  x   |  x   |  x   |  x   |  -  |  x  |    -  |    x   |  -   |  - |     - |     -  |    -   |  -   | 26 cicd with .build & .deploy PCF_JAVA   |


 # build and deploy smoke tests defined  - CICD + runtime testing

  | 27     |  CICD   |   PCF   | x | x | x | - | - |  x   |x |x |x |  x   |  -   |  -   |  x   |  x   |  x   |  x   |  x  |  x  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 27   cicd with .build & .deploy & .smoketest  |
  | 27 a   |  CICD   |   PCF   | x | x | x | - | - |  x   |x |- |x |  x   |  -   |  -   |  x   |  x   |  x   |  x   |  x  |  x  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 27 a cicd with .build & .deploy & .smoketest no tests  |
  | 27 b   |  CICD   |   PCF   | x | x | x | - | - |  x   |- |x |x |  x   |  -   |  -   |  x   |  x   |  -   |  x   |  x  |  x  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 27 b cicd with .build & .deploy & .smoketest no source  |
  | 27 c   |  CICD   |   PCF   | x | x | x | - | - |  x   |- |- |x |  x   |  -   |  -   |  x   |  -   |  -   |  x   |  -  |  -  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 27 c cicd with .build & .deploy & .smoketest no source and tests  |
  | 27 d   |  CICD   |   POM   | x | x | x | - | - |  x   |- |- |x |  x   |  -   |  -   |  x   |  -   |  -   |  x   |  -  |  -  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 27 d cicd with .build & .deploy & .smoketest   |

 # build and deploy smoke tests defined  - CD + runtime testing

  | 28     |    CD   |   PCF   | x | x | x | - | - |  -   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 28   cd with .build & .deploy & .smoketest  |
  | 28 a   |    CD   |   PCF   | x | x | x | - | - |  -   |x |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 28 a cd with .build & .deploy & .smoketest no tests  |
  | 28 b   |    CD   |   PCF   | x | x | x | - | - |  -   |- |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 28 b cd with .build & .deploy & .smoketest no source  |
  | 28 c   |    CD   |   PCF   | x | x | x | - | - |  -   |- |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 28 c cd with .build & .deploy & .smoketest no source and tests  |
  | 28 d   |    CD   |   POM   | x | x | x | - | - |  -   |- |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  - |     - |     -  |    -   |  -   | 28 d cd with .build & .deploy & .smoketest   |

# build and deploy smoke/it tests defined  - CD + runtime testing

  | 29     |    CD   |   PCF   | x | x | x | x | - |  -   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     - |     -  |    -   |  -   | 29   cd with .build & .deplo y & .smoketest & .ittest  |
  | 29 a   |    CD   |   PCF   | x | x | x | x | - |  -   |x |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     - |     -  |    -   |  -   | 29 a cd with .build & .deploy & .smoketest & .ittest no tests  |
  | 29 b   |    CD   |   PCF   | x | x | x | x | - |  -   |- |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     - |     -  |    -   |  -   | 29 b cd with .build & .deploy & .smoketest & .ittest no source  |
  | 29 c   |    CD   |   PCF   | x | x | x | x | - |  -   |- |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     - |     -  |    -   |  -   | 29 c cd with .build & .deploy & .smoketest & .ittest no source and tests  |
  | 29 d   |    CD   |   POM   | x | x | x | x | - |  -   |- |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     - |     -  |    -   |  -   | 29 d cd with .build & .deploy & .smoketest & .ittest   |


# build and deploy smoke/it tests defined  - CD + runtime testing

  | 30     |    CD   |   PCF   | x | x | x | x | x |  -   |x |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     x |     -  |    -   |  -   | 30   cd with .build & .deplo y & .smoketest & .ittest & .e2e  |
  | 30 a   |    CD   |   PCF   | x | x | x | x | x |  -   |x |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     x |     -  |    -   |  -   | 30 a cd with .build & .deploy & .smoketest & .ittest & .e2e no tests  |
  | 30 b   |    CD   |   PCF   | x | x | x | x | x |  -   |- |x |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     x |     -  |    -   |  -   | 30 b cd with .build & .deploy & .smoketest & .ittest & .e2e no source  |
  | 30 c   |    CD   |   PCF   | x | x | x | x | x |  -   |- |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     x |     -  |    -   |  -   | 30 c cd with .build & .deploy & .smoketest & .ittest & .e2e no source and tests  |
  | 30 d   |    CD   |   POM   | x | x | x | x | x |  -   |- |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     x |     -  |    -   |  -   | 30 d cd with .build & .deploy & .smoketest & .ittest & .e2e   |
  | 30 e   |    CD   |   PYTHON| x | x | x | x | x |  -   |x |- |x |  x   |  -   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    x   |  x   |  x |     x |     -  |    -   |  -   | 30 e cd with .build & .deploy & .smoketest & .ittest & .e2e on python  |

# build and deploy smoke/it tests defined  - CT + runtime testing

  | 31     |    CT   |   PCF   | x | x | x | x | x |  -   |x |x |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  x |     x |     -  |    -   |  -   | 31   CT with .build & .deplo y & .smoketest & .ittest & .e2e  |
  | 31 a   |    CT   |   PCF   | x | x | x | x | x |  -   |x |- |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  x |     x |     -  |    -   |  -   | 31 a CT with .build & .deploy & .smoketest & .ittest & .e2e no tests  |
  | 31 b   |    CT   |   PCF   | x | x | x | x | x |  -   |- |x |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  x |     x |     -  |    -   |  -   | 31 b CT with .build & .deploy & .smoketest & .ittest & .e2e no source  |
  | 31 c   |    CT   |   PCF   | x | x | x | x | x |  -   |- |- |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  x |     x |     -  |    -   |  -   | 31 c CT with .build & .deploy & .smoketest & .ittest & .e2e no source and tests  |
  | 31 d   |    CT   |   POM   | x | x | x | x | x |  -   |- |- |x |  -   |  x   |  -   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  x |     x |     -  |    -   |  -   | 31 d CT with .build & .deploy & .smoketest & .ittest & .e2e   |


# build and deploy smoke/it tests defined  - ST + runtime testing

  | 32     |    ST   |   PCF   | x | x | x | x | x |  -   |x |x |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  - |     - |     -  |    -   |  -   | 32   ST with .build & .deplo y & .smoketest & .ittest & .e2e  |
  | 32 a   |    ST   |   PCF   | x | x | x | x | x |  -   |x |- |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  - |     - |     -  |    -   |  -   | 32 a ST with .build & .deploy & .smoketest & .ittest & .e2e no tests  |
  | 32 b   |    ST   |   PCF   | x | x | x | x | x |  -   |- |x |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  - |     - |     -  |    -   |  -   | 32 b ST with .build & .deploy & .smoketest & .ittest & .e2e no source  |
  | 32 c   |    ST   |   PCF   | x | x | x | x | x |  -   |- |- |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  - |     - |     -  |    -   |  -   | 32 c ST with .build & .deploy & .smoketest & .ittest & .e2e no source and tests  |
  | 32 d   |    ST   |   POM   | x | x | x | x | x  |  -   |- |- |x |  -   |  -   |  x   |  -   |  -   |  -   |  -   |  -  |  -  |    x  |    -   |  -   |  - |     - |     -  |    -   |  -   | 32 d ST with .build & .deploy & .smoketest & .ittest & .e2e   |


   @tag2
   Scenario Outline: release scenario # <case> with <name>
     Given I want to write a workflow with <name>
     And b=<b> d=<d> flag file exists
     And flag files for smoketest, integration and e2e testing exist,
     And job type is set to <jobtype>,
     And sources and tests exists  
     And application type is set to JAVA,
     And perform release cut checkbox is checked, <perf_rc>,
     When calculating the workflow
     Then I verify the <doci>, <docd>, <doct> and <dost> in step
     And stages for build <bld>, upload snapshot <upsnap>,
     And deployment <depl>, post smoketest <post>,
     And integration testing <it>, end-to-end testing <e2e>
     And release gate <rlgt> enabled, do tagging <tag> and upload release to nexus <uprl>
     And new release is <version> and <dorelea>

     Examples:
   |  case |jobtype| b | d | perf_rc | doci | docd | dorc | dost | bld  |upsnap| depl |post| it | e2e |rlgt|tag| uprl |dorelea|version| name                 |
   | r01   |   CI  | - | - |    -    |  x   |  -   |  -   |  -   |  -   |  -   |   -  | -  |  - |  -  |  - | - |  -   |  -    |  -    | 01 ci with no flag files |
   | r02   |   CI  | x | - |    -    |  x   |  -   |  -   |  -   |  x   |  x   |   -  | -  |  - |  -  |  - | - |  -   |  -    |  -    | 02 ci with no flag files |
   | r03   |  CICD | x | - |    -    |  x   |  x   |  -   |  -   |  x   |  x   |   -  | -  |  - |  -  |  - | - |  -   |  -    |  -    | 03 cicd with no flag files |
   | r04   |  CICD | x | x |    -    |  x   |  x   |  -   |  -   |  x   |  x   |   x  | x  |  x |  x  |  - | - |  -   |  -    |  -    | 04 cicd with no flag files |

   | r01a  |   CI  | - | - |    x    |  x   |  -   |  -   |  -   |  -   |  -   |   -  | -  |  - |  -  |  - | - |  -   |  -    |  -    | 01a ci with no flag files, with user selecting PERFORM_RELEASE_CUT |
   | r02a  |   CI  | x | - |    x    |  x   |  -   |  -   |  -   |  x   |  -   |   -  | -  |  - |  -  |  - | x |  x   |  x    |  -    | 02a ci with no flag files, with user selecting PERFORM_RELEASE_CUT |
   | r03a  |  CICD | x | - |    x    |  x   |  x   |  -   |  -   |  x   |  -   |   -  | -  |  - |  -  |  - | x |  x   |  x    |  -    | 03a cicd with no flag files, with user selecting PERFORM_RELEASE_CUT |
   | r04a  |  CICD | x | x |    x    |  x   |  x   |  -   |  -   |  x   |  -   |   x  | x  |  x |  x  |  - | x |  x   |  x    |  -    | 04a cicd with no flag files, with user selecting PERFORM_RELEASE_CUT |
   | r05a  |  CD   | x | x |    -    |  -   |  x   |  -   |  -   |  -   |  -   |   x  | x  |  x |  x  |  - | - |  -   |  -    |  -    | 05a cd with no flag files, with user selecting PERFORM_RELEASE_CUT |
   

   | r01b  |   RC  | - | - |    -    |  -   |  -   |  x   |  -   |  -   |  -   |   -  | -  |  - |  -  |  - | - |  -   |  -    |  -    | 01b rc with no flag files, |
   | r02b  |   RC  | x | - |    -    |  -   |  -   |  x   |  -   |  x   |  -   |   -  | -  |  - |  -  |  - | x |  x   |  x    |  -    | 02b rc with no flag files, |
   | r03b  |   RC  | x | x |    -    |  -   |  -   |  x   |  -   |  x   |  -   |   -  | -  |  - |  -  |  - | x |  x   |  x    |  -    | 03b rc with no flag files, |
   
   
   
 
   @tag2
   Scenario Outline: autodeploy gate scenario # <case> with <name>
     Given I want to write a workflow with <name>
     And b=<b> d=<d> flag file exists
     And flag files for smoketest, integration and e2e testing exist,
     And job type is set to <jobtype>,
     And sources and tests exists  
     And application type is set to JAVA,
     And perform release cut checkbox is checked, <perf_rc>,
     And set parameter checkbox noADG <par_noADG>, release cut <perf_rc>,
     And set env noADG variable <env_noADG>
     And perform release cut checkbox is checked, <perf_rc>,
     When calculating the workflow
     Then I verify the <doci>, <docd>, <doct> and <dost> in step
     And stages for build <bld>, upload snapshot <upsnap>,
     And deployment <depl>, use the autodeploygate <autodeploygate>,

     Examples:
   |  case |jobtype| b | d |env_noADG|par_noADG| perf_rc | doci | docd | dorc | dost | bld  |upsnap| depl |autodeploygate | name                 |
   | a01   |   CI  | - | - |    -    |    -    |    -    |  x   |  -   |  -   |  -   |  -   |  -   |   -  | -             | 01 ci with no flag files |
   | a02   |   CI  | x | x |    -    |    -    |    -    |  x   |  -   |  -   |  -   |  x   |  x   |   -  | -             | 02 ci with .deploy and .build files |
   | a03   |  CICD | x | - |    -    |    -    |    -    |  x   |  x   |  -   |  -   |  x   |  x   |   -  | -             | 03 cicd with no flag files |
   | a04   |  CICD | x | x |    -    |    -    |    -    |  x   |  x   |  -   |  -   |  x   |  x   |   x  | x             | 04 cicd with no flag files |
   | a05   |  CICD | x | x |    -    |    x    |    -    |  x   |  x   |  -   |  -   |  x   |  x   |   x  | -             | 05 cicd with no flag files |
   | a06   |  CICD | x | x |    x    |    -    |    -    |  x   |  x   |  -   |  -   |  x   |  x   |   x  | -             | 06 cicd with no flag files |
   | a07   |  CICD | x | x |    x    |    x    |    -    |  x   |  x   |  -   |  -   |  x   |  x   |   x  | -             | 07 cicd with no flag files |

   | a01a  |   CI  | - | - |    x    |    x    |    x    |  x   |  -   |  -   |  -   |  -   |  -   |   -  | -             | 01a ci with no flag files, with user selecting PERFORM_RELEASE_CUT |
   | a02a  |   CI  | x | - |    x    |    x    |    x    |  x   |  -   |  -   |  -   |  x   |  -   |   -  | -             | 02a ci with no flag files, with user selecting PERFORM_RELEASE_CUT |
   | a03a  |  CICD | x | - |    x    |    x    |    x    |  x   |  x   |  -   |  -   |  x   |  -   |   -  | -             | 03a cicd with no flag files, with user selecting PERFORM_RELEASE_CUT |
   | a04a  |  CICD | x | x |    x    |    x    |    x    |  x   |  x   |  -   |  -   |  x   |  -   |   x  | -             | 04a cicd with no flag files, with user selecting PERFORM_RELEASE_CUT |
   | a05a  |  CD   | x | x |    -    |    -    |    -    |  -   |  x   |  -   |  -   |  -   |  -   |   x  | -             | 05a cd with no flag files, with user selecting PERFORM_RELEASE_CUT |
   
   
   
   
   
   
   
   
   
      
