#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Testing methods in definitoion
  I want to use this template for my feature file


  @tag2
  Scenario: Title of your scenario outline
		Given that noQG is set to true
		When I validate I should see 
		Then we assume doQualityGate is false
		
  @tag3
  Scenario: Title of your scenario outline
		Given that noQG is set to null
		When I validate I should see 
		Then we assume doQualityGate is true		
		
		
  @tag3
  Scenario: Title of your scenario outline
		Given that noQG is set to ''
		When I validate I should see 
		Then we assume doQualityGate is true		
		
  @tag3
  Scenario: Title of your scenario outline
		Given that noQG is set to YES
		When I validate I should see 
		Then we assume doQualityGate is false		
		