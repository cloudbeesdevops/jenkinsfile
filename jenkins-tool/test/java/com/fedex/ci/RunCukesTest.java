package com.fedex.ci;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
//@CucumberOptions(features = {"test/resources/com/fedex/ci/single.feature"}, glue= {"classpath:com.fedex.ci"} )
@CucumberOptions(features = {
"test/resources/com/fedex/ci/definition_methods.feature",
"test/resources/com/fedex/ci/Workflow.feature",
 //"test/resources/com/fedex/ci/single.feature",
  "test/resources/com/fedex/ci/ExternalControlWorkflow.feature",
 }, glue= {"classpath:com.fedex.ci"} )
public class RunCukesTest {
}
