
import json
hooks = {
    '3534681-SEFS_COMMON':
        'https://outlook.office.com/webhook/9f2bd5a1-809f-4fbb-afc6-f37377e61cb0@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/22e54680bb79423bb10be24b57f1a365/22e82a6b-9388-455e-926e-e72cf565ad9b',
    'HANDLING_UNIT-3534545':
        'https://outlook.office.com/webhook/9f2bd5a1-809f-4fbb-afc6-f37377e61cb0@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/00411e2c5d9f41c28e41ee9df134f794/22e82a6b-9388-455e-926e-e72cf565ad9b',
    'TRIP-3534546':
        'https://outlook.office.com/webhook/9f2bd5a1-809f-4fbb-afc6-f37377e61cb0@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/83b7277e51cd4107a67c124e1b86e6c0/22e82a6b-9388-455e-926e-e72cf565ad9b',
    'SHIPMENT-3534550':
        'https://outlook.office.com/webhook/9f2bd5a1-809f-4fbb-afc6-f37377e61cb0@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/58fe47574c6a4dcaabd6504fdf826664/22e82a6b-9388-455e-926e-e72cf565ad9b',
    'WAYPOINT-3534587':
        'https://outlook.office.com/webhook/9f2bd5a1-809f-4fbb-afc6-f37377e61cb0@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/0f680f446a1d407f87c231660004a954/22e82a6b-9388-455e-926e-e72cf565ad9b',
    'ORCHESTRATION-3534641':
        'https://outlook.office.com/webhook/9f2bd5a1-809f-4fbb-afc6-f37377e61cb0@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/23cf4730ff3547009aa66a1a2b609d3e/22e82a6b-9388-455e-926e-e72cf565ad9b',
    'ITINERARY-3534642':
        'https://outlook.office.com/webhook/9f2bd5a1-809f-4fbb-afc6-f37377e61cb0@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/ff36e31d2519474ab068a99017488dd2/22e82a6b-9388-455e-926e-e72cf565ad9b',
}

print json.dumps(hooks, indent=3)
