def our_deploy(extra_build_options) {
    sh 'echo "Running on $(hostname)"'
    our_execute(Definitions.EXCLUDE_ALL_BUT_DEPLOY,'deploy')

}
def our_build(extra_build_options, goal) {
    our_execute( extra_build_options, goal)

}
def our_execute(extra_build_options, goal) {
    def javahome = tool name: 'JAVA_8', type: 'jdk'
    def m2home = tool name : Definitions.APACHE_MAVEN_VERSION
    withEnv(["JAVA_HOME=${javahome}",
        "M2_HOME=${m2home}", "PATH+MAVEN=${m2home}/bin:${javahome}/bin",
            "MAVEN_OPTS=-Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true \
-Xmx2048m -Xms2048m -Djava.security.egd=file:/dev/urandom -Dmaven.artifact.threads=10 ${extra_build_options}",
            "SONAR_SCANNER_OPTS=-Dhttps.proxyHost=internet.proxy.fedex.com -Dhttps.proxyPort=3128",
            "https_proxy=http://internet.proxy.fedex.com:3128",
            "http_proxy=http://internet.proxy.fedex.com:3128",
            "SONAR_AUTH_TOKEN=18cc609b8b470b5504756a3b9f8cd6853cdd1280",
"SONAR_CONFIG_NAME=SonarQube",
"SONAR_HOST_URL=https://sonar.prod.cloud.fedex.com:9443"
    ]) {
      sh 'set | tee a's
      withSonarQubeEnv('SonarQube') {
        sh 'set | tee b; diff -y a b'
        def cmd = "set -xv;set | grep HOME; set | grep NODE; set | grep PATH ;hostname ;${m2home}/bin/mvn -B -U -V -e -Dpipeline.run=true -s ./settings.xml ${extra_build_options} ${goal}"
        def buildSuccess = sh returnStatus: true, script: cmd
        if (buildSuccess != 0) {
            throw new Exception("STEP: Error while executing ${goal}")
        }
      }
    }
}

return this;
