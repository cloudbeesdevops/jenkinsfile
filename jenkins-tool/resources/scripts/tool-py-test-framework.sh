
#set -xv
INSTALL_PATH=${HOME}/local
export PATH=${PATH}:${INSTALL_PATH}/bin:${HOME}/.local/bin
echo $PATH
export https_proxy=http://internet.proxy.fedex.com:3128
export http_proxy=http://internet.proxy.fedex.com:3128
[[ ! -e "${INSTALL_PATH}" ]] && mkdir -p "${INSTALL_PATH}"
if [[ ! -e "${HOME}/.local//bin/pytest" ]]; then
  cd "${INSTALL_PATH}"
  hostname
  export GETPIP=https://bootstrap.pypa.io/get-pip.py
  [[ `python --version 2>&1 ` == *2.6* ]] && export GETPIP=https://bootstrap.pypa.io/2.6/get-pip.py
  export https_proxy=http://internet.proxy.fedex.com:3128
  export http_proxy=http://internet.proxy.fedex.com:3128
  curl -v -O "${GETPIP}"
  python get-pip.py --user --proxy="http://internet.proxy.fedex.com:3128"
  pip install pytest-cov --user
fi
pip install --upgrade pip --user
pip install python-dateutil --user

if [[ ! -e "${HOME}/.local/bin/twine" ]]; then
  pip install twine --user
fi

if [[ ! -e "${HOME}/.pypirc" ]]; then
cat << EOF > ~/.pypirc
[distutils]
index-servers =
  pypi
  c0009869
[pypi]
username:
password:
[c0009869]
repository: http://10.249.23.232:58080
username: admin
password: blabla
EOF
fi

cat <<EOF > requirements.txt
--index-url https://pypi.python.org/simple/
--extra-index-url http://c0009869.test.cloud.fedex.com:58080
--trusted c0009869.test.cloud.fedex.com:58080

requests>=2.6.0
requests_cache>=0.4.3
dnspython==1.15.0
argparse>=1.2.1
glob2==0.6
fnexus==1.1.0
six==1.11.0
EOF
echo "install fnexus upgrade"
pip -q install --upgrade --force-reinstall -i http://c0009869.test.cloud.fedex.com:58080 fnexus --user --trusted c0009869.test.cloud.fedex.com
pip -q install jasypt4py --user --no-deps

pip -q install -r requirements.txt --user


install_cryptology() {
  cd /var/tmp/tools
  export http_proxy=http://internet.proxy.fedex.com:3128
  export https_proxy=http://internet.proxy.fedex.com:3128
  PACKAGE=$(echo $(uname -r) |  awk -F. '{ print "https://nexus.prod.cloud.fedex.com:8443/nexus/repository/SEFS-6270-releases/python-crypto/python-crypto/2.6.1-1."$6"."$7"/python-crypto-2.6.1-1."$6"."$7".rpm" }')
  set -xv
  curl -o python-crypto.rpm  "${PACKAGE}"
  rpm2cpio python-crypto.rpm | cpio -idv
  rm python-crypto.rpm
}


set -xv
if [[ ! -e /var/tmp/tools/usr/lib64/python2.7/site-packages/Crypto ]] && [[ ! -e /var/tmp/tools/usr/lib64/python2.6/site-packages/Crypto ]]; then
  install_cryptology
fi

#pip freeze
