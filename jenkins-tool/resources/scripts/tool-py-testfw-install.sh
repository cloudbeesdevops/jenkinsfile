#set -xv
INSTALL_PATH=${HOME}/local
export PATH=${PATH}:${INSTALL_PATH}/bin:${HOME}/.local/bin
echo $PATH
export https_proxy=http://internet.proxy.fedex.com:3128
export http_proxy=http://internet.proxy.fedex.com:3128
[[ ! -e "${INSTALL_PATH}" ]] && mkdir -p "${INSTALL_PATH}"
if [[ ! -e "${INSTALL_PATH}/bin/pytest" ]]; then
  cd "${INSTALL_PATH}"
  hostname
  export GETPIP=https://bootstrap.pypa.io/get-pip.py
  [[ `python --version 2>&1 ` == *2.6* ]] && export GETPIP=https://bootstrap.pypa.io/2.6/get-pip.py
  export https_proxy=http://internet.proxy.fedex.com:3128
  export http_proxy=http://internet.proxy.fedex.com:3128
  curl -v -O "${GETPIP}"
  python get-pip.py --user --proxy="http://internet.proxy.fedex.com:3128"
  pip install pytest-cov --user
fi
pip install --upgrade pip
pip install python-dateutil --user

if [[ ! -e "${HOME}/.local/bin/twine" ]]; then
  pip install twine --user
fi

if [[ ! -e "${HOME}/.pypirc" ]]; then
cat << EOF > ~/.pypirc
[distutils]
index-servers =
  pypi
  urh00614
[pypi]
username:
password:
[urh00614]
repository: http://10.255.60.124:58080
username: admin
password: blabla
EOF
fi

if [[ -z `python -c "import fnexus"` ]]; then
pip install -i http://urh00614.ute.fedex.com:58080 fnexus --user --trusted urh00614.ute.fedex.com
fi

install_cryptology() {
  cd /var/tmp/tools
  export http_proxy=http://internet.proxy.fedex.com:3128
  export https_proxy=http://internet.proxy.fedex.com:3128
  PACKAGE=$(echo $(uname -r) |  awk -F. '{ print "https://nexus.prod.cloud.fedex.com:8443/nexus/repository/SEFS-6270/python-crypto/python-crypto/2.6.1-1."$6"."$7"/python-crypto-2.6.1-1."$6"."$7".rpm" }')
  set -xv
  curl -o python-crypto.rpm  "${PACKAGE}"
  rpm2cpio python-crypto.rpm | cpio -idv
  rm python-crypto.rpm
}


set -xv
if [[ ! -e /var/tmp/tools/usr/lib64/python2.7/site-packages/Crypto ]] && [[ ! -e /var/tmp/tools/usr/lib64/python2.6/site-packages/Crypto ]]; then
  install_cryptology
fi
