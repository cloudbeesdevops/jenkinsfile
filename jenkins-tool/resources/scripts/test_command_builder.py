# -*- coding: utf-8 -*-

import logging
from command_builder import main

POM = '''<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
    http://maven.apache.org/maven-v4_0_0.xsd">
    <groupId>xopco.common.tools</groupId>
    <artifactId>xopco-common-tools-fdeploy</artifactId>
    <build>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
  </project>
'''
POM_WITH_JACOCO = '''<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
    http://maven.apache.org/maven-v4_0_0.xsd">
    <groupId>xopco.common.tools</groupId>
    <artifactId>xopco-common-tools-fdeploy</artifactId>
    <build>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
             </plugin>
        </plugins>
    </build>
  </project>
'''
POM_BW = '''<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
    http://maven.apache.org/maven-v4_0_0.xsd">
    <groupId>xopco.common.tools</groupId>
    <artifactId>xopco-common-tools-fdeploy</artifactId>
    <build>
        <plugins>
            <plugin>
                <groupId>org.planet</groupId>
                <artifactId>tibco-build-plugin</artifactId>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
             </plugin>
        </plugins>
    </build>
  </project>
'''


class TestCommandBuilder(object):



    def test_get_artifactId(self):
        with open('test.txt', 'w') as f:
            f.write(POM)
        assert 'clean org.jacoco:jacoco-maven-plugin:0.8.1:prepare-agent deploy org.jacoco:jacoco-maven-plugin:0.8.1:report  pmd:pmd -Daggregate=true -Dpmd.typeResolution=false  org.codehaus.mojo:findbugs-maven-plugin:3.0.5:findbugs' == main('test.txt')
    def test_bw_tester(self):
        with open('test.txt', 'w') as f:
            f.write(POM_BW)
        assert 'clean -Punittest deploy' == main('test.txt')
    def test_with_jacoco(self):
        with open('test.txt', 'w') as f:
            f.write(POM_WITH_JACOCO)
        assert 'clean pmd:pmd -Daggregate=true -Dpmd.typeResolution=false deploy org.codehaus.mojo:findbugs-maven-plugin:3.0.5:findbugs' == main('test.txt')
