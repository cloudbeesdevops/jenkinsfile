# -*- coding: utf-8 -*-

import unittest
import logging
from gitlab_admin_functions import get_artifactId, get_sonar_project_ref, \
    get_sonar_project_ref,job2jobUrl,job2jobName

class TestGitlabFunctions(unittest.TestCase):

    def test_get_artifactId(self):
        s= get_artifactId('../../pom.xml')
        self.assertEquals('sefs-jenkins-shared-library', s)
        s= get_sonar_project_ref(s  )
        self.assertEquals('SEFS_JENKINS_SHARED_LIBRARY_SEFS_JENKINS_SHARED_LIBRARY-SEFS_JENKINS_SHARED_LIBRARY', s)

    def test_get_sonar_project_ref_without_sefsprefix(self):
        s= get_artifactId('../../test/resources/pom.xml')
        self.assertEquals('dashboard-parent', s)
        s= get_sonar_project_ref(s)
        self.assertEquals('SEFS_DASHBOARD_PARENT_DASHBOARD_PARENT-DASHBOARD_PARENT', s)

    def test_name_to_path(self):
        self.assertEquals('https://jenkins-shipment.web.fedex.com:8443/jenkins/job/3534596-FDEPLOY/job/XOPCO_COMMON_TOOLS_FDEPLOY/job/master', job2jobUrl('https://jenkins-shipment.web.fedex.com:8443/jenkins/job/3534596-FDEPLOY/job/XOPCO_COMMON_TOOLS_FDEPLOY/job/master'))
        self.assertEquals('https://jenkins-shipment.web.fedex.com:8443/jenkins/job/3534596-FDEPLOY/job/XOPCO_COMMON_TOOLS_FDEPLOY/job/master', job2jobUrl('3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master'))
        self.assertEquals('https://jenkins-shipment.web.fedex.com:8443/jenkins/job/3534596-FDEPLOY/job/XOPCO_COMMON_TOOLS_FDEPLOY/job/master', job2jobUrl('3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master/'))

        self.assertEquals('3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master', job2jobName('https://jenkins-shipment.web.fedex.com:8443/jenkins/job/3534596-FDEPLOY/job/XOPCO_COMMON_TOOLS_FDEPLOY/job/master'))
        self.assertEquals('3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master', job2jobName('https://jenkins-shipment.web.fedex.com:8443/jenkins/job/3534596-FDEPLOY/job/XOPCO_COMMON_TOOLS_FDEPLOY/job/master/'))
        self.assertEquals('3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master', job2jobName('3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master'))
        self.assertEquals('3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master', job2jobName('3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master/'))
