import json
from xml.etree import ElementTree as ET
import re
import sys
import os
import glob
import argparse
import logging

def mkdirs(path):
    try:
        os.makedirs(path)
    except:
        pass
def read_all(file):
    if os.path.exists(file):
        with open(file) as f:
            xmlstring = f.read()
            xmlstring = re.sub(r'\sxmlns="[^"]+"', '', xmlstring, count=1)
        return ET.fromstring(xmlstring)
    else:
        raise Exception("%s does not exist." % (file))
def get_tag(p, path):
    for char in p.findall(path):
        return char.text
def write_all(file,p):
    path = os.path.dirname(file)
    mkdirs(path)
    with open(file, "wb") as pom:
        pom.write(ET.tostring(p))
def alter_tag(p, paths,value):
    for path in paths:
        for char in p.findall(path):
            char.text = value

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', action='store', help='Version.')
    parser.add_argument('-p', '--pom', default='pom.xml' ,action='store', help='path.')
    # parse options and arguments and save groups for menu operations
    options = parser.parse_args(sys.argv[1:])
    if options.pom is None or os.path.exists(options.pom) == False:
        raise Exception("%s cannot load." % (options.pom))
    p = read_all(options.pom)
    if options.version is None:
        version = get_tag(p,'version')
    artifactId = get_tag(p,'artifactId')
    groupId = get_tag(p,'groupId')
    groupPath = re.sub( r'\.','/', groupId)

    pp = read_all('pom-parent.xml')
    alter_tag(p,['version'], version)
    alter_tag(pp,['version', 'dependencies/dependency/version'], version)
    alter_tag(p,['parent/groupId'],groupId)
    alter_tag(p,['parent/artifactId'],'%s-parent' % (artifactId))
    alter_tag(p,['parent/version'], version)

    dompath = 'target/m2/%s/%s/%s/' % (groupPath, artifactId, version)
    parentpath = 'target/m2/%s/%s-parent/%s/' % (groupPath, artifactId, version)
    #mkdirs(parentpath)
    f =  glob.glob('%s/%s-*.pom' % (dompath,artifactId))
    if len(f) == 1:
        f=f[0]
    else:
        f='%s/%s-%s.pom' % (dompath,artifactId, version)
    logging.info('writing dom pom %s' % (dompath))
    write_all(f,p)
    logging.info('writing parent pom %s' % (parentpath))
    write_all('%s/%s-parent-%s.pom' % (parentpath, artifactId, version), pp)
