# !/usr/bin/python

from lxml import etree as ET
import sys
import re
import getopt

def main(file='pom.xml', type=None):
    base = ['clean']
    root = ET.parse(file).getroot()
    context = root.attrib[root.attrib.keys()[0]].split(' ')[0]
    cmd = {'pmd': {'plugin': 'maven-pmd-plugin', 'cmd': 'pmd:pmd -Daggregate=true -Dpmd.typeResolution=false'},
           'jacoco': {'plugin': 'jacoco-maven-plugin', 'cmd': 'org.jacoco:jacoco-maven-plugin:0.8.1:prepare-agent %s org.jacoco:jacoco-maven-plugin:0.8.1:report '},
           'findbugs': {'plugin': 'findbugs-maven-plugin', 'cmd': '%s org.codehaus.mojo:findbugs-maven-plugin:3.0.5:findbugs'},
           'bw-tester': {'plugin': 'tibco-build-plugin', 'cmd': '-Punittest'}
           }
    for i in ['jacoco', 'pmd', 'findbugs','bw-tester']:
        elem_pom = root.xpath(".//pom:build//pom:plugins//pom:plugin//pom:artifactId[text()='%s']" % (
            cmd[i]['plugin']),  namespaces={"pom": "http://maven.apache.org/POM/4.0.0"})
        elem_profile_build = root.xpath(".//pom:profile//pom:build//pom:plugins//pom:plugin//pom:artifactId[text()='%s']" % (
            cmd[i]['plugin']),  namespaces={"pom": "http://maven.apache.org/POM/4.0.0"})
        print >> sys.stderr, "searching : %s, found inpom=%s, inpomprofile=%s" % (
            i, len(elem_pom), len(elem_profile_build))

        if 'tester' in i and len(elem_pom) > 0:
            base = ['clean']
            base.append(cmd[i]['cmd'])
        else:
            alter = False
            if len(elem_pom) > 1 and len(elem_profile_build) > 0:
                alter = False
            if len(elem_pom) == 1 and len(elem_profile_build) > 0:
                alter = True
            elif len(elem_pom) == 0 and 'tester' in i:
                alter = False
            elif len(elem_pom) == 0:
                alter = True
            if alter == True:
                command = cmd[i]['cmd']
                if '%s' in command:
                    if any('deploy' in item for item in base) == False:
                        command = command % ('deploy')
                    else:
                        command = re.sub('%s', '', command)
                base.append(command)
    if any('deploy' in item for item in base) == False:
        base.append('deploy')
    return " ".join(base)


if __name__ == '__main__':
    file='pom.xml'
    type='java'
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hf:t:")
    except getopt.error, msg:
        print ("Usage: command_builder.py - this text\n\
-c [command] \n-u giturl\n%s\n" % (msg))
        print(msg)
        print ("for help use --help")
        sys.exit( 2)
    for o, a in opts:
        if o in ("-h", "--help"):
            print (__doc__)
            sys.exit(0)
        elif o in ("-f"):
            file = a
        elif o in ("-t"):
            type=a
    print(main(file,type))
