# !/usr/bin/python

import os
import sys
import re
import getopt
import glob2
import logging

def finder(directory, patterns, remove_leading_path=True):
    paths = []
    for pattern in patterns:
        for path in os.walk(directory):
            path = path[0]
            found = re.findall(pattern, path)
            #logging.debug("found: %s", found)
            if found is None:
                continue
            for f in found:
                if remove_leading_path == True and directory.startswith(".") == False:
                    f = re.sub( r'%s/' % (directory), '', f)
                if not f in paths and not '-testing' in f:
                    paths.append(f)
    if paths is None or len(paths) == 0:
        return ''
    return ",".join(paths)

# def filefinder(directory, patterns, remove_leading_path=True):
#     paths = []
#     for pattern in patterns:
#         for path in os.walk(directory):
#             for file in os.listdir(path[0]):
#                 found = re.findall(pattern, path)
#                 #logging.debug("found: %s", found)
#                 if found is None:
#                     continue
#                 for f in found:
#                     if remove_leading_path == True and directory.startswith(".") == False:
#                         f = re.sub( r'%s/' % (directory), '', f)
#                     if not f in paths:
#                         paths.append(f)
#     if paths is None or len(paths) == 0:
#         return ''
#     return ",".join(paths)

def has_tests(directory='.'):
    return finder(directory,['.*src/test', '.*tests'])
def has_sources(directory='.'):
    return finder(directory,['.*src/main'])
def has_poms(directory='.'):
    return filefinder(directory,['.*/pom.xml'])


def main(directory='.', check='has_tests'):
    METHOD = globals()[check]
    return METHOD(directory)


if __name__ == '__main__':
    directory = '.'
    check = None
    escape= False
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hd:tspe")
    except getopt.error, msg:
        print ("Usage: workflow_setup.py - this text\n\
-c [command] \n-u giturl\n%s\n" % (msg))
        print(msg)
        print ("for help use --help")
        sys.exit( 2)
    for o, a in opts:
        if o in ("-h", "--help"):
            print (__doc__)
            sys.exit(0)
        elif o in ("-d"):
            directory = a
        elif o in ("-t"):
            check = 'has_tests'
        elif o in ("-s"):
            check = 'has_sources'
        elif o in ("-p"):
            check = 'has_pom'
        elif o in ("-e"):
            escape = True
    if check is None:
        print "must specify either [-tsp] "

    ret = main(directory,check)
    if escape == True:
        ret = re.sub(r'/', '\\/', ret)
    ret = R"%s" % (ret)
    sys.stdout.write(ret)
