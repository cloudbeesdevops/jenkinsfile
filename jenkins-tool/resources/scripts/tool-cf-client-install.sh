export https_proxy=http://internet.proxy.fedex.com:3128
export http_proxy=http://internet.proxy.fedex.com:3128
[[ -e /var/tmp/tools/cf ]] && exit 0
cd /var/tmp/tools/
curl -L "https://cli.run.pivotal.io/stable?release=linux64-binary&source=github" --fail | tar -zx
ls -alrt
pwd
chmod +x cf
./cf --version
