#!/usr/bin/python

from fnexus.manifestResolver import manifestResolver
import argparse
import logging
import sys
import os
import logging

def update(mani, artifactId,version):
    mani.__manifest__.set('RELEASE_MANIFEST',artifactId,version)

def write(mani):
    release_manifest = "%s/%s" % (mani.manifestdir,'RELEASE_MANIFEST.MF' )
    print("rewriting %s" % (release_manifest))
    with open(release_manifest, 'w+') as f:
        mani.__manifest__.write(f)
    mani.refresh()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', required=True, action='store', help='Version.')
    parser.add_argument('-a','--artifactId', required=True, action='store', help='artifactId')
    parser.add_argument('-p', '--path' ,required=True, action='store', help='path.')
    parser.add_argument('-l', '--level', required=True, action='store')

    options = parser.parse_args(sys.argv[1:])
    path = "%s/%s/RELEASE_MANIFEST.MF" %(options.path, options.level)
    if options.path is None or os.path.exists(path) == False:
        raise Exception("%s cannot load release manifest." % (path))
    #updater = manifest_updater.updater.ManifestUpdater(manifestResolver(options.path ,options.level))
    mani = manifestResolver(options.path ,options.level)

    update(mani, options.artifactId,options.version)
    write(mani)
