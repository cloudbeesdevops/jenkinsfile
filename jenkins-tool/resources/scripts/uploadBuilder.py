#!/usr/bin/python

import sys
import json
import os
import re
from xml.etree import ElementTree as ET
import logging
import argparse
r = [] # for artifact uploader (not used yet)
OPTIONS = None

def mkdirs(path):
    try:
        os.makedirs(path)
    except:
        pass
def read_all(file):
    if os.path.exists(file):
        with open(file) as f:
            xmlstring = f.read()
            xmlstring = re.sub(r'\sxmlns="[^"]+"', '', xmlstring, count=1)
        return ET.fromstring(xmlstring)
    else:
        raise Exception("%s does not exist." % (file))
def get_tag(p, path):
    for char in p.findall(path):
        return char.text
def write_all(file,p):
    path = os.path.dirname(file)
    mkdirs(path)
    with open(file, "wb") as pom:
        pom.write(ET.tostring(p))
def alter_tag(p, paths,value):
    for path in paths:
        for char in p.findall(path):
            char.text = value

def include(f):
    include = True
    for ext in ['.sha1', '.md5', '.properties', '.repositories', 'maven-metadata.xml', 'maven-metadata-nexus.xml', '.py', 'local.xml', 'snapshots.xml', '.sh', '.groovy', 'METADATA']:
        if f.endswith(ext):
            include = False
    #logging.error("include=%s %s" % (include, f))
    return include

def config_files(files=None):
    configfiles=[]
    if not files is None:
        logging.info("use files object")
        for f in files:
            dirpath = os.path.dirname(f)
            if include(f) == True:
                logging.info('adding %s', f)
                configfiles.append(f)
    elif os.path.exists('test.txt'):
        logging.info("load from file")
        with open('test.txt','r') as f:
            configfiles = f.read().split("\n")
    else:
        #global OPTIONS
        logging.info("use scanned files on disk [%s]" % (OPTIONS))

        for dirpath, dirnames, files in os.walk(OPTIONS.path):
            for f in files:
                if include(f) == True:
                    configfiles.append(os.path.join(dirpath, f))
    return configfiles

def get_classifier(f):
    paths = os.path.basename(f).split('-')[::-1]
    idx = 0
    #logging.debug( "\n\t%s\n" % ( paths))
    if len(paths) > 0 and len(paths[0]) > 0:
        if not paths[0][0].isdigit():
            for i in paths:
                if i[0].isdigit():
                    x = "-".join(reversed(paths[:idx]))
                    paths = [x] + paths[idx::]
                    break
                idx += 1
    else:
        return None
    repo = 'SEFS-6270-releases'
    if 'SNAPSHOT' in f:
        repo = 'SEFS-6270-snapshots'
    logging.debug( json.dumps(f))
    logging.debug( json.dumps(paths))
    classifier = paths[0].split('.')[::-1]
    if not classifier[1][0].isdigit():
        return [classifier[0], classifier[1], repo]
    if not classifier[1].startswith('SNAPSHOT') and not classifier[1][0].isdigit():
        return [classifier[0], classifier[1], repo]
    return [classifier[0], '', repo]

def path_trim_and_extract(f,extract_method, rootdir='target/m2'):
    logging.debug( "analize: %s %s", f, rootdir)
    root = f.replace(rootdir,'')
    logging.debug( "analize: %s", f)
    paths = root.split('/')[::-1]
    print "analyze: ", json.dumps(paths)
    try:
        del paths[paths.index('')]
    except:
        pass
    j = 0
    for i in paths:
        if paths[j][0].isdigit() and not paths[j + 1][0].isdigit():
            return extract_method(paths,j)
        j = j + 1
    return

def get_groupId(f, rootdir='target/m2'):
    def group_extract(paths,j):
        artifact = paths[int(j + 2):]
        return ".".join(reversed(artifact))
    return path_trim_and_extract(f, group_extract, rootdir )

def get_version(f, rootdir='target/m2'):
    def version_extract(paths,j):
        version = paths[int(j)]
        return version
    return path_trim_and_extract(f, version_extract, rootdir)

def get_artifactId(f):
    paths = os.path.basename(f).split('-')[::-1]
    j = 0
    for i in paths:
        if paths[j].startswith('SNAPSHOT') and paths[j + 1][0].isdigit():
            artifact = paths[int(j + 2):]
            return "-".join(reversed(artifact))
        if paths[j][0].isdigit() and not paths[j + 1][0].isdigit():
            artifact = paths[int(j + 1):]
            return "-".join(reversed(artifact))
        j = j + 1
    return

def nexusArtifactUploader(f, r):
    s = get_classifier(f)
    if not s is None:
        r.append({'artifactId': get_artifactId(f),
                  'classifier': s[1], 'file': f, 'type': s[0]})


def nexusPublisher(f,r):
    s = get_classifier(f)
    if not s is None:
        r.append({'classifier': s[1], 'filePath': f, 'extension': s[0]})


def create_arr(strict,key):
    if key not in strict.keys():
        strict[key]=[]
    return strict[key]

def file_keys(files, snapshots_only=False):
    strct={}
    logging.debug(json.dumps(files, indent=3))
    for f in files:
        if not OPTIONS.artifactId is None and not OPTIONS.artifactId in f:
            continue
        if not '-SNAPSHOT' in f and snapshots_only == True:
            continue
        c = get_classifier(f)
        if not c is None:
            logging.debug("c=%s -> f=%s",c,f)
            if c[1] == "" or c[1] == 'SNAPSHOT' or len(files) == 1:
                k = re.sub( r'\.\w+$','',f)
                #k = os.path.dirname(f)
                logging.info("%s\t%s" % (f,k))
                if c[1] != "" and c[1] != 'SNAPSHOT':
                    k = re.sub( r'-%s' %  (c[1]), '', k)
                if k not in strct.keys():
                    logging.info('adding key %s', k)
                    strct[k]={}
                else:
                    logging.debug('key %s already exists', k    )
        else:
            logging.debug("unfit format for %s", f)
    return strct


def gather_files(files, snapshots_only=False):
    logging.debug( 'files=%s' % (files))
    #for files in file_bases(_files)
    strct=file_keys(files, snapshots_only)
    #logging.error(strct.keys())
    #       logging.debug( files)
    for f in files:
        if include(f) == False:
            logging.info("excluding '%s'" % (os.path.basename(f)))
            continue
        for k in strct.keys():
            if f.startswith(k):
                s = get_classifier(f)
                logging.info("%s -> %s", s, f)
                if s[0] == 'pom':
                    strct[k]['pom'] = f
                else:
                    create_arr(strct[k],'clz').append(s[1])
                    create_arr(strct[k],'ctypes').append(s[0])
                    create_arr(strct[k],'cfiles').append(f)
            else:
                logging.info("the key '%s' is not found in '%s'" % (k,f))
    logging.info( json.dumps(strct, indent=3))
    return strct

def gather_url_refs(strct, files, snapshots_only=False):
    strct=file_keys(files, snapshots_only)
    logging.debug( files)
    for f in files:
        for k in strct.keys():
            if f.startswith(k):
                s = get_classifier(f)
                logging.debug( f)
                if s[0] == 'pom':
                    strct[k]['pom'] = f
                else:
                    create_arr(strct[k],'clz').append(s[1])
                    create_arr(strct[k],'ctypes').append(s[0])
                    create_arr(strct[k],'cfiles').append(f)
    logging.debug( json.dumps(strct, indent=3))
    return strct

def build_refresh_url(item):
    if item.endswith('.pom'):
        return None
    listed_version = get_version(item)
    logging.warn('listed version %s', listed_version)
    item = re.sub( r'target/m2/', '', item)
    item = re.sub( r'(.+)-\d{8}\.\d+\-\d(.+)$', r'\1-SNAPSHOT\2', item)
    if not OPTIONS.version is None:
        item = re.sub( r'%s' % (listed_version),  OPTIONS.version, item)
    return "%s/%s" % ('http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content', item)

def gather_command(item):
    def ifNot(item,key):
        if key in item:
            return item[key]
        return []
    _file = ''
    _packaging = 'jar'
    found_primary=None
    _pomfile = item['pom'] if 'pom' in item.keys() else None
    _cfiles = ifNot(item,'cfiles')
    _ctypes = ifNot(item,'ctypes')
    _clz = ifNot(item,'clz')
    def alter_version():
        print "altering version of %s with %s" % (_pomfile, OPTIONS.version)
        pp=read_all(_pomfile)
        alter_tag(pp,['version'], OPTIONS.version)
        write_all(_pomfile, pp)

    logging.debug( json.dumps(item, indent=3))
    if OPTIONS.version:
        logging.warn('version will be altered to %s' % (OPTIONS.version))
    if len(_cfiles) == 0 and _pomfile:
        if not OPTIONS.version is None:
            alter_version()
        return '-DpomFile=%s -Dfile=%s' % (_pomfile, _pomfile)
    if len(_cfiles) == 1 and _pomfile:
        if not OPTIONS.version is None:
            alter_version()
        return '-DpomFile=%s -Dfile=%s -Dpackaging=%s' % (_pomfile, _cfiles[0], _ctypes[0])
    if len(_cfiles) == 1 and not _pomfile:
        # calculate the groupid / artifactid, packaging, classifier and version from path
        version = get_version(_cfiles[0],OPTIONS.path) if OPTIONS.version is None else OPTIONS.version
        gav = '-Dfile=%s -DgroupId=%s -DartifactId=%s -Dversion=%s -Dpackaging=%s' % (_cfiles[0],
            get_groupId(_cfiles[0],OPTIONS.path), get_artifactId(_cfiles[0]), version, _ctypes[0])
        if len(_clz) == 1 and _clz[0] != '':
            gav = '%s -Dclassifier=%s' % (gav, _clz[0])
        return '%s' % (gav)
    elif len(_cfiles) > 1 and found_primary is None:
        pombase = re.sub(r'pom$','',_pomfile)
        logging.debug( 'x %s', pombase)
        idx = -1
        for _cf in _cfiles:
            idx += 1
            if _cf.startswith(pombase) :
                break
        _file = _cfiles[idx]
        _packaging = _ctypes[idx]
        del _cfiles[idx]
        del _clz[idx]
        del _ctypes[idx]
        found_primary = True
        logging.debug( "found primary artf=%s %s", _file, _packaging)
    elif len(_clz) == 0:
        if not OPTIONS.version is None:
            alter_version()
        return '-DpomFile=%s -Dfile=%s -Dpackaging=%s -Dtypes=%s -Dfiles=%s' % (_pomfile, _file, _packaging, ",".join(_ctypes), ",".join(_cfiles))
    if not OPTIONS.version is None:
        alter_version()
    return '-DpomFile=%s -Dfile=%s -Dpackaging=%s -Dclassifiers=%s -Dtypes=%s -Dfiles=%s' % (_pomfile, _file, _packaging, ",".join(_clz), ",".join(_ctypes), ",".join(_cfiles))



def deployFile(files, snapshots_only=False, version=None):
    strct=gather_files(files, snapshots_only)
    logging.debug( 'deployfile %s', json.dumps(strct.keys(),indent=3))
    logging.debug( 'deploystrt %s', json.dumps(strct,indent=3))
    commands=[]
    for s in strct.keys():
        commands.append( gather_command(strct[s]) )
    return commands

def deploySnapshotsOnly(files,version=None):
    return deployFile(files, True)

def proxyRefresh(files):
    commands=[]
    for s in files:
        if not OPTIONS.artifactId is None and not OPTIONS.artifactId in s:
            continue
        n = build_refresh_url(s)
        if not n is None:
            commands.append( build_refresh_url(s) )
    return sorted(commands)


# this is a dynamic method to invoke deployFile giving the option to switch implementations...
# FORMAT = deployFile which means the deployFile() method in this list of methods is executed
# this is a dynamic method to invoke deployFile giving the option to switch implementations...
# FORMAT = deployFile which means the deployFile() method in this list of methods is executed
def set_options(options):
    global OPTIONS
    OPTIONS = {} if options is None else options 
    logging.warn('set options %s', OPTIONS)

def main(__argv):
    VERSION = None
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--version', action='store', help='Version.')
    parser.add_argument('-p', '--path', default='target/m2' ,action='store', help='path.')
    parser.add_argument('-f', '--format', choices=('deployFile','deploySnapshotsOnly','proxyRefresh'), default='deployFile', action='store', help='format.')
    parser.add_argument('-a', '--artifactId',action='store', help='path.')
    # parse options and arguments and save groups for menu operations
    #options = parser.parse_args(sys.argv[1:])
    options, unknown = parser.parse_known_args()
    if not options.version is None:
        VERSION = options.version


    set_options(options)
    configfiles=config_files()

    METHOD = globals()[options.format]

    logging.debug("args: %s (%d)" % (__argv, len(sys.argv)))
    logging.info(configfiles)
    if options.format in  ['deployFile','proxyRefresh', 'deploySnapshotsOnly']:
        with open("%s.json" % (options.format), "w+") as f:
            f.write(json.dumps(METHOD(configfiles),'indent=3'))
    else:
        for f in configfiles:
            METHOD(f, r)
        print json.dumps(r, indent=3)


if __name__ == "__main__":
    __args = sys.argv
    # strip of the test arguments and take
    if sys.argv[0]=='python -m unittest':
        __args = sys.argv[2:] if 'TestFdeploy.TestFdeploy' in sys.argv[1] else sys.argv[6:]
        __args = sys.argv[4:] if 'TestFdeploy.py' in sys.argv[3] else __args
    else:
        __args = sys.argv[1:]
    #print " \targs=%s " % (__args)
    print "uploadBuilder.py.__main__[%s](%d)" % (__args, len(sys.argv))
    print "here1"
    main(__args)
else:
    if not 'pytest' in sys.argv[0]:
        print "here2", sys.argv
        main(sys.argv[2:])
