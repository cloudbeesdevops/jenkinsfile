import os
import sys
import multiprocessing as mp
import requests
import json
import re
import urllib
import requests_cache

from lxml import etree as ET
import logging
import getopt
try:
    import http.client as http_client
except ImportError:
    import httplib as http_client


def setup():
    try:
        requests.packages.urllib3.disable_warnings()
    except AttributeError as ar:
        pass

    try:
        requests_cache.install_cache()
        logging.info("caching enabled")
    except:
        logging.info("no caching")
        pass

PRIVATE_TOKEN='CzfzBYUiV7xncWS4DhR8'
JENKINS_URL='https://jenkins-shipment.web.fedex.com:8443/jenkins'
GITLAB_URL='https://gitlab.prod.fedex.com/api/v4'
GITLAB_API_DEPLOY_KEYS='%s/projects/%s/deploy_key'
GITLAB_API_JENKINS_CI='%s/projects/%s/services/jenkins'
GITLAB_API_BADGES='%s/projects/%s/badges'
SONAR_BADGE_GATES='https://sonar.prod.cloud.fedex.com:9443/api/project_badges/measure?project=%s&metric=alert_status'
SONAR_BADGE_CODE_COVERAGE='https://sonar.prod.cloud.fedex.com:9443/api/project_badges/measure?project=%s&metric=coverage'
SONAR_BADGE_MAINTAINABLITY='https://sonar.prod.cloud.fedex.com:9443/api/project_badges/measure?project=%s&metric=sqale_rating'
# Quality https://sonar.prod.cloud.fedex.com:9443/api/project_badges/measure?project=SEFS_XOPCO_COMMON_TOOLS_FDEPLOY_XOPCO_COMMON_TOOLS_FDEPLOY-XOPCO_COMMON_TOOLS_FDEPLOY&metric=alert_status
# coverage https://sonar.prod.cloud.fedex.com:9443/api/project_badges/measure?project=SEFS_XOPCO_COMMON_TOOLS_FDEPLOY_XOPCO_COMMON_TOOLS_FDEPLOY-XOPCO_COMMON_TOOLS_FDEPLOY&metric=coverage
JENKINS_BADGES='https://jenkins-shipment.web.fedex.com:8443/jenkins/buildStatus/icon?job=%s'
JENKINS_PROTECTED_BADGE='https://jenkins-shipment.web.fedex.com:8443/jenkins/%s/badge/icon'
#https://jenkins-shipment.web.fedex.com:8443/jenkins/buildStatus/icon?job=CleanUpJobs/BUILD_JOB_BATCH
#https://jenkins-shipment.web.fedex.com:8443/jenkins/buildStatus/icon?job=3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master
#https://jenkins-shipment.web.fedex.com:8443/jenkins/buildStatus/icon?job=3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/58-feature-post_smoketest-implementation
BADGE_FORMAT='link_url=%s&image_url=%s'
HEADERS = {'PRIVATE-TOKEN' : PRIVATE_TOKEN , 'Content-Type' : 'application/json'}

connection = None

# LOCAL TESTING
#   658  export JOB_NAME=3534596-FDEPLOY/XOPCO_COMMON_TOOLS_FDEPLOY/master
#   733  export JOB_URL=https://jenkins-shipment.web.fedex.com:8443/jenkins/job/3534596-FDEPLOY/job/XOPCO_COMMON_TOOLS_FDEPLOY/job/master/
#
# update / create
# python gitlab_admin_functions.py -u git@gitlab.prod.fedex.com:APP3534596/xopco-common-tools-fdeploy.git
#
# delete badges
# python gitlab_admin_functions.py -u git@gitlab.prod.fedex.com:APP3534596/xopco-common-tools-fdeploy.git -c delete

# deploy key update
SET_DEPLOY_KEYS = '''
{
  "id": 17,
  "title": "ssh-gitlab-enterprise-edition",
  "key": "ssh-dss AAAAB3NzaC1kc3MAAACBAO7LwO49gmIYoXC35ymznMiwfYJXxkXUL73alCy1BtJKAG6/vW88WnW6XXIeYHXBI52wcSyCC3jA2iiMWR1gjExY5Ip9eBLGhi3asFDHkGLEX5w2c8kUvrLgYWDbHnisWoZ2gU987y46bz44s9JCvimsOMQmb55VOkaFHTROuVU/AAAAFQC6SkjlNXQ8YqHJzBvy9wCvx4T0dQAAAIEA2P1rp12JDf/H+LB9H82bZ0D7kYZ5TYinzP9tQ77QVUkUEcPuRfaQh5mQjD6hy2+pTnthr1/idgFrlYiPnVTjJhxfqZn/IdB5nWgAQy70whhzjsU3uejzs2QocEX458PZT7x4ZfCJs46rYMyO31gbP0peuNQF4Dfk8Thk2ysdemcAAACBAML5nhqkoCbcAHx4L8L4wsjvq6wD83CIo8/GCWyB3az6jt2y1WQZsj6CYdY+NXQpUnAvH4ZIhBm406sUOrJ2i43R1g4Fkt5jfsaSn0nWP0tidMh2ZB1sGvCbNOC8QoeGfncOu7Q/PHzoO6YLusretT1whqaBp5t95Nvihu1/t620",
  "created_at": "2018-02-26T00:45:58.952Z",
  "can_push": true
}
'''
# ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

def method(gp,requrl):
    #logging.info(gp.__dict__)
    if gp.ok:
        try:
            return gp.json()
        except:
            return gp
    else:
        logging.info("(http_code=%s) %s" % (gp.status_code, requrl))
    return gp

# ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

class Connection(object):

    def __init__(self, session, pid):
        self.session = session
        self.project_id = pid

    def get(self,requrl):

        #print requrl
        dta = method(self.session.get(requrl, headers=HEADERS ), requrl)
        #print "response", dta
        return dta

    def put(self,requrl, data=None):
        if data:
            return method(self.session.put(requrl, data=data, headers=HEADERS ), requrl)
        else:
            return method(self.session.put(requrl, headers=HEADERS ), requrl)

    def post(self,requrl,data):
        return method(self.session.post(requrl, params=data, headers=HEADERS ), requrl)

    def delete(self,requrl):
        return method(self.session.delete(requrl, headers=HEADERS ), requrl)

# # ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

# ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

def job2jobUrl(nameOrPath):
    if '/job/' in nameOrPath:
        return nameOrPath
    name = re.sub(r'/$','',nameOrPath)
    if not name.startswith('/'):
        name = "/%s" % (name)
    name = re.sub(r'/','/job/',name)
    name = "%s%s" % (JENKINS_URL, name)
    return name

def job2jobName(nameOrPath):
    nameOrPath = re.sub(r'/$','',nameOrPath)
    if not '/job/' in nameOrPath:
        return nameOrPath
    name = re.sub( r'%s' % (JENKINS_URL),'',nameOrPath)
    name = re.sub(r'/job/','/',name)
    name = re.sub( r'^/','', name)
    return name


def get_artifactId(file='pom.xml'):
    root = ET.parse(file).getroot()
    context = root.attrib[root.attrib.keys()[0]].split(' ')[0]
    d = "./{%s}artifactId" % (context)
    if (len(root.findall(d)) > 0):
        return root.findall(d)[0].text
    return None

def get_sonar_project_ref(org_artifactId):
    if org_artifactId is not None:
        artifactId = re.sub(r'[-:]','_', org_artifactId.upper())
        if not artifactId.startswith('SEFS_'):
            artifactId = "SEFS_%s_%s-%s" % (artifactId,artifactId,artifactId)
        else:
            artifactId = "%s_%s-%s" % (artifactId,artifactId,artifactId)
        #logging.info("artifactId=%s as key '%s'" % (org_artifactId,artifactId))
        return artifactId
    return None

def get_badge_references(connection):
    reference={}
    req = GITLAB_API_BADGES % (GITLAB_URL, connection.project_id)
    res = connection.get(req)
    if type(res) == list:
        if len(res) == 0:
            logging.warn("no previous bagdes found for %s" % (connection.project_id) )
            logging.warn("curl --header 'PRIVATE-TOKEN: %s' %s" % (PRIVATE_TOKEN,req))
        for l in res:
            reference[str(l['image_url'])]=l['id']
            reference[l['id']] = l['image_url']
    return reference, req

def post_badges(connection, req,link_url,image_url,reference):
    image_url_decoded = urllib.unquote_plus(image_url)
    link_url_decoded = urllib.unquote_plus(link_url)
    data = { 'link_url' : link_url_decoded, 'image_url' : image_url_decoded }
    logging.debug( reference.keys())
    try:
        if reference[image_url_decoded] is not None or reference[image_url] is not None:
            URL_FORMAT = "%s/%s?%s" % (req,reference[image_url_decoded],BADGE_FORMAT)
            httpreq = URL_FORMAT % (link_url,image_url)
            logging.info("update #%s badge as %s" % (reference[image_url_decoded], image_url_decoded))
            connection.put( httpreq)
    except Exception as e:
        #logging.info(e)
        URL_FORMAT = "%s" % (req)
        logging.info("create #new new badge adding %s for %s" % (req,data))
        connection.post( req,data)

def delete_badges(connection):
    logging.info("delete badges for %s", connection.project_id)
    reference , req = get_badge_references(connection)
    logging.debug("reference response=%s" % (json.dumps(reference,indent=2)))
    for id in reference.keys():
        try:
            # see if we have an badge id
            int(id)
        except:
            continue
        URL_FORMAT = "%s/%s" % (req,id)
        logging.info('deleting badge %s wth %s' % (id,URL_FORMAT))
#        try:
        connection.delete( URL_FORMAT)
        logging.info("deleted #%s badge as %s" % (id, URL_FORMAT))
#         except Exception as e:
#             logging.info("failed to delete #%s badge with %s (reason:%s)" % (id, URL_FORMAT,e))

# ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

def set_jenkins_badges(connection):
    if 'JOB_NAME' in os.environ and 'JOB_URL' in os.environ:
        logging.info("setting jenkins badge in gitlab for %s" % (os.environ['JOB_NAME']))
        jenkinspath =  os.environ['JOB_NAME']
        #jenkinspath =  os.path.dirname(os.environ['JOB_NAME'])
        reference, req = get_badge_references(connection)
        image_url = JENKINS_BADGES % (jenkinspath)
        link_url = os.environ['JOB_URL']
        logging.info("\n\t=> %s\n" % (image_url))
        post_badges(connection, req,link_url,image_url,reference)
    else:
        logging.warn('not running in Jenkins Environment use JOB_NAME and JOB_URL env for local testing')

def set_sonar_badges(connection, artifact_id=None):
    if artifact_id is None:
        artifactId = get_artifactId()
    else:
        artifactId = artifact_id
    if artifactId is not None:
        logging.info("setting sonar badges in gitlab for %s" % (artifactId))
        reference, req = get_badge_references(connection)
        for i in [SONAR_BADGE_CODE_COVERAGE, SONAR_BADGE_GATES, SONAR_BADGE_MAINTAINABLITY]:
            projectref_to_sonar = get_sonar_project_ref(artifactId)
            image_url = link_url = urllib.quote_plus(i % (projectref_to_sonar))
            post_badges(connection,req,link_url,image_url,reference)

def set_jenkins_ci( connection,command='update'):
    if 'JOB_NAME' in os.environ:
        logging.info("setting jenkins ci reference path for %s" % (os.environ['JOB_NAME']))
        #jenkinspath = os.environ['JOB_NAME'] #
        jenkinspath = os.path.dirname(os.environ['JOB_NAME'])
        requrl = GITLAB_API_JENKINS_CI % (GITLAB_URL, connection.project_id)
        jenkinsci = connection.get(requrl)
        logging.info("jenkinsci %s" , jenkinsci)
        logging.info("jenkinspath = %s", urllib.quote_plus(jenkinspath))
        update_url = '%s?jenkins_url=%s&project_name=%s' % (requrl, urllib.quote_plus(JENKINS_URL), urllib.quote_plus(jenkinspath))
        logging.info(update_url)
        if command == 'update':
            connection.put( update_url)
        elif command == 'delete':
            connection.delete( update_url)
    else:
        logging.warn('not running in Jenkins Environment use JOB_NAME env for local testing')

def get(s,requrl):
    return method(s.get(requrl, headers=HEADERS ), requrl)

def set_deploy_keys(connection):
    found = None
    req = GITLAB_API_DEPLOY_KEYS % (GITLAB_URL, connection.project_id)
    res = connection.get(req)
    if type(res) == list:
        found = None
        if len(res) == 0:
            logging.warn("no previous bagdes found for %s" % (connection.project_id) )
            logging.warn("curl --header 'PRIVATE-TOKEN: %s' %s" % (PRIVATE_TOKEN,req))
        for l in res:
            if l['id'] == '17':
                found = l;
                break
    if not found is None:
        logging.warn("installing deployment keys for %s.",req)
        connection.post(req, SET_DEPLOY_KEYS)
    else:
        logging.warn("deployment keys already installed.")
    return


# ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----

def main(command,search,command_artifact):
    reference={}
    s = requests.Session()
    for p in get(s,'%s/groups' % (GITLAB_URL)):
        requrl = '%s/groups/%s/projects' % (GITLAB_URL, p['id'])
        for proj in get(s,requrl):
            sys.stdout.write('.')
            if 'ssh_url_to_repo' in proj.keys():
                reference[proj['ssh_url_to_repo']]={ 'pid' : proj['id'], 'gid' : p['id']}
            if 'http_url_to_repo' in proj.keys():
                reference[proj['http_url_to_repo']]={ 'pid' : proj['id'], 'gid' : p['id']}
            sys.stdout.flush()
    sys.stdout.write("\n")
    try:
        logging.info("lookup %s repository url %s" % (search,reference[search]))
    except KeyError as e:
        logging.error("git repo %s not found, registered are %s" % (search, "\t\n".join(reference.keys())))
        sys.exit(3)
    connection = Connection(s,reference[search]['pid'])

    if command is None:
        logging.info('setting defaults for project integration')
        def doit(command_artifact):
            set_jenkins_ci(connection)
            set_deploy_keys(connection)
            set_jenkins_badges(connection)
            set_sonar_badges(connection, command_artifact)
        with requests_cache.disabled():
            doit(command_artifact)
    elif command == 'list':
        logging.info("git repo %s not found, registered are %s" % (search, "\t\n".join(reference.keys())))
        sys.exit(1)
    elif command == 'delete':
        def funct(pid):
            delete_badges(connection)
            set_jenkins_ci(connection,'delete')
        with requests_cache.disabled():
            funct(reference[search]['pid'])
    else:
        raise Exception("unknown command %s" % (command))

if __name__ == '__main__':
    level = logging.INFO
    logging.basicConfig(format='%(levelname)s:%(message)s', level=level)
    setup()

    command=None
    command_artifact=None
    url=None
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hc:u:a:vlxn:")
    except getopt.error, msg:
        print ("Usage: gitlab_admin_functions.py - this text\n\
-c [command] \n-u giturl\n%s\n" % (msg))
        print(msg)
        print ("for help use --help")
        sys.exit( 2)
    for o, a in opts:
        if o in ("-h", "--help"):
            print (__doc__)
            sys.exit(0)
        elif o in ("-c"):
            if a in ['delete','list']:
                command=a
            else:
                print " delete is the registered command"
                sys.exit(2)
        elif o in ("-u"):
            url=a
        elif o in ("-a"):
            command_artifact=a
        elif o in ("-x"):
            logging.info("caching cleared at start")
            requests_cache.clear()
        elif o in ("-v"):
            logging.info("enabled debugging")
            level = logging.DEBUG
        elif o in ("-n"):
            os.environ['JOB_URL'] = job2jobUrl(a)
            os.environ['JOB_NAME'] = job2jobName(a)
        elif o in ("-l"):
            http_client.HTTPConnection.debuglevel = 1
            requests_log = logging.getLogger("requests.packages.urllib3")
            requests_log.setLevel(logging.DEBUG)
            requests_log.propagate = True
    logging.basicConfig(format='%7(levelname)s:%(message)s', level=level)
    setup()

    if not url:
        print " url must be defined -u", url
        sys.exit(2)
    main(command,url,command_artifact)
