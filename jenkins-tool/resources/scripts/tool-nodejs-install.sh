export https_proxy=http://internet.proxy.fedex.com:3128
export http_proxy=http://internet.proxy.fedex.com:3128
cd /var/tmp/tools
[[ -e /var/tmp/tools/node-v8.9.4  ]] && exit 0
curl -L https://nodejs.org/dist/v8.9.4/node-v8.9.4-linux-x64.tar.gz --fail | tar -zx
find . -type d -maxdepth 3
