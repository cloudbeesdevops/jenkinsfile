# -*- coding: utf-8 -*-
import sys
import os
import unittest
import json
import imp
import pytest
import logging
from xml.etree import ElementTree as ET
from subprocess import Popen, PIPE
import shutil
import uploadBuilder
from io import TextIOWrapper, BytesIO
from uploadBuilder import read_all, get_tag, get_classifier, get_artifactId, get_groupId, get_version, deploySnapshotsOnly, deployFile, file_keys, gather_files, build_refresh_url, proxyRefresh, config_files


MINIMAL_POM = '''<project>
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.mycompany.app</groupId>
  <artifactId>my-app</artifactId>
  <version>1</version>
</project>
'''

SETTINGS_XML = '''
'''


def do_maven_deploy(s):
    cmd = ['mvn', 'deploy:deploy-file']
    cmd.extend(s[0].split(' '))
    try:
        os.makedirs('target/out')
    except:
        pass
    cmd.extend(['-Durl=file:./target/out'])
    logging.warn('executing %s' % (' '.join(cmd)))
    p = Popen(cmd, stdout=PIPE)
    p.wait()
    logging.error( p.stdout.read() )

def readfile(name='deployFile', as_json=True):
    b=''
    with open('%s.json' % (name),'r') as f:
        b = f.read()
    if as_json == False:
        return b
    return json.loads(b)

def create_test_structure():
    base = 'target/m2/com/fedex/artifact/1.5.0'
    if os.path.exists(base) == False:
        os.makedirs(base)
    for i in ['-test.jar', '.pom', '.jar']:
        path = "%s/%s-%s%s" % (base, 'artifact', '1.5.0', i)
        with open(path, 'wb') as f:
            os.utime(path, None)
            if i == '.pom':
                f.write(MINIMAL_POM)
    base = 'target/m2/com/fedex/group/1.5.0-SNAPSHOT'
    if os.path.exists(base) == False:
        os.makedirs(base)
    for i in ['-test.jar', '.pom', '.jar']:
        path = "%s/%s-%s%s" % (base, 'group', '1.5.0-20190505.053522-20', i)
        with open(path, 'wb') as f:
            os.utime(path, None)
            if i == '.pom':
                f.write(MINIMAL_POM)

class StdoutBuffer(TextIOWrapper):
    def write(self, string):
        try:
            return super(StdoutBuffer, self).write(string)
        except TypeError:
            # redirect encoded byte strings directly to buffer
            return super(StdoutBuffer, self).buffer.write(string)


class TestUploadBuilder(unittest.TestCase):

    def setup_method(self, method):
        try:
            uploaderBuilder.OPTIONS={'artifactId': ''}
            os.remove('test.txt')
            
        except:
            pass

    def teardown_method(self, method):
        try:
            os.remove('test.txt')
            shutil.rmtree('target')
        except:
            pass
    def test_get_classifier(self):
        s = get_classifier(
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar')
        assert 'jar' == s[0]
        assert '' == s[1]
        assert 'SEFS-6270-snapshots' == s[2]

        s = get_classifier(
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.zip')
        assert 'zip' == s[0]
        assert '' == s[1]
        assert 'SEFS-6270-releases' == s[2]

        s = get_classifier(
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar')
        assert 'jar' == s[0]
        assert 'stubs' == s[1]
        assert 'SEFS-6270-snapshots' == s[2]

        s = get_classifier(
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar')
        assert 'jar' == s[0]
        assert 'jar-with-dependencies' == s[1]
        assert 'SEFS-6270-snapshots' == s[2]

    def test_get_artifactIdAndGroupId(self):
        s = get_artifactId(
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar')
        assert 'ReportsParser' == s
        s = get_groupId(
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar')
        assert 'com.fedex.cicd' == s
        s = get_version(
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar')
        assert '1.0.5-SNAPSHOT' == s

        s = get_artifactId(
            'target/m2/com/fedex/cicd/a-b-c/1.0.5/a-b-c-1.0.5.zip')
        assert 'a-b-c' == s
        s = get_groupId('target/m2/com/fedex/cicd/a-b-c/1.0.5/a-b-c-1.0.5.zip')
        assert 'com.fedex.cicd' == s
        s = get_version('target/m2/com/fedex/cicd/a-b-c/1.0.5/a-b-c-1.0.5.zip')
        assert '1.0.5' == s

    def test_deployFile(self):
        s = deployFile(
            ['target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.zip'])
        assert ['-Dfile=target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.zip\
 -DgroupId=com.fedex.cicd -DartifactId=ReportsParser -Dversion=1.0.5 -Dpackaging=zip'] == s

    def test_deployFile4(self):

        s = deployFile(
            ['target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar'])
        assert '-Dfile=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar \
-DgroupId=com.fedex.cicd \
-DartifactId=ReportsParser \
-Dversion=1.0.5-SNAPSHOT \
-Dpackaging=jar' == s[0]
        assert '-Dfile=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar \
-DgroupId=com.fedex.cicd \
-DartifactId=ReportsParser \
-Dversion=1.0.5-SNAPSHOT \
-Dpackaging=jar' == s[0]

    def test_deployFile2(self):
        a = ['target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar']
        assert [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1'] == file_keys(a).keys()
        s = deployFile(a)
        assert '-Dfile=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar \
-DgroupId=com.fedex.cicd \
-DartifactId=ReportsParser \
-Dversion=1.0.5-SNAPSHOT \
-Dpackaging=jar \
-Dclassifier=jar-with-dependencies' == s[0]

    def test_deployFile_with_pom(self):
        s = deployFile(['target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.zip',
                        'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.pom'])
        assert '-DpomFile=target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.pom \
-Dfile=target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.zip -Dpackaging=zip' == s[0]

    def test_deployFile_with_pom1(self):
        s = deployFile(['target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.zip',
                        'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.pom',
                        'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5-stubs.jar'])

        assert '-DpomFile=target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.pom \
-Dfile=target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.zip \
-Dpackaging=zip \
-Dclassifiers=stubs \
-Dtypes=jar \
-Dfiles=target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5-stubs.jar\
' == s[0]

    def test_deployFile_with_pom4(self):
        a = [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.pom',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.04324-1.zip',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.04324-1.pom',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.04324-1.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar']

        assert [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1','target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.04324-1'] == file_keys(a).keys()
        strct = file_keys(a)
        b = gather_files(a)
        c = 'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1'
        assert c == b.keys()[0]
        assert ['cfiles', 'pom', 'ctypes', 'clz'] == b[c].keys()
        d = [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar']
        assert d == b[c]['cfiles']
        s = deployFile(a)


    def test_deployFile_with_pom3(self):
        a = [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.pom',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar']

        assert [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1'] == file_keys(a).keys()
        strct = file_keys(a)
        b = gather_files(a)
        c = 'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1'
        assert c == b.keys()[0]
        assert ['cfiles', 'pom', 'ctypes', 'clz'] == b[c].keys()
        d = [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar']
        assert d == b[c]['cfiles']
        s = deployFile(a)
        assert '-DpomFile=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.pom \
-Dfile=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip \
-Dpackaging=zip \
-Dclassifiers=jar-with-dependencies,,stubs \
-Dtypes=jar,jar,jar \
-Dfiles=\
target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar,\
target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar,\
target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar\
' == s[0]

    def test_deployFileSnapshotsOnly_with_pom3(self):
        a = [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.pom',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar']

        assert [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1'] == file_keys(a).keys()
        strct = file_keys(a,True)
        b = gather_files(a, True)
        c = 'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1'
        assert c == b.keys()[0]
        assert ['cfiles', 'pom', 'ctypes', 'clz'] == b[c].keys()
        d = [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar']
        assert d == b[c]['cfiles']
        s = deploySnapshotsOnly(a)
        assert '-DpomFile=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.pom \
-Dfile=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip \
-Dpackaging=zip \
-Dclassifiers=jar-with-dependencies,,stubs \
-Dtypes=jar,jar,jar \
-Dfiles=\
target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-jar-with-dependencies.jar,\
target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar,\
target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar\
' == s[0]

    def test_file_keys(self):
        files = [
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.zip',
           'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.pom',
           'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5-stubs.jar'
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.pom',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar',
            'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar' ]
        s = file_keys(files)
        assert 'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1' == s.keys()[
            0]
        assert 'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5' == s.keys()[
            1]
        s = file_keys(files,True)
        assert 'target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1' == s.keys()[
            0]
        s = deploySnapshotsOnly(files)
        print json.dumps(s)
        assert ['-DpomFile=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.pom -Dfile=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.zip -Dpackaging=zip -Dclassifiers=,stubs -Dtypes=jar,jar -Dfiles=target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1.jar,target/m2/com/fedex/cicd/ReportsParser/1.0.5-SNAPSHOT/ReportsParser-1.0.5-20190102.042220-1-stubs.jar'] == s



    def test_file_keys_mixin(self):
        s = file_keys(['target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.zip',
                       'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5.pom',
                       'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5-stubs.jar'])
        assert 'target/m2/com/fedex/cicd/ReportsParser/1.0.5/ReportsParser-1.0.5' == s.keys()[
            0]


    def test_parent_child2(self):
        s = deployFile([
            'target/m2/com/fedex/sefs/dashboard/dashboard-parent/5.4.0-SNAPSHOT/dashboard-parent-5.4.0-20190131.183355-1.pom',
            'target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190131.183410-1.jar',
            'target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190131.183410-1.pom',
            'target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190131.183410-1-tests.jar',
            'target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190131.183429-1.jar',
            'target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190131.183429-1.pom',
            'target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190131.183429-1.zip',
            'target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190131.184417-1.jar',
            'target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190131.184417-1.pom',
            'target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190131.184509-1.pom',
            'target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190131.184509-1.zip'])
        result = [
                  "-DpomFile=target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190131.184417-1.pom -Dfile=target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190131.184417-1.jar -Dpackaging=jar",
                  "-DpomFile=target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190131.183410-1.pom -Dfile=target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190131.183410-1.jar -Dpackaging=jar -Dclassifiers=tests -Dtypes=jar -Dfiles=target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190131.183410-1-tests.jar",
                  "-DpomFile=target/m2/com/fedex/sefs/dashboard/dashboard-parent/5.4.0-SNAPSHOT/dashboard-parent-5.4.0-20190131.183355-1.pom -Dfile=target/m2/com/fedex/sefs/dashboard/dashboard-parent/5.4.0-SNAPSHOT/dashboard-parent-5.4.0-20190131.183355-1.pom",
                  "-DpomFile=target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190131.183429-1.pom -Dfile=target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190131.183429-1.jar -Dpackaging=jar -Dclassifiers= -Dtypes=zip -Dfiles=target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190131.183429-1.zip",
                  "-DpomFile=target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190131.184509-1.pom -Dfile=target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190131.184509-1.zip -Dpackaging=zip"]

        i = 0

        assert len(s) == len(result)
        result = sorted(result)
        for a in sorted(s):
            assert result[i] == a
            i+=1

    def test_parent_child(self):
        s = deployFile(
            ["target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1-tests.jar",


             "target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1.pom",
             "target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1.jar", "target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190120.234752-1.pom",
             "target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190120.234752-1.jar",
             "target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190120.234214-1.jar",
             "target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190120.234214-1.zip",
             "target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190120.234214-1.pom",
             "target/m2/com/fedex/sefs/dashboard/dashboard-parent/5.4.0-SNAPSHOT/dashboard-parent-5.4.0-20190120.234133-1.pom",
             "target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190120.234845-1.zip",
             "target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190120.234845-1.pom"])

        assert '-DpomFile=target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1.pom -Dfile=target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1.jar -Dpackaging=jar -Dclassifiers=tests -Dtypes=jar -Dfiles=target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1-tests.jar' == s[
            0]
        assert '-DpomFile=target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190120.234752-1.pom -Dfile=target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190120.234752-1.jar -Dpackaging=jar' == s[
            1]
        assert '-DpomFile=target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190120.234845-1.pom -Dfile=target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190120.234845-1.zip -Dpackaging=zip' == s[
            2]
        assert '-DpomFile=target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190120.234214-1.pom -Dfile=target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190120.234214-1.jar -Dpackaging=jar -Dclassifiers= -Dtypes=zip -Dfiles=target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190120.234214-1.zip' == s[
            3]

        t = json.dumps(s)
        arr = json.loads(t)
        assert 5 == len(arr)

    def test_build_refresh_url(self):
        s = build_refresh_url('target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1-tests.jar')
        assert s == 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/\
com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-SNAPSHOT-tests.jar'
        s = build_refresh_url('target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-tests.jar')
        assert s == 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/\
com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-tests.jar'

    def test_refresh_proxy(self):
        s = proxyRefresh(
            ["target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1-tests.jar",
             "target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1.pom",
             "target/m2/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-20190120.234150-1.jar",
             "target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190120.234752-1.pom",
             "target/m2/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-20190120.234752-1.jar",
             "target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190120.234214-1.jar",
             "target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190120.234214-1.zip",
             "target/m2/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-20190120.234214-1.pom",
             "target/m2/com/fedex/sefs/dashboard/dashboard-parent/5.4.0-SNAPSHOT/dashboard-parent-5.4.0-20190120.234133-1.pom",
             "target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190120.234845-1.zip",
             "target/m2/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-20190120.234845-1.pom"])

        assert 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/com/fedex/sefs/dashboard/dashboard-client/5.4.0-SNAPSHOT/dashboard-client-5.4.0-SNAPSHOT.jar' == s[0]
        assert 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-SNAPSHOT-tests.jar' == s[1]
        assert 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/com/fedex/sefs/dashboard/dashboard-domain/5.4.0-SNAPSHOT/dashboard-domain-5.4.0-SNAPSHOT.jar' == s[2]
        assert 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-SNAPSHOT.jar' == s[3]
        assert 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-SNAPSHOT.zip' == s[4]
        assert 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/com/fedex/sefs/dashboard/dashboard-web/5.4.0-SNAPSHOT/dashboard-web-5.4.0-SNAPSHOT.zip' == s[5]
        #assert 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/com/fedex/sefs/dashboard/dashboard-process/5.4.0-SNAPSHOT/dashboard-process-5.4.0-SNAPSHOT.zip' == s[4]

    def __run(self, arguments):
        old_stdout = sys.stdout
        old_args = sys.argv
        sys.argv = ['uploadBuilder.py']
        sys.argv.extend(arguments)
        #print sys.argv
        with StdoutBuffer(BytesIO(), sys.stdout.encoding) as sys.stdout:
            create_test_structure()
            runpy = imp.load_source('__main__', 'uploadBuilder.py')
            sys.stdout.seek(0)      # jump to the start
            out = sys.stdout.read() # read output
        sys.stdout = old_stdout
        sys.argv = old_args
        return out

    def test_sdom_usecase(self):
        
        assert not uploadBuilder.OPTIONS is None

        list = '''target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/maven-metadata.xml.md5
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/maven-metadata.xml
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/maven-metadata.xml.sha1
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/maven-metadata.xml.md5
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.jar
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.jar.md5
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.pom.sha1
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.jar.sha1
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/maven-metadata.xml
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.pom
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/maven-metadata.xml.sha1
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.pom.md5
target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom-parent/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-parent-2.2.0.0-SNAPSHOT.pom'''

        _files = list.split("\n")
        assert 13 == len(_files)
        files = config_files(_files)
        key = file_keys(files)
        assert 3 == len(files)
        assert 2 == len(key.keys())
        with open("test.txt","w+") as f:
            f.write(list)
        self.__run(['deployFile'])
        s = readfile('deployFile')
        target = [
         'target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom\
/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.jar',
           'target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom\
/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.pom',
           'target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom-parent\
/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-parent-2.2.0.0-SNAPSHOT.pom'
        ]
        # -DpomFile=target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.pom
        #  -Dfile=target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.jar -Dpackaging=jar
        t1 = '-DpomFile=target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom\
/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.pom \
-Dfile=target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom/2.2.0.0-SNAPSHOT/\
xopco-common-tools-waypoint-dom-2.2.0.0-20190521.193239-1.jar -Dpackaging=jar'
        t2 = "-DpomFile=target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom-parent/\
2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-parent-2.2.0.0-SNAPSHOT.pom \
-Dfile=target/m2/sefs/xopco/core/common-tools/xopco-common-tools-waypoint-dom-parent\
/2.2.0.0-SNAPSHOT/xopco-common-tools-waypoint-dom-parent-2.2.0.0-SNAPSHOT.pom"
        logging.warn("\n%s\n", json.dumps(s, indent=3))
        assert len(s) == 2
        assert s[0] == t1
        assert s[1] == t2

    def test_itinery_usecase(self):
        list = '''target/m2/com/fedex/sefs/core/java/jms/itinerary-proxy-service/3.4.0-SNAPSHOT/itinerary-proxy-service-3.4.0-20190524.213238-1-stubs.jar
target/m2/com/fedex/sefs/core/java/jms/itinerary-proxy-service/3.4.0-SNAPSHOT/itinerary-proxy-service-3.4.0-20190524.213238-1.pom
target/m2/com/fedex/sefs/core/java/jms/itinerary-proxy-service/3.4.0-SNAPSHOT/itinerary-proxy-service-3.4.0-20190524.213238-1.jar'''
        _files = list.split("\n")
        assert 3 == len(_files)
        files = config_files(_files)
        key = file_keys(files)
        assert 3 == len(files)
        assert 1 == len(key.keys())
        with open("test.txt","w+") as f:
            f.write(list)
        self.__run(['deployFile'])
        s = readfile('deployFile')
        assert 1 == len(s)
        assert s[0] == '\
-DpomFile=target/m2/com/fedex/sefs/core/java/jms/itinerary-proxy-service/3.4.0-SNAPSHOT/itinerary-proxy-service-3.4.0-20190524.213238-1.pom \
-Dfile=target/m2/com/fedex/sefs/core/java/jms/itinerary-proxy-service/3.4.0-SNAPSHOT/itinerary-proxy-service-3.4.0-20190524.213238-1.jar\
 -Dpackaging=jar -Dclassifiers=stubs -Dtypes=jar \
-Dfiles=target/m2/com/fedex/sefs/core/java/jms/itinerary-proxy-service/3.4.0-SNAPSHOT/itinerary-proxy-service-3.4.0-20190524.213238-1-stubs.jar'

    def test_command_line_deployfile(self):
        self.__run(['-a','artifact'])
        a = readfile(as_json=False)
        logging.error( a)
        assert 242 == len(a)
        s = json.loads(a)
        assert '-DpomFile=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0.pom -Dfile=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0.jar -Dpackaging=jar -Dclassifiers=test -Dtypes=jar -Dfiles=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0-test.jar' == s[0]
        self.__run(['-f','deployFile','-a','artifact'])
        a = readfile(as_json=False)
        assert 242 == len(a)
        s = json.loads(a)
        assert '-DpomFile=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0.pom -Dfile=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0.jar -Dpackaging=jar -Dclassifiers=test -Dtypes=jar -Dfiles=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0-test.jar' == s[0]


    def test_command_line_deployfileWithVersionChangeManulal(self):
        self.__run(['-a','artifact'])
        a = readfile(as_json=False)
        #print a
        assert 242 == len(a)
        s = json.loads(a)
        assert '-DpomFile=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0.pom -Dfile=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0.jar -Dpackaging=jar -Dclassifiers=test -Dtypes=jar -Dfiles=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0-test.jar' == s[0]
        out = self.__run(['-f','deployFile','-v', '1.5.1', '-a', 'artifact' ])
        print out
        a = readfile(as_json=False)
        assert 242 == len(a)
        s = json.loads(a)
        assert '-DpomFile=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0.pom -Dfile=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0.jar -Dpackaging=jar -Dclassifiers=test -Dtypes=jar -Dfiles=target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0-test.jar' == s[0]
        xmlstring = read_all('target/m2/com/fedex/artifact/1.5.0/artifact-1.5.0.pom')
        print ET.dump(xmlstring)
        assert '1.5.1' == get_tag(xmlstring, 'version')
        try:
            do_maven_deploy(s)
            assert os.path.exists('target/out/com/mycompany/app/my-app/1.5.1/my-app-1.5.1.pom')
            assert '1.5.1' == get_tag(read_all('target/out/com/mycompany/app/my-app/1.5.1/my-app-1.5.1.pom'),'version')
        except:
            pass

    def test_command_line_deployfileSnapshotOnlyWithVersionChangeManulal(self):
        out = self.__run(['-f','deploySnapshotsOnly','-v', '1.5.1-SNAPSHOT', '-a', 'group' ])
        logging.warn(out)
        a = readfile('deploySnapshotsOnly',as_json=False)
        assert 308 == len(a)
        s = json.loads(a)
        assert '-DpomFile=target/m2/com/fedex/group/1.5.0-SNAPSHOT/group-1.5.0-20190505.053522-20.pom -Dfile=target/m2/com/fedex/group/1.5.0-SNAPSHOT/group-1.5.0-20190505.053522-20.jar -Dpackaging=jar -Dclassifiers=test -Dtypes=jar -Dfiles=target/m2/com/fedex/group/1.5.0-SNAPSHOT/group-1.5.0-20190505.053522-20-test.jar' == s[0]
        xmlstring = read_all('target/m2/com/fedex/group/1.5.0-SNAPSHOT/group-1.5.0-20190505.053522-20.pom')
        assert '1.5.1-SNAPSHOT' == get_tag(xmlstring, 'version')
        try:
            do_maven_deploy(s)
            assert os.path.exists('target/out/com/fedex/group/1.5.0-SNAPSHOT/group-1.5.0-20190505.053522-20.pom')
            assert '1.5.1.SNAPSHOT' == get_tag(read_all('target/out/com/fedex/group/1.5.0-SNAPSHOT/group-1.5.0-20190505.053522-20.pom'),'version')
        except:
            pass

    def test_command_line_proxyrefresh(self):
        a = self.__run(['-f','proxyRefresh','-a', 'artifact'])
        a = readfile('proxyRefresh', as_json=False)
        assert 261 == len(a)
        s = json.loads(a)
        assert 2 == len(s)
        assert s[0] == 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/com/fedex/artifact/1.5.0/artifact-1.5.0-test.jar'
        # overwrite version
        a = self.__run(['-f','proxyRefresh', '-v','1.5.3', '-a', 'artifact'])
        a = readfile('proxyRefresh', as_json=False)
        assert 261 == len(a)
        s = json.loads(a)
        assert 2 == len(s)
        assert s[0] == 'http://sefsmvn.ute.fedex.com:9999/nexus/service/local/repo_groups/public/content/com/fedex/artifact/1.5.3/artifact-1.5.3-test.jar'
