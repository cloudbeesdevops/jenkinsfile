# -*- coding: utf-8 -*-
import sys
import os
import json
import imp
import pytest
import shutil
from workflow_setup import main
from io import TextIOWrapper, BytesIO



class StdoutBuffer(TextIOWrapper):
    def write(self, string):
        try:
            return super(StdoutBuffer, self).write(string)
        except TypeError:
            # redirect encoded byte strings directly to buffer
            return super(StdoutBuffer, self).buffer.write(string)



class TestWorkflowSetup(object):


    def teardown_method(self, method):
        try:
            shutil.rmtree('tmp')
        except:
            pass


    def test_if_tests(self):
        os.makedirs("tmp/src/test/java")
        os.makedirs("tmp/src/main/java")
        assert main('tmp', 'has_tests') == "src/test"
        assert main('tmp', 'has_sources') == "src/main"

    def test_if_tests_python(self):
        os.makedirs("tmp/tests")
        os.makedirs("tmp/src/main/java")
        assert main('tmp', 'has_tests') == "tests"
        assert main('tmp', 'has_sources') == "src/main"

    def test_if_nested_srcs(self):
        os.makedirs("tmp/module/src/main/java")
        os.makedirs("tmp/module/src/test/java")
        os.makedirs("tmp/wap-testing/src/main/java")
        os.makedirs("tmp/wap-testing/src/test/java")
        os.makedirs("tmp/src/main/java")
        assert main('tmp', 'has_tests') == "module/src/test"
        assert main('tmp', 'has_sources') == 'module/src/main,src/main'

    def test_all_command_line_options(self):
        os.makedirs("tmp/module/src/main/java")
        os.makedirs("tmp/module/src/test/java")
        os.makedirs("tmp/src/main/java")
        out = self.__run(['-d','tmp','-t'])
        assert out == "module/src/test"
        out = self.__run(['-d','tmp','-s'])
        assert out == "module/src/main,src/main"
        out = self.__run(['-d','tmp','-s', '-e'])
        assert out == "module\/src\/main,src\/main"



    def __run(self, arguments):
        old_stdout = sys.stdout
        old_args = sys.argv
        sys.argv = ['workflow_setup.py']
        sys.argv.extend(arguments)
        #print sys.argv
        with StdoutBuffer(BytesIO(), sys.stdout.encoding) as sys.stdout:
            runpy = imp.load_source('__main__', 'workflow_setup.py')
            sys.stdout.seek(0)      # jump to the start
            out = sys.stdout.read() # read output
        sys.stdout = old_stdout
        sys.argv = old_args
        return out
