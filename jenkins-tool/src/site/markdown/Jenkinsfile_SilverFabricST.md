
## Jenkinsfile_SilverFabric ST ##

The autodeploySilverFabric step must be run on a specified node and will execute deployments on Silver Fabric. It is a consolidate step for deploying to Silver Fabric.

##Externalized Configuration

The notification can be configured externally by supplying the following environment variables.

| Environment Variable | Description | Default |
|----|
| `SEFS_OPCO` | the Operating Company abbreviation | `N/A` |
| `SEFS_LEVEL` | the level for execution | supplied by `initializeDeploy` or `N/A` |
| `SEFS_VERSION` | the version of the silver properties being used for this deployment | `N/A` |
| `RUN_DEPLOYMENT` | run the deployment | `true` | 
| `RUN_SMOKETEST` | run the smoke test | `true` |
| `NODE_NAME` | the executor label | `ShipmentEFS` |




