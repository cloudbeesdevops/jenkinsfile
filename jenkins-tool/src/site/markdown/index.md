
## The Jenkinsfile ##

Example of a Jenkinsfile using the jenkins_shared_library

 1. loading the shared libraries thru an alias (use the exact syntax)
 2. setup options regarding job control
 3. setup triggers if no jobs have been run for a week
 4. run on any or specific node (override envvar = NODE_NAME)
 5. install or reference tools required for the build (java and maven)
 6. parameters to control and customize the workflow of the pipeline
 7. setup runtime environment
 8. analyse the job based on pulled code, install auxilary tools, and set definitions for rest of the workflow
 9. execute the build, which calculates the build command in maven to enforce quality gates IF our jobtype is CI or CICD
 10. collecting all reporting data, currently unittest,failsafe,jacoco,findbugs, sonar if any testing was done
 11. publishing all reporting for Cloudbees, currently unittest,failsafe,jacoco,findbugs if any testing or reporting was captured
 12. tagging the code IF perform_release_cut was selected in the parameters under 6)
 13. performing the quality gate check with Sonar
 14. uploading to nexus
 15. seek user input on autodeploy if we selected don't as questions in 6) then this will be skipped. times out after 1 minute, CD and CICD as the job selection 6)
 16. deployment to level if CD or CICD was the job type under 6)


```groovy
#!/usr/bin/groovy
import com.fedex.ci.*

// 1. library loading 
library 'libz'
library 'AppServiceAccount'
// 2. step 
pipeline {
  options {
    timeout(time: 15, unit: 'MINUTES')
    timestamps()
    disableConcurrentBuilds()
    retry(1)
  }
// 3. setup trigger for once in a while builds
triggers{ cron('H H/4 * * 0') }
// 4. node assignment 
agent { label env.NODE_NAME }
// 5. install tools and tool references
tools {
    jdk 'JAVA_8'
    maven Definitions.APACHE_MAVEN_VERSION
}
// 6. job parameters for customizing the workflow
  parameters {
    booleanParam(name: 'PERFORM_RELEASE_CUT', defaultValue: false, description: 'Performing a release cut on source + tagging and publishing to nexus.')
    booleanParam(name: 'FORTIFY_SCAN', defaultValue: false, description: '')
    booleanParam(name: 'noADG', defaultValue: false, description: 'Acknowledgde not to pause for autodeploy gate (question).')
    choice(name: 'JOB_TYPE', choices: 'CICD\nCD\nCI' , description: '')
    choice(name: 'LEVEL', choices: 'L1\nL2\nL3' , description: '')
  }
  // 7. setup runtime environment
  environment {
    https_proxy = 'http://internet.proxy.fedex.com:3128'
    http_proxy = 'http://internet.proxy.fedex.com:3128'
  }

  stages {
    // 8. analyse the job based on pulled code, install auxilary tools, and set definitions for rest of the workflow
    stage('Setup') {
        steps {
          script {
            try {
              setup = buildSetup()
            } catch (err) {
              Log.error(this,"buildsteup",err)
              throw err
            }
          }
        }
    }
    // 9. execute the build, which calculates the build command in maven to enforce quality gates IF our jobtype is CI or CICD
    stage('Build') {
      when {
        expression { setup.doBuildStage() }
      }
      steps { builder(perform_release_cut : params.PERFORM_RELEASE_CUT, fortify : params.FORTIFY_SCAN) }
    }
    // 10. collecting all reporting data, currently unittest,failsafe,jacoco,findbugs, sonar if any testing was done
    stage('Collecting Results') {
      when {
        expression { setup.doPublishReportingStage() }
      }
      steps {
        script {
          collectTestResults(setup)
        }
      }
    }
    // 11. publishing all reporting for Cloudbees, currently unittest,failsafe,jacoco,findbugs if any testing or reporting was captured
    stage('Publishing') {
      when {
        expression { setup.doPublishReportingStage() }
      }
      steps {
        script {
          publishTestResults(setup, params.APP_TYPE)
        }
      }
    }
    // 12. performing the quality gate check with Sonar
    stage('Quality Gate') {
      when {
        expression { setup.doQualityGate() }
      }
      steps { script { waitForQualityGate abortPipeline : true } }
    }
    // 13. tagging the code IF perform_release_cut was selected in the parameters under 6)
    stage('Tagging') {
      when {
        expression { params.PERFORM_RELEASE_CUT == true }
      }
      steps { builder(perform_release_cut : params.PERFORM_RELEASE_CUT, fortify : params.FORTIFY_SCAN)
      }
    }
    // 14. uploading to nexus 
    stage('Upload To Nexus') {
      when {
        expression { setup.doUploadSnapshotToNexus() }
      }
      steps { builder(upload : true)
      }
    }
    // 15. seek user input on autodeploy if we selected dont as questions in 6) then this will be skipped. times out after 1 minute
    stage('Autodeployment Gate') {
      when {
        expression { setup.doDeploymentStage() }
      }
      steps {
        script {
          node('master') {
            try {
              Log.debug(this,"autodeploy gate currentBuild begin state '${currentBuild.result}'")
              timeout ( time: 1, unit: "MINUTES" )  {
                answer = input message: "Should we continue with deployment to ${params.LEVEL}?",
                      parameters: [choice(name: 'AutoDeployment', choices: 'no\nyes', description: 'Choose "yes" if you want to deploy this build')]
                setup.doAutoDeployGate = answer == 'yes'
              }
            } catch (err) {
              setup.doAutoDeployGate = false
              currentBuild.result = 'SUCCESS'
              Log.error(this,err)
            }
            Log.debug(this,"autodeploy gate currentBuild end state '${currentBuild.result}'")
          }
        }
      }
    }
    stage('Deployment') {
      when {
        expression { setup.doDeploymentStage() }
      }
      agent { label 'ShipmentEFS' }
      steps {
            deployer( JOB_TYPE : params.JOB_TYPE, LEVEL : params.LEVEL, setup : setup )
      }
    }
  } // stages

  post {
    success {
      step([$class: 'WsCleanup', cleanWhenFailure: false, cleanWhenNotBuilt: false, cleanWhenUnstable: false, notFailBuild: true])
      notification(status : 'Success')
    }
    unstable {
      notification(status : 'Unstable')
    }
    failure {
      notification(status : 'Failed')
    }
    aborted {
      notification(status : 'ABORTED')
    }
    changed {
      step([$class: 'WsCleanup'])
      echo 'Things were different before...'
    }
  }
} // pipeline
```


![Sample Visualization of pipeline in blue ocean](./images/pipeline-blue-ocean.png "Sample Visualization of the pipeline")