



## executeFDeploy ##

| property | type	 | description | default |
|--------|
| release_branch | string | the name release of the deployment items  |  |
| opco | string | the deployment group, mostly defined by the opco codes, but can be expanded to non standard abbreviations |`FXS` |
| level | String | The environment identified [L1/2/3/4/5/6,PROD] | `L1` |
| fdaction | string | the command action string (see fdeploy documentation) | `test-inspect` | 
| fdjson | string | the application deployment descriptor json as a string |  |

The `executeFDeploy` executes deployments on a defined set of applications. 

More information about `fdeploy` can be found on [the sitedocs :
 for Fdeploy](http://maven.ground.fedex.com/site/sefs_silverControl/sefs_silverControl-6.0.3-SNAPSHOT/sefs_silverControl/)

For example: We would like to deploy the `fxs-dashboard-web` component to `L2` with the `2.0.0-SNAPSHOT` version of the release branch in `fxs-deploy`.

```groovy
      timestamps {
   		node {
   			executeFDeploy {
               opco = 'FXS'
               level = 'L2'
               release_branch = '2.0.0-SNAPSHOT'
               fdaction = 'deploy-deploy'
               fdjson = 'fxs-dashboard-web.json'
   			}
         } // node
   	} // timestamps
```

##Externalized Configuration

The notification can be configured externally by supplying the following environment variables.

| Environment Variable | Description | Default |
|----|



