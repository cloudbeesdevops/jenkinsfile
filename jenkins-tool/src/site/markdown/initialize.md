
## initialize ##

| property | type	 | description | default | External Variable |
|--------|
| `cron` | String | see "Cron syntax" | `H/3 * * * *` (every 3min) | `BUILD_SCHEDULE` |
| `release_tag` | String | Fixed tag for releasing a product. Normally not be used explicitly since its a transient variable to perform flow control. The version form should be in the ***OSGI format*** also known as ***Semantic Versioning*** | N/A | N/A |
| `concurrency` | Boolean | Allow concurrent builds of this pipeline | `false` | N/A |
| `build_recepients` | String | comma separated list of email addresses to report on all build steps | `` | `BUILD_RECEPIENTS` |
| `build_failure_recepients` | String | comma separated list of email addresses to report failures only | `` | `BUILD_FAILURE_RECEPIENTS` |
| `goal` | String | maven build goal | `clean deploy` | `BUILD_GOAL` |
| 
| run_jacoco | Boolean | include jacoco run | `false` | `RUN_JACOCO` |
	
Example of a ***initialize*** plugin step that gets send to the system team

```groovy
#!/usr/bin/groovy
@Library('libz') _

# Must be set in the initialization phase before executing anything
# setting schedule run every fifteen minutes during business hours.
# ALL TIMES ARE SET IN GMT
initialize {
	cron = 'H/15 * * * (1-6)'
}

node {
}
```


###Cron syntax###
This field follows the syntax of cron (with minor differences). Specifically, each line consists of 5 fields separated by TAB or whitespace:
```
MINUTE HOUR DOM MONTH DOW
MINUTE	Minutes within the hour (0–59)
HOUR	The hour of the day (0–23)
DOM	The day of the month (1–31)
MONTH	The month (1–12)
DOW	The day of the week (0–7) where 0 and 7 are Sunday.
```
To specify multiple values for one field, the following operators are available. In the order of precedence,

-	specifies all valid values
-	M-N specifies a range of values
-	M-N/X or */X steps by intervals of X through the specified range or whole valid range
-	A,B,...,Z enumerates multiple values

To allow periodically scheduled tasks to produce even load on the system, the symbol `H` (for “hash”) should be used wherever possible. For example, using `0 0 * * *` for a dozen daily jobs will cause a large spike at midnight. In contrast, using `H H * * *` would still execute each job once a day, but not all at the same time, better using limited resources.

The H symbol can be used with a range. For example, `H H(0-7) * * *` means some time **between 12:00 AM (midnight) to 7:59 AM**. You can also use step intervals with H, with or without ranges.

The `H` symbol can be thought of as a random value over a range, but it actually is a hash of the job name, not a random function, so that the value remains stable for any given project.

Beware that for the day of month field, short cycles such as `*/3` or `H/3` will not work consistently near the end of most months, due to variable month lengths. For example, `*/3` will run on the 1st, 4th, …31st days of a long month, then again the next day of the next month. Hashes are always chosen in the 1-28 range, so `H/3` will produce a gap between runs of between 3 and 6 days at the end of a month. (Longer cycles will also have inconsistent lengths but the effect may be relatively less noticeable.)

Empty lines and lines that start with # will be ignored as comments.

In addition, `@yearly, @annually, @monthly, @weekly, @daily, @midnight`, and `@hourly` are supported as convenient aliases. These use the hash system for automatic balancing. For example, `@hourly` is the same as `H * * * *` and could mean at any time during the hour. @midnight actually means some time between `12:00 AM` and `2:59 AM.`

Examples:
```
# every fifteen minutes (perhaps at :07, :22, :37, :52)
H/15 * * * *
# every ten minutes in the first half of every hour (three times, perhaps at :04, :14, :24)
H(0-29)/10 * * * *
# once every two hours at 45 minutes past the hour starting at 9:45 AM and finishing at 3:45 PM every weekday.
45 9-16/2 * * 1-5
# once in every two hours slot between 9 AM and 5 PM every weekday (perhaps at 10:38 AM, 12:38 PM, 2:38 PM, 4:38 PM)
H H(9-16)/2 * * 1-5# once a day on the 1st and 15th of every month except December
H H 1,15 1-11 
```