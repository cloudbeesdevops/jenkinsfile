

## buildPlugin ##

| property | type	 | description | default |
|--------|
| parallel | boolean | true for executing maven in threading mode increasing performance on long lasting jobs. | `false` |
| headless | boolean | true for running the maven command in xvfb-run mode, allowing graphics access to console |`false` |
| nodeName | String | the name of the executor node name for running the job | `Java7` |
| goal | String | the maven goal(s) executed on the job. | `clean deploy` | 
| stageName | String | the display name of the step in the pipeline. | `Build` |
| operationMode | String | if in reactor mode telling the job to fail fast (-ff) or fail at the end (default=-fae), see mvn -h for more details. | `-fae` |
| module | String | the module or directory we start executing the job. | current dir |
| perform_release_cut | boolean | true if we include release cut in the run. will create an automated versioned release, increasing `1.0.0-SNAPSHOT` to `1.0.0` (depending on existing releases). | `false` |
| incremental_versioning | boolean | true if we increment the release from `1.0.0-SNAPSHOT` to `1.0.1.0` | `false` |
	
The buildPlugin step must be run on a specified node. Example of a build plugin step that gets executed on node '`ShipmentEFS`' and executes '`clean deploy`' on a `headless` configuration.

```groovy
node("ShipmentEFS") {
	buildPrepare { }
	buildCheckout { }
	buildPlugin {
		nodeName = "ShipmentEFS"
		goal = 'clean deploy'
		headless = true
	}
}
```


----------


##Externalized Configuration

The notification can be configured externally by supplying the following environment variables.

| Environment Variable | Description | Default |
|----|
| `NODE_NAME` | the executors name as it appears in the list of executors | `Java7` |
| `BUILD_MODULE` | the (sub)module being worked on | `.` |
| `BUILD_GOAL` | the goal being executed | `clean deploy` |
| `BUILD_OPTIONS` | the list of extra build options like -Dpropery=value  | `[none]` |



----------


##NODE_NAME syntax
Typically the builds would run on a particular node for all the branches if that is not the default ***Java7*** then we can specify alternate node names which correspond with the executor names or labels 

For example look at https://jenkins.prod.cloud.fedex.com:8443/jenkins/computer/Weblogic%20(1603)/:

| Agent | Labels |
|---|---|
| `Weblogic (1603)` | `1603, Java7, Java8, Weblogic` |

So our node name notation would be:

    NODE_NAME=1603             # pin to 1603 machine
    NODE_NAME='Java7 && !1603' # pin to a Java7 machine but not 1603

How do we setup up the multibranch build when branches 1.0 need to be on a Java7 agent and 2.0 on Java8, but have the same build execution flow: 

| Branch | NODE_NAME |
|--|--|
| `1.0` | `Java7` | 
| `2.0` | `Java8` |

```scss
    // select java7 for the 1.0 branch and the rest of the branches are Java8
    NODE_NAME=Java7#branches/1.0,Java8
    // select java8 for the 2.0 branch and the rest of the branches are Java7
    NODE_NAME=Java8#branches/2.0,Java7
    // etc etc 
    NODE_NAME=Java8#branches/2.0,Java8#branches/3.0,Java7
```
