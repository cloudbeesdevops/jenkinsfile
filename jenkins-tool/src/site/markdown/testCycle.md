
## testJUnit ##

| property | type	 | description | default |
|--------|
| parallel | boolean | true for executing maven in threading mode increasing performance on long lasting jobs. | `false` |
| headless | boolean | true for running the maven command in xvfb-run mode, allowing graphics access to console |`false` |
| nodeName | String | the name of the executor node name for running the job | `Java7` |
| goal | String | the maven goal(s) executed on the job. | `clean deploy` | 
| operationMode | String | if in reactor mode telling the job to fail fast (-ff) or fail at the end (default=-fae), see mvn -h for more details. | `-fae` |
| module | String | the module or directory we start executing the job. | current dir |
	
The buildPlugin step must be run on a specified node. Example of a build plugin step that gets executed on node '`ShipmentEFS`' and executes '`clean deploy`' on a `headless` configuration.

```groovy
def RUN_PMD = True
def RUN_CHECKSTYLE = True


node("ShipmentEFS") {
	buildPrepare { }
	buildCheckout { }
	testCycle {
		nodeName = "ShipmentEFS"
	}
}
```

##Externalized Configuration

The notification can be configured externally by supplying the following environment variables.

| Environment Variable | Description | Default |
|----|---|---|
| `RUN_JACOCO` | execute the goal for Java CodeCoverage | `false` |
| `RUN_TEST` | execute the goal for Unit Testing | `true` |
| `RUN_PMD` | execute the goal for PMD reporting | `false` |
| `RUN_CHECKSTYLE` | execute the goal for checkstyle | `false` |
| `RUN_FINDBUGS` | execute the goal for findbugs | `false` |



