

## buildPrepare ##

| property | type	 | description |
|--------|--|--|
| url | String | the url of the settings.xml for execution of the job (default = http://sefsmvn.ute.fedex.com/settings.xml). |
	
Example of a build plugin step that gets executed on node '`Java8`' and executes '`clean deploy`' on a `parallel threading` configuration.

```groovy
node("Java8") {
	buildPrepare {
    	url = http://maven.ground.fedex.com/settings-fxg-dev.xml
	}
	buildCheckout { }
	buildPlugin {
		goal = 'clean deploy'
		parallel = true
	}
}
```

----------


##Externalized Configuration

The notification can be configured externally by supplying the following environment variables.

| Environment Variable | Description | Default |
|----|--|--|
| `BUILD_SETTINGS_XML` | the settings xml for maven with repository information | `http://maven.ground.fedex.com/sefs/settings.xml` |


----------

