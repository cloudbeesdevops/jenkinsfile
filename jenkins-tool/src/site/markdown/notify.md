

## notify ##

| Property | Type	 | Description | Default |
|--------|---|---|---|
| status | String | Any status that needs reported in the heading and content. The Success or Failed status will result in a emojo in the subject. | empty string |
| message | String | additional message for the body, typically the error that gets caught in the failure. | `no message` |
| recipients | String | comma delimited string with the recipients of this message notification | `akaan@fedex.com` |
| context | String | Project / Application indicator used in the subject | `PRJ` |


Example of a notify plugin step that gets send to the system team

```groovy
notify (
	status = 'Failure',
	message = 'application failed to deploy',
	recipients = 'migthymouse@corp.ds.fedex.com,mymanager@fedex.com,akaan@fedex.com',
	context = 'myproject'
)
```
All failures will be sent to `SEFS-BuildFailures@corp.ds.fedex.com` no matter which recipients are specified.

##Externalized Configuration

The notification can be configured externally by supplying the following environment variables.

| Environment Variable | Description |
|----|--|
| `BUILD_RECEPIENTS` | email all steps to build receipents comma seperated email address list |
| `BUILD_FAILURE_RECEPIENTS` | email failures only to the comma separated list of email addresses |






##Example Failure Email


![Sample Email of Failure in Outlook 2013](./images/email_sample.png "Sample Email of Failure in Outlook 2013")

When incorrect email addresses are entered your job will fail.
```
com.sun.mail.smtp.SMTPSendFailedException: 250 2.0.0 v37Ami4a010447 Message accepted for delivery
;
  nested exception is:
	com.sun.mail.smtp.SMTPAddressFailedException: 550 5.0.0 <mymanager@fedex.com>... User unknown

	at com.sun.mail.smtp.SMTPTransport.sendMessage(SMTPTransport.java:1112)
	at javax.mail.Transport.send0(Transport.java:195)
	at javax.mail.Transport.send(Transport.java:124)
	at org.jenkinsci.plugins.workflow.steps.MailStep$MailStepExecution.run(MailStep.java:136)
	at org.jenkinsci.plugins.workflow.steps.MailStep$MailStepExecution.run(MailStep.java:122)
	at org.jenkinsci.plugins.workflow.steps.SynchronousNonBlockingStepExecution$1$1.call(SynchronousNonBlockingStepExecution.java:49)
	at hudson.security.ACL.impersonate(ACL.java:213)
	at org.jenkinsci.plugins.workflow.steps.SynchronousNonBlockingStepExecution$1.run(SynchronousNonBlockingStepExecution.java:46)
	at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
	at java.util.concurrent.FutureTask.run(FutureTask.java:266)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
	at java.lang.Thread.run(Thread.java:745)
Caused by: com.sun.mail.smtp.SMTPAddressFailedException: 550 5.0.0 <mymanager@fedex.com>... User unknown

	at com.sun.mail.smtp.SMTPTransport.rcptTo(SMTPTransport.java:1686)
	at com.sun.mail.smtp.SMTPTransport.sendMessage(SMTPTransport.java:1098)
	... 12 more
Finished: FAILURE
```
