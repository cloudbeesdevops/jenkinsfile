
## `Environment Control Variables`


| Variable | Description | Default | Example |
|-----|------|------|------|
| `noQC` | Disable the quality gate step | none | `noQC=Right` |
| `noSonar` | Disable the sonar analysis | none | `noSonar=Right` |
| `BUILD_SETTINGS_XML` | The settings xml for maven as url | `http://sefsmvn.ute.fedex.com/settings.xml | `BUILD_SETTINGS_XML=http://maven.ground.fedex.com/settings.xml` |
| `BUILD_MODULE` | The relative directory where the main module is located | `.` (current workspace) | `BUILD_MODULE=trip-event` |
| `BUILD_FAILURE_RECEPIENTS` | Email notification for failures | `noreply@jenkins-shipment.web.fedex.com` | `BUILD_FAILURE_RECEPIENTS=akaan@fedex.com` |
| `BUILD_RECEPIENTS` | Email notifications all builds | `noreply@jenkins-shipment.web.fedex.com` | `BUILD_RECEPIENTS=akaan@fedex.com` |
| `APP_TYPE` | The application type [BE/BW/JAVA/POM/PCF-JAVA] | `java` | `APP_TYPE=POM` |
| `JOB_TYPE` | Type of functionality executed in the pipeline | `CICD` | `JOB_TYPE=CD` |
| `NODE_NAME` | The executor node we want to execute explicitly on | none | `NODE_NAME=python-2.7` |
| `DEPLOY_NODE` | The executor node want to perform the CD part on | =`NODE_NAME` | `DEPLOY_NODE=JAVA_8` |
| `LEVEL` | The level we targetting the deploy for CD | none | `LEVEL=L1/L3/L4` |
 
