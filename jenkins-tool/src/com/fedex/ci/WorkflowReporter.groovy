package com.fedex.ci
import com.fedex.ci.*



class WorkflowReporter implements Serializable {

  def setup = null

  public WorkflowReporter(Definitions setup) {
    this.setup = setup
  }
  static boolean has(TreeMap e, String key) {
		return has(e.containsKey(key) && 'true'.equalsIgnoreCase(e.get(key)))
  }

  static String has(boolean value) {
    return value == false ? "\u26d4" : "\u2705"
  }
  static String has(String value) {
     return has(value == 'true')
 }
 static String has(Step value) {
    return has(value.state)
}
  static String trim(String value) {
    return
  }

  public String toString() {
    def report = ""

   // requires script approval
   def e = setup.env.getEnvironment()
   report += "====== Flags ========\n"
   if (setup == null || setup.flagmap == null) {
      return report
   }
   for (key in setup.flagmap.keySet()) {
      report += has(setup.flagmap["${key}"]) + " - has flag .${key}\n"
   }
   report += "====== Specified Phase (${setup.jobType}) ========\n"
   report += has(setup.doCI) + " => doCI phase\n"
   report += has(setup.doCD) + " => doCD phase\n"
   report += has(setup.doCT) + " => doCT phase\n"
   report += has(setup.doST) + " => doST phase\n"


    report += "====== Environment Overwrites ========\n"
  report += has(e,'NO_ENFORCER') + " - do enforce rules (noEnforcer=true)\n"
  report += has(e, 'NO_TESTING') + " - do not execute tests (NO_TESTING=true)\n"
  report += has(e, 'NO_SONAR') + " - do Sonar Quality gate check (NO_SONAR=true)\n"
  report += has(e, 'NO_LEGACY_PUB') + " - do legacy publishing LDD (NO_LEGACY_PUB=true)\n"
  report += has(e, 'NO_ARCHIVING') + " - do legacy publishing LDD (NO_ARCHIVING=true)\n"
  report += has(e, 'noQG') + " - no quality gate (noQG=true)\n"
  report += has(e, 'noADG') + " - no auto deploygate (noADG=true)\n"
  report += has(e, 'DISABLE_ITTEST') + " - no ittest executed (DISABLE_ITTEST=true)\n"
  report += has(e, 'DISABLE_SMOKETEST') + " - no smoketests executed (DISABLE_SMOKETEST=true)\n"

    report += "====== Calculated ========\n"
    report += has(setup.hasCode) + " - has sources\n"
    report += has(setup.hasTests) + " - has tests\n"
    report += has(setup.hasPoms) + " - has pom\n"
    report += has(setup.performReleaseCut) + " - perform release cut\n"
    report += "====== STAGES ========\n"

    report += has(setup.doBuildStage()) + "       - CI: do build stage\n"
    report += has(setup.doPublishing)          + "       - CI: do publish report stage\n"
    report += has(setup.doQualityGate )     + "       - CI: do quality gate stage\n"
    report += has( setup.doUploadSnapshotToNexus )  + "       - CI: do upload snapshot to nexus\n"
    report += has( setup.doAutoDeployGate ) + "       - CI: do autodeploy gate\n"
    report += has( setup.doPreSmokeTest )   + "       - CD : do pre-smoketest stage\n"
    report += has( setup.doDeployment )               + "       - CD: do deployment stage\n"
    report += has( setup.doPostSmokeTest )               + "       - CT or CD: do post-smoketest stage\n"
    report += has( setup.doITs )               + "       - CT or CD: do integrationTesting stage\n"
    report += has( setup.doE2Es )               + "       - CT or CD: do integrationTesting stage\n"
    report += has( setup.doDeploymentStage() && setup.doAutoDeployGate ) + "       - CI or CD: sdo release gate\n"
    report += has(setup.doReleaseGate) + " => doRelease phase\n"
    report += has( setup.doTagging )  + "       - do release tagging stage\n"
    report += has( setup.doUploadReleaseToNexus )  + "       - do uploading release stage\n"

    return report
  }


  public String toStringCI() {
     return "\u261d Definitions \n\u261d workflow = [doCI=" + has(setup.doCI) + ", doCD=" + has(setup.doCD) +
			", doBuild=" +
			has(setup.doBuild.state) + "," +
			", doSonar=" +
			has(setup.doSonar.state) +
			", doPublishing=" +
			has(setup.doPublishing.state) +
			", doUploadSnapshotToNexus=" +
			has(setup.doUploadSnapshotToNexus.state)  +
			",  quality_gate=" +
			has(setup.doQualityGate.state) +
			", doAutoDeployGate=" +
			has(setup.doAutoDeployGate.state) +
     		", doPreSmokeTest=" +
			has(setup.doPreSmokeTest.state) +
			"}, doDeployment=" +
			has(setup.doDeployment.state) +
			", doPostSmokeTest=" +
			has(setup.doPostSmokeTest.state) +
			", doITs=" +
			has(setup.doITs.state) + "," +
			"doE2Es=" +
			has(setup.doE2Es.state) +
			", doReleaseGate=" +
			has (setup.doReleaseGate.state) +
			", workflow_string=${setup.workflow_string}] \n\
			\u261d internal = [hasTests=" + has(setup.hasTests) + ", hasCode=" + has(setup.hasCode)
			+ ", hasPoms=" + has(setup.hasPoms)
			+ ", performReleaseCut=" + has(setup.performReleaseCut)
			+ " , testPaths=${setup.testPaths}, sourcePaths=${setup.sourcePaths}, nodeName=${setup.nodeName}, noLegacyPublishing="
			+ has(setup.doLegacyPublishing.state) +
			", fortifyScan=${setup.fortifyScan}, jacocoReportsXml=${setup.jacocoReportsXml}, cobuteraReportsXml=${setup.cobuteraReportsXml}] \n\
			\u261d gates    = [ askAutoDeployQuestion="
			+ has(setup.askAutoDeployQuestion.state)
			+ ",  quality_gate="
			+ has(setup.doQualityGate.state) +
			"]\n\
			\u261d global   = [appType=${setup.appType}, jobType=${setup.jobType}, loglevel=${setup.loglevel}, VCS_SOURCE=${setup.VCS_SOURCE}, JAVA_HOME=${setup.JAVA_HOME}, MAVEN_HOME=${setup.MAVEN_HOME}, fdeployVersion=${setup.fdeployVersion}]\n\
			\u261d release  = [version=${setup.version}, artifactId=${setup.artifactId}, groupId=${setup.groupId}, versionBase=${setup.versionBase}, nextVersion=${setup.nextVersion}]\n\
			]"
  }

}
