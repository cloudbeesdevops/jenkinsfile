package com.fedex.ci

import java.io.*
import com.fedex.ci.Step
import groovy.json.JsonSlurper
import java.util.*
import groovy.transform.Field

class Definitions implements Serializable {
    public static String DEPLOYMENT_INSTALL_URL = 'http://sefsmvn.ute.fedex.com/fdeploy-install.sh'
    public static String APACHE_MAVEN_VERSION = 'apache-maven-3.3.9'
    public static String NODE_JS_VERSION = 'NodeJS-8.9.4'
    public static String EXCLUDE_ALL_BUT_DEPLOY = '-DskipTests=true -DskipITs=true -Dskip.yarn=true -Dskip.bower=true -Dskip.grunt=true -Dskip.gulp=true -Dskip.karma=true -Dskip.webpack=true'
    public static String SEFS_ST_CICD_TEAMS_HOOK_URL = 'https://outlook.office.com/webhook/9f2bd5a1-809f-4fbb-afc6-f37377e61cb0@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/c3899b1fdf0b4b5dbdaa0888c11700f4/22e82a6b-9388-455e-926e-e72cf565ad9b'
    public static String MATTERMOST_HOOK_URL = 'https://mattermost.paas.fedex.com/hooks/brcuuinskidoxq5kx7fnfr9mpw'
    public static String LDD_HOOK_URL = 'http://c0009869.test.cloud.fedex.com:8090/sefs-dashboard'
    public static String SEFS_ST_CICD_CHANNEL = 'https://teams.microsoft.com/l/channel/19%3a7ee18be058c24f19ba059d45de26156c%40thread.skype/SEFS-ST-CICD?groupId=9f2bd5a1-809f-4fbb-afc6-f37377e61cb0&tenantId=b945c813-dce6-41f8-8457-5a12c2fe15bf'
    public static String AUTO_DEPLOY_HOOK_URL = 'https://outlook.office.com/webhook/9f2bd5a1-809f-4fbb-afc6-f37377e61cb0@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/7d387272fa9945ada5e344dd2f8a5a21/22e82a6b-9388-455e-926e-e72cf565ad9b'
    public static String AUTOBAHN_HOOK_URL = 'https://outlook.office.com/webhook/45a76f07-9c71-462b-9e57-a98ab6274372@b945c813-dce6-41f8-8457-5a12c2fe15bf/IncomingWebhook/550d54a53bf74033b8df5e3b6fc57d2e/22e82a6b-9388-455e-926e-e72cf565ad9b'
    public static String LEVEL_CHOICES = '\nL1\nL2\nL2-B\nL3\nL3-B\nL3-FXG\nL4\nL4-EDC\nL4-EDC-FXG\nL4-FXG\nL6\nL6-FXG\nL6-WTC-FXG\nPROD\nPROD-EDC-FXG\nPROD-FXG\nPROD-WTC-FXG'
    	String name // name of the workflow
    def String fdeployVersion
    def boolean doCI = false
    def boolean doCD = false
    def boolean doCT = false
    def boolean doST = false
    def boolean doRC = false
    def Step doSonar = new Step(true)
    def Step doLegacyPublishing = new Step(true)
    def Step doTesting = new Step(true)
    def Step doBuild = new Step()
    def Step doPublishing = new Step()
    def Step doQualityGate = new Step()
    def Step doUploadSnapshotToNexus = new Step()
    def Step doPreSmokeTest = new Step()
    def Step doDeployment = new Step()
    def Step doPostSmokeTest = new Step()
    def Step doITs = new Step()
    def Step doE2Es = new Step()
    def Step doUploadReleaseToNexus = new Step()
    def Step doAutoDeployGate = new Step()
    def Step doTagging = new Step()
    def Step doRelease = new Step()
    def Step doReleaseGate = new Step()
    def boolean doEnforcer = true
    def boolean hasTests = false
    def boolean hasCode = false
    def boolean hasPoms = false
    def String selected_level = 'L1'
    def String appType
    def int loglevel = 40
    def String module = '.'
    def String VCS_SOURCE
    def String version
    def String artifactId
    def String groupId
    def String versionBase
    def String testPaths = 'src/test'
    def String sourcePaths = 'src/main'
    def String nodeName
    def String jacocoReportsXml = 'target/site/jacoco/jacoco.xml'
    def String cobuteraReportsXml = null
    def String binaries = 'target'
    def lib = null
    def String scmDisabled
    def String scmRepo
    def String JAVA_HOME
    def String MAVEN_HOME
    def String SONAR_SCAN_HOME
    def String WORKSPACE
    def String fortifyScan
    def boolean performReleaseCut = false
    def String sonarKey
    def String sonarName
    def String jobpath
    def String jobslist
    def String jobType
    def String nextVersion
    def String workflow_string = ""
    def Map flagmap = null
    def WorkflowReporter w = null
    def env = null

   public Definitions(String jobpath) {
      env = [:]
      w = new WorkflowReporter(this)
      this.jobpath = jobpath
   }

    public def doBuildStage() {
      return doBuild.state
    }

    public def doDeploymentStage() {
      return doDeployment.state
    }

    public def doAutoDeployGateStage() {
      return doAutoDeployGate.state
    }

    public def doPreSmokeTestingStage() {
      return doPreSmokeTest.state
    }

    public def doPostSmokeTestingStage() {
      return doPostSmokeTest.state
    }

    public def doIntegrationTestingStage() {
      return doITs.state
    }

    public def doPublishReportingStage() {
      return doPublishing.state
    }

    public def doUploadSnapshotToNexus() {
      return  doUploadSnapshotToNexus.state
    }
    public def doUploadReleaseToNexus() {
      return doUploadReleaseToNexus.state
    }

    public def doSonar() {
      return doSonar && hasPoms && hasCode
    }

   public def doE2ETestingStage() {
      return doE2Es.state
   }

    public def doReleaseGateStage() {
      return doReleaseGate.state
   }

    public def doRelease() {
      return doRelease.state
   }

    public def doQualityGate() {
      return doQualityGate.state
    }

    public def isPOMType() {
      return appType != null && appType.toUpperCase() == 'POM'
    }

    public def isJava() {
      return nodeName == null || !nodeName.toUpperCase().contains('PYTHON')
    }

    public def defaultEnv() {
      def arr=[]
      def pathext = ''
      if (JAVA_HOME != null) {
        arr << "JAVA_HOME=${JAVA_HOME}"
        pathext += "${JAVA_HOME}/bin"
      }
      if (MAVEN_HOME != null) {
        arr << "M2_HOME=${MAVEN_HOME}"
        pathext += "${MAVEN_HOME}/bin"

      }
      arr << "https_proxy=https://internet.proxy.fedex.com:3128"
      arr << "http_proxy=https://internet.proxy.fedex.com:3128"
      if (pathext != '') {
        arr << "PATH+MAVEN=${pathext}"
      }
      arr << "MAVEN_OPTS=-Xmx2048m -Xms2048m -Djava.security.egd=file:/dev/urandom -Dmaven.artifact.threads=10 -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true"
      return arr
    }

    public def getSonarName() {
      if (sonarName == null) {
        def a = artifactId.toUpperCase()
        if (!a.startsWith("SEFS")) {
          a = "SEFS_" + a
          sonarName = normalize(a)
        }
      }
      return sonarName
    }

    public def getSonarKey() {
      if (sonarKey == null) {
        def a = artifactId.toUpperCase()
        if (!a.startsWith("SEFS")) {
          a = "SEFS_" + a
        }
        sonarKey = normalize(a)
      }
      return sonarKey
    }

    public def getSonarTestPaths() {
      return hasTests && (testPaths == null || testPaths == '') ? 'src/test' : testPaths
    }

    public def getSonarSourcePaths() {
      return hasCode && (sourcePaths == null || sourcePaths == '') ? 'src/main' : sourcePaths
    }

    public def getWorkspaceRepositoryOptions() {
       return "-DaltReleaseDeploymentRepository=releases::default::file://${WORKSPACE}/target/m2 -DaltSnapshotDeploymentRepository=snapshots::default::file://${WORKSPACE}/target/m2"
    }

    public def ask(script, message, currentBuild) {
      def answer = 'no'
      try {
         Log.debug(script,"autodeploy gate currentBuild begin state '${currentBuild.result}'")
         script.timeout ( time: 1, unit: "MINUTES" )  {
           answer = script.input message: message,
                 parameters: [
                  script.choice(name: 'AutoDeployment', choices: 'no\nyes', description: 'Choose "yes" if you want to deploy this build')]
           if (answer != 'yes') {
             disableCD()
           }
         } // timeout
       } catch (err) {
         disableCD()
         currentBuild.result = 'SUCCESS'
         Log.error(script,err)
       }
       script.echo "${this.toString()}"
       return answer
    } // ask

    public static def normalize(a) {
      if (a != null) {
        a = a.toUpperCase()
        a = a.replace('-', '_')
        a = a.replace(':', '_')
      }
      return a
    }


    // ------
    public void release(flag) {
      doReleaseGate.state = flag
      doTagging.state = flag
      doUploadReleaseToNexus.state = flag
      performReleaseCut = flag
      workflow_string = workflow_string + (flag ? 'R' : 'r')
    }
    public void enableRelease() {
      release(true)
    }
    public void disableRelease() {
      release(false)
    }
    // ------

    def checkReleaseAnswer(release_answer) {
      // default turn on deploygate
      disableRelease()
      if (performReleaseCut == true || (doCI == true && release_answer == 'yes')) {
        enableRelease()
        enableBuild()
      }
    }

    def enableCD() {
      doDeployment.on()
      doAutoDeployGate.on()
      doPreSmokeTest.on()
      doPostSmokeTest.on()
      doITs.on()
      doE2Es.on()
    }

    def disableCD() {
      doDeployment.off()
      doAutoDeployGate.off()
      doPreSmokeTest.off()
      doPostSmokeTest.off()
      doITs.off()
      doE2Es.off()
    }

    def enableCI() {
      doBuild.on()
      doPublishing.on()
      doAutoDeployGate.on()
      doQualityGate.on()
      doUploadSnapshotToNexus.on()
      doSonar.on()
      doLegacyPublishing.on()

    }
   def disableCI() {
      doBuild.off()
      doPublishing.off()
      doAutoDeployGate.off()
      doQualityGate.off()
      doUploadSnapshotToNexus.off()
      doSonar.disable()
      doLegacyPublishing.off()
   }

    def disableAll() {
    	disableCI()
    	disableCD()
    }

    def cover_ci(xnoSonar, xnoLegacy) {
        enableCI()
        //println( "\nnosonar=" + xnoSonar + ", nolegacy="+ xnoLegacy)
        //println( printStateSonar())
	   	 if (!hasPoms) {
	   		 doBuild.off()
	   		 doSonar.off()
	   	 }
        if (!hasCode) {
       	 doSonar.off()
       	 doPublishing.off()
       	 doQualityGate.off()
       	 doAutoDeployGate.off()
        }
        if (!hasTests && !hasCode) {
       	 	doPublishing.off()
        }
        if (xnoSonar) {
           doSonar.off()
           doQualityGate.off()
        }
        if (xnoLegacy) {
           doLegacyPublishing.off()
        }
        if (xnoLegacy && xnoSonar) {
           doPublishing.off()
        }
        if ('POM' == appType) {
        	doQualityGate.off()
        	doAutoDeployGate.off()
        	doPublishing.off()
        }
    }

    def cover_ct() {
        if (flagmap['smoketest']) {
            doPreSmokeTest.on()
            doPostSmokeTest.on()
         }
         if (flagmap['ittest']) {
               doITs.on()
         }
         if (flagmap['e2e']) {
               doE2Es.on()
         }
    }
    def cover_cd() {
      doDeployment.on()
      cover_ct()
   }
   def cover_rc(do_PerformReleaseCut) {
      if (do_PerformReleaseCut) {
         doTagging.on()
         doRelease.on()
         doUploadReleaseToNexus.on()
         doUploadSnapshotToNexus.off()
      } else {
         doRelease.off()
         doUploadReleaseToNexus.off()
         doTagging.off()
         doUploadSnapshotToNexus.on()
      }
  }

    def printStateSonar(msg) {
        return  msg + ": dosonar=" + doSonar.state + ", dolegacy="+ doLegacyPublishing.state + ", qualitygate=" + doQualityGate.state + ", autodeploygate = " + doAutoDeployGate.state
    }

    def isSet(envvar) {
    	//println("var=" +envvar + ", "  +  (envvar == '' || envvar == "null" || envvar == "false"))
      if (envvar == '' || envvar == "null" || envvar == "false") {
    	   return false
      }
      return true
   }

    // ---------------- ---------------- ---------------- ----------------
	// ----------------

    def setWorkflow(env, params) {
      workflow_string = "" // reset wf
      disableCD()
      disableCI()
      // take default type from the environment and see if params contains
		// overwrite
      jobType = "${env.JOB_TYPE}"
      appType = "${env.APP_TYPE}"
      def no_Legacy = isSet("${env.NO_LEGACY_PUB}")
      def no_Testing = isSet("${env.NO_TESTING}")
      def no_Sonar = isSet("${env.NO_SONAR}")
      def do_PerformReleaseCut = isSet("${params.PERFORM_RELEASE_CUT}")
      def do_BypassAutoDeployGate = isSet("${params.noADG}")
      if (params.JOB_TYPE != null && params.JOB_TYPE != '') {
          jobType = params.JOB_TYPE
      }
      // print params
      doCT = false
      doCI = false
      doCD = false
      doST = false
      disableAll()
      if (jobType == 'CT') {
         doCT = true
         cover_ct()
         doPostSmokeTest.off()

      } else
      if (jobType =='ST') {
        doST = true
        if (flagmap['smoketest']) {
        	doPreSmokeTest.on()
        	doPostSmokeTest.disable()
        }
        return
      } else
      if (jobType == 'CI'  ) {
    	  doCI = true
    	  if (flagmap['build']) {
    	         cover_ci(no_Sonar, no_Legacy)
    	         doAutoDeployGate.off()
               doReleaseGate.off()
               cover_rc(do_PerformReleaseCut)
    	  }
    	  else {
    		  disableCI()
    	  }
          //println( printStateSonar("#3"))
      } else
      if (jobType == 'CD') {
          doCD = true
         if (flagmap['deploy']) {
        	 cover_cd()
         }
      } else
      if (jobType == 'CICD') {
         doCD = true
         doCI = true
         if (flagmap['build'] == true) {
        	 cover_ci(no_Sonar, no_Legacy)
	         if (hasTests) {
 	         	doPublishing.on()
 	         }
           	 if (hasTests && !hasCode) {
           		 doAutoDeployGate.on()
           	 }
             if (do_BypassAutoDeployGate) {
                doAutoDeployGate.off()
             }
          cover_rc(do_PerformReleaseCut)
         }
         if (flagmap['deploy']) {
            cover_cd()
         } else {
            disableCD()
         }

      } else
      if (jobType == 'RC' && flagmap['build'] == true) {
         cover_rc(true)
         cover_ci(no_Sonar, no_Legacy)
         doAutoDeployGate.off()
         doReleaseGate.off()
         doUploadSnapshotToNexus.off()
      }

      if (doCI) {
         // if (params.PERFORM_RELEASE_CUT != null &&
         //   (params.PERFORM_RELEASE_CUT == 'true' || params.PERFORM_RELEASE_CUT == true)) {
         //     doRelease.state = true
         //     performReleaseCut = true
         // } else {
         //    disableRelease()
         // }
         //println( "l=" + no_Legacy + ", s=" + no_Sonar + ", t=" + no_Testing + ", at=" + appType)
         if (no_Legacy || !flagmap['build']) {
            doLegacyPublishing.off()
         }
         if (no_Sonar || !flagmap['build']) {
            doSonar.off()
         }
         if (no_Testing || !flagmap['build'] || 'POM' == appType) {
            doTesting.off()
            doPublishing.off()
         }
         if (doSonar.isOn() && !flagmap['build']) {
            doSonar.off()
            doQualityGate.off()
         }

         // noLegacyPublishing = (no_Legacy == "" || no_Legacy == 'true')
         // noTesting = (no_Testing == "" || no_Testing == 'true')
         // noSonar = (no_Sonar == "" || no_Sonar == 'true')
      }
      //println( printStateSonar("#4"))
      def noqg = isSet("${env.noQG}")
      //println( "${env.noQG}")
      if ( noqg ) {
  	    doQualityGate.off()
      }
      def noadg = isSet("${env.noADG}")
      if ( noadg ) {
    	  doAutoDeployGate.off()
	  }
      //println( printStateSonar("#5"))


   }

    public def static getNodeJsVersion(env) {
        if ( "${env.NODE_JS_VERSION}" == "null" || env.NODE_JS_VERSION == null) {
            return Definitions.NODE_JS_VERSION
        }
        return Definitions.NODE_JS_VERSION
    }

    public def static getApacheMavenVersion(env) {
        if ( "${env.APACHE_MAVEN_VERSION}" == "null" || env.APACHE_MAVEN_VERSION == null) {
            return Definitions.APACHE_MAVEN_VERSION
        }
        return "${env.APACHE_MAVEN_VERSION}"
    }


    public def static getDefaultNode(env) {
        if ( "${env.APP_TYPE}" == "null" && "${env.NODE_NAME}" == "null" ) {
            return "sefs"
        }
        if ("${env.NODE_NAME}" != "null" ) {
            return "${env.NODE_NAME}"
        }
        return "${env.APP_TYPE}"
    }


   public def static getTimeout(env) {
      def strnumber =  "${env.TIMEOUT}"
      return strnumber.isInteger() ? strnumber.toInteger() : 30
   }


    public def getConfigName(env, name) {
      def jobpath = "${env.JOB_NAME}".split("/")
      def configName = name ?: jobpath[jobpath.length - 2] + "@${env.BRANCH_NAME}"
      if (!name && ("${env.BRANCH_NAME}" == "null" || env.BRANCH_NAME == null)) {
        // is a trunk
        configName = jobpath[jobpath.length - 1]
      }
      return configName
    }


    public def getProject() {
      def _jobpath = this.jobpath.split('/')
      def project_name = _jobpath[_jobpath.length-2] + "-" +_jobpath[_jobpath.length-2]
      def project_key = _jobpath[_jobpath.length-2]
      project_key = normalize(artifactId)
      project_name = project_key + "-" + project_key + "-" + versionBase
      def projectid = 'SEFS_' + project_key + '_' + project_name + "-" + versionBase
      return [project_key,project_name, projectid]
    }

    public def String toString() {
        def w = new WorkflowReporter(this)
        return w.toString()
      }

    public static def getTestSuite(opco, level) {
      def xop = opco
      if (xop == 'FXE') {
        xop = ''
      }
      return "test/${level}/${xop}SmokeTest/Tests/Suites/${level}Suite.ste"
    }

    public static def getRegisteredLevels() {
    	return LEVEL_CHOICES
    }
    
    public static def isHeadless(key) {
      if ("ShipmentEFS" == key || key.startsWith("urh0061") || key.startsWith('ShipmentEFS' )) {
         return true
      }
      return false
    }

    public static def getProjectFromEnv(job_path) {
      def _jobpath = job_path.split('/')
      def project_name = _jobpath[_jobpath.length-2] + "-" +_jobpath[_jobpath.length-2]
      def project_key = _jobpath[_jobpath.length-2]
      project_key = normalize(project_key)
      project_name = project_key + "-" + project_key
      def projectid = 'SEFS_' + project_key + '_' + project_name
      return [project_key,project_name, projectid]
    }

    public static teamsHooks(jobpath, teamsHookJson) {
      def _jobpath = jobpath.split('/')
      def project_folder = _jobpath[0]
      def jsonSlurper = new JsonSlurper()
      def hooks = jsonSlurper.parseText teamsHookJson
      if ( hooks.containsKey(project_folder)) {
        return hooks.get(project_folder)
      }
      return Definitions.SEFS_ST_CICD_TEAMS_HOOK_URL
    }

    public static def createLddRecord(env,nodeName, configName, jobDuration,
      status, statusMessage) {
        return
          "{\"jobName\":\"${env.JOB_NAME}\"," +
          "\"nodeNm\":\"${nodeName}\",\"issuer\":null,\"lastBuild\":null," +
          "\"sourceLocation\":\"${configName}\",\"comments\":\"${statusMessage}\"," +
          "\"notification\":\"${env.BUILD_RECEPIENTS}\"," +
          "\"failedAt\":null,\"buildNumber\":\"${env.BUILD_NUMBER}\"," +
          "\"buildCause\":\"${env.BUILD_CAUSE}\"," +
          "\"duration\":\"${jobDuration}\"," +
          "\"releaseTag\":\"${env.RELEASE_TAG}\"," +
          "\"status\":\"${status}\"}";
    }

    def get_repo_url() {
      def repoInstance = 'SEFS-6270-snapshots'
      def repo = "nexus.prod.cloud.fedex.com:8443/nexus"
      def __version = version
      def repo_url = ""
      __version = version.replaceAll("(?:\\n|\\r)", "")
      if ("${performReleaseCut}" == 'true') {
        repoInstance = "SEFS-6270-releases"
        __version = "${nextVersion}"
        repo_url = "-DrepositoryId=releases -Durl=https://${repo}/content/repositories/${repoInstance} -Dversion=${__version}"
      } else {
        repo_url = "-DrepositoryId=snapshots -Durl=https://${repo}/content/repositories/${repoInstance} -Dversion=${__version}"
      }
      return repo_url
    }




    def our_deploy(script, extra_build_options) {
        script.sh 'echo "Running on $(hostname)"'
        our_execute(script, Definitions.EXCLUDE_ALL_BUT_DEPLOY,'deploy')
    }
    def our_build(script, extra_build_options, goal) {
        our_execute(script, extra_build_options, goal)
    }
    def our_build(script, extra_build_options, goal, app_type) {
        our_execute(script, extra_build_options, goal,'./pom.xml', './settings.xml',app_type)
    }
    def our_execute(script, extra_build_options, goal) {
      our_execute(script,extra_build_options, goal, './pom.xml', './settings.xml','')
    }
    def our_execute(script, extra_build_options, goal, pom_file) {
      our_execute(script,extra_build_options, goal, pom_file, './settings.xml','')
    }
    def our_execute(script, extra_build_options, goal, pom_file, settings_xml, app_type) {
        def javahome = script.tool name: 'JAVA_8', type: 'jdk'
        def m2home = script.tool name : Definitions.APACHE_MAVEN_VERSION
        def xrun = ''
        if (app_type == 'BW') {
          xrun = "/usr/bin/xvfb-run -a -f ${script.env.HOME}/.Xauthority"
        }
        script.withEnv(["PYTHONPATH=.:/var/tmp/tools/usr/lib64/python2.7/site-packages","JAVA_HOME=${javahome}",
          "M2_HOME=${m2home}", "PATH+MAVEN=${m2home}/bin:${javahome}/bin",
          "MAVEN_OPTS=-Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true \
-Xmx2048m -Xms2048m -Djava.security.egd=file:/dev/urandom -Dmaven.artifact.threads=10 ${extra_build_options}",
          "https_proxy=http://internet.proxy.fedex.com:3128",
          "http_proxy=http://internet.proxy.fedex.com:3128"
        ]) {
            // weird construction to avoid jvm crashes with surefire.
            def pomref = ''
            if (pom_file != './pom.xml') {
              pomref = pom_file
            }
            if (pomref != '' && !pomref.startsWith('-f')) {
              pomref = '-f ' + pomref
            }
            def cmd = "set -xv;pwd; hostname ;${xrun} ${m2home}/bin/mvn -B -U -V -e -Dpipeline.run=true -s ${settings_xml} ${pomref} ${extra_build_options} ${goal}"
            cmd.replaceAll("(?:\n|\r)", "")
            script.echo("command: ${cmd}")
            def buildSuccess = script.sh returnStatus: true, script: cmd
            if (buildSuccess != 0) {
                throw new Exception("STEP: Error while executing ${goal}; exit_code=${buildSuccess}")
            }
        }
    }

    static Map getFlags() {
      return ["build" : false, "deploy" : false, "smoketest" : false, "ittest" : false, 'e2e' : false, 'release' : false]
   }

    static boolean fileExists(path) {
      return new File(".$path").exists()
    }
    static void test(Definitions d) {
      def flagmap = ["build" : false, "deploy" : false, "smoketest" : false, "ittest" : false, 'e2e' : false, 'release' : false]
      test_suites(d,flagmap)
    }
    static void apply_map(Definitions d, flagmap) {
      for (key in flagmap.keySet()) {
        def exists = fileExists(".${key}")
        flagmap[key] = exists
        if (exists) {
          stash includes: ".${key}", name: "${key}.flag"
        }
      }
      d.applyWorkflow(flagmap)
    }


}
