package com.fedex.ci

class Step implements Serializable {

   def state = false

   def Step(devault) {
      state = devault
   }
   def Step() {
      state = false
   }
   def on() {
      state = true
   }
   def off() {
      state = false
   }
   def enable() {
      on()
   }
   def disable() {
      off()
   }
   def state(value) {
      state = value
   }
   def toggle() {
      state = !state
   }
   def String toString() {
      return "${state}"
   }
   def isOn() {
	   return state == true
   }
   def isOff() {
	   return state == false
   }
}
