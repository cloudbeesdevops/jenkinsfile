package com.fedex.ci

import java.io.Serializable

class Log implements Serializable {
  def static int loglevel = 0

  public Log() {
  }
  public static def warn(script,message) {
    if (loglevel <= 40) {
      script.echo "\u2757 [WARN]: ${message}"
    }
  }
  public static def debug(script,message) {
    if (loglevel <= 10) {
      script.echo "\u270d [DEBUG]: ${message}"
    }
  }
  public static def info(script,message) {
    if (loglevel <= 20) {
      script.echo "\u261d [INFO]: ${message}"
    }
  }
  public static def trace(script,message) {
    if (loglevel <= 0) {
      script.echo "\u23F9 [TRACE]: ${message}"
    }
  }
  public static def error(script, err) {
    script.echo "\u26d4 [ERROR]: ${err}"
    err.printStackTrace(System.out)
  }
  public static def warnerror(script, err) {
    script.echo "\u26d4 [WARN]: ${err}"
  }
  public static def error(script,message,err) {
    script.echo "\u26d4 [ERROR] : ${message}"
    err.printStackTrace(System.out)
  }
  public static def symbol(ucode, times, msg) {
    def message = ""
    def ucde = ""
    (1..times).each{ it ->
      ucde = ucde + "${ucode} " 
    }
    return "${ucde} ${msg} ${ucde}"
  }
  public static def echoBuildStatus(script, currentBuild, message) {
      if (currentBuild.result == 'FAILURE' || currentBuild.result == 'UNSTABLE') {
        compileError(script, "${message} status is ${currentBuild.result}")
      } else if (currentBuild.result == 'ABORTED') {
        compileWaring(script, "${message} status is ${currentBuild.result}")
      } else {
        compileInfo(script, "${message} status is SUCCESS")
      }

  }
  public static def header(script, message) {
    script.echo( symbol('\u2705', 10, message))
  }
  public static def compileWarning(script, message) {
    script.echo( symbol('\u2622', 10, message) ) }
  public static def compileInfo(script, message) {
    script.echo( symbol('\u2600\ufe0f', 10, message) ) }
  public static def compileError(script, message) {
    script.echo( symbol('\u2623', 10, message) ) }
  public static def compileErrorAndExit(script, message) {
    script.echo( symbol('\u2623', 10, message) )
    script.error(message)
  }

}
