out.println('Jenkins DSL wiki: https://github.com/jenkinsci/job-dsl-plugin/wiki')

def jobFolderName = 'DSL-Folder'
def jobFolderValue = "${jobFolderName}/"
def descriptionDslWarning = '<font color="blue">*** DSL MAINTAINED ***</font> -- \nDO NOT MANUALLY MODIFY. \nManual config changes will not survive. \nEdit, commit and push the dsl script instead.'
UUID uuid = UUID.randomUUID()

folder(jobFolderName) {
description('descriptionDslWarning')
configure {
      it / "healthMetrics"  << {
        "com.cloudbees.hudson.plugins.folder.health.WorstChildHealthMetric " {
      nonRecursive("false")
      }
        "com.cloudbees.hudson.plugins.folder.health.JobStatusHealthMetric" {
        success("true")
          failure("true")
          unstable("true")
          unbuilt("true")
          countVirginJobs("false")
        }
      }
	  it / icon (class : "com.cloudbees.hudson.plugins.folder.icons.UrlFolderIcon") {
	  url ("http://sefsmvn.ute.fedex.com/images/waypoint.gif")
	  }
	  it / "properties"  << {
	  "com.cloudbees.hudson.plugins.folder.properties.EnvVarsFolderProperty" {
		properties("NODE_NAME=ShipmentEFS-Java8 \n APP_TYPE=PCF-JAVA")
	  }
        "org.jenkinsci.plugins.workflow.libs.FolderLibraries" {
		"libraries" {
		"org.jenkinsci.plugins.workflow.libs.LibraryConfiguration " { 
	  name("libz")
	  retriever (class: "org.jenkinsci.plugins.workflow.libs.SCMSourceRetriever") <<{
	  scm  (class : "jenkins.plugins.git.GitSCMSource") {
	  id(uuid)
	  remote("git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git")
	  credentialsId("gitlab-user-for-master")
	  traits {
      'jenkins.plugins.git.traits.BranchDiscoveryTrait'()
       }
	  }
	  }
	  defaultVersion("development")
	  implicit("true")
	  allowVersionOverride("true")
	  includeInChangesets("true")	  
	  }
	  }
	  }
	  }
	  }
}

@groovy.transform.ToString(includeNames = true, includeFields=true)
class MultibranchPipelineJob {
    String sourcescmUrl
    String sourcecredentialsId
    String jenkinsscmUrl
    String jenkinscredentialsId
    String MarkerFile
    String scriptPath
    String jenkinsbranches
    String EnvCommands
}
def multibranchPipelineJobs = [
    MultibranchPipelineJob.newInstance([sourcescmUrl: 'git@gitlab.prod.fedex.com:APP3534587/waypoint-definition-service.git', sourcecredentialsId : 'gitlab-user-for-master',jenkinsscmUrl: 'git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git',jenkinscredentialsId: 'gitlab-user-for-master',MarkerFile: '.build',jenkinsbranches: '*/master',scriptPath:'./JenkinsfileCICD',EnvCommands:'NODE_NAME=ShipmentEFS-Java8'])
    ]
    
    out.println('----- pipeline Jobs entries begin -----')
    multibranchPipelineJobs.each {
        out.println("  -- ${it}")
    }
    out.println('----- pipeline Jobsentries end -----')
    out.println()
    
    multibranchPipelineJobs.each {
        def aJob = it
        def repoPath = aJob.sourcescmUrl
        def repoName = repoPath.substring(repoPath.lastIndexOf('/')+1)
        repoName = repoName.substring(0, repoName.indexOf('.git'))
        def Jenkinsbranches = aJob.jenkinsbranches
        def  ScriptPath = aJob.scriptPath
        def SourcecredentialsId  = aJob.sourcecredentialsId
        def JenkinsscmUrl = aJob.jenkinsscmUrl
        def JenkinscredentialsId = aJob.jenkinscredentialsId
        def markerFile = aJob.MarkerFile
        def envCommands = aJob.EnvCommands
        def jobName = jobFolderValue + repoName +'Branch'
        multibranchPipelineJob(jobName){
        
    description "Builds "
          configure {
      it / "properties" / 'com.cloudbees.hudson.plugins.folder.properties.EnvVarsFolderProperty'{
      properties(envCommands)
      }
        it / "healthMetrics"  << {
        "com.cloudbees.hudson.plugins.folder.health.WorstChildHealthMetric " {
      nonRecursive("false")
      }
        "com.cloudbees.hudson.plugins.folder.health.JobStatusHealthMetric" {
        success("true")
          failure("true")
          unstable("true")
          unbuilt("true")
          countVirginJobs("false")
        }
      }
            it / sources / 'data' / 'jenkins.branch.BranchSource' << {
            source(class: 'jenkins.plugins.git.GitSCMSource') {
                id(uuid)
                remote(repoPath)
                credentialsId(SourcecredentialsId)
                includes('*')
                excludes('')
                ignoreOnPushNotifications('false')
                traits {
                    'jenkins.plugins.git.traits.BranchDiscoveryTrait'()
                }
            }
            // default strategy when sourcing from a branch
            strategy {
        defaultBranchPropertyStrategy {
          props {
            noTriggerBranchProperty()
          }
        }
            }
        }
            // customise the branch project factory
        it /factory( class: "com.cloudbees.workflow.multibranch.CustomBranchProjectFactory") << {
          owner(class: "org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject") {
          }
            marker("pipelines/customPipeline.groovy")
            definition(class: "org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition") {
         scm(class: "hudson.plugins.git.GitSCM" ){
          configVersion("2")                                        
            userRemoteConfigs{
      "hudson.plugins.git.UserRemoteConfig" {
        url(JenkinsscmUrl)
        credentialsId(JenkinscredentialsId)
      }
      }                       
      branches{
        "hudson.plugins.git.BranchSpec"{
            name(Jenkinsbranches)
            }
    }                      
      
doGenerateSubmoduleConfigurations("false")
               submoduleCfg (class : "list")
               extensions()
         
         }
         scriptPath(ScriptPath)
         lightweight("true")
        
              }
        
              }
              }
        }
 
        }