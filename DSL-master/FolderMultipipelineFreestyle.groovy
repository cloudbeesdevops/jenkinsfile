def descriptionDslWarning = '<font color="blue">*** DSL MAINTAINED ***</font> -- \nDO NOT MANUALLY MODIFY. \nManual config changes will not survive. \nEdit, commit and push the dsl script instead.'
@groovy.transform.ToString(includeNames = true, includeFields=true)
class FreestyleJob {
	String JobName
	String NodeLabel
	String CronTime
	String ExecuteShell1
	String ExecuteShell2
	String SVNurl
	String SVNCred
}
def freestyleJobs = [
    FreestyleJob.newInstance([JobName:'freestyle',NodeLabel:'ShipmentEFS2',CronTime:'H H/6 * * *',ExecuteShell1:'echo \"Success1\"',ExecuteShell2:'echo \"Success2\"',SVNurl:'https://conexus.prod.fedex.com:9443/subversion/sefs_common/CICDReportsParser/branches/1.0.0',SVNCred:'9e863997-c0d9-4acb-8d54-99bd6c0b6ae1'])
    ]
    freestyleJobs.each{
	def aJob = it
	def jobName = it.JobName
	def nodeLabel = it.NodeLabel
	def cronTime = it.CronTime
	def executeShell1 = it.ExecuteShell1
	def executeShell2 = it.ExecuteShell2
	def svnurl = it.SVNurl
	def svnCred = it.SVNCred

			job(jobName) {
		description()
		logRotator(-1,10)
		label(nodeLabel)
		triggers{
			cron(cronTime)
		}
		steps {
	        shell(executeShell1)
	        shell(executeShell2)
	 }
		scm{
			svn{
			location(svnurl)
			{
				credentials(svnCred)
			}
		}
		}
		publishers{
			wsCleanup{
			}
		}
	}
}
class Folder {
    String JobFolderName
    String Iconurl
    String Folderenvprop
    String Namelibz
    String RemoteURL
    String RemotecredID
    String RemoteDefaultVersion
}
def folders = [
    Folder.newInstance([JobFolderName:'DSL-AU',Iconurl: 'http://sefsmvn.ute.fedex.com/images/waypoint.gif',Namelibz:'',RemoteURL:'',RemotecredID:'',RemoteDefaultVersion:'',Folderenvprop:'NODE_NAME=ShipmentEFS-Java8'])
    ]
    
    out.println('----- pipeline Jobs entries begin -----')
    folders.each {
        out.println("  -- ${it}")
    }
    out.println('----- pipeline Jobsentries end -----')
    out.println()
    
    folders.each {
        def aJob = it
        def JobFolderName = aJob.JobFolderName
        def FinalJobFolder = "${JobFolderName}/"
        def iconurl = aJob.Iconurl
        def  namelibz = aJob.Namelibz
        def remoteURL  = aJob.RemoteURL
        def remotecredID = aJob.RemotecredID
        def remoteDefaultVersion = aJob.RemoteDefaultVersion
        def folderenvprop = aJob.Folderenvprop
        UUID uuid = UUID.randomUUID()
        folder(JobFolderName) {
        description("${descriptionDslWarning}")
        configure {
      it / "healthMetrics"  << {
        "com.cloudbees.hudson.plugins.folder.health.WorstChildHealthMetric " {
      nonRecursive("false")
      }
        "com.cloudbees.hudson.plugins.folder.health.JobStatusHealthMetric" {
        success("true")
          failure("true")
          unstable("true")
          unbuilt("true")
          countVirginJobs("false")
        }
      }
      it / icon (class : "com.cloudbees.hudson.plugins.folder.icons.UrlFolderIcon") {
      url (iconurl)
      }
      it / "properties"  << {
      "com.cloudbees.hudson.plugins.folder.properties.EnvVarsFolderProperty" {
        properties(folderenvprop)
      }
        "org.jenkinsci.plugins.workflow.libs.FolderLibraries" {
        "libraries" {
        "org.jenkinsci.plugins.workflow.libs.LibraryConfiguration " { 
      name(namelibz)
      retriever (class: "org.jenkinsci.plugins.workflow.libs.SCMSourceRetriever") <<{
      scm  (class : "jenkins.plugins.git.GitSCMSource") {
      id(uuid)
      remote(remoteURL)
      credentialsId(remotecredID)
      traits {
      'jenkins.plugins.git.traits.BranchDiscoveryTrait'()
       }
      }
      }
      defaultVersion(remoteDefaultVersion)
      implicit("true")
      allowVersionOverride("true")
      includeInChangesets("true")      
      }
      }
      }
      }
      }

        }
        }
    @groovy.transform.ToString(includeNames = true, includeFields=true)
    class MultibranchPipelineJob {
        String sourcescmUrl
        String sourcecredentialsId
        String jenkinsscmUrl
        String jenkinscredentialsId
        String MarkerFile
        String scriptPath
        String jenkinsbranches
        String EnvCommands
        String jobFolderValue
    }
    def multibranchPipelineJobs = [
        MultibranchPipelineJob.newInstance([jobFolderValue:'DSL-AU/',sourcescmUrl: 'git@gitlab.prod.fedex.com:APP3534587/waypoint-definition-service.git', sourcecredentialsId : 'gitlab-user-for-master',jenkinsscmUrl: 'git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git',jenkinscredentialsId: 'gitlab-user-for-master',MarkerFile: '.build',jenkinsbranches: '*/master',scriptPath:'./JenkinsfileCICD',EnvCommands:'NODE_NAME=ShipmentEFS-Java8'])
        ]
        
        out.println('----- pipeline Jobs entries begin -----')
        multibranchPipelineJobs.each {
            out.println("  -- ${it}")
        }
        out.println('----- pipeline Jobsentries end -----')
        out.println()
        
        multibranchPipelineJobs.each {
            def aJob = it
            def JobFolderValue =  aJob.jobFolderValue
            def repoPath = aJob.sourcescmUrl
            def repoName = repoPath.substring(repoPath.lastIndexOf('/')+1)
            repoName = repoName.substring(0, repoName.indexOf('.git'))
            def Jenkinsbranches = aJob.jenkinsbranches
            def  ScriptPath = aJob.scriptPath
            def SourcecredentialsId  = aJob.sourcecredentialsId
            def JenkinsscmUrl = aJob.jenkinsscmUrl
            def JenkinscredentialsId = aJob.jenkinscredentialsId
            def markerFile = aJob.MarkerFile
            def envCommands = aJob.EnvCommands
            UUID uuid = UUID.randomUUID()
            def jobName = JobFolderValue + repoName +'Branch'
            multibranchPipelineJob(jobName){
            
        description "Builds "
              configure {
          it / "properties" / 'com.cloudbees.hudson.plugins.folder.properties.EnvVarsFolderProperty'{
          properties(envCommands)
          }
            it / "healthMetrics"  << {
            "com.cloudbees.hudson.plugins.folder.health.WorstChildHealthMetric " {
          nonRecursive("false")
          }
            "com.cloudbees.hudson.plugins.folder.health.JobStatusHealthMetric" {
            success("true")
              failure("true")
              unstable("true")
              unbuilt("true")
              countVirginJobs("false")
            }
          }
                it / sources / 'data' / 'jenkins.branch.BranchSource' << {
                source(class: 'jenkins.plugins.git.GitSCMSource') {
                    id(uuid)
                    remote(repoPath)
                    credentialsId(SourcecredentialsId)
                    includes('*')
                    excludes('')
                    ignoreOnPushNotifications('false')
                    traits {
                        'jenkins.plugins.git.traits.BranchDiscoveryTrait'()
                    }
                }
                // default strategy when sourcing from a branch
                strategy {
            defaultBranchPropertyStrategy {
              props {
                noTriggerBranchProperty()
              }
            }
                }
            }
                // customise the branch project factory
            it /factory( class: "com.cloudbees.workflow.multibranch.CustomBranchProjectFactory") << {
              owner(class: "org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject") {
              }
                marker("pipelines/customPipeline.groovy")
                definition(class: "org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition") {
             scm(class: "hudson.plugins.git.GitSCM" ){
              configVersion("2")                                        
                userRemoteConfigs{
          "hudson.plugins.git.UserRemoteConfig" {
            url(JenkinsscmUrl)
            credentialsId(JenkinscredentialsId)
          }
          }                       
          branches{
            "hudson.plugins.git.BranchSpec"{
                name(Jenkinsbranches)
                }
        }                      
          
    doGenerateSubmoduleConfigurations("false")
                   submoduleCfg (class : "list")
                   extensions()
             
             }
             scriptPath(ScriptPath)
             lightweight("true")
            
                  }
            
                  }
                  }
            }
     
            }
