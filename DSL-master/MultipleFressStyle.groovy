def descriptionDslWarning = '<font color="blue">*** DSL MAINTAINED ***</font> -- \nDO NOT MANUALLY MODIFY. \nManual config changes will not survive. \nEdit, commit and push the dsl script instead.'
@groovy.transform.ToString(includeNames = true, includeFields=true)
class FreestyleJob {
	String JobName 
}
def freestyleJobs = [
    FreestyleJob.newInstance([JobName:'freestyle'])
    ]
    freestyleJobs.each{
	def aJob = it
	def jobName = it.JobName
			job(jobName) {
		description()
		logRotator(-1,10)
		label('ShipmentEFS2')
		triggers{
			cron('H H/6 * * *')
		}
		steps {
	        shell("echo \"Success1\"")
	        shell("echo \"Success1\"")
	 }
		scm{
			svn{
			location('https://conexus.prod.fedex.com:9443/subversion/sefs_common/CICDReportsParser/branches/1.0.0')
			{
				credentials("9e863997-c0d9-4acb-8d54-99bd6c0b6ae1")
			}
		}
		}
		publishers{
			wsCleanup{
			}
		}
	}
}
