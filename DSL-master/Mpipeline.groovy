//A new UUID must be generated for the first run and re-used for your Job DSL, the plugin updates jobs based on ID
UUID uuid = UUID.randomUUID()
println "RandomUUID :" + uuid
//UUID uuid = UUID.fromString("dd847135-8391-4f66-a54c-7f8781dc3119") // generate one @ https://www.uuidgenerator.net

multibranchPipelineJob("my_awesome_job") {
    displayName "my awesome job"
    description "multi-branch pipeline job thingy"
  
    configure {
      it / "properties" / 'com.cloudbees.hudson.plugins.folder.properties.EnvVarsFolderProperty'{
      properties("CF_CLI_PATH=/opt/fedex/tibco/cf-cli/bin")
      }
      it / "healthMetrics"  << {
        "com.cloudbees.hudson.plugins.folder.health.WorstChildHealthMetric " {
      nonRecursive("false")
      }
        "com.cloudbees.hudson.plugins.folder.health.JobStatusHealthMetric" {
        success("true")
          failure("true")
          unstable("true")
          unbuilt("true")
          countVirginJobs("false")
        }
      }
      
        it / sources / 'data' / 'jenkins.branch.BranchSource' << {
            source(class: 'jenkins.plugins.git.GitSCMSource') {
                id(uuid)
                remote("git@gitlab.prod.fedex.com:APP3534587/waypoint-definition-service.git")
                credentialsId("gitlab-user-for-master")
                includes('*')
                excludes('')
                ignoreOnPushNotifications('false')
                traits {
                    'jenkins.plugins.git.traits.BranchDiscoveryTrait'()
                }
            }
            // default strategy when sourcing from a branch
            strategy {
        defaultBranchPropertyStrategy {
          props {
            noTriggerBranchProperty()
          }
        }
            }
        }
        // customise the branch project factory
        it / factory(class: "com.cloudbees.workflow.multibranch.CustomBranchProjectFactory") << {
             owner(class: "org.jenkinsci.plugins.workflow.multibranch.WorkflowMultiBranchProject") << {
                }
            marker("pipelines/customPipeline.groovy")
            definition(class: "org.jenkinsci.plugins.workflow.cps.CpsScmFlowDefinition") {
         
         scm(class: "hudson.plugins.git.GitSCM" ){
          configVersion("2")                                        
           userRemoteConfigs{
      "hudson.plugins.git.UserRemoteConfig" {
        url("git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git")
        credentialsId('gitlab-user-for-master')
      }
      }                       
      branches{
        "hudson.plugins.git.BranchSpec"{
            name(('*/master'))
            }
    }
doGenerateSubmoduleConfigurations("false")
               submoduleCfg (class : "list")
               extensions()
         
         }
         scriptPath("./JenkinsfileCICD")
         lightweight("true")
        
              }
              }
         
         }
         
        }