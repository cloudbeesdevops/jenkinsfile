def descriptionDslWarning = '<font color="blue">*** DSL MAINTAINED ***</font> -- \nDO NOT MANUALLY MODIFY. \nManual config changes will not survive. \nEdit, commit and push the dsl script instead.'
@groovy.transform.ToString(includeNames = true, includeFields=true)
class Folder {
    String JobFolderName
    String Iconurl
    String Folderenvprop
    String Namelibz
    String RemoteURL
    String RemotecredID
    String RemoteDefaultVersion
}
def folders = [
    Folder.newInstance([JobFolderName:'DSL-AU',Iconurl: 'http://sefsmvn.ute.fedex.com/images/waypoint.gif',Namelibz:'',RemoteURL:'',RemotecredID:'',RemoteDefaultVersion:'',Folderenvprop:'NODE_NAME=ShipmentEFS-Java8'])
    ]
    
    out.println('----- pipeline Jobs entries begin -----')
    folders.each {
        out.println("  -- ${it}")
    }
    out.println('----- pipeline Jobsentries end -----')
    out.println()
    
    folders.each {
        def aJob = it
        def JobFolderName = aJob.JobFolderName
        def FinalJobFolder = "${JobFolderName}/"
        def iconurl = aJob.Iconurl
        def  namelibz = aJob.Namelibz
        def remoteURL  = aJob.RemoteURL
        def remotecredID = aJob.RemotecredID
        def remoteDefaultVersion = aJob.RemoteDefaultVersion
        def folderenvprop = aJob.Folderenvprop
        UUID uuid = UUID.randomUUID()
        folder(JobFolderName) {
        description("${descriptionDslWarning}")
        configure {
      it / "healthMetrics"  << {
        "com.cloudbees.hudson.plugins.folder.health.WorstChildHealthMetric " {
      nonRecursive("false")
      }
        "com.cloudbees.hudson.plugins.folder.health.JobStatusHealthMetric" {
        success("true")
          failure("true")
          unstable("true")
          unbuilt("true")
          countVirginJobs("false")
        }
      }
      it / icon (class : "com.cloudbees.hudson.plugins.folder.icons.UrlFolderIcon") {
      url (iconurl)
      }
      it / "properties"  << {
      "com.cloudbees.hudson.plugins.folder.properties.EnvVarsFolderProperty" {
        properties(folderenvprop)
      }
        "org.jenkinsci.plugins.workflow.libs.FolderLibraries" {
        "libraries" {
        "org.jenkinsci.plugins.workflow.libs.LibraryConfiguration " { 
      name(namelibz)
      retriever (class: "org.jenkinsci.plugins.workflow.libs.SCMSourceRetriever") <<{
      scm  (class : "jenkins.plugins.git.GitSCMSource") {
      id(uuid)
      remote(remoteURL)
      credentialsId(remotecredID)
      traits {
      'jenkins.plugins.git.traits.BranchDiscoveryTrait'()
       }
      }
      }
      defaultVersion(remoteDefaultVersion)
      implicit("true")
      allowVersionOverride("true")
      includeInChangesets("true")      
      }
      }
      }
      }
      }

        }
        }
        
        
        