def descriptionDslWarning = '<font color="blue">*** DSL MAINTAINED ***</font> -- \nDO NOT MANUALLY MODIFY. \nManual config changes will not survive. \nEdit, commit and push the dsl script instead.'
@groovy.transform.ToString(includeNames = true, includeFields=true)
//Arguments
class Folder {
    String JobFolderName
    String Iconurl
}
def folders = [
    Folder.newInstance([JobFolderName:'DSL-AU',Iconurl:'http://sefsmvn.ute.fedex.com/images/waypoint.gif'])
    ]
    out.println('----- folder Jobs entries begin -----')
    folders.each {
        out.println("  -- ${it}")
    }
    out.println('----- folder Jobsentries end -----')
    out.println()
    
    folders.each {
        def aJob = it
        def jobFolderName = aJob.JobFolderName
        def iconurl = aJob.Iconurl
        folder(jobFolderName) {
        description("${descriptionDslWarning}")
        configure {
        	 it / icon (class : "com.cloudbees.hudson.plugins.folder.icons.UrlFolderIcon") {
        	      url (iconurl)
        	      }
        }
        }
    }