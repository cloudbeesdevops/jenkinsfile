UUID uuid = UUID.randomUUID()
println "RandomUUID :" + uuid
folder('DSL-Folder'){
description('DSL created folder')
configure {
      it / "healthMetrics"  << {
        "com.cloudbees.hudson.plugins.folder.health.WorstChildHealthMetric " {
      nonRecursive("false")
      }
        "com.cloudbees.hudson.plugins.folder.health.JobStatusHealthMetric" {
        success("true")
          failure("true")
          unstable("true")
          unbuilt("true")
          countVirginJobs("false")
        }
      }
	  it / icon (class : "com.cloudbees.hudson.plugins.folder.icons.UrlFolderIcon") {
	  url ("http://sefsmvn.ute.fedex.com/images/waypoint.gif")
	  }
	  it / "properties"  << {
	  "com.cloudbees.hudson.plugins.folder.properties.EnvVarsFolderProperty" {
		properties("NODE_NAME=ShipmentEFS-Java8 \n APP_TYPE=PCF-JAVA")
	  }
        "org.jenkinsci.plugins.workflow.libs.FolderLibraries" {
		"libraries" {
		"org.jenkinsci.plugins.workflow.libs.LibraryConfiguration " { 
	  name("libz")
	  retriever (class: "org.jenkinsci.plugins.workflow.libs.SCMSourceRetriever") <<{
	  scm  (class : "jenkins.plugins.git.GitSCMSource") {
	  id(uuid)
	  remote("git@gitlab.prod.fedex.com:APP6270/sefs-xopco-common-jenkinssharedlibrary.git")
	  credentialsId("gitlab-user-for-master")
	  traits {
      'jenkins.plugins.git.traits.BranchDiscoveryTrait'()
       }
	  }
	  }
	  defaultVersion("development")
	  implicit("true")
	  allowVersionOverride("true")
	  includeInChangesets("true")	  
	  }
	  }
	  }
	  }
	  }
}