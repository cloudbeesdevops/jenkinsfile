user=%s
host_name=%s
if [[ $host_name == *"test.cloud"* ]]
then
	SYS_INFO_DATA=`timeout 5s ssh -o StrictHostKeyChecking=no -i ~/.ssh/test_deploy_user_dsa $user@$host_name '/usr/bin/sadf -dht -- -u -q -r -S -n DEV'`
else
	SYS_INFO_DATA=`timeout 5s ssh -o StrictHostKeyChecking=no -i ~/.ssh/test_deploy_user_dsa $user@$host_name '/usr/bin/sadf -dht -- -u -q -r -S -n DEV'`
fi

echo "$SYS_INFO_DATA"

