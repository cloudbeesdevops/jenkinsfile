#! /bin/sh
export PATH=$PATH:/opt/fedex/tibco/cf-cli/bin
export USERNAME=%s
export PASSWORD=%s
export CF_HOME=%s
cf api %s
cf auth $USERNAME $PASSWORD
cf target -o %s -s %s
