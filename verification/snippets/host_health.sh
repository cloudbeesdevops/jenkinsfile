user=%s
host_name=%s
opcoId=%s
level=%s
healthreport="%s"
opcoType=""

if [[ $host_name == *"test.cloud"* ]]
then
	EAI_META_DATA=`timeout 5s ssh -oConnectTimeout=20 -oStrictHostKeyChecking=no \
-oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null -i ~/.ssh/test_deploy_user_dsa $user@$host_name 'echo "HOST=$(hostname) $(/opt/fedex/cloud/bin/cloudenv)"'`
else
	EAI_META_DATA=`timeout 5s ssh -oConnectTimeout=20 -oStrictHostKeyChecking=no \
-oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null -i ~/.ssh/test_deploy_user_dsa $user@$host_name 'echo "HOST=$(hostname)"'`
fi
publicIpAddress=`dig +short $host_name|awk '$1~/^[0-9]/'`
privateIpAddress=`nslookup $host_name|awk '/^Address: / { print $2 }'`
eaiNumber="";appUsers="";hostCname="";LEVEL_TAG=$level;activeFlag="0";
ramSize=`timeout 5s ssh -oConnectTimeout=20 -oStrictHostKeyChecking=no \
-oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null -i ~/.ssh/test_deploy_user_dsa $user@$host_name 'echo "$(cat /proc/cpuinfo | grep ^processor | wc -l)"'`
if [[ $host_name == *"test.cloud"* ]]
then
	if [[  -z  ${EAI_META_DATA} ]]
	then
		lastSuccessful="";
		lastFailed=`date +%%Y%%m%%d%%H%%M%%S`
	else
		eaiNumber=`echo "$EAI_META_DATA" |grep ^EAI|cut -f2 -d "="|sed 's/[\"]//g'`
		host_name=`echo "$EAI_META_DATA" |grep ^HOST|sed 's/.*\=\(.*\)\ .*/\1/'|cut -f 2 -d "="`
		appUsers=`echo "$EAI_META_DATA" |grep ^APP_USERS|cut -f2 -d "="|sed 's/[\"]//g'`
		hostCname=`echo "$EAI_META_DATA"|grep ^ALIASES|cut -f 2 -d "="|cut -f 1 -d " "|sed 's/[\"]//g'`
		LEVEL_TAG=`echo "$EAI_META_DATA" |grep ^LEVEL_TAG|cut -f2 -d "="|sed 's/[\"]//g'|perl -pe 's/.*\s(L[\d+]).*/$1/'`
		activeFlag="1"
		lastSuccessful=`date +%%Y%%m%%d%%H%%M%%S`;
		lastFailed="";
	fi
else
	if [[  -z  ${EAI_META_DATA} ]] 
	then
		lastSuccessful="";
		lastFailed=`date +%%Y%%m%%d%%H%%M%%S`
	else
		lastSuccessful=`date +%%Y%%m%%d%%H%%M%%S`;
		lastFailed="";
		host_name=`echo "$EAI_META_DATA" |grep ^HOST|sed 's/.*\=\(.*\)\ .*/\1/'|cut -f 2 -d "="`
	fi
fi
echo "$host_name:$hostCname:0:$activeFlag:$lastSuccessful:$lastFailed:"":$publicIpAddress:$privateIpAddress:$level:$LEVEL_TAG:$eaiNumber:$ramSize:"$appUsers":$opcoId:$opcoType" | tee -a $healthreport
