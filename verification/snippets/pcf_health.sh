#! /bin/sh
export PATH=$PATH:/opt/fedex/tibco/cf-cli/bin
export USERNAME=%s
export PASSWORD=%s
cf api %s
cf auth $USERNAME $PASSWORD
cf target -o %s -s %s
cf apps
