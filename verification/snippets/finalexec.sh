comp_type=%s
comp_name=%s
user=%s
host_name=%s
path=%s
grepword=%s
Adminhost=%s
AdminUSer=%s
probereport="%s"
level="%s"
artifact_id=%s
#echo "checkout  process id"
PID=`timeout 5s ssh -oConnectTimeout=20 -oStrictHostKeyChecking=no \
-oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null -i ~/.ssh/test_deploy_user_dsa $user@$host_name "/bin/ps  -o user:20 -o pid= -o %%cpu= -o %%mem= -o vsz= -o rss= -o tty= -o stat= -o stime= -o time= -o cmd= -U $user  | /bin/egrep 'fx.[-_]|FX.[-_]|java'|grep ${grepword}|grep -v grep"`
retCodePID=$?
VERSION=`timeout 5s ssh -oConnectTimeout=20 -oStrictHostKeyChecking=no \
-oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null -i ~/.ssh/test_deploy_user_dsa $AdminUSer@$Adminhost readlink "${path}/current"`
HOST_NAME=`timeout 5s ssh -oConnectTimeout=20 -oStrictHostKeyChecking=no \
-oBatchMode=yes -oLogLevel=error -oUserKnownHostsFile=/dev/null -i ~/.ssh/test_deploy_user_dsa $user@$host_name 'echo "HOST=$(hostname)"'`
export status="RUNNING"
export occurances="200"
if [[  -z  ${HOST_NAME} ]] ;
then
host_name=$host_name
else
host_name=`echo "$HOST_NAME" |grep ^HOST|sed 's/.*\=\(.*\)\ .*/\1/'|cut -f 2 -d "="`
fi
if [[  -z  ${PID} ]] ;
then
    export occurances="500"
	export status=""
	export pid=""
	export process_time=""
	export date=""
	export cmd=""
	export cpu=""
	export mem=""
else
cmd=`echo $PID |awk '{print $11}'`
pid=`echo $PID |awk '{print $2}'`
date=`echo $PID|awk '{print $9}'`
process_time=`echo $PID|awk '{print $10}'`
cpu=`echo $PID|awk '{print $3}'`
mem=`echo $PID|awk '{print $4}'`
fi
if [[  -z  ${VERSION} ]] ;
then
	export VERSION=""
fi
echo "$comp_type,$comp_name,$occurances,$cpu,$date,$host_name,$mem,0,$pid,$process_time,$level,$artifact_id,$comp_name,$VERSION" | tee -a $probereport
#sleep 0.1
