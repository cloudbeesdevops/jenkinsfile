import re
import fnmatch
import glob
import sys
import shutil
import json
import os
import subprocess
import argparse
import importlib
import logging
from  f_env import jsonextract,type_search,csvtoJSON,pcfhealthgenerator
from f_env.descriptors import digest, readDescriptors,create_manifest_resolver

logger = logging.getLogger('main')
LOG_FORMAT = "%(asctime)-15s [%(levelname)-5s](%(module)s:%(lineno)d) - %(funcName)s: %(message)s"


def cleantempfile(outdir):
    if (os.path.exists(outdir)):
        shutil.rmtree(outdir)
        print("path existed, deleting ... Deleted")
        os.mkdir("tmp",0775)
    else:
        os.mkdir("tmp",0775)
app_type = []
def makeFunc(x,argu,opco_type):
    if x in argu:
        print x
        if (x == "BW"):
            #y = opco_type +"-" + x.lower()+"2-*.json"
            y = "*-" + x.lower()+"2-*.json"
        else:
            y = "*/"+opco_type +"-" + x.lower()+"-*.json"
        app_type.append(lambda: y)
        #y = opco_type +"-" + x.lower() +"-*.json"
def main(argv):
    print argv
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--directory',  action="store", nargs="+",
                        help='Deployment descriptor file in JSON format (required)')
    parser.add_argument('-t', '--app_type',  action="store",
                        nargs="?", default=None, help='app_type')
    parser.add_argument('-l', '--level',  action="store",
                        nargs="?", default=None, help='filter levels')
    parser.add_argument('-r', '--report',  action="store", nargs="?",
                        help='host_health or app_health needs to pass as argument')
    parser.add_argument('-i', '--ignore-syntax', action="store_true",
                        help='ignore syntax errors in json files')
    parser.add_argument('-w', '--write-to-disk', action="store_true",
                        help='ignore syntax errors in json files')
    parser.add_argument('-v', action='count',
        help="verbosity level. (v=INFO, vv=DEBUG,vvv=TRACE, default=ERROR) see also --log-level for explicit level set.")
    parser.add_argument('-p', '--post', action='store_true', help="posting http to LDD.")

    if '-h' in argv or '--help' in argv:
        parser.print_help()
        sys.exit(0)
    args = parser.parse_args(argv)
    argsopt = args.directory
    if not args.v is None:
        if args.v == 3:
            logging.basicConfig(format=LOG_FORMAT,level=logging.DEBUG)
        elif args.v == 2:
            logging.basicConfig(format=LOG_FORMAT,level=logging.INFO)
        elif args.v == 1:
            logging.basicConfig(format=LOG_FORMAT,level=logging.ERROR)
    if args.ignore_syntax == True:
        logger.info('ignoring syntax errors in json.')
    logging.warn("starting")

    argsdir = argsopt[0]

    if not 'OPCO' in os.environ:
        _COM = re.compile('config\.(\w+).*')
        m = _COM.match(argsdir)
        opco_type = m.group(1).lower()
        os.environ['OPCO'] = m.group(1).lower()
    else:
        opco_type = os.environ['OPCO'].lower()
        
    os.environ['ARG_LEVEL'] = args.level
    
    if args.app_type is None:
        print "args are none"
        argu = 'BW,BE,JAVA,PCF'
    else:
        print "args from command"
        argu = args.app_type
    __level = None
    argu = argu.split(',')
    
    if args.level:
        __level = args.level
    
    makeFunc('BE',argu,opco_type)
    makeFunc('BW',argu,opco_type)
    makeFunc('JAVA',argu,opco_type)
    makeFunc('PCF',argu,opco_type) 
    
    cleantempfile("tmp")
    handler_class = getattr(importlib.import_module(
        'f_env.descriptors'), 'digest')
    component_resolver = None
    for filename in readDescriptors(argsopt, handler_class, ignore_syntax=args.ignore_syntax):
        if not filename.endswith(".json"):
            return
        # print filename, args.ignore_syntax
        for match in app_type:
            #print filename,match()
            if fnmatch.fnmatch(filename, match()):
                comps = digest(filename, ignore_syntax=args.ignore_syntax)
                for c in comps:
                    component_resolver = create_manifest_resolver(argsdir,__level)
                    if c.artifactId in component_resolver.manifestLookup.keys():
                        for levelname in c.get_defined_levels():
                            _levelname = str(levelname)
                            if (__level and _levelname == __level):
                                level = c.get_level(levelname)
                                tagetinfo = level['targets']
                                if (c.type == "pcf"):
                                        pcfhealthgenerator.pcfhealth(
                                            c.rtv, level['targets'], __level)
                                else:
                                    extr = c.flattern_rtv_data(__level)
                                    if extr is not None:
                                        #print json.dumps(extr, indent=3)
                                        type_search.healthCheck(c.id, c.type, extr, level['targets'], level['level'])
       
    if (os.path.exists("tmp")):
        type_search.GenerateReport(args.report, __level,args.write_to_disk,args.post)
      
if __name__ == '__main__':
    print "sys.argv=%s" % (sys.argv)
    main(sys.argv[1:    ])
# if (os.path.exists("tmp")):
#     type_search.GenerateReport(args.report,__level)
