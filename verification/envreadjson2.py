import re
import fnmatch
import glob
import shutil
import json
import os
import sys
import subprocess
import logging
import argparse
import f_env
from  f_env import jsonextract
from f_env import type_search
from f_env import csvtoJSON
from f_env import pcfhealthgenerator

TRACE=None
LOG_FORMAT = "%(asctime)-15s [%(levelname)-5s](%(filename)s:%(module)s:%(lineno)d) - %(funcName)s: %(message)s"
LOGGER = logging.getLogger('verify')
logging.basicConfig(format=LOG_FORMAT,level=logging.DEBUG)

app_type = []
def makeFunc(app_type, x,argu,opco_type):
    if x in argu:
        print x
        if (x == "BW"):
            y = opco_type +"-" + x.lower()+"2*.json"
        else:
            y = opco_type +"-" + x.lower()+"*.json"
        app_type.append(lambda: y)
        #y = opco_type +"-" + x.lower() +"-*.json"

def Validatejson(filename):
    try:
        return json.load(filename)
    except ValueError as e :
        print ("json file error %s" % (e))
        return None


def main(__argv):
    parser = argparse.ArgumentParser(add_help = False)
    parser = argparse.ArgumentParser(add_help = False)
    parser.add_argument('-d', '--directory',  action="store", nargs="+", help='Deployment descriptor file in JSON format (required)')
    parser.add_argument('-t', '--app_type',  action="store", nargs="?",default = None, help='app_type')
    parser.add_argument('-l', '--level',  action="store", nargs="?",default = None, help='filter levels')
    parser.add_argument('-r', '--report',  action="store", nargs="?", help='host_health or app_health needs to pass as argument')
    args = parser.parse_args(__argv)
    LOGGER.info("processing: %s", args)
    argsopt = args.directory

    __level = None
    argu = args.app_type.split(',')
    LOGGER.info("app_types: %s" % (argu))
    argsdir = argsopt[0]

    try:
        if not 'OPCO' in os.environ:
            _COM=re.compile('config\.(\w+).*')
            m = _COM.match(argsdir)
            opco_type = m.group(1).lower()
            os.environ['OPCO'] = m.group(1).lower()
    except:
        raise Exception('missing proper config directory')

    def cleantempfile(outdir):
        if (os.path.exists(outdir)):
            shutil.rmtree(outdir)
            print("path existed, deleting ... Deleted")
            os.mkdir("tmp",0775)
        else:
            os.mkdir("tmp",0775)
    if args.app_type is None:
        print "args are none"
        argu = 'BW,BE,JAVA,PCF'
    else:
        print "arga are from command"
        argu = args.app_type

    if args.level:
        __level = args.level


    makeFunc(app_type, 'BE',argu,opco_type)
    makeFunc(app_type, 'BW',argu,opco_type)
    makeFunc(app_type, 'JAVA',argu,opco_type)
    makeFunc(app_type, 'PCF',argu,opco_type)

    FleDir = argsopt[0]
    cleantempfile("tmp")

    already_=[]
    directory = os.path.join(os.getcwd(),FleDir)
    print os.listdir(directory)
    for files in os.listdir(directory):
        for match in app_type:
            LOGGER.info("file match: %s = %s" % (files, fnmatch.fnmatch(files, match())))
            if fnmatch.fnmatch(files, match()):
                LOGGER.warn("match %s",files)
                directory = os.path.join(os.getcwd(),FleDir)
                filename = os.path.join(directory,files)
                # if not filename.endswith(".json"):
                #     continue
                with open(filename) as json_file:
                    data = Validatejson(json_file)
                    if (data != None):
                        id =  jsonextract.GetId(data)
                        type = jsonextract.GetType(data)
                        levels = jsonextract.lvlList(data)
                        for level in levels:
                            lvl = str(level)
                            if __level and lvl == __level or __level is None:
                                tagetinfo = jsonextract.level_targets(data,level)
                                if (type == "pcf"):
                                    domain_info = jsonextract.rtv_data(data)
                                    pcfhealthgenerator.pcfhealth(domain_info,tagetinfo,level)
                                else:
                                    extr = jsonextract.flattern_rtv_data(data,level)
                                    if extr is not None:
                                        if (type == "tibco_bw2" or type == "tibco_bw"):
                                            type = "tibco_bw"
                                        elif (type == "java_auto" or type == "java"):
                                            type = "java"
                                        else:
                                            type = type
                                        type_search.healthCheck(id,type,extr,tagetinfo,level)
    if (os.path.exists("tmp") and os.path.exists('tmp/probe.csv')):
        type_search.GenerateReport(args.report,__level)

if __name__ == "__main__":
    __args = sys.argv
    # strip of the test arguments and take
    #print "print=",__args
    if 'pytest' in sys.argv[0]:
        #print "print=",__args
        idx = __args.index('-d')
        if idx > 0:
            __args = sys.argv[idx:]
    # print " \targs=%s " % (__args)
    # print "fdeploy.py.__main__[%s](%d)" % (__args, len(sys.argv))
    main(__args)
else:
    main(sys.argv[2:])
