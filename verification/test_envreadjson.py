# -*- coding: utf-8 -*-
import os
import json
import pytest
import imp
import unittest
import sys
import copy
import codecs
import logging
from f_env.csvtoJSON import process_line, APP_HEALTH_FIELDS, app_health_indexer
#from f_env.type_search import load_snippet

def load_snippet(snippetFile):
    snippet_text = ''
    #with open ("snippets/" + snippetFile, "r") as snippet:
    with codecs.open (snippetFile, "r", "utf-8") as snippet:
        snippet_text=snippet.read()
    return snippet_text.encode('utf8', 'ignore')


def addOrReplace(base, command):
    for c in command:
        base.append(c)
    #print "base: ", str(base)

os.environ['level']='L1'

def doSsh(opco,level='L1', app='BW', reporttype='app_health'):
    print sys.argv
    addOrReplace(sys.argv, ['-d', "config.%s" % (opco),'-t',app,'-l',level,'-r', reporttype])
    runpy = imp.load_source('__main__', 'envreadjson2.py')

class TestEnvPython(object):
    _original_argv_ = []

    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)
        if len(self._original_argv_) < 1:
            self._original_argv_ = copy.deepcopy(sys.argv)
        #print "setUp: %s (class-argv=%s)" % (self, TestFdeploy._original_argv_)
        sys.argv = copy.deepcopy(TestEnvPython._original_argv_)


    def test_envreadjson(self):
        doSsh('FXE')
        assert os.path.exists('./config.FXE/fxe-bw2-ClearanceFeed.txt')
        assert os.path.exists('./tmp/fxe-bw2-ClearanceFeed.sh')
        a = load_snippet('./config.FXE/fxe-bw2-ClearanceFeed.txt')
        b = load_snippet('./tmp/fxe-bw2-ClearanceFeed.sh')
        assert a == b

if __name__ == '__main__':
    unittest.main()
