import json
import re
import requests
import os

FDX_PROXIES = {
  'http': 'http://internet.proxy.fedex.com:3128',
  'https': 'http://internet.proxy.fedex.com:3128'
}

def removekey(d, key):
    r = dict(d)
    if key in r:
        del r[key]
    return r

APP_HEALTH_FIELDS = ('type','cmd', 'code', 'cpu', 'date', 'host', 'mem', 'occurances', 'pid','time','level','artifactId','deployId','version')
HOST_HEALTH_FIELDS = ('hostNm','hostCname','useSsl','activeFlag','lastSuccessful','lastFailed','lastHostDown','publicIpAddress','privateIpAddress','level','level_tag','eaiNumber','ramSize','appUsers','opcoId','opcoType')
SYSTEM_INFO_FIELDS = ('hostname','interval','timestamp','cpu','user','nice','system','iowait','steal','idle','kbmemfree','kbmemused','percentMemused','kbbuffers','kbcached','kbcommit','percentCommit','kbswpfree','kbswpused','percentSwpused','percentSwpcad','kbswpcad','runq_sz','plist_sz','ldavg_15','ldavg_1','ldavg_5','iface','rxpck','txpck','rxkB','txkB','rxcmp','txcmp','rxmcst')
class app_health_indexer(object):
    fileds = APP_HEALTH_FIELDS
    endpoint="probe"
    separtaor=","

    def __init__(self):
        pass

    def get_index(self,r):
        if r[0].lower() == 'pcf':
            return "%s-%s" % (r[8], r[10])
        else:
            return r[10]

class host_health_indexer(object):
    fileds = HOST_HEALTH_FIELDS
    endpoint="agent"
    separtaor=":"

    def __init__(self):
        pass

    def get_index(self,r):
        return "%s-%s"  % (r[8],os.environ['OPCO'])
    
class host_info_indexer(object):
    fileds = SYSTEM_INFO_FIELDS
    endpoint="system"
    separtaor=";"

    def __init__(self):
        pass

    def get_index(self,r):
        return "%s"  % (os.environ['OPCO'])


class csvtoJSON(object):
    pass
def __init__():
    pass

def process_line(l, indexer, levels_hash,type):
    r = {}
    l = re.sub('\n','',l)
    i = 0
    # csvparser
    index = ''
    
    columns = l.split(indexer.separtaor,35)
    print len(columns)
    if len(columns) == 36:
        columns.pop()
    index = indexer.get_index(columns)
    # for app_health:
    #   pcf = L1-3534545
    #   other = L1
    # for host heatlh:
    #   any = L1-FXG
    if index not in levels_hash.keys():
        if type == 'app_health':
            levels_hash[index] = { 'level' : columns[10],'process_type':columns[0], 'opco' : columns[8].upper(), 'response' : [] } if columns[0] == 'PCF' else  { 'level' : columns[10], 'process_type':columns[0],'opco' : os.environ['OPCO'].upper(), 'response' : [] }
        else:
            levels_hash[index] = levels_hash[index] ={ 'level' : os.environ['ARG_LEVEL'].upper(), 'opco' : os.environ['OPCO'].upper(), 'response' : [] }
    for f in columns:
        r[indexer.fileds[i]] = f
        i += 1
    if index != '':
        r = removekey(r, 'level')
        levels_hash[index]['response'].append(r)
        print "saving[%s]: %s" % (index,r)


def publish(csvfile,type):
    indexer = None
    host_pub = "irh00601.ute.fedex.com"
    if 'LDD_HOST' in os.environ:
        host_pub = os.environ['LDD_HOST']
    if ( type == "app_health"):
        indexer = app_health_indexer()
    elif ( type == "system_info"):
        indexer = host_info_indexer()
    else:
        indexer = host_health_indexer()
    responses=[]
    levels_hash ={}
    with open(csvfile, 'r') as f:
        for l in f:
            if not l.startswith('#'):
                process_line(l, indexer, levels_hash,type)
    result=[]
    for lvl in levels_hash.keys():
        result.append(levels_hash[lvl])
    print "sending below json response to LDD"
    print json.dumps(result, indent=2)
    if 'LDD_POSTING' in os.environ:
        try:
            requrl = "http://"+host_pub+":7003/"+indexer.endpoint
            print requrl
            ret = requests.post(requrl, stream=True, proxies= FDX_PROXIES, headers={'Cache-Control': 'no-cache', 'Content-Type':'application/json'}, json = result )
            print ret
            if not ret.ok :
                print "content=%s" % (str(ret.content))
                print str(ret.content)
        except IOError as e:
            print "error=%s" % (e)
    return result
