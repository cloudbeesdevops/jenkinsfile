import copy

def ConvertToHtml(csvreport):
    with open(csvreport, "r") as csvfile:
        lines = csvfile.readlines()
        #content = [x.strip() for x in content] 
        
        b = []
        b.append('''
        <head>
        <style>
        table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        }
        table th{
            border: 2px solid black;
            border-collapse: collapse;
        }
        </style>
        </head>
        <table>
        <tr>
        ''')
        for line in lines:
            headerfields = line.split(",")
            for headers in headerfields:
                if(lines.index(line) == 0):
                    b.append("<th>"+headers.strip()+"</th>" + "\n")
                else:
                    if (headers.strip() == "RUNNING"):
                        b.append("<td style='color:green'>"+headers.strip()+"</td>"+ "\n")
                    elif (headers.strip() == "NOT RUNNING"):
                        b.append("<td style='color:red'>"+headers.strip()+"</td>"+ "\n")
                    elif (headers.strip() == "PARTIAL RUNNING"):
                        b.append("<td style='color:orange'>"+headers.strip()+"</td>"+ "\n")
                    else:
                        b.append("<td>"+headers.strip()+"</td>"+ "\n")
            b.append("</tr>" + "\n")
        b.append('''
        </table>
        </body>
        </html>
        ''')
        c = "".join(b)
        return c
def html_codeToHTML(csvreport,reportfile):
    html_code = ConvertToHtml(csvreport)
    with open(reportfile,'w')as f:
       f.write(html_code) 
        
    
#    with open('report.html','w')as f:
#        f.write("".join(c))
        