import json
import re
import requests
import os
import logging


LDD_URLS = ['irh00601.ute.fedex.com','srh00601.ute.fedex.com','c0009869.test.cloud.fedex.coms']
FDX_PROXIES = {
  'http': 'http://internet.proxy.fedex.com:3128',
  'https': 'http://internet.proxy.fedex.com:3128'
}

logger = logging.getLogger('csvtoJSON')

def removekey(d, key):
    r = dict(d)
    if key in r:
        del r[key]
    return r

APP_HEALTH_FIELDS = ('type','cmd', 'code', 'cpu', 'date', 'host', 'mem', 'occurances', 'pid','time','level','artifactId','deployId','version')
HOST_HEALTH_FIELDS = ('hostNm','hostCname','useSsl','activeFlag','lastSuccessful','lastFailed','lastHostDown','publicIpAddress','privateIpAddress','level','level_tag','eaiNumber','ramSize','appUsers','opcoId','opcoType')
SYSTEM_INFO_FIELDS = ('hostname','interval','timestamp','cpu','user','nice','system','iowait','steal','idle','kbmemfree','kbmemused','percentMemused','kbbuffers','kbcached','kbcommit','percentCommit','kbswpfree','kbswpused','percentSwpused','percentSwpcad','kbswpcad','runq_sz','plist_sz','ldavg_15','ldavg_1','ldavg_5','iface','rxpck','txpck','rxkB','txkB','rxcmp','txcmp','rxmcst')
class app_health_indexer(object):
    fileds = APP_HEALTH_FIELDS
    endpoint="probe"
    separtaor=","

    def __init__(self):
        pass

    def get_index(self,r):
        if r[0].lower() == 'pcf':
            return "%s-%s" % (r[8], r[10])
        else:
            return r[10]

class host_health_indexer(object):
    fileds = HOST_HEALTH_FIELDS
    endpoint="agent"
    separtaor=":"

    def __init__(self):
        pass

    def get_index(self,r):
        return "%s-%s"  % (r[8],os.environ['OPCO'])
    
class host_info_indexer(object):
    fileds = SYSTEM_INFO_FIELDS
    endpoint="system"
    separtaor=";"

    def __init__(self):
        pass

    def get_index(self,r):
        return "%s"  % (os.environ['OPCO'])


class csvtoJSON(object):
    pass
def __init__():
    pass

def process_line(l, indexer, levels_hash,type):
    r = {}
    l = re.sub('\n','',l)
    i = 0
    # csvparser
    index = ''
    columns = l.split(indexer.separtaor,35)
    if len(columns) == 36:
        columns.pop()
    try:
        index = indexer.get_index(columns)
    except:
        logger.error("problems getting an index for record: " + ",".join(columns))
        return
    # for app_health:
    #   pcf = L1-3534545
    #   other = L1
    # for host heatlh:
    #   any = L1-FXG
    if index not in levels_hash.keys():
        if type == 'app_health':
            levels_hash[index] = { 'level' : columns[10],'process_type':columns[0], 'opco' : columns[8].upper(), 'response' : [] } if columns[0] == 'PCF' else  { 'level' : columns[10], 'process_type':columns[0],'opco' : os.environ['OPCO'].upper(), 'response' : [] }
        else:
            levels_hash[index] = levels_hash[index] ={ 'level' : os.environ['ARG_LEVEL'].upper(), 'opco' : os.environ['OPCO'].upper(), 'response' : [] }
    for f in columns:
        r[indexer.fileds[i]] = f
        i += 1
    if index != '':
        r = removekey(r, 'level')
        levels_hash[index]['response'].append(r)
        logger.debug("saving[%s]: %s" % (index,r))


def publish(csvfile,type, write2disk=False, post=False):
    indexer = None
    host_pub = LDD_URLS[0]
    if 'LDD_HOST' in os.environ:
        host_pub = os.environ['LDD_HOST']
    if ( type == "app_health"):
        indexer = app_health_indexer()
    elif ( type == "system_info"):
        indexer = host_info_indexer()
    else:
        indexer = host_health_indexer()
    responses=[]
    levels_hash ={}
    count = 0
    with open(csvfile, 'r') as f:
        for l in f:
            if not l.startswith('#'):
                process_line(l, indexer, levels_hash,type)
                count += 1
        logger.warn("uploading %s probe responses to LDD." % (count))
    result=[]
    for lvl in levels_hash.keys():
        result.append(levels_hash[lvl])
    logger.info("sending json response to LDD")
    logger.debug(json.dumps(result, indent=2))
    if write2disk == True:
        if os.path.isfile(csvfile):
            filename = os.path.basename(csvfile)
            with open("tmp/%s.json" % (filename),'w') as fw:
                fw.write(json.dumps(result, indent=2))
    if 'LDD_POSTING' in os.environ:
        logger.warning( " posting enabled, sending json to LDD")
        try:
            requrl = "http://"+host_pub+":7003/"+indexer.endpoint
            logger.info(requrl)
            os.environ['NO_PROXY']=','.join(LDD_URLS    )
            ret = requests.post(requrl, stream=True, proxies= FDX_PROXIES, headers={'Cache-Control': 'no-cache', 'Content-Type':'application/json'}, json = result )
            logger.debug(ret)
            if not ret.ok :
                logger.info("content=%s" % (str(ret.content)))
                logger.info(str(ret.content))
        except IOError as e:
            logger.info("error=%s" % (e))
    return result
