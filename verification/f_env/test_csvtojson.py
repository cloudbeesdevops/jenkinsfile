# -*- coding: utf-8 -*-
import os
import json
import pytest
import logging
from f_env.csvtoJSON import publish, process_line, APP_HEALTH_FIELDS, HOST_HEALTH_FIELDS, app_health_indexer, host_health_indexer


class TestUploadBuilder(object):

    def test_app_parsing(self):
        l = {}
        indexer = app_health_indexer()
        os.environ['OPCO']='FXG'
        process_line('BE,StopCoreFXGPU_AA_1,0,,,c0012128.test.cloud.fedex.com,,500,,,L4,StopCoreFXG,StopCoreFXGPU_AA_1,3.7.0',indexer,l)
        print l.keys()
        assert l.keys() == ['L4']
        ll = l['L4']
        assert len(ll['response']) == 1

    def test_host_parsing(self):
        l = {}
        indexer = app_health_indexer()
        os.environ['OPCO']='FXG'
        process_line('pcf,StopCoreFXGPU_AA_1,0,,,c0012128.test.cloud.fedex.com,,500,DOMAIN,,L4,StopCoreFXG,StopCoreFXGPU_AA_1,3.7.0',indexer,l)
        print l.keys()
        assert l.keys() == ['DOMAIN-L4']
        ll = l['DOMAIN-L4']
        assert len(ll['response']) == 1

    def test_app_indexer_app(self):
        os.environ['OPCO']='FXG'
        indexer = app_health_indexer()
        arr = 'BE,StopCoreFXGPU_AA_1,0,,,c0012128.test.cloud.fedex.com,,500,,,L4,StopCoreFXG,StopCoreFXGPU_AA_1,3.7.0'.split(',')
        assert len(arr) == len(APP_HEALTH_FIELDS)
        assert indexer.get_index(arr) == 'L4'
        arr = 'pcf,StopCoreFXGPU_AA_1,0,,,c0012128.test.cloud.fedex.com,,500,DOMAIN,,L4,StopCoreFXG,StopCoreFXGPU_AA_1,3.7.0'.split(',')
        assert len(arr) == len(APP_HEALTH_FIELDS)
        assert indexer.get_index(arr) == 'DOMAIN-L4'

    def test_host_indexer_app(self):
        os.environ['OPCO']='FXG'
        indexer = host_health_indexer()
        arr = 'BE,StopCoreFXGPU_AA_1,0,,,c0012128.test.cloud.fedex.com,,500,XYZ,,L4,StopCoreFXG,StopCoreFXGPU_AA_1,3.7.0,,'.split(',')
        assert len(arr) == len(HOST_HEALTH_FIELDS)
        assert indexer.get_index(arr) == 'XYZ-FXG'

    def test_app_indexer_app(self):
        os.environ['OPCO']='FXG'
        indexer = app_health_indexer()
        result = publish('config.FXE/csvfile.csv', 'app_health')
        assert 5 == len(result[0]['response'])
