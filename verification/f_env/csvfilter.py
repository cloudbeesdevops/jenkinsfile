import csv
def uniquelist(filecont):
    fname = []
    for row in filecont:
        if row[0] not in fname:
            fname.append(row[0])
    return fname
                
def filterstatus(reader,name):
    filtered = filter(lambda p: name == p[0], reader)
    finalStatus=[]
    for i in filtered:
        finalStatus.append(i[3])
        if 'RUNNING' in finalStatus and 'NOT RUNNING' in finalStatus:
            filstatus = "PARTIAL RUNNING"
        elif 'RUNNING' in finalStatus:
            filstatus = "RUNNING"
        else:
            filstatus = "NOT RUNNING"
    return filstatus

def filterversion(reader,name):
    filtered = filter(lambda p: name == p[0], reader)
    b =[]
    d = []
    for i in filtered:
        b.append(i[4])
        for c in b:
            if c not in d:
                d.append(c)
    return d
            

def summaryList(filename,outfile):
    print ("summarylist geerating "+ filename + " and " + outfile)
    with open(filename,'r') as f:
        with open(outfile,'a+') as ofile:
            reader = csv.reader(f, delimiter=",")
            header = next(reader)
            rlist = list(reader)
            filtereelist = uniquelist(rlist)
            for i in filtereelist:
                fversion = filterversion(rlist,i)
                fver = '&'.join(fversion)
                fstatus =  filterstatus(rlist,i)
                fname = i
                finalstrig = i + "," + fstatus + "," + fver
                ofile.write(finalstrig+ "\n") 

#summaryList("logfile.txt")
    



