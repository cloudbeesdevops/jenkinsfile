# -*- coding: utf-8 -*-

import os
import os.path
import ConfigParser
import logging

LOGGER = logging.getLogger('manifestResolver')

class manifestResolver:

    __manifest__ = None
    __metadata__ = None
    isLoaded = False
    manifestFile = None
    manifestdir = None
    debug = False
    manifestLookup = {}

    def __init__(self, basedir, level):
        self.__manifest__ = ConfigParser.RawConfigParser()
        self.__manifest__.optionxform = str
        self.manifestdir = basedir + "/" + level
        self.manifestLookup = {}
        self.__readManifest()

    def __readManifest(self):
        self.manifestFile = os.path.normpath(
            os.path.abspath(self.manifestdir + '/RELEASE_MANIFEST.MF'))
        if os.path.exists(self.manifestFile):
            self.__manifest__.read(self.manifestFile)
            LOGGER.info( " < read manifest " + self.manifestFile )
            self.isLoaded = True
            print str(self.__manifest__.items('RELEASE_MANIFEST'))
            for artifactId in self.__manifest__.items('RELEASE_MANIFEST'):
                print "-> %s" % (str(artifactId))
                if artifactId[1] is not None: # and artifactId[1].endswith("*"):
                    # remove astrix
                    version= artifactId[1].replace('*','')
                    
                    self.manifestLookup[artifactId[0]]=version
                else:
                    LOGGER.debug("  --> %s is omitted by manifest" % (artifactId[0]))
        else:
            LOGGER.warn( " ! no release_manifest found for " + str(self.manifestFile))


    def lookup(self, key):
        try:
            return self.manifestLookup[key.lower()]
        except KeyError:
            LOGGER.warn("key %s not found, registered %s." % (key, self.manifestLookup.keys()))
            return None

# manifest.get(
#     'RELEASE_MANIFEST', gav.artifactId)
