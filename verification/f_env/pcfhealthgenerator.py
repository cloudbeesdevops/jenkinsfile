import subprocess
import requests
import re
import pam
import json
from f_env import type_search
from pam import pamAuth
import tempfile
import os
import logging
from formatter import NullWriter
logger = logging.getLogger('pcfhealthgenerator')
try:
    requests.packages.urllib3.disable_warnings()
except AttributeError as ar:
    pass

class pcfhealthgenerator(object):
    pass
def __init__():
    pass

tmpdir = "tmp"
probereport = os.path.join(tmpdir, "probe.csv")
index_dict = {3:'%',4:'M'}

def check_output(*popenargs, **kwargs):
    process = subprocess.Popen(stdout=subprocess.PIPE, *popenargs, **kwargs)
    output, unused_err = process.communicate()
    retcode = process.poll()
    if retcode:
        cmd = kwargs.get("args")
        if cmd is None:
            cmd = popenargs[0]
        error = subprocess.CalledProcessError(retcode, cmd)
        error.output = output
        raise error
    return output

def dos2unix(filename):
    text = open(filename, 'rb').read().replace('\r\n', '\n')
    open(filename, 'wb').write(text)

def despace (sentence):
    while "  " in sentence:
        sentence = sentence.replace("  "," ")
    return sentence

def parse_list_index(nest_list,index):
    matrix = []
    for _nest_list in nest_list:
        matrix.append(_nest_list[index])
    return matrix

def writeTooutfile(outfile,content):
    with open(outfile,'a+') as _outfile:
        _outfile.write(content)

def getartifactId(level,string2):
    if ((string2.startswith(level)) and (string2.startswith("PROD") == False)):
        artifact_id =  string2[string2.index('-')+1:string2.index('.app')]
    else:
        artifact_id = string2[0:string2.index('.app')]
    return artifact_id
def temp_directory():
    tempd = tempfile.mkdtemp()
    return tempd

def parse_health_str(string,index):
    sum_list = []
    for lk in (string.split('#')[1:]):
        split_at_index = lk.split(' ')[index]
        _split_at_index = float(split_at_index[:-1])
        last_letter_str = split_at_index[-1:]
        if (last_letter_str == 'G'):
            _split_at_index = _split_at_index/1000
        elif (last_letter_str == 'M'):
            _split_at_index = _split_at_index
        sum_list.append(_split_at_index)
    return (sum(sum_list))

def get_cpu_memory_disk (status_str,index_dict):
    cpu_memory_disk = []
    for i in index_dict:
        cpu_memory_disk.append(parse_health_str(status_str,i))
    return (cpu_memory_disk)

def update_two_lists(list1,list2):
    finallist = []
    for i in list2:
        for j in list1:
            if (i[-2] == j[0]):
                i[-3] = j[1] #artifact_id
                i[-1] = j[2] #version
                i[1] = "https://"+j[3] #routes
                i[4] = j[4] # Date
                i[3] = str(j[5]) #cpu
                i[6] = str(j[6]) #mem
        finallist.append(i)
    return finallist

def MultilisttoString(list1,list2):
    finallist = update_two_lists(list1,list2)
    contentstring = ''
    for i in finallist:
        fstr = ','.join(i)
        contentstring = contentstring + "%s\n" % (fstr.strip())
    return contentstring


def generate_pcf_commands(scriptfile):
    command_data = []
    script_begin = type_search.load_snippet('pcf_authentication.sh')
    command_data.append(script_begin)
    with open(scriptfile,'a+') as f:
        for scriptline in command_data:
            f.write(scriptline.decode('ASCII') % (
                    creds[0],
                    creds[1],
                    cf_home,
                    api_url,
                    org,
                    space
                )
                )
def generate_pcf_env_command(scriptfile,app_list):
    command_data = []
    script_begin = type_search.load_snippet('pcf_app_env.sh')
    command_data.append(script_begin)
    with open(scriptfile,'a+') as f:
        for scriptline in command_data:
            f.write(scriptline.decode('ASCII') % (
                    createEnvCommand(app_list)
                )
                )

def append_files(filelist,outfile):
    with open(outfile,'w') as out_file:
        for fname in filelist:
            with open(fname) as infile:
                for line in infile:
                    out_file.write(line)


def getUserpassword(pamid):
    values = pamAuth().get_passwd(pamid)
    return values

def createEnvCommand(list):
    envvar_list = ''
    for _list in list:
        envvar_list = envvar_list + "cf env %s\ncf app %s\n" % (_list.strip(),_list.strip())
    return envvar_list

def getappinfo(app_list,domain_lvl):
    search_strings = ['APP_ARTIFACT','APP_VERSION','routes','last uploaded','state ']
    #artifact_id,version,#routes,#loast upload
    value = ""
    cfenvscript = os.path.join(tmpdir, domain_lvl+"authenv"+".sh")
    cfauthscript = os.path.join(tmpdir, domain_lvl+"auth"+".sh")
    cfauthenvscript = os.path.join(tmpdir, domain_lvl+"finalauthenv"+".sh")
    generate_pcf_env_command(cfenvscript,parse_list_index(app_list,0))
    envscriptlist = [cfauthscript,cfenvscript]
    append_files(envscriptlist, cfauthenvscript)
    try:
        app_env_info = check_output('/bin/sh '+ cfauthenvscript, shell=True)
        logger.info(app_env_info)
        list2 = []
        global fvalue
        split_leth = len(app_env_info.split('Getting env variables for app'))
        splitvalue = app_env_info.split('Getting env variables for app')
        list2 = []
        for i in range(0,split_leth):
            for app in app_list:
                app1 = app[0]+" in org"
                if app1 in splitvalue[i]:
                    list1 = []
                    length = int(app[1])
                    list1.append(app[0])
                    for _search_strings in search_strings:
                        if  _search_strings in splitvalue[i]:
                            iter_obj = iter(splitvalue[i].splitlines())
                            for k in iter_obj:
                                if _search_strings in k:
                                    fvalue = ""
                                    if (_search_strings == 'state '):
                                        actual = 0
                                        strco = ""
                                        if (int(app[2]) == 200):
                                            while actual < length:
                                                a = (next(iter_obj))
                                                strco = (strco+a)
                                                actual += 1
                                            fvalue = get_cpu_memory_disk(despace(strco), index_dict)
                                        else:
                                            fvalue = ["",""]
                                    else:
                                        fvalue =  k.split(': ')[1].strip()
                                        if not fvalue.strip():
                                            fvalue = [""]
                        else:
                            if (_search_strings == 'state '):
                                fvalue = ["",""]
                            else:
                                fvalue =  ""
                        if (type(fvalue) == list):
                            list1.extend(fvalue)
                        else:
                            list1.append(fvalue)
                    list2.append(list1)
        return list2
    except subprocess.CalledProcessError as e:
        logger.error(e.output)
        logger.info("It is handling by exception call"    )



def getprocessinfo(filesuffix,process,probereport,level):
    process = re.sub(r'^$\n','',process) # removing blank lines
    valuesplit = process.split('OK')
    appinfo = valuesplit[len(valuesplit) -1]
    first_list = []
    app_list = []
    for i in appinfo.split("\n"):
        if ((i.startswith(level)) or (i.startswith(level.lower()))):
            despace1 =  despace(i)
            splitContent = despace1.split(" ")
            APP_NAME = splitContent[0]
            if not (('backout' in APP_NAME) or ('green' in APP_NAME)):
                instances = splitContent[2].split('/')[1]
                #memory = splitContent[3]
                #disk = splitContent[4]
                if (splitContent[2].split('/')[1] != splitContent[2].split('/')[0]):
                    status = "500"
                else:
                    status = "200"
                first_list.append(["PCF","",status,'',"",api_url,'',instances,org,"",level,"",APP_NAME,""])
                app_list.append([APP_NAME,instances,status])
    app_info = getappinfo(app_list,filesuffix)
    content = MultilisttoString(app_info, first_list)
    writeTooutfile(probereport,content)

def Validatejson(filename):
    try:
        return json.load(filename)
    except ValueError as e :
        logger.info(("json file error %s" % (e)))
        return None

def pcfhealth(domains,targets,level):
    global creds,api_url,org,space,cf_home
    for domain in domains:
        for target in targets:
            space,api_url = target.split('@')
            creds = getUserpassword(domain['value'])
            _COM=re.compile('FXS_app(\d+).*')
            m = _COM.match(creds[0])
            org = m.group(1)
            data = []
            domain_lvl = domain['name']+"-"+level
            cfauthscript = os.path.join(tmpdir, domain_lvl+"auth"+".sh")
            finalscriptname =os.path.join(tmpdir, domain_lvl+"-"+level+".sh")
            cf_home = os.path.join(tmpdir, temp_directory())
            generate_pcf_commands(cfauthscript)
            appscriptlist = [cfauthscript,'snippets/pcf_apps.sh']
            append_files(appscriptlist, finalscriptname)
            dos2unix(finalscriptname)
            try:
                process = check_output('/bin/sh '+ finalscriptname, shell=True)
                getprocessinfo(domain_lvl,process,probereport,level)
            except subprocess.CalledProcessError as e:
                logger.error(e.output)
                logger.info("It is handling by exception call"    )
