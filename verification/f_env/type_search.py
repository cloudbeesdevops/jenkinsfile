import os
import json
import jsonextract
import codecs
import subprocess
import requests
import csvtoJSON
import logging
import tempfile
logger = logging.getLogger('type_search')

class type_search(object):
    pass
def __init__():
    pass

tempdir = "tmp"
probereport = os.path.join(tempdir, "probe.csv")
healthreport = os.path.join(tempdir, "healthreport.csv")
OPCO_ID = {'FXE':'1','FXF':'3','FXG':'2','3534681':'3534681'}

def listTOdict(list):
    fdict = {}
    for value in list:
        fdict[value['name']] = value ["value"]
    return fdict

def load_snippet(snippetFile):
    snippet_text = ''
    #with open ("snippets/" + snippetFile, "r") as snippet:
    with codecs.open ("snippets/" + snippetFile, "r", "utf-8") as snippet:
        snippet_text=snippet.read()
    return snippet_text.encode('utf8', 'ignore')

def removeDupItems(list):
    final_list = []
    for list_items in list:
        if list_items not in final_list:
            final_list.append(list_items)
    return final_list


host_array = []
def tibco_be(id,extr,targets,level):
    global user,grepword,host,path,AdminHost,comp_name,Adminuser,comp_type,comp_nam,artifact_id
    outfile = id+".sh"
    comp_type = "BE"
    fd =  listTOdict(extr)
    artifact_id = fd['comp_Name']
    if 'DISPLAY_NAME' in fd:
        comp_nam = fd['DISPLAY_NAME']
    else:
        comp_nam = fd['comp_Name']
    index = 1
    for target in targets:
        comp_name =  ("%s_%s" % (comp_nam,index))
        index += 1
        user =  target.split('@')[0]
        if user.endswith("cm"):
            user = user.replace("cm", "").strip()
        host =  target.split('@')[1].split(':')[0]
        path = target.split(':')[1]
        grepword =  fd['LOGICAL_NAME']
        AdminHost = host
        Adminuser = user
        user_host = user.strip()+"@"+host.strip()
        generate_commands(outfile,level)
        host_array.append(user_host)

def tibco_bw(id,extr,targets,level):
    global user,grepword,host,path,AdminHost,comp_name,Adminuser,comp_type,comp_nam,artifact_id
    comp_type = "BW"
    outfile = id+".sh"
    fd =  listTOdict(extr)
    artifact_id = fd['comp_Name']
    if 'DISPLAY_NAME' in fd:
        comp_nam = fd['DISPLAY_NAME']
    else:
        comp_nam = fd['comp_Name']
    targeturl = fd['ADMIN_TARGET_URL']
    path = targeturl.split('@')[1].split(':')[1]
    grepword =  fd['PROCESS_NAME']
    index = 1
    for target in targets:
        comp_name =  ("%s_%s" % (comp_nam,index))
        index += 1
        host =  target.split('@')[1]
        if host.startswith(("urh","irh","srh","vrh")):
            user="tibco"
        else:
            user =  targeturl.split('@')[0]
        if user.endswith("cm"):
            user = user.replace("cm", "").strip()
        if ((user == 'tibcosilver' or user == 'tibco') and host.__contains__('cloud.fedex.com')):
            user = 'sefs'
        Adminuser =  targeturl.split('@')[0]
        AdminHost =  targeturl.split('@')[1].split(':')[0]
        generate_commands(outfile,level)
        user_host = user.strip()+"@"+host.strip()
        adminuser_host = Adminuser.strip()+"@"+AdminHost.strip()
        host_array.append(user_host)
        host_array.append(adminuser_host)

def java(id,extr,targets,level):
    global user,grepword,host,path,AdminHost,comp_name,Adminuser,comp_type,comp_nam,artifact_id
    outfile = id+".sh"
    comp_type = "JAVA"
    fd =  listTOdict(extr)
    artifact_id = fd['comp_Name']
    if 'APP_ID' in fd:
        grepword=fd['APP_ID']
    else:
        grepword=fd['comp_Name']
    comp_nam = fd['comp_Name']
    index = 1
    for target in targets:
        comp_name =  ("%s_%s" % (comp_nam,index))
        index += 1
        user =  target.split('@')[0]
        if user.endswith("cm"):
            user = user.replace("cm", "").strip()
        host =  target.split('@')[1].split(':')[0]
        path = target.split(':')[1]
        AdminHost = host
        Adminuser = user
        user_host = user.strip()+"@"+host.strip()
        generate_commands(outfile,level)
        host_array.append(user_host)


def healthCheck(id,type,extr,targets,level):
    if (type == 'tibco_be'):
        tibco_be(id,extr,targets,level)
    if (type == 'tibco_bw'):
        tibco_bw(id,extr,targets,level)
    if (type == 'java'):
        java(id,extr,targets,level)

def generate_commands(scriptfile,level):
    command_data = []
    outfile = os.path.join(tempdir,scriptfile)
    script_begin = load_snippet('finalexec.sh')
    command_data.append(script_begin)
    with open(outfile,'a+') as f:
        for scriptline in command_data:
            f.write(scriptline.decode('ASCII') % (
                    comp_type,
                    comp_name,
                    user,
                    host,
                    path,
                    grepword,
                    AdminHost,
                    Adminuser,
                    probereport,
                    level,
                    artifact_id
                )
                )

def generate_host_health(level):
    command_data = []
    scriptfile = "host_agent_health.sh"
    outfile = os.path.join(tempdir,scriptfile)
    host_list = removeDupItems(host_array)
    os.environ['OPCO'].upper()
    script_begin = load_snippet('host_health.sh')
    command_data.append(script_begin)
    for host_info in host_list:
        user =  host_info.split('@')[0]
        host = host_info.split('@')[1]
        #if host.startswith("c"):
        with open(outfile,'a+') as f:
            for scriptline in command_data:
                f.write(scriptline.decode('ASCII') % (
                        user,
                        host,
                        OPCO_ID[os.environ['OPCO'].upper()],
                        level,
                        healthreport
                    )
                    )
                
def generate_system_info(level):
    command_data = []
    scriptfile = "host_system_info.sh"
    outfile = os.path.join(tempdir,scriptfile)
    host_list = removeDupItems(host_array)
    script_begin = load_snippet('system_info.sh')
    command_data.append(script_begin)
    for host_info in host_list:
        user =  host_info.split('@')[0]
        host = host_info.split('@')[1]
        #if host.startswith("c"):
        with open(outfile,'a+') as f:
            for scriptline in command_data:
                f.write(scriptline.decode('ASCII') % (
                        user,
                        host
                    )
                    )

def dos2unix(filename):
    text = open(filename, 'rb').read().replace('\r\n', '\n')
    open(filename, 'wb').write(text)

def GenerateReport(report,level, write2disk=False, posting=False):
    #logger.info(report)
    name = probereport
    if (os.environ['OPCO'] != 'PCF'):
        name = None
        with tempfile.NamedTemporaryFile(delete=False) as fp:
            name = fp.name
            if (report == "host_health"):
                generate_host_health(level)
                shellfiles = "health.sh"
            elif (report == "system_info"):
                generate_system_info(level)
                shellfiles = "info.sh"
            else:
                shellfiles = ".sh"
            if os.path.exists(tempdir):
                for file in os.listdir(tempdir):
                    if file.endswith(shellfiles):
                        abspath_file = os.path.join(tempdir,file)
                        os.chmod(abspath_file, 0b111101101)
                        logger.info("processing %s" % (abspath_file))
                        cmd = "/bin/sh "+abspath_file
                        #subprocess.call(['dos2unix '+ abspath_file],shell=True)
                        dos2unix(abspath_file)
                        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
                        for line in process.stdout:
                            #line = line.strip()
                            fp.write(line)
                            logger.debug(line.strip())
                        process.wait()
    logger.info("reading csv for json generation: %s" % (name))
    csvtoJSON.publish(name,report)
