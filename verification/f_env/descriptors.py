
import os
import logging
import glob
import json
import re
from f_env.jsonextract import compName, rtv_data
import logging
from f_env.manifestResolver import manifestResolver

logger = logging.getLogger('descriptors')


class fComponent(object):

    def __init__(self, dict, source):
        if dict is None:
            return None
        self.id = None
        self.dict = dict
        self.levels = dict['levels']
        if 'rtv' in dict:
            self.rtv = dict['rtv']
        else:
            self.rtv = []
        self.id = dict['id']
        self.platform = dict['platform']
        self.type = dict['type']
        if "tibco_bw2" in self.type:
            self.type = 'tibco_bw'
        if "java_auto" in self.type:
            self.type = 'java'

        self.lddtype = re.sub(r'tibco_', '', dict['type'])
        self.lddtype = re.sub(r'tibco', '', self.type)
        self.lddtype = re.sub(r'leech', '', self.type)


        self.files = []
        self.artifactId = ""
        self.source = source
        self.content = dict['content'] if 'content' in dict else []
        self.contents = []
        self.rtvMapping = {}
        self.rtvlist = []
        for item in self.rtv:
            name = str(item['name'])
            self.rtvMapping[name] = str(item['value'])
            self.rtvlist.append({u'name': name, u'value': item['value']})
        if self.content:
            for cont in self.content:
                if not 'gav' in cont.keys():
                    continue
                self.artifactId = cont['gav'].split(":")[1]
                self.contents.append({u'name': u'comp_Name', u'value': self.artifactId})

    def get_defined_levels(self):
        arr = []
        for l in self.levels:
            arr.append(l['level'])
        return arr

    def get_level(self,levelname):
        for l in self.levels:
            if l['level'] == levelname:
                return l
        return None
    

    def flattern_rtv_data (self,level):
        rtv_lvl = []
        _lvl = self.get_level(level)
        logger.error("lvel value is %s" %(_lvl))
        if 'rtv' in _lvl:
            rtv_lvl = _lvl["rtv"]
            rtv_lvl.extend(self.contents)
            rtv_lvl.extend(self.rtvlist)
        else:
            rtv_lvl.extend(self.contents)
            rtv_lvl.extend(self.rtvlist)
        return rtv_lvl
    

    ''' digest '''
def digest(loadingFile,ignore_syntax=False):
    deploy_descr = []
    with open(loadingFile) as descriptor:
        try:
            deploy_descr = json.loads(descriptor.read())
        except Exception, e:
            logger.error("loading %s failed.: reason: %s (relaxed syntax-%s)" % (loadingFile, e, ignore_syntax))
            if ignore_syntax == False:
                raise Exception("loading %s failed.: reason: %s" % (loadingFile, e))
                return
    comps = []
    for component in deploy_descr:
        comp = fComponent(component, loadingFile)
        logger.debug("adding %s" % (comp))
        comps.append(comp)
    return comps

def create_manifest_resolver(_loaddir_,level):
    return manifestResolver(_loaddir_, level)


def readDescriptors(argv, digest_function=None, ignore_syntax=False):
    cfg_files = []
    readme_content = ''
    for argvJSON in argv[:]:
        cfg_descriptors = glob.glob(os.path.expandvars(argvJSON+"/*.json"))
        cfg_files.extend(cfg_descriptors)
    if len(cfg_files) > 0:
        dir = os.path.dirname(os.path.abspath(cfg_files[0]))
    logger.info(" > found %s descriptor files." %
                  (len(cfg_files)))

    if len(cfg_files) == 0:
        raise Exception("No files found with %s " % (argv))
    return cfg_files
#     for cfgProp in cfg_files[:]:
#         cfg_descriptorsFile = os.path.abspath(os.path.normpath(cfgProp))
#         logger.debug("processing '%s'", cfg_descriptorsFile)
#         #assert os.path.isfile(
#         #    cfg_descriptorsFile), "Properties file: " + cfg_descriptorsFile + " NOT FOUND!"
#         cfg_descriptorsFileBasename = os.path.basename(
#             cfg_descriptorsFile)
#         logger.debug(" < reading descriptor file: " +
#                       cfg_descriptorsFile)
#         if digest_function is not None:
#             loadingFile = os.path.abspath(cfg_descriptorsFile)
#             digest_function(loadingFile,ignore_syntax)
    
