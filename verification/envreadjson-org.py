import re
import fnmatch
import glob
import shutil
import json
import os
import subprocess
import argparse
import f_env
from  f_env import jsonextract
from f_env import type_search
from f_env import csvtoJSON
from f_env import pcfhealthgenerator

parser = argparse.ArgumentParser(add_help = False)
parser = argparse.ArgumentParser(add_help = False)
parser.add_argument('-d', '--directory',  action="store", nargs="+", help='Deployment descriptor file in JSON format (required)')
parser.add_argument('-t', '--app_type',  action="store", nargs="?",default = None, help='app_type')
parser.add_argument('-l', '--level',  action="store", nargs="?",default = None, help='filter levels')
parser.add_argument('-r', '--report',  action="store", nargs="?", help='host_health or app_health or system_info needs to pass as argument')
args = parser.parse_args()
argsopt = args.directory

argsdir = argsopt[0]

if not 'OPCO' in os.environ:
    _COM=re.compile('config\.(\w+).*')
    m = _COM.match(argsdir)
    opco_type = m.group(1).lower()
    os.environ['OPCO'] = m.group(1).lower()
else:
    opco_type = os.environ['OPCO'].lower()

os.environ['ARG_LEVEL'] = args.level

def cleantempfile(outdir):
    if (os.path.exists(outdir)):
        shutil.rmtree(outdir)
        print("path existed, deleting ... Deleted")
        os.mkdir("tmp",0775)
    else:
        os.mkdir("tmp",0775)
if args.app_type is None:
    print "args are none"
    argu = 'BW,BE,JAVA,PCF'
else:
    print "arga are from command"
    argu = args.app_type
__level = None
argu = argu.split(',')
print argu

if args.level:
    __level = args.level






app_type = []
def makeFunc(x,argu,opco_type):
    if x in argu:
        print x
        if (x == "BW"):
            #y = opco_type +"-" + x.lower()+"2-*.json"
            y = "*-" + x.lower()+"2-*.json"
        else:
            y = opco_type +"-" + x.lower()+"-*.json"
        app_type.append(lambda: y)
        #y = opco_type +"-" + x.lower() +"-*.json"



makeFunc('BE',argu,opco_type)
makeFunc('BW',argu,opco_type)
makeFunc('JAVA',argu,opco_type)
makeFunc('PCF',argu,opco_type)
print app_type
def Validatejson(filename):
    try:
        return json.load(filename)
    except ValueError as e :
        print ("json file error %s" % (e))
        return None


def Main():
     FleDir = argsopt[0]
     cleantempfile("tmp")
     for files in os.listdir(FleDir):
         for match in app_type:
             if fnmatch.fnmatch(files, match()):
                 directory = os.path.join(os.getcwd(),FleDir)
                 filename = os.path.join(directory,files)
                 if filename.endswith(".json"):
                     print filename
                     with open(filename) as json_file:
                         data = Validatejson(json_file)
                         if (data != None):
                             print filename
                             id =  jsonextract.GetId(data)
                             type = jsonextract.GetType(data)
                             levels = jsonextract.lvlList(data)
                             for level in levels:
                                 lvl = str(level)
                                 if __level and lvl == __level or __level is None:
                                     tagetinfo = jsonextract.level_targets(data,level)
                                     if (type == "pcf"):
                                         domain_info = jsonextract.rtv_data(data)
                                         pcfhealthgenerator.pcfhealth(domain_info,tagetinfo,level)
                                     else:
                                         extr = jsonextract.flattern_rtv_data(data,level)
                                         if extr is not None:
                                             if (type == "tibco_bw2" or type == "tibco_bw"):
                                                 type = "tibco_bw"
                                             elif (type == "java_auto" or type == "java"):
                                                 type = "java"
                                             else:
                                                 type = type
                                             type_search.healthCheck(id,type,extr,tagetinfo,level)

Main()
if (os.path.exists("tmp")):
    type_search.GenerateReport(args.report,__level)
