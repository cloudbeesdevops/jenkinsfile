# -*- coding: utf-8 -*-

import os
import json
import requests
requests.packages.urllib3.disable_warnings()

JENKINS_TO_VAULT = "uHgC9cq|Fz+V0jH{"
userName = 'jenkins_api_user-1459'
pam_url = 'https://pam-wtc-vip.prod.fedex.com/api.php/v1/passwords.json/%s?reason=Severity+3%%3A+Pre-production+application+testing'

class pamAuth:


    def __init__(self):
        pass

    def get_passwd(self, pam_id ):
        url = pam_url % (pam_id)
        print ("pam request %s", url)
        r = requests.get(url, auth=(userName, JENKINS_TO_VAULT), verify=False )
        if r.ok:
            dict = r.json()
            return dict['accountName'], dict['password']
        else:
            print ("pam request %s failed", url)
            raise Exception('pam request failed for pam_id=%s' % (pam_id))
