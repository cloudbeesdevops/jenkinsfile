#!/bin/sh
#Version 1.0
#------------------------------------------------------------------------------#
echo "Archiving & Purging SRS-RDT-InvAdj Application logs."
#------------------------------------------------------------------------------#


#------------------------------------------------------------------------------#
#variables
#------------------------------------------------------------------------------#

RDT_WEBSVC_ARCHIVE=${ARCHIVE_DIR_45D}/rdt_websvc_domain/
REVENUE_DESKTOP_SERVER_ARCHIVE=${ARCHIVE_DIR_45D}/invadj_websvc_domain/
INVOICE_ADJMT_ARCHIVE=${ARCHIVE_DIR_45D}/rdt_mdb_domain/

RDT_WEBSVC_LOGS=${LOGFILES}/rdt-websvc/
REVENUE_DESKTOP_SERVER_LOGS=${LOGFILES}/revenue.desktop.server/
INVOICE_ADJMT_EAR_LOGS=${LOGFILES}/invoice-adjustments/invoice-adjustments-ear/
INVOICE_ADJMT_WAR_LOGS=${LOGFILES}/invoice-adjustments/invoice-adjustments-war/


if [ -n ${RDT_ARCHLOGS_DAYS} ]
then
        RDT_ARCHIVE_DAYS="20"
        export RDT_ARCHIVE_DAYS
fi

if [ -n ${RDT_PURGELOGS_DAYS} ]
then
        RDT_PURGELOGS_DAYS="20"
        export RDT_PURGELOGS_DAYS
fi


#------------------------------------------------------------------------------#
echo "Moving logs older than ${RDT_ARCHIVE_DAYS} days to archive directories ..."
#------------------------------------------------------------------------------#

cd ${RDT_WEBSVC_LOGS}
find ./* -type f -mtime +${RDT_ARCHIVE_DAYS} -exec gzip -9 {} \; -exec mv -f {}.gz ${RDT_WEBSVC_ARCHIVE} \;

cd ${REVENUE_DESKTOP_SERVER_LOGS}
find ./* -type f -mtime +${RDT_ARCHIVE_DAYS} -exec gzip -9 {} \; -exec mv -f {}.gz ${REVENUE_DESKTOP_SERVER_ARCHIVE} \;

cd ${INVOICE_ADJMT_EAR_LOGS}
find ./* -type f -mtime +${RDT_ARCHIVE_DAYS} -exec gzip -9 {} \; -exec mv -f {}.gz ${INVOICE_ADJMT_ARCHIVE} \;

cd ${INVOICE_ADJMT_WAR_LOGS}
find ./* -type f -mtime +${RDT_ARCHIVE_DAYS} -exec gzip -9 {} \; -exec mv -f {}.gz ${INVOICE_ADJMT_ARCHIVE} \;


#------------------------------------------------------------------------------#
echo "Delete all archived files older than ${RDT_PURGELOGS_DAYS} days ..."
#------------------------------------------------------------------------------#

cd ${RDT_WEBSVC_ARCHIVE}
find ./* -type f -mtime +${RDT_PURGELOGS_DAYS} -exec rm -f {} \;

cd ${REVENUE_DESKTOP_SERVER_ARCHIVE}
find ./* -type f -mtime +${RDT_PURGELOGS_DAYS} -exec rm -f {} \;

cd ${INVOICE_ADJMT_ARCHIVE}
find ./* -type f -mtime +${RDT_PURGELOGS_DAYS} -exec rm -f {} \;

#------------------------------------------------------------------------------#

echo "Archiving process completed."
