package com.fedex.chronos.irdt;

import org.junit.*;
import static org.junit.Assert.*;


/**
 * The class <code>AppUtilsTest</code> contains tests for the class <code>{@link AppUtils}</code>.
 *
 * @generatedBy CodePro at 11/21/13 6:22 PM
 * @author 944633
 * @version $Revision: 1.0 $
 */
public class AppUtilsTest{
	/**
	 * Run the AppUtils() constructor test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testAppUtils_1()
	{

		AppUtils result = new AppUtils();
		assertNotNull(result);
	}

	/**
	 * Run the String appendSpaces(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testAppendSpaces_1()
	{
		String str = "SRS-INVADJ";
		int len = 15;

		String result = AppUtils.appendSpaces(str, len);

		// add additional test code here
		assertEquals("SRS-INVADJ     ", result);
	}

	/**
	 * Run the String appendSpaces(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testAppendSpaces_2()
	{
		String str = "SRS-INVADJ";
		int len = 10;

		String result = AppUtils.appendSpaces(str, len);

		// add additional test code here
		assertEquals("SRS-INVADJ", result);
	}

	/**
	 * Run the String appendSpaces(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testAppendSpaces_3()
	{
		String str = "SRS-INVADJ";
		int len = 8;

		String result = AppUtils.appendSpaces(str, len);

		// add additional test code here
		assertEquals("SRS-INVADJ", result);
	}

	/**
	 * Run the String getHostname() method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testGetHostname_1()
	{

		String result = AppUtils.getHostname();
		//assertEquals("wtc-153987-w20", result);
		assertNotNull(result);
	}

	/**
	 * Run the boolean isAllSpaces(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testIsAllSpaces_1()
	{
		String str = "AA";

		boolean result = AppUtils.isAllSpaces(str);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the boolean isAllSpaces(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testIsAllSpaces_2()
		throws Exception {
		String str = " ";

		boolean result = AppUtils.isAllSpaces(str);

		// add additional test code here
		assertEquals(true, result);
	}

	/**
	 * Run the boolean isAlpha(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testIsAlpha_1()
	{
		String str = "ABCDEfgh";

		boolean result = AppUtils.isAlpha(str);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at com.fedex.chronos.irdt.AppUtils.isAlpha(AppUtils.java:89)
		assertTrue(result);
	}

	/**
	 * Run the boolean isAlpha(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testIsAlpha_2()
	{
		String str = "123456";

		boolean result = AppUtils.isAlpha(str);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at com.fedex.chronos.irdt.AppUtils.isAlpha(AppUtils.java:89)
		assertTrue(!result);
	}

	/**
	 * Run the boolean isAlpha(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testIsAlpha_3()
	{
		String str = "123ABC";

		boolean result = AppUtils.isAlpha(str);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at com.fedex.chronos.irdt.AppUtils.isAlpha(AppUtils.java:89)
		assertTrue(result);
	}

	/**
	 * Run the boolean isAlpha(String) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testIsAlpha_4()
		throws Exception {
		String str = "";

		boolean result = AppUtils.isAlpha(str);

		// add additional test code here
		assertEquals(false, result);
	}

	/**
	 * Run the String prependZeros(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testPrependZeros_1()
	{
		String str = "AA";
		int len = 5;

		String result = AppUtils.prependZeros(str, len);

		// add additional test code here
		assertEquals("000AA", result);
	}

	/**
	 * Run the String prependZeros(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testPrependZeros_2()
	{
		String str = null;
		int len = 1;

		String result = AppUtils.prependZeros(str, len);

		// add additional test code here
		assertEquals("0", result);
	}

	/**
	 * Run the String prependZeros(String,int) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Test
	public void testPrependZeros_3()
		throws Exception {
		String str = "1";
		int len = 1;

		String result = AppUtils.prependZeros(str, len);

		// add additional test code here
		assertEquals("1", result);
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@Before
	public void setUp()
	{
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	@After
	public void tearDown()
	{
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 11/21/13 6:22 PM
	 */
	public static void main(String[] args) 
	{
		new org.junit.runner.JUnitCore().run(AppUtilsTest.class);
	}
}