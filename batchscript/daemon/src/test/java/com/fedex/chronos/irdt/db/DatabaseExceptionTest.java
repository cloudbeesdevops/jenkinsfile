package com.fedex.chronos.irdt.db;

import org.junit.*;
import static org.junit.Assert.*;

public class DatabaseExceptionTest {
	DatabaseException fixture1;
	
	private String generateString(DatabaseException de)
	{
		String newline = System.getProperty("line.separator", "\r\n");
        StringBuffer buffer = new StringBuffer();

        buffer.append(newline);
        buffer.append("************************************************************");
        buffer.append(newline);
        buffer.append("DATABASE_ERROR");
        buffer.append(newline);
        buffer.append("Error Code : ");
        buffer.append(de.getErrorCode());
        buffer.append(newline);
        buffer.append("Error Message : ");
        buffer.append(de.getErrorMessage());
        buffer.append(newline);
        buffer.append("Cause : ");
        buffer.append(de.getCause());
        buffer.append(newline);
        buffer.append("************************************************************");
        buffer.append(newline);

        return buffer.toString();
	}
	
	@Test
	public void testDatabaseException_1()
		throws Exception {

		DatabaseException result = new DatabaseException();

		assertNotNull(result);
		assertEquals("", result.getErrorCode());
		assertEquals("", result.getErrorMessage());
		assertEquals(null, result.getCause());
		assertEquals(generateString(result), result.toString());
		assertEquals(null, result.getMessage());
		//assertEquals(null, result.getLocalizedMessage());
	}

	@Test
	public void testDatabaseException_2()
		throws Exception {
		String errorMessage = "errorMessage";

		DatabaseException result = new DatabaseException(errorMessage);
		assertNotNull(result);
		assertEquals("", result.getErrorCode());
		assertEquals("errorMessage", result.getErrorMessage());
		assertEquals(null, result.getCause());
		assertEquals(generateString(result), result.toString());
		assertEquals(null, result.getMessage());
	//	assertEquals(null, result.getLocalizedMessage());
	}

	@Test
	public void testDatabaseException_3()
		throws Exception {
		
		Throwable cause = new Throwable();
		DatabaseException result = new DatabaseException(cause);

		assertNotNull(result);
		assertEquals("", result.getErrorCode());
		assertEquals("", result.getErrorMessage());
		assertEquals("java.lang.Throwable", result.getCause().toString());
		assertEquals(generateString(result), result.toString());
		assertEquals(null, result.getMessage());
		assertEquals(null, result.getLocalizedMessage());
	}

	@Test
	public void testDatabaseException_4()
		throws Exception {
		String errorCode = "errorCode";
		String errorMessage = "errorMessage";

		DatabaseException result = new DatabaseException(errorCode, errorMessage);

		assertNotNull(result);
		assertEquals("errorCode", result.getErrorCode());
		assertEquals("errorMessage", result.getErrorMessage());
		assertEquals(null, result.getCause());
		assertEquals(generateString(result), result.toString());
		assertEquals(null, result.getMessage());
	//	assertEquals(null, result.getLocalizedMessage());
	}

	@Test
	public void testDatabaseException_5()
		throws Exception {
//		String errorCode = "";
//		String errorMessage = "";
//		Throwable cause = new Throwable();
//
//		DatabaseException result = new DatabaseException(errorCode, errorMessage, cause);

		assertNotNull(fixture1);
		assertEquals("Error_Code", fixture1.getErrorCode());
		assertEquals("Err_msg", fixture1.getErrorMessage());
		assertEquals("java.lang.Throwable", fixture1.getCause().toString());
		assertEquals(generateString(fixture1), fixture1.toString());
		assertEquals(null, fixture1.getMessage());
	//	assertEquals(null, result.getLocalizedMessage());
	}

	@Test
	public void testGetCause_1()
		throws Exception {

		Throwable result = fixture1.getCause();

		assertNotNull(result);
		assertEquals(null, result.getCause());
		assertEquals("java.lang.Throwable", result.toString());
		assertEquals(null, result.getMessage());
	//	assertEquals(null, result.getLocalizedMessage());
	}

	@Test
	public void testGetErrorCode_1()
		throws Exception {
		
		String result = fixture1.getErrorCode();
		assertEquals("Error_Code", result);
	}

	@Test
	public void testGetErrorMessage_1()
		throws Exception {

		String result = fixture1.getErrorMessage();
		assertEquals("Err_msg", result);
	}

	@Test
	public void testToString_1()
		throws Exception {
		
		String result = fixture1.toString();
		assertEquals(generateString(fixture1), result);
	}

	@Before
	public void setUp()
		throws Exception {
		 fixture1=new DatabaseException("Error_Code","Err_msg",new Throwable());
		// fixture2=new DatabaseException();
				
	}

	@After
	public void tearDown()
		throws Exception {
	}

	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(DatabaseExceptionTest.class);
	}
}