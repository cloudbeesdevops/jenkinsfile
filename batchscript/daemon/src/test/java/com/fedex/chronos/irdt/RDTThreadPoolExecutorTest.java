package com.fedex.chronos.irdt;

import org.junit.*;
import static org.junit.Assert.*;

public class RDTThreadPoolExecutorTest {
	
	RDTThreadPoolExecutor fixture = null;
	
	@Test
	public void testRDTThreadPoolExecutor_1()
		throws Exception {
		RDTThreadPoolExecutor result = new RDTThreadPoolExecutor();
		assertNotNull(result);
	}

	@Test
	public void testCreateThreadPool_1()
		throws Exception {
		RDTThreadPoolExecutor fixture = new RDTThreadPoolExecutor();
		fixture.setMaxPoolSize(1);
		fixture.setQueueSize(1);
		fixture.setKeepAliveTime(1);
		fixture.setPoolSize(1);
		
		fixture.createThreadPool();
		
		assertNotNull(RDTThreadPoolExecutor.queue);
		assertNotNull(fixture.threadPool);

	}

	@Test
	public void testGetQueueSize_1()
		throws Exception {
		
		RDTThreadPoolExecutor fixture = new RDTThreadPoolExecutor();
		
		fixture.setQueueSize(2000);
		fixture.createThreadPool();

		int result = RDTThreadPoolExecutor.getQueueSize();
		
		

			assertEquals(RDTThreadPoolExecutor.queue.size(), result);
	}

	
	@Ignore
	public void testRunTask_1()
		throws Exception {
		//String s = "empty";
		Thread t = new Thread(){
			public void run() {				
				
				fixture.createThreadPool();
				fixture.setQueueSize(200);
				fixture.createThreadPool();
			}
		};		
		
		fixture.runTask(t);
		
		assertTrue(200 == RDTThreadPoolExecutor.getQueueSize());
		
	}

	@Test
	public void testSetKeepAliveTime_1()
		throws Exception {
		
		fixture.setKeepAliveTime(1000);
		
		assertEquals(fixture.keepAliveTime,1000);
	}

	@Test
	public void testSetMaxPoolSize_1()
		throws Exception {
		RDTThreadPoolExecutor fixture = new RDTThreadPoolExecutor();
		fixture.setMaxPoolSize(100);
		
		
		assertEquals(fixture.maxPoolSize,100);

	}

	@Test
	public void testSetPoolSize_1()
		throws Exception {
		RDTThreadPoolExecutor fixture = new RDTThreadPoolExecutor();
		
		fixture.setPoolSize(100);
		assertEquals(fixture.poolSize,100);
		
	}

	@Test
	public void testSetQueueSize_1()
		throws Exception {
		RDTThreadPoolExecutor fixture = new RDTThreadPoolExecutor();
		
		fixture.setQueueSize(100);
		fixture.createThreadPool();

		assertEquals(fixture.queueSize,100);
		

	}

	@Test
	public void testShutDown_1()
		throws Exception {
		RDTThreadPoolExecutor fixture = new RDTThreadPoolExecutor();
		fixture.setMaxPoolSize(1);
		fixture.setQueueSize(1);
		fixture.setKeepAliveTime(1);
		fixture.setPoolSize(1);
		fixture.createThreadPool();
		
		assertNotNull(fixture.threadPool);
		
		fixture.shutDown();
		
		assertTrue(fixture.threadPool.getTaskCount()==0);

	}

	@Before
	public void setUp()
		throws Exception {
		
		fixture = new RDTThreadPoolExecutor();
		
		
	}

	@After
	public void tearDown()
		throws Exception {
	}

	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(RDTThreadPoolExecutorTest.class);
	}
}