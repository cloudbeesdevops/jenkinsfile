#!/bin/ksh
PROJECT=irdt-batch
cd $SCRIPTS/$PROJECT
nohup $SCRIPTS/$PROJECT/runBatch.sh > /dev/null &

ps -aef | grep RDTCompMain | grep -v grep > /dev/null
status=$?

if [ $status -eq 0 ]
then
  echo " The runBatch started successfully"
else
  echo "Problem starting the runBatch process. Page invoice adjustments oncall support."
fi
