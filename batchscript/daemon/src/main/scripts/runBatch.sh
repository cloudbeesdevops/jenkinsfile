#!/bin/ksh
SOURCE_TEAMS="invadj"
export SOURCE_TEAMS

if [ -f $CFG/chronos.env ]
then
    . $CFG/chronos.env
fi

JAVA_HOME=$PBS_JAVA16_HOME
JAVA=$JAVA_HOME/bin/java

export DB2_HOME=/opt/ibm/db2/V9.7/dsdriver
export DB2_LIB=${DB2_HOME}/java
export ORA_LIB=$ORACLE_HOME/jdbc/lib

LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${DB2_HOME}/bin
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${DB2_HOME}/lib

export LD_LIBRARY_PATH

RDT_BATCH_PROJECT=irdt-batch
export RDT_BATCH_PROJECT

LIBDIR=$LIB/$RDT_BATCH_PROJECT
export LIBDIR

LOG_DIR=$LOGFILES/$RDT_BATCH_PROJECT
export LOG_DIR

${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${RDT_BATCH_PROJECT}/irdt.template ${CFG}/${RDT_BATCH_PROJECT}/irdt.properties
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${RDT_BATCH_PROJECT}/logger.template ${CFG}/${RDT_BATCH_PROJECT}/logger.properties
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${RDT_BATCH_PROJECT}/mcd.template ${CFG}/${RDT_BATCH_PROJECT}/mcd.properties


RDT_BATCH_CONFIG_DIR=$CFG/$RDT_BATCH_PROJECT/
export RDT_BATCH_CONFIG_DIR

CLASSPATH=`echo $LIBDIR/*.jar $LIBDIR/*.zip | tr '= =' '=:='`

CLASSPATH=$CLASSPATH:$ORA_LIB/ojdbc14.jar

echo "CLASSPATH: $CLASSPATH"

export SSLPKGS="-Djava.protocol.handler.pkgs=com.sun.net.ssl.internal.www.protocol"

#export KEYSTORE="-Djavax.net.ssl.keyStore=/opt/fedex/chronos/b/infiles/rs-admin/cacerts"
export KEYSTORE="-Djavax.net.ssl.keyStore=/opt/java/jdk1.5.0_16/jre/lib/security/cacerts"

export KEYSTOREPASS="-Djavax.net.ssl.keyStorePassword=changeit"

#export TRUSTSTORE="-Djavax.net.ssl.trustStore=/opt/fedex/chronos/b/infiles/rs-admin/cacerts"
export TRUSTSTORE="-Djavax.net.ssl.trustStore=/opt/java/jdk1.5.0_16/jre/lib/security/cacerts"

export TRUSTSTOREPASS="-Djavax.net.ssl.trustStorePassword=changeit"
export SSL_ARGS="$SSLPKGS $KEYSTORE $KEYSTOREPASS $TRUSTSTORE $TRUSTSTOREPASS"

$JAVA $SSL_ARGS -cp $CLASSPATH -DRDT_BATCH_CONFIG_DIR=$RDT_BATCH_CONFIG_DIR com.fedex.chronos.irdt.RDTCompMain test ${CFG}/${RDT_BATCH_PROJECT}/ 

retCode=$?

if [ $retCode -ne 0 ]
then
	echo "RDTINVADJ:1200 Exception in Connecting to DB2 from RDTCompMain. - java program failed." >> $BATCH_OVO_ERROR_LOG
fi


