 package com.fedex.chronos.irdt;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.Document;




public class Configuration {
	
	 /**
     * loads properties from the configuration directory
     * 
     * @param aFileName property file name
     * @return the loaded properties
     */
    public static final Properties loadConfigProperties(String aFileName) throws Exception {
        //
        // ensure static variables are initialized
        //
        //initialize();
        //
        // verify that configuration directory was specified
        //
        if (_configurationDirectory == null) {
            throw new Exception("Configuration property not specified: " + IRDT_APP_CONFIG_DIR_PROPERTY);
        }
        //
        // initialize to null in case errors occur
        //
        Properties properties = null;
        //
        // determine filename:
        //
        String fileName = _configurationDirectory + aFileName;
        //
        // attempt to load properties
        //
        FileInputStream stream = null;
        try {
            stream = new FileInputStream(fileName);
            properties = new Properties();
            properties.load(stream);
        } finally {
            if (stream != null) {
                stream.close();
            }
        }
        //
        // return loaded properties
        //
        return properties;
    }
  
    
    public static final void setConfigurationDirectory(String aDirName) throws Exception {
    
    	_configurationDirectory = aDirName;
   
    }
    
    /**
     * loads documents from the configuration directory
     * 
     * @param aFileName the configuration document name
     * @return the configuration document
     */
    public static final Document loadConfigDocument(String aFileName) throws Exception {
        //
        // ensure static variables are initialized
        //
        initialize();
        //
        // verify that configuration directory was specified
        //
        if (_configurationDirectory == null) {
            throw new Exception("Configuration property not specified: " + IRDT_APP_CONFIG_DIR_PROPERTY);
        }
        //
        // determine filename:
        //
        String fileName = _configurationDirectory + aFileName;
        File file = new File(fileName);
        //
        // attempt to load properties
        //
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        builderFactory.setNamespaceAware(true);
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.parse(file);
        //
        // return loaded document
        //
        return document;
    }

    /**
     * loads and caches documents from the classpath
     * 
     * @param aDocument name of document to be loaded
     * @return the document
     * @throws Exception if an error occurs
     */
    public static Document loadDocument(String aDocument) throws Exception {
        //
        // ensure static variables are initialized
        //
        initialize();
        //
        // set document to null in case of errors
        //
        Document document = null;
        //
        // load document or retrieve from cache
        //
        synchronized (_documentLock) {
            document = (Document)_documentTable.get(aDocument);
            if (document == null) {
                StreamSource source = loadResourceStream(aDocument);
                if (source == null) {
                    throw new Exception("Cannot load document: " + aDocument);
                }
                DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
                builderFactory.setNamespaceAware(true);
                DocumentBuilder builder = builderFactory.newDocumentBuilder();
                document = builder.parse(source.getInputStream());
                _documentTable.put(aDocument,document);
            }
        }
        //
        // return the document
        //
        return document;
    }

    
      

    /**
     * initializes varables
     */
    private static void doInitialize() {
        //
        // load installation directory from system properties
        //
    	//_configurationDirectory="src/main/resources/config/";
//    	_configurationDirectory = System.getProperty(INVOICEVERIFICATION_APP_CONFIG_DIR_PROPERTY);
    	_configurationDirectory = "/opt/fedex/chronos/current/infiles/irdt";
    	
        if (_configurationDirectory != null) 
        {
            if (!_configurationDirectory.endsWith(File.separator)) 
            {
                _configurationDirectory += File.separator;
            }
        }
        //
        // create schema factory:
        //
        //_schemaFactory = SchemaFactory.newInstance(
         //       XMLConstants.W3C_XML_SCHEMA_NS_URI
        //);
        //
        // set transformer factory:
        //
        System.setProperty(
                "javax.xml.transform.TransformerFactory",
                "net.sf.saxon.TransformerFactoryImpl"
        );
    }

    /**
     * handles synchronization for initializing variables
     */
    private static void initialize() {
        synchronized (_initializationLock) {
            if (!_initialized) {
                doInitialize();    
                _initialized = true;
            }
        }
    }

    /**
     * loads a resource from the classpath
     * 
     * @param aResource resource to be loaded
     * @return the loaded resource
     */
    protected static StreamSource loadResourceStream(
            String aResource
    )
    throws Exception {
        Class currentClass = Class.forName("com.fedex.xopco.invoiceverification.websvc.Configuration");
        InputStream input = currentClass.getResourceAsStream(aResource);
        if (input == null) {
            return null;
        }
        
        StreamSource result = new StreamSource(input);
        return result;
    }
    
    /**
     * loads a resource from the classpath
     * 
     * @param aResource resource to be loaded
     * @return the loaded resource
     */
    protected static InputStream loadInputStream(
            String aResource
    )
    throws Exception {
        Class currentClass = Class.forName("com.fedex.chronos.irdt.Configuration");
        InputStream input = currentClass.getResourceAsStream(aResource);
        if (input == null) {
            return null;
        }
        
        return input;
    }
 
    /*
    protected StreamSource loadResource(String aResource) throws Exception {
        String result = null;
        InputStream input = null;
        InputStreamReader inputReader = null;
        BufferedReader reader = null;
        try {
            input = ClassLoader.getSystemResourceAsStream(aResource);
            inputReader = new InputStreamReader(input);
            reader = new BufferedReader(inputReader);
            StringBuffer buffer = new StringBuffer();
            String line = reader.readLine();
            while (line != null) {
                buffer.append(line).append("\n");
                line = reader.readLine();
            }
            result = buffer.toString();
        } finally {
             if (reader != null) { reader.close();}
        }
        return result;
 
  */  
    
    /**
     * constant for the configuration directory location property
     */
    protected static String IRDT_APP_CONFIG_DIR_PROPERTY = "IRDT_APP_CONFIG_DIR_PROPERTY";
    
    /**
     * directory where configuration files are installed
     */
    private static String _configurationDirectory = null;
    
    /**
     * lock for document initialization
     */
    private static Object _documentLock = new Object(); 
    
    /**
     * table to contain document resources
     */
    private static HashMap _documentTable = new HashMap();
    
    /**
     * lock for initializing static variables
     */
    private static Object _initializationLock = new Object();
    
    /**
     * indicates whether static variables have been itialized
     */
    private static boolean _initialized = false;
    
    /**
     * lock for schema initialization
     */
    private static Object _schemaLock = new Object(); 
    
    /**
     * table to contain schema resources
     */
    private static HashMap _schemaTable = new HashMap();
    
    /**
     * lock for transformation initialization
     */
    private static Object _templateLock = new Object(); 
    
    /**
     * table to contain transformation resources
     */
    private static HashMap _templateTable = new HashMap();
    


}
