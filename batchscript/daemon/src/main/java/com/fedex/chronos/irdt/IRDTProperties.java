/*
 * BOAProperties.java
 *
 * Created on May 21, 2008, 10:22 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.fedex.chronos.irdt;


import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

/**
 *
 * @author 577500
 */
public final class IRDTProperties  {
    
    public static final String POOL_SIZE			= "POOL_SIZE";

	public static final String MAX_POOL_SIZE		= "MAX_POOL_SIZE";

	public static final String KEEP_ALIVE_TIME		= "KEEP_ALIVE_TIME";
	
	private static final String QUEUE_SIZE			= "QUEUE_SIZE";

	private static final String DB2_ALIAS			= "DB2_ALIAS";

	private static final String DB2_URL				= "DB2_URL";
	
	private static final String DB2_DRIVER			= "DB2_DRIVER";
	
	private static final String DB2_USERNAME		= "DB2_USERNAME";
	
	private static final String DB2_PASSWORD		= "DB2_PASSWORD";
	
	private static final String MAX_SEQ_NBR_CNT		= "MAX_SEQ_NBR_CNT";

	//private static final String ORACLE_WEBSERVICE_WSDL	= "ORACLE_WEBSERVICE_WSDL";

	private static final String enableWsUrl	= "enableWsUrl";
	
	private static final String MAIN_THREAD_WAIT_TIME	= "MAIN_THREAD_WAIT_TIME";




    
    /** Creates a new instance of BOAProperties */
    public IRDTProperties() throws Exception{
        try
        {
            loadProperties();
        }
        catch(Exception ex)
        {
            throw ex;
        }
                
    }
    
    
   /**
	 * *********************************************************************************
	 * 
	 * Loads the properties file
	 * 
	 * @throws Exception
	 * 
	 * *********************************************************************************
	 */
	public static void loadProperties() throws Exception {

		Properties IRDTProperties = Configuration
				.loadConfigProperties(PROPERTIES_FILE);
		if (_IRDTPropertiesMap == null) {
			_IRDTPropertiesMap = new TreeMap(IRDTProperties);
		}
	}
   
	public static int getPoolSize(){
            int val = Integer.parseInt((String) _IRDTPropertiesMap.get(POOL_SIZE));
            return val;
	}
        
	public static int getMaxPoolSize(){
            int val = Integer.parseInt((String) _IRDTPropertiesMap.get(MAX_POOL_SIZE));
            return val;
	}

	public static int getKeepAliveTime(){
            int val = Integer.parseInt((String) _IRDTPropertiesMap.get(KEEP_ALIVE_TIME));
            return val;
	}

	public static int getQueueSize(){
            int val = Integer.parseInt((String) _IRDTPropertiesMap.get(QUEUE_SIZE));
            return val;
	}


	public static int getMaxSeqNbrCount(){
        int val = Integer.parseInt((String) _IRDTPropertiesMap.get(MAX_SEQ_NBR_CNT));
        return val;
	}
	
	
	public static String getDb2Alias(){
            String val = (String) _IRDTPropertiesMap.get(DB2_ALIAS);
            return val;
	}


	public static String getDb2Driver(){
            String val = (String) _IRDTPropertiesMap.get(DB2_DRIVER);
            return val;
	}

	public static String getDb2Url(){
            String val = (String) _IRDTPropertiesMap.get(DB2_URL);
            return val;
	}

	public static String getDb2UserName(){
            String val = (String) _IRDTPropertiesMap.get(DB2_USERNAME);
            return val;
	}

	public static String getDb2Password(){
            String val = (String) _IRDTPropertiesMap.get(DB2_PASSWORD);
            return val;
	}

	public static String getOracleWebserviceWSDL(){
        String val = (String) _IRDTPropertiesMap.get(enableWsUrl);
        return val;
	}

	public static int getMainThreadWaitTime(){
        int val = Integer.parseInt((String) _IRDTPropertiesMap.get(MAIN_THREAD_WAIT_TIME));
        return val;
	}
	


        
    private static Map _IRDTPropertiesMap ;     
    private static final String PROPERTIES_FILE = "irdt.properties";
      
}
