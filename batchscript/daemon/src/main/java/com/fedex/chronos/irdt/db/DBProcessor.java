/* <P>
 * This software is the confidential and proprietary information of FedEx Inc.
 * (Confidential Information). You shall not disclose such confidential
 * information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FedEx.
 */

package com.fedex.chronos.irdt.db;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.fedex.chronos.irdt.AppLogger;


public class DBProcessor
{
  private String queryId = null ;
  private ResultSet resultSet = null ;
  private Connection dbConnection = null ;
  private Statement statement = null ;
  private PreparedStatement preparedStatement = null ;
  private CallableStatement callableStatement = null ;
 
  /*--------------------------------------------------------------------------------------------------*/
  public DBProcessor ()
  {

  }

  public void executeQuery (PreparedStatement preparedStatement) throws Exception
  {
    try
    {
      resultSet = preparedStatement.executeQuery () ;
    }
    catch (SQLException sqlException)
    {
      throw new Exception ("" + sqlException.getErrorCode (), sqlException) ;
    }
  }

  /*--------------------------------------------------------------------------------------------------*/
  public PreparedStatement getPreparedStatement (String query) throws DatabaseException
  {
    try
    {
      preparedStatement = dbConnection.prepareStatement (query) ;
    }
    catch (SQLException sqlException)
    {
      throw new DatabaseException ("" + sqlException.getErrorCode (), sqlException.getMessage (),
        sqlException) ;
    }
    return preparedStatement ;
  }

  /*--------------------------------------------------------------------------------------------------*/
   
   /*--------------------------------------------------------------------------------------------------------*/
   public void openDBConnection (String db2Url,String db2Username,String db2Pwd) throws Exception
   {
    
        try
        {
        	System.out.println("******* db2Url******* <" + db2Url + ">");
        	System.out.println("************ db2Username ********* <" + db2Username + ">");
        	//System.out.println("*************** db2Pwd ******** <" + db2Pwd + ">");

        	//  Class.forName ("COM.ibm.db2.jdbc.app.DB2Driver").newInstance () ;
        	
        	Class.forName ("com.ibm.db2.jcc.DB2Driver").newInstance () ;
        	//dbConnection = DriverManager.getConnection("jdbc:db2:D2TA", "fwcrtest", "fus10n") ;
        	dbConnection = DriverManager.getConnection(db2Url, db2Username, db2Pwd) ;
        	if (AppLogger.isDebugEnabled ()) 
        		AppLogger.debug ("OPEN_A_NEW_DATABASE_CONNECTION") ;
      
        }catch (SQLException sqlException)
        {
        	AppLogger.fatal ("DB2_INACCESSIBLE") ; // OVO TEMPLATE STRING
        	throw new DatabaseException ("Exception is" + sqlException.getErrorCode (), sqlException.getMessage (),
            sqlException) ;
        }
        catch (Exception exception)
        {
        	AppLogger.fatal ("DB2_INACCESSIBLE") ; // OVO TEMPLATE STRING
        	exception.printStackTrace();
        	throw new Exception ("Exception is",exception) ;
        }
        catch (Throwable exception)
        {
        	AppLogger.fatal ("DB2_INACCESSIBLE") ; // OVO TEMPLATE STRING
        	exception.printStackTrace();
        	throw new Exception ("Throwable is",exception) ;
        }
    
   }



  /*--------------------------------------------------------------------------------------------------*/
  public void closeDBConnection ()
  {
    try
    {
      closeResultSet () ;

      closeStatement (preparedStatement) ;
      closeStatement (callableStatement) ;

      if (dbConnection != null)
      {
        dbConnection.close () ;
        if (AppLogger.isDebugEnabled ())
        {
        	AppLogger.debug ("CLOSE_DATABASE_CONNECTION") ;
        }
      }
    }
    catch (SQLException sqlException)
    {
      AppLogger.error ("DBProcessor.closeDBConnection - SQLErrorCode:" + sqlException.getErrorCode () + ": ErrorMessage:" +
      
      sqlException.getMessage (), sqlException) ;
    }
    catch (Throwable th) 
    {
        AppLogger.error ("DBProcessor.closeDBConnection - Exception ErrorMessage:" + th.getMessage(), th) ;
    }
    finally
    {
      resultSet = null ;
      preparedStatement = null ;
      callableStatement = null ;
      dbConnection = null ;
    }
  }

  
  private void closeResultSet ()
  {
    try
    {
      if (resultSet != null)
      {
        resultSet.close () ;
      }
    }
    catch (SQLException sqlException)
    {
      AppLogger.error ("SQLErrorCode:" + sqlException.getErrorCode () + ": ErrorMessage:" +
        sqlException.getMessage (), sqlException) ;
    }
  }

  /*--------------------------------------------------------------------------------------------------*/
  private void closeStatement (Statement statementRef)
  {
    try
    {
      if (statementRef != null)
      {
        statementRef.close () ;
      }
    }
    catch (SQLException sqlException)
    {
      AppLogger.error ("SQLErrorCode:" + sqlException.getErrorCode () + ": ErrorMessage:" +
        sqlException.getMessage (), sqlException) ;
    }
  }

  /*--------------------------------------------------------------------------------------------------*/
 
 
  public String getQueryId ()
  {
    return queryId ;
  }
  
  /*--------------------------------------------------------------------------------------------------*/
  public void commitTransaction ()
  {
    try
    {
      if (dbConnection != null)
      {
        dbConnection.commit () ;
      }  
    }
    catch (SQLException sqlException)
    {
      AppLogger.error ("SQLErrorCode:" + sqlException.getErrorCode () + ": " +
        "ErrorMessage:" + sqlException.getMessage (), sqlException) ;
    }
  }
  
  /*----------------------------------------------------------------------------------------
   * Modified 779437
   * 
   * Method needed to handle oracle status.
   * 
   * 
   * --------------------------------------------------------------------------------------*/
  
  public void openORACLEConnection (String db2Url, String db2Username, String db2Pwd) throws Exception
  {
	 
       try
     {
    	AppLogger.info ("DBProcessor.openORACLEConnection - processing");
       	
       	AppLogger.debug ("DBProcessor.openORACLEConnection - db2Url ::"+db2Url);
       	
    	AppLogger.debug ("DBProcessor.openORACLEConnection - db2Username ::"+db2Username);
       	
      // 	AppLogger.debug ("DBProcessor.openORACLEConnection - db2Pwd ::"+db2Pwd);

       	Class.forName ("oracle.jdbc.OracleDriver").newInstance () ;
     
       	dbConnection = DriverManager.getConnection(db2Url, db2Username, db2Pwd) ;

       	AppLogger.info ("DBProcessor.openORACLEConnection - processed, got Connection");
     
     }
       catch (SQLException sqlException)
       {

    	   AppLogger.error ("DBProcessor.openORACLEConnection - SQLException", sqlException) ; // OVO TEMPLATE STRING
         
    	   throw new DatabaseException ("Exception is" + sqlException.getErrorCode (), sqlException.getMessage (),
           sqlException) ;
       }
   
       catch (Exception exception)
   	  {
     		AppLogger.error ("DBProcessor.openORACLEConnection - Exception",exception) ; // OVO TEMPLATE STRING
    
    		exception.printStackTrace();
     
     		throw new Exception ("Exception is",exception) ;
   	   }
	
  }
}