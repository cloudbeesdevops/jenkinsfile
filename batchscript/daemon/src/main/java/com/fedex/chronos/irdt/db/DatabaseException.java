/* <P>
 * This software is the confidential and proprietary information of FedEx Inc.
 * (Confidential Information). You shall not disclose such confidential
 * information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FedEx.
 */

package com.fedex.chronos.irdt.db;

public class DatabaseException extends Exception
{

  private String errorCode = "" ;
  private String errorMessage = "" ;
  private Throwable cause = null ;

  /*--------------------------------------------------------------------------------------------------*/  
  public DatabaseException ()
  {

  }

  /*--------------------------------------------------------------------------------------------------*/  
  public DatabaseException (String errorMessage)
  {
    this.errorMessage = errorMessage ;
  }

  /*--------------------------------------------------------------------------------------------------*/  
  public DatabaseException (String errorCode, String errorMessage)
  {
    this.errorCode = errorCode ;
    this.errorMessage = errorMessage ;
  }

  /*--------------------------------------------------------------------------------------------------*/  
  public DatabaseException (String errorCode, String errorMessage, Throwable cause)
  {
    this.errorCode = errorCode ;
    this.errorMessage = errorMessage ;
    this.cause = cause ;
  }

  /*--------------------------------------------------------------------------------------------------*/  
  public DatabaseException (Throwable cause)
  {
    this.cause = cause ;
  }

  /*--------------------------------------------------------------------------------------------------*/  
  public Throwable getCause ()
  {
    return cause ;
  }

  /*--------------------------------------------------------------------------------------------------*/  
  public String getErrorCode ()
  {
    return errorCode ;
  }

  /*--------------------------------------------------------------------------------------------------*/  
  public String getErrorMessage ()
  {
    return errorMessage ;
  }

  /*--------------------------------------------------------------------------------------------------*/  
  public String toString ()
  {
    String NEWLINE = System.getProperty ("line.separator", "\r\n") ;
    StringBuffer buffer = new StringBuffer () ;
    
    buffer.append (NEWLINE + "************************************************************" + NEWLINE) ;
    buffer.append ("DATABASE_ERROR" + NEWLINE) ;
    buffer.append ("Error Code : " + errorCode + NEWLINE) ;
    buffer.append ("Error Message : " + errorMessage + NEWLINE) ;
    buffer.append ("Cause : " + cause + NEWLINE) ;
    buffer.append ("************************************************************" + NEWLINE) ;
    
    return buffer.toString () ;
  }
}