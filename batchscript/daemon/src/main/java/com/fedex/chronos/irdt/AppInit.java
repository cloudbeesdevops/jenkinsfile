/* <P>
 * This software is the confidential and proprietary information of FedEx Inc.
 * (Confidential Information). You shall not disclose such confidential
 * information and shall use it only in accordance with the terms of the
 * license agreement you entered into with FedEx.
 */

package com.fedex.chronos.irdt;

import java.io.File;
import java.io.FileInputStream;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;


public class AppInit
{
  private static Properties configProp = null ;
  public static McDUtils mcdUtil = null ;
  private static boolean mcdFailed = false ;
  private static boolean mcdStarted = false ;
  private static Properties mcdProp=null;
  private static Hashtable jmsSessionMap = null ;
  private static Hashtable jmsProducerMap = null ;
  private static Hashtable jmsConnectionMap = null ;
  
  
 
  /*--------------------------------------------------------------------------------------------------*/  
 
  public static void initConfig () throws Exception
  {
	 
    FileInputStream fileInputStream = null ;
    File configFile = null ;
    File mcdConfigFile=null ;
    String loadingFile = null ;    
    String RDT_BATCH_PROPERTY_FILES_ROOT =null  ;
    try
    {
    	System.out.println("in App-Init of rdt-daemon");  
      	RDT_BATCH_PROPERTY_FILES_ROOT = System.getProperty("RDT_BATCH_CONFIG_DIR");   
    	//RDT_BATCH_PROPERTY_FILES_ROOT = System.getProperty("RDT_BATCH_CONFIG_DIR","/home/RDT/chronos/r41.0/devel/build/infiles/irdt-batch/");
    	String configfile1=RDT_BATCH_PROPERTY_FILES_ROOT + "irdt.properties";
        System.out.println("RDT_BATCH_PROPERTY_FILES_ROOT::"+RDT_BATCH_PROPERTY_FILES_ROOT);
       	System.out.println("configfile1::"+configfile1);
        configFile = new File (configfile1) ;
	    mcdConfigFile= new File(RDT_BATCH_PROPERTY_FILES_ROOT +"mcd.properties");
        loadingFile = configFile.getAbsolutePath () ;
        fileInputStream = new FileInputStream (configFile) ;
        System.out.println("after fileinput stream");
      
   try
   {
       configProp = new Properties () ;
       configProp.load (fileInputStream) ;
   }
   finally
   {
      fileInputStream.close () ;
   }
    loadingFile = mcdConfigFile.getAbsolutePath () ;
   fileInputStream = new FileInputStream (mcdConfigFile) ;
    
   try
   {
//     mcdProp = new Properties () ;
//     mcdProp.load (fileInputStream) ;
   }
   finally
   {
     fileInputStream.close () ;
   }
      
      loadingFile = mcdConfigFile.getAbsolutePath () ;
      fileInputStream = new FileInputStream (mcdConfigFile) ;
      loadingFile = RDT_BATCH_PROPERTY_FILES_ROOT + "logger.properties" ;
      PropertyConfigurator.configure (RDT_BATCH_PROPERTY_FILES_ROOT + "logger.properties") ;
      if (AppLogger.isDebugEnabled ()) AppLogger.debug ("\n=========================================") ;
      if (AppLogger.isDebugEnabled ()) AppLogger.debug ("Log4j property file has been successfully loaded.") ;
      if (AppLogger.isDebugEnabled ()) AppLogger.debug ("=========================================\n") ;

    }
    catch (Exception exception)
    {
      System.out.println ("Error in loading the " + loadingFile + " file. Exception in the" + " initConfig method of AppInit. ErrorMessage:" + exception) ;
      throw exception ;
    }
    finally
    {
      fileInputStream = null ;
      configFile = null ;
      mcdConfigFile = null ;
      loadingFile = null ;
    
    }
  }

  /*--------------------------------------------------------------------------------------------------*/  
  public static String getProperty (String key)
  {
    return configProp.getProperty (key) ;
  }


}