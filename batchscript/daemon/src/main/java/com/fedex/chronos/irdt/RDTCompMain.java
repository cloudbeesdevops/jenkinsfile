package com.fedex.chronos.irdt;

import com.fedex.chronos.irdt.thread.RdtServiceMainThread;




public class RDTCompMain {

	
/**
 This will load from a config file
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


 	public static void main(String[] args){
		try{
			System.out.println("Number of args"+args.length);
			String confDir = args[1];
			Configuration.setConfigurationDirectory(confDir);
			AppLogger.debug("Read Confugration from "+ confDir +" Directory");

			RDTThreadPoolExecutor mtpe = new RDTThreadPoolExecutor();
			
			IRDTProperties.loadProperties(); 
			
			mtpe.setPoolSize(IRDTProperties.getPoolSize());
			mtpe.setMaxPoolSize(IRDTProperties.getMaxPoolSize());
			mtpe.setKeepAliveTime(IRDTProperties.getKeepAliveTime());
			mtpe.setQueueSize(IRDTProperties.getQueueSize());
			mtpe.createThreadPool();
			
			
			RdtServiceMainThread rdtThread = new RdtServiceMainThread(mtpe);
			rdtThread.run();

			AppLogger.debug("Thread started");
						
			//			chagCompServiceMain.run(args[0],infile);
		
		}catch(Exception e){
			AppLogger.fatal("General Exception in RDTCompMain",e);
			e.printStackTrace();
			System.exit(1);
		}
	}
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
