package com.fedex.chronos.irdt.thread;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.fedex.chronos.irdt.AppLogger;
import com.fedex.chronos.irdt.IRDTProperties;
import com.fedex.chronos.irdt.RDTThreadPoolExecutor;
import com.fedex.chronos.irdt.db.Constants;
import com.fedex.chronos.irdt.db.DBProcessor;
import com.fedex.rdt.common.AppLog.AppInit;

public class RdtServiceMainThread extends Thread {

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constructor which gets initialised from the main program.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public RdtServiceMainThread(RDTThreadPoolExecutor _executor){
		this.executor = _executor;
	
	}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Init function which creates individual classes for all services specified in config xml.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void init(){
	}

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// create() method which calls other create() methods of all services specified in config xml.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public void create(){
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//eventListener() is called every 1 minute by default until otherwise overridden
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void eventListener(Object o){
	}



	public void run() {
		DBProcessor db = null;
		String db2Url= null;
		String db2Username = null;
		String db2Pwd = null;
		PreparedStatement ps = null;
		
		ResultSet rs = null;
		
		try
		{
			int count = 0;
		
			waitTime = IRDTProperties.getMainThreadWaitTime();
			
			AppLogger.debug("Creating DB connection =  " + count);
			db = new DBProcessor();
			//System.out.println("Now Connecting to DB2 tables");
			AppLogger.debug("Now Connecting to DB2 tables");
			AppInit.initConfig_Rdt_batch();
        	db2Url=AppInit.getProperty("db2Url");
        	db2Username=AppInit.getProperty("db2Username");
        	db2Pwd=AppInit.getProperty("db2Pwd");
        	//db.openDBConnection(db2Url,db2Username,db2Pwd);
			//AppLogger.debug("After Creating DB connection =  " + count);
			
			while(processing)
			{				
				String prevInvoiceNbr = "";
				String requestIdquery=Constants.GETREQUESTID;
				//QC302119 DB2 Exception handling fix starts
				int dbMaxRetryCnt=fetchRetryCnt();
				int maxRetryCnt=0;
				while(maxRetryCnt<dbMaxRetryCnt)
				{
					try
					{
						db.openDBConnection(db2Url,db2Username,db2Pwd);
			        	AppLogger.debug("After Creating DB2 connection =  " + count);
						ps =db.getPreparedStatement (requestIdquery) ;
						rs =ps.executeQuery();
						break;
					}
					catch(Exception ex)
					{
						AppLogger.fatal("RdtServiceMainThread.run  Exception in connecting DB2 ErrorMessage :"+ex.getMessage(),ex);
						maxRetryCnt++;
						System.out.println("In catch exception"+maxRetryCnt);
						sleep(waitTime);
					}
					finally
					{
						
					}
				}
				if(maxRetryCnt==dbMaxRetryCnt)
				{
					System.out.println("In system.exit if");
					System.exit(1);
				}
				//QC302119 DB2 Exception handling fix ends
				
				if (rs.next()) {
				     do {
				        String requestIdNbr = rs.getString("REQUEST_ID_NBR"); 
				        String invoiceNbr = rs.getString("INVOICE_NBR");

						if (prevInvoiceNbr.compareTo(invoiceNbr) != 0) {
							AppLogger.debug("Processing started requestIdNbr: "+requestIdNbr + " invoiceNbr: " + invoiceNbr);
							RdtServiceWorkerThread workerThread = new RdtServiceWorkerThread(requestIdNbr);
							executor.runTask(workerThread);
							prevInvoiceNbr = invoiceNbr ;
							updateStatus(requestIdNbr, db2Url, db2Username, db2Pwd);
							AppLogger.debug("Processing completed requestIdNbr: "+requestIdNbr + " invoiceNbr: " + invoiceNbr);
						}else {
							System.out.println("Skipping requestIdNbr: "+requestIdNbr + " invoiceNbr: " + invoiceNbr);
							AppLogger.debug("Skipping requestIdNbr: "+requestIdNbr + " invoiceNbr: " + invoiceNbr);
						}
				       } while (rs.next());
				     }
				  else {
				     System.out.println("No REQUEST_ID_NBR with status 'Y' present in the database. System will perform check again after 2 minutes");
				}

				if(shutdownFlag)
				{
					executor.shutDown();
					break;
				}
				count++;

				Thread.sleep(waitTime);
			
			}
		}
		catch(java.lang.Exception e)
		{
			AppLogger.fatal("RdtServiceMainThread.run  Exception ErrorMessage :"+e.getMessage(),e);
			e.printStackTrace();
			/*@author-821242
			 *@Desc-Commenting system.exit to allow it to run every 30 mins  
			*/
			//System.exit(1);
		}
		 catch (Throwable th) 
		 {
		        AppLogger.fatal ("RdtServiceMainThread.run - Throwable ErrorMessage :" + th.getMessage(), th) ;
		 }
		 finally
		 {
			 close(rs, ps);			 
		 }
		

	}
	
	//QC302119 DB2 Exception handling fix Starts
	private int fetchRetryCnt()
	{
		int maxRetryCnt=0;
		PreparedStatement pStatement=null;
		ResultSet rsMax=null;
		DBProcessor oraDB=null;
		try
		{
			oraDB = new DBProcessor();
        	oraDB.openORACLEConnection(AppInit.getProperty("oraUrl"),AppInit.getProperty("oraUsername"),AppInit.getProperty("oraPasword"));
			pStatement = oraDB.getPreparedStatement(Constants.maxRetryCntQry);
			rsMax=pStatement.executeQuery();
			if(rsMax.next())
			{
				maxRetryCnt=rsMax.getInt("LOOKUP_VALUE_DESC");
			}
		}
		catch(Exception e)
		{
			AppLogger.error("RdtServiceMainThread.fetchRetryCnt method - Exception", e);
		}
		finally
		{
			if(oraDB!=null)
				oraDB.closeDBConnection();
		}
		return maxRetryCnt;
	}
	//QC302119 DB2 Exception handling fix ends

	private void updateStatus(String requestIdNbr, String db2Url, String db2Username, String db2Pwd) 
	{
		String databaseURL = null; 
		
		String dbUserName = null;
		
		String dbPassword = null;
		
		PreparedStatement statement = null;
		
		PreparedStatement pStatement = null;
		
		ResultSet resultSet = null;
		
		int  statusOID = 0;
		
		DBProcessor oraDB = null;
		
		DBProcessor db2ConProcessor = null;
		
		try
		{
			AppLogger.debug("RdtServiceMainThread.updateStatus method - Processing for requestIdNbr ::"+requestIdNbr);
		
			databaseURL=db2Url;
		
			dbUserName=db2Username;
		
			dbPassword=db2Pwd;
			
			db2ConProcessor = new DBProcessor();
			 
			db2ConProcessor.openDBConnection(databaseURL, dbUserName, dbPassword);
			
			statement = db2ConProcessor.getPreparedStatement(Constants.queryStatusID) ;
			
			statement.setObject(1, requestIdNbr);
			
			resultSet = statement.executeQuery();
			
			if(resultSet.next())
			{
				statusOID = resultSet.getInt("IA_REQUEST_STATUS_OID_NBR");
				
				AppLogger.debug("RdtServiceMainThread.updateStatus method -  Updating oracle record as Complete for ID ::"+statusOID);
				
				oraDB = new DBProcessor();
	        	
				oraDB.openORACLEConnection(AppInit.getProperty("oraUrl"),AppInit.getProperty("oraUsername"),AppInit.getProperty("oraPasword"));
				
				pStatement = oraDB.getPreparedStatement(Constants.queryUpdateRecordStatus);
				
				pStatement.setString(1, "Complete");
				
				pStatement.setInt(2, statusOID);
				
				pStatement.executeUpdate();
				
				AppLogger.debug("RdtServiceMainThread.updateStatus method -  Updated oracle record as Complete for ID ::"+statusOID);
				
			}
			
		}catch(SQLException sqe)
		{
			AppLogger.error("RdtServiceMainThread.updateStatus method - SQLException", sqe);
			
			sqe.printStackTrace();
			
		}
		catch(Throwable th)
		{
			AppLogger.error("RdtServiceMainThread.updateStatus method - Throwable:"+ th.getMessage(), th);
			
			th.printStackTrace();
					
		}finally
		{
			if(oraDB != null)
			{
				oraDB.closeDBConnection();
			}
			if(db2ConProcessor != null )
			{
				db2ConProcessor.closeDBConnection();
			}
			
		}
		
	}

		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//shutDown() thread gets initiated when a kill sginal has been issued
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public static void shutDown(){
		Runtime.getRuntime().addShutdownHook(
			new Thread(){
				public void run(){
				try{
					shutdownFlag = true;
				}catch(Exception e){
					AppLogger.fatal("Failed in RdtServiceMainThread in shutdown"+e.getMessage());
					shutdownFlag = true;
				}
				}
			});
	}		


	
	public static void close(ResultSet rs, PreparedStatement ps)
	{
	        if (rs!=null)
	        {
	                try
	                {
	                        rs.close();

	                }
	                catch(Exception e)
	                {
	                        AppLogger.error("The result set cannot b closed.",e);
	                }
	        }
	        if (ps != null)
	        {
	                try
	                {
	                        ps.close();
	                } catch (Exception e)
	                {
	                	AppLogger.error("The statement cannot be closed.",e);
	                }
	        }	      

	}



  private static int waitTime = 40000;
  private static boolean shutdownFlag = false;
  
  private boolean processing = true;
  private RDTThreadPoolExecutor executor;

}	
