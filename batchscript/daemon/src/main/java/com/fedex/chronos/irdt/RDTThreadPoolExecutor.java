package com.fedex.chronos.irdt;

//http://java.sun.com/j2se/1.5.0/docs/api/java/util/concurrent/ThreadPoolExecutor.html

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

 
public class RDTThreadPoolExecutor
{
    public int poolSize = 4;
 
    int maxPoolSize = 10;
 
    long keepAliveTime = 10;
 
    int queueSize = 2000;
    
    ThreadPoolExecutor threadPool = null;
 
    public static ArrayBlockingQueue queue; 
 

    public void runTask(Runnable task)
    {
        try
        {
	    	// System.out.println("Task count.."+threadPool.getTaskCount() );
	        // System.out.println("Queue Size before assigning the
	        // task.."+queue.size() );
	        threadPool.execute(task);
	        //System.out.println("Queue Size after assigning the task.."+queue.size() );
	        // System.out.println("Pool Size after assigning the
	        // task.."+threadPool.getActiveCount() );
	        // System.out.println("Task count.."+threadPool.getTaskCount() );
	        //System.out.println("Task count.." + queue.size());
        }
        catch(Exception ex)
        {
        	System.out.println(ex);
        }
 
    }
 
    public static int getQueueSize()
	{
		return queue.size();
	}
    
    public void shutDown()
    {
        threadPool.shutdown();
    }
 
    public void setPoolSize(int _poolSize)
	{
		this.poolSize = _poolSize;
	}
 
    public void setMaxPoolSize(int _maxPoolSize)
	{
		this.maxPoolSize = _maxPoolSize;
	}
    
	public void setKeepAliveTime(int _keepAliveTime)
	{
		this.keepAliveTime = _keepAliveTime;
	}

	public void setQueueSize(int _queueSize)
	{
		this.queueSize = _queueSize;
	}

    public void createThreadPool()
    {
    	queue = new ArrayBlockingQueue(queueSize);
        threadPool = new ThreadPoolExecutor(poolSize, maxPoolSize, keepAliveTime, TimeUnit.SECONDS, queue);
    }

 
    public static void main(String args[])
    {
        RDTThreadPoolExecutor mtpe = new RDTThreadPoolExecutor();
        
		
		// start first one
		for(int i=0;i<10;i++)
		{
			//mtpe.runTask(new DoNothingWorker(i));
		}

  
		mtpe.shutDown();
    }
 
}

