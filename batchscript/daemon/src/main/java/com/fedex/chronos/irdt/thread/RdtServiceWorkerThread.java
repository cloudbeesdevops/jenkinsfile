package com.fedex.chronos.irdt.thread;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.xmlbeans.XmlObject;

import com.fedex.chronos.irdt.AppLogger;
import com.fedex.chronos.irdt.RDTThreadPoolExecutor;
import com.fedex.chronos.irdt.db.Constants;
import com.fedex.chronos.irdt.db.DBProcessor;
import com.fedex.grcc.schemas.ErrorDescriptor;
import com.fedex.grcc.schemas.impl.ArMaintenanceResponseDocumentImpl;
import com.fedex.rdt.common.AppLog.AppInit;
import com.fedex.rdt.common.webservice.ArWebservice;


public class RdtServiceWorkerThread extends Thread {


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Constructor which gets initialised from the main program.
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*	public RdtServiceWorkerThread(String xmlMsg){
		this.xmlMsg = xmlMsg;
	} */ 
	
	public RdtServiceWorkerThread(String reqId){
		this.reqId = reqId;
	}

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Init function which creates individual classes for all services specified in config xml.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void init(){
	}

	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// create() method which calls other create() methods of all services specified in config xml.
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public void create(){
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//eventListener() is called every 1 minute by default until otherwise overridden
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void eventListener(Object o){
	}



	private boolean isValidToProcess(DBProcessor db, String reqId) {
		boolean isValid = false;
	    PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			pstmt = db.getPreparedStatement(Constants.GETREQUESTDATA) ;
			
			pstmt.setObject(1, reqId);
			
			rs = pstmt.executeQuery();

			isValid = rs.next();
			
			if (pstmt != null) {
				pstmt.close();
			}

		} catch (Throwable th) {
		    AppLogger.debug("Database Exception in isValidToProcess:" + reqId);
		}
		
    	return isValid;
	}

	public void update12D(DBProcessor db, String reqId, boolean completed) throws Exception {
	       
		String strUpdate= null;
		if (completed) {
			strUpdate = Constants.UPDATE12D;
		} else {
			strUpdate=Constants.UPDATE12D_PROCESSING;	
		}

	    PreparedStatement pstmt = null;
	    int numUpd;

	    try {
	    	AppLogger.debug("in update12D with completion flag - " + completed + " for requestId - " + reqId);
	    	pstmt =db.getPreparedStatement (strUpdate) ;
			pstmt.setString(1,reqId);
			numUpd = pstmt.executeUpdate();
		    AppLogger.debug("numUpd=="+numUpd);
	    } finally {
	    	db.commitTransaction();
			if (pstmt != null) {
				pstmt.close();
			}
		}
	//return numberOfRowsUpdated;
			
	}
	
	
	public void run(){
		DBProcessor db = null;
		String db2Url= null;
		String db2Username = null;
		String db2Pwd = null;
		
		try
		{
	    	db2Url=AppInit.getProperty("db2Url");
	    	db2Username=AppInit.getProperty("db2Username");
	    	db2Pwd=AppInit.getProperty("db2Pwd");
	    	db = new DBProcessor();
	    	db.openDBConnection(db2Url,db2Username,db2Pwd);
	    	
	    	if (isValidToProcess(db, reqId)) {
				update12D(db, reqId, false);
				double requestnumber = (Math.random() * 10000);
				System.out.println("Processing thread id = " + requestnumber +" and queue size is: " + RDTThreadPoolExecutor.getQueueSize());
				AppLogger.debug("Request Id == "+reqId);
					
				//Call oracle AR for Reuest ID update
				ArRebillServiceNew(reqId);
				
				AppLogger.debug("arWebserviceSuccessResponse: " + arWebserviceSuccessResponse);
				
				if (arWebserviceSuccessResponse) {
					update12D(db, reqId, true);
				}
	    	}
	    	
		}
		catch(java.lang.Exception e)
		{
			AppLogger.fatal("In RdtServiceWorkerThread::execute Exception:",e);
			e.printStackTrace();
		}
		catch(java.lang.Throwable e)
		{
			AppLogger.fatal("In RdtServiceWorkerThread::execute - Throwable:",e);
			e.printStackTrace();
		}
	}


			
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//shutDown() thread gets initiated when a kill sginal has been issued
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void shutDown() throws Exception{
	
	
	}
	
	
	
	public void ArRebillServiceNew(String DB_REQ_ID)
	{
		System.out.println("Calling Ar Common webservice from RDT service worker thread.." );
		AppLogger.debug("Calling Ar Common webservice from RDT service worker thread.." );
		XmlObject arMaintResponse = null;
		try
		{									
			
			ArWebservice arWebservice = new ArWebservice();
			
			long startTime = System.currentTimeMillis() ;
									
			arMaintResponse = arWebservice.ArMaintenanceProcessor_RDT_BATCH(DB_REQ_ID);
																
			System.out.println("arMaintResponse=="+arMaintResponse );	
			AppLogger.debug("arMaintResponse=="+arMaintResponse );
			
			System.out.println("Response received in RDT Daemon batch" );	
			AppLogger.debug("Response received in RDT Daemon batch" );
			
			ArMaintenanceResponseDocumentImpl resImpl = (ArMaintenanceResponseDocumentImpl)arMaintResponse;
			com.fedex.grcc.schemas.ResponseType arMaintenanceResponseType = resImpl.getArMaintenanceResponse() ;
			com.fedex.grcc.schemas.ReplyHeader replyHeader = arMaintenanceResponseType.getHeader() ;
			
			ErrorDescriptor[] errorDescriptorArray =  arMaintenanceResponseType.getHeader().getErrorStatusArray();
			if (errorDescriptorArray != null && errorDescriptorArray.length != 0) {
				arWebserviceSuccessResponse = false;
			}
			
			if((arMaintResponse instanceof ArMaintenanceResponseDocumentImpl) == false)
			{
				System.out.println("RDT Daemon-instance Error occured");
				AppLogger.error ("ErrorMessage:-AR Batch MIA Error==Reply Object Instance Error occured ");
			}
			if(arMaintResponse == null)
			{
				AppLogger.error ("ErrorMessage: arMaintResponse in rdt-batch is NULL") ;				
			}
			
		}	
		catch (Exception exception)
		{
			AppLogger.error ("ErrorMessage:Exception Occured while calling ArService for RDT Daemon batch process " + exception, exception) ;
		}		
	} 

	private String xmlMsg;
	private String reqId;
	private boolean arWebserviceSuccessResponse = true;
	
}	
