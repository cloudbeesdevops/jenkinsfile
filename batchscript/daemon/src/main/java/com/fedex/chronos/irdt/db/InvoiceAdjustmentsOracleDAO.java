package com.fedex.chronos.irdt.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import com.fedex.chronos.irdt.AppLogger;
import com.fedex.rdt.common.AppLog.AppInit;

public class InvoiceAdjustmentsOracleDAO {
	
	public InvoiceAdjustmentsOracleDAO() throws Exception{
		
		
			AppInit.initConfig_Rdt_batch();
			
			String oraUrl=AppInit.getProperty("oraUrl");
			
			String oraUsername=AppInit.getProperty("oraUsername");
			
			String oraPasword=AppInit.getProperty("oraPasword");
			
			Class.forName("oracle.jdbc.OracleDriver").newInstance ();
			
			dbConnection = DriverManager.getConnection(oraUrl, oraUsername, oraPasword);
			
	}

	private void closeConnection() throws Exception{
		
		if ( dbConnection != null){
			
			dbConnection.close();
			
		}
		
	}
	
	public void updateBatchStatus( String reqId ) throws Exception {
		
		
		try{
		
			dbConnection.setAutoCommit(false);
		
			updateBatchDetail(reqId);

			updateSingleAdjustmentRequestStatus(reqId);

			dbConnection.commit();
		
			dbConnection.setAutoCommit(true);
		}
		
		catch (Exception e) {
			
			throw e;
		}
		finally{

			closeConnection();
		}
	}

	private void updateBatchDetail(String reqId) throws Exception {
		
		String queryUpdateBatchDetail = "UPDATE INVADJ_SCHEMA.IA_BATCH_DETAIL SET " +
  		"REQUEST_STATUS_NM = 'Success', LAST_UPDATE_TMSTP = SYSTIMESTAMP" +
  		" WHERE MAINFRAME_REQ_ID_NBR = ?";
		
		String queryUpdateBatchSummary = "UPDATE INVADJ_SCHEMA.IA_BATCH_SUMMARY SET " +
  		"SUCCESSFUL_REQUEST_QTY = NVL(SUCCESSFUL_REQUEST_QTY,0) + 1, " +
  		"FAILURE_REQUEST_QTY = NVL(FAILURE_REQUEST_QTY,1) - 1, " +
  		"LAST_UPDATE_TMSTP = SYSTIMESTAMP " +
  		"WHERE BATCH_SUMMARY_NBR = " +
  		"( SELECT BATCH_SUMMARY_NBR FROM INVADJ_SCHEMA.IA_BATCH_DETAIL " +
  		"WHERE MAINFRAME_REQ_ID_NBR = ? )";
		
		PreparedStatement psDetail = null;
		
		PreparedStatement psSummary = null;
		
		AppLogger.debug("query: " + queryUpdateBatchDetail);
		AppLogger.debug("theArguments: " +  reqId);
		
		AppLogger.debug("query: " + queryUpdateBatchSummary);
		AppLogger.debug("theArguments: " +  reqId);
		
		try{
			psDetail = dbConnection.prepareStatement(queryUpdateBatchDetail);
			
			psDetail.setInt(1, Integer.parseInt(reqId));
			
			psDetail.execute();
			
			psSummary = dbConnection.prepareStatement(queryUpdateBatchSummary);
			
			psSummary.setInt(1, Integer.parseInt(reqId));
			
			psSummary.execute();
		}
		catch (Exception e) {
			throw e;
		}
		finally{
			if ( psDetail != null )
			{
				try {
					psDetail.close();
				} catch (Exception e) {
					throw e;
				}
			}
			if ( psSummary != null )
			{
				try {
					psSummary.close();
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
	}

	private void updateSingleAdjustmentRequestStatus(String reqId) throws Exception {
		
		String query = "UPDATE INVADJ_SCHEMA.SINGLE_ADJ_REQ_STATUS SET " +
  		"REQUEST_STATUS_NM = 'Success', " +
  		"LAST_UPDATE_TMSTP = SYSTIMESTAMP " +
  		"WHERE MAINFRAME_REQ_ID_NBR = ?";
		
		AppLogger.debug("query: " + query);
		AppLogger.debug("theArguments: " +  reqId);
		
		PreparedStatement ps = null;
		
		try{
			ps = dbConnection.prepareStatement(query);
			
			ps.setInt(1, Integer.parseInt(reqId));
			
			ps.execute();
		}
		catch (Exception e) {
			throw e;
		}
		finally{
			if (ps != null)
			{
				try {
					ps.close();
				} catch (Exception e) {
					throw e;
				}
			}
		}
		
	}

	
	private Connection dbConnection = null ;


}
