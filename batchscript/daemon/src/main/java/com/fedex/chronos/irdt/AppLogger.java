package com.fedex.chronos.irdt;

import java.util.Properties;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public final class AppLogger {
    
  /*--------------------------------------------------------------------------------------------------*/
  public static void debug (String loggerText)
  {
      Logger logger = getLogger();
      logger.log (wrapperName, Level.DEBUG, getHashCode () + loggerText + NEWLINE, null) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void debug (String loggerText, Throwable throwable)
  {
    Logger logger = getLogger();
    if (logger.isDebugEnabled()) {
        logger.log (wrapperName, Level.DEBUG, getHashCode () + loggerText + NEWLINE, null) ;
        printStackTrace (Level.DEBUG, throwable);
    }
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void info (String loggerText)
  {
      Logger logger = getLogger();
      logger.log (wrapperName, Level.INFO, getHashCode () + loggerText + NEWLINE, null) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void info (String loggerText, Throwable throwable) {
      Logger logger = getLogger();
      logger.log (wrapperName, Level.INFO, getHashCode () + loggerText + NEWLINE, null) ;
      printStackTrace (Level.INFO, throwable) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void warn (String loggerText) {
      Logger logger = getLogger();
      logger.log (wrapperName, Level.WARN, getHashCode () + loggerText + NEWLINE, null) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void warn (String loggerText, Throwable throwable) {
      Logger logger = getLogger();
      logger.log (wrapperName, Level.WARN, getHashCode () + loggerText + NEWLINE, null) ;
      printStackTrace (Level.WARN, throwable) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void error (String loggerText) {
      Logger logger = getLogger();
      logger.log (wrapperName, Level.ERROR, getHashCode () + loggerText + NEWLINE, null) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void error (Throwable throwable) {
    printStackTrace (Level.ERROR, throwable) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void error (String loggerText, Throwable throwable) {
      Logger logger = getLogger();
      logger.log (wrapperName, Level.ERROR, getHashCode () + loggerText + NEWLINE, null) ;
      printStackTrace (Level.ERROR, throwable) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void fatal (String loggerText) {
      Logger logger = getLogger();
      logger.log (wrapperName, Level.FATAL, getHashCode () + loggerText + NEWLINE, null) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static void fatal (String loggerText, Throwable throwable) {
      Logger logger = getLogger();
      logger.log (wrapperName, Level.FATAL, getHashCode () + loggerText + NEWLINE, null) ;
      printStackTrace (Level.FATAL, throwable) ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static boolean isDebugEnabled () {
      Logger logger = getLogger();
      return logger.isDebugEnabled () ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  public static boolean isInfoEnabled () {
      Logger logger = getLogger();
      return logger.isInfoEnabled () ;
  }

  /*--------------------------------------------------------------------------------------------------*/
  private static void printStackTrace (Level loggerLevel, Throwable throwable)
  {
      Logger logger = getLogger();
    try
    {
      boolean cfStackTraceStarted = false ;

      StringBuffer stackTraceBuffer = new StringBuffer (NEWLINE + "EXCEPTION STACK TRACE:") ;
      stackTraceBuffer.append (NEWLINE) ;

      StackTraceElement[] stackTraceElementArray = throwable.getStackTrace () ;

      for (int count = 0 ; count < stackTraceElementArray.length ; count++)
      {
        StackTraceElement stackTraceElement = stackTraceElementArray [count] ;

        String className = stackTraceElement.getClassName () ;

        if (className != null && className.startsWith ("com.fedex."))
        {
          stackTraceBuffer.append ("  at " + stackTraceElement.toString ()) ;
          stackTraceBuffer.append (NEWLINE) ;

          if (cfStackTraceStarted == false)
          {
            cfStackTraceStarted = true ;
          }
        }
        else
        {
          if (cfStackTraceStarted == false)
          {
            stackTraceBuffer.append ("  at " + stackTraceElement.toString ()) ;
            stackTraceBuffer.append (NEWLINE) ;
          }
          else
          {
            break ;
          }
        }
      }

      logger.log (wrapperName, loggerLevel, getHashCode () + stackTraceBuffer.toString () + NEWLINE, null) ;
    }
    catch (Exception exception)
    {
      logger.log (wrapperName, loggerLevel, null, throwable) ;
    }
  }

  /*--------------------------------------------------------------------------------------------------*/
  private static String getHashCode ()
  {
    String hashCode = "" ;

    try
    {
      int hcode = Thread.currentThread ().hashCode () ;
      hcode = (hcode < 0 ? hcode * -1 : hcode) ;
      hashCode = "[" + hcode + "] - " + NEWLINE ;
    }
    catch (Exception exception)
    {
      error ("Exception while getting the thread hashcode. Error Message:" + exception, exception) ;
    }

    return hashCode ;
  }

    /**
     * returns a static reference to the logger; should reload properties 
     * and instatiate a new logger if the application is redeployed
     * 
     * @return the logger
     */
    private static Logger getLogger() {
        synchronized (_loggerLock) {
            if (_logger == null) {
                Properties properties = null;
                try {
                    properties = Configuration.loadConfigProperties("logger.properties");
                    PropertyConfigurator.configure(properties);
                    _logger = Logger.getLogger("irdt");
               } catch (Throwable t) {
                    t.printStackTrace();
               }
            }
        }
        return _logger;
    }

    private static final String NEWLINE = "\n" ;

    private static Logger _logger = null;

    private static Object _loggerLock = new Object();
    
    private static String wrapperName = "com.fedex.chronos.irdt.AppLogger" ;

}