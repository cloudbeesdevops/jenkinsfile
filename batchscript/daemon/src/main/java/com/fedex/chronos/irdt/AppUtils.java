/*
 * AppUtils.java
 *
 * Created on June 12, 2007, 3:35 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.fedex.chronos.irdt;


public class AppUtils {

	/** Creates a new instance of AppUtils */
	public AppUtils() {
	}

	/*---------------------------------------------------------------------------------------------------*/
	public static String getHostname()
	{
		String host = "unknown";
		try {
			host = java.net.InetAddress.getLocalHost().getHostName();
			int pos = host.indexOf('.');
		} catch (Exception ex) {
		}
		return host;
	}
	/*---------------------------------------------------------------------------------------------------*/
	public static boolean isAllSpaces(String str)
	{
		int strLen = (str.trim()).length();
		if(strLen==0)
			return true;
		else
			return false;
	}

	/*---------------------------------------------------------------------------------------------------*/
	public static String appendSpaces(String str, int len)
	{
		int strLen = (str.trim()).length();

		if (strLen >= len)
			return str.trim() ;

		StringBuffer buf = new StringBuffer(str.trim());
		for (int i = strLen ; i < len; i++) {
			buf.append(" ");
		}
		return buf.toString();
	}

	
	/*---------------------------------------------------------------------------------------------------*/
	public static String prependZeros(String str, int len)
	{
		StringBuffer buf ;
		int strLen ;
		if (str == null) {
			strLen = 0 ;
			buf = new StringBuffer() ;
		} else {
			strLen = (str.trim()).length();
			buf = new StringBuffer(str.trim());
		}
		if (strLen >= len)
			return str.trim () ; // Let it fail. The data will be shifted and will be investigated why it was not captured at client.

		for (int i = 0; i < len - strLen ; i++) {
			buf.insert(i, "0");
		}
		// if (AppLogger.isDebugEnabled ()) AppLogger.debug ("In Rerate::prependZeros before retrun :  '" + buf.toString () + "' length '" + len + "' strlen '" + strLen + "' seq '" + seq + "'") ;
		// seq ++ ;
		return buf.toString();
	}

	/*---------------------------------------------------------------------------------------------------*/

	public static boolean isAlpha(String str)
	{
		boolean blnAlpha = false;

		char chr[] = null;
		if(str != null)
			chr = str.toCharArray();

		for(int i=0; i<chr.length; i++)
		{
			if((chr[i] >= 'A' && chr[i] <= 'Z') || (chr[i] >= 'a' && chr[i] <= 'z'))
			{
				blnAlpha = true;
				break;
			}
		}
		return (blnAlpha);
	}
}
