#!/bin/sh

correct_app_cfg_folder_permissions() {
	
	chmod -R 775 $APP_DEPLOY_DIR/*/cfg
	chmod 775 $APP_DEPLOY_DIR/*/cfg/*
	chmod 775 $APP_DEPLOY_DIR/*/cfg/*/*
	chmod 775 $APP_DEPLOY_DIR/*/cfg/invoice-adjustments
	chmod 775 $APP_DEPLOY_DIR/*/cfg/invoice-adjustments/invoice-adjustments-ear
	chmod 775 $APP_DEPLOY_DIR/*/cfg/invoice-adjustments/invoice-adjustments-webapp
	chmod 775 $APP_DEPLOY_DIR/*/cfg/invoice-adjustments/invoice-adjustments-batch
	chmod 775 $APP_DEPLOY_DIR/*/cfg/rdt-dom-adjustments/properties
	chmod 775 $APP_DEPLOY_DIR/*/cfg/rdt-dom-adjustments/properties/operations
	chmod 775 $APP_DEPLOY_DIR/*/cfg/rdt-mug-websvc/properties
	chmod 775 $APP_DEPLOY_DIR/*/cfg/rdt-mug-websvc/properties/operations
	chmod 444 $APP_DEPLOY_DIR/*/cfg/env/*

}

correct_var_folder_permissions() {

	chmod 775 data
	chmod 775 data/archive
	chmod 775 data/archive/45D
	chmod 775 data/archive/45D/domadj
	chmod 775 data/archive/45D/edi
	chmod 775 data/archive/45D/invadj_webservice_domain
	chmod 775 data/archive/45D/massAdj
	chmod 775 data/archive/45D/massAdjAUS
	chmod 775 data/archive/45D/massAdjBLT
	chmod 775 data/archive/45D/massAdjCAN
	chmod 775 data/archive/45D/massAdjGUM
	chmod 775 data/archive/45D/massAdjHKG
	chmod 775 data/archive/45D/massAdjIDT
	chmod 775 data/archive/45D/massAdjCIP
	chmod 775 data/archive/45D/massAdjJPN
	chmod 775 data/archive/45D/massAdjKOR
	chmod 775 data/archive/45D/massAdjMAC
	chmod 775 data/archive/45D/massAdjMPC
	chmod 775 data/archive/45D/massAdjMYS
	chmod 775 data/archive/45D/massAdjNRD
	chmod 775 data/archive/45D/massAdjNZL
	chmod 775 data/archive/45D/massAdjPHI
	chmod 775 data/archive/45D/massAdjSGP
	chmod 775 data/archive/45D/massAdjTHT
	chmod 775 data/archive/45D/massAdjTWN
	chmod 775 data/archive/45D/massAdjUKD
	chmod 775 data/archive/45D/massAdjVNM
	chmod 775 data/archive/45D/rdt-batch
	chmod 775 data/archive/45D/rdt_dom_adjustments_domain
	chmod 775 data/archive/45D/rdt_mdbservice_domain
	chmod 775 data/archive/45D/rdt_webservice_domain
	chmod 775 data/archive/45D/rebills
	chmod 775 data/archive/45D/staticdataload
	chmod 775 data/infiles
	chmod 775 data/infiles/invadj-rdt-admin
	chmod 775 data/infiles/invadj-rdt-admin/common
	chmod 775 data/infiles/irdt-batch
	chmod 775 data/infiles/rdt-batch
	chmod 775 data/sxfr/infiles
	chmod 775 data/sxfr/infiles/adjautomation
	chmod 775 data/sxfr/infiles/edi
	chmod 775 data/sxfr/infiles/massupload
	chmod 775 data/sxfr/infiles/massupload/china
	chmod 775 data/sxfr/infiles/massAdj
	chmod 775 data/sxfr/infiles/massAdjAUS
	chmod 775 data/sxfr/infiles/massAdjBLT
	chmod 775 data/sxfr/infiles/massAdjCAN
	chmod 775 data/sxfr/infiles/massAdjGUM
	chmod 775 data/sxfr/infiles/massAdjHKG
	chmod 775 data/sxfr/infiles/massAdjIDT
	chmod 775 data/sxfr/infiles/massAdjCIP
	chmod 775 data/sxfr/infiles/massAdjJPN
	chmod 775 data/sxfr/infiles/massAdjKOR
	chmod 775 data/sxfr/infiles/massAdjMAC
	chmod 775 data/sxfr/infiles/massAdjMPC
	chmod 775 data/sxfr/infiles/massAdjMYS
	chmod 775 data/sxfr/infiles/massAdjNRD
	chmod 775 data/sxfr/infiles/massAdjNZL
	chmod 775 data/sxfr/infiles/massAdjPHI
	chmod 775 data/sxfr/infiles/massAdjSGP
	chmod 775 data/sxfr/infiles/massAdjTHT
	chmod 775 data/sxfr/infiles/massAdjTWN
	chmod 775 data/sxfr/infiles/massAdjUKD
	chmod 775 data/sxfr/infiles/massAdjVNM
	chmod 775 data/sxfr/infiles/rebillautomation
	chmod 775 data/sxfr/infiles/rebills
	chmod 775 data/sxfr/infiles/staticdataload
	chmod 775 data/outfiles
	chmod 775 data/outfiles/rebills
	chmod 775 data/outfiles/rebills/reject
	chmod 775 data/sxfr/outfiles
	chmod 775 data/sxfr/outfiles/adjautomation
	chmod 775 data/sxfr/outfiles/domadj
	chmod 775 data/sxfr/outfiles/massupload
	chmod 775 data/sxfr/outfiles/massupload/china
	chmod 775 data/sxfr/outfiles/edi
	chmod 775 data/sxfr/outfiles/massAdj
	chmod 775 data/sxfr/outfiles/massAdj/reject
	chmod 775 data/sxfr/outfiles/massAdjAUS
	chmod 775 data/sxfr/outfiles/massAdjAUS/reject
	chmod 775 data/sxfr/outfiles/massAdjBLT
	chmod 775 data/sxfr/outfiles/massAdjBLT/reject
	chmod 775 data/sxfr/outfiles/massAdjCAN
	chmod 775 data/sxfr/outfiles/massAdjCAN/reject
	chmod 775 data/sxfr/outfiles/massAdjGUM
	chmod 775 data/sxfr/outfiles/massAdjGUM/reject
	chmod 775 data/sxfr/outfiles/massAdjHKG
	chmod 775 data/sxfr/outfiles/massAdjHKG/reject
	chmod 775 data/sxfr/outfiles/massAdjIDT
	chmod 775 data/sxfr/outfiles/massAdjIDT/reject
	chmod 775 data/sxfr/outfiles/massAdjCIP
	chmod 775 data/sxfr/outfiles/massAdjCIP/reject
	chmod 775 data/sxfr/outfiles/massAdjJPN
	chmod 775 data/sxfr/outfiles/massAdjJPN/reject
	chmod 775 data/sxfr/outfiles/massAdjKOR
	chmod 775 data/sxfr/outfiles/massAdjKOR/reject
	chmod 775 data/sxfr/outfiles/massAdjMAC
	chmod 775 data/sxfr/outfiles/massAdjMAC/reject
	chmod 775 data/sxfr/outfiles/massAdjMPC
	chmod 775 data/sxfr/outfiles/massAdjMPC/reject
	chmod 775 data/sxfr/outfiles/massAdjMYS
	chmod 775 data/sxfr/outfiles/massAdjMYS/reject
	chmod 775 data/sxfr/outfiles/massAdjNRD
	chmod 775 data/sxfr/outfiles/massAdjNRD/reject
	chmod 775 data/sxfr/outfiles/massAdjNZL
	chmod 775 data/sxfr/outfiles/massAdjNZL/reject
	chmod 775 data/sxfr/outfiles/massAdjPHI
	chmod 775 data/sxfr/outfiles/massAdjPHI/reject
	chmod 775 data/sxfr/outfiles/massAdjSGP
	chmod 775 data/sxfr/outfiles/massAdjSGP/reject
	chmod 775 data/sxfr/outfiles/massAdjTHT
	chmod 775 data/sxfr/outfiles/massAdjTHT/reject
	chmod 775 data/sxfr/outfiles/massAdjTWN
	chmod 775 data/sxfr/outfiles/massAdjTWN/reject
	chmod 775 data/sxfr/outfiles/massAdjUKD
	chmod 775 data/sxfr/outfiles/massAdjUKD/reject
	chmod 775 data/sxfr/outfiles/massAdjVNM
	chmod 775 data/sxfr/outfiles/massAdjVNM/reject
	chmod 775 data/sxfr/outfiles/rebillautomation
	chmod 775 logs
	chmod 775 logs/domadj
	chmod 775 logs/edi
	chmod 775 logs/invoice-adjustments
	chmod 775 logs/invoice-adjustments/invoice-adjustments-ear
	chmod 775 logs/invoice-adjustments/invoice-adjustments-webapp
	chmod 775 logs/irdt-batch
	chmod 775 logs/massAdj
	chmod 775 logs/massAdjAUS
	chmod 775 logs/massAdjBLT
	chmod 775 logs/massAdjCAN
	chmod 775 logs/massAdjGUM
	chmod 775 logs/massAdjHKG
	chmod 775 logs/massAdjIDT
	chmod 775 logs/massAdjCIP
	chmod 775 logs/massAdjJPN
	chmod 775 logs/massAdjKOR
	chmod 775 logs/massAdjMAC
	chmod 775 logs/massAdjMPC
	chmod 775 logs/massAdjMYS
	chmod 775 logs/massAdjNRD
	chmod 775 logs/massAdjNZL
	chmod 775 logs/massAdjPHI
	chmod 775 logs/massAdjSGP
	chmod 775 logs/massAdjTHT
	chmod 775 logs/massAdjTWN
	chmod 775 logs/massAdjUKD
	chmod 775 logs/massAdjVNM
	chmod 775 logs/rdt-1source-ear
	chmod 775 logs/rdt-batch
	chmod 775 logs/rdt-c3cache
	chmod 775 logs/rdt-c3cache/rdt-c3cache-ear
	chmod 775 logs/rdt-dom-adjustments
	chmod 775 logs/rdt-mug-websvc
	chmod 775 logs/rdt-mdbservice
	chmod 775 logs/rdt-webservice
	chmod 775 logs/rebills
	chmod 775 logs/revenue.desktop.server
	chmod 775 logs/staticdataload
	chmod 775 logs/purge
	chmod 775 logs/massupload

}

clear

export TS=`date +%Y%m%d%H%M%S`

# >> Set the default branch.
#    ~~~~~~~~~~~~~~~~~~~~~~

#export DEFAULT_BRANCH=RB-R59.CL1701
export DEFAULT_BRANCH=RB-r62.CL1802
echo
echo -ne "Enter branch [${DEFAULT_BRANCH}]: "
#echo -ne "Enter branch [RB-R58.CL1602]: "
read BRANCH; export BRANCH;

# >> Set to default branch number if branch number is not provided.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ ! $BRANCH ]
then
        export BRANCH=$DEFAULT_BRANCH
        echo "Branch is not provided.  Assumed >>> $BRANCH <<<"
        echo
fi

# >> Set the default release number.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#export DEFAULT_RELEASE=r59
export DEFAULT_RELEASE=r62
echo
#echo -ne "Enter release number [r59]: "
echo -ne "Enter release number [r62]: "
read RELEASE; export RELEASE;

# >> Set to default release number if release number is not provided.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ ! $RELEASE ]
then
	export RELEASE=$DEFAULT_RELEASE
        echo "Release number is not provided.  Assumed >>> $RELEASE <<<"
fi

# >> Determine the RELEASE_NUMBER number from $RELEASE
RELEASE_NUMBER=`echo $RELEASE | cut -c2-3`.0
echo "The Release Number determined from the $RELEASE is >>> $RELEASE_NUMBER <<<"

# >> Set the default release number.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
export DEFAULT_ENV_ID=b
echo
echo -ne "Enter the environement b - test & p - prod [b]: "
read ENV_ID; export ENV_ID;

# >> Set to default release number if release number is not provided.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ ! $ENV_ID ]
then
        export ENV_ID=$DEFAULT_ENV_ID
        echo "Environment ID is not provided.  Assumed >>> $ENV_ID <<<"
        echo
fi

# >> Get the confirmation for moving the patches to depot folder.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
export DEFAULT_COPY_TO_DEPOT=N
echo -ne "Copy the patches to the deploy folder? Y/N [N]: "
read COPY_TO_DEPOT; export COPY_TO_DEPOT;

# >> Set to default release number if release number is not provided.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ ! $COPY_TO_DEPOT ]
then
        export COPY_TO_DEPOT=$DEFAULT_COPY_TO_DEPOT
        echo "Confirmation to copy to depot is not provided.  Assumed >>> N <<<<"
        echo
fi

FINAL_APP_TAR_FILE="srs-rdt-app-${RELEASE_NUMBER}${ENV_ID}-${TS}.linux.tar"
FINAL_VAR_TAR_FILE="srs-rdt-var-${RELEASE_NUMBER}${ENV_ID}-${TS}.linux.tar"
#FINAL_APP_TAR_FILE="srs-rdt-app-57${ENV_ID}-${TS}.linux.tar"
#FINAL_VAR_TAR_FILE="srs-rdt-var-57${ENV_ID}-${TS}.linux.tar"

DEPLOY_DEST=/home/build/srs/depot/TARS/rdt/$RELEASE.0

export APP_DEPLOY_DIR=$SRC/$BRANCH-app-deploy
export VAR_DEPLOY_DIR=$SRC/$BRANCH-var-deploy

echo "APP_DEPLOY_DIR:	$APP_DEPLOY_DIR"
echo "VAR_DEPLOY_DIR:   $VAR_DEPLOY_DIR"

# >> Create the $APP_DEPLOY_DIR if it did not exist.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ ! -d $APP_DEPLOY_DIR ]
then
        echo
        echo "Creating $APP_DEPLOY_DIR now..."
        mkdir $APP_DEPLOY_DIR
fi

# >> Create the $VAR_DEPLOY_DIR if it did not exist.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ ! -d $VAR_DEPLOY_DIR ]
then
        echo
        echo "Creating $VAR_DEPLOY_DIR now..."
        mkdir $VAR_DEPLOY_DIR
fi

# >> Remove the contents of $APP_DEPLOY_DIR & VAR_DEPLOY_DIR
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
rm -rf $APP_DEPLOY_DIR/*
rm -rf $VAR_DEPLOY_DIR/*


APP_TARS=`ls $SRC/$BRANCH-patches/app/*app*$RELEASE.tar*`
VAR_TARS=`ls $SRC/$BRANCH-patches/var/*var*$RELEASE.tar*`

echo
echo "Changing directory to $APP_DEPLOY_DIR"
echo
cd $APP_DEPLOY_DIR


# >> Run thru the loop and expand each of the tars in app folder.
for APP_TAR in $APP_TARS ;
do
        tar xzfp $APP_TAR
	echo ""
	echo "-------------------------------------------------"
	echo "- "
	echo "- Now unzipping $APP_TAR"
        echo "- $APP_TAR"
	echo "-------------------------------------------------"
	echo ""
done

# >> Set the correct permissions on the patch
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# >> Set 775 to cfg cfg/*
echo
echo "-------------------------------------------------"
echo "- Changing the folder permissions on cfg         "
echo "-------------------------------------------------"
echo
correct_app_cfg_folder_permissions

# >> Set 755 to scripts and scripts/*
echo
echo "-------------------------------------------------"
echo "- Changing the folder permissions on scripts     "
echo "-------------------------------------------------"
echo
chmod 755 $APP_DEPLOY_DIR/*/scripts
chmod -R 755 $APP_DEPLOY_DIR/*/scripts/*

# >> Set 755 to bin and bin/*
echo
echo "-------------------------------------------------"
echo "- Changing the folder permissions on bin         "
echo "-------------------------------------------------"
echo
chmod 755 $APP_DEPLOY_DIR/*/bin
chmod 755 $APP_DEPLOY_DIR/*/bin/*
chmod 755 $APP_DEPLOY_DIR/*/bin/*/*

# >> Set 755 to lib and lib/*
echo
echo "-------------------------------------------------"
echo "- Changing the folder permissions on lib         "
echo "-------------------------------------------------"
echo
chmod 755 $APP_DEPLOY_DIR/*/lib
chmod 755 $APP_DEPLOY_DIR/*/lib/*

# >> Set 775 to app/web
echo
echo "-------------------------------------------------"
echo "- Changing the folder permissions on app/web     "
echo "-------------------------------------------------"
echo
chmod 775 $APP_DEPLOY_DIR/*/app/web

# >> Create the app-tar file
echo
echo "-------------------------------------------------"
echo "- Now zipping all $APP_DEPLOY_DIR/* to app tar   "
echo "-------------------------------------------------"
echo
tar cfp $FINAL_APP_TAR_FILE *

# >> Compress the app-tar file
echo
echo "-------------------------------------------------"
echo "- Now compressing the $FINAL_APP_TAR_FILE        "
echo "-------------------------------------------------"
echo
compress -f $FINAL_APP_TAR_FILE

echo
echo "Changing directory to $VAR_DEPLOY_DIR"
echo
cd $VAR_DEPLOY_DIR

# >> Run thru the loop and expand each of the tars in var folder.
for VAR_TAR in $VAR_TARS ;
do
        tar xzfp $VAR_TAR
	echo ""
        echo "-------------------------------------------------"
        echo "- "
        echo "- Now unzipping $APP_TAR"
        echo "- $VAR_TAR"
        echo "-------------------------------------------------"
        echo ""
done

# >> Create the var-tar file
echo "-------------------------------------------------"
echo "- Now correcting the folder permissions in var   "
echo "-------------------------------------------------"
echo 
correct_var_folder_permissions

echo
echo "-------------------------------------------------"
echo "- Now zipping all $VAR_DEPLOY_DIR/* to var tar   "
echo "-------------------------------------------------"
echo
tar cfp $FINAL_VAR_TAR_FILE *

# >> Compress the var-tar file
echo
echo "-------------------------------------------------"
echo "- Now compressing the $FINAL_VAR_TAR_FILE        "
echo "-------------------------------------------------"
echo
compress -f $FINAL_VAR_TAR_FILE


# Copy logic here based on $COPY_TO_DEPOT -
if [[ $COPY_TO_DEPOT == "Y" ]] || [[ $COPY_TO_DEPOT == "y" ]]
then

	# >> Done with creating the tar files for the distribution
	# >> Now copy them to the distribution folder for CM team to push them test/prod boxes.
	echo 
	echo "-------------------------------------------------"
	echo "- Now copying the file "
	echo "- $APP_DEPLOY_DIR/$FINAL_APP_TAR_FILE.Z"
	echo "- to "
	echo "- $DEPLOY_DEST "
	echo "-------------------------------------------------"
	cp $APP_DEPLOY_DIR/$FINAL_APP_TAR_FILE.Z $DEPLOY_DEST

	echo
	echo "-------------------------------------------------"
	echo "- Now copying the file "
	echo "- $VAR_DEPLOY_DIR/$FINAL_VAR_TAR_FILE.Z"
	echo "- to "
	echo "- $DEPLOY_DEST "
	echo "-------------------------------------------------"
	cp $VAR_DEPLOY_DIR/$FINAL_VAR_TAR_FILE.Z $DEPLOY_DEST
else
	echo
	echo "-------------------------------------------------"
	echo "- Patches are not copied over to the DEPLOY folder as copy was requested."
	echo "- The patches created are - "
	echo "- $APP_DEPLOY_DIR/$FINAL_APP_TAR_FILE.Z "
	echo "- and "
	echo "- $VAR_DEPLOY_DIR/$FINAL_VAR_TAR_FILE.Z "
	echo "-------------------------------------------------"
	echo
fi

echo


