#!/bin/sh

clear

# >> Set the default branch.
#    ~~~~~~~~~~~~~~~~~~~~~~
export DEFAULT_BRANCH=RB-CL1202
echo
echo -ne "Enter branch number [RB-CL1202]: "
read BRANCH; export BRANCH;

# >> Set to default branch number if branch number is not provided.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ ! $BRANCH ]
then
        export BRANCH=$DEFAULT_BRANCH
        echo "Branch is not provided.  Assumed >>> $BRANCH <<<"
        echo
fi

# >> Set the default release number.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
export DEFAULT_RELEASE=r50
echo
echo -ne "Enter release number [r50]: "
read RELEASE; export RELEASE;

# >> Set to default release number if release number is not provided.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ ! $RELEASE ]
then
	export RELEASE=$DEFAULT_RELEASE
        echo "Release number is not provided.  Assumed >>> $RELEASE <<<"
fi

# >> Determine the RELEASE_NUMBER number from $RELEASE
RELEASE_NUMBER=`echo $RELEASE | cut -c2-3`.0
echo "The Release Number determined from the $RELEASE is >>> $RELEASE_NUMBER <<<"

# >> Set the default release number.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
export DEFAULT_ENV_ID=b
echo
echo -ne "Enter the environement b - test & p - prod [b]: "
read ENV_ID; export ENV_ID;

# >> Set to default release number if release number is not provided.
#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if [ ! $ENV_ID ]
then
        export ENV_ID=$DEFAULT_ENV_ID
        echo "Environment ID is not provided.  Assumed >>> $ENV_ID <<<"
        echo
fi


# >> Deploy the app tar file in dev -
echo 
echo "------------------------------------------------------------------"
echo "- Now deploying the app patch in dev"
echo "- The following file is being deployed"
echo "- $SRC/$BRANCH-app-deploy/srs-rdt-app-$RELEASE_NUMBER$ENV_ID-*.linux.tar.Z"
echo "------------------------------------------------------------------"
cd $SCRIPTS/../..
cp $SRC/$BRANCH-app-deploy/srs-rdt-app-$RELEASE_NUMBER$ENV_ID-*.linux.tar.Z .
APP_TARS=`ls srs-rdt-app-$RELEASE_NUMBER$ENV_ID-*.linux.tar.Z`
for APP_TAR in $APP_TARS ;
do
        tar xvzfp $APP_TAR
	rm -f $APP_TAR
done

# >> Deploy the var tar file in dev -
echo
echo "------------------------------------------------------------------"
echo "- Now deploying the var patch in dev"
echo "- The following file is being deployed"
echo "- $SRC/$BRANCH-var-deploy/srs-rdt-var-$RELEASE_NUMBER$ENV_ID-*.linux.tar.Z"
echo "------------------------------------------------------------------"
cd $LOGS/..
VAR_TARS=`ls srs-rdt-var-$RELEASE_NUMBER$ENV_ID-*.linux.tar.Z`
for VAR_TAR in $VAR_TARS
do
	tar xvzfp $VAR_TAR
	rm -f $APP_TAR
done
echo

exit 0
