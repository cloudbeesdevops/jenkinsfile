clear

setStagePhase1() {

	export TS=`date +%Y%m%d%H%M%S`
	
	# >> Set the default branch.
	#    ~~~~~~~~~~~~~~~~~~~~~~
	export DEFAULT_BRANCH=RB-CL1202
	echo
	echo -ne "Enter branch number [RB-CL1202]: "
	read BRANCH; export BRANCH;
	
	# >> Set to default branch number if branch number is not provided.
	#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if [ ! $BRANCH ]
	then
	        export BRANCH=$DEFAULT_BRANCH
	        echo "Branch is not provided.  Assumed >>> $BRANCH <<<"
	        echo
	fi
	
	# >> Set the default release number.
	#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	export DEFAULT_RELEASE=r50
	echo
	echo -ne "Enter release number [r50]: "
	read RELEASE; export RELEASE;
	
	# >> Set to default release number if release number is not provided.
	#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if [ ! $RELEASE ]
	then
	        export RELEASE=$DEFAULT_RELEASE
	        echo "Release number is not provided.  Assumed >>> $RELEASE <<<"
	fi
	
	# >> Determine the RELEASE_NUMBER number from $RELEASE
	RELEASE_NUMBER=`echo $RELEASE | cut -c2-3`.0
	echo "The Release Number determined from the $RELEASE is >>> $RELEASE_NUMBER <<<"
	
	
}

setStagePhase2() {
	
	# >> Set the default ENV_ID
	#    ~~~~~~~~~~~~~~~~~~~~~~
	export DEFAULT_ENV_ID=b
	echo
	echo -ne "Enter the environement b - test & p - prod [b]: "
	read ENV_ID; export ENV_ID;
	
	# >> Set to default ENV_ID if one is not provided.
	#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if [ ! $ENV_ID ]
	then
	        export ENV_ID=$DEFAULT_ENV_ID
	        echo "Environment ID is not provided.  Assumed >>> $ENV_ID <<<"
	        echo
	fi
	
	# >> Get the confirmation for moving the patches to depot folder.
	#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	export DEFAULT_COPY_TO_DEPOT=N
	echo -ne "Copy the patches to the deploy folder? Y/N [N]: "
	read COPY_TO_DEPOT; export COPY_TO_DEPOT;
	
	# >> Set to default release number if release number is not provided.
	#    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	if [ ! $COPY_TO_DEPOT ]
	then
	        export COPY_TO_DEPOT=$DEFAULT_COPY_TO_DEPOT
	        echo "Confirmation to copy to depot is not provided.  Assumed >>> N <<<<"
	        echo
	fi

}

copy_content() {

	# Define these -
        FINAL_SQL_TAR_FILE="srs-rdt-sql-$RELEASE_NUMBER$ENV_ID-$TS.noarch.tar"
        DEPLOY_DEST=/home/build/srs/depot/TARS/rdt/$RELEASE.0

        export SQL_DEPLOY_DIR=$SRC/$BRANCH-sql-deploy

        echo "SQL_DEPLOY_DIR: $SQL_DEPLOY_DIR"

        # >> Create the $SQL_DEPLOY_DIR if it did not exist.
        #    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        if [ ! -d $SQL_DEPLOY_DIR ]
        then
                echo
                echo "Creating $SQL_DEPLOY_DIR now..."
                mkdir $SQL_DEPLOY_DIR
        fi

        echo
        echo "Removing the contents of $SQL_DEPLOY_DIR"
        rm -rf $SQL_DEPLOY_DIR/*

        echo
        echo "Copying the contents from $SRC/$BRANCH/rdt-sql/$RELEASE_NUMBER to $SQL_DEPLOY_DIR/$RELEASE_NUMBER now..."
        echo

	cp -r $SRC/$BRANCH/rdt-sql/$RELEASE_NUMBER $SQL_DEPLOY_DIR/$RELEASE_NUMBER
	
	# Now remove all the unwanted stuff...
	cd $SQL_DEPLOY_DIR
	LS=`find . -name .svn -print`
	for x in $LS ;
	do
		rm -rf $x
	done

        echo
        echo "Files are copied... Here is the folder structure after copying the file - "
        echo

        #cd $SQL_DEPLOY_DIR
	pwd
        tree
}

create_sql_patch() {

	cd $SQL_DEPLOY_DIR
	echo
	echo "Now creating $FINAL_SQL_TAR_FILE -"
	tar cvfp $FINAL_SQL_TAR_FILE *
	echo "File in the tar file are -"
	tar tvfp $FINAL_SQL_TAR_FILE
	echo "Now compressing -"
	
	# >> Compress the sql-tar file
	echo
	echo "-------------------------------------------------"
	echo "- Now compressing the $FINAL_SQL_TAR_FILE        "
	echo "-------------------------------------------------"
	echo
	compress -f $FINAL_SQL_TAR_FILE

}

copy_to_dest() {
	
	# Copy logic here based on $COPY_TO_DEPOT -
	if [[ $COPY_TO_DEPOT == "Y" ]] || [[ $COPY_TO_DEPOT == "y" ]]
	then
	
	        # >> Done with creating the tar file for the distribution
	        # >> Now copy it to the distribution folder for CM team to push them test/prod boxes.
	        echo
	        echo "-------------------------------------------------"
	        echo "- Now copying the file "
	        echo "- $SQL_DEPLOY_DIR/$FINAL_SQL_TAR_FILE.Z"
	        echo "- to "
	        echo "- $DEPLOY_DEST "
	        echo "-------------------------------------------------"
	        cp $SQL_DEPLOY_DIR/$FINAL_SQL_TAR_FILE.Z $DEPLOY_DEST
	else
	        echo
	        echo "-------------------------------------------------"
	        echo "- Patches are not copied over to the DEPLOY folder as copy was requested."
	        echo "- The patches created are - "
	        echo "- $VAR_DEPLOY_DIR/$FINAL_VAR_TAR_FILE.Z "
	        echo "-------------------------------------------------"
	        echo
	fi
	
}

# Logic to see if the src folder has any sql's in them for this release ... if so, continue - else comeout saying that there is nothing to build.
setStagePhase1

# Check the source code has sql data for this release - if not return.
if [ -d $SRC/$BRANCH/rdt-sql/$RELEASE ]
then
	echo 
	echo "$SRC/$BRANCH/rdt-sql/$RELEASE does not exist."
	echo "Assuming that there are no manual load changes for R$RELEASE, I'm quitting."
	echo "No sql patch would be created."
	echo
else
	echo
	echo "Found sql files for this release...."
	echo "Processing to create the sql-patch..."
	echo
	setStagePhase2
	copy_content
	create_sql_patch
	copy_to_dest
fi

