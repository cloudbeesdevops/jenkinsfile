#!/bin/sh

export Project_List="c3case-client invoice-adjustments rdt-1source rdt-c3cache \
rdt-common-scripts rdt-commons rdt-daemon rdt-dom-adjustments rdt-environment rdt-mdb rdt-mia \
rdt-rebillAutomation rdt-websvc rdt-xmlbeans revenue.desktop.server rdt-batch rdt-sql";

setUserPass() {
        echo -ne "Enter username for checkout:"
        read USER_NAME
        export USER_NAME

        echo -ne "Enter password for $USER_NAME:"
        read -s PASSWORD
        export PASSWORD
	echo
	echo "RB-R53.CL1401"
        echo -ne "Enter the Branch Name: "
        read SVN_BRANCH
        export SVN_BRANCH

	echo "Build initiated by user:$USER_NAME"
}

setEnv() {

        export TS=`date +%Y-%m-%dT%H.%M.%S`
        export LOG_DIR=$LOGFILES/build
        export ARCHIVE_DIR=$LOGFILES/build_archive
        export LOG_FILE="$LOG_DIR/RDT_Build.$TS.log"

        # >> Create the LOG_DIR if it did not exist.
        if [ ! -d $LOG_DIR ]
        then
                echo "Creating $LOG_DIR now..."
                mkdir $LOG_DIR
        fi

        # >> Create the $LOG_DIR/archive if it did not exist.
        if [ ! -d $ARCHIVE_DIR ]
        then
                echo "Creating $ARCHIVE_DIR now..."
                mkdir $ARCHIVE_DIR
        fi

	export SVN_BRANCH="RB-R53.CL1401"
        export SVN_URL="https://svn1.prod.fedex.com/svn/repos/rdt_invadj/branches/$SVN_BRANCH"
        export SVN_EXE="/opt/CollabNet_Subversion/bin/svn"
        export DEST="$HOME/rev/depot/TARS/enable_app/r53.0"
        export SRC_DIR=$SRC
        export BUILD_DIR="$SRC_DIR/$SVN_BRANCH"

        echo "SVN Url is :$SVN_URL"
        echo "Source repository is :$SRC_DIR"
        echo "Build destination is :$DEST"

}

checkout() {
	
	# >> Create the $LOG_DIR/archive if it did not exist.
        if [ ! -d $BUILD_DIR ]
        then
                echo "Creating $BUILD_DIR now..."
                mkdir $SRC/$SVN_BRANCH
        fi

        echo "************Executing checkout and build for $SVN_BRANCH************"
        cd $SRC_DIR/$SVN_BRANCH
        for Project in $Project_List
        do
                Project_Url="$SVN_URL/$Project"
                $SVN_EXE checkout --username $USER_NAME --password $PASSWORD $Project_Url
        done
}

setUserPass
setEnv 
mv $LOG_DIR/* $ARCHIVE_DIR
checkout
#checkout >>$LOG_FILE 2>&1;

