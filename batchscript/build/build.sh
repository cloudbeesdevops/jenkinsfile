#!/bin/ksh

do_rdt_environment() {
    build_rdt_environment
    assemble_rdt_environment
    deploy_rdt_environment
}

do_rdt_1source() {
    build_rdt_1source
    assemble_rdt_1source
    deploy_rdt_1source
}

do_rdt_c3cache() {
    build_rdt_c3cache
    assemble_rdt_c3cache
    deploy_rdt_c3cache
}

do_rdt_c3case_client() {
    build_c3case_client
}

do_rdt_commons() {
    build_rdt_commons
}

do_rdt_email_util() {
    build_rdt_email_util
}

do_rdt_mainframe_services() {
    build_rdt_mainframe_services
    assemble_rdt_mainframe
    deploy_rdt_mainframe
}

do_rdt_websvc() {
    build_rdt_websvc
    assemble_rdt_websvc
    deploy_rdt_websvc
}

do_revenue_desktop_server() {
    build_rdt_server
    assemble_rdt_server
    deploy_rdt_server
}

do_rdt_dom_adjustments() {
    build_rdt_dom_adjustments
    assemble_rdt_dom_adjustments
    deploy_rdt_dom_adjustments
}

do_rdt_mug_websvc() {
    build_rdt_mug_websvc
    assemble_rdt_mug_websvc
    deploy_rdt_mug_websvc
}

do_invoice_adjustments() {
    build_invoice_adjustments
    assemble_invoice_adjustments
    deploy_invoice_adjustments
}

do_rdt_mdb() {
    build_rdt_mdb
    assemble_rdt_mdb
    deploy_rdt_mdb
}

do_rdt_mia() {
    build_rdt_mia
    assemble_rdt_mia
    deploy_rdt_mia
}

do_rdt_daemon() {
    build_rdt_daemon
    assemble_rdt_daemon
    deploy_rdt_daemon
}

do_rdt_rebillAutomation() {
    build_rdt_rebillAutomation
    assemble_rdt_rebillAutomation
    deploy_rdt_rebillAutomation
}

do_rdt_batch() {
    build_rdt_batch
    assemble_rdt_batch
    deploy_rdt_batch
}

do_rdt_edi_autoadjust() {
        build_rdt_edi_autoadjust
        assemble_rdt_edi_autoadjust
        deploy_rdt_edi_autoadjust
}

do_rdt_customer_services() {
        build_rdt_customer_services
        assemble_rdt_customer_services
        deploy_rdt_customer_services
}

setEnv() {

    # >> Set the default Branch.
    export DEFAULT_BRANCH=RB-r63.CL1901
    export DEFAULT_RELEASE=r63
    echo
    echo -ne "Enter branch to build [RB-r63.CL1901]:"
    read BRANCH; export BRANCH

    # >> Set to default branch if branch name is not provided during the build.
    if [ ! $BRANCH ]
    then
        export BRANCH=$DEFAULT_BRANCH
        echo "Branch name not provided.  Assumed >>> $BRANCH <<<"
    fi
    
    # >> Set the default release number.
    echo 
    echo -ne "Enter release number [r63]: "
    read RELEASE; export RELEASE; 

    # >> Set to default release number if branch name is not provided during the build.
    if [ ! $RELEASE ]
    then
        export RELEASE=$DEFAULT_RELEASE
        echo "Release number is not provided.  Assumed >>> $RELEASE <<<"
    fi

    # >> set $BUILD_RELEASE to current release.
    export BUILD_RELEASE=$RELEASE
    echo 
    echo "BUILD_RELEASE is set to >>> $BUILD_RELEASE <<<"

    # >> set $RELEASE_NUMBER to current release.
    export RELEASE_NUMBER=`echo $RELEASE | cut -c2-3`.0
    echo
    echo "RELEASE_NUMBER is set to >>> $RELEASE_NUMBER <<<"

    export TS=`date +%Y-%m-%dT%H.%M.%S`
    export LOG_DIR=$LOGFILES/build
    export ARCHIVE_DIR=$LOGFILES/build_archive
    export LOG_FILE="$LOG_DIR/RDT_Build.$TS.log"
    export MVN=/home/RDT/apache-maven-2.2.1/bin
    export SRC=$SRC

    export DEST=$SRC/$BRANCH-patches
    export DEST_ARCHIVE=$SRC/$BRANCH-patches/archive
    export SRC_DIR=/home/RDT/rdt/61.0/devel/src
    export BUILD_DIR=$SRC_DIR/$BRANCH

    # >> Create the LOG_DIR if it did not exist.
    if [ ! -d $LOG_DIR ]
    then
        echo
        echo "Creating $LOG_DIR now..."
        mkdir $LOG_DIR
    fi

    # >> Create the $LOG_DIR/archive if it did not exist.
    if [ ! -d $ARCHIVE_DIR ]
    then
        echo
        echo "Creating $ARCHIVE_DIR now..."
        mkdir $ARCHIVE_DIR
    fi

    # >> Create the folder $DEST if it did not exist.
    if [ ! -d $DEST ]
    then
        echo
        echo "Creating $DEST now..."
        mkdir $DEST
    fi

    # >> Create the folder $DEST/app if it did not exist.
    if [ ! -d $DEST/app ]
    then
        echo
        echo "Creating $DEST/app now..."
        mkdir $DEST/app
    fi

    # >> Create the folder $DEST/var if it did not exist.
    if [ ! -d $DEST/var ]
    then
        echo
        echo "Creating $DEST/var now..."
        mkdir $DEST/var
    fi
    
    # >> Create the folder $DEST_ARCHIVE if it did not exist.
    if [ ! -d $DEST_ARCHIVE ]
    then
        echo
        echo "Creating $DEST_ARCHIVE now..."
        mkdir $DEST_ARCHIVE
    fi

    # - Check if the branch in question exists on the disk -
    if [ ! -d "$SRC_DIR/$BRANCH" ]
    then
        echo "$SRC_DIR/$BRANCH does not exist... "
        echo "Please check out the branch $BRANCH using ./checkout_code.sh and run this script to build the code."
        echo "Execution will quit now..."
        echo
        exit 1
    fi
}

moveToArchive() {

    if [ "$(ls -A $LOG_DIR)" ]
    then
        echo
        echo "Now moving the log files to archive..."
        echo
        mv $LOG_DIR/* $ARCHIVE_DIR
    else
        echo
        echo "No log files found to move to archive..."
        echo
    fi

}

# ---------------------------------------------------------
# - rdt-environment
# ---------------------------------------------------------
build_rdt_environment() {
        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                       Now building rdt-environment                         -"
        echo " -                       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~                         -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo
        $MVN/mvn $MVN_OPTS org.apache.maven.plugins:maven-dependency-plugin:2.8:copy -Dartifact=com.fedex:csm:12.1.2:war -DoutputDirectory=$BIN/csm
}


assemble_rdt_environment() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                       Now assembling rdt-environment                         -"
        echo " -                       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~                         -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo
        cd "$BUILD_DIR/rdt-environment"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean deploy assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly
}

deploy_rdt_environment() {

        APP_FILE=rdt-environment-app-$RELEASE.tar.gz
        VAR_FILE=rdt-environment-var-$RELEASE.tar.gz

        if [ -s "$DEST/app/$APP_FILE" ]
        then
                mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
        fi
        cp "$BUILD_DIR/rdt-environment/target/$APP_FILE" $DEST/app

        if [ -s "$DEST/var/$VAR_FILE" ]
        then
                mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
        fi
        cp "$BUILD_DIR/rdt-environment/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-1source
# ---------------------------------------------------------
build_rdt_1source() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-1source                           -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~                           -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo
        cd "$BUILD_DIR/rdt-1source"
        $MVN/mvn $MVN_OPTS  -f pom.xml clean install

        cd "$BUILD_DIR/rdt-1source/rdt-1source-war"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install

        cd "$BUILD_DIR/rdt-1source/rdt-1source-ear"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
        $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install
}

assemble_rdt_1source() {

        cd "$BUILD_DIR/rdt-1source/rdt-1source-war"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly

        cd "$BUILD_DIR/rdt-1source/rdt-1source-ear"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly
}

deploy_rdt_1source() {

    APP_FILE=rdt-1source-ear-app-$RELEASE.tar.gz
    VAR_FILE=rdt-1source-ear-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-1source/rdt-1source-ear/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-1source/rdt-1source-ear/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-c3cache
# ---------------------------------------------------------
build_rdt_c3cache() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-c3cache                           -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~                           -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo
        cd "$BUILD_DIR/rdt-c3cache"
        $MVN/mvn $MVN_OPTS  -f pom.xml clean install

        cd "$BUILD_DIR/rdt-c3cache/rdt-c3cache-ear"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install
}

assemble_rdt_c3cache() {

        cd "$BUILD_DIR/rdt-c3cache/rdt-c3cache-ear"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly
}

deploy_rdt_c3cache() {

    APP_FILE=rdt-c3cache-ear-app-$RELEASE.tar.gz
    VAR_FILE=rdt-c3cache-ear-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-c3cache/rdt-c3cache-ear/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-c3cache/rdt-c3cache-ear/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - c3case-client
# ---------------------------------------------------------
build_c3case_client() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building c3case-client                         -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~~~                         -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo
        cd "$BUILD_DIR/c3case-client"
        $MVN/mvn $MVN_OPTS  -f pom.xml clean install
}

# ---------------------------------------------------------
# - rdt-commons
# ---------------------------------------------------------
build_rdt_commons() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-commons                           -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~                           -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo
        cd "$BUILD_DIR/rdt-commons"
        $MVN/mvn $MVN_OPTS  -f pom.xml clean install
}

# ---------------------------------------------------------
# - rdt-email-util
# ---------------------------------------------------------
build_rdt_email_util() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-email-util                        -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~                           -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo
        cd "$BUILD_DIR/rdt-email-util"
        $MVN/mvn $MVN_OPTS  -f pom.xml clean install
}

# ---------------------------------------------------------
# - rdt-mainframe-services
# ---------------------------------------------------------
build_rdt_mainframe_services() {
        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-mainframe-services                      -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~                           -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-mainframe-services"
        $MVN/mvn $MVN_OPTS -f app-pom.xml clean install
}
assemble_rdt_mainframe() {

    cd "$BUILD_DIR/rdt-mainframe-services"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        #$MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly
}

deploy_rdt_mainframe() {

    APP_FILE=rdt-mainframe-services-app-$RELEASE.tar.gz
    #VAR_FILE=rdt-mainframe-services-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
            mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-mainframe-services/target/$APP_FILE" $DEST/app

    #if [ -s "$DEST/var/$VAR_FILE" ]
    #then
    #        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    #fi
    #cp "$BUILD_DIR/rdt-mainframe-services/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-websvc
# ---------------------------------------------------------
build_rdt_websvc() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-websvc                            -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~                            -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-xmlbeans"
        $MVN/mvn $MVN_OPTS  -f pom.xml clean install

        cd "$BUILD_DIR/rdt-websvc"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install
}

assemble_rdt_websvc() {

        cd "$BUILD_DIR/rdt-websvc"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly
}

deploy_rdt_websvc() {

    APP_FILE=rdt-websvc-app-$RELEASE.tar.gz
    VAR_FILE=rdt-websvc-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-websvc/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-websvc/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - revenue.desktop.server
# ---------------------------------------------------------
build_rdt_server() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                     Now building revenue.desktop.server                    -"
        echo " -                     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                    -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/revenue.desktop.server"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install

}

assemble_rdt_server() {

        cd "$BUILD_DIR/revenue.desktop.server"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly

}

deploy_rdt_server() {

    APP_FILE=rdt-irdtserver-app-$RELEASE.tar.gz
    VAR_FILE=rdt-irdtserver-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/revenue.desktop.server/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/revenue.desktop.server/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-dom-adjustments
# ---------------------------------------------------------
build_rdt_dom_adjustments() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                      Now building rdt-dom-adjustments                      -"
        echo " -                      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                      -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-dom-adjustments"
        $MVN/mvn1 $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn1 $MVN_OPTS  -f var-pom.xml clean install

}

assemble_rdt_dom_adjustments() {

        cd "$BUILD_DIR/rdt-dom-adjustments"
        $MVN/mvn1 $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn1 $MVN_OPTS  -f var-pom.xml assembly:assembly

}

deploy_rdt_dom_adjustments() {

    APP_FILE=rdt-dominvadj-app-$RELEASE.tar.gz
    VAR_FILE=rdt-dominvadj-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-dom-adjustments/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-dom-adjustments/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-mug-websvc
# ---------------------------------------------------------
build_rdt_mug_websvc() {

        echo
        echo " -----------------------------------------------------------------------------"
        echo " -                      Now building rdt-mug-websvc                           -"
        echo " -                      ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                      -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-mug-websvc"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install

}

assemble_rdt_mug_websvc() {

        cd "$BUILD_DIR/rdt-mug-websvc"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly

}

deploy_rdt_mug_websvc() {

    APP_FILE=rdt-mug-websvc-app-$RELEASE.tar.gz
    VAR_FILE=rdt-mug-websvc-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
            mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-mug-websvc/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
            mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-mug-websvc/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - invoice-adjustments
# ---------------------------------------------------------
build_invoice_adjustments() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                     Now building invoice-adjustments                       -"
        echo " -                     ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                       -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo
        cd "$BUILD_DIR/invoice-adjustments"
        $MVN/mvn $MVN_OPTS  -f pom.xml clean install

        cd "$BUILD_DIR/invoice-adjustments/invoice-adjustments-ear"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install

        cd "$BUILD_DIR/invoice-adjustments/invoice-adjustments-webapp"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install

        cd "$BUILD_DIR/invoice-adjustments/invoice-adjustments-batch"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
}

assemble_invoice_adjustments() {

        cd "$BUILD_DIR/invoice-adjustments/invoice-adjustments-ear"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly

        cd "$BUILD_DIR/invoice-adjustments/invoice-adjustments-webapp"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly

        cd "$BUILD_DIR/invoice-adjustments/invoice-adjustments-batch"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly
}

deploy_invoice_adjustments() {

        APP_FILE=invoice-adjustments-ear-app-$RELEASE.tar.gz
        VAR_FILE=invoice-adjustments-ear-var-$RELEASE.tar.gz

        if [ -s "$DEST/app/$APP_FILE" ]
        then
                mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
        fi
        cp "$BUILD_DIR/invoice-adjustments/invoice-adjustments-ear/target/$APP_FILE" $DEST/app

        if [ -s "$DEST/var/$VAR_FILE" ]
        then
                mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
        fi
        cp "$BUILD_DIR/invoice-adjustments/invoice-adjustments-ear/target/$VAR_FILE" $DEST/var
        
        APP_FILE=invoice-adjustments-webapp-app-$RELEASE.tar.gz
        VAR_FILE=invoice-adjustments-webapp-var-$RELEASE.tar.gz

        if [ -s "$DEST/app/$APP_FILE" ]
        then
                mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
        fi
        cp "$BUILD_DIR/invoice-adjustments/invoice-adjustments-webapp/target/$APP_FILE" $DEST/app

        if [ -s "$DEST/var/$VAR_FILE" ]
        then
                mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
        fi
        cp "$BUILD_DIR/invoice-adjustments/invoice-adjustments-webapp/target/$VAR_FILE" $DEST/var

        APP_FILE=invoice-adjustments-batch-app-$RELEASE.tar.gz
        VAR_FILE=invoice-adjustments-batch-var-$RELEASE.tar.gz
 
        if [ -s "$DEST/app/$APP_FILE" ]
        then
                mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
        fi
        cp "$BUILD_DIR/invoice-adjustments/invoice-adjustments-batch/target/$APP_FILE" $DEST/app
 
        if [ -s "$DEST/var/$VAR_FILE" ]
        then
                mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
        fi
        cp "$BUILD_DIR/invoice-adjustments/invoice-adjustments-batch/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-mdb
# ---------------------------------------------------------
build_rdt_mdb() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-mdb                               -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~                               -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-mdb"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install
}

assemble_rdt_mdb() {

        cd "$BUILD_DIR/rdt-mdb"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly
}

deploy_rdt_mdb() {

    APP_FILE=rdt-mdb-app-$RELEASE.tar.gz
    VAR_FILE=rdt-mdb-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-mdb/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-mdb/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-mia
# ---------------------------------------------------------
build_rdt_mia() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-mia                               -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~                               -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-mia"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install

}

assemble_rdt_mia() {

        cd "$BUILD_DIR/rdt-mia"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly

}

deploy_rdt_mia() {

    APP_FILE=rdt-mia-app-$RELEASE.tar.gz
    VAR_FILE=rdt-mia-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-mia/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-mia/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-daemon
# ---------------------------------------------------------
build_rdt_daemon() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-daemon                            -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~                            -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-daemon"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install
}

assemble_rdt_daemon() {

        cd "$BUILD_DIR/rdt-daemon"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly
}

deploy_rdt_daemon() {

    APP_FILE=rdt-daemon-app-$RELEASE.tar.gz
    VAR_FILE=rdt-daemon-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-daemon/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-daemon/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-rebillAutomation
# ---------------------------------------------------------
build_rdt_rebillAutomation() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-rebillAutomation                  -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                  -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-rebillAutomation"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install

}

assemble_rdt_rebillAutomation() {

        cd "$BUILD_DIR/rdt-rebillAutomation"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly
}

deploy_rdt_rebillAutomation() {

    APP_FILE=rdt-rebillAutomation-app-$RELEASE.tar.gz
    VAR_FILE=rdt-rebillAutomation-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-rebillAutomation/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-rebillAutomation/target/$VAR_FILE" $DEST/var
}


# ---------------------------------------------------------
# - rdt-batch
# ---------------------------------------------------------
build_rdt_batch() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-batch                             -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~                  -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-batch"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml clean install
#       $MVN/mvn $MVN_OPTS  -f var-pom.xml clean install

}

assemble_rdt_batch() {

        cd "$BUILD_DIR/rdt-batch"
        $MVN/mvn $MVN_OPTS  -f app-pom.xml assembly:assembly
        $MVN/mvn $MVN_OPTS  -f var-pom.xml assembly:assembly

}

deploy_rdt_batch() {

    APP_FILE=rdt-batch-app-$RELEASE.tar.gz
    VAR_FILE=rdt-batch-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-batch/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi
    cp "$BUILD_DIR/rdt-batch/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-edi-autoadjust
# ---------------------------------------------------------

build_rdt_edi_autoadjust() {

        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-edi-autoadjust                    -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~                           -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-edi-autoadjust"
        $MVN/mvn $MVN_OPTS -f app-pom.xml clean install
        echo
}

assemble_rdt_edi_autoadjust() 
{

        cd "$BUILD_DIR/rdt-edi-autoadjust"
        echo
        $MVN/mvn $MVN_OPTS -f app-pom.xml assembly:assembly
        echo
        $MVN/mvn $MVN_OPTS -f var-pom.xml assembly:assembly
        echo
}

deploy_rdt_edi_autoadjust() {

    APP_FILE=rdt-edi-autoadjust-app-$RELEASE.tar.gz
    VAR_FILE=rdt-edi-autoadjust-var-$RELEASE.tar.gz

    if [ -s "$DEST/app/$APP_FILE" ]
    then
        mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
    fi

    cp "$BUILD_DIR/rdt-edi-autoadjust/target/$APP_FILE" $DEST/app

    if [ -s "$DEST/var/$VAR_FILE" ]
    then
        mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
    fi

    cp "$BUILD_DIR/rdt-edi-autoadjust/target/$VAR_FILE" $DEST/var
}

# ---------------------------------------------------------
# - rdt-customer-services
# ---------------------------------------------------------
build_rdt_customer_services()
{
        echo
        echo " ------------------------------------------------------------------------------"
        echo " -                         Now building rdt-customer-services                 -"
        echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~                           -"
        echo " -                                                                            -"
        echo " ------------------------------------------------------------------------------"
        echo

        cd "$BUILD_DIR/rdt-customer-services"
        echo "The command is - $MVN/mvn -f app-pom.xml clean install $MVN_OPTS"
        $MVN/mvn -f app-pom.xml clean install $MVN_OPTS

}

assemble_rdt_customer_services()
{
        cd "$BUILD_DIR/rdt-customer-services"
        $MVN/mvn -f app-pom.xml assembly:assembly $MVN_OPTS
        $MVN/mvn -f var-pom.xml assembly:assembly $MVN_OPTS
}

deploy_rdt_customer_services() {

        APP_FILE=rdt-customer-services-app-$RELEASE.tar.gz
        VAR_FILE=rdt-customer-services-var-$RELEASE.tar.gz

        if [ -s "$DEST/app/$APP_FILE" ]
        then
            mv "$DEST/app/$APP_FILE" "$DEST_ARCHIVE/$APP_FILE.$TS"
        fi
        cp "$BUILD_DIR/rdt-customer-services/target/$APP_FILE" $DEST/app

        if [ -s "$DEST/var/$VAR_FILE" ]
        then
            mv "$DEST/var/$VAR_FILE" "$DEST_ARCHIVE/$VAR_FILE.$TS"
        fi
        cp "$BUILD_DIR/rdt-customer-services/target/$VAR_FILE" $DEST/var
}


clear

#   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# - First thing first - Setup the environment for build -
#   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
setEnv

#   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# - Move any existing log file to archive folders -
#   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
moveToArchive

echo
echo "Log is being written to: $LOG_DIR/RDT_Build.$TS.log"
echo

echo                                                                                   >> $LOG_FILE 1>&1 ;
echo " ------------------------------------------------------------------------------" >> $LOG_FILE 2>&1 ;
echo " -                                                                            -" >> $LOG_FILE 2>&1 ;
echo " -                         B U I L D   S T A R T                              -" >> $LOG_FILE 2>&1 ;
echo " -                         ~~~~~~~~~~~~~~~~~~~~~                              -" >> $LOG_FILE 2>&1 ;
echo " ------------------------------------------------------------------------------" >> $LOG_FILE 2>&1 ;
echo                                                                                   >> $LOG_FILE 2>&1 ;

export MVN_OPTS=-Dmaven.test.skip=true

do_rdt_environment        >> $LOG_FILE 2>&1 ;
do_rdt_1source            >> $LOG_FILE 2>&1 ;
do_rdt_c3cache            >> $LOG_FILE 2>&1 ;
do_rdt_c3case_client      >> $LOG_FILE 2>&1 ;
do_rdt_commons            >> $LOG_FILE 2>&1 ;
do_rdt_email_util         >> $LOG_FILE 2>&1 ;
do_rdt_mainframe_services >> $LOG_FILE 2>&1 ;
do_rdt_customer_services  >> $LOG_FILE 2>&1 ;
do_rdt_websvc             >> $LOG_FILE 2>&1 ;
do_revenue_desktop_server >> $LOG_FILE 2>&1 ;
do_rdt_dom_adjustments    >> $LOG_FILE 2>&1 ;
do_invoice_adjustments    >> $LOG_FILE 2>&1 ;
do_rdt_mdb                >> $LOG_FILE 2>&1 ;
do_rdt_mia                >> $LOG_FILE 2>&1 ;
do_rdt_daemon             >> $LOG_FILE 2>&1 ;
do_rdt_rebillAutomation   >> $LOG_FILE 2>&1 ;
do_rdt_batch              >> $LOG_FILE 2>&1 ;
do_rdt_mug_websvc         >> $LOG_FILE 2>&1 ;
do_rdt_edi_autoadjust     >> $LOG_FILE 2>&1 ;
do_rdt_customer_services  >> $LOG_FILE 2>&1 ;



echo                                                >> $LOG_FILE 2>&1 ;
echo " ------------------------------------------------------------------------------" >> $LOG_FILE 2>&1 ;
echo " -                                                                            -" >> $LOG_FILE 2>&1 ;
echo " -                         B U I L D   C O M P L E T E                        -" >> $LOG_FILE 2>&1 ;
echo " -                         ~~~~~~~~~~~~~~~~~~~~~~~~~~~                        -" >> $LOG_FILE 2>&1 ;
echo " ------------------------------------------------------------------------------" >> $LOG_FILE 2>&1 ;
echo                                               >> $LOG_FILE 2>&1 ;

exit 0

