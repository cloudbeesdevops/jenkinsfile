#!/bin/ksh

####################################################
# pardaman singh / FedEx Services, Inc.            #
# a utility script for DEV work of this directory. #
#                                                  #
# *** WARNING ***                                  #
# CAREFUL - you should know what you are doing.    #
#                                                  #
####################################################

###########################################################################################################################
# what we are doing with this script
# data in .../certs/test is to be copied to ../framework/test/<___module___>/
# data in .../certs/prod is to be copied to ../framework/prod/<___module___>/
# in the patch we have data/framework/test/<___module___>/
# on the destination (test or prod) machine we will have /var/fedex/srs/rdt/data/framework/test/<___module___>/
###########################################################################################################################

TSCR=`basename $0`
basedir=$(dirname `pwd`)
fwdir=${basedir}/framework
fwtest=${fwdir}/test
fwprod=${fwdir}/prod
src_test=./test
src_prod=./prod

if [ -d ${fwdir} ] ; then
    printf "\nERROR -- directory=${fwdir} exists - it should not for us to use this script; goodbye. [${TSCR},${LINENO}]\n"
    echo;exit 10
elif [ ! -d ${src_test} ] ; then
    printf "\nERROR -- directory=${src_test} doest not exist; it should for us to use this script; goodbye. [${TSCR},${LINENO}]\n"
    echo;exit 11
elif [ ! -d ${src_prod} ] ; then
    printf "\nERROR -- directory=${src_prod} doest not exist; it should for us to use this script; goodbye. [${TSCR},${LINENO}]\n"
    echo;exit 12
fi

mkdir ${fwdir} ${fwtest} ${fwprod}
if [ 0 -ne $? ] ; then
    printf "\nERROR -- command \"mkdir ${fwdir} ${fwtest} ${fwprod}\" failed - a prblem. [${TSCR},${LINENO}]\n"
    echo;exit 13
fi

# each application that needs to rotate a certificate, needs to be its own "module"; this list can/should grow.
MODULES=" invadj_websvc_domain rdt_dom_adjustments_domain rdt_mdbservice_domain rdt_new_services_domain rdt_webservice_domain rdt_batchsvc_domain "
for m in ${MODULES}
do
    mtest=${fwtest}/${m}
    mprod=${fwprod}/${m}

    mkdir ${mtest} ${mprod}
    if [ 0 -ne $? ] ; then
        printf "\nERROR -- command \"mkdir ${mtest} ${mprod}\" failed; a problem. [${TSCR},${LINENO}]\n"
        echo;exit 14
    fi

    cp ${src_test}/* ${mtest}/
    if [ 0 -ne $? ] ; then
        printf "\nERROR -- command \"cp ${src_test}/* ${mtest}/\" failed - a problem. [${TSCR},${LINENO}]\n"
        echo;exit 15
    fi

    cp ${src_prod}/* ${mprod}/
    if [ 0 -ne $? ] ; then
        printf "\nERROR -- command \"cp ${src_prod}/* ${mprod}/\" failed - a problem. [${TSCR},${LINENO}]\n"
        echo;exit 15
    fi
done

printf "\nCongratulations,\ndirectory=${fwdir}\nwas successfully replicated from\ncurrent-working-directory=`basename $(pwd)`\n\n"

return 0

