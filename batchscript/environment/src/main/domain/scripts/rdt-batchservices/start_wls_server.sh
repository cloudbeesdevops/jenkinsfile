#!/bin/ksh

if [ "x4201" = "x${EAINUMBER}" ] ; then
    echo > /dev/null # do nothing since rdt.env is already sourced in
else
    . /opt/fedex/srs/rdt/current/cfg/env/rdt.env
fi

printf "\nworking . . .\n\n"
cd $SCRIPTS/rdt-batchservices
localdir=$(basename `pwd`)
report_file=${LOGFILES}/${localdir}_`basename $0`_`date +"%Y%m%d%H%M%S"`.`id --user --name`.$$.output.txt

printf "\n[INFO `date`]\ndirectory `pwd`\nhostname  `hostname`\nsubscript $SCRIPTS/invadj-rdt-admin/start_weblogic.sh\nreport    ${report_file}\n[`basename $0`,${LINENO}]\n\n" | tee -a ${report_file}
. $SCRIPTS/invadj-rdt-admin/start_weblogic.sh                                                                                                                                         >> ${report_file} 2>&1

