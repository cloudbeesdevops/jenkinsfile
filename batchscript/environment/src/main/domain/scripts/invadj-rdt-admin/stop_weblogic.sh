
. $SCRIPTS/invadj-rdt-admin/wls_profile.sh  $*

PATH=/bin:/opt/bin; export PATH

#######################
# SET WEBLOGIC SERVER #
#######################

if [ "xY" = "x${LATESTPB_MACHINE}" ] ; then
    beaHome=${RDT_WL_HOME}
else
    beaHome=/opt/weblogic/wl12.1.3.0
fi
weblogicHome=${beaHome}
wlsHome=${weblogicHome}/server
shutdownMode="FORCESHUTDOWN"

####################
# SET JAVA OPTIONS #
####################

javaHome=${JAVA_HOME}
javaVM=-server

####################
# SET CLASSPATH    #
####################

classpath=${classpath}:${wlsHome}/lib/weblogic.jar
classpath=${classpath}:${javaHome}/lib/tools.jar

printf "\n[INFO `date`] On `hostname` for stop; SERVER_URL=\"${SERVER_URL}\" , SERVER_NAME=\"${SERVER_NAME}\"\n\n"

${javaHome}/bin/java ${MEM_ARGS}                        \
            -classpath ${classpath}                     \
            weblogic.Admin ${shutdownMode}              \
            -url ${SERVER_URL}                          \
            -username ${WLS_USER} -password ${WLS_PWD}  \
            ${SERVER_NAME}  2>&1

