#!/bin/sh

. $SCRIPTS/invadj-rdt-admin/wls_profile.sh $*

export WL_HOME=${RDT_WL_HOME}

CLASSPATH=${WL_HOME}/server/lib/weblogic.jar:${CLASSPATH}
export CLASSPATH

PATH=$JAVA_HOME/bin:/bin:/usr/bin
export PATH

ITER_MAX=3

#######################################################################
# WebLogic Admin Tool Pings Server
#######################################################################
ITER=0
while [ ${ITER} -ne ${ITER_MAX} ]
do
    ITER=`expr ${ITER} + 1`
    java -classpath ${CLASSPATH} weblogic.Admin -url t3://${nodeName}:${hostPort} -username ${WLS_USER} -password ${WLS_PWD} ping 3 100
    PING_STATUS=${?}

done
#######################################################################

if [ "${PING_STATUS}" = "1" ]
then
    echo -e "\t [${SERVER_NAME}] Server for [${GRS_PROJECT_NAME}] at port [${hostPort}] is NOT responding at `date` ..."
fi

if [ "${PING_STATUS}" = "0" ]
then
    echo -e "\t [${SERVER_NAME}] Server for [${GRS_PROJECT_NAME}] at port [${hostPort}] is responding at `date` ..."
    exit 0
fi

exit 1

