#!/bin/sh
TSCR="wls_profile.sh"

printf "Entering the script; current-directory=`pwd` at `date` on `hostname` [${TSCR},${LINENO}]\n"

currentDirectory=`pwd`
GRS_PROJECT_NAME=`basename $currentDirectory`; export GRS_PROJECT_NAME
printf "INFORMATION: The value contained in GRS_PROJECT_NAME is \"${GRS_PROJECT_NAME}\" [${TSCR},${LINENO}]\n"

if [ $ENV_RELEASE == "59" ]
then
  RDT_RELEASE=$ENV_RELEASE
else
  RDT_RELEASE=`echo $ENV_RELEASE |  cut -f 1 -d "."`
fi

printf "INFORMATION: Work for $SCRIPTS/$GRS_PROJECT_NAME/profile.sh -- start [${TSCR},${LINENO}]\n"
. $SCRIPTS/$GRS_PROJECT_NAME/profile.sh
printf "INFORMATION: Work for $SCRIPTS/$GRS_PROJECT_NAME/profile.sh -- stop  [${TSCR},${LINENO}]\n"

printf "\nINFORMATION: Value contained in WLS_MANAGED_SERVERS=${WLS_MANAGED_SERVERS} [${TSCR},${LINENO}]\n"

wlsUser=weblogic
wlsPw=fedex123

nodeName=""
#get the server name
OLDIFS=$IFS
IFS=$'\n'
nodeNames=$(/usr/local/bin/cnames)
IFS=$OLDIFS
for node in $(echo $nodeNames)
do
  $(echo ${WLS_MANAGED_SERVERS} | grep -l $node > /dev/null)
  retCode=$?
  if [[ $retCode -eq 0 ]]
  then
    nodeName=$node
    break
  fi
done

export DB2Home=/opt/ibm/db2/V9.7/dsdriver

printf "\nINFORMATION: input parameter \"$1\" [${TSCR},${LINENO}]\n\n"

if [ "$1" = "admin" ]
then
    nodeName=${WLS_ADMIN_SERVER_ADDRESS}
    isAdminServer="true"
    UNIQUE_IDENTIFIER=$WLS_ADMIN_UNIQUE_IDENTIFIER
    ADMIN_URL=""
    hostPort=$WLS_ADMIN_PORT
    #startInformation="Starting Admin Server on `hostname` "
    SERVER_NAME="AdminServer"
else
    isManagedServer="true"
    UNIQUE_IDENTIFIER=$WLS_MANAGED_UNIQUE_IDENTIFIER
    ADMIN_URL=$WLS_ADMIN_URL
    hostPort=$WLS_MANAGED_PORT
    nodeBaseName=`echo $nodeName | cut -d. -f1`
    managedServerName=WLS_${nodeBaseName}_SERVER_NAME
    SERVER_NAME=`eval echo '$'$managedServerName`
fi

hostURL=http://${nodeName}

printf "INFORMATION: Value of variable SERVER_NAME       is \"${SERVER_NAME}\"       [${TSCR},${LINENO}]\n"
printf "INFORMATION: Value of variable UNIQUE_IDENTIFIER is \"${UNIQUE_IDENTIFIER}\" [${TSCR},${LINENO}]\n"
printf "INFORMATION: Value of variable hostURL           is \"${hostURL}\"           [${TSCR},${LINENO}]\n"

###########################################
# Setting up the project specific details #
###########################################
db2ClassPath=${DB2Home}/java/db2jcc4.jar
db2ClassPath=${db2ClassPath}:${DB2Home}/java/db2jcc_license_cu.jar

teraClassPath=${LIB}/invadj-rdt-admin/terajdbc4.jar
teraClassPath=${teraClassPath}:${LIB}/invadj-rdt-admin/tdgssconfig.jar

applicationClassPath=${db2ClassPath}:${teraClassPath}

applicationJavaOptionsCommon="-Djava.net.preferIPv4Stack=true -DFEDEXJMS_CACHE_FILE_DIR=${FEDEXJMS_CACHE_FILE_DIR}"

###########################################

case "${UNIQUE_IDENTIFIER}" in
    rdtWebSvc)
        dependanciesHome="${LIB}/${GRS_PROJECT_NAME}/dependencies"

        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-dom-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltwls-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltjse-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/jolt-10gR3.jar
        applicationClassPath=${applicationClassPath}:${CFG}/${GRS_PROJECT_NAME}
        applicationClassPath=${applicationClassPath}:${APPLICATION_P12_DIR}/rdt_webservice_domain

        enableConfigDir="${CFG}/enable-websvc/appconfig/${DEPLOYMENT_LEVEL}/"

        applicationJavaOptions="-DRDT_CONFIG_DIR=${CFG}/${GRS_PROJECT_NAME}/ -DRDT_1SOURCE_CONFIG_DIR=${CFG}/rdt-1source-ear/"
        applicationJavaOptions="${applicationJavaOptions} "
        applicationJavaOptions="${applicationJavaOptions} -Djavax.xml.soap.MessageFactory=oracle.j2ee.ws.saaj.soap.MessageFactoryImpl"
        applicationJavaOptions="${applicationJavaOptions} -Dtangosol.coherence.clusteraddress=$RDT_WEBSVC_COHERENCE_CLUSTER_ADDRESS  -Dtangosol.coherence.clusterport=$RDT_WEBSVC_COHERENCE_CLUSTER_PORT"
        applicationJavaOptions="${applicationJavaOptions} -Dtangosol.coherence.cacheconfig=$CFG/rdt-webservice/coherence-cache-override-config.xml"
        applicationJavaOptions="${applicationJavaOptions} -DTOKEN_DECOUPLE_SWITCH=$TOKEN_DECOUPLE_SWITCH"
        applicationJavaOptions="${applicationJavaOptions} -DRDT_CUSTOMER_CONFIG_DIR=${CFG}/rdt-customer-services/"
        applicationJavaOptions="${applicationJavaOptions} ${applicationJavaOptionsCommon}"
        applicationJavaOptions="${applicationJavaOptions} -Dweblogic.management.clearTextCredentialAccessEnabled=true"
    ;;
    mock-webservice)
        dependanciesHome="${LIB}/${GRS_PROJECT_NAME}/dependencies"

        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-dom-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltwls-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltjse-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/jolt-10gR3.jar

        echo 'Setting in for mock-webservice'
    ;;
    invadjWebSvc)
        dependanciesHome="${LIB}/rdt-webservice/dependencies"

        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-dom-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltwls-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltjse-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/jolt-10gR3.jar
        applicationClassPath=${applicationClassPath}:${CFG}/revenue.desktop.server
        applicationClassPath=${applicationClassPath}:${CFG}/rdt-customer-services
        applicationClassPath=${applicationClassPath}:${CFG}/rdt-customer-services/properties
        applicationClassPath=${applicationClassPath}:${APPLICATION_P12_DIR}/invadj_websvc_domain

        applicationJavaOptions="-DIRDTSERVER_APP_CONFIG_DIR=${CFG}/revenue.desktop.server/"
        applicationJavaOptions="${applicationJavaOptions} -DINVADJ_APP_CONFIG_DIR=${CFG}/invoice-adjustments/invoice-adjustments-webapp/"
        applicationJavaOptions="${applicationJavaOptions} -Djavax.xml.soap.MessageFactory=oracle.j2ee.ws.saaj.soap.MessageFactoryImpl"
        applicationJavaOptions="${applicationJavaOptions} -DTOKEN_DECOUPLE_SWITCH=$TOKEN_DECOUPLE_SWITCH"
        applicationJavaOptions="${applicationJavaOptions} ${applicationJavaOptionsCommon}"
        applicationJavaOptions="${applicationJavaOptions} -DRDT_CUSTOMER_CONFIG_DIR=${CFG}/rdt-customer-services/"
        applicationJavaOptions="${applicationJavaOptions} -Dweblogic.management.clearTextCredentialAccessEnabled=true"
    ;;
    rdtMDB)
        dependanciesHome="${LIB}/rdt-webservice/dependencies"

        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-dom-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltwls-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltjse-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/jolt-10gR3.jar
        applicationClassPath=${applicationClassPath}:${APPLICATION_P12_DIR}/rdt_mdbservice_domain
        applicationJavaOptions="-DRDT_CONFIG_DIR=${CFG}/${GRS_PROJECT_NAME}/"
        applicationJavaOptions="${applicationJavaOptions} -DINVADJ_APP_CONFIG_DIR=${CFG}/invoice-adjustments/invoice-adjustments-ear/"
        applicationJavaOptions="${applicationJavaOptions} -DIRDTSERVER_APP_CONFIG_DIR=${CFG}/revenue.desktop.server/"
        applicationJavaOptions="${applicationJavaOptions} -Djavax.xml.soap.MessageFactory=oracle.j2ee.ws.saaj.soap.MessageFactoryImpl"
        applicationJavaOptions="${applicationJavaOptions} -DUseSunHttpHandler=true"
        applicationJavaOptions="${applicationJavaOptions} ${applicationJavaOptionsCommon}"
    ;;
    rdtDomAdj)
        export GRS_PROJECT_NAME2=rdt-mug-websvc
        dependanciesHome="${LIB}/rdt-dom-adjustments/dependencies"

        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-dom-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltwls-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltjse-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/jolt-10gR3.jar
        applicationClassPath=${applicationClassPath}:${CFG}/rdt-dom-adjustments
        applicationClassPath=${applicationClassPath}:${CFG}/rdt-dom-adjustments/properties
        applicationClassPath=${applicationClassPath}:${APPLICATION_P12_DIR}/rdt_dom_adjustments_domain
        applicationJavaOptions="-DRDT_CONFIG_DIR=${CFG}/${GRS_PROJECT_NAME}/"
        applicationJavaOptions="${applicationJavaOptions} -DRDTMUG_CONFIG_DIR=${CFG}/${GRS_PROJECT_NAME2}/"
        applicationJavaOptions="${applicationJavaOptions} -DRDT_CUSTOMER_CONFIG_DIR=${CFG}/rdt-customer-services/"
        applicationJavaOptions="${applicationJavaOptions} -Djavax.xml.soap.MessageFactory=oracle.j2ee.ws.saaj.soap.MessageFactoryImpl"
        applicationJavaOptions="${applicationJavaOptions} ${applicationJavaOptionsCommon}"
    ;;
    rdtNewSvc)
        dependanciesHome="${LIB}/rdt-webservice/dependencies"

        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-dom-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltwls-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltjse-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/jolt-10gR3.jar
        applicationClassPath=${applicationClassPath}:${APPLICATION_P12_DIR}/rdt_new_services_domain
        applicationJavaOptions="${applicationJavaOptions} -Djavax.xml.soap.MessageFactory=oracle.j2ee.ws.saaj.soap.MessageFactoryImpl"
        applicationJavaOptions="${applicationJavaOptions} ${applicationJavaOptionsCommon}"
        applicationJavaOptions="${applicationJavaOptions} -DRDT_CUSTOMER_CONFIG_DIR=${CFG}/rdt-customer-services/"
        applicationJavaOptions="${applicationJavaOptions} -DRDT_EDI_AUTOADJUST_CONFIG_DIR=${CFG}/rdt-edi-autoadjust/"
        applicationJavaOptions="${applicationJavaOptions} -DUseSunHttpHandler=true"
    ;;
    rdtBatchSvc)
        dependanciesHome="${LIB}/rdt-webservice/dependencies"

        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-dom-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltwls-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltjse-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/jolt-10gR3.jar
        applicationClassPath=${applicationClassPath}:${APPLICATION_P12_DIR}/rdt_batchsvc_domain
        applicationJavaOptions="${applicationJavaOptions} -Djavax.xml.soap.MessageFactory=oracle.j2ee.ws.saaj.soap.MessageFactoryImpl"
        applicationJavaOptions="${applicationJavaOptions} ${applicationJavaOptionsCommon}"
        applicationJavaOptions="${applicationJavaOptions} -DRDT_CUSTOMER_CONFIG_DIR=${CFG}/rdt-customer-services/"
        applicationJavaOptions="${applicationJavaOptions} -DUseSunHttpHandler=true"
    ;;
    rdtThinClientWebSvc)
        dependanciesHome="${LIB}/rdt-webservice/dependencies"

        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-dom-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/saxon-8.7.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltwls-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/joltjse-10gR3.jar
        applicationClassPath=${applicationClassPath}:${dependanciesHome}/jolt-10gR3.jar
        applicationClassPath=${applicationClassPath}:${APPLICATION_P12_DIR}/rdt_thinclient_domain
        applicationJavaOptions="${applicationJavaOptions} -Djavax.xml.soap.MessageFactory=oracle.j2ee.ws.saaj.soap.MessageFactoryImpl"
        applicationJavaOptions="${applicationJavaOptions} ${applicationJavaOptionsCommon}"
        applicationJavaOptions="${applicationJavaOptions} -DRDT_CUSTOMER_CONFIG_DIR=${CFG}/rdt-customer-services/"
        applicationJavaOptions="${applicationJavaOptions} -DRDT_EDI_AUTOADJUST_CONFIG_DIR=${CFG}/rdt-edi-autoadjust/"
        applicationJavaOptions="${applicationJavaOptions} -DUseSunHttpHandler=true"
    ;;
    rdtWebSvcAdmin)
        echo "Project is: ${UNIQUE_IDENTIFIER}"
    ;;
    rdtMDBAdmin)
        echo "Project is: ${UNIQUE_IDENTIFIER}"
    ;;
    invadjWebSvcAdmin)
        echo "Project is: ${UNIQUE_IDENTIFIER}"
    ;;
    rdtDomAdjAdmin)
        echo "Project is: ${UNIQUE_IDENTIFIER}"
    ;;
    rdtNewSvcAdmin)
        echo "Project is: ${UNIQUE_IDENTIFIER}"
    ;;
    rdtThinClientAdmin)
        echo "Project is: ${UNIQUE_IDENTIFIER}"
    ;;
    rdtBatchSvcAdmin)
        echo "Project is: ${UNIQUE_IDENTIFIER}"
    ;;
    *)
        printf "\n[ERROR user=`id --user --name` on `hostname` at `date`] value of variable [UNIQUE_IDENTIFIER=${UNIQUE_IDENTIFIER}] something we do not know what to do with; help; this is a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    ;;
esac

MEM_ARGS="undefined"
case "${UNIQUE_IDENTIFIER}" in
    rdtWebSvcAdmin)
        MEM_ARGS="-Xms256m -Xmx256m"
    ;;
    rdtMDBAdmin)
        MEM_ARGS="-Xms256m -Xmx256m"
    ;;
    invadjWebSvcAdmin)
        MEM_ARGS="-Xms256m -Xmx256m"
    ;;
    rdtDomAdjAdmin)
        MEM_ARGS="-Xms256m -Xmx256m"
    ;;
    rdtNewSvcAdmin|rdtBatchSvcAdmin)
        MEM_ARGS="-Xms256m -Xmx256m"
        MEM_ARGS="${MEM_ARGS} -XX:-OmitStackTraceInFastThrow"
    ;;
    rdtThinClientAdmin)
        MEM_ARGS="-Xms256m -Xmx256m"
    ;;
    *)
        printf "\n[ERROR user=`id --user --name` on `hostname` at `date`] unknown [UNIQUE_IDENTIFIER=${UNIQUE_IDENTIFIER}] something we do not know what to do with; help; this is a problem. [${TSCR},${LINENO}]\n\n"
    ;;
esac

case "${UNIQUE_IDENTIFIER}" in
    rdtMDB|rdtDomAdj|invadjWebSvc|rdtWebSvc)
        if [ "xbnch" = "x${ENV_PHASE}" ] || [ "xprod" = "x${ENV_PHASE}" ] ; then
            MEM_ARGS="-Xms4096m -Xmx4096m"
        elif [ "xprodtest" = "x${ENV_PHASE}" ] ; then
            MEM_ARGS="-Xms1024m -Xmx1024m"
        else
            MEM_ARGS="-Xms512m -Xmx512m"
        fi
    ;;
    rdtBatchSvc)
        if [ "xbnch" = "x${ENV_PHASE}" ] || [ "xprod" = "x${ENV_PHASE}" ] ; then
            MEM_ARGS="-Xms4096m -Xmx4096m"
        elif [ "xprodtest" = "x${ENV_PHASE}" ] ; then
            MEM_ARGS="-Xms4096m -Xmx4096m"
        else
            MEM_ARGS="-Xms512m -Xmx512m"
        fi
    ;;
    rdtThinClientWebSvc)
        if [ "xbnch" = "x${ENV_PHASE}" ] || [ "xprod" = "x${ENV_PHASE}" ] ; then
            MEM_ARGS="-Xms4096m -Xmx4096m"
        elif [ "xprodtest" = "x${ENV_PHASE}" ] ; then
            MEM_ARGS="-Xms4096m -Xmx4096m"
        else
            MEM_ARGS="-Xms512m -Xmx512m"
        fi
    ;;
    rdtNewSvc)
        if [ "xbnch" = "x${ENV_PHASE}" ] || [ "xprod" = "x${ENV_PHASE}" ] ; then
            MEM_ARGS="-Xms6144m -Xmx6144m"
        elif [ "xprodtest" = "x${ENV_PHASE}" ] ; then
            MEM_ARGS="-Xms4096m -Xmx4096m"
        else
            MEM_ARGS="-Xms512m -Xmx512m"
        fi
    ;;
esac

export MEM_ARGS="${MEM_ARGS} -XX:+UseConcMarkSweepGC -Xloggc:${LOGS}/${GRS_PROJECT_NAME}/applog/${GRS_PROJECT_NAME}-gc.log -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=5m -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCApplicationStoppedTime -XX:+PrintGCApplicationConcurrentTime -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=${LOGS}/${GRS_PROJECT_NAME}/applog/ -XX:SurvivorRatio=6 -XX:-UseAdaptiveSizePolicy -XX:+UnlockCommercialFeatures -XX:+FlightRecorder -XX:FlightRecorderOptions=defaultrecording=true,maxsize=300m,disk=true,repository=${LOGS}/${GRS_PROJECT_NAME}/applog/flight_recordings_repository,dumponexit=true,dumponexitpath=${LOGS}/${GRS_PROJECT_NAME}/applog/flight_recordings_repository/myFlightRecording.jfr"

#############################################################################

export SERVER_NAME
export UNIQUE_IDENTIFIER
export ADMIN_URL

REQUIRES_SAAJ=${requiresSaaj}
export REQUIRES_SAAJ

REQUIRES_JOLT=${requiresJolt}
export REQUIRES_JOLT

SERVER_URL=${hostURL}:${hostPort}
export SERVER_URL

APPLICATION_JAVA_OPTIONS=${applicationJavaOptions}
export APPLICATION_JAVA_OPTIONS

APPLICATION_CLASSPATH=${applicationClassPath}
export APPLICATION_CLASSPATH

WLS_USER=${wlsUser}
export WLS_USER

WLS_PWD=${wlsPw}
export WLS_PWD

printf "Exiting the script at `date` on `hostname` [${TSCR},${LINENO}]\n"

