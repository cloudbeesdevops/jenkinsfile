#!/bin/ksh

. $SCRIPTS/invadj-rdt-admin/wls_profile.sh

echo

export WL_HOME=${RDT_WL_HOME}

printf "[INFO `date`] WL_HOME=${WL_HOME} [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"

CLASSPATH=${WL_HOME}/server/lib/weblogic.jar:${CLASSPATH}
export CLASSPATH
printf "[INFO `date`] CLASSPATH=${CLASSPATH} [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"

if [ -d ${APP}/web/weblogic/config/${WLS_DOMAIN_NAME} ]
then
    rm -r ${APP}/web/weblogic/config/${WLS_DOMAIN_NAME}
else
    mkdir -m 775 ${APP}/web/weblogic/config/${WLS_DOMAIN_NAME}
fi
printf "[INFO `date`] domain_directory=${APP}/web/weblogic/config/${WLS_DOMAIN_NAME} [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"

JAVA_OPTIONS=" -Dpython.cachedir=${APP}/web/weblogic/config/${WLS_DOMAIN_NAME}/wlstTemp ${JAVA_OPTIONS}"
export JAVA_OPTIONS

PATH=${JAVA_HOME}/bin:/bin:/usr/bin
export PATH
printf "[INFO `date`] PATH=${PATH} [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"

if [ -f $CFG/chdaOraVariables.sh ]
then
    . $CFG/chdaOraVariables.sh
    expandOracleVariables="true"
fi

if [ -f $CFG/endaOraVariables.sh ]
then
    . $CFG/endaOraVariables.sh
    expandOracleVariables="true"
fi

if [ "${expandOracleVariables}" != "true" ]
then
    echo "DA patch does not have the OraVariables.sh file"
fi

printf "[INFO `date`] GRS_PROJECT_NAME=${GRS_PROJECT_NAME} [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"

${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invadj-rdt-admin/${GRS_PROJECT_NAME}/domain.properties.template $CFG/invadj-rdt-admin/domain.properties
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invadj-rdt-admin/common/boot.identity.template                  $CFG/invadj-rdt-admin/boot.identity

#######################################################################
# Some certificate work now for the /var directory
#######################################################################
src_certs_dir=${DATA}/certs/`basename ${APPLICATION_P12_DIR}`
dst_certs_dir=${DATA}/framework/`basename ${APPLICATION_P12_DIR}`/${WLS_DOMAIN_NAME}

mkdir -pv ${dst_certs_dir}
if [ 0 -eq $? ] ; then
    printf "[INFO `date`] Command \"mkdir -pv ${dst_certs_dir} \" worked [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"
else
    printf "\n[ERROR `date`] Command \"mkdir -pv ${dst_certs_dir} \" FAILED [invadj-rdt-admin_createDomain.sh,${LINENO}]\n\n"
    exit 1
fi

cp -v ${src_certs_dir}/* ${dst_certs_dir}/
if [ 0 -eq $? ] ; then
    printf "[INFO `date`] Command \"cp -v ${src_certs_dir}/* ${dst_certs_dir}/ \" worked [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"
    chmod -v ug+rw ${dst_certs_dir}/*
else
    printf "\n[ERROR `date`] Command \"cp -v ${src_certs_dir}/* ${dst_certs_dir}/ \" FAILED [invadj-rdt-admin_createDomain.sh,${LINENO}]\n\n"
    exit 1
fi

src_certs_fqcpfile=${dst_certs_dir}/client.properties.template
dst_certs_fqcpfile=${dst_certs_dir}/client.properties
cert_search_str="@@P12DIR@@"

> ${dst_certs_fqcpfile}
if [ 0 -ne $? ] ; then
    printf "\n[ERROR `date`] Command \" > ${dst_certs_fqcpfile} \" FAILED [invadj-rdt-admin_createDomain.sh,${LINENO}]\n\n"
    exit 1
fi

chmod ug+rw ${dst_certs_fqcpfile} > /dev/null 2>&1

cat ${src_certs_fqcpfile} | sed -e "s|${cert_search_str}|${dst_certs_dir}|" > ${dst_certs_fqcpfile}
cpfile2check=$(grep client.keystore.file ${dst_certs_fqcpfile} | cut -f2 -d'=')
if [ -f ${cpfile2check} ] && [ -r ${cpfile2check} ] ; then
    printf "[INFO `date`] client-properties-file\n[`ls -l ${cpfile2check}`]\nin\n${dst_certs_fqcpfile}\nis good. [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"
else
    printf "[ERROR `date`] client-properties-file\n[`ls -l ${cpfile2check}`]\nin\n${dst_certs_fqcpfile}\nis BAD. [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"
    exit 1
fi

#######################################################################
# WebLogic Scripting Tool creates the Domain
#######################################################################

printf "[INFO `date`] Using java to call weblogic.WLST with ${SCRIPTS}/invadj-rdt-admin/createDomain.py [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"
${RDT_JAVA_HOME}/bin/java -classpath ${CLASSPATH} ${JAVA_OPTIONS} weblogic.WLST ${SCRIPTS}/invadj-rdt-admin/createDomain.py

if [ "${GRS_PROJECT_NAME}" = "rdt-mdbservice" ]
then
    ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invadj-rdt-admin/${GRS_PROJECT_NAME}/jmsConfig.xml.template $APP/web/weblogic/config/${WLS_DOMAIN_NAME}/config/jms/jmsConfig-jms.xml
    cp -v $LIB/invadj-rdt-admin/fedexjms*.jar ${WEB}/weblogic/config/rdt_mdbservice_domain/lib
    cp -v $LIB/invadj-rdt-admin/tib*.jar ${WEB}/weblogic/config/rdt_mdbservice_domain/lib
fi

printf "[INFO `date`] domain_configuration_file [`ls -li --full-time ${APP}/web/weblogic/config/${WLS_DOMAIN_NAME}/config/config.xml`] [invadj-rdt-admin_createDomain.sh,${LINENO}]\n"

exit 0

