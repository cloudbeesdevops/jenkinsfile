import javax.management.Attribute
from javax.management import *
from java.util import *
from java.io import *
import sys,re
import jarray
import ConfigParser 
 
print "Preparing ConfigParser"
config = ConfigParser.ConfigParser()
config.read('../../cfg/invadj-rdt-admin/application.properties')
sectionList = config.sections()

print 'Loading Properties'
#loadProperties('../../infiles/invadj-rdt-admin/application.properties')
loadProperties('../../cfg/invadj-rdt-admin/boot.identity')

for section in sectionList:
  if section.startswith('APP'):
    adminUrl = config.get(section,'adminUrl')
    connect(url=adminUrl, adminServerName='AdminServer', username=username, password=password)
    break
  else:
      pass

config = ConfigParser.ConfigParser()
config.read('../../cfg/invadj-rdt-admin/application.properties')
sectionList = config.sections()
###############################################################################
# Redeploy Applications.
###############################################################################

for section in sectionList:
  print section
  if section.startswith('APP'):
    appName = config.get(section,'appName')
    print 'Redeploying Application  [' + appName + ']'
    appSourcePath = config.get(section,'appSourcePath')
    print 'Setting  Source Path  [' + appSourcePath + ']'
    appTarget = config.get(section,'appTargets')
    print 'Targeting Application [' + appName + '] to Target [' + appTarget + ']'
    undeploy(appName, timeout=0)
    redeploy(appName, appPath=appSourcePath, targets=appTarget)


###############################################################################
# Exit WLST.
###############################################################################

exit()