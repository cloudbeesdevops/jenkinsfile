###############################################################################
# Creates WebLogic Domain.
###############################################################################

###############################################################################
# Open a Domain Template.
###############################################################################

import javax.management.Attribute
from javax.management import *
from java.util import *
from java.io import *
import sys,re
import jarray
import ConfigParser

print "Preparing ConfigParser"
config = ConfigParser.ConfigParser()
config.read('../../cfg/invadj-rdt-admin/domain.properties')
sectionList = config.sections()

print 'Loading Properties'
loadProperties('../../cfg/invadj-rdt-admin/boot.identity')

###############################################################################
# Read Certificate Data.
###############################################################################

for section in sectionList:
  if section.startswith('CERT'):
    keyStore = config.get(section,'keyStore')
    customIdentityKeyStoreFileName = config.get(section,'customIdentityKeyStoreFileName')
    customIdentityKeyStoreType = config.get(section,'customIdentityKeyStoreType')
    customIdentityKeyStorePassPhrase = config.get(section,'customIdentityKeyStorePassPhrase')
    customTrustKeyStoreFileName =  config.get(section,'CustomTrustKeyStoreFileName')
    customTrustKeyStoreType =  config.get(section,'CustomTrustKeyStoreType')
    customTrustKeyStorePassPhrase =  config.get(section,'CustomTrustKeyStorePassPhrase')
    serverPrivateKeyAlias = config.get(section,'serverPrivateKeyAlias')
    serverPrivateKeyPassPhrase = config.get(section,'serverPrivateKeyPassPhrase')

###############################################################################
# Read Template.
###############################################################################

for section in sectionList:
  if section.startswith('TEMPLATE'):
    templateFile = config.get(section,'templateFile')
    print 'Reading Template File [' + templateFile + ']'
    readTemplate(templateFile)

###############################################################################
# Domain Settings.
###############################################################################

for section in sectionList:
  if section.startswith('DOMAIN'):
    domainLocation = config.get(section,'domainLocation')
    overwriteDomain = config.get(section,'overwriteDomain')
    javaHome = config.get(section,'javaHome')
    serverStartMode = config.get(section,'serverStartMode')

    print ''
    print 'Setting Domain Options >>>>>'
    setOption('OverwriteDomain', overwriteDomain)
    print 'Setting Java Home at [' + javaHome + ']'
    setOption('JavaHome', javaHome)
    setOption('ServerStartMode', serverStartMode)

###############################################################################
# Configure the Administration Server and SSL port.
#
# To enable access by both local and remote processes, you should not
# set the listen address for the server instance (that is, it should
# be left blank or not set).  In this case, the server instance will
# determine the address of the machine and listen on it.
###############################################################################

for section in sectionList:
  if section.startswith('ADMIN'):
    adminServerListenAddres = config.get(section,'adminServerListenAddres')
    adminPort = config.get(section,'adminPort')
    adminSslPort = config.get(section,'adminSslPort')
    adminServerName = config.get(section,'adminServerName')
    httpTunnelling = config.get(section,'httpTunnelling')

    print ''
    print 'Setting Listen Port for AdminServer at [' + adminPort + ']'
    cd('/Servers/AdminServer')
#QC184955 - artf356290#
    set('ListenAddress', adminServerListenAddres)
    set('ListenPort', Integer.parseInt(adminPort))
    print 'Enabling http Tunneling to [' + httpTunnelling + ']'
    set('TunnelingEnabled', httpTunnelling)
    print 'Setting the Key Store'
    set('KeyStore' ,keyStore)
    set('CustomIdentityKeyStoreFileName', customIdentityKeyStoreFileName)
    set('CustomIdentityKeyStoreType', customIdentityKeyStoreType)
    set('CustomIdentityKeyStorePassPhraseEncrypted', customIdentityKeyStorePassPhrase)

    create('AdminServer', 'SSL')
    cd('SSL/AdminServer')
    set('Enabled', 'False')
    print 'Setting Private Key Alias'
    set('ServerPrivateKeyAlias', serverPrivateKeyAlias)
    set('ServerPrivateKeyPassPhraseEncrypted', serverPrivateKeyPassPhrase)

###############################################################################
# Create Managed Servers.
###############################################################################

for section in sectionList:
  if section.startswith('MSLIST'):
    managedServers = config.get(section,'managedServers')
    msiEnabled = config.get(section,'msiEnabled')
    managedPort = config.get(section,'managedPort')
    managedSSLPort = config.get(section,'managedSSLPort')
    httpTunnelling = config.get(section,'httpTunnelling')

    print ''
    print 'Creating Managed Servers >>>>>'

    print 'ManagedServers are [' + managedServers + ']'

    mslist = re.split(',', managedServers)

    msserverlist = ""

    for ms in mslist:
      cd('/')
      s = String(ms)
      managedserver= s.substring(0, ms.index(':'))
      msserverlist = msserverlist + managedserver + ","
      listenAddress=s.substring(ms.index(':') + 1, s.length())

      print ''
      print 'Creating Managed Server [' + managedserver + ':' + managedPort + ']'
      create(managedserver, 'Server')
      cd('Servers/' + managedserver)
      set('ListenPort', Integer.parseInt(managedPort))
      set('ListenAddress', listenAddress)
      print 'Enabling http Tunneling to [' + httpTunnelling + ']'
      set('TunnelingEnabled', httpTunnelling)

      print 'Setting the Key Store'
      set('KeyStore' ,keyStore)
      set('CustomIdentityKeyStoreFileName', customIdentityKeyStoreFileName)
      set('CustomIdentityKeyStoreType', customIdentityKeyStoreType)
      set('CustomIdentityKeyStorePassPhraseEncrypted', customIdentityKeyStorePassPhrase)

      print 'Setting Trust Store Info'
      set ('CustomTrustKeyStoreFileName', customTrustKeyStoreFileName)
      set ('CustomTrustKeyStoreType', customTrustKeyStoreType)
      set ('CustomTrustKeyStorePassPhraseEncrypted', customTrustKeyStorePassPhrase)
      
      create(managedserver, 'SSL')
      cd('SSL/' + managedserver)
      set('Enabled', 'True')
      set('ListenPort', Integer.parseInt(managedSSLPort))
      print 'Setting Private Key Alias'
      set('ServerPrivateKeyAlias', serverPrivateKeyAlias)
      set('ServerPrivateKeyPassPhraseEncrypted', serverPrivateKeyPassPhrase)

###############################################################################
# Define the user password for WebLogic.
###############################################################################

cd('/')

print ''
print 'Setting Password for User [' + username + ']'

cd('Security/base_domain/User/' + username)
cmo.setPassword(password)

###############################################################################
# Oracle Data Sources.
###############################################################################

for section in sectionList:
  if section.startswith('DATASRC'):

    dataSource = config.get(section,'dataSource')
    dsJndiName = config.get(section,'dsJndiName')
    driverName = config.get(section,'driverName')
    dsUrl = config.get(section,'dsUrl')
    dsUser = config.get(section,'dsUser')
    dsPassword = config.get(section,'dsPassword')
    dsServer = config.get(section,'dsServer')
    dsPort = config.get(section,'dsPort')
    dsTestTableName = config.get(section,'dsTestTableName')
    dsTargets = config.get(section,'dsTargets')
    try:
      dsDatabaseName = config.get(section,'dsDatabaseName')
    except:
      dsDatabaseName = 'null'
    try:
      dsSid = config.get(section,'dsSid')
    except:
      dsSid = 'null'

    cd('/')

    cd('/')
    print ''
    print 'Creating JDBC Data Source [' + dataSource + ']'
    cmo = create(dataSource, 'JDBCSystemResource')

    cd('JDBCSystemResource/' + dataSource + '/JdbcResource/' + dataSource)
    cmo = create('myJdbcDriverParams', 'JDBCDriverParams')
    print 'Setting Driver Name [' + driverName + ']'
    cmo.setDriverName(driverName)
    print 'Setting URL [' + dsUrl + ']'
    cmo.setUrl(dsUrl)
    cmo.setPasswordEncrypted(dsPassword)
    cmo.setUseXaDataSourceInterface(false)
    cd('JDBCDriverParams/NO_NAME')
    cmo = create('myProps', 'Properties')
    cd('Properties/NO_NAME')
    cmo = create('user', 'Property')
    cd('Property/user')
    cmo.setValue(dsUser)
    cd('../..')
    cmo = create('url', 'Property')
    cd('Property/url')
    cmo.setValue(dsUrl)
    cd('../..')
    cmo = create('portNumber', 'Property')
    cd('Property/portNumber')
    cmo.setValue(dsPort)

    if dsDatabaseName != 'null':
      print 'Setting Database Name [' + dsDatabaseName + ']'
      cd('../..')
      cmo = create('databaseName', 'Property')
      cd('Property/databaseName')
      cmo.setValue(dsDatabaseName)

    if dsSid != 'null':
      print 'Setting SID [' + dsSid + ']'
      cd('../..')
      cmo = create('SID', 'Property')
      cd('Property/SID')
      cmo.setValue(dsSid)

    cd('../..')

    cmo = create('serverName', 'Property')
    cd('Property/serverName')
    cmo.setValue(dsServer)

    cd('/JDBCSystemResource/' + dataSource + '/JdbcResource/' + dataSource)
    cmo = create('myJdbcDataSourceParams', 'JDBCDataSourceParams')
    print 'Setting JNDI Name [' + dsJndiName + ']'
    jndiNames = jarray.array([dsJndiName], java.lang.String)
    cmo.setJNDINames(jndiNames)

    cd('/JDBCSystemResource/' + dataSource + '/JdbcResource/' + dataSource)
    cmo = create('myJdbcConnectionPoolParams', 'JDBCConnectionPoolParams')
    cmo.setTestTableName(dsTestTableName)
  
    try:
      dsInitialCapacity = config.get(section,'dsInitialCapacity')
    except:
      dsInitialCapacity = 'null'

    print 'Initial Capacity is [' + dsInitialCapacity + ']'
    if dsInitialCapacity != 'null':
      cmo.setInitialCapacity(Integer.parseInt(dsInitialCapacity))

    try:
      dsMaxCapacity = config.get(section,'dsMaxCapacity')
    except:
      dsMaxCapacity = 'null'

    if dsMaxCapacity != 'null':
      cmo.setMaxCapacity(Integer.parseInt(dsMaxCapacity))

    try:
      dsCapacityIncrement = config.get(section,'dsCapacityIncrement')
    except:
      dsCapacityIncrement = 'null'

    if dsCapacityIncrement != 'null':
      cmo.setCapacityIncrement(Integer.parseInt(dsCapacityIncrement))
    
    try:
      dsConnectionCreationRetryFrequencySeconds = config.get(section,'dsConnectionCreationRetryFrequencySeconds')
    except:
      dsConnectionCreationRetryFrequencySeconds = 'null'

    if dsConnectionCreationRetryFrequencySeconds != 'null':
      cmo.setConnectionCreationRetryFrequencySeconds(Integer.parseInt(dsConnectionCreationRetryFrequencySeconds))
      
    try:
      dsConnectionReserveTimeoutSeconds = config.get(section,'dsConnectionReserveTimeoutSeconds')
    except:
      dsConnectionReserveTimeoutSeconds = 'null'

    if dsConnectionReserveTimeoutSeconds != 'null':
      cmo.setConnectionReserveTimeoutSeconds(Integer.parseInt(dsConnectionReserveTimeoutSeconds))

    try:
      dsInactiveConnectionTimeoutSeconds = config.get(section,'dsInactiveConnectionTimeoutSeconds')
    except:
      dsInactiveConnectionTimeoutSeconds = 'null'

    if dsInactiveConnectionTimeoutSeconds != 'null':
      cmo.setInactiveConnectionTimeoutSeconds(Integer.parseInt(dsInactiveConnectionTimeoutSeconds))

    cmo.setIgnoreInUseConnectionsEnabled(true)
    cmo.setPinnedToThread(false)
    cmo.setRemoveInfectedConnections(true)
    
    try:
      dsSecondsToTrustAnIdlePoolConnection = config.get(section,'dsSecondsToTrustAnIdlePoolConnection')
    except:
      dsSecondsToTrustAnIdlePoolConnection = 'null'

    if dsSecondsToTrustAnIdlePoolConnection != 'null':
      cmo.setSecondsToTrustAnIdlePoolConnection(Integer.parseInt(dsSecondsToTrustAnIdlePoolConnection))
      
    try:
      dsStatementTimeout = config.get(section,'dsStatementTimeout')
    except:
      dsStatementTimeout = 'null'

    if dsStatementTimeout != 'null':
      cmo.setStatementTimeout(Integer.parseInt(dsStatementTimeout))
    
    cmo.setTestConnectionsOnReserve(true)
    
    try:
      dsTestFrequencySeconds = config.get(section,'dsTestFrequencySeconds')
    except:
      dsTestFrequencySeconds = 'null'

    if dsTestFrequencySeconds != 'null':
      cmo.setTestFrequencySeconds(Integer.parseInt(dsTestFrequencySeconds))
      
    cd('/')
    print ''
    targetslist = re.split(',', dsTargets)
    for dsTarget in targetslist:
        print 'Assigning Data Source [' + dataSource + '] to Target [' + dsTarget + ']'
        assign('JDBCSystemResource', dataSource, 'Target', dsTarget)


###############################################################################
# Assigning Startup classes.
###############################################################################
for section in sectionList:
  if section.startswith('STARTUPCLASS'):
  
    startupClassName = config.get(section,'startupClassName')
    startupClass = config.get(section,'startupClass')
    startupTargets = config.get(section,'startupTargets')

    cd('/')
    print ''
    print 'Creating Startup Class [' + startupClassName + ']'
    cmo = create(startupClassName, 'StartupClass')
    cmo.setClassName(startupClass)

    cd('/')
    print ''
    targetslist = re.split(',', startupTargets)
    for target in targetslist:
        print 'Assining Startup Class [' + startupClass + '] to [' + target + ']'
        assign('StartupClass', startupClassName, 'Target', target)

###############################################################################
# Assigning Shutdown classes.
###############################################################################

for section in sectionList:
  if section.startswith('SHUTDOWNCLASS'):
  
    shutdownClassName = config.get(section,'shutdownClassName')
    shutdownClass = config.get(section,'shutdownClass')
    shutdownTargets = config.get(section,'shutdownTargets')
    
    cd('/')
    print ''
    print 'Creating Startup Class [' + shutdownClassName + ']'
    cmo = create(shutdownClassName, 'ShutdownClass')
    cmo.setClassName(shutdownClass)

    cd('/')
    print ''
    targetslist = re.split(',', shutdownTargets)
    for target in targetslist:
        print 'Assining Shutdown Class [' + shutdownClass + '] to [' + target + ']'
        assign('ShutdownClass', shutdownClassName, 'Target', target)

###############################################################################
# JMS System Resource.
###############################################################################
for section in sectionList:
  if section.startswith('JMS'):
    jmsModule = config.get(section,'jmsModule')
    jmsConfigFile = config.get(section,'jmsConfigFile')
    jmsModuleTargets = config.get(section,'jmsModuleTargets')

    cd('/')
    print ''
    print 'Creating JMS Module [' + jmsModule + ']'
    cmo = create(jmsModule, 'JMSSystemResource')
    
    cd('JMSSystemResource/' + jmsModule)
    set('DescriptorFileName', jmsConfigFile)

    targetslist = re.split(',', jmsModuleTargets)
    for target in targetslist:
        print 'Assining JMS Module [' + jmsModule + '] to [' + target + ']'
#        set('Target', target)
        assign('JMSSystemResource', jmsModule, 'Target', target)
    


#for section in sectionList:
#  if section.startswith('JMS'):

#    jmsModule = config.get(section,'jmsModule')
#    foreignServer = config.get(section,'foreignServer')
#    initialContextFactory = config.get(section,'initialContextFactory')
#    connectionURL = config.get(section,'connectionURL')

#    cd('/')
#    print ''
#    print 'Creating JMS Module [' + jmsModule + ']'
#    cmo = create(jmsModule, 'JMSSystemResource')

#    cd('JMSSystemResource/' + jmsModule + '/weblogicJms/')
#    cmo = create(foreignServer, 'foreignServer')
#    cmo.setInitialContextFactory(initialContextFactory)
#    cmo.setconnectionUrl(connectionURL)

#    cd('foreignServer/' + foreignServer)
#    for section in sectionList:
#      if section.startswith('CONNFACTORY'):
#        foreignConnectionFactory = config.get(section,'foreignConnectionFactory')
#        localJndiName = config.get(section,'localJndiName')
#        remoteJndiName = config.get(section,'remoteJndiName')
#        username = config.get(section,'username')
#        password = config.get(section,'password')

#        cmo = create(foreignConnectionFactory, 'foreignConnectionFactory')
#        cmo.setLocalJndiName(localJndiName)
#        cmo.setRemoteJndiName(remoteJndiName)
#        cmo.setUserName(userName)
#        cmo.setPasswordEncrypted(password)

#    foreignJMSServer = JMSForeignServerName.createForeignServer(JMSForeignServerName);
#        foreignJMSServer.setInitialContextFactory(fsInitialContextFactory)
#        foreignJMSServer.setConnectionURL(fsConnectionURL)

#    cd('/')
#    print ''
#    targetslist = re.split(',', jmsModuleTargets)
#    for target in targetslist:
#        print 'Assining JMS Module [' + JMSSystemModuleName + '] to [' + target + ']'
#        assign('JMSSystemResource', JMSSystemModuleName, 'Target', target)

###############################################################################
# Assigning Jolt Connection Pool.
###############################################################################
for section in sectionList:
  if section.startswith('JOLTCONNPOOL'):
  
    jcPoolName = config.get(section,'jcPoolName')
    jcPoolPrimaryAddresses = config.get(section,'jcPoolPrimaryAddresses')
    jcPoolFailoverAddresses = config.get(section,'jcPoolFailoverAddresses')
    jcPoolUser = config.get(section,'jcPoolUser')
    jcPoolUserPassword = config.get(section,'jcPoolUserPassword')
    jcPoolAppPassword = config.get(section,'jcPoolAppPassword')
    jcPoolTargets = config.get(section,'jcPoolTargets')
    
    cd('/')
    print ''
    print 'Creating Jolt Connection Pool  [' + jcPoolName + ']'
    cmo = create(jcPoolName, 'JoltConnectionPool')
    cd('JoltConnectionPool/' + jcPoolName)
    primaryAddresses = re.split(',', jcPoolPrimaryAddresses)
    set('PrimaryAddress', primaryAddresses)
    failoverAddresses = re.split(',', jcPoolFailoverAddresses)
    set('FailoverAddress', failoverAddresses)
    set('UserName', jcPoolUser);
    set('UserPasswordEncrypted' , jcPoolUserPassword)
    set('ApplicationPasswordEncrypted', jcPoolAppPassword)
    print 'Assining Jolt Connection Pool [' + jcPoolName + ']' 
    set('Target', jcPoolTargets)
    

###############################################################################
# Deployments.
###############################################################################

for section in sectionList:
  if section.startswith('APP'):
    appName = config.get(section,'appName')
    appModuleType = config.get(section,'appModuleType')
    appSourcePath = config.get(section,'appSourcePath')
    appTargets = config.get(section,'appTargets')

    cd('/')
    print ''
    print 'Creating Application  [' + appName + ']'
    cmo = create(appName, 'AppDeployment')
    print 'Setting  Module Type  [' + appModuleType + ']'
    cmo.setModuleType(appModuleType)
    print 'Setting  Source Path  [' + appSourcePath + ']'
    cmo.setSourcePath(appSourcePath)

    targetslist = re.split(',', appTargets)
    for appTarget in targetslist:
      print 'Targeting Application [' + appName + '] to Target [' + appTarget + ']'
      assign('AppDeployment', appName, 'Target', appTarget)

###############################################################################
# Libraries.
###############################################################################

for section in sectionList:
  if section.startswith('LIB'):
    appName = config.get(section,'appName')
    appModuleType = config.get(section,'appModuleType')
    appSourcePath = config.get(section,'appSourcePath')
    appTargets = config.get(section,'appTargets')

    cd('/')
    print ''
    print 'Creating Application  [' + appName + ']'
    cmo = create(appName, 'Library')
    print 'Setting  Module Type  [' + appModuleType + ']'
    cmo.setModuleType(appModuleType)
    print 'Setting  Source Path  [' + appSourcePath + ']'
    cmo.setSourcePath(appSourcePath)
    cmo.setSecurityDDModel('DDOnly')
	
    targetslist = re.split(',', appTargets)
    for appTarget in targetslist:
      print 'Targeting Application [' + appName + '] to Target [' + appTarget + ']'
      assign('Library', appName, 'Target', appTarget)

###############################################################################
# Write the Domain and close the Domain Template.
###############################################################################

print ''
print 'Writing Domain at [' + domainLocation + ']'

writeDomain(domainLocation)
print 'Closing Template'
closeTemplate()
print 'done'

###############################################################################
# Exit WLST.
###############################################################################

exit()
      
