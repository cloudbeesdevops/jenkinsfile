#!/bin/ksh
SWLTSCR="start_weblogic.sh"

printf "\nEntering the script; current-directory=`pwd` at `date` on `hostname` [${SWLTSCR},${LINENO}]\n\n"

if [ ! -d ${APP}/web/weblogic/config/${WLS_DOMAIN_NAME} ]
then
    printf "\nERROR `date` : Directory \"${APP}/web/weblogic/config/${WLS_DOMAIN_NAME}\" is NOT available; goodbye. [${SWLTSCR},${LINENO}]\n\n"
    exit 1
else
    printf "INFORMATION: Directory \"${APP}/web/weblogic/config/${WLS_DOMAIN_NAME}\" is available - a good thing. [${SWLTSCR},${LINENO}]\n"
fi    

function doAPMwork
{
    if [ -z ${ERA_APPD_JAVAAGENTJAR} ] ; then
        printf "WARNING -- The ERA_APPD_JAVAAGENTJAR variable is not availabe - a problem; goodbye [${SWLTSCR},${LINENO}]\n"
        exit 1
    elif [ -z ${ERA_APPD_MACHINEAGENTJAR} ] ; then
        printf "WARNING -- The ERA_APPD_MACHINEAGENTJAR variable is not availabe - a problem; goodbye [${SWLTSCR},${LINENO}]\n"
        exit 1
    elif [ -z ${ERA_APPD_MACHINEAGENTSCR} ] ; then
        printf "WARNING -- The ERA_APPD_MACHINEAGENTSCR variable is not availabe - a problem; goodbye [${SWLTSCR},${LINENO}]\n"
        exit 1
    elif [ -z ${ERA_APPD_APPAGENTSCR} ] ; then
        printf "WARNING -- The ERA_APPD_APPAGENTSCR variable is not availabe - a problem; goodbye [${SWLTSCR},${LINENO}]\n"
        exit 1
    fi

    typeset fqf=""
    for fqf in ${ERA_APPD_JAVAAGENTJAR} ${ERA_APPD_MACHINEAGENTJAR} ${ERA_APPD_MACHINEAGENTSCR} ${ERA_APPD_APPAGENTSCR}
    do
        if [ ! -f ${fqf} ] ; then
            printf "WARNING -- The file=${fqf} is NOT a file; no APM work is done. [${SWLTSCR},${LINENO}]\n"
            return 21
        elif [ ! -r ${fqf} ] ; then
            printf "WARNING -- The file=${fqf} is NOT readable; no APM work is done. [${SWLTSCR},${LINENO}]\n"
            return 22
        elif [ ! -s ${fqf} ] ; then
            printf "WARNING -- The file=${fqf} is NOT a good size; no APM work is done. [${SWLTSCR},${LINENO}]\n"
            return 23
        fi
    done

    unset CLASSPATH
    unset JAVA_OPTIONS
    printf "INFORMATION: Work for \"${ERA_APPD_APPAGENTSCR} ${EAINUMBER}\" -- start [${SWLTSCR},${LINENO}]\n"
    . ${ERA_APPD_APPAGENTSCR} ${EAINUMBER}
    printf "INFORMATION: Work for \"${ERA_APPD_APPAGENTSCR} ${EAINUMBER}\" -- stop  [${SWLTSCR},${LINENO}]\n"
    javaOptions="${javaOptions} ${JAVA_OPTIONS}"
    classpath=${classpath}:${CLASSPATH}

    unset CLASSPATH
    unset JAVA_OPTIONS
    printf "INFORMATION: Work for \"${ERA_APPD_MACHINEAGENTSCR} start\" -- start [${SWLTSCR},${LINENO}]\n"
    ${ERA_APPD_MACHINEAGENTSCR} start
    printf "INFORMATION: Work for \"${ERA_APPD_MACHINEAGENTSCR} start\" -- stop  [${SWLTSCR},${LINENO}]\n"
    javaOptions="${javaOptions} ${JAVA_OPTIONS}"
    classpath=${classpath}:${CLASSPATH}

    return 0
}

#####################################
# CLEAR COMMON ENVIRONMENT SETTING  #
#####################################

PATH=/bin:/opt/bin; export PATH
LD_LIBRARY_PATH=""; export LD_LIBRARY_PATH

umask 002

#######################
# SET WEBLOGIC SERVER #
#######################

if [ "xY" = "x${LATESTPB_MACHINE}" ] ; then
    beaHome=${RDT_WL_HOME}
else
    beaHome=/opt/weblogic/wl12.1.3.0
fi
weblogicHome=${beaHome}
wlsHome=${weblogicHome}/server
productionMode="true"
weblogicPatchHome=${beaHome}/patch_wls1030/patch_jars

####################
# SET JAVA OPTIONS #
####################

javaHome=${JAVA_HOME}
javaVM=-server

###########################################
# Setting up the project specific details #
###########################################

echo
printf "INFORMATION: Work for \"$SCRIPTS/invadj-rdt-admin/createProperties.sh $*\" -- start [${SWLTSCR},${LINENO}]\n"
. $SCRIPTS/invadj-rdt-admin/createProperties.sh $*
if [ 0 -ne $? ] ; then
    printf "ERROR `date` : We have a non-zero return value - stopping; exiting with failure. [${SWLTSCR},${LINENO}]\n"
    exit 1
fi
printf "INFORMATION: Work for \"$SCRIPTS/invadj-rdt-admin/createProperties.sh $*\" -- stop  [${SWLTSCR},${LINENO}]\n"
echo

domainRoot="${APP}/web/weblogic/config"
domainName="${WLS_DOMAIN_NAME}"
domainHome="${domainRoot}/${domainName}"

echo 'domainName = ' $domainName
echo 'domainHome = ' $domainHome
echo 'domainRoot = ' $domainRoot

###########################################
# Setting up the DB2 specific details
###########################################

DB2INSTANCE=db2inst1
export DB2INSTANCE
DB2Home=/opt/ibm/db2/V9.7/dsdriver

#############################################################################
# Set the PATH and the LD_LIBRARY_PATH
#############################################################################

arch=`uname -m`

PATH=${javaHome}/bin 
PATH=${PATH}:/bin
PATH=${PATH}:/usr/bin
PATH=${PATH}:${wlsHome}/bin
PATH=${PATH}:${wlsHome}/native/linux

LD_LIBRARY_PATH=${wlsHome}/native/linux/${arch}
LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${wlsHome}/native/linux/${arch}/oci817_8

export LD_LIBRARY_PATH
export PATH

echo "LD_LIBRARY_PATH=${LD_LIBRARY_PATH}"
echo
echo "PATH=${PATH}"

#################################
# Validating Weblogic and Java  #
#################################
if [ ! -f ${wlsHome}/lib/weblogic.jar ]
then
    printf "\nERROR `date` : File ${wlsHome}/lib/weblogic.jar is NOT available -- a problem. [${SWLTSCR},${LINENO}]\n\n"
    exit 1
else
    printf "INFORMATION: File ${wlsHome}/lib/weblogic.jar is available -- a good thing. [${SWLTSCR},${LINENO}]\n"
fi

if [ ! -d ${javaHome}/lib ] 
then
    printf "\nERROR `date` : Directory ${javaHome}/lib is NOT available -- a problem. [${SWLTSCR},${LINENO}]\n"
    exit 1
else
    printf "INFORMATION: Directory ${javaHome}/lib is available -- a good thing. [${SWLTSCR},${LINENO}]\n"
fi

#########################
# Setting the classpath #
#########################

classpath=${APPLICATION_CLASSPATH}
classpath=${classpath}:${beaHome}/modules/com.bea.core.ws.wsdl4j_1.5.1.jar
classpath=${classpath}:${wlsHome}/lib/weblogic.jar
classpath=${classpath}:${wlsHome}/lib/webservices.jar
classpath=${classpath}:${javaHome}/lib/tools.jar

############################
# SET LOGGING INFORMATION  #
############################

log4jConfigFile=""
wlsLogDirectory=${LOGS}/${domainName}/weblogic
wlsArchiveLogDirectory=${ARCHIVE_DIR_45D}/${domainName}
wlsStdoutLog=${wlsLogDirectory}/${SERVER_NAME}_console.out
wlsStderrLog=${wlsLogDirectory}/${SERVER_NAME}_console.err
wlsServerLog=${wlsLogDirectory}/${SERVER_NAME}_server.log

##################################################
# CREATE LOGGING DIRECTORY IF IT DOES NOT EXIST  #
##################################################

if [ ! -r ${wlsLogDirectory} ]; then
    mkdir -p ${wlsLogDirectory}
fi

##################################################
# TEMPORARY FIX FOR LDAP FAILURES ON STARTUP     #
##################################################
if [ "$1" == "admin" ]
then
  rm -rf ${APP}/web/weblogic/config/${WLS_DOMAIN_NAME}/servers/*/data/ldap
fi

#######################
# ACHIVE OLD LOG FILE #
#######################
DATE=`date  +"%Y%m%d.%H%M%S"`

if [ ! -d ${wlsArchiveLogDirectory} ]; then
    mkdir ${wlsArchiveLogDirectory}
fi

if [ -w ${wlsServerLog} ] ; then 
    mv ${wlsServerLog} ${wlsArchiveLogDirectory}/${SERVER_NAME}_Server.log_${DATE}
fi

if [ -w ${wlsStdoutLog} ] ; then 
    mv ${wlsStdoutLog} ${wlsArchiveLogDirectory}/${SERVER_NAME}_Console.out_${DATE}
fi

if [ -w ${wlsStderrLog} ] ; then 
    mv ${wlsStderrLog} ${wlsArchiveLogDirectory}/${SERVER_NAME}_Console.err_${DATE}
fi

########################
# Setting javaOptions #
########################

javaOptions="-da -DuniqueID=${UNIQUE_IDENTIFIER} \
              -Dplatform.home=${weblogicHome} \
              -Dweblogic.RootDirectory=${domainHome} \
              -Dweblogic.Name=${SERVER_NAME} \
              -Dweblogic.ProductionModeEnabled=${productionMode} \
              -Dwls.home=${wlsHome} \
              -Djava.security.policy=${wlsHome}/lib/weblogic.policy \
              -Dweblogic.management.discover=true \
              -Dwlw.iterativeDev=false \
              -Dweblogic.data.canTransferAnyFile=true \
              -Dwlw.testConsole=false \
              -Dweblogic.wsee.wstx.wsat.deployed=false \
              -Dwlw.logErrorsToConsole="

if [ "${wlsStdoutLog}" != "" ] ; then       
    javaOptions="${javaOptions} -Dweblogic.Stdout=${wlsStdoutLog}"
fi

if [ "${wlsStderrLog}" != "" ] ; then
    javaOptions="${javaOptions} -Dweblogic.Stderr=${wlsStderrLog}"
fi

if [ "${log4jConfigFile}" != "" ] ; then
    javaOptions="${javaOptions} -Dlog4j.configuration=file:${log4jConfigFile}"
fi

if [ "${WLS_USER}" != "" ] ; then
    javaOptions="${javaOptions} -Dweblogic.management.username=${WLS_USER}"
fi

if [ "${WLS_PWD}" != "" ] ; then
    javaOptions="${javaOptions} -Dweblogic.management.password=${WLS_PWD}"
fi

if [ "${ADMIN_URL}" != "" ] ; then
    javaOptions="${javaOptions}   -Dweblogic.management.server=${ADMIN_URL}"
fi

if [ "${APPLICATION_JAVA_OPTIONS}" != "" ] ; then
    javaOptions="${javaOptions}   ${APPLICATION_JAVA_OPTIONS}"
fi

javaOptions="${javaOptions} ${RDT_JSECPROP_DEFINE}"

echo "The JAVA TOOL OPTIONS ARE: "
echo $JAVA_TOOL_OPTIONS
      
case "${UNIQUE_IDENTIFIER}" in
    EnableWebSvc)
        JAVA_OPTIONS="${JAVA_OPTIONS} -Xmanagement:ssl=false,authenticate=false,port=57030"
    ;;
    *)
        if [ "${ADMIN_URL}" != "" ] ; then # we are on a managed (not admin) server
            printf "INFORMATION: doing APM work. [${SWLTSCR},${LINENO}]\n"
            doAPMwork
        fi
    ;;
esac

export JAVA_OPTIONS
export javaOptions

echo "classpath=$classpath"
echo " "
echo "javaOptions=$javaOptions"

##########################
# START WEBLOGIC SERVER  #
##########################

echo "${START_INFO} for ${GRS_PROJECT_NAME}"
echo " - DATE            -> ${DATE}"
echo " - Deployment Environment  -> ${DEPLOYMENT_LEVEL}"

OLD_DIR=`pwd`
cd ${wlsArchiveLogDirectory}
nohup ${javaHome}/bin/java \
    ${javaVM} ${MEM_ARGS} ${javaOptions} ${JAVA_OPTIONS} \
    -Djrockit.optfile=${SCRIPTS}/invadj-rdt-admin/donotOptimize.opt \
    -classpath ${classpath} \
    weblogic.Server > ${wlsServerLog} 2>&1 &
    
cd ${OLD_DIR}

printf "Exiting the script; current-directory=`pwd` at `date` on `hostname` [${SWLTSCR},${LINENO}]\n"

