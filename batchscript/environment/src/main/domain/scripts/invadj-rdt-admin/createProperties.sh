. $SCRIPTS/invadj-rdt-admin/wls_profile.sh $*

${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-customer-services/logger.template ${CFG}/rdt-customer-services/logger.properties
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-customer-services/config.template ${CFG}/rdt-customer-services/config.properties
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-customer-services/properties/security.properties.template ${CFG}/rdt-customer-services/security.properties
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-customer-services/properties/log4j.xml.template ${CFG}/rdt-customer-services/log4j.xml
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-customer-services/properties/fp.properties.template ${CFG}/rdt-customer-services/fp.properties
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-customer-services/properties/client.properties.template ${CFG}/rdt-customer-services/client.properties
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-customer-services/properties/operations/security.xml.template ${CFG}/rdt-customer-services/properties/operations/security.xml

case "${GRS_PROJECT_NAME}" in
    rdt-webservice)
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/description.template ${CFG}/${GRS_PROJECT_NAME}/description.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/error.template ${CFG}/${GRS_PROJECT_NAME}/error.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/logger.template ${CFG}/${GRS_PROJECT_NAME}/logger.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/mcd.template ${CFG}/${GRS_PROJECT_NAME}/mcd.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/security.template ${CFG}/${GRS_PROJECT_NAME}/security.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/sql.template ${CFG}/${GRS_PROJECT_NAME}/sql.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/config.template  ${CFG}/${GRS_PROJECT_NAME}/config.properties

        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-c3cache/rdt-c3cache-ear/config.properties.template ${CFG}/rdt-c3cache/rdt-c3cache-ear/config.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-c3cache/rdt-c3cache-ear/logger.properties.template ${CFG}/rdt-c3cache/rdt-c3cache-ear/logger.properties
        
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-1source-ear/webapp.properties.template ${CFG}/rdt-1source-ear/webapp.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-1source-ear/log4j.properties.template ${CFG}/rdt-1source-ear/log4j.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-1source-ear/custinquiry.properties.template ${CFG}/rdt-1source-ear/custinquiry.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-1source-ear/edw.properties.template ${CFG}/rdt-1source-ear/edw.properties
        
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/coherence-cache-override-config.xml.template  ${CFG}/${GRS_PROJECT_NAME}/coherence-cache-override-config.xml
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/security.template ${CFG}/${GRS_PROJECT_NAME}/security.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/log4j.xml.template ${CFG}/${GRS_PROJECT_NAME}/log4j.xml
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/fp.properties.template ${CFG}/${GRS_PROJECT_NAME}/fp.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/client.properties.template ${CFG}/${GRS_PROJECT_NAME}/client.properties		
    ;;
    invadj-webservice)
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/revenue.desktop.server/MiigConfig.xml.template ${CFG}/revenue.desktop.server/MiigConfig.xml
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/revenue.desktop.server/logger.properties.template ${CFG}/revenue.desktop.server/logger.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/revenue.desktop.server/config.properties.template ${CFG}/revenue.desktop.server/config.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/revenue.desktop.server/security.properties.template ${CFG}/revenue.desktop.server/security.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/revenue.desktop.server/log4j.xml.template ${CFG}/revenue.desktop.server/log4j.xml
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/revenue.desktop.server/fp.properties.template ${CFG}/revenue.desktop.server/fp.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/revenue.desktop.server/client.properties.template ${CFG}/revenue.desktop.server/client.properties
		
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invoice-adjustments/invoice-adjustments-webapp/config.properties.template ${CFG}/invoice-adjustments/invoice-adjustments-webapp/config.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invoice-adjustments/invoice-adjustments-webapp/logger.properties.template ${CFG}/invoice-adjustments/invoice-adjustments-webapp/logger.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invoice-adjustments/invoice-adjustments-webapp/namespace.properties.template ${CFG}/invoice-adjustments/invoice-adjustments-webapp/namespace.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invoice-adjustments/invoice-adjustments-webapp/webservice.properties.template ${CFG}/invoice-adjustments/invoice-adjustments-webapp/webservice.properties
	;;
    rdt-mdbservice)
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-mdbservice/CodeTranslationResponse.xml.template ${CFG}/rdt-mdbservice/CodeTranslationResponse.xml
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-mdbservice/config.template ${CFG}/rdt-mdbservice/config.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-mdbservice/error.template ${CFG}/rdt-mdbservice/error.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-mdbservice/fedexjms_ssl.conf.template ${CFG}/rdt-mdbservice/fedexjms_ssl.conf
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-mdbservice/logger.template ${CFG}/rdt-mdbservice/logger.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-mdbservice/mcd.template ${CFG}/rdt-mdbservice/mcd.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-mdbservice/security.template ${CFG}/rdt-mdbservice/security.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-mdbservice/sql.template ${CFG}/rdt-mdbservice/sql.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-mdbservice/supportedOpcos.xml.template ${CFG}/rdt-mdbservice/supportedOpcos.xml

        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invoice-adjustments/invoice-adjustments-ear/config.properties.template ${CFG}/invoice-adjustments/invoice-adjustments-ear/config.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invoice-adjustments/invoice-adjustments-ear/logger.properties.template ${CFG}/invoice-adjustments/invoice-adjustments-ear/logger.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invoice-adjustments/invoice-adjustments-ear/MiigConfig.xml.template ${CFG}/invoice-adjustments/invoice-adjustments-ear/MiigConfig.xml
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invoice-adjustments/invoice-adjustments-ear/namespace.properties.template ${CFG}/invoice-adjustments/invoice-adjustments-ear/namespace.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invoice-adjustments/invoice-adjustments-ear/webservice.properties.template ${CFG}/invoice-adjustments/invoice-adjustments-ear/webservice.properties
    ;;
	 rdt-dom-adjustments)
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/logger.template ${CFG}/${GRS_PROJECT_NAME}/logger.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/config.template  ${CFG}/${GRS_PROJECT_NAME}/config.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/properties/security.properties.template ${CFG}/${GRS_PROJECT_NAME}/security.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/properties/log4j.xml.template ${CFG}/${GRS_PROJECT_NAME}/log4j.xml
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/properties/fp.properties.template ${CFG}/${GRS_PROJECT_NAME}/fp.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/properties/client.properties.template ${CFG}/${GRS_PROJECT_NAME}/client.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME}/properties/operations/security.xml.template ${CFG}/${GRS_PROJECT_NAME}/properties/operations/security.xml
		# rdt-mug-websvc expansion	
		export GRS_PROJECT_NAME2=rdt-mug-websvc
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME2}/logger.template ${CFG}/${GRS_PROJECT_NAME2}/logger.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME2}/config.template  ${CFG}/${GRS_PROJECT_NAME2}/config.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME2}/properties/security.properties.template ${CFG}/${GRS_PROJECT_NAME2}/security.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME2}/properties/log4j.xml.template ${CFG}/${GRS_PROJECT_NAME2}/log4j.xml
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME2}/properties/fp.properties.template ${CFG}/${GRS_PROJECT_NAME2}/fp.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME2}/properties/client.properties.template ${CFG}/${GRS_PROJECT_NAME2}/client.properties
		${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/${GRS_PROJECT_NAME2}/properties/operations/security.xml.template ${CFG}/${GRS_PROJECT_NAME2}/properties/operations/security.xml
    ;;
    rdt-new-services)
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-edi-autoadjust/config.template ${CFG}/rdt-edi-autoadjust/config.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-edi-autoadjust/logger.template ${CFG}/rdt-edi-autoadjust/logger.properties
    ;;
    rdt-thinclient)
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-edi-autoadjust/config.template ${CFG}/rdt-edi-autoadjust/config.properties
        ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-edi-autoadjust/logger.template ${CFG}/rdt-edi-autoadjust/logger.properties
    ;;
    rdt-batchservices)
        echo > /dev/null
    ;;
    *)
        START_INFO=" [ createProperties.sh error ] Warning! You are running this script createProperties.sh in an Unauthorized Server. Please contact Technical Support."
        exit 1
    ;;
esac

