#!/bin/ksh -p
USAGE="usage: $0 <input file> <output file>"
if [[ $# -gt  2 ]]
then
        print "$USAGE"
        exit 1
elif [[ $# -eq  2 ]]
then
        InputFile=$1
        OutputFile=$2
else
        print "$USAGE"
        exit 1
fi

END_OF_FILE="!@!"
TEMP_OUTFILE=/tmp/tmenv.$$.tmp

echo "cat << $END_OF_FILE" > $TEMP_OUTFILE

cat $InputFile >> $TEMP_OUTFILE

echo  "$END_OF_FILE" >> $TEMP_OUTFILE

chmod +x $TEMP_OUTFILE
. $TEMP_OUTFILE >$OutputFile

rm $TEMP_OUTFILE

exit 0

