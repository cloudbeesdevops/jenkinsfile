#!/bin/sh

if [ $# -eq 0 ]
then
    echo ""
    echo "USAGE: redeployApplication.sh APPLICATION NAME"
    echo ""
    exit 1
fi                                                                                

applicationName=$1                    

. $SCRIPTS/invadj-rdt-admin/wls_profile.sh

export WL_HOME=${RDT_WL_HOME}

CLASSPATH=${WL_HOME}/server/lib/weblogic.jar:${CLASSPATH}
export CLASSPATH

JAVA_OPTIONS=" -Dpython.cachedir=${APP}web/weblogic/config/${DOMAIN_NAME}/wlstTemp ${JAVA_OPTIONS}"
export JAVA_OPTIONS

#PATH=$JAVA15_HOME/jre/bin:/bin:/usr/bin
PATH=$JAVA_HOME/bin:/bin:/usr/bin
export PATH

${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invadj-rdt-admin/common/boot.identity.template $CFG/invadj-rdt-admin/boot.identity
${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/invadj-rdt-admin/${GRS_PROJECT_NAME}/redeploy_${applicationName}.properties.template $CFG/invadj-rdt-admin/application.properties

#######################################################################
# WebLogic Scripting Tool creates the Domain
#######################################################################

java -classpath ${CLASSPATH} ${JAVA_OPTIONS} weblogic.WLST ${SCRIPTS}/invadj-rdt-admin/redeployApplication.py

#######################################################################

exit 0
