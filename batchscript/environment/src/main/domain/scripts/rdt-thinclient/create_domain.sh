#!/bin/ksh

if [ "x4201" = "x${EAINUMBER}" ] ; then
    echo > /dev/null # do nothing since rdt.env is already sourced in
else
    . /opt/fedex/srs/rdt/current/cfg/env/rdt.env
fi

function replace_domain_properties
{
    if [ "xts" = "x${ENV_LEVEL}" ] ; then
        typeset new_properties_df=${CFG}/invadj-rdt-admin/rdt-thinclient/domain.properties.template.${ENV_LEVEL}
        typeset old_properties_df=${CFG}/invadj-rdt-admin/rdt-thinclient/domain.properties.template
        if [ -f ${new_properties_df} ] ; then
            echo;cp -v ${new_properties_df} ${old_properties_df}
            if [ 0 -ne $? ] ; then
                printf "\n[On `hostname` at `date` WARN] The command to replace the domain properties\n[cp -v ${new_properties_df} ${old_properties_df}]\ndid NOT work; a problem but continuing.\n"
            fi
            echo
        fi
    fi

    return 0
}

printf "\nworking . . .\n\n"
cd $SCRIPTS/rdt-thinclient
localdir=$(basename `pwd`)
report_file=${LOGFILES}/${localdir}_`basename $0`_`date +"%Y%m%d%H%M%S"`.`id --user --name`.$$.output.txt

if [ "x${ERASEQEXECCALL}" = "xY" ] ; then
    sleep 5
else
    sleep 25
fi

printf "\n[INFO `date`]\ndirectory `pwd`\nhostname  `hostname`\nsubscript $SCRIPTS/invadj-rdt-admin/createDomain.sh\nreport    ${report_file}\n[`basename $0`,${LINENO}]\n\n" | tee -a ${report_file}
replace_domain_properties                                                                                                                                                           >> ${report_file} 2>&1
. $SCRIPTS/invadj-rdt-admin/createDomain.sh                                                                                                                                         >> ${report_file} 2>&1

