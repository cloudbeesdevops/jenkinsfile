
. $SCRIPTS/invadj-rdt-admin/sourceEnvVars.sh

WLS_ADMIN_SERVER_ADDRESS=$RDT_THINCLIENT_ADMIN_SERVER_ADDRESS;      export WLS_ADMIN_SERVER_ADDRESS
WLS_ADMIN_PORT=$RDT_THINCLIENT_ADMIN_PORT;                          export WLS_ADMIN_PORT
WLS_ADMIN_SSL_PORT=$RDT_THINCLIENT_ADMIN_SSL_PORT;                  export WLS_ADMIN_SSL_PORT
WLS_ADMIN_URL="http://${WLS_ADMIN_SERVER_ADDRESS}:$WLS_ADMIN_PORT"; export WLS_ADMIN_URL
WLS_ADMIN_UNIQUE_IDENTIFIER=rdtThinClientAdmin;                     export WLS_ADMIN_UNIQUE_IDENTIFIER

WLS_MANAGED_SERVERS=$RDT_THINCLIENT_MANAGED_SERVERS;                export WLS_MANAGED_SERVERS
WLS_MANAGED_UNIQUE_IDENTIFIER=rdtThinClientWebSvc;                  export WLS_MANAGED_UNIQUE_IDENTIFIER
WLS_MANAGED_PORT=$RDT_THINCLIENT_MANAGED_PORT;                      export WLS_MANAGED_PORT
WLS_MANAGED_SSL_PORT=$RDT_THINCLIENT_MANAGED_SSL_PORT;              export WLS_MANAGED_SSL_PORT

WLS_devrdtgws11_SERVER_NAME=$RDT_THINCLIENT_devrdtgws11_SERVER_NAME; export WLS_devrdtgws11_SERVER_NAME
WLS_devrdtgws12_SERVER_NAME=$RDT_THINCLIENT_devrdtgws12_SERVER_NAME; export WLS_devrdtgws12_SERVER_NAME
WLS_devrdtgws13_SERVER_NAME=$RDT_THINCLIENT_devrdtgws13_SERVER_NAME; export WLS_devrdtgws13_SERVER_NAME
WLS_devrdtgws14_SERVER_NAME=$RDT_THINCLIENT_devrdtgws14_SERVER_NAME; export WLS_devrdtgws14_SERVER_NAME
WLS_devrdtgws15_SERVER_NAME=$RDT_THINCLIENT_devrdtgws15_SERVER_NAME; export WLS_devrdtgws15_SERVER_NAME

WLS_asrdtgwsa01_SERVER_NAME=$RDT_THINCLIENT_asrdtgws01_SERVER_NAME; export WLS_asrdtgwsa01_SERVER_NAME
WLS_asrdtgwsa02_SERVER_NAME=$RDT_THINCLIENT_asrdtgws02_SERVER_NAME; export WLS_asrdtgwsa02_SERVER_NAME
WLS_asrdtgwsa03_SERVER_NAME=$RDT_THINCLIENT_asrdtgws03_SERVER_NAME; export WLS_asrdtgwsa03_SERVER_NAME
WLS_asrdtgwsa04_SERVER_NAME=$RDT_THINCLIENT_asrdtgws04_SERVER_NAME; export WLS_asrdtgwsa04_SERVER_NAME
WLS_asrdtgwsa05_SERVER_NAME=$RDT_THINCLIENT_asrdtgws05_SERVER_NAME; export WLS_asrdtgwsa05_SERVER_NAME

WLS_tsrdtgwsa01_SERVER_NAME=$RDT_THINCLIENT_tsrdtgws01_SERVER_NAME; export WLS_tsrdtgwsa01_SERVER_NAME
WLS_tsrdtgwsa02_SERVER_NAME=$RDT_THINCLIENT_tsrdtgws02_SERVER_NAME; export WLS_tsrdtgwsa02_SERVER_NAME
WLS_tsrdtgwsa03_SERVER_NAME=$RDT_THINCLIENT_tsrdtgws03_SERVER_NAME; export WLS_tsrdtgwsa03_SERVER_NAME
WLS_tsrdtgwsa04_SERVER_NAME=$RDT_THINCLIENT_tsrdtgws04_SERVER_NAME; export WLS_tsrdtgwsa04_SERVER_NAME
WLS_tsrdtgwsa05_SERVER_NAME=$RDT_THINCLIENT_tsrdtgws05_SERVER_NAME; export WLS_tsrdtgwsa05_SERVER_NAME

WLS_fcrdtgwsa01_SERVER_NAME=$RDT_THINCLIENT_fcrdtgws01_SERVER_NAME; export WLS_fcrdtgwsa01_SERVER_NAME
WLS_fcrdtgwsa02_SERVER_NAME=$RDT_THINCLIENT_fcrdtgws02_SERVER_NAME; export WLS_fcrdtgwsa02_SERVER_NAME
WLS_fcrdtgwsa03_SERVER_NAME=$RDT_THINCLIENT_fcrdtgws03_SERVER_NAME; export WLS_fcrdtgwsa03_SERVER_NAME
WLS_fcrdtgwsa04_SERVER_NAME=$RDT_THINCLIENT_fcrdtgws04_SERVER_NAME; export WLS_fcrdtgwsa04_SERVER_NAME
WLS_fcrdtgwsa05_SERVER_NAME=$RDT_THINCLIENT_fcrdtgws05_SERVER_NAME; export WLS_fcrdtgwsa05_SERVER_NAME
WLS_fcrdtgwsa06_SERVER_NAME=$RDT_THINCLIENT_fcrdtgws06_SERVER_NAME; export WLS_fcrdtgwsa06_SERVER_NAME

WLS_elrdtgwsa01_SERVER_NAME=$RDT_THINCLIENT_elrdtgws01_SERVER_NAME; export WLS_elrdtgwsa01_SERVER_NAME
WLS_elrdtgwsa02_SERVER_NAME=$RDT_THINCLIENT_elrdtgws02_SERVER_NAME; export WLS_elrdtgwsa02_SERVER_NAME
WLS_elrdtgwsa03_SERVER_NAME=$RDT_THINCLIENT_elrdtgws03_SERVER_NAME; export WLS_elrdtgwsa03_SERVER_NAME
WLS_elrdtgwsa04_SERVER_NAME=$RDT_THINCLIENT_elrdtgws04_SERVER_NAME; export WLS_elrdtgwsa04_SERVER_NAME

WLS_pfrdtgwsa01_SERVER_NAME=$RDT_THINCLIENT_pfrdtgws01_SERVER_NAME; export WLS_pfrdtgwsa01_SERVER_NAME
WLS_pfrdtgwsa02_SERVER_NAME=$RDT_THINCLIENT_pfrdtgws02_SERVER_NAME; export WLS_pfrdtgwsa02_SERVER_NAME
WLS_pfrdtgwsa03_SERVER_NAME=$RDT_THINCLIENT_pfrdtgws03_SERVER_NAME; export WLS_pfrdtgwsa03_SERVER_NAME
WLS_pfrdtgwsa04_SERVER_NAME=$RDT_THINCLIENT_pfrdtgws04_SERVER_NAME; export WLS_pfrdtgwsa04_SERVER_NAME

WLS_bmrdtgwsa01_SERVER_NAME=$RDT_THINCLIENT_bmrdtgws01_SERVER_NAME; export WLS_bmrdtgwsa01_SERVER_NAME
WLS_bmrdtgwsa02_SERVER_NAME=$RDT_THINCLIENT_bmrdtgws02_SERVER_NAME; export WLS_bmrdtgwsa02_SERVER_NAME
WLS_bmrdtgwsa03_SERVER_NAME=$RDT_THINCLIENT_bmrdtgws03_SERVER_NAME; export WLS_bmrdtgwsa03_SERVER_NAME
WLS_bmrdtgwsa04_SERVER_NAME=$RDT_THINCLIENT_bmrdtgws04_SERVER_NAME; export WLS_bmrdtgwsa04_SERVER_NAME
WLS_bmrdtgwsa05_SERVER_NAME=$RDT_THINCLIENT_bmrdtgws05_SERVER_NAME; export WLS_bmrdtgwsa05_SERVER_NAME
WLS_bmrdtgwsa06_SERVER_NAME=$RDT_THINCLIENT_bmrdtgws06_SERVER_NAME; export WLS_bmrdtgwsa06_SERVER_NAME

WLS_rdtgwsa01_SERVER_NAME=$RDT_THINCLIENT_rdtgws01_SERVER_NAME; export WLS_rdtgwsa01_SERVER_NAME
WLS_rdtgwsa02_SERVER_NAME=$RDT_THINCLIENT_rdtgws02_SERVER_NAME; export WLS_rdtgwsa02_SERVER_NAME
WLS_rdtgwsa03_SERVER_NAME=$RDT_THINCLIENT_rdtgws03_SERVER_NAME; export WLS_rdtgwsa03_SERVER_NAME
WLS_rdtgwsa04_SERVER_NAME=$RDT_THINCLIENT_rdtgws04_SERVER_NAME; export WLS_rdtgwsa04_SERVER_NAME
WLS_rdtgwsa05_SERVER_NAME=$RDT_THINCLIENT_rdtgws05_SERVER_NAME; export WLS_rdtgwsa05_SERVER_NAME
WLS_rdtgwsa06_SERVER_NAME=$RDT_THINCLIENT_rdtgws06_SERVER_NAME; export WLS_rdtgwsa06_SERVER_NAME
WLS_rdtgwsa07_SERVER_NAME=$RDT_THINCLIENT_rdtgws07_SERVER_NAME; export WLS_rdtgwsa07_SERVER_NAME
WLS_rdtgwsa08_SERVER_NAME=$RDT_THINCLIENT_rdtgws08_SERVER_NAME; export WLS_rdtgwsa08_SERVER_NAME
WLS_rdtgwsa09_SERVER_NAME=$RDT_THINCLIENT_rdtgws09_SERVER_NAME; export WLS_rdtgwsa09_SERVER_NAME
WLS_rdtgwsa10_SERVER_NAME=$RDT_THINCLIENT_rdtgws10_SERVER_NAME; export WLS_rdtgwsa10_SERVER_NAME
WLS_rdtgwsa11_SERVER_NAME=$RDT_THINCLIENT_rdtgws11_SERVER_NAME; export WLS_rdtgwsa11_SERVER_NAME
WLS_rdtgwsa12_SERVER_NAME=$RDT_THINCLIENT_rdtgws12_SERVER_NAME; export WLS_rdtgwsa12_SERVER_NAME
WLS_rdtgwsa13_SERVER_NAME=$RDT_THINCLIENT_rdtgws13_SERVER_NAME; export WLS_rdtgwsa13_SERVER_NAME
WLS_rdtgwsa14_SERVER_NAME=$RDT_THINCLIENT_rdtgws14_SERVER_NAME; export WLS_rdtgwsa14_SERVER_NAME
WLS_rdtgwsa15_SERVER_NAME=$RDT_THINCLIENT_rdtgws15_SERVER_NAME; export WLS_rdtgwsa15_SERVER_NAME
WLS_rdtgwsa16_SERVER_NAME=$RDT_THINCLIENT_rdtgws16_SERVER_NAME; export WLS_rdtgwsa16_SERVER_NAME
WLS_rdtgwsa17_SERVER_NAME=$RDT_THINCLIENT_rdtgws17_SERVER_NAME; export WLS_rdtgwsa17_SERVER_NAME
WLS_rdtgwsa18_SERVER_NAME=$RDT_THINCLIENT_rdtgws18_SERVER_NAME; export WLS_rdtgwsa18_SERVER_NAME
WLS_rdtgwsa19_SERVER_NAME=$RDT_THINCLIENT_rdtgws19_SERVER_NAME; export WLS_rdtgwsa19_SERVER_NAME
WLS_rdtgwsa20_SERVER_NAME=$RDT_THINCLIENT_rdtgws20_SERVER_NAME; export WLS_rdtgwsa20_SERVER_NAME
WLS_rdtgwsa21_SERVER_NAME=$RDT_THINCLIENT_rdtgws21_SERVER_NAME; export WLS_rdtgwsa21_SERVER_NAME
WLS_rdtgwsa22_SERVER_NAME=$RDT_THINCLIENT_rdtgws22_SERVER_NAME; export WLS_rdtgwsa22_SERVER_NAME

WLS_DOMAIN_NAME="rdt_thinclient_domain";             export WLS_DOMAIN_NAME
WLS_SERVER_NAME_PREFIX=$GRS_PROJECT_NAME;            export WLS_SERVER_NAME_PREFIX
WLS_TARGETS=$RDT_THINCLIENT_TARGETS;                 export WLS_TARGETS
WLS_MANAGED_SERVERS=$RDT_THINCLIENT_MANAGED_SERVERS; export WLS_MANAGED_SERVERS

printf "WLS_DOMAIN_NAME=${WLS_DOMAIN_NAME}\n"
printf "WLS_SERVER_NAME_PREFIX=${WLS_SERVER_NAME_PREFIX}\n"
printf "WLS_TARGETS=${WLS_TARGETS}\n"

WLS_START_INFO="Starting ${GRS_PROJECT_NAME} in ${PHASE} environment. The deployment level is set to ${DEPLOYMENT_LEVEL}"; export WLS_START_INFO

