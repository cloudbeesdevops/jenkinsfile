#!/bin/sh

if [ $# -eq 5 ]

then

    EMPLOYEE_ID=$1

    EMPLOYEE_OPCO=$2

    EMPLOYEE_ORGCODE=$3

    WS_XML_PAYLOAD=$4

    REQUEST_ID=$5

else

    echo ""

    echo "USAGE: reconcile.sh EMPLOYEE_ID EMPLOYEE_OPCO EMPLOYEE_ORGCODE WS_XML_PAYLOAD REQUEST_ID "

    echo ""

    exit 1

fi

export JAVA_HOME=/opt/java/jre1.6.0_11
export JAVA=$JAVA_HOME/bin/java

export JAVA_SECURITY=$JAVA_HOME/lib/security
RDT_CONFIG_DIR=rdt-websvc
export RDT_CONFIG_DIR
export LIB_DIR=$LIB/$RDT_CONFIG_DIR/dependencies
export LOG_DIR=$LOGS/$RDT_CONFIG_DIR

export SSLPKGS="-Djava.protocol.handler.pkgs=com.sun.net.ssl.internal.www.protocol"

export KEYSTORE="-Djavax.net.ssl.keyStore=$JAVA_HOME/lib/security/cacerts"

export KEYSTOREPASS="-Djavax.net.ssl.keyStorePassword=changeit"

export KEYSTORE="-Djavax.net.ssl.keyStore=$JAVA_HOME/lib/security/cacerts"

export TRUSTSTOREPASS="-Djavax.net.ssl.trustStorePassword=changeit"
export SSL_ARGS="$SSLPKGS $KEYSTORE $KEYSTOREPASS $TRUSTSTORE $TRUSTSTOREPASS"

APP_CLASSPATH=`echo $LIB_DIR/*.jar $LIB_DIR/*.zip | tr '= =' '=:='`

echo "CLASSPATH=$APP_CLASSPATH"

$JAVA $SSL_ARGS -DRDT_CONFIG_DIR=${CFG}/${RDT_CONFIG_DIR}/ -classpath $APP_CLASSPATH com/fedex/rdt/common/webservice/ClientInvWebSvc1 $EMPLOYEE_ID $EMPLOYEE_OPCO $EMPLOYEE_ORGCODE $WS_XML_PAYLOAD $REQUEST_ID

