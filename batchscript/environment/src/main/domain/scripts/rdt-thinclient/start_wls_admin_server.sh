#!/bin/ksh

if [ "x4201" = "x${EAINUMBER}" ] ; then
    echo > /dev/null # do nothing since rdt.env is already sourced in
else
    . /opt/fedex/srs/rdt/current/cfg/env/rdt.env
fi

cd $SCRIPTS/rdt-thinclient

. $SCRIPTS/invadj-rdt-admin/start_weblogic.sh admin

