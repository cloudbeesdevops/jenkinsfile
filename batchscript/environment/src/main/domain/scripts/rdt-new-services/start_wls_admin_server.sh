#!/bin/sh

if [ "x4201" = "x${EAINUMBER}" ] ; then
    echo > /dev/null # do nothing since rdt.env is already sourced in
else
    . /opt/fedex/srs/rdt/current/cfg/env/rdt.env
fi

###################################################################
#
# PROJECT:      RDT-REWRITE
# APPLICATION:  WEBLOGIC - START SCRIPT
#
# DATE:         16-JAN-2015
#
###################################################################

cd $SCRIPTS/rdt-new-services

. $SCRIPTS/invadj-rdt-admin/start_weblogic.sh admin
