#!/bin/ksh

if [ "x4201" = "x${EAINUMBER}" ] ; then
    echo > /dev/null # do nothing since rdt.env is already sourced in
else
    . /opt/fedex/srs/rdt/current/cfg/env/rdt.env
fi

printf "\nworking . . .\n\n"
cd $SCRIPTS/rdt-mdbservice
localdir=$(basename `pwd`)
report_file=${LOGFILES}/${localdir}_`basename $0`_`date +"%Y%m%d%H%M%S"`.`id --user --name`.$$.output.txt

if [ "x${ERASEQEXECCALL}" = "xY" ] ; then
    sleep 5
else
    sleep 15
fi

printf "\n[INFO `date`]\ndirectory `pwd`\nhostname  `hostname`\nsubscript $SCRIPTS/invadj-rdt-admin/createDomain.sh\nreport    ${report_file}\n[`basename $0`,${LINENO}]\n\n" | tee -a ${report_file}
. $SCRIPTS/invadj-rdt-admin/createDomain.sh                                                                                                                                         >> ${report_file} 2>&1

