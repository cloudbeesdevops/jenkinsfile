#!/bin/sh

if [ "x4201" = "x${EAINUMBER}" ] ; then
    echo > /dev/null # do nothing since rdt.env is already sourced in
else
    . /opt/fedex/srs/rdt/current/cfg/env/rdt.env
fi

###################################################################
#
# PROJECT:      SRS-RDT-InvAdj
# APPLICATION:  WEBLOGIC - START SCRIPT
#
# DATE:         27-JUNE-2007
#
###################################################################

cd $SCRIPTS/invadj-webservice

. $SCRIPTS/invadj-rdt-admin/start_weblogic.sh admin
