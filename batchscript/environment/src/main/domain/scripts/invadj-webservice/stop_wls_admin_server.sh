#!/bin/sh
###################################################################
#
# PROJECT:      SRS-RDT-InvAdj
# APPLICATION:  WEBLOGIC - START SCRIPT
#
# DATE:         27-JUNE-2007
#
###################################################################

cd $SCRIPTS/invadj-webservice

. $SCRIPTS/invadj-rdt-admin/stop_weblogic.sh admin
