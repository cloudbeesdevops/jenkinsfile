#######################################################################
# Template File
#######################################################################
[TEMPLATE]
templateFile=${RDT_WL_HOME}/common/templates/wls/wls.jar

#######################################################################
# Domain
#######################################################################
[DOMAIN]
domainLocation=${APP}/web/weblogic/config/rdt_thinclient_domain
overwriteDomain=true
javaHome=${RDT_JAVA_HOME}/bin
serverStartMode=prod
###############################################################################
# Admin Server
###############################################################################
[ADMIN]
adminServerListenAddres=${WLS_ADMIN_SERVER_ADDRESS}
adminPort=${WLS_ADMIN_PORT}
adminSslPort=${WLS_ADMIN_SSL_PORT}
adminServerName=AdminServer
httpTunnelling=true

#######################################################################
# Managed Servers
#######################################################################
[MSLIST-1]
managedServers=${WLS_MANAGED_SERVERS}
msiEnabled=true
managedPort=${WLS_MANAGED_PORT}
managedSSLPort=${WLS_MANAGED_SSL_PORT}
httpTunnelling=true

#######################################################################
# Data Sources
#######################################################################
# Oracle Datasource
[DATASRC-1]
dataSource=RDT_PROPS
dsType=oracle
dsTargets=$WLS_TARGETS
dsJndiName=RDT_PROPS
driverName=oracle.jdbc.OracleDriver
dsUrl=jdbc:oracle:thin:@${INVADJ_APPS_DBHOST}
dsUser=${INVADJ_APPS_DBLOG}
dsPassword=${INVADJ_APPS_DBPWD}
dsServer=${INVADJ_APPS_DBHOST}
dsPort=${INVADJ_APPS_DBPORT}
dsCapacityIncrement=1
dsConnectionCreationRetryFrequencySeconds=20
dsConnectionReserveTimeoutSeconds=-1
dsInactiveConnectionTimeoutSeconds=45
dsIntialCapacity=80
dsIgnoreInUseConnectionsEnabled=true
dsMaxCapacity=80
dsRemoveInfectedConnections=true
dsSecondsToTrustAnIdlePoolConnection=15
dsStatementTimeout=120
dsTestConnectionsOnReserve=true
dsTestFrequencySeconds=120
dsTestTableName=SQL SELECT 1 FROM DUAL

[DATASRC-2]
dataSource=edw_pool
dsType=Teradata
dsTargets=$WLS_TARGETS
dsJndiName=edw_pool
driverName=com.ncr.teradata.TeraDriver
dsUrl=jdbc:teradata://${EDW_HOST}
dsServer=${EDW_HOST}
dsPort=
dsUser=${EDW_USER}
dsPassword=${EDW_PWD}
dsTestConnectionsOnReserve=false
dsCapacityIncrement=1
dsConnectionCreationRetryFrequencySeconds=20
dsConnectionReserveTimeoutSeconds=-1
dsIntialCapacity=80
dsIgnoreInUseConnectionsEnabled=true
dsMaxCapacity=80
dsRemoveInfectedConnections=true
dsSecondsToTrustAnIdlePoolConnection=15
dsStatementTimeout=120
dsTestFrequencySeconds=120
dsTestTableName=

# Libraries
###############################################################################
[LIB-1]
appName=frameworklib#5.1.1@5.1.1
appModuleType=war
appSourcePath=${RDT_DEVFRAMEWORK_DIR}/weblogic/frameworklib.war
appTargets=${WLS_TARGETS}

###############################################################################
# Deployments
###############################################################################
[APP-1]
appName=RDTConnectorGridWeb-1
appModuleType=war
appSourcePath=$BIN/RDTConnectorGridWeb/RDTConnectorGridWeb-1.0.war
appTargets=$WLS_TARGETS

[APP-2]
appName=RDTDomAdjustments-1
appModuleType=war
appSourcePath=$BIN/RDTDomAdjustments/RDTDomAdjustments-1.0.war
appTargets=$WLS_TARGETS

[APP-3]
appName=RDTInvoiceAdjustments-1
appModuleType=war
appSourcePath=$BIN/RDTInvoiceAdjustments/RDTInvoiceAdjustments-1.0.war
appTargets=$WLS_TARGETS

[APP-4]
appName=RDTInvoiceInquiry-1
appModuleType=war
appSourcePath=$BIN/RDTInvoiceInquiry/RDTInvoiceInquiry-1.0.war
appTargets=$WLS_TARGETS

[APP-5]
appName=RDTRebillConnectorGrid
appModuleType=war
appSourcePath=$BIN/RDTRebillConnectorGrid/RDTRebillConnectorGrid.war
appTargets=$WLS_TARGETS

[APP-6]
appName=RDTRebillService
appModuleType=war
appSourcePath=$BIN/RDTRebillService/RDTRebillService.war
appTargets=$WLS_TARGETS

[APP-7]
appName=RDTRerateService-1
appModuleType=war
appSourcePath=$BIN/RDTRerateService/RDTRerateService-1.0.war
appTargets=$WLS_TARGETS

[APP-8]
appName=RDTServiceFailureConnectorGrid
appModuleType=war
appSourcePath=$BIN/RDTServiceFailureConnectorGrid/RDTServiceFailureConnectorGrid-1.0.war
appTargets=$WLS_TARGETS

[APP-9]
appName=RDTStaticDataService-1
appModuleType=war
appSourcePath=$BIN/RDTStaticDataService/RDTStaticDataService-2.0.war
appTargets=$WLS_TARGETS

[APP-10]
appName=RDTCaseService-1
appModuleType=war
appSourcePath=$BIN/RDTCaseService/RDTCaseService-1.0.war
appTargets=$WLS_TARGETS

[APP-11]
appName=RDTPromotionCodeService-1
appModuleType=war
appSourcePath=$BIN/RDTPromotionCodeService/RDTPromotionCodeService.war
appTargets=$WLS_TARGETS

[APP-12]
appName=RDTRebillStatusWeb-1
appModuleType=war
appSourcePath=$BIN/RDTRebillStatusWeb/Rdtweb.war
appTargets=$WLS_TARGETS

[APP-13]
appName=RDTRebillStatusSearch-1
appModuleType=war
appSourcePath=$BIN/RDTRebillStatusSearch/RDTRebillStatusSearch.war
appTargets=$WLS_TARGETS

[APP-14]
appName=RDTServiceFailureService-1
appModuleType=war
appSourcePath=$BIN/RDTServiceFailureService/RDTServiceFailureService-1.0.war
appTargets=$WLS_TARGETS

[APP-15]
appName=RDTAdjustmentHistory-1
appModuleType=war
appSourcePath=$BIN/RDTAdjustmentHistory/RDTAdjustmentHistory-1.0.war
appTargets=$WLS_TARGETS

[APP-16]
appName=RDTMBGInquiry-1
appModuleType=war
appSourcePath=$BIN/RDTMBGInquiry/RDTMBGInquiry-1.0.war
appTargets=$WLS_TARGETS

[APP-17]
appName=RDTPaymentInquiry-1
appModuleType=war
appSourcePath=$BIN/RDTPaymentInquiry/RDTPaymentInquiry-1.0.war
appTargets=$WLS_TARGETS

[APP-18]
appName=RDTRefundHistory-1
appModuleType=war
appSourcePath=$BIN/RDTRefundHistory/RDTRefundHistory-1.0.war
appTargets=$WLS_TARGETS

[APP-19]
appName=RDTFreightInquiry-1
appModuleType=war
appSourcePath=$BIN/RDTFreightInquiry/RDTFreightInquiry-1.0.war
appTargets=$WLS_TARGETS

[APP-20]
appName=csm
appModuleType=war
appSourcePath=${BIN}/csm/csm-12.1.2.war
appTargets=$WLS_TARGETS

[APP-21]
appName=DisputeLateFeeService-1.0
appModuleType=war
appSourcePath=${BIN}/DisputeLateFeeService/DisputeLateFeeService-1.0.war
appTargets=$WLS_TARGETS

[APP-22]
appName=RDTEDIAdjService-1.0
appModuleType=war
appSourcePath=${BIN}/RDTEDIAdjService/RDTEDIAdjService-1.0.war
appTargets=$WLS_TARGETS

[APP-23]
appName=RevenueWebAppLite-1
appModuleType=war
appSourcePath=${BIN}/RevenueWebAppLite/RevenueWebAppLite-1.0.war
appTargets=${WLS_TARGETS}

#######################################################################
# Startup Class
#######################################################################
[STARTUPCLASS-1]
startupClassName=RDTJoltStartupClass
startupClass=bea.jolt.pool.servlet.weblogic.PoolManagerStartUp
startupTargets=${WLS_TARGETS}

#######################################################################
# ShutDown Class
#######################################################################
[SHUTDOWNCLASS-1]
shutdownClassName=RDTJoltShutdownClass
shutdownClass=bea.jolt.pool.servlet.weblogic.PoolManagerShutDown
shutdownTargets=${WLS_TARGETS}

#######################################################################
# JOLT Connection Pool
#######################################################################
[JOLTCONNPOOL-1]
jcPoolName=chWlJoltConnPool
jcPoolPrimaryAddresses=//${JSLEDTHOST1}:${JSLEDTPORT1}
jcPoolFailoverAddresses=//${JSLEDTHOST2}:${JSLEDTPORT2}
jcPoolUser=ch_admin
jcPoolUserPassword=testpas1
jcPoolAppPassword=chronos
jcPoolTargets=$WLS_TARGETS

#######################################################################
# Key Store
#######################################################################
[CERT-1]
keyStore=CustomIdentityAndCustomTrust
customIdentityKeyStoreFileName=${RDT_RDT_IDENTITY_KEYSTORE}
customIdentityKeyStoreType=jks
customIdentityKeyStorePassPhrase=${SSL_CLIENT_CERT_JKS_STORE_PASS}
CustomTrustKeyStoreFileName=${RDT_CUSTOM_TRUST_FILE}
CustomTrustKeyStoreType=jks
CustomTrustKeyStorePassPhrase=${RDT_CUSTOM_TRUST_PASS}

serverPrivateKeyAlias=${RDT_RDT_IDENTITY_ALIAS}
serverPrivateKeyPassPhrase=${SSL_CLIENT_CERT_JKS_KEY_PASS}

