#!/bin/ksh

if [ "x4201" = "x${EAINUMBER}" ] ; then
    echo > /dev/null # do nothing since rdt.env is already sourced in
else
    . /opt/fedex/srs/rdt/current/cfg/env/rdt.env
fi

export ERASEQEXECCALL=Y

for dcscr in $(find  -L /opt/fedex/srs/rdt/current -name "create_domain.sh")
do
    printf "88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888\n"
    printf "[INFO `date`] About to kick off ${dcscr} . . .\n"
    sleep 3
    ${dcscr}
    printf "[INFO `date`] Done with ${dcscr}\n"
    printf "88888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888888\n\n\n\n"
done

return 0

