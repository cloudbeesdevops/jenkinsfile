#!/bin/ksh

# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504/pardaman singh
# Create Date:   2017/03/03 16:41:29
#
# Copyright (c) 2016 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`
unset CLASSPATH ; CLASSPATH=${APPLICATION_JARS} ; export CLASSPATH
if [ ! -z ${APMCLASSPATH} ] ; then
    CLASSPATH=${CLASSPATH}:${APMCLASSPATH}
fi
WIP_DIR=/var/tmp
let SLEEPSECONDS=60*60*6

. ${SCRIPTS}/eraCommon.sh

function find_a_nice_minute
{
    while [ 1 -eq 1 ]
    do
        typeset curr_m=`date "+%M"`
        case ${curr_m} in
            "00"|"15"|"30"|"45")
                return 0
                ;;
            *)
                sleep 1
                ;;
        esac
    done
    return 0
}

printf "INFO[`date`] - begin of script; hostname[`hostname`] user[`id --user --name`] PID[$$] [${TSCR},${LINENO}]\n"
find_a_nice_minute
printf "INFO[`date`] - Found a good start-time; will now iterate and to the job every ${SLEEPSECONDS} seconds [${TSCR},${LINENO}]\n"

while [ 1 -eq 1 ]
do
    if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "INFO[`date`] - stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] is present; so we obey and stop. [${TSCR},${LINENO}]\n"
        break
    fi

    unset PROC_FILE ; PROC_FILE=${WIP_DIR}/$$.${APPLICATION_CLASS}.process ; export PROC_FILE
    unset SUCC_FILE ; SUCC_FILE=${WIP_DIR}/$$.${APPLICATION_CLASS}.success ; export SUCC_FILE
    unset FAIL_FILE ; FAIL_FILE=${WIP_DIR}/$$.${APPLICATION_CLASS}.failure ; export FAIL_FILE
    touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_create via the command \"touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    else
        printf "\nINFO[`date`] - file_initialization_create via the command \"touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" was a success. [${TSCR},${LINENO}]\n"
    fi
    rm -f ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_removal via the command \"touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    unset APPLICATION_ARGS
    APPLICATION_ARGS=" -Dapplication.success.file=${SUCC_FILE} -Dapplication.failure.file=${FAIL_FILE} -Dapplication.processing.file=${PROC_FILE} ${RDT_JSECPROP_DEFINE} ${APMJAVAOPTIONS} -cp ${CLASSPATH}"
    export APPLICATION_ARGS

    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] -\nJAVA=${JAVA}\nAPPLICATION_ARGS=${APPLICATION_ARGS}\nAPPLICATION_CLASS=${APPLICATION_CLASS}\nin [${TSCR},${LINENO}]\n"
    fi

    SUBLOGFILE=${LOGS}/sublog_${APPLICATION_NAME}.`date +"%Y%m%d%H%M%S%N"`.P$$.R${RANDOM}.log.txt;sleep 3
    printf "INFO[`date`] - application_start logfile=${SUBLOGFILE} [${TSCR},${LINENO}]\n"
    ${JAVA} ${APPLICATION_ARGS} ${APPLICATION_CLASS} > ${SUBLOGFILE} 2>&1 ; typeset -i app_ret_val=$?
    printf "INFO[`date`] - application_stop  app_ret_val=[${app_ret_val}] [${TSCR},${LINENO}]\n"

    if [ -f ${SUCC_FILE} ] && [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ ! -f ${SUCC_FILE} ] && [ ! -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - neither_success_nor_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -f ${SUCC_FILE} ] && [ -f ${PROC_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_process_present_not_good ${SUCC_FILE} ${PROC_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - failure_reported_by_application ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${PROC_FILE} ] ; then
        printf "\nERROR[`date`] - process_file_present_not_good ${PROC_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${SUCC_FILE} ] ; then
        printf "INFO[`date`] - success_reported_by_application via ${SUCC_FILE} ; will go into wait mode for ${SLEEPSECONDS} seconds now. [${TSCR},${LINENO}]\n"
        ping_alive; sleep ${SLEEPSECONDS}
        printf "INFO[`date`] - back; will do the next iteration now. [${TSCR},${LINENO}]\n"
        continue
    fi

    printf "\nERROR[`date`] - something_not_right_should_not_reach_here ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
    exit 1
done

printf "INFO[`date`] - end of script; PID[$$] [${TSCR},${LINENO}]\n"

return 0

