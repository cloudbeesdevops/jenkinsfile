#!/bin/ksh

# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504/pardaman singh
# Create Date:   2016/12/28 17:30:25
#
# Copyright (c) 2016 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

                     TSCRDTTM=`date +%Y-%m-%dT%H%M%SUTC`                       # this script start date and time
                         TSCR=`basename $0`                                    # this script name
               SHORT_HOSTNAME=`hostname|cut -f1 -d'.'`                         # short hostname for this machine
                        ALIAS=`cnames|grep CNAME|head -n1|awk '{print $2}'`    # one alias name for this machine
               ALL_PARAMETERS="$*"                                             # all input parameters to this script
                        EXP_U="rdt"                                            # the expected user of this script
                     ACT_USER=$(id --user --name)                              # the actual user
                          LOG=""                                               # the logfile for the application
         typeset -i PID_COUNT=0                                                # count of the process id(s)
         typeset -i prev_hour=99;export prev_hour
                          PID=""                                               # if one process id, the number
             APPLICATION_NAME="";export APPLICATION_NAME                       # application name
             APPLICATION_JARS="";export APPLICATION_JARS                       # jar files for the application (if applicable)
            APPLICATION_CLASS="";export APPLICATION_CLASS                      # classname for the application
              APPLICATION_SCR="";export APPLICATION_SCR                        # script for the application
           APPLICATION_CONFIG="";export APPLICATION_CONFIG                     # a configuration that might be needed for some applications
                     AC_PARAM="";export AC_PARAM                               # a variable containing the application-config-parameter
  STOPFILE_FOR_BACKGROUND_JOB="";export STOPFILE_FOR_BACKGROUND_JOB            # directory+filename of a stop file
typeset -i ITERATION_INTERVAL=10;export ITERATION_INTERVAL                     # time (in seconds) between checks
                ERACOMMON_SCR=${SCRIPTS}/eraCommon.sh                          # internal commonly used method(s)

function p_banner
{
    printf "\n"
    printf "\tusage: ${TSCR} -m <mode> -a <application> [-c <configuration.file>]\n"
    printf "\t-m    : mode          -- start , stop , status\n"
    printf "\t-a    : application   -- MasReRateMUTFile2Table|MasReRatePurge|MasReRateRequestGeneration|MasReRateResponseConsumption|RDTTransfer|eraAcpInteraction|eraCleanup|RDTMASAutoAdjustments|MASAdjMUTFileProcessor|MASAdjMUTPubProcessor\n"
    printf "\t-c    : configuration -- an optional parameter, needed for some applications\n"
    printf "\n"

    return 0
}

function vldt_application
{
    if [ -z "${inp_application}" ] ; then
        printf "\nERROR[`date`] - empty application passed in as input to this script -- goodbye. [${TSCR},${LINENO}]\n"
        p_banner
        exit 1
    fi

    case "${inp_application}" in
        MasReRateMUTFile2Table)
            APPLICATION_CLASS="com.fedex.srs.rdt.massrerate.file.processor.MASRerateFileProcessor"
            ;;
        MasReRatePurge)
            APPLICATION_CLASS="com.fedex.srs.rdt.massrerate.purge.processor.MASReratePurgeProcessor"
            ;;
        MasReRateRequestGeneration)
            APPLICATION_CLASS="com.fedex.srs.rdt.massrerate.idearequest.processor.MASRerateIDEARequestProcessor"
            ;;
        MasReRateResponseConsumption)
            APPLICATION_CLASS="com.fedex.srs.rdt.massrerate.idearesponse.processor.MASRerateIDEAResponseProcessor"
            ;;
        RDTMASAutoAdjustments)
            APPLICATION_CLASS="com.fedex.srs.rdt.autoadjust.launcher.MASAutoAdjustJobLauncher"
            ;;
        RDTTransfer)
            APPLICATION_CONFIG=${inp_application_config}
            ;;
        eraAcpInteraction)
            ;;
        eraCleanup)
            APPLICATION_CONFIG=${inp_application_config}
            ;;
        MASAdjMUTFileProcessor)
            APPLICATION_CONFIG=${inp_application_config} # holds an indicator
            APPLICATION_CLASS="com.fedex.srs.rdt.massupload.file.processor.MASAdjUploadFileProcessor"
            ;;
        MASAdjMUTPubProcessor)
            APPLICATION_CONFIG=${inp_application_config} # holds an indicator
            APPLICATION_CLASS="com.fedex.srs.rdt.massupload.publish.processor.MASAdjPublishProcessor"
            ;;
        *) printf "\nERROR[`date`] - invalid-application[${inp_application}] passed in as input to this script -- goodbye. [${TSCR},${LINENO}]\n"
            p_banner
            exit 1
            ;;
    esac
    APPLICATION_NAME=${inp_application}

    if [ "x" != "x${APPLICATION_CONFIG}" ] ; then
        AC_PARAM="APPLICATION_CONFIG=${APPLICATION_CONFIG}"
    fi

    return 0
}

function vldt_mode
{
    if [ -z "${inp_mode}" ] ; then
        printf "\nERROR[`date`] - empty mode passed in as input to this script -- goodbye. [${TSCR},${LINENO}]\n"
        p_banner
        exit 1
    fi

    case "${inp_mode}" in
        START)
            ;;
        STOP)
            ;;
        STATUS)
            ;;
        *) printf "\nERROR[`date`] - invalid mode [${inp_mode}] passed in as input to this script -- goodbye. [${TSCR},${LINENO}]\n"
            p_banner
            exit 1
            ;;
    esac

    return 0
}

function do_start
{
    unset APPLICATION_LOGFILE;APPLICATION_LOGFILE=${LOGFILES}/${TSCR%.sh}.${inp_application}.`date +%Y%m%d%H%M%S%N`.nohup.log.txt ; export APPLICATION_LOGFILE

    printf "INFO[`date`] - Inside do_start() about to send to the background the script[${APPLICATION_SCR}],application-logfile-is[${APPLICATION_LOGFILE}] [${TSCR},${LINENO}]\n" >> ${LOG}

    nohup ${APPLICATION_SCR} ${AC_PARAM} > ${APPLICATION_LOGFILE} 2>&1 &
    typeset bgPID=$!
    sleep 3

    printf "INFO[`date`] - Inside do_start() script[${APPLICATION_SCR}],bgPID[${bgPID}]; job of start done successfully; stop-file-is[${STOPFILE_FOR_BACKGROUND_JOB}] if you need it. [${TSCR},${LINENO}]\n" >> ${LOG}

    return 0
}

function do_force_stop
{
    get_pid_count
    if [ 0 -eq ${PID_COUNT} ] ; then
        printf "INFO[`date`] - stopfile[${STOPFILE_FOR_BACKGROUND_JOB}] did the job of the stop; force-stop not needed. [${TSCR},${LINENO}]\n" >> ${LOG}
        return 0
    elif [ 1 -eq ${PID_COUNT} ] ; then
        kill -9 ${PID}
        if [ 0 -eq $? ] ; then
            printf "INFO[`date`] - Force stop for PID[${PID}],application[${inp_application}] with plan-b worked. [${TSCR},${LINENO}]\n" >> ${LOG}
            return 0
        else
            printf "\nERROR[`date`] - Force stop for PID[${PID}],application[${inp_application}] with plan-b also failed; *STOP IT MANUALLY NOW* -- goodbye. [${TSCR},${LINENO}]\n" | tee -a ${LOG}
            exit 1
        fi
    else
        printf "\nERROR[`date`] - Force stop for application[${inp_application}] with process_id_count[${PID_COUNT}] is NOT possible; *STOP IT MANUALLY NOW* -- goodbye. [${TSCR},${LINENO}]\n" | tee -a ${LOG}
        exit 1
    fi

    return 0
}

function do_stop
{
    printf "INFO[`date`] - Inside do_stop() for application[${inp_application}] [${TSCR},${LINENO}]\n" >> ${LOG}

    touch ${STOPFILE_FOR_BACKGROUND_JOB}
    if [ 0 -eq $? ] ; then
        printf "INFO[`date`] - stopfile[${STOPFILE_FOR_BACKGROUND_JOB}] created. [${TSCR},${LINENO}]\n" >> ${LOG}
        typeset -i SECONDS=5+ITERATION_INTERVAL
        sleep ${SECONDS}
        do_force_stop # if necessary
    else
        printf "\nERROR[`date`] - failure to create stopfile[${STOPFILE_FOR_BACKGROUND_JOB}], attempting a force stop. [${TSCR},${LINENO}]\n" >> ${LOG}
        do_force_stop
    fi

    return 0
}

function perf_initial_work
{
    if [ -z "${ENV_LEVEL}" ] ; then
        printf "\nERROR[`date`] - environment setup is NOT as expected -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ "x${ACT_USER}" != "x${EXP_U}" ] && [ "x${ENV_LEVEL}" != "xdvel" ] ; then
        printf "\nERROR[`date`] - expected-user[${EXP_U}] does not match actual-user[${ACT_USER}] -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    vldt_mode
    vldt_application

    LOG=${LOGFILES}/${TSCR%.sh}.${inp_application}.log.txt
    STOPFILE_FOR_BACKGROUND_JOB=${LOGFILES}/stopfile_for_background_job.${inp_application}.stop
    typeset wip_suffix=""
    if [ ! -z ${AC_PARAM} ] ; then
        wip_suffix="`echo ${AC_PARAM}|sed 's/\//./g'|sed 's/=/./g'`"
        STOPFILE_FOR_BACKGROUND_JOB=${LOGFILES}/stopfile_for_background_job.${inp_application}.${wip_suffix}.stop
    fi

    if [ "STATUS" = "${inp_mode}" ] ; then
        return 0
    elif [ "STOP" = "${inp_mode}" ] ; then
        return 0
    fi

    if [ ! -f ${ERACOMMON_SCR} ] || [ ! -s ${ERACOMMON_SCR} ] || [ ! -r ${ERACOMMON_SCR} ] || [ ! -x ${ERACOMMON_SCR} ] ; then
        printf "\nERROR[`date`] -- The script=${ERACOMMON_SCR} is not availabe - a problem; goodbye [${TSCR},${LINENO}]\n"
        exit 1
    fi
    . ${ERACOMMON_SCR}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] -- Sourcing in the script=${ERACOMMON_SCR} gave us a non-zero return value - a problem; goodbye [${TSCR},${LINENO}]\n"
        exit 1
    fi
    doBatchAPMwork ${APPLICATION_NAME}

    unset JAVA ; JAVA=${RDT_JAVA_HOME}/bin/java ; export JAVA
    if [ ! -f ${JAVA} ] || [ ! -s ${JAVA} ] || [ ! -r ${JAVA} ] ; then
        printf "\nERROR[`date`] - java[${JAVA}] is NOT available - what a horrid thing to happen -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ ! -z "${APPLICATION_CLASS}" ] ; then
        typeset jarfile=${BIN}/${APPLICATION_NAME}/${APPLICATION_NAME}.jar
        if [ ! -f ${jarfile} ] || [ ! -r ${jarfile} ] || [ ! -s ${jarfile} ] ; then
            printf "\nERROR[`date`] - a necessary jar-file[${jarfile}] is not available for us to start up the application -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        typeset directory=${LIB}/${APPLICATION_NAME}
        if [ ! -d ${directory} ] ; then
            printf "\nERROR[`date`] - a necessary directory[${directory}] is not available for us to start up the application -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        typeset -i count=$(ls -1 ${directory}/*.jar 2> /dev/null |wc -l)
        if [ 0 -eq ${count} ] ; then
            printf "\nERROR[`date`] - zero jar files in directory[${directory}] is a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        APPLICATION_JARS=${jarfile}
        for jarfile in `ls -1 ${directory}/*.jar`
        do
            APPLICATION_JARS=${APPLICATION_JARS}:${jarfile}
        done
    fi

    APPLICATION_SCR=${SCRIPTS}/start_${inp_application}.sh
    if [ ! -f ${APPLICATION_SCR} ] || [ ! -r ${APPLICATION_SCR} ] || [ ! -s ${APPLICATION_SCR} ] || [ ! -x ${APPLICATION_SCR} ] ; then
        printf "\nERROR[`date`] - a necessary script-file[${APPLICATION_SCR}] is not available for us to start up the application -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ "RDTTransfer" = "${inp_application}" ] || [ "eraCleanup" = "${inp_application}" ] ; then
        if [ -z "${inp_application_config}" ] ; then
            printf "\nERROR[`date`] - appliction[${inp_application}] cannot work with an empty configuration, a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        elif [ ! -f ${inp_application_config} ] || [ ! -r ${inp_application_config} ] || [ ! -s ${inp_application_config} ] ; then
            printf "\nERROR[`date`] - appliction[${inp_application}] cannot work with a bad-configuration[${inp_application_config}], a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
    fi
    if [ "MASAdjMUTFileProcessor" = "${inp_application}" ] || [ "MASAdjMUTPubProcessor" = "${inp_application}" ] ; then
        if [ -z "${inp_application_config}" ] ; then
            printf "\nERROR[`date`] - appliction[${inp_application}] cannot work with an empty configuration, a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
    fi

    touch ${STOPFILE_FOR_BACKGROUND_JOB}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - sanity-check-stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] touch failed; not a good thing -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    rm -f ${STOPFILE_FOR_BACKGROUND_JOB}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - the sanity-check-stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] removal failed; not a good thing -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    return 0
}

function get_pid_count
{
    PID="";PID_COUNT=0

    if [ -z ${AC_PARAM} ] ; then
        PID_COUNT=$(ps -eafwwww|grep "^${ACT_USER}"|grep -v " -m"|grep -v swp|grep -v tail|grep ${inp_application}|grep -i start|grep -v grep|wc -l)
        if [ 1 -eq ${PID_COUNT} ] ; then
            PID=$(  ps -eafwwww|grep "^${ACT_USER}"|grep -v " -m"|grep -v swp|grep -v tail|grep ${inp_application}|grep -i start|grep -v grep|awk '{print $2}')
        fi
    else
        PID_COUNT=$(ps -eafwwww|grep "^${ACT_USER}"|grep -v " -m"|grep -v swp|grep -v tail|grep ${inp_application}|grep -i start|grep "${AC_PARAM}"|grep -v grep|wc -l)
        if [ 1 -eq ${PID_COUNT} ] ; then
            PID=$(  ps -eafwwww|grep "^${ACT_USER}"|grep -v " -m"|grep -v swp|grep -v tail|grep ${inp_application}|grep -i start|grep "${AC_PARAM}"|grep -v grep|awk '{print $2}')
        fi
    fi

    return 0
}

function perf_work
{
    get_pid_count

    if [ "STATUS" = "${inp_mode}" ] ; then
        if [ 0 -eq ${PID_COUNT} ] ; then
            exit 1
        elif [ 1 -eq ${PID_COUNT} ] ; then
            exit 0 # do nothing; exactly one PID for our application is a good thing
        else
            printf "\nERROR[`date`] - during status; something not right; we have an unexpected count=${PID_COUNT} for user=${ACT_USER},application=${inp_application} -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
    elif [ "START" = "${inp_mode}" ] ; then
        if [ 0 -eq ${PID_COUNT} ] ; then
            do_start
        elif [ 1 -eq ${PID_COUNT} ] ; then
            printf "\nWARN[`date`] - we already have an instance started-up; can NOT start another instance. PID=${PID},user=${ACT_USER},application=${inp_application} -- goodbye. [${TSCR},${LINENO}]\n"
            exit 0
        else
            printf "\nERROR[`date`] - during start; something not right; we have an unexpected count=${PID_COUNT} for user=${ACT_USER},application=${inp_application} -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
    elif [ "STOP" = "${inp_mode}" ] ; then
        if [ 0 -eq ${PID_COUNT} ] ; then
            printf "\nWARN[`date`] - nothing to stop; can NOT stop since we have no PID up for user=${ACT_USER},application=${inp_application} -- goodbye. [${TSCR},${LINENO}]\n"
            exit 0
        elif [ 1 -eq ${PID_COUNT} ] ; then
            do_stop
        else
            printf "\nERROR[`date`] - during stop; something not right; we have an unexpected count=${PID_COUNT} for user=${ACT_USER},application=${inp_application} -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
    fi

    return 0
}

#########################
# main work starts here #
#########################

typeset -u inp_mode=""
typeset    inp_application=""
typeset    inp_application_config=""

typeset -u getoptsOption=""
while getopts a:A:c:C:m:M: getoptsOption > /dev/null 2> /dev/null
do
    case ${getoptsOption} in
        A) inp_application=${OPTARG}
           ;;
        C) inp_application_config=${OPTARG}
           ;;
        M) inp_mode=${OPTARG}
           ;;
        \?) printf "\nERROR[`date`] - invalid input and/or options [${ALL_PARAMETERS}] passed in as input to this script -- goodbye. [${TSCR},${LINENO}]\n"
            p_banner
            exit 1
            ;;
    esac
done

perf_initial_work
perf_work

return 0

