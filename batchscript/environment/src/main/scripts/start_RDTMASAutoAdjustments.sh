#!/bin/ksh
# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504/pardaman singh
# Create Date:   2017/12/06 20:19:04
#
# Copyright (c) 2017 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`
unset CLASSPATH ; CLASSPATH=${APPLICATION_JARS} ; export CLASSPATH
if [ ! -z ${APMCLASSPATH} ] ; then
    CLASSPATH=${CLASSPATH}:${APMCLASSPATH}
fi
let ITERATION_INTERVAL=60*30 # thirty minutes

#########################
# main work begins here #
#########################
printf "INFO[`date`] - begin of script; hostname[`hostname`] user[`id --user --name`] PID[$$] [${TSCR},${LINENO}]\n"

printf "\nINFO[`date`] - some variable(s), only for information;\n[CLASSPATH=${CLASSPATH}]\n[${TSCR},${LINENO}]\n\n"

. ${SCRIPTS}/eraCommon.sh

while [ 1 ]
do
    if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "INFO[`date`] - stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] is present; so we obey and stop. [${TSCR},${LINENO}]\n"
        break
    fi

    APPLICATION_ARGS="${RDT_JSECPROP_DEFINE} ${APMJAVAOPTIONS} -cp ${CLASSPATH}"
    export APPLICATION_ARGS

    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] -\nJAVA=${JAVA}\nAPPLICATION_ARGS=${APPLICATION_ARGS}\nAPPLICATION_CLASS=${APPLICATION_CLASS}\nin [${TSCR},${LINENO}]\n"
    fi

    SUBLOGFILE=${LOGS}/sublog_${APPLICATION_NAME}.`date +"%Y%m%d%H%M%S%N"`.P$$.R${RANDOM}.log.txt;sleep 3
    printf "INFO[`date`] - application_start [logfile=${SUBLOGFILE}] [${TSCR},${LINENO}]\n"
    ${JAVA} ${APPLICATION_ARGS} ${APPLICATION_CLASS} > ${SUBLOGFILE} 2>&1 ; typeset -i app_ret_val=$?
    printf "INFO[`date`] - application_stop app_ret_val[${app_ret_val}] [${TSCR},${LINENO}]\n"

    if [ 0 -ne ${app_ret_val} ] ; then
        printf "\nERROR[`date`] - non zero return value [${app_ret_val}] -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    ping_alive; sleep ${ITERATION_INTERVAL}
done

printf "INFO[`date`] - end of script; PID[$$] [${TSCR},${LINENO}]\n"

return 0

