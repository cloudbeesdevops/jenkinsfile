#!/bin/ksh
# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504 / pardaman singh
# Create Date:   2018/01/31 10:26:11
#
# Copyright (c) 2018 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`
unset CLASSPATH ; CLASSPATH=${APPLICATION_JARS} ; export CLASSPATH
if [ ! -z ${APMCLASSPATH} ] ; then
    CLASSPATH=${CLASSPATH}:${APMCLASSPATH}
fi

BATCH_SUMMARY_NBR=""
let ITERATION_INTERVAL=60

IND_SML="S"
IND_MED="M"
IND_LAR="L"

function queryDB
{
    BATCH_SUMMARY_NBR=""
    typeset sqlfileinp=/var/tmp/${TSCR%.sh}.P$$.R${RANDOM}.`date "+%Y%m%d%H%M%S"`.sql
    typeset sqlfileout=${sqlfileinp}.output.txt

    if [ "xping" = "x$1" ] ; then
        whence -q sqlplus tnsping
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date`] - one or both of sqlplus,tnsping are not available; a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        printf "\-\- DBLOG=${DBLOG},DBSVR=${DBSVR}\n"                                                                     >> ${sqlfileinp}
        printf "select sys_context('USERENV','DB_NAME') , sys_context('USERENV','DB_UNIQUE_NAME') , sysdate from dual;\n" >> ${sqlfileinp}
        printf "exit;\n"                                                                                                  >> ${sqlfileinp}

        echo ${DBPWD} | sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout} 2>&1
        grep -q "^ORA\-" ${sqlfileout}
        if [ 0 -eq $? ] ; then
            printf "\nERROR[`date`] - Our ping-sqlplus command [sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout}] did NOT work; a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        mv -v ${sqlfileinp} ${sqlfileinp}.save
        mv -v ${sqlfileout} ${sqlfileout}.save
        printf "\nINFO[`date`] - Our ping-sqlplus command DID work; [sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout}] input-and-output moved to .save for reference. [${TSCR},${LINENO}]\n"
    elif [ "xMADJ_RFPUB" = "x$1" ] ; then
        printf "set heading off;\n"                                                          >> ${sqlfileinp}
        printf "set feedback off;\n\n"                                                       >> ${sqlfileinp}
        printf "select min(batch_summary_nbr) from invadj_schema.mass_adj_summary where\n"   >> ${sqlfileinp}
        printf "nvl(rerate_status_cd,'') = 'MADJ_RFPUB' and\n"                               >> ${sqlfileinp}
        printf "nvl(batch_chrst_desc,'') like '\%BHOST=`hostname`\%' and\n"                  >> ${sqlfileinp}
        printf "nvl(batch_chrst_desc,'') like '\%BTYPE=${APPLICATION_CONFIG}\%' ;\n"         >> ${sqlfileinp}
        printf "exit;\n"                                                                     >> ${sqlfileinp}

        echo ${DBPWD} | sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout} 2>&1
        grep -q "^ORA\-" ${sqlfileout}
        if [ 0 -eq $? ] ; then
            printf "\nERROR[`date`] - Our real-sqlplus command [sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout}] did NOT work; a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi

        typeset -i line_count=$(cat ${sqlfileout}|grep -v "^$"|wc -l)
        if [ ${line_count} -gt 0 ] ; then
            BATCH_SUMMARY_NBR=$(cat ${sqlfileout}|grep -v "^$"|head -n1|awk '{print $1}')
            printf "\nINFO[`date`] - Found ready-for-publish-batch [${BATCH_SUMMARY_NBR}] in the database [${DBSVR}] [${TSCR},${LINENO}]\n"
            printf "\ncontents_of_file=${sqlfileinp}\n"
            cat -n ${sqlfileinp}
            printf "end_of_file_contents\n"
            printf "\ncontents_of_file=${sqlfileout}\n"
            cat -n ${sqlfileout}
            printf "end_of_file_contents\n\n"
        fi
        rm -f ${sqlfileinp} ${sqlfileout}
    elif [ "xMADJ_INPUB" = "x$1" ] ; then
        printf "set heading off;\n"                                                          >> ${sqlfileinp}
        printf "set feedback off;\n\n"                                                       >> ${sqlfileinp}
        printf "select min(batch_summary_nbr) from invadj_schema.mass_adj_summary where\n"   >> ${sqlfileinp}
        printf "nvl(rerate_status_cd,'') = 'MADJ_INPUB' and\n"                               >> ${sqlfileinp}
        printf "nvl(batch_chrst_desc,'') like '\%BHOST=`hostname`\%' and\n"                  >> ${sqlfileinp}
        printf "nvl(batch_chrst_desc,'') like '\%BTYPE=${APPLICATION_CONFIG}\%' and\n"       >> ${sqlfileinp}
        printf "last_update_tmstp < sysdate - (20/1440) ;\n"                                 >> ${sqlfileinp} # last_update_tmstp is over 20 minutes ago
        printf "exit;\n"                                                                     >> ${sqlfileinp}

        echo ${DBPWD} | sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout} 2>&1
        grep -q "^ORA\-" ${sqlfileout}
        if [ 0 -eq $? ] ; then
            printf "\nERROR[`date`] - Our real-sqlplus command [sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout}] did NOT work; a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi

        typeset -i line_count=$(cat ${sqlfileout}|grep -v "^$"|wc -l)
        if [ ${line_count} -gt 0 ] ; then
            BATCH_SUMMARY_NBR=$(cat ${sqlfileout}|grep -v "^$"|head -n1|awk '{print $1}')
            printf "\nINFO[`date`] - Found batch-in-publish [${BATCH_SUMMARY_NBR}] in the database [${DBSVR}] [${TSCR},${LINENO}]\n"
            printf "\ncontents_of_file=${sqlfileinp}\n"
            cat -n ${sqlfileinp}
            printf "end_of_file_contents\n"
            printf "\ncontents_of_file=${sqlfileout}\n"
            cat -n ${sqlfileout}
            printf "end_of_file_contents\n\n"
        fi
        rm -f ${sqlfileinp} ${sqlfileout}
    fi

    return 0
}

function do_initializations
{
    if [ -z "${APPLICATION_CONFIG}" ] ; then
        printf "\nERROR[`date`] - a necessary input indicator is absent [${APPLICATION_CONFIG}], a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    elif [ "${IND_SML}" = "${APPLICATION_CONFIG}" ] || [ "${IND_MED}" = "${APPLICATION_CONFIG}" ] || [ "${IND_LAR}" = "${APPLICATION_CONFIG}" ] ; then
        echo > /dev/null # do nothing
    else
        printf "\nERROR[`date`] - a necessary input indicator [${APPLICATION_CONFIG}] is invalid, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    export DBLOG=${INVADJ_APPS_DBLOG}
    export DBSVR=${BATCH_INVADJ_APPS_SERVICE}
    export DBPWD=${INVADJ_APPS_DBPWD}

    queryDB ping

    printf "\nINFO[`date`] - some variable(s), only for information;\n[CLASSPATH=${CLASSPATH}]\n[DBLOG=${DBLOG}]\n[DBSVR=${DBSVR}]\n[${TSCR},${LINENO}]\n\n"

    return 0
}


#########################
# main work begins here #
#########################
printf "INFO[`date`] - begin of script; hostname[`hostname`] user[`id --user --name`] PID[$$] [${TSCR},${LINENO}]\n"
. ${SCRIPTS}/eraCommon.sh

do_initializations

while [ 1 ]
do
    if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "INFO[`date`] - stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] is present; so we obey and stop. [${TSCR},${LINENO}]\n"
        break
    fi

    queryDB MADJ_INPUB
    if [ -z "${BATCH_SUMMARY_NBR}" ] ; then
        queryDB MADJ_RFPUB
        if [ -z "${BATCH_SUMMARY_NBR}" ] ; then
            ping_alive; sleep ${ITERATION_INTERVAL}
            continue
        fi
    fi

    OUTPUT_DIR=${DATA}/outfiles
    INPUT_DIR=${DATA}/infiles
    OUTPUT_DFNAME=${OUTPUT_DIR}/MADJ_BATCH_${BATCH_SUMMARY_NBR}.LCK
    DONE_FILE=${OUTPUT_DFNAME}.done
    PROC_FILE=${OUTPUT_DFNAME}.process
    SUCC_FILE=${OUTPUT_DFNAME}.success
    FAIL_FILE=${OUTPUT_DFNAME}.failure

    touch ${OUTPUT_DFNAME} ${DONE_FILE} ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_create via the command \"touch ${OUTPUT_DFNAME} ${DONE_FILE} ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    rm -fv ${OUTPUT_DFNAME} ${DONE_FILE} ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_removal via the command \"rm -f ${OUTPUT_DFNAME} ${DONE_FILE} ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    unset APPLICATION_ARGS
    APPLICATION_ARGS=" -Dapp.batch.id=${BATCH_SUMMARY_NBR} -Dapp.input.directory=${INPUT_DIR} -Dapp.output.directory=${OUTPUT_DIR}"
    APPLICATION_ARGS="${APPLICATION_ARGS} ${RDT_JSECPROP_DEFINE} ${APMJAVAOPTIONS} -cp ${CLASSPATH}"
    export APPLICATION_ARGS

    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] -\nJAVA=${JAVA}\nAPPLICATION_ARGS=${APPLICATION_ARGS}\nAPPLICATION_CLASS=${APPLICATION_CLASS}\nin [${TSCR},${LINENO}]\n"
    fi

    SUBLOGFILE=${LOGS}/sublog_${APPLICATION_NAME}.`date +"%Y%m%d%H%M%S%N"`.P$$.R${RANDOM}.log.txt;sleep 3
    printf "INFO[`date`] - application_start BATCH_SUMMARY_NBR[${BATCH_SUMMARY_NBR}] [logfile=${SUBLOGFILE}] [${TSCR},${LINENO}]\n"
    ${JAVA} ${APPLICATION_ARGS} ${APPLICATION_CLASS} > ${SUBLOGFILE} 2>&1 ; typeset -i app_ret_val=$?
    printf "INFO[`date`] - application_stop app_ret_val[${app_ret_val}],BATCH_SUMMARY_NBR[${BATCH_SUMMARY_NBR}] [${TSCR},${LINENO}]\n"

    printf "INFO[`date`] - list of files output by the application -- begin [${TSCR},${LINENO}]\n"
    ls -larti --full-time ${OUTPUT_DFNAME}*
    printf "INFO[`date`] - list of files output by the application -- end   [${TSCR},${LINENO}]\n"

    if [ -f ${SUCC_FILE} ] && [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ ! -f ${SUCC_FILE} ] && [ ! -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - neither_success_nor_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${SUCC_FILE} ] ; then
        printf "\nINFO[`date`] - success_reported_by_the_application ${SUCC_FILE} [${TSCR},${LINENO}]\n"
    fi

    if [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - failure_reported_by_application ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        # Log a message; but continue; no exit
    fi

    printf "INFO[`date`] - job_for_current_batch_done_going_to_look_for_more_data_in_the_table_now [${TSCR},${LINENO}]\n"

    continue
done

unset DBLOG ; unset DBSVR ; unset DBPWD
printf "INFO[`date`] - end of script; PID[$$] [${TSCR},${LINENO}]\n"

return 0

