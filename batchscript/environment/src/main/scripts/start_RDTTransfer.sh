#!/bin/ksh

# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504/pardaman singh
# Create Date:   2016/12/31 11:32:25
#
# Copyright (c) 2016 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`
printf "INFO[`date`] - begin of script; hostname[`hostname`] user[`id --user --name`] PID[$$] [${TSCR},${LINENO}]\n"
SFTPLOGFILE=/var/tmp/${TSCR%.sh}_`date +%Y%m%d%H%M%S%N`.P$$.R${RANDOM}.txt

whence -q sftp 
if [ 0 -ne $? ] ; then
    printf "\nERROR[`date`] - sftp is not found, a problem -- goodbye. [${TSCR},${LINENO}]\n"
    exit 1
else
    availablesftp=`whence sftp`
    printf "\nINFO[`date`] - the sftp [${availablesftp}] will be used later, if needed/possible. [${TSCR},${LINENO}]\n"
fi

transfer_done=N
condition_met=N
typeset -i dseconds=0
configfileinformation=`ls -l ${APPLICATION_CONFIG}`
printf "\nINFO[`date`] - configuration file [${configfileinformation}] is out input parameter. [${TSCR},${LINENO}]\n"

. ${SCRIPTS}/eraCommon.sh

function do_sftp
{
    typeset PRIVATE_KEY_IDENTITY_FILE=~/.ssh/id_${ENV_APPNAME}SXFR
    if [ ! -f ${PRIVATE_KEY_IDENTITY_FILE} ] || [ ! -s ${PRIVATE_KEY_IDENTITY_FILE} ] || [ ! -r ${PRIVATE_KEY_IDENTITY_FILE} ] ; then
        printf "\nWARN[`date`] - key-file-for-sftp=${PRIVATE_KEY_IDENTITY_FILE} is not available for us to work with, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    else
        printf "\nINFO[`date`] - key-file-for-sftp[${PRIVATE_KEY_IDENTITY_FILE}] will be used later, if needed/possible. [${TSCR},${LINENO}]\n"
    fi
    typeset OPTION_CONNECT_TIMEOUT="ConnectTimeout=10"
    typeset OPTION_IDENTITY_FILE="IdentityFile=${PRIVATE_KEY_IDENTITY_FILE}"
    typeset OPTION_STRICTHOST="StrictHostKeyChecking=no"

    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] - :${OPTION_CONNECT_TIMEOUT}:,:${OPTION_IDENTITY_FILE}:,:${FTP_UNM}:,:${REMOTE_MACHINE}:,:${LOCAL_FILE}:,:${REMOTE_FILE}: [${TSCR},${LINENO}]\n"
    fi

    sftp -o${OPTION_CONNECT_TIMEOUT} -o${OPTION_IDENTITY_FILE} -o${OPTION_STRICTHOST} ${FTP_UNM}@${REMOTE_MACHINE}<<STOPFTP > ${SFTPLOGFILE} 2>&1
pwd
!date
!hostname
lls -l ${LOCAL_FILE}
put ${LOCAL_FILE} ${REMOTE_FILE}
ls -l ${REMOTE_FILE}
!date
quit
STOPFTP
    typeset -i FTP_RET_VAL=$?

    typeset ERRSTR1="No such file or directory"
    grep -q "${ERRSTR1}" ${SFTPLOGFILE}
    if [ 0 -eq $? ] ; then
        printf "\nERROR[`date`] : FAILURE_1; message [${ERRSTR1}] is in file [${SFTPLOGFILE}] during sftp on machine `hostname` ; unable to transfer local-file[${LOCAL_FILE}] as remote-file[${REMOTE_FILE}] to machine[${REMOTE_MACHINE}] as user[${FTP_UNM}]. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    ERRSTR1="get handle: Permission denied"
    grep -q "${ERRSTR1}" ${SFTPLOGFILE}
    if [ 0 -eq $? ] ; then
        printf "\nERROR[`date`] : FAILURE_2; message [${ERRSTR1}] is in file [${SFTPLOGFILE}] during sftp on machine `hostname` ; unable to transfer local-file[${LOCAL_FILE}] as remote-file[${REMOTE_FILE}] to machine[${REMOTE_MACHINE}] as user[${FTP_UNM}]. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    ERRSTR1=" not found"
    grep -q "${ERRSTR1}" ${SFTPLOGFILE}
    if [ 0 -eq $? ] ; then
        printf "\nERROR[`date`] : FAILURE_3; message [${ERRSTR1}] is in file [${SFTPLOGFILE}] during sftp on machine `hostname` ; unable to transfer local-file[${LOCAL_FILE}] as remote-file[${REMOTE_FILE}] to machine[${REMOTE_MACHINE}] as user[${FTP_UNM}]. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    ERRSTR1=" Failure"
    grep -q "${ERRSTR1}" ${SFTPLOGFILE}
    if [ 0 -eq $? ] ; then
        printf "\nERROR[`date`] : FAILURE_4; message [${ERRSTR1}] is in file [${SFTPLOGFILE}] during sftp on machine `hostname` ; unable to transfer local-file[${LOCAL_FILE}] as remote-file[${REMOTE_FILE}] to machine[${REMOTE_MACHINE}] as user[${FTP_UNM}]. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    if [ 0 -eq ${FTP_RET_VAL} ] ; then
        transfer_done=Y
        printf "INFO[`date`] : From machine `hostname` as `id --user --name` sent local-file[${LOCAL_FILE}] as remote-file[${REMOTE_FILE}] to remote-machine[${REMOTE_MACHINE}] as remote-user[${FTP_UNM}] successfully. [${TSCR},${LINENO}]\n"
        printf "\nfile_contents_begin( `ls -li --full-time ${SFTPLOGFILE}` )\n"
        cat ${SFTPLOGFILE}
        printf "file_contents_end\n\n"
    else
        printf "\nERROR[`date`] : FAILURE [${FTP_RET_VAL}] in sftp on machine `hostname` unable to transfer local-file[${LOCAL_FILE}] as remote-file[${REMOTE_FILE}] to machine[${REMOTE_MACHINE}] as user[${FTP_UNM}]. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    return 0
}

function parse_conditions
{
    if [ -z "${s_conditions}" ] ; then
        condition_met=Y
        return 0
    fi
    condition_met=N

    typeset -i P_COUNT=1
    typeset    P_CURRP=""

    typeset    P_LINE=lines
    echo ${s_conditions}|grep -q ${P_LINE}
    if [ 0 -eq $? ] ; then
        let P_COUNT=1
        while [ 1 ]
        do
            P_CURRP=`echo ${s_conditions}|cut -f${P_COUNT} -d','`
            if [ -z "${P_CURRP}" ] ; then
                break
            fi
            echo ${P_CURRP}|grep -q ${P_LINE}
            if [ 0 -eq $? ] ; then
                break
            fi
            let P_COUNT=1+P_COUNT
        done
        if [ -z "${P_CURRP}" ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} does not look right for conditions [${s_conditions}] for [property=${P_LINE}], a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        typeset -i C_LINECOUNT=`echo ${P_CURRP}|cut -f2 -d'='`
        if [ 0 -eq ${C_LINECOUNT} ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} we have an invalid value for lines in conditions [${s_conditions}] for [property=${P_LINE}], a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        typeset -i F_LINECOUNT=`cat ${DATA_DFNAME}|wc -l`
        if [ ${F_LINECOUNT} -ge ${C_LINECOUNT} ] ; then
            condition_met=Y
        fi
    fi

    P_LINE=delay
    echo ${s_conditions}|grep -q ${P_LINE}
    if [ 0 -eq $? ] ; then
        let P_COUNT=1
        while [ 1 ]
        do
            P_CURRP=`echo ${s_conditions}|cut -f${P_COUNT} -d','`
            if [ -z "${P_CURRP}" ] ; then
                break
            fi
            echo ${P_CURRP}|grep -q ${P_LINE}
            if [ 0 -eq $? ] ; then
                break
            fi
            let P_COUNT=1+P_COUNT
        done
        if [ -z "${P_CURRP}" ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} does not look right for conditions [${s_conditions}] for [property=${P_LINE}], a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        typeset -i C_DELAY=`echo ${P_CURRP}|cut -f2 -d'='`
        if [ 0 -eq ${C_DELAY} ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} we have an invalid value for delay in conditions [${s_conditions}] for [property=${P_LINE}], a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        dseconds=${C_DELAY}
        condition_met=Y
    fi

    return 0
}

function mv_done_out_of_mix
{
    typeset donefqf=$1                                         # done fully qualified filename
    typeset datafqf=$2                                         # data fully qualified filename
    typeset ctstamp=`date +"%Y%m%d%H%M%S%N"`                   # current timestamp
    typeset doned=`dirname  ${donefqf}`                        # done directory
    typeset donef=`basename ${donefqf}`                        # done filename
    typeset np="__NOTPROCESSING__"                             # not processing constant
    typeset npdonefqf=${doned}/${np}${donef}${np}.${ctstamp}   # not processing done fully qualified filename
    mv -v ${donefqf} ${npdonefqf}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - When tryingg to remove a done file from the mix (mv ${donefqf} ${npdonefqf}) we failed; something wrong; bailing out; goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    printf "\nINFO[`date`] - mv_done_out_of_mix a success; data-file[${datafqf}] done-file[${donefqf}] moved-done[`ls -li --full-time ${npdonefqf}`] ; for information. [${TSCR},${LINENO}]\n"

    return 0
}

function process_files_if_found
{
    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] - looking for files of the pattern \"${source_dfname}\" [${TSCR},${LINENO}]\n"
    fi

    typeset -i count=$(ls -1 ${source_dfname} 2> /dev/null |wc -l)
    if [ 0 -eq ${count} ] ; then
        return 0
    fi

    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] - count[${count}] files found for the pattern \"${source_dfname}\" [${TSCR},${LINENO}]\n"
    fi

    unset DONE_DFNAME ; DONE_DFNAME=$(ls -1 ${source_dfname} |head -n1)      ; export DONE_DFNAME
    unset DATA_DFNAME ; DATA_DFNAME=$(echo ${DONE_DFNAME}|sed 's/\.done$//') ; export DATA_DFNAME
    if [ ! -f ${DATA_DFNAME} ] || [ ! -s ${DATA_DFNAME} ] || [ ! -r ${DATA_DFNAME} ] || [ ! -w ${DATA_DFNAME} ] ; then
        printf "\nWARN[`date`] - data-file[${DATA_DFNAME}] corresponding to done-file[${DONE_DFNAME}] is NOT available for us to work with; will get the done out of the mix now. [${TSCR},${LINENO}]\n"
        mv_done_out_of_mix ${DONE_DFNAME} ${DATA_DFNAME}
        return 0
    fi
    if [ ! -f ${DONE_DFNAME} ] || [ ! -r ${DONE_DFNAME} ] || [ ! -w ${DONE_DFNAME} ] ; then
        printf "\nWARN[`date`] - done-file[${DONE_DFNAME}] is not available for us to work with; will get the done out of the mix now. [${TSCR},${LINENO}]\n"
        mv_done_out_of_mix ${DONE_DFNAME} ${DATA_DFNAME}
        return 0
    fi

    parse_conditions
    if [ "xY" != "x${condition_met}" ] ; then
        return 0
    fi

    sleep ${dseconds} # (maybe) wait so as to not immediately pick up after file production; concurrency issues

    for f in ${DATA_DFNAME} ${DONE_DFNAME}
    do
        FTP_UNM=${d_user}
        REMOTE_MACHINE=${d_machine}
        LOCAL_FILE=${f}
        REMOTE_FILE=${destination_dir}"/"`basename ${f}`
        do_sftp
    done

    typeset archive_dfname=${ARCHIVE_DIR}/45D/${archive_subdir}/`basename ${DATA_DFNAME}`.`date +"%Y%m%d%H%M%S%N"`
    if [ ! -d `dirname ${archive_dfname}` ] ; then
        printf "\nERROR[`date`] - directory [`dirname ${archive_dfname}`] is not available; archive not possible, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    cp -v ${DATA_DFNAME} ${archive_dfname}
    if [ 0 -eq $? ] ; then
        printf "INFO[`date`] - archive with the command \"cp ${DATA_DFNAME} ${archive_dfname}\" was a grand success. [${TSCR},${LINENO}]\n"
    else
        printf "\nERROR[`date`] - archive with the command \"cp ${DATA_DFNAME} ${archive_dfname}\" FAILED, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    rm -fv ${DONE_DFNAME} ${DATA_DFNAME}
    if [ 0 -eq $? ] ; then
        printf "INFO[`date`] - files removal with the command \"rm -f ${DONE_DFNAME} ${DATA_DFNAME}\" was done successfully. [${TSCR},${LINENO}]\n"
    else
        printf "\nERROR[`date`] - files removal with the command \"rm -f ${DONE_DFNAME} ${DATA_DFNAME}\" FAILED. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    return 0
}

while [ 1 ]
do
    if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "INFO[`date`] - stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] is present; so we obey and stop. [${TSCR},${LINENO}]\n"
        break
    fi

    typeset -i linenumber;linenumber=0
    while read level d_user d_machine d_dir s_dir s_fop archive_subdir s_conditions
    do
        transfer_done=N
        dseconds=0
        let linenumber=1+linenumber
        if [ -z "${level}" ] ; then
            continue
        fi
        echo ${level}|grep -q "#"
        if [ 0 -eq $? ] ; then
            continue
        fi
        if [ "x${level}" != "x${ENV_LEVEL}" ] ; then
            continue
        fi
        if [ -z "${d_user}" ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} does not look right for user, a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        elif [ -z "${d_machine}" ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} does not look right for machine, a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        elif [ -z "${d_dir}" ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} does not look right for dest-dir, a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        elif [ -z "${s_dir}" ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} does not look right for src-dir, a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        elif [ -z "${s_fop}" ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} does not look right for src-file-or-pattern, a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        elif [ -z "${archive_subdir}" ] ; then
            printf "\nERROR[`date`] - configuration-file=${APPLICATION_CONFIG} line-number=${linenumber} does not look right for archive-subdir, a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi

          source_dfname=`eval echo ${s_dir}`/${s_fop}
        destination_dir=`eval echo ${d_dir}`

        process_files_if_found
        if [ "xY" = "x${transfer_done}" ] ; then
            break
        fi

    done < ${APPLICATION_CONFIG}

    ping_alive; sleep ${ITERATION_INTERVAL}
done

printf "INFO[`date`] - end of script; PID[$$] [${TSCR},${LINENO}]\n"

return 0

