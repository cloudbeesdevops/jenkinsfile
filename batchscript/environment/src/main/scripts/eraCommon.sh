#!/bin/ksh
# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504/pardaman singh
# Create Date:   2017/11/27 13:21:29
#
# Copyright (c) 2016 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

function send_my_message
{
    if [ -z ${SMM_IDS} ] ; then
        printf "\nERROR[`date` on `hostname` as `id --user --name`] - Variable SMM_IDS is empty ; a problem in send_my_message(); exiting with a non-zero status.\n\n"
        exit 1
    elif [ -z ${SMM_SUB} ] ; then
        printf "\nERROR[`date` on `hostname` as `id --user --name`] - Variable SMM_SUB is empty ; a problem in send_my_message(); exiting with a non-zero status.\n\n"
        exit 1
    elif [ -z ${SMM_MSG} ] ; then
        printf "\nERROR[`date` on `hostname` as `id --user --name`] - Variable SMM_MSG is empty ; a problem in send_my_message(); exiting with a non-zero status.\n\n"
        exit 1
    fi
    typeset id=""
    for id in ${SMM_IDS}
    do
        echo|mail -s " Environment(${ENV_PHASE}) Machine(${ERA_SALIAS}) ${SMM_SUB} --important-- " -q "${SMM_MSG}" ${id}
    done

    return 0
}

function ensure_var_fedex_space
{
    typeset -i max_we_can_have=95
    typeset -i var_fedex_space=`df -hk /var/fedex|tail -n1 |awk '{print $4}'| tr -dc [:digit:]`
    if [ ${var_fedex_space} -ge ${max_we_can_have} ] ; then
        echo;echo;date;host `hostname`;echo;df -hk /var/fedex;echo;echo
        printf "ERROR[`date` on `hostname`] - we do not have enough space to proceeed; ( ${var_fedex_space} >= ${max_we_can_have} ); details printed above; exiting with a non-zero status.\n\n"
        exit 1
    fi

    return 0
}

function ping_alive # caller beware [typeset -i prev_hour=99;export prev_hour] is expected from you at the top of the script.
{
    if [ -z ${prev_hour} ] ; then
        printf "ERROR[`date` on `hostname`] - masking a problem; call to ping_alive() has not setup [prev_hour] variable.\n\n"
        typeset -i prev_hour=99
    fi

    typeset -i curr_hour=`date +"%H"`
    if [ ${prev_hour} -ne ${curr_hour} ] ; then
        printf "[INFO on `hostname` at `date`] ping_alive_signal by PID=$$ [${TSCR},${LINENO}]\n"
        prev_hour=${curr_hour}
    fi

    return 0
}

function run_sql
{
    typeset runsqltimestamp=`date +"%Y%m%d%H%M%S%N"`
    if [ -z ${INVADJPARAM_SQL_INP} ] ; then
        printf "\nERROR[`date` on `hostname`] - Expected variable INVADJPARAM_SQL_INP to be available; it is not; a problem -- goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    elif [ -z ${INVADJPARAM_SQL_OUT} ] ; then
        printf "\nERROR[`date` on `hostname`] - Expected variable INVADJPARAM_SQL_OUT to be available; it is not; a problem -- goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    elif [ -z ${DBLOG} ] ; then
        printf "\nERROR[`date` on `hostname`] - Expected variable DBLOG to be available; it is not; a problem -- goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    if [ "xping" = "x$1" ] ; then
        >${INVADJPARAM_SQL_INP}
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date` on `hostname`] - Unable to write to ${INVADJPARAM_SQL_INP}; a problem -- goodbye. [${TSCR},${LINENO}]\n\n"
            exit 1
        fi

        whence -q sqlplus tnsping
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date` on `hostname`] - one or both of sqlplus,tnsping are not available; a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi

        printf "\nREM DBLOG=${DBLOG},DBSVR=${DBSVR} on `hostname` at `date`\n\n"                                           > ${INVADJPARAM_SQL_INP}
        printf "select sys_context('USERENV','DB_NAME') , sys_context('USERENV','DB_UNIQUE_NAME') , sysdate from dual;\n" >> ${INVADJPARAM_SQL_INP}
        printf "exit;\n"                                                                                                  >> ${INVADJPARAM_SQL_INP}
    fi

    printf ${DBPWD}|sqlplus -s ${DBLOG}@${DBSVR} @${INVADJPARAM_SQL_INP} >${INVADJPARAM_SQL_OUT} 2>&1
    grep -q "^ORA\-" ${INVADJPARAM_SQL_OUT}
    if [ 0 -eq $? ] ; then
        cp -v ${INVADJPARAM_SQL_INP} /var/tmp/saved_err_${runsqltimestamp}_`basename ${INVADJPARAM_SQL_INP}`
        cp -v ${INVADJPARAM_SQL_OUT} /var/tmp/saved_err_${runsqltimestamp}_`basename ${INVADJPARAM_SQL_OUT}`
        printf "\nERROR[`date` on `hostname`] - Our command [sqlplus -s ${DBLOG}@${DBSVR} @${INVADJPARAM_SQL_INP} >${INVADJPARAM_SQL_OUT}] did NOT work; a problem; /var/tmp has files with ${runsqltimestamp} in their name -- goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    if [ "xping" = "x$1" ] ; then
        cp -v ${INVADJPARAM_SQL_INP} /var/tmp/saved_ping_${runsqltimestamp}_`basename ${INVADJPARAM_SQL_INP}`
        cp -v ${INVADJPARAM_SQL_OUT} /var/tmp/saved_ping_${runsqltimestamp}_`basename ${INVADJPARAM_SQL_OUT}`
    fi

    return 0
}

function setup_INVADJ_db
{
    export DBLOG=INVADJ_APP
    export DBPWD=${INVADJ_APPS_DBPWD}
    if [ "xdvel" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_DEV
    elif [ "xas" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_INT
    elif [ "xts" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_SYS
    elif [ "xfc" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_PLA
    elif [ "xprod" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_PRD
    else
        printf "\nERROR[`date`] - db credentials we do not know for [${ENV_LEVEL}], a problem -- goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    export DBSVR

    run_sql ping

    return 0
}

# Notice to the consumers of this method; variables JAVA_OPTIONS and CLASSPATH are lost (please re-set them after this call)
function doBatchAPMwork
{
    typeset thismethod=doBatchAPMwork
    typeset inp_application=$1

    if [ -z ${ERA_APPD_JAVAAGENTJAR} ] ; then
        printf "\nERROR[`date`] -- The ERA_APPD_JAVAAGENTJAR variable is not availabe - a problem; goodbye [method=${thismethod},${LINENO}]\n"
        exit 1
    elif [ -z ${ERA_APPD_MACHINEAGENTJAR} ] ; then
        printf "\nERROR[`date`] -- The ERA_APPD_MACHINEAGENTJAR variable is not availabe - a problem; goodbye [method=${thismethod},${LINENO}]\n"
        exit 1
    elif [ -z ${ERA_APPD_MACHINEAGENTSCR} ] ; then
        printf "\nERROR[`date`] -- The ERA_APPD_MACHINEAGENTSCR variable is not availabe - a problem; goodbye [method=${thismethod},${LINENO}]\n"
        exit 1
    elif [ -z ${ERA_APPD_APPAGENTSCR} ] ; then
        printf "\nERROR[`date`] -- The ERA_APPD_APPAGENTSCR variable is not availabe - a problem; goodbye [method=${thismethod},${LINENO}]\n"
        exit 1
    elif [ -z ${EAINUMBER} ] ; then
        printf "\nERROR[`date`] -- The EAINUMBER variable is not availabe - a problem; goodbye [method=${thismethod},${LINENO}]\n"
        exit 1
    elif [ -z ${inp_application} ] ; then
        printf "\nERROR[`date`] -- The inp_application (parameter into ${thismethod}) is not availabe - a problem; goodbye [method=${thismethod},${LINENO}]\n"
        exit 1
    fi

    # variables available for the caller of this method.
    SERVER_NAME=""    ; export SERVER_NAME    # this exact variable used for APM configuration (standard/no-choice)
    APMCLASSPATH=""   ; export APMCLASSPATH   # APM configuration variable used later
    APMJAVAOPTIONS="" ; export APMJAVAOPTIONS # APM configuration variable used later

    typeset fqf=""
    for fqf in ${ERA_APPD_JAVAAGENTJAR} ${ERA_APPD_MACHINEAGENTJAR} ${ERA_APPD_MACHINEAGENTSCR} ${ERA_APPD_APPAGENTSCR}
    do
        if [ ! -f ${fqf} ] ; then
            printf "\nWARNING[`date`] -- The [file=${fqf}] is NOT a file; no APM work is done for [application=${inp_application}]. [method=${thismethod},${LINENO}]\n"
            return 21
        elif [ ! -r ${fqf} ] ; then
            printf "\nWARNING[`date`] -- The [file=${fqf}] is NOT readable; no APM work is done for [application=${inp_application}]. [method=${thismethod},${LINENO}]\n"
            return 22
        elif [ ! -s ${fqf} ] ; then
            printf "\nWARNING[`date`] -- The [file=${fqf}] is NOT a good size; no APM work is done for [application=${inp_application}]. [method=${thismethod},${LINENO}]\n"
            return 23
        fi
    done

    SERVER_NAME=${EAINUMBER}_${inp_application}

    unset CLASSPATH
    unset JAVA_OPTIONS
    printf "INFO [`date`] Work for \"${ERA_APPD_APPAGENTSCR} ${EAINUMBER}\" -- start [method=${thismethod},${LINENO}]\n"
    . ${ERA_APPD_APPAGENTSCR} ${EAINUMBER}
    printf "INFO [`date`] Work for \"${ERA_APPD_APPAGENTSCR} ${EAINUMBER}\" -- stop  [method=${thismethod},${LINENO}]\n"
    APMJAVAOPTIONS="${JAVA_OPTIONS}"
    if [ ! -z ${CLASSPATH} ] ; then
        APMCLASSPATH=${CLASSPATH}
    fi

    unset CLASSPATH
    unset JAVA_OPTIONS
    printf "INFO [`date`] Work for \"${ERA_APPD_MACHINEAGENTSCR} start\" -- start [method=${thismethod},${LINENO}]\n"
    ${ERA_APPD_MACHINEAGENTSCR} start
    printf "INFO [`date`] Work for \"${ERA_APPD_MACHINEAGENTSCR} start\" -- stop  [method=${thismethod},${LINENO}]\n"
    APMJAVAOPTIONS="${APMJAVAOPTIONS} ${JAVA_OPTIONS} -Dappdynamics.cron.vm=true"
    if [ ! -z ${CLASSPATH} ] ; then
        if [ ! -z ${APMCLASSPATH} ] ; then
            APMCLASSPATH=${APMCLASSPATH}:${CLASSPATH}
        else
            APMCLASSPATH=${CLASSPATH}
        fi
    fi

    printf "INFO [`date`] variable values [SERVER_NAME=${SERVER_NAME}],[APMCLASSPATH=${APMCLASSPATH}],[APMJAVAOPTIONS=${APMJAVAOPTIONS}] [method=${thismethod},${LINENO}]\n"

    return 0
}

