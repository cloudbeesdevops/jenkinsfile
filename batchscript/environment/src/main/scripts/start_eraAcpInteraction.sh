#!/bin/ksh
# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504/pardaman singh
# Create Date:   2018/09/05 12:07:59
#
# Copyright (c) 2018 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`
ID=${TSCR%.sh}.`date +"%Y%m%d%H%M%S%N"`.`id --user --name`.P$$.R${RANDOM}
log=${LOGS}/${ID}.log.txt

batches_to_return_input_sql=${LOGS}/${ID}.batches_to_return_input_sql.txt
batches_to_return=${LOGS}/${ID}.batches_to_return.txt
typeset -i batches_to_return_count=0

#APAC interaction
APACD2WATCH=${DATA}/sxfr/infiles/apac
APACF2WATCH="ADJUSTMENT_??????????????_*.csv.done"
APACARCHDIR=${ARCHIVE_DIR}/45D/apac

# Post Petetion Rebill = pprebill
PPREBILLD2WATCH=${DATA}/sxfr/infiles/rebillBankruptcy
PPREBILLF2WATCH="REBILL_*_??????????????.csv.done"
PPREBILLARCHDIR=${ARCHIVE_DIR}/45D/rebillBankruptcy

# ACP interaction
D2WATCH=${DATA}/sxfr/infiles/era_mass_adj_acp
F2WATCH="ACPREQUEST_??????????????_*.csv.done"
ACPARCHDIR=${ARCHIVE_DIR}/45D/era_mass_adj_acp

ALLINPUTPARAMS="$*"

export INVADJPARAM_SQL_INP=/var/tmp/${ID}_sqlinput.sql
export INVADJPARAM_SQL_OUT=/var/tmp/${ID}_sqlinput.sql.out

BATCH_NUMBER_IN_REQUEST_FILE=""
NEW_BATCH_NUMBER=""

FILE_TYPE=""
     FILE_TYPE_ACP="ACP"      # constant
FILE_TYPE_PPREBILL="PPREBILL" # constant
    FILE_TYPE_APAC="APAC"     # constant

FILE_ARCH_DIR=""

typeset -i tirdb=46 # technical implementation (to have some efficiency) reasonable days back

OUTPUT_ADJUSTED_SQL=${CFG}/sql/era_to_acp_rep.sql
FQAVAILABLEDONEFILE=""
FQAVAILABLEDATAFILE=""
  AVAILABLEDATAFILE=""

#########################################################################################################################################
# The timestamp on all four files (request-data,request-done,response-data,response-done) is the same.
# The ID        on all four files (request-data,request-done,response-data,response-done) is the same.
#
# The ACP ID is up to six digits; full-filename is unique; always (same file never arrives a second time); all this is the agreement.
# ACPREQUEST_YYYYMMDDHHMMSS_<ID>.csv
# ACPREQUEST_YYYYMMDDHHMMSS_<ID>.csv.done
# ACPREQUEST_YYYYMMDDHHMMSS_<ID>.response.csv
# ACPREQUEST_YYYYMMDDHHMMSS_<ID>.response.csv.done
#
# The Rebill ID is all digits; full-filename is unique; always (same file never arrives a second time); all this is the agreement.
# REBILL_<ID>_YYYYMMDDHHMMSS.csv
# REBILL_<ID>_YYYYMMDDHHMMSS.csv.done
# REBILL_<ID>_YYYYMMDDHHMMSS.response.csv
# REBILL_<ID>_YYYYMMDDHHMMSS.response.csv.done
#########################################################################################################################################

STATUS_RECEI="0001000" # eRA has received the file from client (staging before MUT done)
STATUS_INMUT="0002000" # Batch in mass-adjustments-MUT
STATUS_RFEXT="0004000" # Batch mass-adjustment-in-MUT is complete; we should be able to now extract results for response to ACP
STATUS_RETUR="0005000" # eRA has returned the file to ACP

typeset -i SLSECONDS=4
typeset -i REP_ITR_COUNT=0

function get_batches_to_return
{
    printf "\nREM line_number=${LINENO} machine=`hostname` timestamp=`date` run_id=${ID}\n\n"             > ${INVADJPARAM_SQL_INP}
    printf "set feedback off\nset heading off\nset pagesize 0\n\n"                                       >> ${INVADJPARAM_SQL_INP}
    printf "SELECT BATCH_SUMMARY_NBR || ' ' || INBOUND_NM\n"                                             >> ${INVADJPARAM_SQL_INP}
    printf "FROM INVADJ_SCHEMA.MASS_ADJ_ACP_SUMMARY\n"                                                   >> ${INVADJPARAM_SQL_INP}
    printf "WHERE TRUNC(INBOUND_TMSTP) >= (SYSDATE - ${tirdb})\n"                                        >> ${INVADJPARAM_SQL_INP}
    printf "AND OUTBOUND_NM            IS NULL\n"                                                        >> ${INVADJPARAM_SQL_INP}
    printf "AND OUTBOUND_TMSTP         IS NULL\n"                                                        >> ${INVADJPARAM_SQL_INP}
    printf "AND BATCH_SUMMARY_NBR      IN\n"                                                             >> ${INVADJPARAM_SQL_INP}
    printf "  (SELECT DISTINCT BATCH_SUMMARY_NBR\n"                                                      >> ${INVADJPARAM_SQL_INP}
    printf "  FROM INVADJ_SCHEMA.MASS_ADJ_DETAIL\n"                                                      >> ${INVADJPARAM_SQL_INP}
    printf "  WHERE TRUNC(CREATE_TMSTP) >= (SYSDATE - ${tirdb})\n"                                       >> ${INVADJPARAM_SQL_INP}
    printf "  AND BATCH_SUMMARY_NBR NOT            IN\n"                                                 >> ${INVADJPARAM_SQL_INP}
    printf "    (SELECT DISTINCT BATCH_SUMMARY_NBR\n"                                                    >> ${INVADJPARAM_SQL_INP}
    printf "    FROM INVADJ_SCHEMA.MASS_ADJ_DETAIL\n"                                                    >> ${INVADJPARAM_SQL_INP}
    printf "    WHERE NVL(RECORD_STATUS_DESC,' ') NOT IN ('SUCCESS','FAILED','ERROR','DUPERROR')\n"      >> ${INVADJPARAM_SQL_INP}
    printf "    )\n"                                                                                     >> ${INVADJPARAM_SQL_INP}
    printf "  )\n"                                                                                       >> ${INVADJPARAM_SQL_INP}
    printf "ORDER BY 1 ASC;\nexit;\n\n"                                                                  >> ${INVADJPARAM_SQL_INP}
    cp ${INVADJPARAM_SQL_INP} ${batches_to_return_input_sql}

    run_sql
    cp ${INVADJPARAM_SQL_OUT} ${batches_to_return}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date` on `hostname`] - The command (cp ${INVADJPARAM_SQL_OUT} ${batches_to_return}) failed}; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    batches_to_return_count=`cat ${batches_to_return}|grep -v "^$"|wc -l`

    return 0
}

function setup_for_file_type ### *** required input ***
{
    typeset filename2inspect=${1}
    if [ -z ${filename2inspect} ] ; then
        printf "\nERROR[`date` on `hostname`] - empty input function parameter 1 (filename to inspect) - a problem; goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    FILE_TYPE=""
    FILE_ARCH_DIR=/empty/no/such/dir
    BATCH_NUMBER_IN_REQUEST_FILE=""

    typeset -i  acprequest=1
    typeset -i  rebrequest=1
    typeset -i apacrequest=1

    printf ${filename2inspect} |grep -q ACPREQUEST_ ;  acprequest=$?
    printf ${filename2inspect} |grep -q REBILL_     ;  rebrequest=$?
    printf ${filename2inspect} |grep -q ADJUSTMENT_ ; apacrequest=$?
    if [ 0 -eq ${acprequest} ] ; then
        FILE_TYPE=${FILE_TYPE_ACP}
        FILE_ARCH_DIR=${ACPARCHDIR}
        BATCH_NUMBER_IN_REQUEST_FILE=`printf ${filename2inspect}|cut -f1 -d'.'|cut -f3 -d'_'`
    elif [ 0 -eq ${rebrequest} ] ; then
        FILE_TYPE=${FILE_TYPE_PPREBILL}
        FILE_ARCH_DIR=${PPREBILLARCHDIR}
        BATCH_NUMBER_IN_REQUEST_FILE=`printf ${filename2inspect}|cut -f1 -d'.'|cut -f2 -d'_'`
    elif [ 0 -eq ${apacrequest} ] ; then
        FILE_TYPE=${FILE_TYPE_APAC}
        FILE_ARCH_DIR=${APACARCHDIR}
        BATCH_NUMBER_IN_REQUEST_FILE=`printf ${filename2inspect}|cut -f1 -d'.'|cut -f3 -d'_'`
    else
        printf "\nERROR[`date` on `hostname`] - unknown filename to inspect; a problem; goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    return 0
}

function do_report_work
{
    # after this many iterations, do report; (SLSECONDS*DOREPORTAFTERCOUNT) i.e. 4*450=1800seconds = 30minutes
    typeset -i DOREPORTAFTERCOUNT=450
    let REP_ITR_COUNT=1+REP_ITR_COUNT
    if [ ${REP_ITR_COUNT} -le ${DOREPORTAFTERCOUNT} ] ; then
        return 0
    fi

    let REP_ITR_COUNT=0
    ensure_var_fedex_space

    get_batches_to_return
    if [ 0 -eq ${batches_to_return_count} ] ; then
        return 0
    fi

    printf "\nbegin_contents (filename=${batches_to_return}) [${TSCR},${LINENO}]\n"
    cat ${batches_to_return}
    printf "end_contents\n\n"

    typeset report_mut_batch=""                        # eRA Mass Adjustments Batch number(MASS_ADJ_ACP_SUMMARY.BATCH_SUMMARY_NBR)
                                                       # ACP-request-file      ACPREQUEST_20181010020202_7555.csv (in MASS_ADJ_ACP_SUMMARY.INBOUND_NM) ties to a MASS_ADJ_SUMMARY.BATCH_SUMMARY_NBR  (matching MASS_ADJ_ACP_SUMMARY.BATCH_SUMMARY_NBR)
                                                       # Post Petition        REBILL_7555_20181010020202.csv      (in MASS_ADJ_ACP_SUMMARY.INBOUND_NM) ties to a MASS_ADJ_SUMMARY.BATCH_SUMMARY_NBR  (matching MASS_ADJ_ACP_SUMMARY.BATCH_SUMMARY_NBR)
                                                       # APAC                  ADJUSTMENT_20181010020202_7555.csv (in MASS_ADJ_ACP_SUMMARY.INBOUND_NM) ties to a MASS_ADJ_SUMMARY.BATCH_SUMMARY_NBR  (matching MASS_ADJ_ACP_SUMMARY.BATCH_SUMMARY_NBR)
    typeset report_request_filename=""                 # ACPREQUEST_20181010020202_7555.csv               _or_ REBILL_7555_20181010020202.csv           _or_ ADJUSTMENT_20181010020202_7555.csv
    typeset report_respons_filename=""                 # ACPREQUEST_20181010020202_7555.response.csv      _or_ REBILL_7555_20181010020202.response.csv  _or_ ADJUSTMENT_20181010020202_7555.response.csv
    typeset report_respons_fqf=""                      # /var/fedex/srs/rdt/data/sxfr/outfiles/ACPREQUEST_20181010020202_7555.response.csv      (or the REBILL filename)
    typeset report_respons_done_fqf=""                 # /var/fedex/srs/rdt/data/sxfr/outfiles/ACPREQUEST_20181010020202_7555.response.csv.done (or the REBILL filename)

    while read report_mut_batch report_request_filename
    do
        if [ -z ${report_mut_batch} ] || [ -z ${report_request_filename} ] ; then
            continue
        fi

        setup_for_file_type ${report_request_filename}

        typeset fname=`printf ${report_request_filename}|cut -f1 -d'.'`
        report_respons_filename=${fname}.response.csv
        report_respons_fqf=${DATA}/sxfr/outfiles/${report_respons_filename}
        report_respons_done_fqf=${report_respons_fqf}.done

        typeset ITERATION_TSTAMP=`date +"%Y%m%d%H%M%S%N"`
        printf "\nINFO [`date` on `hostname`] - ITERATION_TSTAMP(${ITERATION_TSTAMP}) BATCH_SUMMARY_NBR(${report_mut_batch}) INBOUND_NM(${report_request_filename}) report_production_begin_of_work. [${TSCR},${LINENO}]\n"

        sed -e "s/@@BATCH_SUMMARY_NBR@@/${report_mut_batch}/" ${OUTPUT_ADJUSTED_SQL} >${INVADJPARAM_SQL_INP}
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date` on `hostname`] - Using ${OUTPUT_ADJUSTED_SQL} tried to create ${INVADJPARAM_SQL_INP} for batch=${report_mut_batch} but it did not work; a problem. [${TSCR},${LINENO}]\n\n"
            exit 1
        fi
        run_sql
        cp -v ${INVADJPARAM_SQL_INP} ${FILE_ARCH_DIR}/inpsql_reportforbatch_${report_mut_batch}_${ITERATION_TSTAMP}
        cp -v ${INVADJPARAM_SQL_OUT} ${FILE_ARCH_DIR}/outsql_reportforbatch_${report_mut_batch}_${ITERATION_TSTAMP}

        cat ${INVADJPARAM_SQL_OUT} |grep -v "^$" >${report_respons_fqf}
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date` on `hostname`] - Data from ${INVADJPARAM_SQL_OUT} could NOT be written to ${report_respons_fqf} ; a problem. [${TSCR},${LINENO}]\n\n"
            exit 1
        fi
        typeset -i outboundlinecount=`cat ${report_respons_fqf}|wc -l`

        printf "\nREM line_number=${LINENO} machine=`hostname` timestamp=`date` run_id=${ID}\n\n"   > ${INVADJPARAM_SQL_INP}
        printf "UPDATE INVADJ_SCHEMA.MASS_ADJ_ACP_SUMMARY\n"                                       >> ${INVADJPARAM_SQL_INP}
        printf "SET OUTBOUND_NM       = '${report_respons_filename}' ,\n"                          >> ${INVADJPARAM_SQL_INP}
        printf "  OUTBOUND_TMSTP      = CURRENT_TIMESTAMP\n"                                       >> ${INVADJPARAM_SQL_INP}
        printf "WHERE OUTBOUND_NM    IS NULL\n"                                                    >> ${INVADJPARAM_SQL_INP}
        printf "AND OUTBOUND_TMSTP   IS NULL\n"                                                    >> ${INVADJPARAM_SQL_INP}
        printf "AND BATCH_SUMMARY_NBR = ${report_mut_batch}\n"                                     >> ${INVADJPARAM_SQL_INP}
        printf "AND INBOUND_NM        = '${report_request_filename}';\n"                           >> ${INVADJPARAM_SQL_INP}
        printf "COMMIT;\nexit;\n"                                                                  >> ${INVADJPARAM_SQL_INP}
        run_sql
        cp -v ${INVADJPARAM_SQL_INP} ${FILE_ARCH_DIR}/inpsql_updateoutboundforbatch_${report_mut_batch}_${ITERATION_TSTAMP}
        cp -v ${INVADJPARAM_SQL_OUT} ${FILE_ARCH_DIR}/outsql_updateoutboundforbatch_${report_mut_batch}_${ITERATION_TSTAMP}

        touch ${report_respons_done_fqf}
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date` on `hostname`] - Creation of ${report_respons_done_fqf} failed; a problem. [${TSCR},${LINENO}]\n\n"
            exit 1
        fi
        printf "INFO [`date` on `hostname`] -\nITERATION_TSTAMP(${ITERATION_TSTAMP})\nBATCH_SUMMARY_NBR(${report_mut_batch})\nINBOUND_NM(${report_request_filename})\nOUTBOUND_NM(${report_respons_filename})\nOUTBOUNDLINECOUNT(${outboundlinecount})\nreport_production_end_of_work. [${TSCR},${LINENO}]\n\n"
        sleep ${SLSECONDS}
    done < ${batches_to_return}

    return 0
}

function do_init
{
    ensure_var_fedex_space

    typeset manyspaces="                                                                              "
    printf "${ID}\n${manyspaces}\n${manyspaces}\n${manyspaces}\n" >${INVADJPARAM_SQL_INP}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date` on `hostname`] - Unable to write to ${INVADJPARAM_SQL_INP}; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    if [ ! -f ${OUTPUT_ADJUSTED_SQL} ] || [ ! -r ${OUTPUT_ADJUSTED_SQL} ] ; then
        printf "\nERROR[`date` on `hostname`] - SQL file for query output (${OUTPUT_ADJUSTED_SQL}) is not available; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    printf "${ID}\n${manyspaces}\n${manyspaces}\n${manyspaces}\n" >${INVADJPARAM_SQL_OUT}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date` on `hostname`] - Unable to write to ${INVADJPARAM_SQL_OUT} ; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    if [ -z ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "\nERROR[`date` on `hostname`] - empty stop file variable; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    elif [ ! -d ${D2WATCH} ] ; then
        printf "\nERROR[`date` on `hostname`] - directory (D2WATCH=${D2WATCH}) is NOT available; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    elif [ ! -d `dirname ${log}` ] ; then
        printf "\nERROR[`date` on `hostname`] - directoryforlog (`dirname ${log}`) is NOT available; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    setup_INVADJ_db

    return 0
}

function check_if_file_already_processed # ***required input parameter***
{
    typeset inbound_nm=${1}
    if [ -z ${inbound_nm} ] ; then
        printf "\nERROR[`date` on `hostname`] - empty input parameter to method; cannot work; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    typeset already_processed="__ALREADY_PROCESSED__"

    printf "\nREM Construction of the SQL being done on `hostname` at `date`\n\n"  > ${INVADJPARAM_SQL_INP}
    printf "set heading off;\nset feedback off;\n"                                >> ${INVADJPARAM_SQL_INP}
    printf "SELECT '${already_processed}',BATCH_SUMMARY_NBR,INBOUND_NM,INBOUND_TMSTP,OUTBOUND_NM,OUTBOUND_TMSTP,BATCH_STATUS_DESC,LAST_UPDATE_TMSTP from INVADJ_SCHEMA.MASS_ADJ_ACP_SUMMARY where INBOUND_NM = '${inbound_nm}';\nexit;\n" >> ${INVADJPARAM_SQL_INP}
    run_sql

    grep -q "${already_processed}" ${INVADJPARAM_SQL_OUT}
    if [ 0 -eq $? ] ; then
        printf "\nINFO [`date` on `hostname`] - file_in_question_is_already_processed_here_is_what_is_in_the_db\n"

        printf "\nbegin_of_sql_input\n"
        cat ${INVADJPARAM_SQL_INP}
        printf "end_of_sql_input\n\n"

        printf "\nbegin_of_sql_output\n"
        cat ${INVADJPARAM_SQL_OUT}
        printf "begin_of_sql_output\n\n"

        return 1 # The file we were to process; is already processed int the past. Caller can decide what to do with the input/output SQL files if they need logs; return non-zero
    fi

    return 0
}

function validate_file_pair
{
    if [ -z ${FQAVAILABLEDONEFILE} ] ; then
        printf "\nERROR[`date` on `hostname`] - Variable FQAVAILABLEDONEFILE is empty; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    elif [ ! -f ${FQAVAILABLEDONEFILE} ] || [ ! -r ${FQAVAILABLEDONEFILE} ] ; then
        printf "\nERROR[`date` on `hostname`] - done_file ${FQAVAILABLEDONEFILE} is NOT available; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    FQAVAILABLEDATAFILE=`print ${FQAVAILABLEDONEFILE}|sed 's/\.done$//'`
    if [ -z ${FQAVAILABLEDATAFILE} ] ; then
        printf "\nERROR[`date` on `hostname`] - Variable FQAVAILABLEDATAFILE (derived from (${FQAVAILABLEDONEFILE})) is empty; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    # do we have a good file-pair (done and data); if not; we move the done as not to process
    typeset FQNOTTOPROCESSDONEFILE=""
    if [ ! -f ${FQAVAILABLEDATAFILE} ] || [ ! -r ${FQAVAILABLEDATAFILE} ] || [ ! -s ${FQAVAILABLEDATAFILE} ] ; then
        FQNOTTOPROCESSDONEFILE=${FQAVAILABLEDONEFILE}_nogooddatafile_MOVED_NOT_PROCESSING.`date +"%Y%m%d%H%M%S%N"`
    else
        check_if_file_already_processed `basename ${FQAVAILABLEDATAFILE}`
        if [ 0 -ne $? ] ; then
            FQNOTTOPROCESSDONEFILE=${FQAVAILABLEDONEFILE}_processedinpast_MOVED_NOT_PROCESSING.`date +"%Y%m%d%H%M%S%N"`
        fi
    fi

    if [ ! -z "${FQNOTTOPROCESSDONEFILE}" ] ; then
        echo;ls -li --full-time ${FQAVAILABLEDONEFILE}
        mv -v ${FQAVAILABLEDONEFILE} ${FQNOTTOPROCESSDONEFILE}
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date` on `hostname`] - The command (mv -v ${FQAVAILABLEDONEFILE} ${FQNOTTOPROCESSDONEFILE}) failed; a problem. [${TSCR},${LINENO}]\n\n"
            exit 1
        fi
        echo;ls -li --full-time ${FQNOTTOPROCESSDONEFILE};echo
        return 1
    fi

    return 0
}

function get_new_batch_number
{
    printf "\nset heading off;\nset feedback off;\n"                                                          > ${INVADJPARAM_SQL_INP}
    printf "SELECT INVADJ_SCHEMA.MAS_BATCH_SUMMARY_NBR_SEQ.NEXTVAL AS BATCH_SUMMARY_NBR FROM DUAL;\nexit;\n" >> ${INVADJPARAM_SQL_INP}
    run_sql
    NEW_BATCH_NUMBER=`cat ${INVADJPARAM_SQL_OUT}|tr -d [:space:]`
    if [ -z ${NEW_BATCH_NUMBER} ] ; then
        printf "\nERROR[`date` on `hostname`] - Empty new batch number; something did not work; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    printf "INFO[`date` on `hostname`] - NEW_BATCH_NUMBER_GENERATED (${NEW_BATCH_NUMBER}) [${TSCR},${LINENO}]\n"

    return 0
}


function do_db_stage
{
    get_new_batch_number
    typeset inbound_nm=`basename ${FQAVAILABLEDATAFILE}`

    printf "\nREM Construction of the SQL being done on `hostname` at `date`\n\n"                                                                                                                                        > ${INVADJPARAM_SQL_INP}
    printf "\ninsert into INVADJ_SCHEMA.MASS_ADJ_ACP_SUMMARY(BATCH_SUMMARY_NBR     , INBOUND_NM      , INBOUND_TMSTP     , OUTBOUND_NM , OUTBOUND_TMSTP , BATCH_CHRST_DESC , BATCH_STATUS_DESC , LAST_UPDATE_TMSTP)\n"  >> ${INVADJPARAM_SQL_INP}
    printf                                           "values('${NEW_BATCH_NUMBER}' , '${inbound_nm}' , current_timestamp , null        , null           , ''               , '${STATUS_RECEI}' , current_timestamp);\n" >> ${INVADJPARAM_SQL_INP}
    printf "commit;\nexit;\n"                                                                                                                                                                                           >> ${INVADJPARAM_SQL_INP}
    run_sql
    printf "INFO[`date` on `hostname`] - MASSADJUST_DATAFILE_STAGE_COMPLETE_IN_DB; table INVADJ_SCHEMA.MASS_ADJ_ACP_SUMMARY data (${NEW_BATCH_NUMBER},${inbound_nm}) insert done; just before now. [${TSCR},${LINENO}]\n"

    return 0
}

function do_fs_stage # file-system=fs
{
    typeset MASSADJMUT_DIR=${DATA}/sxfr/infiles/era_mass_adj_mut/large
    if [ "xprod" != "x${ENV_LEVEL}" ] ; then
        MASSADJMUT_DIR=${DATA}/sxfr/infiles/era_mass_adj_mut/medium
    fi

    if [ ! -d ${MASSADJMUT_DIR} ] ; then
        printf "\nERROR[`date` on `hostname`] - MASSADJMUT_DIR is NOT available (${MASSADJMUT_DIR}); a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    elif [ ! -d ${FILE_ARCH_DIR} ] ; then
        printf "\nERROR[`date` on `hostname`] - FILE_ARCH_DIR is NOT available (${FILE_ARCH_DIR}); a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    typeset CURR_YYYYMMDDHHMMSS=`date +"%Y%m%d%H%M%S"`
    typeset mass_adj_data_fqf=${MASSADJMUT_DIR}/${NEW_BATCH_NUMBER}_MUTUPLOAD_${CURR_YYYYMMDDHHMMSS}.csv
    typeset mass_adj_done_fqf=${mass_adj_data_fqf}.done

    cp -v ${FQAVAILABLEDATAFILE} ${mass_adj_data_fqf}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date` on `hostname`] - The ACP data file could NOT be copied as a file to MASS Adjust; the command (cp -v ${FQAVAILABLEDATAFILE} ${mass_adj_data_fqf}) failed; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    typeset -i linecount=`wc -l ${mass_adj_data_fqf}|awk '{print $1}'`
    typeset arch_data_fqf=${FILE_ARCH_DIR}/`basename ${FQAVAILABLEDATAFILE}`_`date +"%Y%m%d%H%M%S%N"`

    cp -v ${FQAVAILABLEDATAFILE} ${arch_data_fqf}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date` on `hostname`] - Archival step failure (cp -v ${FQAVAILABLEDATAFILE} ${arch_data_fqf}) failed; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    echo;ls -larti --full-time ${FQAVAILABLEDATAFILE} ${mass_adj_data_fqf} ${arch_data_fqf};echo
    printf "INFO[`date` on `hostname`] - MASSADJUST_DATAFILE_STAGE_COMPLETE_ON_FS; we staged ( `ls -li --full-time ${mass_adj_data_fqf}` , with ${linecount} lines , out_of `basename ${FQAVAILABLEDATAFILE}` ). [${TSCR},${LINENO}]\n"

    rm -fv ${FQAVAILABLEDATAFILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date` on `hostname`] - Removal data file step failure ( rm -fv ${FQAVAILABLEDATAFILE} ) failed; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    touch ${mass_adj_done_fqf}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date` on `hostname`] - Staging MASS Adjust; done_file could NOT be created; the command (touch ${mass_adj_done_fqf}) failed; a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    printf "INFO[`date` on `hostname`] - MASSADJUST_DONEFILE_STAGE_COMPLETE_ON_FS; We staged ( ${mass_adj_done_fqf} ) just about now. [${TSCR},${LINENO}]\n"

    return 0
}

function stage_one_file_as_a_batch
{
    validate_file_pair
    if [ 0 -ne $? ] ; then
        return 0 # work is done for the file pair (moved it out of the mix)
    fi

    setup_for_file_type `basename ${FQAVAILABLEDONEFILE}`

    typeset done_file_begin_processing_fqf=${FQAVAILABLEDONEFILE}_processing_begin_`date +"%Y%m%d%H%M%S%N"`
    echo;ls -li --full-time ${FQAVAILABLEDONEFILE}
    mv -v ${FQAVAILABLEDONEFILE} ${done_file_begin_processing_fqf}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date` on `hostname`] - Picking up an input file, move to processing failed (mv -v ${FQAVAILABLEDONEFILE} ${done_file_begin_processing_fqf} did not work); a problem. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi
    echo;ls -li --full-time ${done_file_begin_processing_fqf};echo

    do_db_stage
    do_fs_stage

    return 0
}

####################
# work begins here #
####################
. ${SCRIPTS}/eraCommon.sh
if [ 0 -ne $? ] ; then
    printf "\nERROR[`date` on `hostname`] - Unable to source in common; a problem. [${TSCR},${LINENO}]\n\n"
    exit 1
fi

do_init

printf "\nINFO[`date` on `hostname`] - SCRIPT_START  user=[`id --user --name`] PID=[$$] [logfile=${log}] all_input_parameters(${ALLINPUTPARAMS}) [${TSCR},${LINENO}]\n\n"
while [ 1 ]
do
    if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "\nINFO[`date` on `hostname`] - stop_file_present(`ls -li --full-time ${STOPFILE_FOR_BACKGROUND_JOB}`); hence we stop.\n";sleep ${SLSECONDS}
        break
    fi

    typeset -i count=`ls -1 ${D2WATCH}/${F2WATCH} ${PPREBILLD2WATCH}/${PPREBILLF2WATCH} ${APACD2WATCH}/${APACF2WATCH} 2>/dev/null|wc -l`
    if [ ${count} -ge 1 ] ; then
        FQAVAILABLEDONEFILE=`ls -1 ${D2WATCH}/${F2WATCH} ${PPREBILLD2WATCH}/${PPREBILLF2WATCH} ${APACD2WATCH}/${APACF2WATCH} 2>/dev/null|head -n1`
        sleep ${SLSECONDS};stage_one_file_as_a_batch
    fi
    do_report_work
    ping_alive; sleep ${SLSECONDS}
done
printf "\nINFO[`date` on `hostname`] - SCRIPT_FINISH user=[`id --user --name`] PID=[$$] [logfile=${log}] [${TSCR},${LINENO}]\n\n"

return 0

