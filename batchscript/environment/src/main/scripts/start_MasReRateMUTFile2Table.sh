#!/bin/ksh

# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504/pardaman singh
# Create Date:   2016/12/28 17:30:25
#
# Copyright (c) 2016 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`
FILEPATTERN="_MUTRERATE_"
unset CLASSPATH ; CLASSPATH=${APPLICATION_JARS} ; export CLASSPATH
if [ ! -z ${APMCLASSPATH} ] ; then
    CLASSPATH=${CLASSPATH}:${APMCLASSPATH}
fi

printf "INFO[`date`] - begin of script; hostname[`hostname`] user[`id --user --name`] PID[$$] [${TSCR},${LINENO}]\n"

. ${SCRIPTS}/eraCommon.sh

unset DIR_TO_WATCH ; DIR_TO_WATCH=${DATA}/sxfr/infiles/rerate ; export DIR_TO_WATCH
if [ ! -d ${DIR_TO_WATCH} ] ; then
    printf "\nERROR[`date`] - a necessary directory[${DIR_TO_WATCH}] is not available for us to watch, a problem -- goodbye. [${TSCR},${LINENO}]\n"
    exit 1
fi

while [ 1 -eq 1 ]
do
    if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "INFO[`date`] - stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] is present; so we obey and stop. [${TSCR},${LINENO}]\n"
        break
    fi

    typeset -i count=$(ls -1 ${DIR_TO_WATCH}/*${FILEPATTERN}*.done 2> /dev/null |wc -l)
    if [ 0 -eq ${count} ] ; then
        ping_alive; sleep ${ITERATION_INTERVAL}
        continue
    fi
    unset DONE_FILE ; DONE_FILE=$(ls -1 ${DIR_TO_WATCH}/*${FILEPATTERN}*.done|head -n1) ; export DONE_FILE
    unset DATA_FILE ; DATA_FILE=$(echo ${DONE_FILE}|sed 's/\.done$//')                  ; export DATA_FILE
    unset PROC_FILE ; PROC_FILE=${DATA_FILE}.process                                    ; export PROC_FILE
    unset SUCC_FILE ; SUCC_FILE=${DATA_FILE}.success                                    ; export SUCC_FILE
    unset FAIL_FILE ; FAIL_FILE=${DATA_FILE}.failure                                    ; export FAIL_FILE
    if [ ! -f ${DATA_FILE} ] || [ ! -s ${DATA_FILE} ] || [ ! -r ${DATA_FILE} ] ; then
        printf "\nERROR[`date`] - data-file=${DATA_FILE} corresponding to done-file=${DONE_FILE} is not available for us to work with, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    if [ ! -f ${DONE_FILE} ] || [ ! -r ${DONE_FILE} ] ; then
        printf "\nERROR[`date`] - done-file=${DATA_FILE} is not available for us to work with, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_create via the command \"touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    rm -f ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_removal via the command \"touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    rm -f ${DONE_FILE}
    if [ 0 -eq $? ] ; then
        printf "INFO[`date`] - done_file_removal via the command \"rm -f ${DONE_FILE}\" was a great success. [${TSCR},${LINENO}]\n"
    else
        printf "\nERROR[`date`] - done_file_removal via the command \"rm -f ${DONE_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    unset APPLICATION_ARGS
    APPLICATION_ARGS="-Drdt.mass.rerate.mut.filename=${DATA_FILE} -Drdt.mass.rerate.mut.directory=${DIR_TO_WATCH} ${RDT_JSECPROP_DEFINE} ${APMJAVAOPTIONS} -cp ${CLASSPATH}"
    export APPLICATION_ARGS

    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] -\nJAVA=${JAVA}\nAPPLICATION_ARGS=${APPLICATION_ARGS}\nAPPLICATION_CLASS=${APPLICATION_CLASS}\nin [${TSCR},${LINENO}]\n"
    fi

    SUBLOGFILE=${LOGS}/sublog_${APPLICATION_NAME}.`date +"%Y%m%d%H%M%S%N"`.P$$.R${RANDOM}.log.txt;sleep 3
    printf "INFO[`date`] - application_start data-file=${DATA_FILE} sublogfile=${SUBLOGFILE} [${TSCR},${LINENO}]\n"
    ${JAVA} ${APPLICATION_ARGS} ${APPLICATION_CLASS} > ${SUBLOGFILE} 2>&1 ; typeset -i app_ret_val=$?
    printf "INFO[`date`] - application_stop  app_ret_val=[${app_ret_val}],data-file=${DATA_FILE} [${TSCR},${LINENO}]\n"

    if [ -f ${SUCC_FILE} ] && [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ ! -f ${SUCC_FILE} ] && [ ! -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - neither_success_nor_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -f ${SUCC_FILE} ] && [ -f ${PROC_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_process_present_not_good ${SUCC_FILE} ${PROC_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - failure_reported_by_application ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${PROC_FILE} ] ; then
        printf "\nERROR[`date`] - process_file_present_not_good ${PROC_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${SUCC_FILE} ] ; then
        printf "INFO[`date`] - success_reported_by_application ${SUCC_FILE} [${TSCR},${LINENO}]\n"
        data_filename_only=$(basename ${DATA_FILE})
        archive_dfname=${ARCHIVE_DIR}/45D/rerate/${data_filename_only}.`date +%Y-%m-%dT%H%M%SUTC`
        cp ${DATA_FILE} ${archive_dfname}
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date`] - archival_step_failure ${DATA_FILE} ${archive_dfname} [${TSCR},${LINENO}]\n"
            exit 1
        else
            printf "INFO[`date`] - archival_with_the_command \"cp ${DATA_FILE} ${archive_dfname}\" was a grand success. [${TSCR},${LINENO}]\n"
        fi
        rm -f ${DATA_FILE} ${SUCC_FILE}
        if [ 0 -eq $? ] ; then
            printf "INFO[`date`] - data_and_success_file_removal via the command \"rm -f ${DATA_FILE} ${SUCC_FILE}\" was a great success. [${TSCR},${LINENO}]\n"
        else
            printf "\nERROR[`date`] - data_file_removal_failure ${DATA_FILE} ${SUCC_FILE} [${TSCR},${LINENO}]\n"
            exit 1
        fi
    fi
    printf "INFO[`date`] - job_done_going_to_look_for_more_data_files_now [${TSCR},${LINENO}]\n"
done

printf "INFO[`date`] - end of script; PID[$$] [${TSCR},${LINENO}]\n"

return 0

