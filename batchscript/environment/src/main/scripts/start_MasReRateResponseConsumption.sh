#!/bin/ksh
# @(#)$URL: https://svn1.prod.fedex.com/svn/repos/rdt_invadj/branches/RB-R60.CL1702/rdt-environment/src/main/scripts/start_MasReRateResponseConsumption.sh $$Rev: 16413 $$Date: 2017-01-31 13:16:13 -0800 (Tue, 31 Jan 2017) $$Author: 487504 $

###############################################################################
#    $HeadURL: https://svn1.prod.fedex.com/svn/repos/rdt_invadj/branches/RB-R60.CL1702/rdt-environment/src/main/scripts/start_MasReRateResponseConsumption.sh $
#   $Revision: 16413 $
# Tab Setting:   4
#      Author:   487504
# Create Date:   2017/01/05 22:26:02
#
# Copyright (c) 2017 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`

SUCC_FILEPATTERN="-RERATE_"
EXCP_FILEPATTERN="-EXCP_"

unset CLASSPATH ; CLASSPATH=${APPLICATION_JARS} ; export CLASSPATH
if [ ! -z ${APMCLASSPATH} ] ; then
    CLASSPATH=${CLASSPATH}:${APMCLASSPATH}
fi

printf "INFO[`date`] - begin of script; hostname[`hostname`] user[`id --user --name`] PID[$$] [${TSCR},${LINENO}]\n"

. ${SCRIPTS}/eraCommon.sh

unset DIR_TO_WATCH ; DIR_TO_WATCH=${DATA}/sxfr/infiles/rerated ; export DIR_TO_WATCH
if [ ! -d ${DIR_TO_WATCH} ] ; then
    printf "\nERROR[`date`] - a necessary directory[${DIR_TO_WATCH}] is not available for us to watch, a problem -- goodbye. [${TSCR},${LINENO}]\n"
    exit 1
fi

while [ 1 -eq 1 ]
do
    if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "INFO[`date`] - stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] is present; so we obey and stop. [${TSCR},${LINENO}]\n"
        break
    fi

    typeset -i succ_done_count=$(ls -1 ${DIR_TO_WATCH}/*${SUCC_FILEPATTERN}*.done 2> /dev/null |wc -l)
    typeset -i excp_done_count=$(ls -1 ${DIR_TO_WATCH}/*${EXCP_FILEPATTERN}*.done 2> /dev/null |wc -l)
    if [ 0 -eq ${succ_done_count} ] || [ 0 -eq ${excp_done_count} ] ; then
        ping_alive; sleep ${ITERATION_INTERVAL}
        continue
    fi

    unset SUCC_DONE_FILE ; SUCC_DONE_FILE=$(ls -1 ${DIR_TO_WATCH}/*${SUCC_FILEPATTERN}*.done|sort|head -n1)               ; export SUCC_DONE_FILE
    unset SUCC_DATA_FILE ; SUCC_DATA_FILE=$(echo ${SUCC_DONE_FILE} |sed 's/\.done$//')                                    ; export SUCC_DATA_FILE
    unset REQ_ID_NBR     ; REQ_ID_NBR=$(echo ${SUCC_DONE_FILE} |cut -f1 -d'-')                                            ; export REQ_ID_NBR
    unset EXCP_DONE_FILE ; EXCP_DONE_FILE=$(ls -1 ${DIR_TO_WATCH}/*${EXCP_FILEPATTERN}*.done|grep ${REQ_ID_NBR}|head -n1) ; export EXCP_DONE_FILE
    unset EXCP_DATA_FILE ; EXCP_DATA_FILE=$(echo ${EXCP_DONE_FILE} |sed 's/\.done$//')                                    ; export EXCP_DATA_FILE

    if [ -z "${SUCC_DONE_FILE}" ] ; then
        printf "\nERROR[`date`] - variable for SUCC_DONE_FILE is empty; succ_done_count[${succ_done_count}],[${excp_done_count}] something did not work; it is a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -z "${SUCC_DATA_FILE}" ] ; then
        printf "\nERROR[`date`] - variable for SUCC_DATA_FILE is empty; SUCC_DONE_FILE[${SUCC_DONE_FILE}],succ_done_count[${succ_done_count}],[${excp_done_count}] something did not work; it is a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -z "${REQ_ID_NBR}" ] ; then
        printf "\nERROR[`date`] - variable for REQ_ID_NBR is empty; SUCC_DONE_FILE[${SUCC_DONE_FILE}],succ_done_count[${succ_done_count}],[${excp_done_count}] something did not work; it is a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -z "${EXCP_DONE_FILE}" ] ; then
        printf "\nERROR[`date`] - variable for EXCP_DONE_FILE is empty; SUCC_DONE_FILE[${SUCC_DONE_FILE}],succ_done_count[${succ_done_count}],[${excp_done_count}] something did not work; it is a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -z "${EXCP_DATA_FILE}" ] ; then
        printf "\nERROR[`date`] - variable for EXCP_DATA_FILE is empty; SUCC_DONE_FILE[${SUCC_DONE_FILE}],succ_done_count[${succ_done_count}],[${excp_done_count}] something did not work; it is a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    typeset f=""
    for f in ${SUCC_DONE_FILE} ${SUCC_DATA_FILE} ${EXCP_DONE_FILE} ${EXCP_DATA_FILE}
    do
        if [ ! -f ${f} ] || [ ! -r ${f} ] ; then
            printf "\nERROR[`date`] - file[${f}] is not available to this user/script; it is a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
    done
    echo ${SUCC_DATA_FILE}|grep -q ${REQ_ID_NBR}; typeset -i data_reqid_found=$?
    echo ${EXCP_DATA_FILE}|grep -q ${REQ_ID_NBR}; typeset -i excp_reqid_found=$?
    if [ 0 -ne ${data_reqid_found} ] || [ 0 -ne ${excp_reqid_found} ] ; then
        printf "\nERROR[`date`] - REQ_ID_NBR[${REQ_ID_NBR}] is NOT in both SUCC_DATA_FILE[${SUCC_DATA_FILE}],EXCP_DATA_FILE[${EXCP_DATA_FILE}] something not right -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    printf "INFO[`date`] - data and exception files we are about to process begin-listing [${TSCR},${LINENO}]\n"
    ls -lart ${SUCC_DONE_FILE} ${SUCC_DATA_FILE} ${EXCP_DONE_FILE} ${EXCP_DATA_FILE}
    printf "INFO[`date`] - data and exception files we are about to process end-listing [${TSCR},${LINENO}]\n"

    APP_PROC_FILE=${SUCC_DATA_FILE}.process
    APP_SUCC_FILE=${SUCC_DATA_FILE}.success
    APP_FAIL_FILE=${SUCC_DATA_FILE}.failure
    touch ${APP_PROC_FILE} ${APP_SUCC_FILE} ${APP_FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_create via the command \"touch ${APP_PROC_FILE} ${APP_SUCC_FILE} ${APP_FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    rm -f ${APP_PROC_FILE} ${APP_SUCC_FILE} ${APP_FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_removal via the command \"rm -f ${APP_PROC_FILE} ${APP_SUCC_FILE} ${APP_FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    rm -f ${SUCC_DONE_FILE} ${EXCP_DONE_FILE}
    if [ 0 -eq $? ] ; then
        printf "INFO[`date`] - done_files_removal via the command \"rm -f ${SUCC_DONE_FILE} ${EXCP_DONE_FILE}\" was a great success. [${TSCR},${LINENO}]\n"
    else
        printf "\nERROR[`date`] - done_files_removal via the command \"rm -f ${SUCC_DONE_FILE} ${EXCP_DONE_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    unset APPLICATION_ARGS
    APPLICATION_ARGS=" -Drdt.mass.rerate.response.filename=${SUCC_DATA_FILE} -Drdt.mass.rerate.response.exception.filename=${EXCP_DATA_FILE} -Drdt.mass.rerate.directory=`dirname ${SUCC_DATA_FILE}` "
    APPLICATION_ARGS="${APPLICATION_ARGS} ${RDT_JSECPROP_DEFINE} ${APMJAVAOPTIONS} -cp ${CLASSPATH}"
    export APPLICATION_ARGS

    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] -\nJAVA=${JAVA}\nAPPLICATION_ARGS=${APPLICATION_ARGS}\nAPPLICATION_CLASS=${APPLICATION_CLASS}\nin [${TSCR},${LINENO}]\n"
    fi

    SUBLOGFILE=${LOGS}/sublog_${APPLICATION_NAME}.`date +"%Y%m%d%H%M%S%N"`.P$$.R${RANDOM}.log.txt;sleep 3
    printf "INFO[`date`] - application_start SUCC_DATA_FILE[${SUCC_DATA_FILE}],EXCP_DATA_FILE[${EXCP_DATA_FILE}] [logfile=${SUBLOGFILE}] [${TSCR},${LINENO}]\n"
    ${JAVA} ${APPLICATION_ARGS} ${APPLICATION_CLASS} > ${SUBLOGFILE} 2>&1 ; typeset -i app_ret_val=$?
    printf "INFO[`date`] - application_stop  app_ret_val[${app_ret_val}],SUCC_DATA_FILE[${SUCC_DATA_FILE}],EXCP_DATA_FILE[${EXCP_DATA_FILE}] [${TSCR},${LINENO}]\n"

    if [ -f ${APP_SUCC_FILE} ] && [ -f ${APP_FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_failure_present_not_good ${APP_SUCC_FILE} ${APP_FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ ! -f ${APP_SUCC_FILE} ] && [ ! -f ${APP_FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - neither_success_nor_failure_present_not_good ${APP_SUCC_FILE} ${APP_FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -f ${APP_SUCC_FILE} ] && [ -f ${APP_PROC_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_process_present_not_good ${APP_SUCC_FILE} ${APP_PROC_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${APP_FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - failure_reported_by_application ${APP_FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${APP_PROC_FILE} ] ; then
        printf "\nERROR[`date`] - process_file_present_not_good ${APP_PROC_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${APP_SUCC_FILE} ] ; then
        printf "INFO[`date`] - success_reported_by_application ${APP_SUCC_FILE} [${TSCR},${LINENO}]\n"

        typeset timestamp=`date +%Y-%m-%dT%H%M%SUTC`

        succ_data_filename_only=$(basename ${SUCC_DATA_FILE})
        succ_arch_dfname=${ARCHIVE_DIR}/45D/rerate/${succ_data_filename_only}.${timestamp}
        excp_data_filename_only=$(basename ${EXCP_DATA_FILE})
        excp_arch_dfname=${ARCHIVE_DIR}/45D/rerate/${excp_data_filename_only}.${timestamp}

        cp ${SUCC_DATA_FILE} ${succ_arch_dfname}
        if [ 0 -ne $? ] ; then
            printf "ERROR[`date`] - archival_with_the_command \"cp ${SUCC_DATA_FILE} ${succ_arch_dfname}\" did NOT succeed -- a problem. [${TSCR},${LINENO}]\n"
            exit 1
        else
            printf "INFO[`date`] - archival_with_the_command \"cp ${SUCC_DATA_FILE} ${succ_arch_dfname}\" was a grand success. [${TSCR},${LINENO}]\n"
        fi

        cp ${EXCP_DATA_FILE} ${excp_arch_dfname}
        if [ 0 -ne $? ] ; then
            printf "INFO[`date`] - archival_with_the_command \"cp ${EXCP_DATA_FILE} ${excp_arch_dfname}\" did NOT succeed -- a problem. [${TSCR},${LINENO}]\n"
            exit 1
        else
            printf "INFO[`date`] - archival_with_the_command \"cp ${EXCP_DATA_FILE} ${excp_arch_dfname}\" was a grand success. [${TSCR},${LINENO}]\n"
        fi

        rm -f ${SUCC_DATA_FILE} ${EXCP_DATA_FILE} ${APP_SUCC_FILE}
        if [ 0 -eq $? ] ; then
            printf "INFO[`date`] - data_and_success_files_removal via the command \"rm -f ${SUCC_DATA_FILE} ${EXCP_DATA_FILE} ${APP_SUCC_FILE}\" was a great success. [${TSCR},${LINENO}]\n"
        else
            printf "ERROR[`date`] - data_and_success_files_removal via the command \"rm -f ${SUCC_DATA_FILE} ${EXCP_DATA_FILE} ${APP_SUCC_FILE}\" was NOT a success. [${TSCR},${LINENO}]\n"
            exit 1
        fi
    fi
    printf "INFO[`date`] - job_done_going_to_look_for_more_dataandexcp_files_now [${TSCR},${LINENO}]\n"
done

printf "INFO[`date`] - end of script; PID[$$] [${TSCR},${LINENO}]\n"

return 0

