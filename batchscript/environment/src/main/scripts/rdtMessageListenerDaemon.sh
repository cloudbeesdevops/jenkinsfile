#!/bin/ksh

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504 (pardaman singh)
# Create Date:   2016/08/22 15:00:48
#
# Copyright (c) 2016 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################
# A script to allow for the start/stop/status of daemon implementation of
# org.springframework.jms.listener.DefaultMessageListenerContainer
###############################################################################

            TSCRDTTM=`date +%Y-%m-%dT%H%M%SUTC`                       # this script start date and time
                TSCR=`basename $0`                                    # this script name
      SHORT_HOSTNAME=`hostname|cut -f1 -d'.'`                         # short hostname for this machine
               ALIAS=`cnames|grep CNAME|head -n1|awk '{print $2}'`    # one alias name for this machine
                   U=`id --user --name`                               # the user who is executing this script
                 RPT=${LOGFILES}/${TSCR%.sh}.report.txt               # the report filename
      ALL_PARAMETERS="$*"                                             # all input parameters to this script
               EXP_U="rdt"                                            # the expected user of this scrip
       ERACOMMON_SCR=${SCRIPTS}/eraCommon.sh

function p_banner
{
    printf "\n"
    printf "\tusage: ${TSCR} -m mode -s <value>\n"
    printf "\t-m    : mode       -- start , stop , status\n"
    printf "\t-s    : subscriber -- RDTRebillPublishBatch , RDTRebillIntlSubscriber , RDTMASSubscriber , RDTEDICoreProcessor , RDTRecycleDataSubscriber\n"
    printf "\n"

    return 0
}

function get_subscriber_pid
{
    let subscriber_pid=0
    typeset -i count=$(ps -fu ${EXP_U}|grep java|grep "${subscriber_c_start}"|grep -v grep|wc -l)
    if [ 0 -eq ${count} ] ; then
        echo > /dev/null
    elif [ 1 -eq ${count} ] ; then
        let subscriber_pid=$(ps -fu ${EXP_U}|grep java|grep "${subscriber_c_start}"|grep -v grep| awk '{print $2}')
    else
        printf "[---begin-pid-info at `date` on `hostname`---]\n"   >> ${RPT}
        ps -fu ${EXP_U}|grep java|grep "${subscriber_c_start}"|grep -v grep >> ${RPT}
        printf "[---end-pid-info---]\n"                             >> ${RPT}
        printf "\nSomething did not work out; we thought atmost one PID could be out there for \"${subscriber_d} with class ${subscriber_c_start}\" check what is going on. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    fi

    return 0
}

function construct_classpath
{
    typeset directory=""
    for directory in ${BIN}/${subscriber_d} ${LIB}/${subscriber_d} ${RDT_DEVFRAMEWORK_DIR}/lib
    do
        if [ ! -d ${directory} ] ; then
            printf "\nERROR - Directory \"${directory}\" is not available at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
            exit 1
        fi

        typeset count=$(ls -1 ${directory}/*.jar 2> /dev/null |wc -l)
        if [ 0 -eq ${count} ] ; then
            printf "\nERROR - There are zero jar files in directory \"${directory}\" (not a good thing) at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
            exit 1
        fi

        typeset f=""
        for f in `ls -1 ${directory}/*.jar|sort`
        do
            if [ -z "${CLASSPATH}" ] ; then
                CLASSPATH=${f}
            else
                CLASSPATH=${CLASSPATH}:${f}
            fi
        done
    done

    if [ ! -z ${APMCLASSPATH} ] ; then
        CLASSPATH=${CLASSPATH}:${APMCLASSPATH}
    fi

    return 0
}

function do_variable_expansion
{
    printf "\nINFO - In do_variable_expansion() for [${subscriber_d}] -- begin [${TSCR},${LINENO}]\n"
    case "${subscriber_d}" in
        "RDTEDICoreProcessor")
            ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-edi-autoadjust/config.template                         ${CFG}/rdt-edi-autoadjust/config.properties
            ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-edi-autoadjust/logger.template                         ${CFG}/rdt-edi-autoadjust/logger.properties
            ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-edi-autoadjust/properties/security.properties.template ${CFG}/rdt-edi-autoadjust/properties/security.properties
            ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-edi-autoadjust/properties/fp.properties.template       ${CFG}/rdt-edi-autoadjust/properties/fp.properties
            ${SCRIPTS}/invadj-rdt-admin/ExpandEnvironmentVariables.sh ${CFG}/rdt-edi-autoadjust/properties/client.properties.template   ${CFG}/rdt-edi-autoadjust/properties/client.properties
            ;;
        *)
            echo > /dev/null # do nothing
            ;;
    esac
    printf "INFO - In do_variable_expansion() for [${subscriber_d}] -- finish [${TSCR},${LINENO}]\n\n"

    return 0
}

function do_start
{
    get_subscriber_pid
    if [ 0 -ne ${subscriber_pid} ] ; then
        printf "\nThe PID number \"${subscriber_pid}\" is already out there for a subscriber \"${subscriber_c_start}\" during do_start() -- goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    fi
    do_variable_expansion >> ${RPT} 2>&1
    construct_classpath
    typeset start_dttm=`date +%Y-%m-%dT%H%M%SUTC`
    typeset nohup_rpt=${LOGFILES}/${TSCR%.sh}.${subscriber_d}_${start_dttm}_start_nohup_rpt.${RANDOM}.txt
    typeset options="-Xms1024m -Xmx2048m -XX:-UseConcMarkSweepGC ${RDT_JSECPROP_DEFINE} ${APMJAVAOPTIONS} -DRDT_EDI_AUTOADJUST_CONFIG_DIR=${CFG}/rdt-edi-autoadjust/ -Dlog4j.debug -cp ${CLASSPATH}"

    printf "\nINFO - For do_start() input to this script is \"${ALL_PARAMETERS}\" [${TSCR},${LINENO}]\n"  >> ${RPT}
    printf "\nINFO - On \"`hostname`\" at \"`date`\" as \"${U}\" we are executing the command\n"          >> ${RPT}
    printf "nohup ${RDT_JAVA_HOME}/bin/java ${options} ${subscriber_c_start} > /dev/null 2>&1 &"          >> ${RPT}

    nohup ${RDT_JAVA_HOME}/bin/java ${options} ${subscriber_c_start} > /dev/null 2>&1 &
    typeset -i bPID=$!
    sleep 3

    printf "End of do_start() at `date` on `hostname` [${TSCR},${LINENO}]\n" >> ${RPT}

    return 0
}

function do_stop
{
    get_subscriber_pid
    if [ 0 -eq ${subscriber_pid} ] ; then
        printf "\nNo PID up for the subscriber \"${subscriber_c_start}\" during do_stop() -- goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    fi
    construct_classpath
    typeset stop_dttm=`date +%Y-%m-%dT%H%M%SUTC`
    typeset nohup_rpt=${LOGFILES}/${TSCR%.sh}_${RANDOM}_${start_dttm}_stop_nohup_rpt.txt
    typeset options="-Xms1024m -Xmx2048m -XX:-UseConcMarkSweepGC -cp ${CLASSPATH}"

    printf "\nINFO - For do_stop() input to this script is \"${ALL_PARAMETERS}\" [${TSCR},${LINENO}]\n" >> ${RPT}
    printf "\nINFO - On \"`hostname`\" at \"`date`\" as \"${U}\" we are executing the command\n"        >> ${RPT}
    printf " ${RDT_JAVA_HOME}/bin/java ${options} ${subscriber_c_stop} localhost ${registry_port}\n"    >> ${RPT}
    printf "\nCLASSPATH is\n${CLASSPATH}\n\n"                                                           >> ${RPT}
    printf "\nThe nohup report is in ${nohup_rpt}\n"                                                    >> ${RPT}

    nohup ${RDT_JAVA_HOME}/bin/java ${options} ${subscriber_c_stop} localhost ${registry_port} > ${nohup_rpt} 2>&1 &
    sleep 11
    get_subscriber_pid
    if [ 0 -ne ${subscriber_pid} ] ; then
        printf "\nWe tried stopping the application for \"${subscriber_d}\", and we waited, but it did not come down; try another way -- goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    fi

    printf "End of do_start() at `date` on `hostname` [${TSCR},${LINENO}]\n" >> ${RPT}

    return 0
}

function do_status
{
    get_subscriber_pid
    if [ 0 -eq ${subscriber_pid} ] ; then
        printf "\nNo PID up for the subscriber \"${subscriber_d}\" during do_status()-- goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    fi

    return 0
}

function do_validations
{
    if [ "x${EXP_U}" != "x${U}" ] ; then
        printf "\nERROR - The actual-user \"${U}\" is NOT the same as the expected-user \"${EXP_U}\" at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    fi

    if [ ! -d ${RDT_DEVFRAMEWORK_DIR}/lib ] ; then
        printf "\nERROR - Directory \"${RDT_DEVFRAMEWORK_DIR}/lib\" is not available at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    fi

    if [ ! -x ${RDT_JAVA_HOME}/bin/java ] ; then
        printf "\nERROR - Executable \"${RDT_JAVA_HOME}/bin/java\" is not available at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    fi

    if [ -z "${mode}" ] ; then
        printf "\nERROR - empty input mode (a problem) by \"${U}\" at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        p_banner
        exit 1
    fi

    if [ -z "${subscriber_d}" ] ; then
        printf "\nERROR - empty input subscriber (a problem) by \"${U}\" at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        p_banner
        exit 1
    fi

    case "${subscriber_d}" in
        "RDTRebillIntlSubscriber")
            registry_port=1098
            subscriber_c_start=com.fedex.srs.rdt.rebill.international.trigger.StartupJMSListenerContainer
            subscriber_c_stop=com.fedex.srs.rdt.rebill.international.trigger.StopJMSListenerContainer
            ;;
        "RDTRebillPublishBatch")
            registry_port=1099
            subscriber_c_start=com.fedex.srs.rdt.rebill.release.trigger.StartupJMSListenerContainer
            subscriber_c_stop=com.fedex.srs.rdt.rebill.release.trigger.StopJMSListenerContainer
            ;;
        "RDTMASSubscriber")
            registry_port=1090
            subscriber_c_start=com.fedex.srs.rdt.mass.subscriber.processor.StartMASSubscriberProcessor
            subscriber_c_stop=com.fedex.srs.rdt.mass.subscriber.processor.StopJMSListenerContainer
            ;;
        "RDTEDICoreProcessor")
            registry_port=1097
            subscriber_c_start=com.fedex.srs.rdt.edi.batch.trigger.StartupJMSListenerContainer
            subscriber_c_stop=com.fedex.srs.rdt.edi.batch.trigger.StopJMSListenerContainer
            ;;
        "RDTRecycleDataSubscriber")
            registry_port=1096
            subscriber_c_start=com.fedex.srs.rdt.recycle.trigger.StartupJMSListenerContainer
            subscriber_c_stop=com.fedex.srs.rdt.recycle.trigger.StopJMSListenerContainer
            ;;
        *)
            printf "\nERROR - Invalid subscriber; all parameters were \"${ALL_PARAMETERS}\" by \"${U}\" at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
            p_banner
            exit 1
            ;;
    esac

    if [ ! -d ${BIN}/${subscriber_d} ] ; then
        printf "\nERROR - Not found bin-subscriber-directory \"${BIN}/${subscriber_d}\" (a problem) by \"${U}\" at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    elif [ ! -d ${LIB}/${subscriber_d} ] ; then
        printf "\nERROR - Not found lib-subscriber-directory \"${LIB}/${subscriber_d}\" (a problem) by \"${U}\" at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
        exit 1
    fi

    if [ ! -f ${ERACOMMON_SCR} ] || [ ! -s ${ERACOMMON_SCR} ] || [ ! -r ${ERACOMMON_SCR} ] || [ ! -x ${ERACOMMON_SCR} ] ; then
        printf "\nERROR[`date`] -- The script=${ERACOMMON_SCR} is not availabe - a problem; goodbye [${TSCR},${LINENO}]\n"
        exit 1
    fi
    . ${ERACOMMON_SCR}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] -- Sourcing in the script=${ERACOMMON_SCR} gave us a non-zero return value - a problem; goodbye [${TSCR},${LINENO}]\n"
        exit 1
    fi
    doBatchAPMwork ${subscriber_d}

    return 0
}

function perform_work
{
    case "x${mode}" in
        "xSTATUS")
            do_status
            break
            ;;
        "xSTART")
            do_start
            break
            ;;
        "xSTOP")
            do_stop
            break
            ;;
        *)
            printf "\nERROR - Invalid mode; all parameters were \"${ALL_PARAMETERS}\" by \"${U}\" at `date` on `hostname` - goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
            p_banner
            exit 1
            ;;
    esac

    return 0
}

#########################
# main work starts here #
#########################
unset CLASSPATH ; CLASSPATH="" ; export CLASSPATH
typeset -i subscriber_pid=0
typeset -i registry_port=0       # specific to the subscriber, in the source code for it (port).
typeset    subscriber_d=""       # specific to the subscriber, directory name in the source code for it.
typeset    subscriber_c_start="" # specific to the subscriber, in the source code for it (class to start).
typeset    subscriber_c_stop=""  # specific to the subscriber, in the source code for it (class to stop).

typeset -u mode=""
typeset -u getoptsOption=""
while getopts m:M:s:S: getoptsOption > /dev/null 2> /dev/null
do
    case ${getoptsOption} in
        M) mode=${OPTARG}
           ;;
        S) subscriber_d=${OPTARG}
           ;;
        \?) printf "\nERROR - invalid input and/or option(s) [${ALL_PARAMETERS}] passed in as input to this script -- goodbye. [${TSCR},${LINENO}]\n" | tee -a ${RPT}
            p_banner
            exit 1
            ;;
    esac
done

do_validations
perform_work

return 0

