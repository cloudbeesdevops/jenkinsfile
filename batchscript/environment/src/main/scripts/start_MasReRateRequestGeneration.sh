#!/bin/ksh
# @(#)$URL: https://svn1.prod.fedex.com/svn/repos/rdt_invadj/branches/RB-R60.CL1702/rdt-environment/src/main/scripts/start_MasReRateRequestGeneration.sh $$Rev: 16413 $$Date: 2017-01-31 13:16:13 -0800 (Tue, 31 Jan 2017) $$Author: 487504 $

###############################################################################
#    $HeadURL: https://svn1.prod.fedex.com/svn/repos/rdt_invadj/branches/RB-R60.CL1702/rdt-environment/src/main/scripts/start_MasReRateRequestGeneration.sh $
#   $Revision: 16413 $
# Tab Setting:   4
#      Author:   487504/pardaman singh
# Create Date:   2017/01/05 22:26:02
#
# Copyright (c) 2017 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`
FILEPATTERN="_MUTRERATE_"
unset CLASSPATH ; CLASSPATH=${APPLICATION_JARS} ; export CLASSPATH
if [ ! -z ${APMCLASSPATH} ] ; then
    CLASSPATH=${CLASSPATH}:${APMCLASSPATH}
fi
BATCH_SUMMARY_NBR=""
RERATE_REQUEST_ID_NBR=""
let ITERATION_INTERVAL=60

OUTPUT_DIR=${DATA}/sxfr/outfiles/rerate
OUTPUT_DFNAME=""

function setupDB
{
    #export DB_TARGET=INVADJ_SVC1
    #export TNS_ADMIN=/opt/fedex/dadmco/srsenv/current/cfg/tns_admin
    #. /opt/fedex/pbs/env/1.1/etc/source.env -G dadmco -n srsenv -a
    # undesirable if condition below since the above is not reliable.
    # once above is good; below can be removed (most of the if condition).

    if [ "xdvel" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_DEV
        DBPWD=perftest
    elif [ "xas" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_INT
        DBPWD=apppwdli
    elif [ "xts" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_SYS
        DBPWD=apppwdls
    elif [ "xfc" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_PLA
        DBPWD=donotknow
    elif [ "xprod" = "x${ENV_LEVEL}" ] ; then
        DBSVR=INVADJ_SVC_PRD
        DBPWD=p9R1O8d
    else
        printf "\nERROR[`date`] - db credentials we do not know for [${ENV_LEVEL}], a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    export DBLOG=INVADJ_APP
    export DBSVR
    export DBPWD

    queryDB ping

    return 0
}

function queryDB
{
    typeset sqlfileinp=/var/tmp/${TSCR%.sh}.$$.${RANDOM}.`date "+%Y%m%d%H%M%S"`.sql
    typeset sqlfileout=${sqlfileinp}.output.txt

    if [ "xping" = "x$1" ] ; then
        whence -q sqlplus tnsping
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date`] - one or both of sqlplus,tnsping are not available; a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        printf "\-\- DBLOG=${DBLOG},DBSVR=${DBSVR}\n"                                                                     >> ${sqlfileinp}
        printf "select sys_context('USERENV','DB_NAME') , sys_context('USERENV','DB_UNIQUE_NAME') , sysdate from dual;\n" >> ${sqlfileinp}
        printf "exit;\n"                                                                                                  >> ${sqlfileinp}

        echo ${DBPWD} | sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout} 2>&1
        grep -q "^ORA\-" ${sqlfileout}
        if [ 0 -eq $? ] ; then
            printf "\nERROR[`date`] - Our ping-sqlplus command [sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout}] did NOT work; a problem -- goodbye. [${TSCR},${LINENO}]\n"
            exit 1
        fi
        mv ${sqlfileinp} ${sqlfileinp}.save
        mv ${sqlfileout} ${sqlfileout}.save
        printf "\nINFO[`date`] - Our ping-sqlplus command DID work; [sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout}] input-and-output moved to .save for reference. [${TSCR},${LINENO}]\n"
        return 0
    fi

    printf "set heading off;\n"                                                                                                                                                                             >> ${sqlfileinp}
    printf "set feedback off;\n"                                                                                                                                                                            >> ${sqlfileinp}
    printf "select batch_summary_nbr,rerate_request_id_nbr from invadj_schema.mass_adj_summary where rerate_request_id_nbr is not null and nvl(rerate_status_cd,'') = 'READY_FOR_RERATE' order by 1 asc;\n" >> ${sqlfileinp}
    printf "exit;\n"                                                                                                                                                                                        >> ${sqlfileinp}

    echo ${DBPWD} | sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout} 2>&1
    grep -q "^ORA\-" ${sqlfileout}
    if [ 0 -eq $? ] ; then
        printf "\nERROR[`date`] - Our real-sqlplus command [sqlplus -s ${DBLOG}@${DBSVR} @${sqlfileinp} > ${sqlfileout}] did NOT work; a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    BATCH_SUMMARY_NBR=""
    RERATE_REQUEST_ID_NBR=""
    typeset -i line_count=$(cat ${sqlfileout}|grep -v "^$"|wc -l)
    if [ ${line_count} -gt 0 ] ; then
            BATCH_SUMMARY_NBR=$(cat ${sqlfileout}|grep -v "^$"|head -n1|awk '{print $1}')
        RERATE_REQUEST_ID_NBR=$(cat ${sqlfileout}|grep -v "^$"|head -n1|awk '{print $2}')
        printf "\nINFO[`date`] - Found [${BATCH_SUMMARY_NBR}],[${RERATE_REQUEST_ID_NBR}] in the database [${DBSVR}] ready for a rerate. [${TSCR},${LINENO}]\n"
    fi
    rm -f ${sqlfileinp} ${sqlfileout}

    return 0
}

#########################
# main work begins here #
#########################
printf "INFO[`date`] - begin of script; hostname[`hostname`] user[`id --user --name`] PID[$$] [${TSCR},${LINENO}]\n"

. ${SCRIPTS}/eraCommon.sh

setupDB

while [ 1 -eq 1 ]
do
    if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "INFO[`date`] - stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] is present; so we obey and stop. [${TSCR},${LINENO}]\n"
        break
    fi

    queryDB
    if [ -z "${RERATE_REQUEST_ID_NBR}" ] ; then
        ping_alive; sleep ${ITERATION_INTERVAL}
        continue
    fi

    OUTPUT_DFNAME=${OUTPUT_DIR}/${RERATE_REQUEST_ID_NBR}-RPAR-
    DONE_FILE=${OUTPUT_DFNAME}.done
    PROC_FILE=${OUTPUT_DFNAME}.process
    SUCC_FILE=${OUTPUT_DFNAME}.success
    FAIL_FILE=${OUTPUT_DFNAME}.failure

    touch ${OUTPUT_DFNAME} ${DONE_FILE} ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_create via the command \"touch ${OUTPUT_DFNAME} ${DONE_FILE} ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    rm -f ${OUTPUT_DFNAME} ${DONE_FILE} ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_removal via the command \"rm -f ${OUTPUT_DFNAME} ${DONE_FILE} ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    unset APPLICATION_ARGS
    APPLICATION_ARGS=" -Drdt.mass.rerate.mut.batch.summary.nbr=${BATCH_SUMMARY_NBR} -Drdt.mass.rerate.mut.rerate.request.id.nbr=${RERATE_REQUEST_ID_NBR} -Drdt.mass.rerate.mut.directory=`dirname ${OUTPUT_DFNAME}` "
    APPLICATION_ARGS="${APPLICATION_ARGS} ${RDT_JSECPROP_DEFINE} ${APMJAVAOPTIONS} -cp ${CLASSPATH}"
    export APPLICATION_ARGS

    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] -\nOUTPUT_DFNAME=${OUTPUT_DFNAME}\nDONE_FILE=${DONE_FILE}\nPROC_FILE=${PROC_FILE}\nSUCC_FILE=${SUCC_FILE}\nFAIL_FILE=${FAIL_FILE}\n"
        printf "DEBUG[`date`] -\nJAVA=${JAVA}\nAPPLICATION_ARGS=${APPLICATION_ARGS}\nAPPLICATION_CLASS=${APPLICATION_CLASS}\nin [${TSCR},${LINENO}]\n"
    fi

    SUBLOGFILE=${LOGS}/sublog_${APPLICATION_NAME}.`date +"%Y%m%d%H%M%S%N"`.P$$.R${RANDOM}.log.txt;sleep 3
    printf "INFO[`date`] - application_start BATCH_SUMMARY_NBR[${BATCH_SUMMARY_NBR}],RERATE_REQUEST_ID_NBR[${RERATE_REQUEST_ID_NBR}],OUTPUT_DFNAME[${OUTPUT_DFNAME}],[logfile=${SUBLOGFILE}] [${TSCR},${LINENO}]\n"
    ${JAVA} ${APPLICATION_ARGS} ${APPLICATION_CLASS} > ${SUBLOGFILE} 2>&1 ; typeset -i app_ret_val=$?
    printf "INFO[`date`] - application_stop app_ret_val[${app_ret_val}],BATCH_SUMMARY_NBR[${BATCH_SUMMARY_NBR}],RERATE_REQUEST_ID_NBR[${RERATE_REQUEST_ID_NBR}],OUTPUT_DFNAME[${OUTPUT_DFNAME}] [${TSCR},${LINENO}]\n"

    printf "INFO[`date`] - list of files output by the application -- begin [${TSCR},${LINENO}]\n"
    ls -lart ${OUTPUT_DFNAME}*
    printf "INFO[`date`] - list of files output by the application -- end   [${TSCR},${LINENO}]\n"

    if [ -f ${SUCC_FILE} ] && [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ ! -f ${SUCC_FILE} ] && [ ! -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - neither_success_nor_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -f ${SUCC_FILE} ] && [ -f ${PROC_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_process_present_not_good ${SUCC_FILE} ${PROC_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -f ${SUCC_FILE} ] && [ ! -f ${DONE_FILE} ] ; then
        printf "\nERROR[`date`] - success_but_no_done ${SUCC_FILE} ${DONE_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -f ${FAIL_FILE} ] && [ -f ${DONE_FILE} ] ; then
        printf "\nERROR[`date`] - fail_and_done_not_good ${FAIL_FILE} ${DONE_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - failure_reported_by_application ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${SUCC_FILE} ] && [ -f ${DONE_FILE} ] ; then
        printf "INFO[`date`] - success-file[${SUCC_FILE}] and done-file[${DONE_FILE}] are present; job-success ; will remove success file. [${TSCR},${LINENO}]\n"
        rm -f ${SUCC_FILE}
        printf "INFO[`date`] - job_done_going_to_look_for_more_data_in_the_table_now [${TSCR},${LINENO}]\n"
        continue
    fi
done

unset DBLOG ; unset DBSVR ; unset DBPWD
printf "INFO[`date`] - end of script; PID[$$] [${TSCR},${LINENO}]\n"

return 0

