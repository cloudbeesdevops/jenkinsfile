#!/bin/ksh
# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504 / pardaman singh
# Create Date:   2018/01/31 10:26:06
#
# Copyright (c) 2018 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`
printf "INFO[`date`] - begin of script; hostname[`hostname`] user[`id --user --name`] PID[$$] [${TSCR},${LINENO}]\n"
. ${SCRIPTS}/eraCommon.sh

FILEPATTERN="_MUTUPLOAD_"
unset CLASSPATH ; CLASSPATH=${APPLICATION_JARS} ; export CLASSPATH
if [ ! -z ${APMCLASSPATH} ] ; then
    CLASSPATH=${CLASSPATH}:${APMCLASSPATH}
fi

IND_SML="S"
IND_MED="M"
IND_LAR="L"
DIR_TO_WATCH="/a/directory/that/does/not/exist"

function do_initializations
{
    DIR_TO_WATCH=""
    if [ -z "${APPLICATION_CONFIG}" ] ; then
        printf "\nERROR[`date`] - a necessary input indicator is absent [${APPLICATION_CONFIG}], a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    elif [ "${IND_SML}" = "${APPLICATION_CONFIG}" ] ; then
        DIR_TO_WATCH=/var/fedex/srs/rdt/data/sxfr/infiles/era_mass_adj_mut/small
    elif [ "${IND_MED}" = "${APPLICATION_CONFIG}" ] ; then
        DIR_TO_WATCH=/var/fedex/srs/rdt/data/sxfr/infiles/era_mass_adj_mut/medium
    elif [ "${IND_LAR}" = "${APPLICATION_CONFIG}" ] ; then
        DIR_TO_WATCH=/var/fedex/srs/rdt/data/sxfr/infiles/era_mass_adj_mut/large
    else
        printf "\nERROR[`date`] - a necessary input indicator [${APPLICATION_CONFIG}] is invalid, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    export DIR_TO_WATCH

    if [ ! -d ${DIR_TO_WATCH} ] ; then
        printf "\nERROR[`date`] - the directory to watch [${DIR_TO_WATCH}] is invalid, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    elif [ ! -r ${DIR_TO_WATCH} ] ; then
        printf "\nERROR[`date`] - the directory to watch [${DIR_TO_WATCH}] is not readable, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    elif [ ! -w ${DIR_TO_WATCH} ] ; then
        printf "\nERROR[`date`] - the directory to watch [${DIR_TO_WATCH}] is not writeable, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    printf "\nINFO[`date`] - some variable(s), only for information;\n[DIR_TO_WATCH=${DIR_TO_WATCH}]\n[CLASSPATH=${CLASSPATH}]\n[${TSCR},${LINENO}]\n\n"

    return 0
}

do_initializations

while [ 1 ]
do
    if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
        printf "INFO[`date`] - stop-file[${STOPFILE_FOR_BACKGROUND_JOB}] is present; so we obey and stop. [${TSCR},${LINENO}]\n"
        break
    fi

    typeset -i count=$(ls -1 ${DIR_TO_WATCH}/*${FILEPATTERN}*.done 2> /dev/null |wc -l)
    if [ 0 -eq ${count} ] ; then
        ping_alive; sleep ${ITERATION_INTERVAL}
        continue
    fi
    unset DONE_FILE ; DONE_FILE=$(ls -1 ${DIR_TO_WATCH}/*${FILEPATTERN}*.done|head -n1) ; export DONE_FILE
    unset DATA_FILE ; DATA_FILE=$(echo ${DONE_FILE}|sed 's/\.done$//')                  ; export DATA_FILE
    unset PROC_FILE ; PROC_FILE=${DATA_FILE}.process                                    ; export PROC_FILE
    unset SUCC_FILE ; SUCC_FILE=${DATA_FILE}.success                                    ; export SUCC_FILE
    unset FAIL_FILE ; FAIL_FILE=${DATA_FILE}.failure                                    ; export FAIL_FILE
    if [ ! -f ${DATA_FILE} ] || [ ! -s ${DATA_FILE} ] || [ ! -r ${DATA_FILE} ] ; then
        printf "\nERROR[`date`] - data-file=${DATA_FILE} corresponding to done-file=${DONE_FILE} is not available for us to work with, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    if [ ! -f ${DONE_FILE} ] || [ ! -r ${DONE_FILE} ] ; then
        printf "\nERROR[`date`] - done-file=${DATA_FILE} is not available for us to work with, a problem -- goodbye. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_create via the command \"touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi
    rm -f ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}
    if [ 0 -ne $? ] ; then
        printf "\nERROR[`date`] - file_initialization_removal via the command \"touch ${PROC_FILE} ${SUCC_FILE} ${FAIL_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    printf "INFO[`date`] - file-pair [`ls -li --full-time ${DATA_FILE}`] and [`ls -li --full-time ${DONE_FILE}`] [${TSCR},${LINENO}]\n"

    rm -fv ${DONE_FILE}
    if [ 0 -eq $? ] ; then
        printf "INFO[`date`] - done_file_removal via the command \"rm -fv ${DONE_FILE}\" was a great success. [${TSCR},${LINENO}]\n"
    else
        printf "\nERROR[`date`] - done_file_removal via the command \"rm -fv ${DONE_FILE}\" did NOT succeed. [${TSCR},${LINENO}]\n"
        exit 1
    fi

    unset APPLICATION_ARGS
    APPLICATION_ARGS=""
    APPLICATION_ARGS="${APPLICATION_ARGS} -Dapp.input.directory=`dirname ${DATA_FILE}`"
    APPLICATION_ARGS="${APPLICATION_ARGS} -Dapp.input.filename=`basename ${DATA_FILE}`"
    APPLICATION_ARGS="${APPLICATION_ARGS} -Dapp.output.directory=`dirname ${DATA_FILE}`"
    APPLICATION_ARGS="${APPLICATION_ARGS} -Dapp.batch.type=BTYPE=${APPLICATION_CONFIG}"
    APPLICATION_ARGS="${APPLICATION_ARGS} -Dapp.hostname=BHOST=`hostname`"
    APPLICATION_ARGS="${APPLICATION_ARGS} ${RDT_JSECPROP_DEFINE} ${APMJAVAOPTIONS} -cp ${CLASSPATH}"
    export APPLICATION_ARGS

    if [ ! -z "${SCRIPTS_IN_DEBUG_MODE}" ] ; then
        printf "DEBUG[`date`] -\nJAVA=${JAVA}\nAPPLICATION_ARGS=${APPLICATION_ARGS}\nAPPLICATION_CLASS=${APPLICATION_CLASS}\nin [${TSCR},${LINENO}]\n"
    fi

    SUBLOGFILE=${LOGS}/sublog_${APPLICATION_NAME}.`date +"%Y%m%d%H%M%S%N"`.P$$.R${RANDOM}.log.txt;sleep 3
    printf "INFO[`date`] - application_start data-file=${DATA_FILE} logfile=${SUBLOGFILE} [${TSCR},${LINENO}]\n"
    ${JAVA} ${APPLICATION_ARGS} ${APPLICATION_CLASS} > ${SUBLOGFILE} 2>&1 ; typeset -i app_ret_val=$?
    printf "INFO[`date`] - application_stop  app_ret_val=[${app_ret_val}],data-file=${DATA_FILE} [${TSCR},${LINENO}]\n"

    printf "INFO[`date`] - list of files output by the application -- begin [${TSCR},${LINENO}]\n"
    ls -larti --full-time ${SUCC_FILE} ${FAIL_FILE} ${PROC_FILE}
    printf "INFO[`date`] - list of files output by the application -- end   [${TSCR},${LINENO}]\n"

    if [ -f ${SUCC_FILE} ] && [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ ! -f ${SUCC_FILE} ] && [ ! -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - neither_success_nor_failure_present_not_good ${SUCC_FILE} ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    elif [ -f ${SUCC_FILE} ] && [ -f ${PROC_FILE} ] ; then
        printf "\nERROR[`date`] - both_success_and_process_present_not_good ${SUCC_FILE} ${PROC_FILE} [${TSCR},${LINENO}]\n"
        exit 1
    fi

    if [ -f ${FAIL_FILE} ] ; then
        printf "\nERROR[`date`] - failure_reported_by_application ${FAIL_FILE} [${TSCR},${LINENO}]\n"
        # Log a message, alert the team; but continue; no exit
    fi

    if [ -f ${PROC_FILE} ] ; then
        printf "\nERROR[`date`] - process_file_present_not_good ${PROC_FILE} [${TSCR},${LINENO}]\n"
        # Log a message, alert the team; but continue; no exit
    fi

    if [ -f ${SUCC_FILE} ] ; then
        data_filename_only=$(basename ${DATA_FILE})
        archive_dfname=${ARCHIVE_DIR}/45D/era_mass_adj_mut/${data_filename_only}.`date +%Y-%m-%dT%H%M%SUTC`
        cp -v ${DATA_FILE} ${archive_dfname}
        if [ 0 -ne $? ] ; then
            printf "\nERROR[`date`] - archival_step_failure ${DATA_FILE} ${archive_dfname} [${TSCR},${LINENO}]\n"
            exit 1
        else
            printf "INFO[`date`] - archival_with_the_command \"cp -v ${DATA_FILE} ${archive_dfname}\" was a grand success. [${TSCR},${LINENO}]\n"
        fi
        rm -fv ${DATA_FILE} ${SUCC_FILE}
        if [ 0 -eq $? ] ; then
            printf "INFO[`date`] - data_and_success_file_removal via the command \"rm -f ${DATA_FILE} ${SUCC_FILE}\" was a great success. [${TSCR},${LINENO}]\n"
        else
            printf "\nERROR[`date`] - data_file_removal_failure ${DATA_FILE} ${SUCC_FILE} [${TSCR},${LINENO}]\n"
            exit 1
        fi
    fi
    printf "INFO[`date`] - job_done_going_to_look_for_more_data_files_now [${TSCR},${LINENO}]\n"
done

printf "INFO[`date`] - end of script; PID[$$] [${TSCR},${LINENO}]\n"

return 0

