#!/bin/ksh
# @(#)$URL$$Rev$$Date$$Author$

###############################################################################
#    $HeadURL$
#   $Revision$
# Tab Setting:   4
#      Author:   487504 / pardaman singh
# Create Date:   2017/12/18 18:38:34
#
# Copyright (c) 2017 FedEx Services.
# All Rights Reserved.
# This material is copyrighted as an unpublished work under sections 104 and
# 408 of title 17 of the United States Code. It contains trade secret data
# which is the property of FedEx Services. It is submitted to
# the recipient in confidence and information contained herein may not be
# used, copied or disclosed in whole or in part except as permitted by a
# written agreement signed by an officer of FedEx Services.
###############################################################################

TSCR=`basename $0`
RID=${TSCR%.sh}.`date +"%Y%m%d%H%M%S%N"`.U`id --user --name`.P$$.R${RANDOM}
log=${LOGS}/${RID}.log.txt
inp_scr_cfg=${SCRIPTS}/eraCleanup.cfg
scr_cfg=${LOGS}/${RID}.cfg.txt
typeset -i interval=30 # seconds
autorolledindicator=autorolled
na="@na" # not applicable
rmcommand=REMOVE

function do_an_iteration
{
    typeset d="" # directory
    typeset p="" # pattern
    typeset s="" # size examples: 10M for 10Megabytes , 7k for 7Kilobytes; (format to comply with find command)
    typeset a="" # age_info (format=days,destination_directory) example: 15,/var/fedex/srs/rdt/data/archive/45D/edi for fifteen days old files to move to the indicated directory

    typeset cmet_src_fqf=""       # fully qualified source filename, for files where the criteria is met
    typeset cmet_src_filename=""  # source filename only
    typeset cmet_dest_filename="" # destination filename only
    typeset cmet_dest_fqf=""      # fully qualified destination filename

    while read d p s a
    do
        if [ -z ${d} ] ; then
            printf "\n\n[WARN on `hostname` at `date`] (${RID}) empty directory; skipping-line (${d},${p}${s},${a}) [${TSCR},${LINENO}]\n\n"
            continue
        elif [ -z ${p} ] ; then
            printf "\n\n[WARN on `hostname` at `date`] (${RID}) empty pattern; skipping-line (${d},${p}${s},${a}) [${TSCR},${LINENO}]\n\n"
            continue
        elif [ -z ${s} ] ; then
            printf "\n\n[WARN on `hostname` at `date`] (${RID}) empty size; (${d},${p}${s},${a}) [${TSCR},${LINENO}]\n\n"
            continue
        elif [ -z ${a} ] ; then
            printf "\n\n[WARN on `hostname` at `date`] (${RID}) empty age; (${d},${p}${s},${a}) [${TSCR},${LINENO}]\n\n"
            continue
        fi
        if [ ! -d ${d} ] ; then
            continue
        fi

        if [ "${s}" != "${na}" ] ; then
            for cmet_src_fqf in $( find -L ${d} -maxdepth 1 -type f -name "${p}" -readable -writable -user `id --user --name` -size +${s} )
            do
                cmet_src_filename=`basename ${cmet_src_fqf}`
                cmet_dest_filename=${cmet_src_filename}.${autorolledindicator}.`date +"%Y%m%d%H%M%S%N"`
                cmet_dest_fqf=`dirname ${cmet_src_fqf}`/${cmet_dest_filename}
                cp -v ${cmet_src_fqf} ${cmet_dest_fqf}
                if [ 0 -eq $? ] ; then
                    > ${cmet_src_fqf}
                    if [ 0 -eq $? ] ; then
                        printf "[INFO on `hostname` at `date`] (${RID})\nFile ${cmet_src_fqf} rolled over to ${cmet_dest_fqf}\nAND\n${cmet_src_fqf} was reset\nFYI. [${TSCR},${LINENO}]\n"
                    else
                        printf "\n\n[ERROR on `hostname` at `date`] (${RID}) The autoroll_clear failed i.e. ( > ${cmet_src_fqf} ) failed; a problem; goodbye. [${TSCR},${LINENO}]\n\n"
                        exit 1
                    fi
                else
                    printf "\n\n[ERROR on `hostname` at `date`] (${RID}) The autoroll_cp failed i.e. (cp -v ${cmet_src_fqf} ${cmet_dest_fqf}) failed; a problem; goodbye. [${TSCR},${LINENO}]\n\n"
                    exit 1
                fi
            done
            continue
        fi

        if [ "${a}" != "${na}" ] ; then
            typeset days=`printf "${a}"|cut -f1 -d','`
            typeset ddir=`printf "${a}"|cut -f2 -d','`
            if [ "x${rmcommand}" = "x${ddir}" ] ; then
                echo > /dev/null # do nothing
            else
                if [ ! -d ${ddir} ] ; then
                    printf "\n\n[WARN on `hostname` at `date`] (${RID}) directory for aged data(${ddir}), is NOT a directory; (${d},${p}${s},${a}) [${TSCR},${LINENO}]\n\n"
                    continue
                fi
            fi

            for cmet_src_fqf in $( find -L ${d} -maxdepth 1 -type f -name "${p}" -readable -writable -user `id --user --name` -mtime +${days} )
            do
                if [ "x${rmcommand}" = "x${ddir}" ] ; then
                    rm -fv ${cmet_src_fqf}
                    if [ 0 -eq $? ] ; then
                        printf "[INFO on `hostname` at `date`] (${RID}) The aged_file_removal worked i.e. ( rm -fv ${cmet_src_fqf} ) , a good thing. [${TSCR},${LINENO}]\n"
                    else
                        printf "\n\n[ERROR on `hostname` at `date`] (${RID}) The aged_file_removal failed i.e. ( rm -fv ${cmet_src_fqf} ) failed; a problem; goodbye. [${TSCR},${LINENO}]\n\n"
                        exit 1
                    fi
                else
                    cmet_src_filename=`basename ${cmet_src_fqf}`
                    cmet_dest_filename=${cmet_src_filename}.`date +"%Y%m%d%H%M%S%N"`
                    cmet_dest_fqf=${ddir}/${cmet_dest_filename}
                    mv -v ${cmet_src_fqf} ${cmet_dest_fqf}
                    if [ 0 -eq $? ] ; then
                        printf "[INFO on `hostname` at `date`] (${RID}) The aged_file_move worked i.e. (mv -v ${cmet_src_fqf} ${cmet_dest_fqf}}) , a good thing. [${TSCR},${LINENO}]\n"
                    else
                        printf "\n\n[ERROR on `hostname` at `date`] (${RID}) The aged_file_move failed i.e. (mv -v ${cmet_src_fqf} ${cmet_dest_fqf}}) failed; a problem; goodbye. [${TSCR},${LINENO}]\n\n"
                        exit 1
                    fi
                fi
            done
        fi
    done < ${scr_cfg}

    return 0

} # end function

function perform_work
{
    printf "\n[INFO on `hostname` at `date`] (${RID}) Begin of work. [${TSCR},${LINENO}]\n\n"
    if [ ! -f ${inp_scr_cfg} ] || [ ! -r ${inp_scr_cfg} ] || [ ! -s ${inp_scr_cfg} ] ; then
        printf "\n\n[ERROR on `hostname` at `date`] (${RID}) The configuration (${inp_scr_cfg}) is not available(permissions/size); a problem; goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    cat ${inp_scr_cfg}|grep -v "#"|grep -v "^$" > ${scr_cfg}
    if [ 0 -ne $? ] ; then
        printf "\n\n[ERROR on `hostname` at `date`] (${RID}) The configuration copy (cat ${inp_scr_cfg} > ${scr_cfg}) failed; a problem; goodbye. [${TSCR},${LINENO}]\n\n"
        exit 1
    fi

    printf "[INFO on `hostname` at `date`] (${RID}) FYI [stopfile=${STOPFILE_FOR_BACKGROUND_JOB}] [config=${scr_cfg}] just in case you need it . . . [${TSCR},${LINENO}]\n\n"

    typeset -i loop_flag=1
    while [ 1 -eq ${loop_flag} ]
    do
        if [ -f ${STOPFILE_FOR_BACKGROUND_JOB} ] ; then
            printf "\n\n[INFO on `hostname` at `date`] (${RID}) The stop file (`ls -li --full-time ${STOPFILE_FOR_BACKGROUND_JOB}`) is present; we thus quit looping. [${TSCR},${LINENO}]\n\n"
            let loop_flag=0; continue
        fi
        do_an_iteration
        ping_alive ; touch ${scr_cfg}
        sleep ${interval}
    done
    printf "\n[INFO on `hostname` at `date`] (${RID}) End of work. [${TSCR},${LINENO}]\n\n"

    return 0

} # end function

#########################
# main work begins here #
#########################
. ${SCRIPTS}/eraCommon.sh
perform_work

return 0 

