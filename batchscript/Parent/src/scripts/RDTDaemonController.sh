daemon_cnt()
{
ps -fu $WHOAMI | grep "./${BASE_NAME}" | grep -cwv "grep"
}


###############################################################################
#
# Function:		start_d
#
# Syntax:		start_d()
#
# Description:  This function is called from main to start the daemon
#
# Notes:		None
#
###############################################################################
start_d()
{

	#--------------------------------------------------
	# Capture the number of daemons currently running & 
	# return successfully if the right number is found.
	# However, this check can be skipped when needed by 
	# setting SKIP_DAEMON_CNT_CHECK to Y
	#--------------------------------------------------


	if [[ "$SKIP_DAEMON_CNT_CHECK" != "Y" ]]
	then
		NUM_DAEMONS=$(daemon_cnt)

		if [[ $NUM_DAEMONS -eq $ID_DAEMON_CNT ]]
		then
			echo "$BASE_NAME: Daemons is already running..."
			return 0
		fi

		#------------------------------------------------
		# If we have any instance running, shut them down
		#------------------------------------------------
		if [[ $NUM_DAEMONS -ne 0 ]]
		then
			echo "$BASE_NAME: Shutting down remaining daemons for ${ID_DAEMON_EXE_NM}..." 
			stop_d

			#-----------------------------------
			# Fail if shutdown was unsuuccessful
			#-----------------------------------
			if [[ $? -ne 0 ]]
			then
				echo "$BASE_NAME: Failed to shutdown remaining daemons!"
				return 1
			fi
		fi
	fi

	#--------------------------------
	# Start up the all of the daemons
	#--------------------------------
	echo "$BASE_NAME: starting daemons..."

	for INSTC_NBR in {1..$ID_DAEMON_CNT}
	do
		nohup ${START_SCRIPT_NAME} > /dev/null 2>&1 &
	done

	return 0
}


###############################################################################
#
# Function:		stop_d
#
# Syntax:		stop_d
#
# Description:  This function is called from main to stop the daemon	
#
# Notes:		None
#
###############################################################################
stop_d()
{
	#---------------------------
	# check if daemon is running
	#---------------------------
	echo $(daemon_cnt)
	if [[ $(daemon_cnt) -gt 0 ]]
	then
		echo "$BASE_NAME: shutting down daemon ..."

		#PIDS=`/bin/ps -fu $WHOAMI | grep "$JOB_NAME" | grep -v "grep" | awk '{print $2}'`

#		kill -s $ID_KILL_SIG $PIDS
		nohup ${STOP_SCRIPT_NAME} > /dev/null 2>&1 &

		if [[ $? -ne 0 ]]
		then
			echo "Error: $SCRIPT_NAME stop_d() - failed to terminate daemon " 
			return 1
		fi

		while [ $(daemon_cnt) -gt 0 ]
		do
			sleep 5
		done

		echo "$BASE_NAME: terminated ..."

	else
		echo "$BASE_NAME: daemon is not running...nothing to be done"

	fi

	return 0
}

###############################################################################
#
# Function:		status_d
#
# Syntax:		status_d
#
# Description:  This function is called from main to check on the daemon	
#
# Notes:		None
#
###############################################################################
status_d()
{
	count=$(daemon_cnt)
	echo "count in status $count"
	echo " actual no $ID_DAEMON_CNT"
	#if [[ ("$SKIP_DAEMON_CNT_CHECK" != "Y" && $count -eq $ID_DAEMON_CNT) ]]
	#then
	#	echo "$BASE_NAME: daemon is running"
	#	return 0
	#fi
	#echo "$BASE_NAME: daemon is not running"
	#return 1

	if [[ ("$SKIP_DAEMON_CNT_CHECK" != "Y" && $count -ne $ID_DAEMON_CNT) ||
		("$SKIP_DAEMON_CNT_CHECK" == "Y" && $count -lt $ID_DAEMON_CNT) ]]
	then
		echo "Error: $BASE_NAME status_d - Found $count instances of $BASE_NAME running when expecting $ID_DAEMON_CNT."
		return 1
	fi

	return 0
}


DAEMONNAME=$2
#SCRIPT_NAME=$2

if [[ "$DAEMONNAME" == "RDTRebillPublishBatch" ]]
then
	START_SCRIPT_NAME="/opt/fedex/srs/rdt/current/scripts/RDTRebillPublishBatch/RDTRebillPublishBatch.sh"
	STOP_SCRIPT_NAME="/opt/fedex/srs/rdt/current/scripts/RDTRebillPublishBatch/STOPRDTRebillPublishBatch.sh"
	JOB_NAME="com.fedex.srs.rdt.rebill.release.trigger.StartupJMSListenerContainer"
	ID_DAEMON_CNT=1
	SKIP_DAEMON_CNT_CHECK=N
else
	echo "$DAEMONNAME is not a configured daemon process."
    echo " Script configure with Daemon RDTRebillPublishBatch"
    exit 1
fi



ACTION=$1
BASE_NAME=`basename $START_SCRIPT_NAME`

ID_KILL_SIG="TERM"
echo "script name $START_SCRIPT_NAME"
echo " action:$ACTION"
echo " basename:$BASE_NAME"

#---------------------------------------------------------------
# Check usage, expect two mandatory parameters -scriptname and -action
#---------------------------------------------------------------
WHOAMI=$(/usr/bin/id -u)
echo "who $WHOAMI"
if [[ $# -lt 2 ]]
then
	echo "Error: ./RDTDaemonController.sh start|stop|status <DAEMONNAME> - invalid usage"
	exit 1
fi

case $ACTION in

	start)

		start_d;;

	stop)

		stop_d;;

	status)
		
		status_d;;

	*) 
	echo "Error: /RDTDaemonController.sh start|stop|status <DAEMONNAME> - invalid usage"
		exit 1	
esac

